//
//  AppDelegate.h
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HomeVC.h"
#import "EventsVC.h"
#import "EnergyVC.h"
#import "SettingsVC.h"
#import "MoreVC.h"
#import "TimersVC.h"

@class LoginVC;
@class MainVC;
@class LightwaveVC;
@class ZoneVC;
@class RegisterVC;
@class CodeVC;
@class IntroVC;
@class Zone;
@class Event;
@class WebVC;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *lwController;
@property (strong, nonatomic) UINavigationController *nav1Controller;
@property (strong, nonatomic) LoginVC *loginController;
@property (strong, nonatomic) RegisterVC *registerController;
@property (strong, nonatomic) CodeVC *codeController;
@property (strong, nonatomic) IntroVC *introController;

@property (strong, nonatomic) NSString *tmpName;
@property (strong, nonatomic) NSString *tmpEmail;
@property (strong, nonatomic) NSString *tmpPin;
@property (strong, nonatomic) NSString *tmpMac;

- (void)switchView:(NSString *)viewName;
- (WebVC *)helpVC;
- (MoreVC *)moreVC;
- (UIViewController *)setupFifthTabController;

@end
