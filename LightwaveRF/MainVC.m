//
//  MainVC.m
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "MainVC.h"
#import "AppDelegate.h"

@interface MainVC ()

@end

@implementation MainVC
@synthesize nav_bar;
@synthesize list_table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    
    NSLog(@"item selected %@", item.title);
    nav_bar.topItem.title = item.title;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Main"];
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(edit)];
    nav_bar.topItem.title = @"Your Home";
    nav_bar.topItem.rightBarButtonItem = editButton;
    [editButton release];
    [list_table setDataSource:self];
    // Do any additional setup after loading the view from its nib.
}

- (void)edit{
    NSLog(@"Edit pressed");
}

- (void)viewDidUnload
{
    [self setNav_bar:nil];
    [self setList_table:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSLog(@"numberOfSections called");
    return 1;
}

-(NSString *)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Zones";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    return [app.zones count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"ZoneCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
    }
    
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString *zone = [app.zones objectAtIndex:indexPath.row];
    
    cell.textLabel.text = zone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString *zone = [app.zones objectAtIndex:indexPath.row];
    
    NSLog(@"Zone selected is %@", zone);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [nav_bar release];
    [list_table release];
    [super dealloc];
}
@end
