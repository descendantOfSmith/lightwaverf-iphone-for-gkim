//
//  EventActionsVC.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "Event.h"
#import "Action.h"
//#import "LightwaveActionSheet.h"
#import "HourMinSecActionSheet.h"
#import "EventActionCell.h"
#import "EventActionDelayCell.h"

typedef enum
{
    kEnergyActionsVCAddDelay = 0,
    kEnergyActionsVCAddRandomDelay,
    kEnergyActionsVCCancel
} EventActionsVCActionSheetTags;

@interface EventActionsVC : RootViewController <
UITableViewDelegate, UITableViewDataSource,
UITextFieldDelegate,
LightwaveActionSheetDataSource, LightwaveActionSheetDelegate,
EventActionDelayCellDelegate> {
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil event:(Event *)tmpEvent;

@end
