//
//  HeatingVC.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 06/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "RootViewController.h"
#import "Zone.h"
#import "HeatingCell.h"
#import "LightwaveActionSheet.h"
#import "DateTimeActionSheet.h"
#import "TempActionSheet.h"

/*****************************************
 * ActionSheet Types
 *****************************************/
typedef enum
{
    kHeatingVCAddProfile = 0,
    kHeatingVCStartTime,
    kHeatingVCEndTime,
    kHeatingVCTemperature,
    kHeatingVCStartProfile
} HeatingVCActionSheetTags;

/*****************************************
 * Select Date ActionSheet
 *****************************************/
typedef enum
{
    kHeatingVCAddProfileWeekday = 0,
    kHeatingVCAddProfileWeekend,
    kHeatingVCAddProfileHoliday,
    kHeatingVCAddProfileCancel
} HeatingVCAddProfileActionSheetTags;

/*****************************************
 * Start || End Time ActionSheet
 *****************************************/
typedef enum
{
    kHeatingVCTimeSet = 0,
    kHeatingVCTimeCancel
} HeatingVCTimeActionSheetTags;

/*****************************************
 * Temperature ActionSheet
 *****************************************/
typedef enum
{
    kHeatingVCTemperatureSet = 0,
    kHeatingVCTemperatureCancel
} HeatingVCTemperatureActionSheetTags;

/*****************************************
 * Start Profile ActionSheet
 *****************************************/
typedef enum
{
    kHeatingVCStartProfileSetDate = 0,
    kHeatingVCStartProfileCancel
} HeatingVCStartProfileActionSheetTags;

@interface HeatingVC : RootViewController <UITableViewDelegate, UITableViewDataSource,
                                            HeatingCellDelegate,
                                            LightwaveActionSheetDataSource, LightwaveActionSheetDelegate> {
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil zone:(Zone *)_zone;

@end
