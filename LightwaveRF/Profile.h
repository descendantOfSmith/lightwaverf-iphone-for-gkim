//
//  Profile.h
//  LightwaveRF
//
//  Created by Nik Lever on 25/06/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profile : NSObject{

}

@property (nonatomic, assign) int profileID;
@property (nonatomic, assign) int type;
@property (nonatomic, assign) int zoneId;
@property (nonatomic, strong) NSString *start;
@property (nonatomic, strong) NSString *end;
@property (nonatomic, assign) float temp;

-(id) initWithProfileId:(int)_profileId type:(int)_type zoneId:(int)_zoneId temp:(float)_temp start:(NSString*)_start end:(NSString*)_end;
-(id) initWithZoneId:(int)_zoneId temp:(float)_temp start:(NSString*)_start end:(NSString*)_end;

- (double)getStartTime;
- (double)getEndTime;
- (NSString *)getCommand;
- (NSString *)toString;
- (NSDictionary *) toDictionary;
- (NSData *) toJSON;

@end
