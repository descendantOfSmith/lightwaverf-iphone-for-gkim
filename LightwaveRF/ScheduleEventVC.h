//
//  ScheduleEventVC.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 01/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "Event.h"
#import "LightwaveActionSheet.h"
#import "DawnDuskActionSheet.h"
#import "DayActionSheet.h"
#import "MonthActionSheet.h"
#import "DateTimeActionSheet.h"

/*****************************************
 * TableView Cells
 *****************************************/
typedef enum
{
    kScheduleEventCellTime = 0,
    kScheduleEventCellDates,
    kScheduleEventCellDays,
    kScheduleEventCellMonths
} ScheduleEventCellTags;

/*****************************************
 * ActionSheet Types
 *****************************************/
typedef enum
{
    kScheduleEventVCMainOptions = 0,
    kScheduleEventVCSetStartDate,
    kScheduleEventVCSetEndDate,
    kScheduleEventVCSetDuskDawn,
    kScheduleEventVCSetDays,
    kScheduleEventVCSetMonths
} ScheduleEventVCActionSheetTags;

/*****************************************
 * Main Options ActionSheet
 *****************************************/
typedef enum
{
    kScheduleEventVCOptionsSetStartTime = 0,
    kScheduleEventVCOptionsSetStartDate,
    kScheduleEventVCOptionsSetEndDate,
    kScheduleEventVCOptionsSetDays,
    kScheduleEventVCOptionsSetMonths,
    kScheduleEventVCOptionsClose
} ScheduleEventVCOptionsActionSheetTags;

/*****************************************
 * Select Start Date ActionSheet
 *****************************************/
typedef enum
{
    kScheduleEventVCStartDateSet = 0,
    kScheduleEventVCStartDateCancel
} ScheduleEventVCStartDateActionSheetTags;

/*****************************************
 * Select End Date ActionSheet
 *****************************************/
typedef enum
{
    kScheduleEventVCEndDateSet = 0,
    kScheduleEventVCEndDateUntilForever,
    kScheduleEventVCEndDateCancel
} ScheduleEventVCEndDateActionSheetTags;

/*****************************************
 * Select Time ActionSheet
 *****************************************/
typedef enum
{
    kScheduleEventVCDuskDawnDone = 0,
    kScheduleEventVCDuskDawnCancel
} ScheduleEventVCDuskDawnActionSheetTags;

/*****************************************
 * Select Days ActionSheet
 *****************************************/
typedef enum
{
    kScheduleEventVCDaysDone = 0,
    kScheduleEventVCDaysCancel
} ScheduleEventVCDaysActionSheetTags;

/*****************************************
 * Select Months ActionSheet
 *****************************************/
typedef enum
{
    kScheduleEventVCMonthsDone = 0,
    kScheduleEventVCMonthsCancel
} ScheduleEventVCMonthsActionSheetTags;

@interface ScheduleEventVC : RootViewController <UITableViewDelegate, UITableViewDataSource, LightwaveActionSheetDataSource, LightwaveActionSheetDelegate> {
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil event:(Event *)_event;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil eventName:(NSString *)_eventName timer:(LTimer *)_timer;
- (void)editPressed;

@end
