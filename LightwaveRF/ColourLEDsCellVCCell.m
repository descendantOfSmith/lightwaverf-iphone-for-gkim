//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import "ColourLEDsCellVCCell.h"
#import "Device.h"
#import "WFLHelper.h"
#import "Toast+UIView.h"
#import "AppDelegate.h"
#import "ButtonsHelper.h"

@interface ColourLEDsCellVCCell ()
@property (strong, nonatomic) UIImageView *onActive_img;
@property (strong, nonatomic) UIImageView *active_img;
@property (strong, nonatomic) UIButton *onButton;
@property (strong, nonatomic) UIButton *offButton;
@property (strong, nonatomic) UIButton *moreButton;
@end

@implementation ColourLEDsCellVCCell

@synthesize icon_img;
@synthesize name_lbl;
@synthesize delegate;
@synthesize device;
@synthesize dimmer_sdr;
@synthesize indexPath;
@synthesize active_img;
@synthesize onActive_img;
@synthesize dimmerbg_img;
@synthesize fadeActive_tmr;
@synthesize onFadeActive_tmr;
@synthesize highlight_img;
@synthesize zoneID;
@synthesize onButton, offButton, moreButton;

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    onActive_img.hidden = YES;
    active_img.hidden = YES;
    
    [icon_img setContentMode:UIViewContentModeScaleAspectFit];
    
    self.dimmer_sdr.minimumValue = DIMMER_MIN;
    self.dimmer_sdr.maximumValue = DIMMER_MAX;
    
    if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isCOCO])
        [self.dimmer_sdr setMinimumTrackTintColor:[UIColor colorWithDivisionOfRed:38.0f green:116.0f blue:32.0f alpha:1.0f]];

    else if ([AppStyleHelper isMegaMan]) {
        [self.dimmer_sdr setMinimumTrackTintColor:[UIColor colorWithDivisionOfRed:137 green:197 blue:39 alpha:1.0f]];
        [self.dimmerbg_img setHidden:YES];
        [self.dimmerUpbg_img setHidden:YES];
    
    } else
        [self.dimmer_sdr setMinimumTrackTintColor:[UIColor colorWithDivisionOfRed:68 green:32 blue:105 alpha:1.0f]];
    
    [self.dimmer_sdr setDataSource:self];
}

- (void)dealloc {
    if (fadeActive_tmr != nil){
        [fadeActive_tmr invalidate];
    }
    
    if (onFadeActive_tmr != nil){
        [onFadeActive_tmr invalidate];
    }
}

+ (float)getCellHeight {
    return 92.0f;
}

- (void)setupCellWithDevice:(Device *)_device
                     zoneID:(int)_zoneID
                  indexPath:(NSIndexPath *)_indexPath
                   delegate:(id<ColourLEDsCellVCDelegate>)_delegate {
    
    self.device = _device;
    self.zoneID = _zoneID;
    self.indexPath = _indexPath;
    self.delegate = _delegate;
    
    [self setupOnButton];
    [self setupOffButton];
    [self setupMoreButton];
    [self.contentView bringSubviewToFront:self.dimmer_sdr];
    
    if (device!=nil)
        [name_lbl setText:device.name];
    
    if (self.device.lastOnState != 0)
        [self.dimmer_sdr setValue:(float)self.device.assumedState animated:NO];
    else {
        [self.dimmer_sdr setValue:DIMMER_MAX / 2 animated:NO];
    }

    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self positionViewsIsEditing:self.isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

- (void)setupOnButton {
    if (!self.onButton) {
        self.onButton = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"on", @"ON") uppercaseString]
                                                    type:kButtonGreenPSwitch
                                                     tag:kBtnOn
                                                   width:40
                                                  height:38
                                                  target:self
                                                selector:@selector(onPressed:)];
        [self.onButton setFrameX:272];
        [self.onButton setFrameY:5];
        [self.contentView addSubview:self.onButton];
    }
    
    if (!self.onActive_img) {
        self.onActive_img = [ViewBuilderHelper getImageViewWithFilename:@"active_blue.png"
                                                                    atX:279
                                                                    atY:32];
        [self.onActive_img setFrameWidth:27];
        [self.onActive_img setFrameHeight:8];
        [self.contentView addSubview:self.onActive_img];
        self.onActive_img.hidden = YES;
    }
}

- (void)setupOffButton {
    if (!self.offButton) {
        self.offButton = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"off", @"OFF") uppercaseString]
                                              type:kButtonGreenOSwitch
                                               tag:kBtnOff
                                             width:40
                                            height:38
                                            target:self
                                          selector:@selector(offPressed:)];
        [self.offButton setFrameX:225];
        [self.offButton setFrameY:5];
        [self.contentView addSubview:self.offButton];
    }
    
    if (!self.active_img) {
        self.active_img = [ViewBuilderHelper getImageViewWithFilename:@"active.png"
                                                                  atX:232
                                                                  atY:32];
        [self.active_img setFrameWidth:27];
        [self.active_img setFrameHeight:8];
        [self.contentView addSubview:self.active_img];
        self.active_img.hidden = YES;
    }
}

- (void)setupMoreButton {
    if (!self.moreButton) {
        self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.moreButton setBackgroundImage:[UIImage imageNamed:@"led_more.png"] forState:UIControlStateNormal];
        [self.moreButton setFrameX:178];
        [self.moreButton setFrameY:5];
        [self.moreButton setFrameWidth:41.0f];
        [self.moreButton setFrameHeight:38.0f];
        [self.moreButton setTag:kBtnMore];
        [self.moreButton addTarget:self action:@selector(morePressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.moreButton];
    }
}

- (void)setIndexPathWithRow:(NSInteger)row andSection:(NSInteger)section{
    indexPath = [NSIndexPath indexPathForRow:row inSection:section];
}




- (float)getPercentage {
    //return self.dimmer_sdr.value * 100;
    return (self.dimmer_sdr.value / DIMMER_MAX) * 100;
}

- (void)alertUserDimmerPercentage:(NSString *)_percentage {
    [kApplicationDelegate.window makeToast:_percentage
                                  duration:3.0f
                                  position:@"center"];
}



- (IBAction)dimmerEditStart:(id)sender {
}

- (IBAction)dimmerDrag:(id)sender {
    self.highlight_img.alpha = [self getPercentage] / 100;
}

- (IBAction)dimmerEditEnd:(id)sender {
    self.highlight_img.alpha = [self getPercentage] / 100;
    
    [self.device setAssumedState:(int)self.dimmer_sdr.value];
    
    [[WFLHelper shared] sendToUDP:YES
                          command:[UDPService getDimCommandWithZoneID:self.zoneID device:device percentage:[self getPercentage]]
                              tag:kRequestDim];
}

#pragma mark - ANPopoverSlider Datasource

- (NSString *)getPopoverText:(ANPopoverSlider *)_slider {
    return [NSString stringWithFormat:@"%.0f%%", [self getPercentage]];
}






- (IBAction)onPressed:(id)sender {
    DebugLog(@"On pressed");
    
    if (self.device.lastOnState != 0) {
        
        [dimmer_sdr setValue:(float)self.device.lastOnState animated:YES];
        [self.device setAssumedState:self.device.lastOnState];
        
        [[WFLHelper shared] sendToUDP:YES
                              command:[UDPService getOnOffCommandWithZoneID:self.zoneID device:device isOn:YES]
                                  tag:kRequestToggleOnOff];
    } else {
        
        [dimmer_sdr setValue:DIMMER_MAX animated:YES];
        [self.device setAssumedState:DIMMER_MAX];
        
        [[WFLHelper shared] sendToUDP:YES
                              command:[UDPService getDimCommandWithZoneID:self.zoneID device:device percentage:[self getPercentage]]
                                  tag:kRequestDim];
    }
    
    [self alertUserDimmerPercentage:[NSString stringWithFormat:@"%.0f%%", [self getPercentage]]];
    
    onActive_img.hidden = NO;
    onActive_img.alpha = 1.0;
    if (onFadeActive_tmr == nil){
        onFadeActive_tmr = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(onFadeActive:) userInfo:nil repeats:YES  ];
    }
}

- (void)onFadeActive:(NSTimer *)timer{
    onActive_img.alpha -= 0.1;
    if (onActive_img.alpha<=0.0){
        [onFadeActive_tmr invalidate];
        onFadeActive_tmr = nil;
        onActive_img.hidden = YES;
    }
}





- (IBAction)offPressed:(id)sender {
    DebugLog(@"Off pressed");
    
    [dimmer_sdr setValue:0.0f animated:YES];
    [self.device setAssumedState:0];
    [[WFLHelper shared] sendToUDP:YES
                          command:[UDPService getOnOffCommandWithZoneID:self.zoneID device:device isOn:NO]
                              tag:kRequestToggleOnOff];
    
    [self alertUserDimmerPercentage:[NSString stringWithFormat:@"0%%"]];
    
    active_img.hidden = NO;
    active_img.alpha = 1.0;
    if (fadeActive_tmr == nil){
        fadeActive_tmr = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(fadeActive:) userInfo:nil repeats:YES  ];
    }
}

- (void)fadeActive:(NSTimer *)timer{
    active_img.alpha -= 0.1;
    if (active_img.alpha<=0.0){
        [fadeActive_tmr invalidate];
        fadeActive_tmr = nil;
        active_img.hidden = YES;
    }
}






- (IBAction)morePressed:(id)sender {
    DebugLog(@"More pressed");
    
    if ([self.delegate respondsToSelector:@selector(colourLEDMorePressed:device:)])
        [self.delegate colourLEDMorePressed:self device:self.device];
}

@end
