//
//  Action.m
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 25/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import "Action.h"
#import "Device.h"
#import "Zone.h"
#import "Home.h"
#import "Mood.h"
#import "NSString+Utilities.h"

@interface Action()

@end

@implementation Action

@synthesize setting = _setting;
@synthesize delay = _delay;
@synthesize udp = _udp;
@synthesize shortString = _shortString;
@synthesize hwKey = _hwKey;

-(Device *)getDevice{
    if (self.hwKey == nil || [self.hwKey isEqualToString:@""]) return nil;
    
    if (self.device == nil){
        Home *home = [Home shared];
        self.device = [home getDeviceFromHardwareKey:self.hwKey];
    }
    return self.device;
}

-(void)device:(Device *)__device{
    self.device = __device;
}

-(NSString *) hardwareKey
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device) return self.device.hardwareKey;
    return @"";
}

-(NSString *) name
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device) return self.device.name;
    return @"";
}

-(NSString *) type
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device) return self.device.type;
    return @"";
}

-(NSString *) zoneName
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device) return self.device.zoneName;
    return @"";
}

-(int)zoneID{
    NSArray *values = [self getValuesFrom:self.hwKey];
    if (values == nil || [values count]!=3) return -1;
    return [values[0] intValue];
}

-(int)deviceID{
    NSArray *values = [self getValuesFrom:self.hwKey];
    if (values == nil || [values count]!=3) return -1;
    return [values[1] intValue];
}

-(void) setAssumedState:(int) setting
{
    self.setting = setting;
}

-(int) assumedState
{
    return self.setting;
}

-(int) lastOnState
{
    return self.setting;
}

-(NSString *) udp
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (!self.device) return @""; // If there still isn't a device then give up and return an empty string.
    NSString *message  = [NSString stringWithFormat:@"Set %@", self.device.name];
    if ([self.device isDimmer])
    {
        if (self.setting == 0) message = [message stringByAppendingString:@"|to Off"];
        else message = [message stringByAppendingFormat:@"|to %d", self.setting];
    }
    else
    {
        if (self.setting == 0) message = [message stringByAppendingString:@"|to Off"];
        else message = [message stringByAppendingString:@"|to On"];
    }
    return [NSString stringWithFormat:@"%@,%@|%@", self.hwKey, self.delay, message];
}

-(NSString *) shortString
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    /*
    if (self.device == nil) {
        Device *__device = [self getDevice];
        return __device.hardwareKey;
    }
    */
    
    NSString *str;
    
    if (self.device == nil){
        int type = [self getTypeIDFromCommandString:self.hwKey];
        //NSArray *values = [self getValuesFrom:self.hwKey];
        switch(type){
            case ACTION_ALLOFF:
                return NSLocalizedString(@"off", @"");
                break;
            case ACTION_MOOD:
                return str = NSLocalizedString(@"start_mood", @"");
                break;
            case ACTION_DIMMER:
                if (self.setting == -2){
                    str = NSLocalizedString(@"on", @"");
                    return [str uppercaseString];
                }else{
                    return [NSString stringWithFormat:@"%d%%", (int)((self.setting / 32.0) * 100.0)];
                }
                break;
            case ACTION_OPENCLOSE:
                if (self.setting==0){
                    str = NSLocalizedString(@"close", @"");
                    return [str uppercaseString];
                }else if (self.setting==1){
                    str = NSLocalizedString(@"open", @"");
                    return [str uppercaseString];
                }else{
                    str = NSLocalizedString(@"stop", @"");
                    return [str uppercaseString];
                }
                break;
            case ACTION_SOCKET:
                if (self.setting == 0){
                    str = NSLocalizedString(@"off", @"");
                    return [str uppercaseString];
                }else{
                    str = NSLocalizedString(@"on", @"");
                    return [str uppercaseString];
                }
                break;
            case ACTION_ERROR:
                return @"";
                break;
        }
    }
    
    if ([self.device isOpenClose] ||
        [self.device isFSL500W] ||
        [self.device isFSL3000W]){
        
        if (self.setting==0){
            str = NSLocalizedString(@"close", @"");
            return [str uppercaseString];
        }else if (self.setting==1){
            str = NSLocalizedString(@"open", @"");
            return [str uppercaseString];
        }else{
            str = NSLocalizedString(@"stop", @"");
            return [str uppercaseString];
        }
    }else{
        if (self.setting == 0)
        {
            str = NSLocalizedString(@"off", @"");
            return [str uppercaseString];
        }
        else if ([self.device isDimmer])
        {
            if (self.setting == -2){
                str = NSLocalizedString(@"on", @"");
                return [str uppercaseString];
            }else{
                return [NSString stringWithFormat:@"%d%%", (int)((self.setting / 32.0) * 100.0)];
            }
        }else if ([self.device isMood]){
            if ([[self.device getMood] isAllOff]){
                str = NSLocalizedString(@"off", @"");
                return [str uppercaseString];
            }else{
                str = NSLocalizedString(@"start_mood", @"");
                return [str uppercaseString];
            }
        }
        else
        {
            str = NSLocalizedString(@"on", @"");
            return [str uppercaseString];
        }
    }
}

-(id) initWithDevice:(Device *)tmpDevice setting:(int) tmpSetting delay:(NSString *) tmpDelay hardwareKey:(NSString *) tmpHardwareKey
{
    if (self = [super init])
    {
        self.device = tmpDevice;
        self.setting = tmpSetting;
        self.delay = tmpDelay;
        
        if ([tmpHardwareKey characterAtIndex:0] != '!')
            tmpHardwareKey = [NSString stringWithFormat:@"!%@", tmpHardwareKey];
        
        self.hwKey = tmpHardwareKey;
        
        if (![tmpHardwareKey containsString:@"F"]) // e.g. !R1D1
            self.hwKey = [NSString stringWithFormat:@"%@%@", tmpHardwareKey, [self settingToKeyString]];
        
        if ([self.hwKey containsString:@"Fa"]) {
            int pos = [self.hwKey rangeOfString:@"Fa"].location;
            self.hwKey = [self.hwKey substringToIndex:pos+2];
        }
    }
    
    return self;
}

-(id) initWithHardwareKey:(NSString *) hardwareKey
{
    Home *home = [Home shared];
    Device *device = [home getDeviceFromHardwareKey:hardwareKey];
    return [self initWithDevice:device setting:[self getSettingFromHardwareKey:hardwareKey] delay:@"00:00:00" hardwareKey:hardwareKey];
}

-(id) initWithDevice:(Device *)device setting:(int)setting
{
    return [self initWithDevice:device setting:setting delay:@"00:00:00" hardwareKey:device.hardwareKey];
}

-(id) initWithHardwareKey:(NSString *)hardwareKey delay:(NSString *) delay
{
    Home *home = [Home shared];
    Device *device = [home getDeviceFromHardwareKey:hardwareKey];
    return [self initWithDevice:device setting:[self getSettingFromHardwareKey:hardwareKey] delay:delay hardwareKey:hardwareKey];
}

-(id) initWithDevice:(Device *)device setting:(int)setting delay:(NSString *) delay
{
    return [self initWithDevice:device setting:setting delay:delay hardwareKey:nil];
}

-(id) initWithCommand:(NSString *) command
{
    NSArray *tokens = [command componentsSeparatedByString:@","];
    NSString *initDelay;
    NSString *initCommand;
    NSString *initHardwareKey;
    int initSetting;
    if ([tokens count] > 1)
    {
        initDelay = [[tokens objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        initCommand = [tokens objectAtIndex:0];
    }
    else
    {
        initDelay = @"00:00:00";
    }
    NSArray *values = [self getValuesFrom:command];
    if (values && [values count] == 3)
    {
        initHardwareKey = [NSString stringWithFormat:@"R%dD%d", [[values objectAtIndex:0] intValue], [[values objectAtIndex:1] intValue]];
        initSetting = [[values objectAtIndex:2] intValue];
    }
    else
    {
        initHardwareKey = command;
        initSetting = 0;
    }
    
    Home *home = [Home shared];
    Device *getDevice = [home getDeviceFromHardwareKey:command];
    
    return [self initWithDevice:getDevice setting:initSetting delay:initDelay hardwareKey:initHardwareKey];
}

-(NSString *)settingToKeyString{
    if (_device == nil){
        return NO;
    }
    
    if (_device.zone == nil){
        return NO;
    }
    
    //We have a device and zone so we need to establish the setting part
    NSString *settingStr;
    if ([_device isMood]){
        
        Mood *mood = [_device getMood];
        if ([mood isAllOff]){
            settingStr = @"Fa";
        }else{
            settingStr = [NSString stringWithFormat:@"FmP%d", mood.moodID];
        }
        
    }else if ([_device isOpenClose] ||
              [_device isFSL500W] ||
              [_device isFSL3000W]){
        
        switch(self.setting){
            case 0:
                //settingStr = @"F)";
                settingStr = @"F0";
                break;
            case 1:
                //settingStr = @"F(";
                settingStr = @"F1";
                break;
            case 2:
                settingStr = @"F^";
                break;
        }
        
    }else if ([_device isDimmer]){
        
        switch(self.setting){
            case 0:
                settingStr = @"F0";
                break;
            case -2:
                settingStr = @"Fo";
                break;
            default:
                settingStr = [NSString stringWithFormat:@"FdP%d", self.setting];
                break;
        }
        
    }else{
        settingStr = [NSString stringWithFormat:@"F%d", self.setting];
    }
    return settingStr;
}

- (BOOL)updateHwKey{
    if (self.hwKey == nil || [self.hwKey isEqualToString:@""]) return NO;
    int pos = [self.hwKey rangeOfString:@"F"].location;
    if (pos!=NSNotFound) return NO; //Already set
    //We have a hwKey so we need to establish the setting part
    NSString *settingStr = [self settingToKeyString];
    self.hwKey = [NSString stringWithFormat:@"%@%@", self.hwKey, settingStr];
    
    return YES;
}

-(int)getSettingFromHardwareKey:(NSString *)hwKey{
    NSArray *values = [self getValuesFrom:hwKey];
    if (values == nil || [values count] != 3) return 0;
    return [[values objectAtIndex:2] intValue];
}

-(int)getTypeIDFromCommandString:(NSString *)cmd{
    //NSString *str = [cmd lowercaseString];
    
    //DebugLog(@"Command = %@", cmd);
    
    if ([cmd rangeOfString:@"a"].location != NSNotFound)
        return ACTION_ALLOFF;
    
    if ([cmd rangeOfString:@"m"].location != NSNotFound)
        return ACTION_MOOD;
    
    if ([cmd rangeOfString:@"d"].location != NSNotFound ||
        [cmd rangeOfString:@"o"].location != NSNotFound)
        
        return ACTION_DIMMER;
    
    if ([cmd rangeOfString:@"("].location != NSNotFound ||
        [cmd rangeOfString:@")"].location != NSNotFound ||
        [cmd rangeOfString:@"^"].location != NSNotFound)
        
        return ACTION_OPENCLOSE;
    
    if ([cmd rangeOfString:@"F"].location != NSNotFound)
        return ACTION_SOCKET;
    
    return ACTION_ERROR;
}

-(NSArray *)getValuesFrom:(NSString *)cmd{
    int roomId = -1;
    int deviceId = -1;
    int settingId = -1;
    //int [] values = { -1, -1, -1 };
    NSArray *values;
    
    int type = [self getTypeIDFromCommandString:cmd];
    if (type==ACTION_ERROR) return nil;
    if ([cmd characterAtIndex:0]=='!') cmd = [cmd substringFromIndex:1];
    if ([cmd characterAtIndex:0]!='R') return nil;
    cmd = [cmd substringFromIndex:1];
    NSString *str = [cmd lowercaseString];
    int dpos = [cmd rangeOfString:@"D"].location;
    int fpos = [cmd rangeOfString:@"F"].location;
    if (dpos==NSNotFound && fpos==NSNotFound) return nil;
    if (dpos!=NSNotFound){
        str = [cmd substringToIndex:dpos];
        roomId = [str intValue];
        if (fpos==NSNotFound) return nil;//A device must have a F action
        str = [cmd substringWithRange:NSMakeRange(dpos+1, fpos-(dpos+1))];
        deviceId = [str intValue];
        cmd = [cmd substringFromIndex:(fpos + 1)];
        
        int pos = [cmd rangeOfString:@"P"].location;
        
        if (pos!=NSNotFound)
            cmd = [cmd substringFromIndex:(pos + 1)];
        
        NSString *setting = [cmd stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([setting isEqualToString:@"("] || [setting isEqualToString:@"&lt;"] ){
            settingId = 1;
        }else if ([setting isEqualToString:@")"]  || [setting isEqualToString:@"&gt;"] ){
            settingId = 0;
        }else if ([setting isEqualToString:@"^"] ){
            settingId = 2;
        }else if ([setting isEqualToString:@"o"] ){
            settingId = -2;
        }else{
            settingId = [[cmd stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] intValue];
        }
        values = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:roomId], [NSNumber numberWithInt:deviceId], [NSNumber numberWithInt:settingId], nil];
    }else{
        str = [cmd substringToIndex:fpos];
        roomId = [str intValue];
        values = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:roomId], [NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1], nil];
    }
    
    //DebugLog(@"Action getValuesFromCommand - roomID: %d deviceID: %d settingID: %d", roomId, deviceId, settingId);
    
    return values;
}

-(BOOL) isDimmer
{
    return [self getTypeIDFromCommandString:self.hwKey] == ACTION_DIMMER;
}

-(BOOL) isColourLEDs
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device)
        return [self.device isColourLEDS];
    return NO;
}

-(BOOL) isSocket
{
    return [self getTypeIDFromCommandString:self.hwKey] == ACTION_SOCKET;
}

-(BOOL) isAllOff
{
    return [self getTypeIDFromCommandString:self.hwKey] == ACTION_ALLOFF;
}

-(BOOL) isOpenClose
{
    return [self getTypeIDFromCommandString:self.hwKey] == ACTION_OPENCLOSE;
}

-(BOOL) isFSL500W {
    return [self getTypeIDFromCommandString:self.hwKey] == ACTION_FSL500W;
}

-(BOOL) isFSL3000W {
    return [self getTypeIDFromCommandString:self.hwKey] == ACTION_FSL3000W;
}

-(BOOL) isLightSwitch
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device)
        return [self.device isLightSwitch];
    return NO;
}

-(BOOL) isHeating
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device)
        return [self.device isHeating];
    return NO;
}

-(BOOL) isMagSwitch
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device)
        return [self.device isMagSwitch];
    return NO;
}

-(BOOL) isPir
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device)
        return [self.device isPir];
    return NO;
}

-(BOOL) isCamera
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device)
        return [self.device isCamera];
    return NO;
}

-(BOOL) isMood
{
    return [self getTypeIDFromCommandString:self.hwKey] == ACTION_MOOD;
}

-(BOOL) isActiveDevice
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device) return [self.device isActiveDevice];
    return NO;
}

-(BOOL) isPassiveDevice
{
    // To do: if there isn't a device then try and get one from the Home class based on the hardware key.
    if (self.device) return [self.device isPassiveDevice];
    return NO;
}

-(int)getMoodID{
    int pos = [self.hwKey rangeOfString:@"mP"].location;
    if (pos==NSNotFound) return -1;
    return [[self.hwKey substringFromIndex:(pos+2)] intValue];
}

-(int) turnOn
{
    return self.lastOnState;
}

-(int) turnOff;
{
    self.setting = 0;
    return self.setting;
}

-(BOOL) isPaired
{
    return YES;
}

- (BOOL)isDelayed {
    if ([self getDelaySecs]>=3)
        return YES;
    else
        return NO;
}

- (int)getDelaySecs {
    NSArray *tokens = [self delayComponents];
    int hours = [[tokens objectAtIndex:0] intValue];
    int minutes = [[tokens objectAtIndex:1] intValue];
    int seconds = [[tokens objectAtIndex:2] intValue];
    return hours * 60 * 60 + minutes * 60 + seconds;
}

- (NSArray *)delayComponents {
    NSString *str = self.delay;
    if ([[str substringToIndex:1] isEqualToString:@"r"]) {
        str = [str substringFromIndex:1];
    }
    
    NSArray *tokens = [str componentsSeparatedByString:@":"];
    return tokens;
}

- (int)getDelaySecsPortion {
    NSArray *tokens = [self delayComponents];
    return [[tokens objectAtIndex:2] intValue];
}

- (int)getDelayMinsPortion {
    NSArray *tokens = [self delayComponents];
    return [[tokens objectAtIndex:1] intValue];
}

- (int)getDelayHoursPortion {
    NSArray *tokens = [self delayComponents];
    return [[tokens objectAtIndex:0] intValue];
}

- (NSString *)getDelay {
    if ([self.delay isEqualToString:@"00:00:00"])
        return self.delay;
    
    if ([self getDelaySecs]<3)
        return @"00:00:03";
    
    return self.delay;
}

- (NSString *)getType {
    Home *home = [Home shared];
    if (self.device==nil)
        self.device = [home getDeviceFromHardwareKey:self.hwKey];
    
    if (self.device==nil)
        return @"";
    
    return [self.device type];
}

- (NSString *)getDeviceType{
    int type = [self getTypeIDFromCommandString:self.hwKey];
    
    switch(type){
        case ACTION_DIMMER:
            return @"D";
            break;
            
        case ACTION_SOCKET:
            return @"O";
            break;
            
        case ACTION_OPENCLOSE:
            return @"P";
            
        case ACTION_FSL500W:
            return @"Q";
            break;
            
        case ACTION_FSL3000W:
            return @"R";
            break;
    }
    
    return @"";
}

-(NSString *) toString
{
    return [NSString stringWithFormat:@"%@,%@", self.hwKey, self.delay];
}

-(NSDictionary *) toDictionary
{
    NSMutableDictionary *actionDictionary = [[NSMutableDictionary alloc] init];
    if (![self.delay isEqualToString:@"00:00:00"]) [actionDictionary setValue:self.delay forKey:@"delay"];
    [actionDictionary setValue:[NSNumber numberWithInt:self.setting] forKey:self.hardwareKey];
    return actionDictionary;
}

-(NSData *) toJSON
{
    NSError *error = nil;
    NSDictionary *jsonAction = self.toDictionary;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonAction options:NSJSONWritingPrettyPrinted error:&error];
    if (!error)
    {
        DebugLog(@"JSON Action data:\n%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
        return jsonData;
    }
    else
    {
        return nil;
    }
}

@end
