//
//  NSString+Utilities.m
//  TemplateARC
//
//  Created by imac on 13/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSString+Utilities.h"

@implementation NSString (Utilities)

- (BOOL)isEmpty {
    return [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""];
}

- (BOOL)containsString:(NSString *)string options:(NSStringCompareOptions)options {
    NSRange rng = [self rangeOfString:string options:options];
    return rng.location != NSNotFound;
}

- (BOOL)containsString:(NSString *)string {
    return [self containsString:string options:0];
}

- (BOOL)isURL {
    return nil != [NSURL URLWithString:self];
}

- (BOOL)isValidEmail {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (NSString *)repeatStringNumberOfTimes:(NSUInteger)times {
    return [@"" stringByPaddingToLength:[self length]*times withString:self startingAtIndex:0];
}

- (NSString *)reverseString {
    NSMutableString *reversedStr;
    int len = [self length];

    reversedStr = [NSMutableString stringWithCapacity:len];     

    while (len > 0)
        [reversedStr appendString:
         [NSString stringWithFormat:@"%C", [self characterAtIndex:--len]]];   
    
    return reversedStr;
}

-(NSString*)stringBetweenString:(NSString*)start andString:(NSString*)end {
    NSRange startRange = [self rangeOfString:start];
    if (startRange.location != NSNotFound) {
        NSRange targetRange;
        targetRange.location = startRange.location + startRange.length;
        targetRange.length = [self length] - targetRange.location;
        NSRange endRange = [self rangeOfString:end options:0 range:targetRange];
        if (endRange.location != NSNotFound) {
            targetRange.length = endRange.location - targetRange.location;
            return [self substringWithRange:targetRange];
        }
    }
    return nil;
}

- (NSString *)inverseCapitalisationString { 
    // This is a quick hack to convert upper case letters to lowercase, and vice versa 
    // It uses the standard C character manipulation functions so it will obviously not 
    // work on anything other than basic ASCII strings. 
    
    // get the length of the string that is to be manipulated
    int len = [self length]; 
    
    // create a string to hold our modified text
    NSMutableString *capitalisedString = [NSMutableString stringWithCapacity:len]; 
    
    // iterate over the original string, pulling out each character for inspection
    for (int i = 0; i < len; i++) 
    { 
        // get the next character in the original string
        char ch = [self characterAtIndex:i]; 
        
        // convert upper-case to lower-case, and lower-case to upper-case
        if (isupper(ch)) 
        { 
            ch = tolower(ch); 
        } 
        else if (islower(ch)) 
        { 
            ch = toupper(ch); 
        } 
        
        // append the manipulated character to the modified string
        [capitalisedString appendString:[NSString stringWithFormat:@"%c", ch]]; 
    } 
    
    // return the newly modified string
    return capitalisedString; 
}

- (NSString *)removeNumbersFromString {
    NSString *trimmedString = nil;
    NSCharacterSet *numbersSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    trimmedString = [self stringByTrimmingCharactersInSet:numbersSet];
    return trimmedString;
}

- (NSString *)stringByTrimmingPrefix:(NSString *)strPrefix {
    NSString *returnValue = [NSString stringWithString:self];
    while ([returnValue hasPrefix:strPrefix]) 
    {
        returnValue = [returnValue substringFromIndex:strPrefix.length];
    }
    return returnValue;
}

- (NSString *)stringByTrimmingLeadingWhitespace {
    NSInteger i = 0;
    
    while ((i < [self length])
           && [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[self characterAtIndex:i]]) {
        i++;
    }
    return [self substringFromIndex:i];
}

- (NSString *)getSpelledOutNumber:(NSInteger)num {
    NSNumber *yourNumber = [NSNumber numberWithInt:(int)num];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterSpellOutStyle];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en"]];
    return [formatter stringFromNumber:yourNumber];
}

- (NSString *)removeLastCharOfString {
    return [self substringToIndex:[self length]-1];
}

- (NSString *)getSpelledOutOrdinalNumber:(NSInteger)num {    
    NSString *spelledOutNumber = [self getSpelledOutNumber:num];
    
    // replace all '-'
    spelledOutNumber = [spelledOutNumber stringByReplacingOccurrencesOfString:@"-" withString:@" "];

    NSArray *numberParts = [spelledOutNumber componentsSeparatedByString:@" "];
    NSMutableString *output = [NSMutableString string];
    NSUInteger numberOfParts = [numberParts count];
    for (int i=0; i<numberOfParts; i++) {
        NSString *numberPart = [numberParts objectAtIndex:i];
        
        if ([numberPart isEqualToString:@"one"])
            [output appendString:@"first"];
        else if([numberPart isEqualToString:@"two"])
            [output appendString:@"second"];
        else if([numberPart isEqualToString:@"three"])
            [output appendString:@"third"];
        else if([numberPart isEqualToString:@"five"])
            [output appendString:@"fifth"];
        else {
            NSUInteger characterCount = [numberPart length];
            unichar lastChar = [numberPart characterAtIndex:characterCount-1];
            if (lastChar == 'y')
            {
                // check if it is the last word
                if (numberOfParts-1 == i)
                { // it is
                    [output appendString:[NSString stringWithFormat:@"%@ieth ", [numberPart removeLastCharOfString]]];
                }
                else
                { // it isn't
                    [output appendString:[NSString stringWithFormat:@"%@-", numberPart]];
                }
            }
            else if (lastChar == 't' || lastChar == 'e')
            {
                [output appendString:[NSString stringWithFormat:@"%@th-", [numberPart removeLastCharOfString]]];
            }
            else
            {
                [output appendString:[NSString stringWithFormat:@"%@th ", numberPart]];
            }
        }
    }
    
    // eventually remove last char
    unichar lastChar = [output characterAtIndex:[output length]-1];
    if (lastChar == '-' || lastChar == ' ')
        return [output removeLastCharOfString];
    else
        return output;
}

- (void)getPhoneNumber {
    CFShow((__bridge CFTypeRef)([[NSUserDefaults standardUserDefaults] objectForKey:@"SBFormattedPhoneNumber"]));
}

@end
