//
//  SwitchLocationVC.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 25/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface SwitchLocationVC : RootViewController <UITableViewDelegate, UITableViewDataSource> {
    
}

@end
