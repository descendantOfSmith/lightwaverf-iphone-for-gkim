//
//  SwitchLocationVC.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 25/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "SwitchLocationVC.h"
#import "Account.h"
#import "DB.h"
#import "Home.h"
#import "AccountCell.h"
#import "ButtonsHelper.h"

@interface SwitchLocationVC ()
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation SwitchLocationVC

@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Creation

- (void)setupUI {
    [self setupNavBar];
    [self setupTableView];
}

- (void)setupNavBar {
    self.navigationItem.title = kAppName;
    
    [self setupTopRightButton];
    [self setupTopLeftButton];
}

- (void)setupTopLeftButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"cancel", nil)
                                                       type:kButtonArrowLeft
                                                        tag:kBtnBack
                                                      width:55
                                                     height:18
                                                     target:self
                                                   selector:@selector(buttonPressed:)];
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;
        
    } else {
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                                          style:UIBarButtonItemStyleBordered
                                                                         target:self
                                                                         action:@selector(buttonPressed:)];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;
    }
}

- (void)setupTopRightButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:self.tableView.isEditing ? NSLocalizedString(@"done", nil) : NSLocalizedString(@"edit", nil)
                                                       type:kButtonArrowLeft
                                                        tag:kBtnEdit
                                                      width:45
                                                     height:18
                                                     target:self
                                                   selector:@selector(buttonPressed:)];
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithCustomView:button];
        [tmpTopRightBtn setTag:kBtnEdit];
        self.navigationItem.rightBarButtonItem = tmpTopRightBtn;
        
    } else {
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithTitle:self.tableView.isEditing ? NSLocalizedString(@"done", nil) : NSLocalizedString(@"edit", nil)
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(buttonPressed:)];
        [tmpTopRightBtn setTag:kBtnEdit];
        self.navigationItem.rightBarButtonItem = tmpTopRightBtn;
    }
}

- (void)setupTableView {
    float padding = 0.0f;
    self.tableView = [ViewBuilderHelper getTableViewWithWidth:self.scrollView.frame.size.width - (padding * 2)
                                                       height:SCREEN_HEIGHT() - (kNavBar.frame.size.height + STATUS_BAR_HEIGHT)
                                                        style:UITableViewStylePlain
                                                       target:self];
    [self.tableView setFrameX:padding];
    [self.tableView setFrameY:0.0f];
    self.tableView.separatorStyle= UITableViewCellSeparatorStyleSingleLine;
    [self.view addSubview:self.tableView];
    self.heightOffset += self.tableView.frame.size.height;
    [self.tableView reloadData];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (BOOL)isCurrentAccount:(NSIndexPath *)indexPath {
    Home *home = [Home shared];
    Account *_account = [home.accounts objectAtIndex:indexPath.row];
    if ([_account isEqual:[home getAccountWithID:[[UserPrefsHelper shared] getCurrentAccountId]]])
        return YES;
    else
        return NO;
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tmpTableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    Home *home = [Home shared];
    return home.accounts.count;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return [AccountCell getCellHeight];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    const char* className = class_getName([self class]);
    NSString *cellIdentifier = cellIdentifier = [NSString stringWithFormat:@"AccountCell-%s", className];
    AccountCell *cell = (AccountCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell = [self setupInitialCell:cell xibName:@"AccountCell"];
    
    Home *home = [Home shared];
    Account *_account = [home.accounts objectAtIndex:indexPath.row];
    
    UIColor *_textColor = nil;
    NSString *_titleStr = nil;
    
    if (_account.name.length > 0 && ![_account.name isEqualToString:@"(null)"]) {
        _textColor= [UIColor blackColor];
        _titleStr = _account.name;
        
    } else {
        _textColor = [UIColor lightGrayColor];
        _titleStr = NSLocalizedString(@"no_name", @"no_name");
        
    }
    
    UIImage *_iconImage = nil;
    if ([self isCurrentAccount:indexPath])
        _iconImage = [UIImage imageNamed:@"icon_account"];
    else
        _iconImage = [UIImage imageNamed:@"icon_account_off"];
    
    // Setup Cell
    [cell setupCellWithTitle:_titleStr
                  detailText:_account.email
                   iconImage:_iconImage];
    
    [cell.titleLabel setTextColor:_textColor];
    
    return cell;
}

- (id)setupInitialCell:(AccountCell *)_cell xibName:(NSString *)_xib {
    if (_cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:_xib owner:self options:nil];
        _cell = [topLevelObjects objectAtIndex:0];
        [_cell setFrameWidth:self.tableView.frame.size.width];
        [_cell.contentView setFrameWidth:self.tableView.frame.size.width];
        [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return _cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Home *home = [Home shared];
    Account *_account = [home.accounts objectAtIndex:indexPath.row];
    if (![self isCurrentAccount:indexPath]) {
        
        // Save All Objects
        Home *home = [Home shared];
        [home saveAll];        
        
        // Swap Account
        [[UserPrefsHelper shared] setCurrentAccountId:_account.accountId];
        
        // Reload Data
        [home loadAndRefreshWFL:YES];
        
        // Refresh
        [self.tableView reloadData];
        
        // Close View
        [self dismissViewAnimated:YES];
    }
}

#pragma mark - Editing Table View

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self isCurrentAccount:indexPath]) {
        return UITableViewCellEditingStyleNone;
    } else
        return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    Home *home = [Home shared];
    DB *db = [DB shared];
    Account *_account = [home.accounts objectAtIndex:indexPath.row];
    if (![self isCurrentAccount:indexPath]) {
        
        if (editingStyle == UITableViewCellEditingStyleDelete){
            
            // Delete
            [db clearAccount:_account.accountId];
            [home removeAccount:_account];
            [home saveAccounts];
            
            // Refresh
            [self.tableView reloadData];
        }
    }
}

#pragma mark - Actions

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnBack:
        {
            [self dismissViewAnimated:YES];
        }
            break;

        case kBtnEdit:
        {
            if ([self.tableView isEditing]) {
                [self.tableView setEditing:NO animated:YES];
                
            } else {
                [self.tableView setEditing:YES animated:YES];
            }
            
            [self setupTopRightButton];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
