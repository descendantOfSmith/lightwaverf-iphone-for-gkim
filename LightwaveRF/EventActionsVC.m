//
//  EventActionsVC.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "EventActionsVC.h"
#import "ButtonsHelper.h"
#import "Constants.h"
#import "EventDevicesVC.h"
#import "Constants.h"
#import "Toast+UIView.h"
#import "Home.h"
#import "HomeVC.h"

@interface EventActionsVC ()
@property (nonatomic, strong) Event *event;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) HourMinSecActionSheet *timeDelayActionSheet;
@property (nonatomic, strong) UIButton *addActionButton;
@property (nonatomic, strong) UIButton *addDelayButton;
@property (nonatomic, strong) Action *selectedAction;
@property (nonatomic, strong) UIView *topHeadingBar;
@property (nonatomic, strong) UITextField *eventTitleTextField;
@end

#define kTopHeadingBarHeight    45.0f

@implementation EventActionsVC

@synthesize event;
@synthesize tableView, timeDelayActionSheet, addDelayButton, addActionButton, selectedAction, topHeadingBar, eventTitleTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil event:(Event *)tmpEvent
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.event = tmpEvent;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
        
    // Help popup
    [self showOneOffAlertForLocalisedKey:@"help5" duration:5.0f];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Creation

- (void)setupUI {
    [self setupNavBar];
    [self setupTopBarButtons];
    [self setupTableView];
    [self setupTimeDelayActionSheet];
}

- (void)setupNavBar {
    self.navigationItem.title = self.event.name;
    
    [self setupTopRightNavBarButton];
    
    if (self.navigationController.viewControllers.count == 1) {
        [self setupTopLeftNavBarButton];
    }
}

- (void)setupTopLeftNavBarButton {
    NSString *_topLeftTitle = NSLocalizedString(@"events", @"Events");
    
    if ([[self.navigationController.viewControllers lastObject] isKindOfClass:[EventActionsVC class]]) {
        _topLeftTitle = NSLocalizedString(@"back", @"Back");
    }
    
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:_topLeftTitle
                                                              type:kButtonArrowLeft
                                                               tag:kBtnBack
                                                             width:60
                                                            height:18
                                                            target:self
                                                          selector:@selector(buttonPressed:)];
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;
        
    } else {
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:_topLeftTitle
                                                                          style:UIBarButtonItemStyleBordered
                                                                         target:self
                                                                         action:@selector(buttonPressed:)];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;
    }
}

- (void)setupTopRightNavBarButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"done", nil)
                                                       type:kButtonArrowRight
                                                        tag:kBtnDone
                                                      width:45
                                                     height:18
                                                     target:self
                                                   selector:@selector(buttonPressed:)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithCustomView:button];
        [tmpTopRightBtn setTag:kBtnDone];
        self.navigationItem.rightBarButtonItem = tmpTopRightBtn;
        
    } else {
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"done", @"Done")
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(buttonPressed:)];
        [tmpTopRightBtn setTag:kBtnDone];
        self.navigationItem.rightBarButtonItem = tmpTopRightBtn;
    }
}

- (void)setupTopLeftBackNavButtonForVC:(UIViewController *)vc {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"cancel", @"Cancel")
                                                       type:kButtonArrowLeft
                                                        tag:0
                                                      width:55
                                                     height:18
                                                     target:self
                                                   selector:@selector(backPressed:)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        vc.navigationItem.leftBarButtonItem = backButton;
        vc.navigationItem.hidesBackButton = YES;
        
    } else {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                       initWithTitle:NSLocalizedString(@"cancel", @"Cancel")
                                       style: UIBarButtonItemStyleBordered
                                       target: nil
                                       action: nil];
        [vc.navigationItem setBackBarButtonItem:backButton];
    }
}

- (void)setupTopBarButtons {
    // Add a containing view for the buttons
    self.topHeadingBar = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.heightOffset, self.scrollView.frame.size.width, kTopHeadingBarHeight)];
    [self.topHeadingBar setBackgroundColor:[UIColor darkGrayColor]];
    [self.topHeadingBar setClipsToBounds:YES];
    [self.view addSubview:self.topHeadingBar];
    self.heightOffset += self.topHeadingBar.frame.size.height;
    
    // Add Buttons
    NSUInteger tmpNumButtons = 2;
    float tmpPadding = 5.0f;
    float tmpWidth = (self.topHeadingBar.frame.size.width / tmpNumButtons) - (tmpPadding * 2);
    float tmpX = 0.0f;
    
    tmpX = [self addButtonToView:self.topHeadingBar text:NSLocalizedString(@"edit_delays", @"Edit") tag:kBtnEdit x:tmpX width:tmpWidth padding:tmpPadding];
    [self addButtonToView:self.topHeadingBar text:NSLocalizedString(@"add_action", @"Add Action") tag:kBtnAddAction x:tmpX width:tmpWidth padding:tmpPadding];
    
    //tmpX = [self addButtonToView:self.topHeadingBar text:NSLocalizedString(@"edit", @"Edit") tag:kBtnEdit x:tmpX width:tmpWidth padding:tmpPadding];
    //tmpX = [self addButtonToView:self.topHeadingBar text:NSLocalizedString(@"add_action", @"Add Action") tag:kBtnAddAction x:tmpX width:tmpWidth padding:tmpPadding];
    //[self addButtonToView:self.topHeadingBar text:NSLocalizedString(@"add_delay", @"Add Delay") tag:kBtnAddDelay x:tmpX width:tmpWidth padding:tmpPadding];
    
    // Get References
    self.addActionButton = [self getButtonFromView:self.topHeadingBar tag:kBtnAddAction];
    self.addDelayButton = [self getButtonFromView:self.topHeadingBar tag:kBtnAddDelay];
    
    // Add Vertical Bar
    if ([AppStyleHelper isMegaMan]) {
        UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 2.0f, self.topHeadingBar.frame.size.height)];
        [bar setBackgroundColor:[UIColor whiteColor]];
        [bar alignToLeftOfView:self.addActionButton padding:0.0f];
        [self.topHeadingBar addSubview:bar];
        
        UIImageView *imageView = [ViewBuilderHelper getImageViewWithFilename:@"add-grey-banner.png" atX:0.0f atY:0.0f];
        [imageView setFrameHeight:bar.frame.size.height];
        [self.topHeadingBar insertSubview:imageView atIndex:0];
    }
    
    // Add Containing View
//    UIView *containingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.heightOffset, self.topHeadingBar.frame.size.width, 0.0f)];
//    [containingView setBackgroundColor:self.topHeadingBar.backgroundColor];
    
    // Add a Textfield for when Editing
    self.eventTitleTextField = [ViewBuilderHelper getTextFieldWithText:self.event.name
                                                           placeholder:NSLocalizedString(@"enter_event_name", @"Enter event name")
                                                          keyboardType:UIKeyboardTypeDefault
                                                                  font:[UIFont fontWithName:@"Helvetica" size:13.0f]
                                                                   atX:tmpPadding
                                                                   atY:self.heightOffset
                                                                 width:self.topHeadingBar.frame.size.width - (tmpPadding * 2)
                                                                height:30.0f
                                                                   tag:0
                                                                target:self];
    [self.eventTitleTextField setBackgroundColor:[UIColor whiteColor]];
    [self.topHeadingBar addSubview:self.eventTitleTextField];

//    [containingView setFrameHeight:self.eventTitleTextField.frame.size.height];
//    [self.eventTitleTextField setFrameY:(containingView.frame.size.height - self.eventTitleTextField.frame.size.height) / 2];
//    [self.topHeadingBar addSubview:containingView];

    // Position Views
    [self positionViewsAnimate:NO];
}

- (void)positionViewsAnimate:(BOOL)_animate {
    // Reposition y + height of scrollview under buttons
    float tmpHeightOffset = self.heightOffset;
    float _adjustByTextField = self.eventTitleTextField.frame.size.height + 5.0f;
    
    if (self.tableView.isEditing)
        tmpHeightOffset += _adjustByTextField;
    
    [UIView animateWithDuration:_animate ? 0.3 : 0.0f
                          delay:0.0f
                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         if (self.tableView.isEditing)
                             [self.topHeadingBar setFrameHeight:kTopHeadingBarHeight + _adjustByTextField];
                         else
                             [self.topHeadingBar setFrameHeight:kTopHeadingBarHeight];
                         
                         [self.scrollView setFrameY:tmpHeightOffset];
                         [self.scrollView setFrameHeight:self.view.frame.size.height - tmpHeightOffset];
                     }
                     completion:^(BOOL completed2) {
                     }];
}

- (void)setupTableView {
    self.tableView = [ViewBuilderHelper getTableViewWithWidth:self.scrollView.frame.size.width
                                                       height:self.scrollView.frame.size.height
                                                        style:UITableViewStylePlain
                                                       target:self];
    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    //[self.tableView setBackgroundColor:[UIColor redColor]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.scrollView addSubview:self.tableView];
    [self.tableView reloadData];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)setupTimeDelayActionSheet {
    self.timeDelayActionSheet = [[HourMinSecActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                                  buttonTitles:[NSArray arrayWithObjects:
                                                                                NSLocalizedString(@"add_delay2", @"Add Delay"),
                                                                                NSLocalizedString(@"add_random", @"Add Random Delay"),
                                                                                nil]
                                                                   cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                                    dataSource:self
                                                                      delegate:self
                                                                showPickerType:kCustomActionSheetHourMinSecPicker];
    [self.view addSubview:self.timeDelayActionSheet];
}

- (UIButton *)getButtonFromView:(UIView *)_searchView tag:(NSUInteger)_tag {
    return (UIButton *)[_searchView viewWithTag:_tag];
}

- (float)addButtonToView:(UIView *)tmpView
                    text:(NSString *)tmpText
                     tag:(NSUInteger)tmpTag
                       x:(float)tmpX
                   width:(float)tmpWidth
                 padding:(float)tmpPadding {
    
    tmpX += tmpPadding;
    UIButton *tmpButton = nil;
    tmpButton = [ButtonsHelper getButtonWithText:tmpText
                                            type:kButtonGreyBacking
                                             tag:tmpTag
                                           width:tmpWidth
                                          height:38.0f
                                          target:self
                                        selector:@selector(buttonPressed:)];
    
    if ([AppStyleHelper isMegaMan]) {
        [tmpButton setBackgroundImage:nil forState:UIControlStateNormal];
        [tmpButton setBackgroundImage:nil forState:UIControlStateHighlighted];
        [tmpButton setBackgroundImage:nil forState:UIControlStateSelected];
        [tmpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [tmpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    }
    
    [tmpButton setFrameX:tmpX];
    [tmpButton setCenterY:tmpView.center.y];
    [tmpView addSubview:tmpButton];
    tmpX += (tmpButton.frame.size.width + tmpPadding);
    return tmpX;
}

#pragma mark - Private

- (Action *)getLastAction {
    return [self.event.actions lastObject];
}

- (Action *)getActionForIndexPath:(NSIndexPath *)_indexPath {
    return [self.event.actions objectAtIndex:_indexPath.row];
}

#pragma mark UITextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

#pragma mark - LightwaveActionSheet Datasource

- (UIView *)presentedFromView:(LightwaveActionSheet *)tmpCustomActionSheet {
    return self.view;
}

- (UIColor *)actionSheetColourTypes:(LightwaveActionSheet *)tmpCustomActionSheet type:(CustomActionSheetStyleColours)_type {
    if (_type == kCustomActionSheetBackgroundColour)
        return [UIColor colorWithDivisionOfRed:96.0f green:101.0f blue:111.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientStart)
        return [UIColor colorWithDivisionOfRed:166.0f green:169.0f blue:175.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientEnd)
        return [UIColor colorWithDivisionOfRed:123.0f green:128.0f blue:136.0f alpha:1.0f];
    else
        return [UIColor clearColor];
}

- (UIImage *)actionSheetButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreenBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (UIImage *)actionSheetCancelButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreyBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (float)actionSheetButtonHeight:(LightwaveActionSheet *)tmpCustomActionSheet {
    return [AppStyleHelper isMegaMan] ? 40.0f : 35.0f;
}

#pragma mark - LightwaveActionSheet Delegate

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet
                           index:(NSUInteger)tmpIndex
                      pickerView:(UIPickerView *)tmpPickerView {
    
    NSUInteger _numHours = [tmpPickerView selectedRowInComponent:0];
    NSUInteger _numMins = [tmpPickerView selectedRowInComponent:1];
    NSUInteger _numSecs = [tmpPickerView selectedRowInComponent:2];

    if (self.event.actions > 0) {
        switch (tmpIndex) {
             
            case kEnergyActionsVCAddRandomDelay:
            case kEnergyActionsVCAddDelay:
            {
                [self.timeDelayActionSheet toggleActionSheetWithDelay:0.0f];
                
                NSMutableString *_delay = [NSMutableString stringWithFormat:@"%@", tmpIndex == kEnergyActionsVCAddRandomDelay ? @"r" : @""];
                
                // Check if we are updating the last in the list, or a specific action from editing mode
                if (self.selectedAction) {
                    [_delay appendString:[self.event sumDelays:@"00:00:00"
                                                        delay2:[NSString stringWithFormat:@"%d:%d:%d", _numHours, _numMins, _numSecs]]];
                    [self.selectedAction setDelay:_delay];
                } else {
                    Action *_action = [self getLastAction];
                    [_delay appendString:[self.event sumDelays:[_action getDelay]
                                                        delay2:[NSString stringWithFormat:@"%d:%d:%d", _numHours, _numMins, _numSecs]]];
                    [_action setDelay:_delay];
                }
                
                [self.tableView reloadData];
            }
                break;
                
            case kEnergyActionsVCCancel:
            {
                [self.timeDelayActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
    } else { 
        [self showAlert:NSLocalizedString(@"error_action_delay", @"Validation")];
    }
}

- (void)didHideActionSheet:(LightwaveActionSheet *)tmpCustomActionSheet {
    self.selectedAction = nil;
}

#pragma mark - EventActionCell delegate

- (void)setDelayPressed:(EventActionCell *)_eventActionCell
              indexPath:(NSIndexPath *)_indexPath {
    
    self.selectedAction = [self getActionForIndexPath:_indexPath];
    [self.timeDelayActionSheet.timePickerView selectRow:[self.selectedAction getDelayHoursPortion] inComponent:0 animated:NO];
    [self.timeDelayActionSheet.timePickerView selectRow:[self.selectedAction getDelayMinsPortion] inComponent:1 animated:NO];
    [self.timeDelayActionSheet.timePickerView selectRow:[self.selectedAction getDelaySecsPortion] inComponent:2 animated:NO];
    [self.timeDelayActionSheet toggleActionSheetWithDelay:0.0f];
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tmpTableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    NSUInteger _count = self.event.actions.count;
    [self.addDelayButton setEnabled:_count > 0 ? YES : NO];
    [self.addActionButton setEnabled:_count < MAX_ACTIONS_PER_EVENT ? YES : NO];
    return _count;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    Action *tmpAction = [self.event.actions objectAtIndex:indexPath.row];
    if ([tmpAction isDelayed])
        if ([self isActionForIndexPath:indexPath])
            return [EventActionDelayCell getCellHeight];
        else
            return [EventActionCell getCellHeight];
    
    else
        return [EventActionCell getCellHeight];
}

- (BOOL)isActionForIndexPath:(NSIndexPath *)indexPath {
    Action *tmpAction = [self.event.actions objectAtIndex:indexPath.row];
    
    // Setup Cell
    if (tmpAction.name.length > 0 &&
        tmpAction.zoneName.length > 0) {
        
        return YES;
        
    } else {
        return NO;
    }
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Action *tmpAction = [self.event.actions objectAtIndex:indexPath.row];
    
    // vars
    const char* className = class_getName([self class]);
    NSString *cellIdentifier = nil;
    
    // Setup Cells
    if ([tmpAction isDelayed]) {
        /*****************************************
         * IS DELAYED CELL
         *****************************************/
        
        cellIdentifier = [NSString stringWithFormat:@"EventActionDelayCell-%s", className];
        EventActionDelayCell *cell = (EventActionDelayCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell = [self setupInitialCell:cell xibName:@"EventActionDelayCell"];
        
        NSString *_delay = [tmpAction.delay characterAtIndex:0] == 'r' ?
                            [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"up_to", @"Up to"),
                            [tmpAction.delay substringFromIndex:1]] : tmpAction.delay;
                
        // Setup Cell
        if ([self isActionForIndexPath:indexPath]) {
        
            [cell setupCellWithTitle:tmpAction.name
                            subtitle:tmpAction.zoneName
                          detailText:tmpAction.shortString
                              device:tmpAction.device
                               delay:_delay
                           indexPath:indexPath
                            delegate:self];
            
        } else {
            [cell setupCellWithDelay:_delay];
        }
    
        return cell;
        
    } else {
        /*****************************************
         * NO DELAY CELL
         *****************************************/
        
        cellIdentifier = [NSString stringWithFormat:@"EventActionCell-%s", className];
        EventActionCell *cell = (EventActionCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell = [self setupInitialCell:cell xibName:@"EventActionCell"];
        
        // Setup Cell
        [cell setupCellWithTitle:tmpAction.name
                        subtitle:tmpAction.zoneName
                      detailText:tmpAction.shortString
                          device:tmpAction.device
                       indexPath:indexPath
                        delegate:self];
        
        return cell;
    }
}

- (id)setupInitialCell:(EventActionCell *)_cell xibName:(NSString *)_xib {
    if (_cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:_xib owner:self options:nil];
        _cell = [topLevelObjects objectAtIndex:0];
        [_cell setFrameWidth:self.tableView.frame.size.width];
        [_cell.contentView setFrameWidth:self.tableView.frame.size.width];
        [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return _cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark -
#pragma mark Table view editing

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.event.actions removeObjectAtIndex:indexPath.row];
        [self.event saveActionsToDB];
        [self.tableView reloadData];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return (indexPath.row != [self.event.actions count]);
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    Action *action = [self.event.actions objectAtIndex:sourceIndexPath.row];
    [self.event removeAction:action];
    [self.event insertAction:action atIndex:destinationIndexPath.row];
    [self.event saveActionsToDB];
    [self.tableView reloadData];
}

#pragma mark - Actions

- (void)dismissView {
    if (self.navigationController != nil)
        [[self navigationController] dismissModalViewControllerAnimated:YES];
}

- (IBAction)backPressed:(id)sender {
    // EventDevicesVC back handler (Megaman fix)
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnDone:
        {
            if (self.event.actions.count <= MAX_ACTIONS_PER_EVENT) {
                
                // Save Actions to DB
                [self.event saveActionsToDB];
                
                // Update WFL
                [self.event createEvent];
                
                [self dismissView];
                
            } else
                [self.view makeToast:[NSString stringWithFormat:NSLocalizedString(@"error_actions_limit", nil), MAX_ACTIONS_PER_EVENT]];
        }
            break;
            
        case kBtnBack:
        {
            [self dismissView];
        }
            break;
            
        case kBtnEdit:
        {
            if ([self.tableView isEditing]) {
                [self.tableView setEditing:NO animated:YES];
                //[sender setTitle:NSLocalizedString(@"edit", @"Edit") forState:UIControlStateNormal];
                [sender setTitle:NSLocalizedString(@"edit_delays", @"Edit") forState:UIControlStateNormal];
                
                if ([self.eventTitleTextField isFirstResponder])
                    [self.eventTitleTextField resignFirstResponder];
                
                if (self.eventTitleTextField.text.length > 0) {
                    if (![self.event.name isEqualToString:self.eventTitleTextField.text]) {
                        self.event.name = self.eventTitleTextField.text;
                        self.navigationItem.title = self.event.name;
                        
                        Home *home = [Home shared];
                        [home saveData:@"Events"];
                    }
                
                } else {
                    [self.view makeToast:NSLocalizedString(@"message16", @"Enter a valid event name.")];
                    self.eventTitleTextField.text = self.event.name;
                }
                
            } else {
                [sender setTitle:NSLocalizedString(@"done", @"Done") forState:UIControlStateNormal];
                [self.tableView setEditing:YES animated:YES];
            }
            
            // Position Views
            [self positionViewsAnimate:YES];
        }
            break;
            
        case kBtnAddAction:
        {
            // Must set title of back button before we push VC
            [self setupTopLeftBackNavButtonForVC:self];
            
            EventDevicesVC *c = [[EventDevicesVC alloc] initWithNibName:kVCXibName bundle:nil event:self.event];
            
            if ([AppStyleHelper isMegaMan]) {
                [self setupTopLeftBackNavButtonForVC:c];
            }
            
            [self.navigationController pushViewController:c animated:YES];
        }
            break;
            
        case kBtnAddDelay:
        {
            [self.timeDelayActionSheet resetPickerValues];
            [self.timeDelayActionSheet toggleActionSheetWithDelay:0.0f];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
