//
//  DateTimeActionSheet.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 06/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "DateTimeActionSheet.h"

@interface DateTimeActionSheet ()
@property (nonatomic, assign) UIDatePickerMode selectedDateMode;
@end

@implementation DateTimeActionSheet

@synthesize selectedDateMode, datePickerView;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
   selectedDateMode:(UIDatePickerMode)tmpDateMode
{
    self.selectedDateMode = tmpDateMode;
    
    return [self initWithPoint:point
                  buttonTitles:tmpButtonTitles
                   cancelTitle:tmpCancelTitle
                    dataSource:tmpDataSource
                      delegate:tmpDelegate
                showPickerType:kCustomActionSheetDateTimePicker];
}

- (void)setupUI {
    [self setBackgroundColor:[self getColourForType:kCustomActionSheetBackgroundColour]];
    
    // Top Highlight
    UIView *_topHighlightView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, 0.0f)];
    [_topHighlightView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_topHighlightView];
    
    if (self.showPickerType == kCustomActionSheetDateTimePicker) {
        // Show Date Picker?
        self.heightOffset = 0.0f;
        ActionSheetDatePicker *_datePickerView = [[ActionSheetDatePicker alloc] initWithTitle:@""
                                                                               datePickerMode:self.selectedDateMode
                                                                                 selectedDate:[NSDate date]
                                                                                       target:self
                                                                                       action:@selector(dateWasSelected:element:)
                                                                                       origin:self];
        self.datePickerView = [_datePickerView configuredPickerView];
        [self.datePickerView setFrameOrigin:CGPointMake(0.0f, self.heightOffset)];
        [self addSubview:self.datePickerView];
        self.heightOffset += self.datePickerView.frame.size.height;
        self.heightOffset += lwActionSheetButtonPadding;
    }
    
    // Set Highlight Height
    [_topHighlightView setFrameHeight:self.heightOffset + 1.0f];
    
    // Apply Highlight
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _topHighlightView.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[self getColourForType:kCustomActionSheetGradientStart] CGColor],
                       (id)[[self getColourForType:kCustomActionSheetGradientEnd] CGColor],
                       nil];
    [_topHighlightView.layer insertSublayer:gradient atIndex:0];
    
    // Create Buttons
    NSUInteger tmpTag = 0;
    for (NSString *tmpTitle in self.buttonTitlesArray) {
        [self addButtonType:kCustomActionSheetButton title:tmpTitle tag:tmpTag];
        tmpTag += 1;
    }
    
    // Create Cancel Button
    self.heightOffset += lwActionSheetButtonCancelPadding;
    [self addButtonType:kCustomActionSheetCancelButton title:self.cancelTitle tag:tmpTag];
    
    self.heightOffset += lwActionSheetBottomPadding;
    [self setFrameHeight:self.heightOffset];
}

#pragma mark - Public

- (void)resetPickerValues {
    if (self.showPickerType == kCustomActionSheetDateTimePicker) {
        UIDatePicker *_datePicker = (UIDatePicker *)self.datePickerView;
        [_datePicker setDate:[NSDate date] animated:YES];
    }
}

#pragma mark - Actions

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    
}

// Override super
- (IBAction)buttonPressed:(id)sender {
    if (self.showPickerType == kCustomActionSheetDateTimePicker) {
        if ([self.delegate respondsToSelector:@selector(actionSheetButtonPressed:index:pickerView:)])
            [self.delegate actionSheetButtonPressed:self index:[sender tag] pickerView:self.datePickerView];
        
    }
}

@end
