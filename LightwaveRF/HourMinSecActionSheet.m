//
//  HourMinSecActionSheet.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 06/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "HourMinSecActionSheet.h"

@interface HourMinSecActionSheet ()

@end

@implementation HourMinSecActionSheet

@synthesize timePickerView;

- (void)dealloc  {
    //DebugLog(@"DEALLOC");
}

- (void)setupUI {
    [self setBackgroundColor:[self getColourForType:kCustomActionSheetBackgroundColour]];
    
    // Top Highlight
    UIView *_topHighlightView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, 0.0f)];
    [_topHighlightView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_topHighlightView];
    
    if (self.showPickerType == kCustomActionSheetHourMinSecPicker) {
        // Show Time Picker?
        self.heightOffset = 0.0f;
        self.timePickerView = [[DistancePickerView alloc] initWithFrame:CGRectMake(0.0f, self.heightOffset, SCREEN_WIDTH(), 180.0f)];
        self.timePickerView.delegate = self;
        self.timePickerView.showsSelectionIndicator = YES;
        [self addSubview:self.timePickerView];
        [self.timePickerView addLabel:NSLocalizedString(@"hours", @"Hours") forComponent:0 forLongestString:NSLocalizedString(@"hours", @"Hours")];
        [self.timePickerView addLabel:NSLocalizedString(@"minutes", @"Mins") forComponent:1 forLongestString:NSLocalizedString(@"minutes", @"Mins")];
        [self.timePickerView addLabel:NSLocalizedString(@"seconds", @"Secs") forComponent:2 forLongestString:NSLocalizedString(@"seconds", @"Secs")];
        self.heightOffset += self.timePickerView.frame.size.height;
        self.heightOffset += lwActionSheetButtonPadding;
    }
    
    // Set Highlight Height
    [_topHighlightView setFrameHeight:self.heightOffset + 1.0f];
    
    // Apply Highlight
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _topHighlightView.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[self getColourForType:kCustomActionSheetGradientStart] CGColor],
                       (id)[[self getColourForType:kCustomActionSheetGradientEnd] CGColor],
                       nil];
    [_topHighlightView.layer insertSublayer:gradient atIndex:0];
    
    // Create Buttons
    NSUInteger tmpTag = 0;
    for (NSString *tmpTitle in self.buttonTitlesArray) {
        [self addButtonType:kCustomActionSheetButton title:tmpTitle tag:tmpTag];
        tmpTag += 1;
    }
    
    // Create Cancel Button
    self.heightOffset += lwActionSheetButtonCancelPadding;
    [self addButtonType:kCustomActionSheetCancelButton title:self.cancelTitle tag:tmpTag];
    
    self.heightOffset += lwActionSheetBottomPadding;
    [self setFrameHeight:self.heightOffset];
}

#pragma mark - Public

- (void)resetPickerValues {
    if (self.showPickerType == kCustomActionSheetHourMinSecPicker) {
        [self.timePickerView selectRow:0 inComponent:0 animated:NO];
        [self.timePickerView selectRow:0 inComponent:1 animated:NO];
        [self.timePickerView selectRow:0 inComponent:2 animated:NO];
    }
}

#pragma mark - UIPickerView Delegate

- (void)pickerView:(UIPickerView *)tmpPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
}

- (NSString *)pickerView:(UIPickerView *)tmpPickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%d", row];
}

/*
 
 iOS 7 fix to use the return view delegate as it offers more functionality in styling the row
 
 */

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                               0,
                                                               pickerView.frame.size.width / ([self numberOfComponentsInPickerView:pickerView] + 1),
                                                               44)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    label.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return label;
}

#pragma mark - UIPickerView DataSource

- (NSInteger)pickerView:(UIPickerView *)tmpPickerView numberOfRowsInComponent:(NSInteger)component {
    switch (component) {
        case 0:
            return 24;
            break;
            
        case 1:
            return 60;
            break;
            
        case 2:
            return 60;
            break;
            
        default:
            return 0;
            break;
    }
}

- (CGFloat)pickerView:(UIPickerView *)tmpPickerView widthForComponent:(NSInteger)component {
    return 300 / [self numberOfComponentsInPickerView:tmpPickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)tmpPickerView {
    return 3;
}

#pragma mark - Actions

- (IBAction)buttonPressed:(id)sender {
    if (self.showPickerType == kCustomActionSheetHourMinSecPicker) {
        if ([self.delegate respondsToSelector:@selector(actionSheetButtonPressed:index:pickerView:)])
            [self.delegate actionSheetButtonPressed:self index:[sender tag] pickerView:self.timePickerView];
        
    }
}

@end
