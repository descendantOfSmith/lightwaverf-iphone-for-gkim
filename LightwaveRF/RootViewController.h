//
//  RootViewController
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewBuilderHelper.h"
#import "MBProgressHUD.h"
#import "UDPService.h"
#import "RemoteSettings.h"
#import "AppStyleHelper.h"

// General NSNotifications
#define kChangeLanguageNotificationKey @"kChangeLanguageNotificationKey"

@interface RootViewController : UIViewController <MBProgressHUDDelegate>  {

}

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, assign) float heightOffset;
@property (nonatomic, assign) BOOL isModal;

- (void)showError:(NSString *)message;
- (void)showMessage:(NSString *)messageID;//messageID is the id of a string in the Localized.strings file
- (void)showAlert:(NSString *)alert;
- (void)hideKeyBoard:(id) sender;
- (void)showSpinnerWithText:(NSString *)tmpText;
- (void)removeSpinner;
- (NSString *)getClassName:(Class)_class;
- (void)scrollToTargetView:(UIView *)tmpView;
- (void)addObserverWithName:(NSString *)_name selector:(SEL)_selector;
- (void)dismissViewAnimated:(BOOL)_animated;

// View 'Toast' Alerts only once
- (void)setViewedAlert:(BOOL)_didView localisedKey:(NSString *)_localisedKey;
- (BOOL)didViewAlertForLocalisedKey:(NSString *)_localisedKey;
- (void)showOneOffAlertForLocalisedKey:(NSString *)_localisedKey duration:(float)_duration;

@end
