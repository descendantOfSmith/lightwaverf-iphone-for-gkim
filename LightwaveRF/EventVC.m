//
//  EventVC.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "EventVC.h"
#import "ActionSheetDatePicker.h"
#import "NSDate+Utilities.h"
#import "EventActionsVC.h"
#import "Toast+UIView.h"
#import "ScheduleEventVC.h"
#import "WFLHelper.h"
#import "ButtonsHelper.h"

@interface EventVC ()
@property (nonatomic, strong) Event *event;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSDate *scheduleDate;
@property (nonatomic, strong) UIButton *wifi_btn;
@end

@implementation EventVC

@synthesize event;
@synthesize tableView;
@synthesize scheduleDate;
@synthesize wifi_btn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil event:(Event *)tmpEvent
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.event = tmpEvent;
        self.scheduleDate = nil;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    LightwaveActionSheet *tmpActionSheet = [[LightwaveActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                                    buttonTitles:[NSArray arrayWithObjects:
                                                                                  NSLocalizedString(@"start_now", @"Start Now"),
                                                                                  NSLocalizedString(@"start_at_time", @"Start at time"),
                                                                                  NSLocalizedString(@"schedule_event", @"Schedule Event"),
                                                                                  nil]
                                                                     cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                                      dataSource:self
                                                                        delegate:self];
    [tmpActionSheet setIsTapToDismissDisabled:YES];
    [self.view addSubview:tmpActionSheet];
    [tmpActionSheet toggleActionSheetWithDelay:0.5f];

    // Help popup
    [self showOneOffAlertForLocalisedKey:@"help4" duration:5.0f];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
    [self checkWifiState];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Creation

- (void)setupUI {
    [self setupNavBar];
    [self setupWiFiButton];
    [self setupTableView];
}

- (void)setupNavBar {
    if (self.navigationController != nil){
        self.navigationItem.title = NSLocalizedString(@"event_info", @"Event Info");
        
        [self setupLeftNavBarButton];
    }
}

- (void)setupLeftNavBarButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *backBarButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"events", @"Events")
                                                              type:kButtonArrowLeft
                                                               tag:0
                                                             width:60
                                                            height:18
                                                            target:self
                                                          selector:@selector(buttonPressed:)];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBarButton];

    } else {
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"events", @"Events")
                                                                          style:UIBarButtonItemStyleBordered
                                                                         target:self
                                                                         action:@selector(buttonPressed:)];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;
    }
}

- (void)setupTopLeftBackNavButtonForVC:(UIViewController *)vc {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"cancel", @"Cancel")
                                                       type:kButtonArrowLeft
                                                        tag:0
                                                      width:55
                                                     height:18
                                                     target:self
                                                   selector:@selector(backPressed:)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        vc.navigationItem.leftBarButtonItem = backButton;
        vc.navigationItem.hidesBackButton = YES;
        
    } else {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                       initWithTitle:NSLocalizedString(@"cancel", @"Cancel")
                                       style: UIBarButtonItemStyleBordered
                                       target: nil
                                       action: nil];
        [vc.navigationItem setBackBarButtonItem:backButton];
    }
}

- (void)setupWiFiButton {
    UIImageView *imageView = nil;
    if ([AppStyleHelper isMegaMan]) {
        imageView = [ViewBuilderHelper getImageViewWithFilename:@"add-grey-banner.png" atX:0.0f atY:self.heightOffset];
        [self.scrollView insertSubview:imageView atIndex:0];
    }

    self.wifi_btn = [ViewBuilderHelper getButtonWithLoFileName:@"icon_wifi_off.png"
                                                    hiFilename:@"icon_wifi_off.png"
                                                           atX:[AppStyleHelper isMegaMan] ? 5.0f : 0.0f
                                                           atY:self.heightOffset
                                                           tag:0
                                                        target:nil
                                                        action:nil];
    [self.wifi_btn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.wifi_btn setImage:[UIImage imageNamed:@"icon_wifi_on.png"] forState:UIControlStateSelected];
    [self.wifi_btn setFrameSize:CGSizeMake(23.0f, 26.0f)];
    [self.wifi_btn setUserInteractionEnabled:NO];
    
    if ([AppStyleHelper isMegaMan])
        [self.wifi_btn setCenterY:imageView.center.y];
        
    [self.scrollView addSubview:self.wifi_btn];
    self.heightOffset += self.wifi_btn.frame.size.height;
}

- (void)setupTableView {
    float padding = 5.0f;
    self.tableView = [ViewBuilderHelper getTableViewWithWidth:self.scrollView.frame.size.width - (padding * 2)
                                                       height:0.0f
                                                        style:UITableViewStyleGrouped
                                                       target:self];
    
    // iOS7
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self.tableView setFrameX:padding];
    [self.tableView setFrameY:self.heightOffset];
    [self.tableView setScrollEnabled:NO];
    [self.scrollView addSubview:self.tableView];
    self.heightOffset += self.tableView.frame.size.height;
    [self reloadData];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)checkWifiState {
    ServiceTypeTags _currentType = [[UDPService shared] getServiceType];
    
    if ([[WFLHelper shared] isCheckingState]) {
        if (_currentType == kServiceUDP) {
            [self.wifi_btn setSelected:YES];
        }
        
        else if  (_currentType == kServiceWeb) {
            [self.wifi_btn setSelected:NO];
        }
    } else {
        [self.wifi_btn setSelected:NO];
    }
}

#pragma mark - Private

- (void)updateTableHeight {
    // Get the exact height required for the tableview to fill the scrollview.
    self.heightOffset -= self.tableView.frame.size.height;
    [self.tableView layoutIfNeeded];
    [self.tableView setFrameHeight:[self.tableView contentSize].height];
    self.heightOffset += self.tableView.frame.size.height;
}

- (void)reloadData {
    [self.tableView reloadData];
    [self updateTableHeight];
}

#pragma mark - CustomActionSheet Datasource

- (UIView *)presentedFromView:(LightwaveActionSheet *)tmpCustomActionSheet {
    return self.view;
}

- (UIColor *)actionSheetColourTypes:(LightwaveActionSheet *)tmpCustomActionSheet type:(CustomActionSheetStyleColours)_type {
    if (_type == kCustomActionSheetBackgroundColour)
        return [UIColor colorWithDivisionOfRed:96.0f green:101.0f blue:111.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientStart)
        return [UIColor colorWithDivisionOfRed:166.0f green:169.0f blue:175.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientEnd)
        return [UIColor colorWithDivisionOfRed:123.0f green:128.0f blue:136.0f alpha:1.0f];
    else
        return [UIColor clearColor];
}

- (UIImage *)actionSheetButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreenBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (UIImage *)actionSheetCancelButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreyBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (float)actionSheetButtonHeight:(LightwaveActionSheet *)tmpCustomActionSheet {
    return [AppStyleHelper isMegaMan] ? 40.0f : 35.0f;
}

#pragma mark - CustomActionSheet Delegate

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet index:(NSUInteger)tmpIndex {
    switch (tmpIndex) {
            
        case kEnergyVCStartNow:
        {
            [self startEventUsingUDP:YES date:nil];
        }
            break;
            
        case kEnergyVCStartAtTime:
        {
            [ActionSheetDatePicker showPickerWithTitle:@""
                                        datePickerMode:UIDatePickerModeDateAndTime
                                          selectedDate:[NSDate date]
                                                target:self
                                                action:@selector(dateWasSelected:element:)
                                                origin:self.view];
        }
            break;
            
        case kEnergyVCScheduleEvent:
        {
            ScheduleEventVC *_vc = [[ScheduleEventVC alloc] initWithNibName:kVCXibName bundle:nil event:self.event];
            [self.navigationController pushViewController:_vc animated:YES];
        }
            break;
            
        case kEnergyVCCancel:
        {
            [self dismissView];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Date/Time Selected

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    if (![NSDate isTodaysDateAfter:selectedDate]) {
        // Start Event at 'selectedDate'
        [self startEventUsingUDP:YES date:selectedDate];
        
    } else {
        // Scheduled Date cannot be before todays date/time
        [self showError:NSLocalizedString(@"event_time_error", @"Please enter a future date/time.")];
    }
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tmpTableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    return 2;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 44.0f;
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    const char* className = class_getName([self class]);
    NSString *cellIdentifier = [NSString stringWithFormat:@"UITableViewCell-%s", className];
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica" size:12.0f]];
        [cell.textLabel setTextColor:[UIColor blackColor]];
        
        [cell.detailTextLabel setFont:[UIFont fontWithName:@"Helvetica" size:12.0f]];
        [cell.detailTextLabel setTextColor:[UIColor lightGrayColor]];
    }
        
    switch (indexPath.row) {
            
        case kEnergyVCNameCell:
        {
            [cell.textLabel setText:NSLocalizedString(@"name", @"Name")];
            [cell.detailTextLabel setText:self.event.name];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
            break;
            
        case kEnergyVCEditActionsCell:
        {
            [cell.textLabel setText:NSLocalizedString(@"edit_actions", @"Edit Actions")];
            [cell.detailTextLabel setText:@""];

            if ([AppStyleHelper isMegaMan])
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellarrow.png"]];
            else
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
            
        case kEnergyVCNameCell:
        {
            
        }
            break;
            
        case kEnergyVCEditActionsCell:
        {
            // Must set title of back button before we push VC
            [self setupTopLeftBackNavButtonForVC:self];
            
            EventActionsVC *c = [[EventActionsVC alloc] initWithNibName:kVCXibName bundle:nil event:self.event];
            
            if ([AppStyleHelper isMegaMan]) {
                [self setupTopLeftBackNavButtonForVC:c];
            }
            
            [self.navigationController pushViewController:c animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Actions

- (void)dismissView {
    if (self.navigationController != nil)
        [[self navigationController] dismissModalViewControllerAnimated:YES];
}

- (IBAction)backPressed:(id)sender {
    // EventActionsVC back handler (Megaman fix)
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)buttonPressed:(id)sender {

    switch ([sender tag]) {
            
        case kBtnBack:
        {
            [self dismissView];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Stop All Events

- (void)startEventUsingUDP:(BOOL)_useUDP date:(NSDate *)_date {
    [self showSpinnerWithText:@""];
    
    self.scheduleDate = _date;
    NSString *_command = [self.event triggerEvent:_date];
    
    // Check if we should add an exclamation mark...
    if ([_command characterAtIndex:0] != '!')
        _command = [NSString stringWithFormat:@"!%@", _command];
        
    if (_useUDP) {
        
        // UDP
        UDPService *udp = [UDPService shared];
        [udp send:_command tag:0 callingClass:[self getClassName:[self class]]];

    } else {
        
        // Remote Settings
        RemoteSettings *remote = [RemoteSettings shared];
        [remote sendCommandWithCallingClass:[self getClassName:[self class]] tag:0 command:_command];
        
    }
}

- (void)eventScheduledComplete {
    [self dismissView];
}

#pragma mark - Remote Delegate

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kRemoteClassNameKey];
    //NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
        {
            /*************************
             * Error
             *************************/
            NSError *error = [_dict objectForKey:kRemoteErrorKey];
            //NSString *message = NSLocalizedString(@"web_service_error", @"There was a problem connecting to the Lightwave server");
            //NSString *str = [NSString stringWithFormat:@"%@\n%@", message, [error localizedDescription]];
            [self showError:[error localizedDescription]];
        }
        
        else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
        {
            /*************************
             * Success
             *************************/
            [self eventScheduledComplete];
        }
        
        else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
        {
            /*************************
             * Failed
             *************************/
            NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
            [self showError:_failed];
        }
    }
}

#pragma mark - UDP Delegate

- (void)receivedUDPServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kUDPClassNameKey];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        if ([[_notification name] isEqualToString:kUDPErrorNotificationKey])
        {
            /*************************
             * Error
             *************************/
            // Attempt the request using RemoteSettings...
            [self startEventUsingUDP:NO date:self.scheduleDate];
        }
        
        else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
        {
            /*************************
             * Success
             *************************/
            [self eventScheduledComplete];
        }
        
        else if ([[_notification name] isEqualToString:kUDPFailedNotificationKey])
        {
            /*************************
             * Failed
             *************************/
            
            BOOL _attemptRemote = [[_dict objectForKey:kUDPAllowSwitchToRemoteKey] boolValue];
            
            if (_attemptRemote) {
                // Attempt the request using RemoteSettings...
                [self startEventUsingUDP:NO date:self.scheduleDate];
            } else {
                DebugLog(@"Attempt Remote Disabled...");
            }
        }
    }
}

// Overridden
- (void)receivedUDPCheckedStateSuccessNotification:(NSNotification *)_notification {
    [self.wifi_btn setSelected:YES];
}

// Overridden
- (void)receivedUDPCheckedStateFailNotification:(NSNotification *)_notification {
    [self.wifi_btn setSelected:NO];
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
