//
//  ChooseProductOpenCloseCell.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 19/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ChooseProductCell.h"
#import "Constants.h"

@protocol ChooseProductOpenCloseCellDelegate;

@interface ChooseProductOpenCloseCell : ChooseProductCell {
    
}

- (void)setupCellWithTitle:(NSString*)tmpTitle
                    device:(Device *)_device
                  selected:(BOOL)_selected
               isOpenClose:(ConstantsIsOpenClose)_isOpenClose
                 indexPath:(NSIndexPath *)_indexPath
                  delegate:(id<ChooseProductOpenCloseCellDelegate>)_delegate;

@end

@protocol ChooseProductOpenCloseCellDelegate <NSObject>

@optional

- (void)chooseProductOpenClose:(ChooseProductOpenCloseCell *)_chooseProductOpenCloseCell
                     indexPath:(NSIndexPath *)_indexPath
                   isOpenClose:(ConstantsIsOpenClose)_isOpenClose;

@end
