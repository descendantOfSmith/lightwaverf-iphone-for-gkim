//
//  HomeVC.m
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "HomeVC.h"
#import "AppDelegate.h"
#import "Zone.h"
#import "Home.h"
#import "Event.h"
#import "Constants.h"
#import "NSString+RangeOfCharacters.h"
#import "Toast+UIView.h"
#import "HeatingVC.h"
#import "WFLHelper.h"
#import "SwitchLocationVC.h"
#import "ButtonsHelper.h"
#import "EventActionsVC.h"
#import "LWErrorView.h"
#import "ZoneVC.h"

@interface HomeVC ()
@property (nonatomic, strong) TempActionSheet *tempActionSheet;
@property (nonatomic, strong) HomeFooterView *footerView;
@property (strong, nonatomic) UIButton *home_btn;
@property (strong, nonatomic) UIButton *away_btn;
@end

@implementation HomeVC

@synthesize tempActionSheet;
@synthesize zone_tbl;
@synthesize newzone_txt;
@synthesize wifi_btn;
@synthesize footerView;
@synthesize home_btn, away_btn;

#define kAlertZoneIndexPathKey    @"kAlertZoneIndexPathKey"

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        adding = NO;
        newzone_txt = [[UITextField alloc] initWithFrame:CGRectMake(5,5,260,30)];
        newzone_txt.tag = kHomeNewZoneNameTextField;
        newzone_txt.placeholder = NSLocalizedString(@"add_zone", @"add a new zone"); 
        newzone_txt.borderStyle = UITextBorderStyleRoundedRect;
        [newzone_txt addTarget:self action:@selector(addZoneDonePressed:) forControlEvents:UIControlEventEditingDidEndOnExit];
        newzone_txt.returnKeyType = UIReturnKeyDone;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    [zone_tbl setBackgroundColor:[AppStyleHelper getViewBackgroundColor]];

    [self setupNavTitle];
    
    [self setupLeftEditButton];
    [self setupRightAddZoneButton];
    
    [zone_tbl setDataSource:self];
    [zone_tbl setDelegate:self];
    
    [self setupUI];
    
    if ([self.zone_tbl respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.zone_tbl setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)setupNavTitle {
    if ([[UserPrefsHelper shared] getUserName].length > 0)
        self.navigationItem.title = [[UserPrefsHelper shared] getUserName];
    else
        self.navigationItem.title = NSLocalizedString(@"your_home", @"Your Home");
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self checkWifiState];
    [self setupNavTitle];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [zone_tbl reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UI Creation

- (void)setupUI {
    [self setupTopButtons];
    [self setupTempActionSheet];
}

- (void)setupTopButtons {
    if ([AppStyleHelper isMegaMan]) {
        UIImageView *imageView = [ViewBuilderHelper getImageViewWithFilename:@"add-grey-banner.png" atX:0.0f atY:0.0f];
        [imageView setFrameHeight:self.zone_tbl.frame.origin.y];
        [self.view insertSubview:imageView atIndex:0];
        
        self.home_btn = [ButtonsHelper getButtonWithText:NSLocalizedString(@"home", @"Home")
                                                    type:kButtonGreyBacking
                                                     tag:kBtnHome
                                                   width:110
                                                  height:27.0f
                                                  target:self
                                                selector:@selector(homePressed:)];
        [self.home_btn setBackgroundImage:nil forState:UIControlStateNormal];
        [self.home_btn setBackgroundImage:nil forState:UIControlStateHighlighted];
        [self.home_btn setBackgroundImage:nil forState:UIControlStateSelected];
        [self.home_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.home_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self.home_btn alignToRightOfView:self.wifi_btn padding:20.0f];
        [self.home_btn setFrameY:8.0f];
        [self.view addSubview:self.home_btn];
        
        self.away_btn = [ButtonsHelper getButtonWithText:NSLocalizedString(@"away", @"Away")
                                                    type:kButtonGreyBacking
                                                     tag:kBtnAway
                                                   width:110
                                                  height:27.0f
                                                  target:self
                                                selector:@selector(awayPressed:)];
        [self.away_btn setBackgroundImage:nil forState:UIControlStateNormal];
        [self.away_btn setBackgroundImage:nil forState:UIControlStateHighlighted];
        [self.away_btn setBackgroundImage:nil forState:UIControlStateSelected];
        [self.away_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.away_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self.away_btn alignToRightOfView:self.home_btn padding:2.0f matchVertical:YES];
        [self.view addSubview:self.away_btn];
        
        UIView *bar = nil;
        
        bar = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 2.0f, imageView.frame.size.height)];
        [bar setBackgroundColor:[UIColor whiteColor]];
        [bar alignToLeftOfView:self.home_btn padding:0.0f];
        [self.view addSubview:bar];
        
        [self.wifi_btn setCenterX:bar.frame.origin.x / 2];
        
        bar = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 2.0f, imageView.frame.size.height)];
        [bar setBackgroundColor:[UIColor whiteColor]];
        [bar alignToRightOfView:self.home_btn padding:0.0f];
        [self.view addSubview:bar];
        
        bar = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 2.0f, imageView.frame.size.height)];
        [bar setBackgroundColor:[UIColor whiteColor]];
        [bar alignToRightOfView:self.away_btn padding:0.0f];
        [self.view addSubview:bar];
    
    } else {
        self.home_btn = [ButtonsHelper getButtonWithText:NSLocalizedString(@"home", @"Home")
                                                    type:kButtonGreyBacking
                                                     tag:kBtnHome
                                                   width:130
                                                  height:27.0f
                                                  target:self
                                                selector:@selector(homePressed:)];
        [self.home_btn setBackgroundImage:[ButtonsHelper getImageForType:kButtonGreenBacking state:kButtonStateHi] forState:UIControlStateSelected];
        [self.home_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self.home_btn setFrameX:27.0f];
        [self.home_btn setFrameY:8.0f];
        [self.view addSubview:self.home_btn];
        
        self.away_btn = [ButtonsHelper getButtonWithText:NSLocalizedString(@"away", @"Away")
                                                    type:kButtonGreyBacking
                                                     tag:kBtnAway
                                                   width:130
                                                  height:27.0f
                                                  target:self
                                                selector:@selector(awayPressed:)];
        [self.away_btn setBackgroundImage:[ButtonsHelper getImageForType:kButtonGreenBacking state:kButtonStateHi] forState:UIControlStateSelected];
        [self.away_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self.away_btn setFrameX:162.0f];
        [self.away_btn setFrameY:8.0f];
        [self.view addSubview:self.away_btn];
    }
}

- (void)setupTempActionSheet {
    self.tempActionSheet = [[TempActionSheet alloc] initWithPoint:CGPointMake(0.0f, SCREEN_HEIGHT())
                                                       buttonTitles:[NSArray arrayWithObjects:
                                                                     NSLocalizedString(@"set_temp", @"Set Temp"),
                                                                     nil]
                                                      cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                       dataSource:self
                                                         delegate:self
                                                showNumComponents:3];
    [self.tempActionSheet setTag:kHomeVCSetTemp];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self.tempActionSheet];
}

#pragma mark - Checks

- (void)checkWifiState {
    ServiceTypeTags _currentType = [[UDPService shared] getServiceType];
    
    if ([[WFLHelper shared] isCheckingState]) {
        if (_currentType == kServiceUDP)
            [self.wifi_btn setSelected:YES];
        
        else if  (_currentType == kServiceWeb)
            [self.wifi_btn setSelected:NO];
    } else
        [self.wifi_btn setSelected:NO];
}

#pragma mark - Private

- (int)getFreeZoneID{
    Home *home = [Home shared];
    int id = -1;
    BOOL used[MAX_ZONES];
    
    for (int i=0; i<MAX_ZONES; i++) used[i] = NO;
    
    for (Zone *zone in  home.zones){
        id = (zone.zoneID-1);
        if (id>=0 && id<MAX_ZONES) used[id]=true;
    }
    
    for(int i=0; i<MAX_ZONES; i++){
        if (!used[i]){
            id = i + 1;
            break;
        }
    }
    
    return id;
}

- (NSString *)isValidZoneName:(NSString *)_text {
    NSMutableString *_validationStr = [NSMutableString string];
    
    if (_text.length <= 0) {
        [_validationStr appendString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"message19", @"Enter a valid zone name.")]];
    
    } else if (_text.length > 16) {
        [_validationStr appendString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"message21", @"16 char limit.")]];
        
    }
    
    // Device naming must be restricted to alphanumeric characters and -, _ and space.
    NSCharacterSet *_charSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_ "];
    _charSet = [_charSet invertedSet];
    NSRange r = [_text rangeOfCharacterFromSet:_charSet];
    if (r.location != NSNotFound) {
        [_validationStr appendString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"message20", @"restricted to alphanumeric characters and -, _ and space.")]];
    }
    
    return _validationStr.length > 0 ? _validationStr : nil;
}

- (NSString *)getValidZoneName:(NSString *)_text {
    // Names of zones in a home MUST be unique.
    // If the name is not unique then append with an integer starting with 1.
    Home *home = [Home shared];
    for (Zone *_zone in home.zones) {
        if ([_zone.name isEqualToString:_text]) {
            
            DebugLog(@"%@ matches %@", _zone.name, _text);
            
            // Name already exists, append with an integer
            // First of all check if there is already a number at the end of the string
            NSInteger _appendInt = 1;
            NSString *_number = [_text substringFromSet:[NSCharacterSet decimalDigitCharacterSet]
                                                options:NSBackwardsSearch|NSAnchoredSearch];
            if (_number != nil) {
                // Found number at end, so add 1
                _appendInt = [_number intValue] + 1;
                
                // Trim the old number off the end
                _text = [_text substringToIndex:[_text length] - [_number length]];
            }
            
            _text = [NSString stringWithFormat:@"%@%d", _text, _appendInt];
            return [self getValidZoneName:_text];
        }
    }
    
    return _text;
}

#pragma mark - CustomActionSheet Datasource

- (UIView *)presentedFromView:(LightwaveActionSheet *)tmpCustomActionSheet {
    return self.view;
}

- (UIColor *)actionSheetColourTypes:(LightwaveActionSheet *)tmpCustomActionSheet type:(CustomActionSheetStyleColours)_type {
    if (_type == kCustomActionSheetBackgroundColour)
        return [UIColor colorWithDivisionOfRed:96.0f green:101.0f blue:111.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientStart)
        return [UIColor colorWithDivisionOfRed:166.0f green:169.0f blue:175.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientEnd)
        return [UIColor colorWithDivisionOfRed:123.0f green:128.0f blue:136.0f alpha:1.0f];
    else
        return [UIColor clearColor];
}

- (UIImage *)actionSheetButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreenBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (UIImage *)actionSheetCancelButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreyBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (float)actionSheetButtonHeight:(LightwaveActionSheet *)tmpCustomActionSheet {
    return [AppStyleHelper isMegaMan] ? 40.0f : 35.0f;
}

#pragma mark - CustomActionSheet Delegate

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet index:(NSUInteger)tmpIndex pickerView:(UIPickerView *)tmpPickerView {
    if ([tmpCustomActionSheet tag] == kHomeVCSetTemp) {
        /*****************************************
         * SET TEMP
         *****************************************/
        switch (tmpIndex) {
                
            case kHomeVCSetTempSetTemp:
            {
                NSString *_selectedTemp = [[tmpPickerView delegate] pickerView:tmpPickerView
                                                                   titleForRow:[tmpPickerView selectedRowInComponent:0]
                                                                  forComponent:0];
                
                NSString *_selectedHours = [[tmpPickerView delegate] pickerView:tmpPickerView
                                                                    titleForRow:[tmpPickerView selectedRowInComponent:1]
                                                                   forComponent:1];
                
                NSString *_selectedMins = [[tmpPickerView delegate] pickerView:tmpPickerView
                                                                   titleForRow:[tmpPickerView selectedRowInComponent:2]
                                                                  forComponent:2];
                
                DebugLog(@"Temp: %@", _selectedTemp);
                DebugLog(@"Hours: %@", _selectedHours);
                DebugLog(@"Mins: %@", _selectedMins);
                
                [self.tempActionSheet toggleActionSheetWithDelay:0.0f];
                [zone_tbl reloadData];
            }
                break;
                
            case kHomeVCSetTempCancel:
            {
                [self.tempActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (!adding){

        Home *home = [Home shared];
//        if (home != nil){
//            if ([home.zones count]>2){

                float _generalCellHeight = 44.0f;
                float _heatingCellHeight = 96.0f;
                float _offsetHeight = 0.0f;
                
                CGPoint pnt = [zone_tbl convertPoint:textField.bounds.origin fromView:textField];
                NSIndexPath *path = [zone_tbl indexPathForRowAtPoint:pnt];
                NSInteger _numRows = path.row + 1;
                
                if ([home hasHeating]) {
                    _numRows -= 1;
                    _offsetHeight += _heatingCellHeight;
                }
                
                _offsetHeight += (_numRows * _generalCellHeight);
                
                float _padding = (_generalCellHeight * 2);
                if (_offsetHeight > _padding)
                    _offsetHeight -= _padding;
                
                zone_tbl.contentOffset = CGPointMake(0.0f, _offsetHeight);
//            }
//        }

    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (!adding){
        [self editingModeZoneNameDonePressed:textField];
    }
}

#pragma mark - HomeHeatingCell Delegate

- (void)heatingPressed:(HomeHeatingCell *)_heatingCellVC
                option:(HeatingCellVCOptions)_option
             indexPath:(NSIndexPath *)_indexPath {
    
    switch (_option) {
            
        case kHeatingCellVCBoost:
        {
#warning - TODO Heating
        }
            break;
            
        case kHeatingCellVCStandby:
        {
#warning - TODO Heating
        }
            break;
            
        case kHeatingCellVCProgram:
        {
            Home *home = [Home shared];
            Zone *_heatedZone = [home hasHeating];
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[[HeatingVC alloc] initWithNibName:kVCXibName bundle:nil zone:_heatedZone]];
            [self presentViewController:nc animated:YES completion:^(void){
                
            }];
        }
            break;
            
        case kHeatingCellVCSetTemp:
        {
            [self.tempActionSheet toggleActionSheetWithDelay:0.0f];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - HomeFooterView Delegate

- (void)switchLocationPressed:(HomeFooterView *)_homeFooterView {
    SwitchLocationVC *c = [[SwitchLocationVC alloc] initWithNibName:kVCXibName bundle:nil];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:c];
    [self presentViewController:nc animated:YES completion:^(void){
        c.isModal = YES;
    }];
}

#pragma mark - UITableView Footer

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    if ([[UserPrefsHelper shared] getShowHomeFooter] && adding == NO)
        return [HomeFooterView getViewHeight];
    else
        return 0.0f;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {

    if ([[UserPrefsHelper shared] getShowHomeFooter] && adding == NO) {
        
        if (!self.footerView)
            self.footerView = [[HomeFooterView alloc] initWithPoint:CGPointZero];
        
        [self.footerView setDelegate:self];
        [self.footerView.selectedButton setSelected:![[UserPrefsHelper shared] getTestWiFi]];

        return self.footerView;
        
    } else {
        return nil;
    }
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSString *)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Home *home = [Home shared];
    Zone *_heatedZone = [home hasHeating];
    if (adding)
        return 44;
    else if (indexPath.row == 0 && _heatedZone)
        return 96;
    else
        return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (adding)
        return 1;
    
    Home *home = [Home shared];
    int count = 0;
    
    if (home!=nil)
        count = [home.zones count];
    
    if ([home hasHeating])
        count += 1;
        
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    const char* className = class_getName([self class]);
    
    if (adding) {
        
        NSString *cellIdentifier = [NSString stringWithFormat:@"UITableViewCell-%s-Adding", className];
        UITableViewCell *cell = [zone_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell.contentView addSubview:newzone_txt];
        }
        
        return cell;

    } else {
        
        Home *home = [Home shared];
        Zone *zone = nil;
        Zone *_heatedZone = [home hasHeating];
        
        if (indexPath.row == 0 && _heatedZone) {
            
            NSString *cellIdentifier = cellIdentifier = [NSString stringWithFormat:@"HomeHeatingCell-%s", className];
            HomeHeatingCell *cell = (HomeHeatingCell*)[zone_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
            cell = [self setupInitialCell:cell xibName:@"HomeHeatingCell"];
            
            // Setup Cell
            [cell setupCellWithZone:_heatedZone
                          indexPath:indexPath
                           delegate:self];
            
            return cell;

        } else {
            
            if ([home hasHeating])
                zone = [home.zones objectAtIndex:indexPath.row - 1];
            else
                zone = [home.zones objectAtIndex:indexPath.row];
            
            if (zone_tbl.editing) {
                
                NSString *cellIdentifier = [NSString stringWithFormat:@"UITableViewCell-%s-Editing", className];
                UITableViewCell *cell = [zone_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
                
                if (cell == nil) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
                    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                    
                    UITextField *zone_txt = [[UITextField alloc] initWithFrame:CGRectMake(5,0,210,30)];
                    [zone_txt setTag:kHomeEditZoneNameTextField];
                    zone_txt.borderStyle = UITextBorderStyleRoundedRect;
                    zone_txt.delegate = self;
                    [zone_txt addTarget:self action:@selector(donePressed:) forControlEvents:UIControlEventEditingDidEndOnExit];
                    zone_txt.returnKeyType = UIReturnKeyDone;
                    [zone_txt setCenterY:cell.contentView.center.y];
                    [cell.contentView addSubview:zone_txt];
                }
                
                UITextField *zone_txt = (UITextField *)[cell.contentView viewWithTag:kHomeEditZoneNameTextField];
                zone_txt.text = zone.name;
                
                return cell;
                
            } else {
                
                NSString *cellIdentifier = [NSString stringWithFormat:@"UITableViewCell-%s-Not-Editing", className];
                UITableViewCell *cell = [zone_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
                
                if (cell == nil) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
                    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                    cell.textLabel.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
                    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellarrow.png"]];
                }
                
                cell.textLabel.text = zone.name;
                
                return cell;
                
            }
            
        }
    }

    return nil;
}

- (id)setupInitialCell:(HomeHeatingCell *)_cell xibName:(NSString *)_xib {
    if (_cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:_xib owner:self options:nil];
        _cell = [topLevelObjects objectAtIndex:0];
        [_cell setFrameWidth:zone_tbl.frame.size.width];
        [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [_cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return _cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Home *home = [Home shared];
    
    //DebugLog(@"didSelectRowAtIndexPath: %d, %d", indexPath.section, indexPath.row);
    
    if (adding) {
        
    } else if (!adding && indexPath.row == 0 && [home hasHeating]) {

    } else {
        
        if (home != nil){
            
            Zone *zone = nil;
            if ([home hasHeating])
                zone = [home.zones objectAtIndex:indexPath.row - 1];
            else
                zone = [home.zones objectAtIndex:indexPath.row];
            
            ZoneVC *c = [[ZoneVC alloc] initWithZone:zone];
            [self.navigationController pushViewController:c animated:YES];
        }
    }
}

#pragma mark - Editing

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!zone_tbl.editing || !indexPath)
        return UITableViewCellEditingStyleNone;
    
    Home *home = [Home shared];
    if (!adding && indexPath.row == 0 && [home hasHeating])
        return UITableViewCellEditingStyleNone;
    
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView
shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    Home *home = [Home shared];
    if (!adding && indexPath.row == 0 && [home hasHeating]) {
        return NO;
    } else
        return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    Home *home = [Home shared];
    if (editingStyle == UITableViewCellEditingStyleDelete){

#if TARGET_IPHONE_SIMULATOR
        [self deleteZoneAtIndexPath:indexPath];
#else
        Zone *zone = [self getZoneForIndexPath:indexPath];
        
        if (zone.devices.count == 1) { //   1 = All Off
            [self deleteZoneAtIndexPath:indexPath];
            
        } else {
            [self showAlert:NSLocalizedString(@"delete_zone_info", @"Deleting zone...")];
        }
#endif
        
    } else if(editingStyle == UITableViewCellEditingStyleInsert){
        NSString *str = [NSString  stringWithString:newzone_txt.text];
        Zone *zone = [[Zone alloc] initWithName:str];
        [home addZone:zone];
        [zone_tbl reloadData];
    }
}

- (Zone *)getZoneForIndexPath:(NSIndexPath *)_indexPath {
    Home *home = [Home shared];
    Zone *zone = nil;
    if ([home hasHeating])
        zone = [home.zones objectAtIndex:_indexPath.row - 1];
    else
        zone = [home.zones objectAtIndex:_indexPath.row];
    
    return zone;
}

- (void)deleteZoneAtIndexPath:(NSIndexPath *)_indexPath {
    Home *home = [Home shared];
    Zone *zone = [self getZoneForIndexPath:_indexPath];
    [home removeZone:zone];
    [home saveData:@"Zones"];
    [zone_tbl reloadData];
}

#pragma mark - Moving

- (NSIndexPath *)tableView:(UITableView *)tableView
targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath
       toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    
    Home *home = [Home shared];
    if (proposedDestinationIndexPath.row == 0 && [home hasHeating]) {
        return sourceIndexPath;
    } else
        return proposedDestinationIndexPath;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    Home *home = [Home shared];
    if (!adding && indexPath.row == 0 && [home hasHeating])
        return NO;
    else
        return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    Home *home = [Home shared];
    
    Zone *zone = nil;
    if ([home hasHeating])
        zone = [home.zones objectAtIndex:sourceIndexPath.row - 1];
    else
        zone = [home.zones objectAtIndex:sourceIndexPath.row];
    
    [home removeZone:zone];
    [home insertZone:zone atIndex:destinationIndexPath.row];
    [home saveData:@"Zones"];
}

#pragma mark - Nav Buttons

- (void)setupLeftEditButton {
    if ([AppStyleHelper isMegaMan]) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[ButtonsHelper getButtonWithDynamicWidthText:NSLocalizedString(@"edit", nil)
                                                                                                                                    type:kButtonArrowLeft
                                                                                                                                     tag:0
                                                                                                                            widthPadding:20
                                                                                                                                  height:18
                                                                                                                                  target:self
                                                                                                                                selector:@selector(edit)]];
    } else {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit", nil)
                                                                                 style:UIBarButtonItemStyleBordered
                                                                                target:self
                                                                                action:@selector(edit)];
    }
}

- (void)setupRightAddZoneButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithDynamicWidthText:@"+"
                                                                   type:kButtonNone
                                                                    tag:0
                                                           widthPadding:10
                                                                 height:30
                                                                 target:self
                                                               selector:@selector(addZone)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:24.0f]];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    } else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addZone)];
    }
}

- (void)setupLeftCancelButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"cancel", nil)
                                                       type:kButtonArrowLeft
                                                        tag:0
                                                      width:55
                                                     height:18
                                                     target:self
                                                   selector:@selector(cancel)];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    } else {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    }
}

- (void)setupRightDoneButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"done", nil)
                                                       type:kButtonArrowRight
                                                        tag:0
                                                      width:45
                                                     height:18
                                                     target:self
                                                   selector:@selector(done)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    } else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
    }
}

- (void)setupLeftDoneButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"done", nil)
                                                       type:kButtonArrowLeft
                                                        tag:0
                                                      width:50
                                                     height:18
                                                     target:self
                                                   selector:@selector(done)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    } else {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
    }
}

#pragma mark - Actions

- (void)addZone{
    Home *_home = [Home shared];
    if ([_home.zones count] < MAX_ZONES) {
        adding = YES;
        [zone_tbl setEditing:NO animated:YES];
        
        [self setupLeftCancelButton];
        [self setupRightDoneButton];
        
        [newzone_txt setText:@""];
        [zone_tbl reloadData];
        
    } else {
        [self showAlert:[NSString stringWithFormat:NSLocalizedString(@"error_zones_limit", nil), MAX_ZONES]];
    }
}

- (void)edit{
    [zone_tbl setEditing:YES animated:YES];
    [zone_tbl reloadData];
    
    [self setupLeftDoneButton];
}

- (void)done{
    if (adding){
        [self addZoneDonePressed:nil];
    }else{
        Home *home = [Home shared];
        [home saveData:@"Zones"];
        [zone_tbl setEditing:NO animated:YES];
        [zone_tbl reloadData];
        
        [self setupLeftEditButton];
        [self setupRightAddZoneButton];
    }
}

- (void)cancel{
    adding = NO;
    [zone_tbl reloadData];
    [newzone_txt resignFirstResponder];
    
    [self setupLeftEditButton];
    [self setupRightAddZoneButton];
}

// Adding new zone, done button pressed
- (void)addZoneDonePressed:(id)sender{
    NSString *_validationStr = [self isValidZoneName:newzone_txt.text];
    if (_validationStr) {
        [self showError:_validationStr];
        
    } else {
        adding = NO;
        Home *home = [Home shared];
        [home.zones addObject:[[Zone alloc] initWithName:newzone_txt.text]];
        Zone *zone = [home.zones objectAtIndex:[home.zones count]-1];
        zone.zoneID = [self getFreeZoneID];
        [home saveData:@"Zones"];
        [zone_tbl reloadData];
        [newzone_txt resignFirstResponder];
        
        [self setupLeftEditButton];
        [self setupRightAddZoneButton];
    }
}

- (void)donePressed:(id)sender{
    UITextField *txt = (UITextField*)sender;
    [txt resignFirstResponder];
}

// In editing mode for the list of zones, all the textfields 'Done' Key hit this function
- (void)editingModeZoneNameDonePressed:(id)sender{
    zone_tbl.contentOffset = CGPointZero;
    
    UITextField *txt = (UITextField*)sender;
    [txt resignFirstResponder];
    
    NSString *_validationStr = [self isValidZoneName:txt.text];
    if (_validationStr) {
        [self showError:_validationStr];
        
    } else {
        
        CGPoint pnt = [zone_tbl convertPoint:txt.bounds.origin fromView:txt];
        NSIndexPath* path = [zone_tbl indexPathForRowAtPoint:pnt];
        Home *home = [Home shared];
        
        Zone *zone = nil;
        if ([home hasHeating])
            zone = [home.zones objectAtIndex:path.row - 1];
        else
            zone = [home.zones objectAtIndex:path.row];
        
        // Only change if the name is different
        // if its the same, ignore it, as otherwise it will add a number to the end.
        if (![zone.name isEqualToString:txt.text]) {
            NSString *_validZoneName = [self getValidZoneName:txt.text];
            DebugLog(@"Name: %@", _validZoneName);
            zone.name = _validZoneName;
            [txt setText:_validZoneName];
        }
    }
}

- (IBAction)homePressed:(id)sender {
    [self.home_btn setSelected:YES];
    [self.away_btn setSelected:NO];
    
    Home *home = [Home shared];
    Event *_event = [home getEventByID:kHome];
    
    if (_event.actions.count > 0) {
        NSString *_command = [_event triggerEvent:nil];
        [self triggerEventUsingUDP:YES command:_command];
    } else {

        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[[EventActionsVC alloc] initWithNibName:kVCXibName bundle:nil event:_event]];
        [self presentViewController:nc animated:YES completion:^(void){
            
        }];
        
    }
}

- (IBAction)awayPressed:(id)sender {
    [self.home_btn setSelected:NO];
    [self.away_btn setSelected:YES];
    
    Home *home = [Home shared];
    Event *_event = [home getEventByID:kAway];
    
    if (_event.actions.count > 0) {
        NSString *_command = [_event triggerEvent:nil];
        [self triggerEventUsingUDP:YES command:_command];
    } else {
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[[EventActionsVC alloc] initWithNibName:kVCXibName bundle:nil event:_event]];
        [self presentViewController:nc animated:YES completion:^(void){
            
        }];
        
    } 
}

#pragma mark - Trigger Events

- (void)triggerEventUsingUDP:(BOOL)_useUDP command:(NSString *)_command {
    [self showSpinnerWithText:@""];
    
    if (_useUDP) {
        
        // UDP
        UDPService *udp = [UDPService shared];
        [udp send:[NSString stringWithFormat:@"%@", _command]
              tag:kRequestTriggerEvent
     callingClass:[self getClassName:[self class]]];
        
    } else {
        
        // Remote Settings
        RemoteSettings *remote = [RemoteSettings shared];
        [remote sendCommandWithCallingClass:[self getClassName:[self class]]
                                        tag:kRequestTriggerEvent
                                    command:[NSString stringWithFormat:@"%@", _command]];
        
    }
}

- (void)eventTriggeredComplete {
    NSString *_str = [NSString stringWithFormat:@"%@ %@",
                      self.home_btn.selected ? NSLocalizedString(@"home", @"Home") : NSLocalizedString(@"away", @"Away"),
                      [NSLocalizedString(@"event_triggered", @"Event triggered") lowercaseString]];
    [self.view makeToast:_str];
}

#pragma mark - Remote Delegate

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kRemoteClassNameKey];
    NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        switch (_tag) {
                
            case kRequestTriggerEvent:
            {
                [self receivedRemoteTriggerEventHandler:_notification];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - Remote Handlers

- (void)receivedRemoteTriggerEventHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
    {
        /*************************
         * Error
         *************************/
        NSError *error = [_dict objectForKey:kRemoteErrorKey];
        //NSString *message = NSLocalizedString(@"web_service_error", @"There was a problem connecting to the Lightwave server");
        //NSString *str = [NSString stringWithFormat:@"%@\n%@", message, [error localizedDescription]];
        [self showError:[error localizedDescription]];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self eventTriggeredComplete];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Failed
         *************************/
        NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
        [self showError:_failed];
    }
}

#pragma mark - UDP Delegate

- (void)receivedUDPServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kUDPClassNameKey];
    NSUInteger _tag = [[_dict objectForKey:kUDPTagKey] intValue];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        switch (_tag) {
                
            case kRequestTriggerEvent:
            {
                [self receivedUDPTriggerEventHandler:_notification];
            }
                break;
                
            default:
                break;
        }
    }
}

// Overridden
- (void)receivedUDPCheckedStateSuccessNotification:(NSNotification *)_notification {
    [self.wifi_btn setSelected:YES];
}

// Overridden
- (void)receivedUDPCheckedStateFailNotification:(NSNotification *)_notification {
    [self.wifi_btn setSelected:NO];
}

#pragma mark - UDP Handlers

- (void)receivedUDPTriggerEventHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kUDPCommandKey];
    
    if ([[_notification name] isEqualToString:kUDPErrorNotificationKey] ||
        [[_notification name] isEqualToString:kUDPFailedNotificationKey])
    {
        /*************************
         * Error || Failed
         *************************/
        
        BOOL _attemptRemote = [[_dict objectForKey:kUDPAllowSwitchToRemoteKey] boolValue];
        
        if (_attemptRemote) {
            // Attempt the request using RemoteSettings...
            [self triggerEventUsingUDP:NO command:_command];
        } else {
            DebugLog(@"Attempt Remote Disabled...");
        }
    }
    
    else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self eventTriggeredComplete];
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
