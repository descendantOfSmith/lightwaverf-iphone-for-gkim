//
//  LWErrorView.h
//  LightwaveRF
//
//  Created by Nik Lever on 04/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWErrorView : UIAlertView

@end
