//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import "AccountCell.h"
#import "ButtonsHelper.h"

@interface AccountCell ()
@property (nonatomic, strong) UIView *containingView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *detailLabel;
@end

@implementation AccountCell

@synthesize containingView, iconImageView, titleLabel, detailLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
    }
    return self;
}

- (void)dealloc {
    
}

+ (float)getCellHeight {
    return 44.0f;
}

- (void)setupContainingView {
    /*************************************
     *  Containing View
     *************************************/
    if (!self.containingView) {
        self.containingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                                       0.0f,
                                                                       self.contentView.frame.size.width,
                                                                       [AccountCell getCellHeight])];
        [self.containingView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.containingView];
    }
}

- (void)setupCellWithTitle:(NSString*)tmpTitle
                detailText:(NSString *)tmpDetailText
                 iconImage:(UIImage *)_iconImage {
    
    [self setupContainingView];

    /*************************************
     *  Icon
     *************************************/
    if (!self.iconImageView) {
        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,
                                                                           0.0f,
                                                                           36.0f,
                                                                           36.0f)];
        [self.iconImageView setContentMode:UIViewContentModeScaleAspectFit];
        self.iconImageView.image = _iconImage;
        [self.containingView addSubview:self.iconImageView];
    }

    /*************************************
     *  Title Label
     *************************************/
    if (!self.titleLabel) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 18.0f)];
        [self.titleLabel setText:tmpTitle];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:[UIColor blackColor]];
        [self.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
        [self.containingView addSubview:self.titleLabel];
    } else {
        [self.titleLabel setText:tmpTitle];
    }
    
    /*************************************
     *  Detail Label
     *************************************/
    if (!self.detailLabel) {
        self.detailLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.detailLabel setText:tmpDetailText];
        [self.detailLabel setBackgroundColor:[UIColor clearColor]];
        [self.detailLabel setTextColor:[UIColor lightGrayColor]];
        [self.detailLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        [self.detailLabel sizeToFit];
        [self.containingView addSubview:self.detailLabel];
    } else {
        [self.detailLabel setText:tmpDetailText];
        [self.detailLabel sizeToFit];
    }
    
    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    
    // Icon Image
    [self.iconImageView setFrameX:tmpIsEditing ? 45.0f : 5.0f];
    [self.iconImageView setCenterY:self.containingView.center.y];
    
    // Title
    [self.titleLabel alignToRightOfView:self.iconImageView padding:5.0f];
    [self.titleLabel setCenterY:self.containingView.center.y];
    
    // Detail
    [self.detailLabel alignToRightOfView:self.titleLabel padding:2.0f];
    [self.detailLabel setCenterY:self.containingView.center.y];
    [self.detailLabel setAlpha:tmpIsEditing ? 0.0f : 1.0f];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self positionViewsIsEditing:self.isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

@end
