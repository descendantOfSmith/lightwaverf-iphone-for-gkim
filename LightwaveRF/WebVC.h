//
//  WebVC.h
//  LightwaveRF
//
//  Created by Nik Lever on 11/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface WebVC : RootViewController <UIWebViewDelegate> {
    NSString *url;
}
@property (weak, nonatomic) IBOutlet UIWebView *web_wvw;

-(id)initWithURL:(NSString *)url;
-(void) setURL:(NSString *)url;

@end
