//
//  AppStyleHelper.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 22/10/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "AppStyleHelper.h"

@implementation AppStyleHelper

+ (UIColor *)getNavigationBarTintColor {
    if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isCOCO])
        return [UIColor colorWithDivisionOfRed:140.0 green:179.0 blue:80.0 alpha:1.0];
    
    else if ([AppStyleHelper isMegaMan])
        return [UIColor colorWithDivisionOfRed:138.0f green:197.0f blue:40.0f alpha:1.0];
    
    else
        return [UIColor colorWithDivisionOfRed:109 green:45 blue:140 alpha:1.0];
}

+ (UIColor *)getTabBarSelectedImageTintColor {
    if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isCOCO])
        return [UIColor colorWithDivisionOfRed:82.0 green:118.0 blue:75.0 alpha:1.0];
    
    else if ([AppStyleHelper isMegaMan])
        return [UIColor colorWithDivisionOfRed:103.0f green:147.0f blue:31.0f alpha:1.0];
    
    else
        return [UIColor colorWithDivisionOfRed:69 green:33 blue:106 alpha:1.0];
}

+ (UIColor *)getTabBarTitleColor {
    if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isCOCO])
        return [UIColor colorWithDivisionOfRed:82.0 green:118.0 blue:75.0 alpha:0.8];
    
    else
        return [UIColor colorWithDivisionOfRed:255 green:255 blue:255 alpha:1.0];
}

+ (UIColor *)getNavBarTextColor {
    if ([AppStyleHelper isMegaMan])
        return [UIColor colorWithDivisionOfRed:0 green:0 blue:0 alpha:1.0];
    else
        return [UIColor colorWithDivisionOfRed:255 green:255 blue:255 alpha:1.0];
}

+ (UIColor *)getIOS7NavBarButtonsTextColor {
    if ([AppStyleHelper isMegaMan])
        return [UIColor blackColor];
    else
        return [UIColor whiteColor];
}

+ (UIColor *)getIOS7ToolBarButtonsTextColor {
    return [UIColor whiteColor];
}

+ (UIColor *)getViewBackgroundColor {
    if ([AppStyleHelper isMegaMan])
        return [UIColor colorWithDivisionOfRed:193 green:193 blue:193 alpha:1.0];
    else
        return [UIColor whiteColor];
}

+ (UIColor *)getScrollViewBackgroundColor {
    if ([AppStyleHelper isMegaMan])
        return [UIColor colorWithDivisionOfRed:193 green:193 blue:193 alpha:1.0];
    else
        return [UIColor whiteColor];
}

+ (UIColor *)getNavBarBackgroundColor {
    if ([AppStyleHelper isMegaMan])
        return [UIColor whiteColor];
    else
        return [UIColor clearColor];
}

+ (BOOL)isLightWaveRF {
#if defined(IS_LIGHTWAVERF)
    return YES;
#else
    return NO;
#endif
}

+ (BOOL)isNexa {
#if defined(IS_NEXA)
    return YES;
#else
    return NO;
#endif
}

+ (BOOL)isKlikaan {
#if defined(IS_KLIKAAN)
    return YES;
#else
    return NO;
#endif
}

+ (BOOL)isFSLDemo {
#if defined(IS_FSLDEMO)
    return YES;
#else
    return NO;
#endif
}

+ (BOOL)isCOCO {
#if defined(IS_COCO)
    return YES;
#else
    return NO;
#endif
}

+ (BOOL)isMegaMan {
#if defined(IS_MEGAMAN)
    return YES;
#else
    return NO;
#endif
}

@end
