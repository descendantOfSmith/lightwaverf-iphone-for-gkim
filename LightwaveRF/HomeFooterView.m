//
//  HomeFooterView.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 24/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "HomeFooterView.h"
#import "ButtonsHelper.h"
#import "UDPService.h"
#import "ViewBuilderHelper.h"

@interface HomeFooterView ()
@property (nonatomic, strong) UIButton *switchLocationButton;
@end

@implementation HomeFooterView

@synthesize delegate;
@synthesize selectedButton;
@synthesize switchLocationButton;

- (id)initWithPoint:(CGPoint)point
{
    self = [super initWithFrame:CGRectMake(point.x, point.y, SCREEN_WIDTH(), [HomeFooterView getViewHeight])];
    if (self) {
        [self setupUI];
    }
    return self;
}

#pragma mark - 

- (void)setupUI {
    [self setBackgroundColor:[UIColor darkGrayColor]];
    
    [self addSwitchLocationButton];
    [self addAtHomeSwitch];
}

- (void)addSwitchLocationButton {
    self.switchLocationButton = [ButtonsHelper getButtonWithDynamicWidthText:NSLocalizedString(@"switch_location", @"Switch Location")
                                                                        type:kButtonGreyBacking
                                                                         tag:kBtnSwitchLocation
                                                                widthPadding:10.0f
                                                                      height:30.0f
                                                                      target:self
                                                                    selector:@selector(buttonPressed:)];
    [self.switchLocationButton setFrameX:10.0f];
    [self.switchLocationButton setFrameY:(self.frame.size.height - self.switchLocationButton.frame.size.height) / 2];
    [self addSubview:self.switchLocationButton];
    
    if ([AppStyleHelper isMegaMan])  {
        UIButton *button = [ViewBuilderHelper getButtonWithLoFileName:@"switch-location-icon.png"
                                                           hiFilename:@"switch-location-icon.png"
                                                                  atX:self.switchLocationButton.frame.origin.x
                                                                  atY:self.switchLocationButton.frame.origin.y
                                                                  tag:kBtnSwitchLocation
                                                               target:self
                                                               action:@selector(buttonPressed:)];
        [button setFrameY:(self.frame.size.height - button.frame.size.height) / 2];
        [self addSubview:button];
        [self.switchLocationButton alignToRightOfView:button padding:0.0f];
        
        [self.switchLocationButton setBackgroundImage:nil forState:UIControlStateNormal];
        [self.switchLocationButton setBackgroundImage:nil forState:UIControlStateHighlighted];
        [self.switchLocationButton setBackgroundImage:nil forState:UIControlStateSelected];
        [self.switchLocationButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)addAtHomeSwitch {
    UIImage *loUIImage = [UIImage imageNamed:@"radio"];
    UIImage *hiUIImage = [UIImage imageNamed:@"radio-on"];
    self.selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.selectedButton setFrame:CGRectMake(0.0f,
                                             0.0f,
                                             loUIImage.size.width,
                                             loUIImage.size.height)];
    [self.selectedButton setImage:loUIImage forState:UIControlStateNormal];
    [self.selectedButton setImage:hiUIImage forState:UIControlStateHighlighted];
    [self.selectedButton setImage:hiUIImage forState:UIControlStateSelected];
    [self.selectedButton setSelected:![[UserPrefsHelper shared] getTestWiFi]];
    [self.selectedButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.selectedButton setTag:kBtnAtHome];
    
    if ([AppStyleHelper isMegaMan])
        [self.selectedButton alignToRightOfView:self.switchLocationButton padding:85.0f];
    else
        [self.selectedButton alignToRightOfView:self.switchLocationButton padding:95.0f];
    
    [self.selectedButton setCenterY:self.switchLocationButton.center.y];
    [self addSubview:self.selectedButton];
    
    
    UIButton *_button = [ButtonsHelper getButtonWithDynamicWidthText:NSLocalizedString(@"at_home", @"at_home")
                                                                type:kButtonNone
                                                                 tag:kBtnAtHome
                                                        widthPadding:0.0f
                                                              height:self.selectedButton.frame.size.height
                                                              target:self
                                                            selector:@selector(buttonPressed:)];
    [_button setBackgroundColor:[UIColor clearColor]];
    [_button alignToRightOfView:self.selectedButton padding:5.0f];
    [_button setCenterY:self.switchLocationButton.center.y];
    [self addSubview:_button];
}

#pragma mark - Public

+ (float)getViewHeight {
    return 40.0f;
}
    
#pragma mark - Actions
    
- (IBAction)buttonPressed:(id)sender {
    
    switch ([sender tag]) {
            
        case kBtnSwitchLocation:
        {
            if ([self.delegate respondsToSelector:@selector(switchLocationPressed:)])
                [self.delegate switchLocationPressed:self];
        }
            break;
            
        case kBtnAtHome:
        {
            
            if ([[UserPrefsHelper shared] getUseWiFiName]) {
                
                /*
                    Request by: Jim @ 09/12/2013
                 
                    If I am using the ‘Use WiFi name to check
                    location’ option, I should not be able to toggle At Home from the app home screen
                */
                
            } else {
                [self.selectedButton setSelected:!self.selectedButton.selected];
                [[UserPrefsHelper shared] setTestWifi:!self.selectedButton.selected];
                
                if (!self.selectedButton.selected) {
                    
                    // Change Indicator to Remote
                    [[UDPService shared] setServiceType:kServiceWeb];
                    
                }
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
