//
//  MoreVC.m
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "MoreVC.h"
#import "AppDelegate.h"
#import "WebVC.h"
#import "SettingsVC.h"


@implementation MoreVC
@synthesize more_tbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupOptions];
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [more_tbl setDataSource:self];
    [more_tbl setDelegate:self];
    
    [self setupTitle];
    
    if ([self.more_tbl respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.more_tbl setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)setupTitle {
    _nav_bar.topItem.title = NSLocalizedString(@"more", @"more");
}

- (void)setupOptions {
    options = [[NSArray alloc] initWithObjects:
               NSLocalizedString(@"help", @"help"),
               NSLocalizedString(@"settings", @"settings"),
               nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSString *)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [options count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"MoreCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    cell.textLabel.text = [options objectAtIndex:[indexPath row]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    // There is a bug in iOS7 when using 'presentViewController' in 'didSelectRowAtIndexPath'
    // where the view has a delay before it is pushed. To fix this delay so that it pushes automatically,
    // we auto call it on the main thread, see below.
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UINavigationController *nc;
        
        switch([indexPath row]){
            case 0://Help
            {
                nc = [[UINavigationController alloc] initWithRootViewController:[kApplicationDelegate helpVC]];
            }
                break;
            case 1://Settings
                nc = [[UINavigationController alloc] initWithRootViewController:[[SettingsVC alloc] initWithNibName:kVCXibName bundle:nil]];
                break;
        }
        
        [self presentViewController:nc animated:YES completion:^(void){
            
        }];
    });
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    [self setupTitle];
    [self setupOptions];
    [more_tbl reloadData];
}

@end
