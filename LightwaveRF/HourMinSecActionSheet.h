//
//  HourMinSecActionSheet.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 06/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "LightwaveActionSheet.h"

@interface HourMinSecActionSheet : LightwaveActionSheet <UIPickerViewDataSource, UIPickerViewDelegate> {
    
}

@property (nonatomic, strong) DistancePickerView *timePickerView;

@end
