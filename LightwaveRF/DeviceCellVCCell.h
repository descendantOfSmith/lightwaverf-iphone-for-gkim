//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Device;

@interface DeviceCellVCCell : UITableViewCell {

}

@property (nonatomic, assign) int zoneID;
@property (strong, nonatomic) IBOutlet UIImageView *icon_img;
@property (strong, nonatomic) IBOutlet UILabel *name_lbl;
@property (strong, nonatomic) Device *device;
@property (strong) NSIndexPath *indexPath;
@property (strong, nonatomic) NSTimer *fadeActive_tmr;
@property (strong, nonatomic) NSTimer *onFadeActive_tmr;
@property (strong, nonatomic) NSTimer *stopFadeActive_tmr;

+ (float)getCellHeight;

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing;

- (void)setupCellWithDevice:(Device *)_device
                     zoneID:(int)_zoneID;

- (IBAction)onPressed:(id)sender;
- (IBAction)offPressed:(id)sender;
- (IBAction)stopPressed:(id)sender;

@end
