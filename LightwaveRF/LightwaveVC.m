//
//  LightwaveVC.m
//  LightwaveRF
//
//  Created by Nicholas Lever on 09/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "LightwaveVC.h"
#import "HomeVC.h"
#import "RoutinesVC.h"
#import "AlertsVC.h"
#import "EventsVC.h"
#import "MoreVC.h"

@interface LightwaveVC ()

@end

@implementation LightwaveVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        UIViewController *vc1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        UIViewController *vc2 = [[RoutinesVC alloc] initWithNibName:@"RoutinesVC" bundle:nil];
        UIViewController *vc3 = [[AlertsVC alloc] initWithNibName:@"AlertsVC" bundle:nil];
        UIViewController *vc4 = [[EventsVC alloc] initWithNibName:@"EventsVC" bundle:nil];
        UIViewController *vc5 = [[MoreVC alloc] initWithNibName:@"MoreVC" bundle:nil];
        self.tabBarController.viewControllers = [NSArray arrayWithObjects:vc1, vc2, vc3, vc4, vc5, nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
