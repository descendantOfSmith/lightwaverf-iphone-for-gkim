//
//  NSString+SSEnumTypes
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "NSString+SSEnumTypes.h"

@implementation NSString (SSEnumTypes)

- (NSDictionary *)returnDictionaryOfButtonTypes {
    NSDictionary *buttonTypeDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInteger:kBtnHome], @"kBtnHome",
                                    [NSNumber numberWithInteger:kBtnAway], @"kBtnAway",
                                    [NSNumber numberWithInteger:kBtnBack], @"kBtnBack",
                                    [NSNumber numberWithInteger:kBtnAction], @"kBtnAction",
                                    [NSNumber numberWithInteger:kBtnDismiss], @"kBtnDismiss",
                                    [NSNumber numberWithInteger:kBtnShare], @"kBtnShare",
                                    [NSNumber numberWithInteger:kBtnAdd], @"kBtnAdd",
                                    [NSNumber numberWithInteger:kBtnDelete], @"kBtnDelete",
                                    [NSNumber numberWithInteger:kBtnEdit], @"kBtnEdit",
                                    [NSNumber numberWithInteger:kBtnCancel], @"kBtnCancel",
                                    [NSNumber numberWithInteger:kBtnDone], @"kBtnDone",
                                    [NSNumber numberWithInteger:kBtnAddAction], @"kBtnAddAction",
                                    [NSNumber numberWithInteger:kBtnAddDelay], @"kBtnAddDelay",
                                    [NSNumber numberWithInteger:kBtnOn], @"kBtnOn",
                                    [NSNumber numberWithInteger:kBtnOff], @"kBtnOff",
                                    [NSNumber numberWithInteger:kBtnStop], @"kBtnStop",
                                    [NSNumber numberWithInteger:kBtnPairDevices], @"kBtnPairDevices",
                                    [NSNumber numberWithInteger:kBtnStartProfile], @"kBtnStartProfile",
                                    [NSNumber numberWithInteger:kBtnLogout], @"kBtnLogout",
                                    [NSNumber numberWithInteger:kBtnSaveElecRate], @"kBtnSaveElecRate",
                                    [NSNumber numberWithInteger:kBtnSaveImpKwh], @"kBtnSaveImpKwh",
                                    [NSNumber numberWithInteger:kBtnGetSettings], @"kBtnGetSettings",
                                    [NSNumber numberWithInteger:kBtnSaveSettings], @"kBtnSaveSettings",
                                    [NSNumber numberWithInteger:kBtnSyncDevice], @"kBtnSyncDevice",
                                    [NSNumber numberWithInteger:kBtnSendSettings], @"kBtnSendSettings",
                                    [NSNumber numberWithInteger:kBtnRegisterWiFiLink], @"kBtnRegisterWiFiLink",
                                    [NSNumber numberWithInteger:kBtnToggle], @"kBtnToggle",
                                    [NSNumber numberWithInteger:kBtnSetLocation], @"kBtnSetLocation",
                                    [NSNumber numberWithInteger:kBtnSwitchLocation], @"kBtnSwitchLocation",
                                    [NSNumber numberWithInteger:kBtnAtHome], @"kBtnAtHome",
                                    [NSNumber numberWithInteger:kBtnTimezone], @"kBtnTimezone",
                                    [NSNumber numberWithInteger:kBtnSSID], @"kBtnSSID",
                                    [NSNumber numberWithInteger:kBtnLogin], @"kBtnLogin",
                                    [NSNumber numberWithInteger:kBtnFindInstaller], @"kBtnFindInstaller",
                                    [NSNumber numberWithInteger:kButtonNew], @"kButtonNew",
                                    [NSNumber numberWithInteger:kButtonSkip], @"kButtonSkip",
                                    [NSNumber numberWithInteger:kBtnProgram], @"kBtnProgram",
                                    [NSNumber numberWithInteger:kBtnSetTemp], @"kBtnSetTemp",
                                    [NSNumber numberWithInteger:kBtnMore], @"kBtnMore",
                                    [NSNumber numberWithInteger:kBtnChange], @"kBtnChange",
                                    [NSNumber numberWithInteger:kBtnForgottenPassword], @"kBtnForgottenPassword",
                                    [NSNumber numberWithInteger:kBtnChooseLanguage], @"kBtnChooseLanguage",

                                    nil
                                    ];
    return buttonTypeDict;
}

- (NSString *)stringFromButtonTypeEnum:(ButtonTypes)buttonType {
    NSDictionary *buttonTypeDict = [self returnDictionaryOfButtonTypes];
    NSString *foundKey = nil;
    
    for (NSString *key in [buttonTypeDict allKeys]) {
        ButtonTypes checkButtonType = (ButtonTypes)[[buttonTypeDict objectForKey:key] intValue];
        if (buttonType == checkButtonType) {
            foundKey = key;
            break;
        }
    }
    
    return foundKey;
}

- (ButtonTypes)buttonTypeEnumFromString {
    NSDictionary *buttonTypeDict = [self returnDictionaryOfButtonTypes];
    return (ButtonTypes)[[buttonTypeDict objectForKey:self] intValue];
}

@end
