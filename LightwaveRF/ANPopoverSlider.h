//
//  ANPopoverSlider.h
//  CustomSlider
//
//  Created by Gabriel  on 30/1/13.
//  Copyright (c) 2013 App Ninja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANPopoverView.h"

@protocol ANPopoverSliderDataSource;

@interface ANPopoverSlider : UISlider {
    
}

@property (nonatomic, assign) id <ANPopoverSliderDataSource> dataSource;

@property (strong, nonatomic) ANPopoverView *popupView;

@property (nonatomic, readonly) CGRect thumbRect;

@end

@protocol ANPopoverSliderDataSource <NSObject>

@optional

- (NSString *)getPopoverText:(ANPopoverSlider *)_slider;

@end

