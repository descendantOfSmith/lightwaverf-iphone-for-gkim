//
//  CurrentLocationHelper.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 20/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "GetAddressLocator.h"


// NSNotifications
#define kCurrentLocationSuccessNotificationKey @"kCurrentLocationSuccessNotificationKey"
#define kCurrentLocationFailedNotificationKey @"kCurrentLocationFailedNotificationKey"

@interface CurrentLocationHelper : NSObject <CLLocationManagerDelegate, GetAddressLocatorDelegate> {
    
}

+ (id)shared;

- (void)getLocation;
- (void)stopSearchingLocation;

@end
