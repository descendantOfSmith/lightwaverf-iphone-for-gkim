//
//  EnergyHelper.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Energy.h"

@interface EnergyHelper : NSObject {
    
}

+ (Energy *)initializeEnergy;
+ (Energy *)getEnergyFromData:(NSString *)data;

@end
