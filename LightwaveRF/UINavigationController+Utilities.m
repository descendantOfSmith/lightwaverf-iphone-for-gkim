//
//  UINavigationController+Utilities.m
//  LimeIT
//
//  Created by Dan's iMac on 24/09/2012.
//
//

#import "UINavigationController+Utilities.h"

@implementation UINavigationController (Utilities)

/*
 * Instead of pushing a new controller onto the stack, use this function to
 * replace the current View Controller with the replacement UIViewController 'controller' parameter
 * @param controller The View Controller to display
 */
- (void)replaceLastWith:(UIViewController *)controller animated:(BOOL)animated {
    NSMutableArray *stackViewControllers = [NSMutableArray arrayWithArray:self.viewControllers];
    [stackViewControllers removeLastObject];
    [stackViewControllers addObject:controller];
    [self setViewControllers:stackViewControllers animated:YES];
}

- (void)replaceControllerAtIndex:(NSInteger)index with:(UIViewController *)controller animated:(BOOL)animated {
    NSMutableArray *stackViewControllers = [NSMutableArray arrayWithArray:self.viewControllers];
    [stackViewControllers replaceObjectAtIndex:index withObject:controller];
    [self setViewControllers:stackViewControllers animated:YES];
}

@end
