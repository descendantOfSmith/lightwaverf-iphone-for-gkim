//
// Debug Log
//

// Better NSLog replacement.
#define DEBUG_MODE
#ifdef DEBUG_MODE
#define DebugLog( s, ... ) NSLog( @"<%s : (%d)> %@",__FUNCTION__, __LINE__, \
    [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DebugLog( s, ... )
#endif

// Sync UI Library - Debugging
#define DebugLogRect( s, rect ) \
    DebugLog(@"Rect: %@: x: %.2f, y: %.2f, w: %.2f, h: %.2f", s, \
    RECT_X(rect), RECT_Y(rect), RECT_WIDTH(rect), RECT_HEIGHT(rect) );
