//
//  Constants.m
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 22/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import "Constants.h"

int const HEAT_ADJUSTMENT = 10;
int const DEFAULT_AMBIENT_TEMPERATURE = 20;
int const DIMMER_MIN = 1;
int const DIMMER_MAX = 32;
int const HEATING_MAX = 30;
int const HEATING_MIN = 10;
//int const MOOD1 = 2;
int const MAX_ACTIONS_PER_EVENT = 10;
int const MAX_ZONES = 15;
int const MAX_TIMERS = 32;
int const MAX_EVENTS = 32;

// Device types
NSString * const LIGHT_SWITCH = @"light-switch";
NSString * const DIMMER = @"light-dimmer";
NSString * const SOCKET = @"socket";
NSString * const HEATING = @"heating";
NSString * const MAG_SWITCH = @"mag-switch";
NSString * const PIR_SENSOR = @"pir-sensor";
NSString * const CAMERA = @"camera";
NSString * const MOOD = @"mood";
NSString * const RADIATOR = @"radiator";
NSString * const COLOUR_LEDS = @"colour-leds";
NSString * const OPEN_CLOSE = @"open-close";
NSString * const FSL500W = @"open-close";
NSString * const FSL3000W = @"open-close";
NSString * const POWER = @"power";
NSString * const DELAY = @"delay";

@implementation Constants

@end
