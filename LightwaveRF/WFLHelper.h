//
//  TimerHelper.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 26/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UDPService.h"
#import "RemoteSettings.h"

@interface WFLHelper : NSObject {
    
}

@property (nonatomic, assign) BOOL isCheckingState;

+ (id)shared;

- (void)updateWFL;
- (void)checkState;
- (void)sendToUDP:(BOOL)_useUDP command:(NSString *)_command tag:(ServiceRequestTags)_tag;
- (void)cleanupAllRequests;

@end
