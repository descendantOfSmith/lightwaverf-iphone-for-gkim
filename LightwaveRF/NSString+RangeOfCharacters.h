//
//  NSString+RangeOfCharacters.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 30/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RangeOfCharacters)

/* note "Characters" is plural in the methods. It has poor readability, hard to
 * distinguish from the rangeOfCharacterFromSet: methods, but it's standard Apple
 * convention.
 */
-(NSRange)rangeOfCharactersFromSet:(NSCharacterSet*)aSet;
-(NSRange)rangeOfCharactersFromSet:(NSCharacterSet*)aSet options:(NSStringCompareOptions)mask;
-(NSRange)rangeOfCharactersFromSet:(NSCharacterSet*)aSet options:(NSStringCompareOptions)mask range:(NSRange)range;

// like the above, but return a string rather than a range
-(NSString*)substringFromSet:(NSCharacterSet*)aSet;
-(NSString*)substringFromSet:(NSCharacterSet*)aSet options:(NSStringCompareOptions)mask;
-(NSString*)substringFromSet:(NSCharacterSet*)aSet options:(NSStringCompareOptions)mask range:(NSRange)range;

@end
