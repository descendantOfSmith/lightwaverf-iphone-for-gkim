//
//  NSMutableArray-Utilities.m
//  ICCEasyLife
//
//  Created by imac on 07/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSMutableArray-Utilities.h"

@implementation NSMutableArray (Utilities)

/*
 * Reverse the order of an array
 */
- (void)reverseArray {
    if ([self count] == 0)
        return;
    NSUInteger i = 0;
    NSUInteger j = [self count] - 1;
    while (i < j) {
        [self exchangeObjectAtIndex:i
                  withObjectAtIndex:j];
        
        i++;
        j--;
    }
}

/*
 * Randomly shuffle an array of objects
 */
- (void)shuffle {
    for (uint i = 0; i < self.count; ++i) {
        // Select a random element between i and end of array to swap with.
        int nElements = self.count - i;
        int n = arc4random_uniform(nElements) + i;
        [self exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
}

/*
 * Move an object in an array from one position to another
 * @param from The Object located at this position
 * @param to The final position of the object
 */
- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to {
    if (to != from) {
        id obj = [self objectAtIndex:from];
        //[obj retain];
        [self removeObjectAtIndex:from];
        if (to >= [self count]) {
            [self addObject:obj];
        } else {
            [self insertObject:obj atIndex:to];
        }
        //[obj release];
    }
}

/*
 * Sort an array of NSDictionarys by 'key' either ascending or descending
 * @param key The dictionary key of the object you want to sort
 * @param isAscending BOOL to sort objects ascending or descending
 */
- (void)sortArrayOfDictionarysWithKey:(NSString *)key ascending:(BOOL)isAscending {
    NSSortDescriptor *dateSortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:isAscending selector:@selector(compare:)];
    [self sortUsingDescriptors:[NSArray arrayWithObjects:dateSortDescriptor, nil]];
}

/*
 * Sort an array of objects either ascending or descending
 * @param isAscending BOOL to sort objects ascending or descending
 * @returns an array of sorted objects
 */
- (NSArray *)sortAscending:(BOOL)isAscending {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:isAscending selector:@selector(localizedCompare:)];
    NSArray *sortedArray = [self sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    return sortedArray;
}

@end
