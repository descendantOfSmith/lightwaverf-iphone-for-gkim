//
//  RegisterVC.m
//  LightwaveRF
//
//  Created by Nik Lever on 05/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "RegisterVC.h"
#import "LWErrorView.h"
#import "AppDelegate.h"
#import "NSString+Utilities.h"
#import "ButtonsHelper.h"

@interface RegisterVC ()
@property (strong, nonatomic) AccessoryView *textFieldToolbar;
@end

@implementation RegisterVC

@synthesize textFieldToolbar;
@synthesize navigaionBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}

- (void)dealloc  {
    self.textFieldToolbar.delegate = nil;
    self.textFieldToolbar = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [navigaionBar setBackgroundColor:[AppStyleHelper getNavBarBackgroundColor]];
    
    [self setupUI];
    
    // Do any additional setup after loading the view from its nib.
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
    gestureRecognizer.numberOfTapsRequired = 1;
    gestureRecognizer.enabled = YES;
    gestureRecognizer.cancelsTouchesInView = NO;
    gestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:gestureRecognizer];

    [self.name_txt setTag:kRegisterVCNameTextField];
    [self.email_txt setTag:kRegisterVCEmailTextField];
    [self.password_txt setTag:kRegisterVCPasswordTextField];
    [self.wifi_txt setTag:kRegisterVCWiFiTextField];

#if TARGET_IPHONE_SIMULATOR
    
    //_name_txt.text = @"John";
    //_email_txt.text = @"john@work.com";
    
    
    _name_txt.text = @"Dan";
    _email_txt.text = [NSString stringWithFormat:@"%@@syncinteractive.co.uk", [[[NSDate date] returnSimpleDateTime] stringByReplacingOccurrencesOfString:@" " withString:@"" ]];
    _password_txt.text = @"2434";
    _wifi_txt.text = @"03248D";
    
     
//    _name_txt.text = @"Jim";
//    _email_txt.text = @"jim@crawley.com";
//    _password_txt.text = @"1234";
//    _wifi_txt.text = @"031394";
    
#endif
}

- (void)setupUI {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self.navigaionBar setFrameY:0.0f];
        [self.scrollView alignToBottomOfView:self.navigaionBar padding:0.0f];
        [self.scrollView setFrameHeight:self.view.frame.size.height - self.scrollView.frame.origin.y];
    }

    [self setupLocalisedLabels];
}

- (void)setupLocalisedLabels {
    [self.navigaionBar.topItem setTitle:NSLocalizedString(@"setup", @"setup")];
    [self.navigaionBar.topItem.leftBarButtonItem setTitle:NSLocalizedString(@"cancel", @"cancel")];
    [self.navigaionBar.topItem.rightBarButtonItem setTitle:NSLocalizedString(@"next", @"next")];
    
    if ([AppStyleHelper isMegaMan])  {
        self.navigaionBar.topItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[ButtonsHelper getButtonWithText:NSLocalizedString(@"cancel", @"cancel")
                                                                                                                              type:kButtonArrowLeft
                                                                                                                               tag:0
                                                                                                                             width:55
                                                                                                                            height:18
                                                                                                                            target:self
                                                                                                                          selector:@selector(cancelPressed:)]];
        
        
        self.navigaionBar.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[ButtonsHelper getButtonWithText:NSLocalizedString(@"next", @"next")
                                                                                                                               type:kButtonArrowRight
                                                                                                                                tag:0
                                                                                                                              width:45
                                                                                                                             height:18
                                                                                                                             target:self
                                                                                                                           selector:@selector(nextPressed:)]];
    }

    [self.name_txt setPlaceholder:NSLocalizedString(@"john_appleseed", @"john_appleseed")];
    [self.email_txt setPlaceholder:NSLocalizedString(@"example_company", @"example_company")];
    [self.password_txt setPlaceholder:NSLocalizedString(@"password_placeholder", @"password_placeholder")];
    [self.wifi_txt setPlaceholder:NSLocalizedString(@"wifi_stub_placeholder", @"wifi_stub_placeholder")];
    
    [self.name_localisated_lbl setText:NSLocalizedString(@"name", @"name")];
    [self.email_localisated_lbl setText:NSLocalizedString(@"email", @"email")];
    [self.password_localisated_lbl setText:NSLocalizedString(@"password", @"password")];
    [self.description_localisated_lbl setText:NSLocalizedString(@"enter_code", @"enter_code")]; 
    [self.wifiLink_localisated_lbl setText:NSLocalizedString(@"wifi_link", @"wifi_link")]; 
    [self.wifiPlaceholder_localisated_lbl setText:NSLocalizedString(@"wifi_stub", @"wifi_stub")];  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    [self setName_txt:nil];
    [self setEmail_txt:nil];
    [self setPassword_txt:nil];
    [self setWifi_txt:nil];
}

#pragma mark - Private

- (void)animationDidStop:(NSString*)animationID finished:(BOOL)finished context:(void *)context {
    if ([animationID isEqualToString:@"BackAnim"])
        [self.view removeFromSuperview];
}

#pragma mark - Validation

- (NSString *)isValid {
    NSMutableString *_str = [NSMutableString string];

    if (_name_txt.text.length <= 0)
        [_str appendString:[NSString stringWithFormat:@"%@\n",  NSLocalizedString(@"register_error1", @"Please enter your name")]];
    
    if (![_email_txt.text isValidEmail])
        [_str appendString:[NSString stringWithFormat:@"%@\n",  NSLocalizedString(@"register_error3", @"Please check your email address")]];
    
    NSNumberFormatter *_formatter = [[NSNumberFormatter alloc] init];
    NSNumber *_num = [_formatter numberFromString:_password_txt.text];

    if (_password_txt.text.length != 4 || !_num)
        [_str appendString:[NSString stringWithFormat:@"%@\n",  NSLocalizedString(@"register_error4", @"Please use 4 numbers in your password")]];
    
    if (_wifi_txt.text.length != 6)
        [_str appendString:[NSString stringWithFormat:@"%@\n",  NSLocalizedString(@"register_error5", @"Please enter the code on the base of your Wifi Link")]];

    // WiFiLink MAC field should only allow for numbers or letters ABCDEF
    NSCharacterSet *_charSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefABCDEF1234567890"];
    _charSet = [_charSet invertedSet];
    NSRange r = [_wifi_txt.text rangeOfCharacterFromSet:_charSet];
    if (r.location != NSNotFound) {
        [_str appendString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"register_error9", @"restricted to numbers and certain letters")]];
    }
    
    return _str.length > 0 ? _str : nil;
}

#pragma mark - Actions

- (IBAction)nextPressed:(id)sender {
    [self hideKeyBoard:nil];
    
//#if TARGET_IPHONE_SIMULATOR
//    [self successResponse];
//    return;
//#endif
    
    NSString *_validationErr = [self isValid];
    
    if (_validationErr) {
        [self showError:_validationErr];
    } else {

        [self showSpinnerWithText:@""];
        
        RemoteSettings *web_service = [RemoteSettings shared];
        [web_service pullHomeWithCallingClass:[self getClassName:[self class]]
                                          tag:kRequestPullHome
                                    withEmail:_email_txt.text
                                  andPassword:_password_txt.text];
    }
}

- (IBAction)cancelPressed:(id)sender {
    [self hideKeyBoard:nil];
    
    CGRect frame = self.view.frame;
    [UIView beginAnimations:@"BackAnim" context:nil];
    self.view.frame = CGRectMake(self.view.bounds.size.width, frame.origin.y, frame.size.width, frame.size.height);
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark Text Field Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self scrollToTargetView:textField];
    
    // Cleanup previous
    if (self.textFieldToolbar) {
        [self.textFieldToolbar removeFromSuperview];
        self.textFieldToolbar = nil;
    }
    
    // Add toolbar
    self.textFieldToolbar = [[AccessoryView alloc] init];
    [self.textFieldToolbar setDelegate:self];
    
    if (textField.tag == kRegisterVCNameTextField)
        [self.textFieldToolbar hidePreviousButton];
    
    if (textField.tag == kRegisterVCWiFiTextField)
        [self.textFieldToolbar hideNextButton];
    
    if (textField.tag == kRegisterVCWiFiTextField)
        [self.textFieldToolbar.doneBtn setTitle:NSLocalizedString(@"submit", @"Submit")];
    
    [self.textFieldToolbar setTag:textField.tag];
    textField.inputAccessoryView = self.textFieldToolbar.view;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark AccessoryView Delegate (for UITextField Toolbar)

- (void)doneTextFieldPressed:(AccessoryView *)tmpAccessoryView {
    if (tmpAccessoryView.tag == kRegisterVCWiFiTextField) {
        [self nextPressed:nil];
        
    } else {
        UITextField *tmpTextField = (UITextField *)[self.view viewWithTag:tmpAccessoryView.tag];
        [self textFieldShouldReturn:tmpTextField];
    }
}

- (void)previousTextFieldPressed:(AccessoryView *)tmpAccessoryView {
    NSUInteger tag = tmpAccessoryView.tag - 1;
    UITextField *tmpTextField = (UITextField *)[self.view viewWithTag:tag];
    [tmpTextField becomeFirstResponder];
}

- (void)nextTextFieldPressed:(AccessoryView *)tmpAccessoryView {
    NSUInteger tag = tmpAccessoryView.tag + 1;
    UITextField *tmpTextField = (UITextField *)[self.view viewWithTag:tag];
    [tmpTextField becomeFirstResponder];
}

#pragma mark - Remote Services Delegate

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kRemoteClassNameKey];
    NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        switch (_tag) {
                
            case kRequestPullHome:
            {
                [self handleRemoteServicePullHome:_notification];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - Remote Services Handlers

- (void)successResponse {
    NSString *firstSection = [_wifi_txt.text substringWithRange:NSMakeRange(0,2)];
    NSString *secondSection = [_wifi_txt.text substringWithRange:NSMakeRange(2,2)];
    NSString *thirdSection = [_wifi_txt.text substringWithRange:NSMakeRange(4,2)];
    
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.tmpName = _name_txt.text;
    app.tmpEmail = _email_txt.text;
    app.tmpPin = _password_txt.text;
    app.tmpMac = [NSString stringWithFormat:@"74:0A:BC:%@:%@:%@", firstSection, secondSection, thirdSection];
    [app switchView:@"Code"];
}

- (void)handleRemoteServicePullHome:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
    {
        /*************************
         * Error
         *************************/
        NSError *error = [_dict objectForKey:kRemoteErrorKey];
        DebugLog(@"%@ %@", [error localizedDescription], [error localizedFailureReason]);
        [self showError:[error localizedDescription]];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        NSString *_response = [[_dict objectForKey:kRemoteResponseKey] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        _response = [_response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        //DebugLog(@"%@", _response);
        
        if ([_response isEqualToString:@"no"]) {
            // New User
            
            [self successResponse];
            
        } else {
            // Existing User
            [self showError:NSLocalizedString(@"register_error8", @"An account with this email address already exists.")];
        }
    }
    
    else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Failed
         *************************/
        NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
        [self showAlert:_failed];
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
