//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"

@interface TimerCell : UITableViewCell {

}

+ (float)getCellHeight;
- (void)positionViewsIsEditing:(BOOL)tmpIsEditing;

- (void)setupCellWithTitle:(NSString*)tmpTitle
                  zoneName:(NSString *)_zoneName
                detailText:(NSString *)tmpDetailText
                 iconImage:(UIImage *)_iconImage;

@end
