//
//  UserPrefsHelper.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 20/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "UserPrefsHelper.h"
#import "Home.h"
#import "Account.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@interface UserPrefsHelper ()

@end

static UserPrefsHelper *userPrefsHelper = nil;

@implementation UserPrefsHelper

+ (id)shared {
    @synchronized(self) {
        if(userPrefsHelper == nil)
            userPrefsHelper = [[super allocWithZone:NULL] init];
    }
    return userPrefsHelper;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self shared];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)dealloc {
    //DebugLog(@"DEALLOC");
}

#pragma mark - Private

- (NSUserDefaults *)getPrefs {
    return [NSUserDefaults standardUserDefaults];
}

- (id)fetchSSIDInfo {
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    //DebugLog(@"Supported interfaces: %@", ifs);
    id info = nil;
    for (NSString *ifnam in ifs) {
        info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        //DebugLog(@"%@ => %@", ifnam, info);
        if (info && [info count]) { break; }
    }
    return info;
}

#pragma mark - Public

- (NSString *)getCurrentWiFi {
    NSDictionary *_dict = [self fetchSSIDInfo];
    NSString *_ssid = [_dict objectForKey:@"SSID"];
    //DebugLog(@"WiFi: %@", _ssid);
    return _ssid;
}

- (void)registerDefaults:(NSDictionary *)_dict {
    [[self getPrefs] registerDefaults:_dict];
}

- (void)save {
    [[self getPrefs] synchronize];
}

- (BOOL)isAtCurrentLocation {
    NSString *_currentConnectedWiFiName = [[UserPrefsHelper shared] getCurrentWiFi];
    NSString *_currentSavedWiFiName = [[UserPrefsHelper shared] getSSID];
    
    if ([_currentConnectedWiFiName isEqualToString:_currentSavedWiFiName]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isLockedToPrivateAccounts {
    if ([[self getUserEmail] isEqualToString:@"*@debug.com"] ||
        [[self getUserEmail] isEqualToString:@"nik@catlystpics.co.uk"] ||
        [[self getUserEmail] isEqualToString:@"jim@crawley.com"] ||
        [[self getUserEmail] isEqualToString:@"test@syncinteractive.co.uk"]) {
        
        return YES;
        
    } else {
        
        return NO;
        
    }
}

#pragma mark - Keys

//- (NSString *)keyForUserName {
//    return @"userName";
//}

//- (NSString *)keyForUserEmail {
//    return @"userEmail";
//}

//- (NSString *)keyForUserPassword {
//    return @"userPassword";
//}

//- (NSString *)keyForUserMac {
//    return @"userMac";
//}

- (NSString *)keyForRegistered {
    return @"registered";
}

- (NSString *)keyForCode {
    return @"Code";
}

- (NSString *)keyForDbInitialised {
    return @"dbInitialised";
}

- (NSString *)keyForDemoAccount {
    return @"demoaccount";
}

- (NSString *)keyForEnableWFLPopups {
    return @"enableWFLPopups";
}

- (NSString *)keyForVideoShown {
    return @"videoShown";
}

- (NSString *)keyForTestWiFi {
    return @"testWifi";
}

- (NSString *)keyForUseWiFiName {
    return @"useWiFiName";
}

- (NSString *)keyForLoginAuto {
    return @"loginAuto";
}

//- (NSString *)keyForElecRate {
//    return @"elecrate";
//}

//- (NSString *)keyForImpKwh {
//    return @"impKwh";
//}

//- (NSString *)keyForLocationInfo {
//    return @"location";
//}

//- (NSString *)keyForTimeZoneInfo {
//    return @"timezone";
//}

//- (NSString *)keyForWiFiPin {
//    return @"wifipin";
//}

//- (NSString *)keyForEmailPwd {
//    return @"emailpwd";
//}

//- (NSString *)keyForSSID {
//    return @"ssid";
//}

- (NSString *)keyForShowHomeFooter {
    return @"showHomeFooter";
}

- (NSString *)keyForShowHomeFooterHasRunBefore {
    return @"showHomeFooterHasRunBefore";
}

- (NSString *)keyForCurrentAccountId {
    return @"currentAccountId";
}

#pragma mark - Getters

- (NSString *)getUserName {
//    return [[self getPrefs] stringForKey:[self keyForUserName]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return _account.name;
}

- (NSString *)getUserEmail {
//    return [[self getPrefs] stringForKey:[self keyForUserEmail]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return _account.email;
}

- (NSString *)getUserPassword {
//    return [[self getPrefs] stringForKey:[self keyForUserPassword]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return _account.password;
}

- (NSString *)getUserMac {
//    return [[self getPrefs] stringForKey:[self keyForUserMac]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return _account.mac;
}

- (NSString *)getCode {
    return [[self getPrefs] stringForKey:[self keyForCode]];
}

- (NSString *)getElecRate {
    //return [[self getPrefs] stringForKey:[self keyForElecRate]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return [NSString stringWithFormat:@"%.2f", _account.elecrate];
}

- (NSString *)getImpKwh {
    //return [[self getPrefs] stringForKey:[self keyForImpKwh]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return [NSString stringWithFormat:@"%d", _account.impkwh];
}

- (NSString *)getLocationInfo {
//    return [[self getPrefs] stringForKey:[self keyForLocationInfo]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return _account.location;
}

- (NSString *)getTimeZoneInfo {
//    return [[self getPrefs] stringForKey:[self keyForTimeZoneInfo]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return _account.timezone;
}

- (NSString *)getTimeZoneInfoWithoutHours {
    NSArray *tokens = [[[UserPrefsHelper shared] getTimeZoneInfo] componentsSeparatedByString:@"|"];
    return [tokens objectAtIndex:0];
}

- (NSString *)getWiFiPin {
//    return [[self getPrefs] stringForKey:[self keyForWiFiPin]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return _account.wifiPin;
}

- (NSString *)getEmailPwd {
//    return [[self getPrefs] stringForKey:[self keyForEmailPwd]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return _account.emailPwd;
}

- (NSString *)getSSID {
//    return [[self getPrefs] stringForKey:[self keyForSSID]];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    return _account.ssid;
}

- (BOOL)getRegistered {
    return [[self getPrefs] boolForKey:[self keyForRegistered]];
}

- (BOOL)getDemoAccount {
    return [[self getPrefs] boolForKey:[self keyForDemoAccount]];
}

- (BOOL)getEnableWFLPopups {
    return [[self getPrefs] boolForKey:[self keyForEnableWFLPopups]];
}

- (BOOL)getVideoShown {
    return [[self getPrefs] boolForKey:[self keyForVideoShown]];
}

- (BOOL)getTestWiFi {
    return [[self getPrefs] boolForKey:[self keyForTestWiFi]];
}

- (BOOL)getUseWiFiName {
    return [[self getPrefs] boolForKey:[self keyForUseWiFiName]];
}

- (BOOL)getLoginAuto {
    return [[self getPrefs] boolForKey:[self keyForLoginAuto]];
}

- (BOOL)getShowHomeFooter {
    BOOL _hasRunBefore = [self getShowHomeFooterHasRunBefore];
    
    if (!_hasRunBefore) {
        [self setShowHomeFooterHasRunBefore:YES];
        
        if ([AppStyleHelper isNexa] ||
            [AppStyleHelper isKlikaan]) {
            
            [self setShowHomeFooter:NO];
            
        } else {
            [self setShowHomeFooter:YES];
        }
        
        return YES;
    }
        
    return [[self getPrefs] boolForKey:[self keyForShowHomeFooter]];
}

- (BOOL)getShowHomeFooterHasRunBefore {
    return [[self getPrefs] boolForKey:[self keyForShowHomeFooterHasRunBefore]];
}

- (NSInteger)getDBInitialised {
    return [[self getPrefs] integerForKey:[self keyForDbInitialised]];
}

- (NSInteger)getCurrentAccountId {
    return [[self getPrefs] integerForKey:[self keyForCurrentAccountId]];
}

- (NSString *)getCurrentLanguageCode {
    return [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
}

- (NSMutableArray *)getAvailableLanguages {
    NSMutableArray *_availableLanguages = [NSMutableArray new];
    
    for (NSString *language in [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]) {
        NSBundle *bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:language ofType:@"lproj"]];
        
        if (bundle) {
            //DebugLog(@"%@: %@", language, NSLocalizedStringFromTableInBundle(@"app_default_language", @"Localizable", bundle, nil));
            [_availableLanguages addObject:language];
        }
    }
    
    return _availableLanguages;
}

#pragma mark - Setters

- (void)setUserName:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForUserName]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.name = _value;
    [[Home shared] saveAccounts];
}

- (void)setUserEmail:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForUserEmail]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.email = _value;
    [[Home shared] saveAccounts];
}

- (void)setUserPassword:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForUserPassword]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.password = _value;
    [[Home shared] saveAccounts];
}

- (void)setUserMac:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForUserMac]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.mac = _value;
    [[Home shared] saveAccounts];
}

- (void)setCode:(NSString *)_value {
    [[self getPrefs] setObject:_value forKey:[self keyForCode]];
    [self save];
}

- (void)setElecRate:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForElecRate]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.elecrate = [_value floatValue];
    [[Home shared] saveAccounts];
}

- (void)setImpKwh:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForImpKwh]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.impkwh = [_value intValue];
    [[Home shared] saveAccounts];
}

- (void)setLocationInfo:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForLocationInfo]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.location = _value;
    [[Home shared] saveAccounts];
}

- (void)setTimeZoneInfo:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForTimeZoneInfo]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.timezone = _value;
    [[Home shared] saveAccounts];
}

- (void)setWiFiPin:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForWiFiPin]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.wifiPin = _value;
    [[Home shared] saveAccounts];
}

- (void)setEmailPwd:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForEmailPwd]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.emailPwd = _value;
    [[Home shared] saveAccounts];
}

- (void)setSSID:(NSString *)_value {
//    [[self getPrefs] setObject:_value forKey:[self keyForSSID]];
//    [self save];
    
    Account *_account = [[Home shared] getAccountWithID:[self getCurrentAccountId]];
    _account.ssid = _value;
    [[Home shared] saveAccounts];
}

- (void)setRegistered:(BOOL)_value {
    [[self getPrefs] setBool:_value forKey:[self keyForRegistered]];
    [self save];
}

- (void)setDemoAccount:(BOOL)_value {
    [[self getPrefs] setBool:_value forKey:[self keyForDemoAccount]];
    [self save];
}

- (void)setEnableWFLPopups:(BOOL)_value {
    [[self getPrefs] setBool:_value forKey:[self keyForEnableWFLPopups]];
    [self save];
}

- (void)setVideoShown:(BOOL)_value {
    [[self getPrefs] setBool:_value forKey:[self keyForVideoShown]];
    [self save];
}

- (void)setTestWifi:(BOOL)_value {
    [[self getPrefs] setBool:_value forKey:[self keyForTestWiFi]];
    [self save];
}

- (void)setUseWiFiName:(BOOL)_value {
    [[self getPrefs] setBool:_value forKey:[self keyForUseWiFiName]];
    [self save];
}

- (void)setLoginAuto:(BOOL)_value {
    [[self getPrefs] setBool:_value forKey:[self keyForLoginAuto]];
    [self save];
}

- (void)setShowHomeFooter:(BOOL)_value {
    [[self getPrefs] setBool:_value forKey:[self keyForShowHomeFooter]];
    [self save];
}

- (void)setShowHomeFooterHasRunBefore:(BOOL)_value {
    [[self getPrefs] setBool:_value forKey:[self keyForShowHomeFooterHasRunBefore]];
    [self save];
}

- (void)setDBInitialised:(NSInteger)_value {
    [[self getPrefs] setInteger:_value forKey:[self keyForDbInitialised]];
    [self save];
}

- (void)setCurrentAccountId:(NSInteger)_value {
    [[self getPrefs] setInteger:_value forKey:[self keyForCurrentAccountId]];
    [self save];
}

@end
