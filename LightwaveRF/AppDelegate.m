//
//  AppDelegate.m
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginVC.h"
#import "Device.h"
#import "MoreVC.h"
#import "ZoneVC.h"
#import "Zone.h"
#import "WebVC.h"
#import "RegisterVC.h"
#import "CodeVC.h"
#import "IntroVC.h"
#import "Home.h"
#import "DB.h"
#import "WFLHelper.h"
#import "AppStyleHelper.h"
#import "FlatUIKit.h"
#import "ButtonsHelper.h"

@implementation AppDelegate

@synthesize nav1Controller;
@synthesize lwController;
@synthesize registerController;
@synthesize codeController;
@synthesize introController;
@synthesize loginController;
@synthesize tmpName;
@synthesize tmpEmail;
@synthesize tmpPin;
@synthesize tmpMac;

-(void)switchView:(NSString *)viewName{
    CGRect frame;
    
    if ([viewName isEqualToString:@"Login"]){
        [self.window addSubview:self.loginController.view];
   
    }else if ([viewName isEqualToString:@"Register"]){
        
        if (self.registerController){
            [self.registerController.view removeFromSuperview];
            self.registerController = nil;
        }
        
        self.registerController = [[RegisterVC alloc] initWithNibName:@"RegisterVC" bundle:nil];
        frame = self.registerController.view.frame;
        self.registerController.view.frame = CGRectMake(320, frame.origin.y, frame.size.width, frame.size.height);
        [UIView beginAnimations:@"Register" context:(__bridge void *)(self)];
        self.registerController.view.frame = CGRectMake(0, frame.origin.y, frame.size.width, frame.size.height);
        [self.window addSubview:self.registerController.view];
        [UIView commitAnimations];

    }else if ([viewName isEqualToString:@"Code"]){
        
        if (self.codeController){
            [self.codeController.view removeFromSuperview];
            self.codeController = nil;
        }
        
        self.codeController = [[CodeVC alloc] initWithNibName:@"CodeVC" bundle:nil];
        frame = self.codeController.view.frame;
        self.codeController.view.frame = CGRectMake(320, frame.origin.y, frame.size.width, frame.size.height);
        [UIView beginAnimations:@"Code" context:nil];
        self.codeController.view.frame = CGRectMake(0, frame.origin.y, frame.size.width, frame.size.height);
        [self.window addSubview:self.codeController.view];
        [UIView commitAnimations];
   
    }else if ([viewName isEqualToString:@"Intro"]){
        
        if (self.introController){
            [self.introController.view removeFromSuperview];
            self.introController = nil;
        }
        
        if (self.registerController){
            [self.registerController.view removeFromSuperview];
            self.registerController = nil;
        }
        
        if (self.codeController){
            [self.codeController.view removeFromSuperview];
            self.codeController = nil;
        }
        
        self.introController = [[IntroVC alloc] initWithNibName:@"IntroVC" bundle:nil];
        frame = self.introController.view.frame;
        self.introController.view.frame = CGRectMake(320, frame.origin.y, frame.size.width, frame.size.height);
        [UIView beginAnimations:@"Intro" context:(__bridge void *)(self)];
        self.introController.view.frame = frame;
        [self.window addSubview:self.introController.view];
        [UIView commitAnimations];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.loginController = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
    
    /*******************************************************
     * BAR BUTTON STYLE
     *******************************************************/

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        
        if (![AppStyleHelper isMegaMan]) {
            [[UIBarButtonItem appearance] setBackgroundImage:[ButtonsHelper getImageForType:kButtonGreenBacking
                                                                                      state:kButtonStateLo]
                                                    forState:UIControlStateNormal
                                                  barMetrics:UIBarMetricsDefault];
            
            [[UIBarButtonItem appearance] setBackgroundImage:[ButtonsHelper getImageForType:kButtonGreenBacking
                                                                                      state:kButtonStateHi]
                                                    forState:UIControlStateHighlighted
                                                  barMetrics:UIBarMetricsDefault];
        }

        // Text Colour

        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                          [AppStyleHelper getIOS7NavBarButtonsTextColor], UITextAttributeTextColor,
                                                                                                          nil]
                                                                                                forState:UIControlStateNormal];
        
        [[UIBarButtonItem appearanceWhenContainedIn:[UIToolbar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                    [AppStyleHelper getIOS7ToolBarButtonsTextColor], UITextAttributeTextColor,
                                                                                                    nil]
                                                                                          forState:UIControlStateNormal];
    }
    
    /*******************************************************
     * STATUS BAR STYLE
     *******************************************************/
    
    // Status Bar with White Text
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    }
    
    /*******************************************************
     * NAVIGATION BAR STYLE
     *******************************************************/
    
    // http://www.appcoda.com/customize-navigation-status-bar-ios-7/
    
    //[[UINavigationBar appearance] setBarTintColor:[UIColor yellowColor]];
    
    // Back Button Colour
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    } else {
        [[UINavigationBar appearance] setTintColor:[AppStyleHelper getNavigationBarTintColor]];
    }
    
    // Nav Bar Image (navbar-ios7.png added in resources also)
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    // Uncomment to change the back indicator image
    /*
     [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"back_btn.png"]];
     [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"back_btn.png"]];
     */
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
        // NSForegroundColorAttributeName
        [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                               [AppStyleHelper getNavBarTextColor], UITextAttributeTextColor,
                                                               nil]];
    }
    
    /*******************************************************
     * TAB BAR BAR STYLE
     *******************************************************/
    
    [[UITabBar appearance] setSelectedImageTintColor:[AppStyleHelper getTabBarSelectedImageTintColor]];
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tabselected.png"]];
    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"tab_bg.png"]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [AppStyleHelper getTabBarTitleColor], UITextAttributeTextColor,
      [UIColor clearColor], UITextAttributeTextShadowColor,
      [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset, nil]
                                             forState: UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:1.0], UITextAttributeTextColor,
      [UIColor clearColor], UITextAttributeTextShadowColor,
      [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset, nil]
                                             forState: UIControlStateSelected];
    
    /*******************************************************
     * VIEW SETUP
     *******************************************************/
    
    UIViewController *vc1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    self.nav1Controller = [[UINavigationController alloc] initWithRootViewController:vc1];
    vc1.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"home_icon", @"Home") image:[UIImage imageNamed:@"tab_home.png"] tag:0];
    [vc1.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tab_home.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_home_unselected.png"]];
    
    UIViewController *vc2 = [[EventsVC alloc] initWithNibName:@"EventsVC" bundle:nil];
    vc2.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"events", @"Events") image:[UIImage imageNamed:@"tab_events_selected.png"] tag:2];
    [vc2.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tab_events_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_events_unselected.png"]];
    
    UIViewController *vc3 = [[TimersVC alloc] initWithNibName:kVCXibName bundle:nil];
    vc3.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"timers", @"Timers") image:[UIImage imageNamed:@"tab_timers.png"] tag:3];
    [vc3.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tab_timers.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_timers_unselected.png"]];

    UIViewController *vc4 = nil;
    UINavigationController *_settingsNavController = nil;
    
    if ([AppStyleHelper isLightWaveRF] ||
        [AppStyleHelper isKlikaan] ||
        [AppStyleHelper isFSLDemo] ||
        [AppStyleHelper isCOCO]) {
        
        vc4 = [[EnergyVC alloc] initWithNibName:@"EnergyVC" bundle:nil];
        vc4.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"energy", @"Energy") image:[UIImage imageNamed:@"tab_energy.png"] tag:4];
        [vc4.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tab_energy.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_energy_unselected.png"]];
    } else {
        vc4 = [self helpVC];
        vc4.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"help", @"Help") image:[UIImage imageNamed:@"tab_help.png"] tag:4];
        [vc4.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tab_help.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_help_unselected.png"]];
    }
    
    _settingsNavController = [[UINavigationController alloc] initWithRootViewController:[self setupFifthTabController]];
    
    _settingsNavController.navigationBar.hidden = YES;
    
    self.lwController = [[UITabBarController alloc] init];
    self.lwController.viewControllers = [NSArray arrayWithObjects:self.nav1Controller, vc2, vc3, vc4, _settingsNavController, nil];
    self.window.rootViewController = self.lwController;
    [self.window addSubview:self.lwController.view];

    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:[NSNumber numberWithBool:YES] forKey:[[UserPrefsHelper shared] keyForLoginAuto]];
    
    [[UserPrefsHelper shared] registerDefaults:dict];

    BOOL registered = [[UserPrefsHelper shared] getRegistered];
    BOOL loginAuto = [[UserPrefsHelper shared] getLoginAuto];
    
    if (!registered || !loginAuto)
        [self.window addSubview:self.loginController.view];
    
    [self.window makeKeyAndVisible];
    
    DB *db = [DB shared];//Init static class
    Home *home = [Home shared];//Init the static class
    if (home != nil && db != nil)
        [home loadAndRefreshWFL:NO];
    
    // Logging App Specific Info:
    DebugLog(@"\n\n App Name: %@\n App Version: %@\n Device Locale: %@\n App Default Language: %@\n Current Language used in App: %@\n Available Languages in App: %@\n\n",
             kAppName,
             kAppVersion,
             [[NSLocale currentLocale] localeIdentifier],
             NSLocalizedString(@"app_default_language", nil),
             [[UserPrefsHelper shared] getCurrentLanguageCode],
             [[UserPrefsHelper shared] getAvailableLanguages]);
    
    return YES;
}

- (UIViewController *)setupFifthTabController {
    if ([AppStyleHelper isLightWaveRF] ||
        [AppStyleHelper isKlikaan] ||
        [AppStyleHelper isFSLDemo] ||
        [AppStyleHelper isCOCO])
        
        return [self moreVC];
    else
        return [self settingsVC];
}

- (MoreVC *)moreVC {
    MoreVC *c = [[MoreVC alloc] initWithNibName:@"MoreVC" bundle:nil];
    c.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"more", @"More") image:[UIImage imageNamed:@"tab_more_selected.png"] tag:5];
    [c.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tab_more_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_more_unselected.png"]];
    return c;
}

- (SettingsVC *)settingsVC {
    SettingsVC *c = [[SettingsVC alloc] initWithNibName:kVCXibName bundle:nil];
    c.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"settings", @"Settings") image:[UIImage imageNamed:@"tab_settings.png"] tag:5];
    [c.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tab_settings.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab_settings_unselected.png"]];
    return c;
}

- (WebVC *)helpVC {
    if ([AppStyleHelper isKlikaan])
        return [[WebVC alloc] initWithURL:@"http://www.coco-technology.nl/app-help/KAKU/help_kaku.html"];
   
    else if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isCOCO])
        return [[WebVC alloc] initWithURL:@"http://www.lightwaverfhost.co.uk/apphelp/ios/lwrf/index.html"];
    
    else if ([AppStyleHelper isNexa])
        return [[WebVC alloc] initWithURL:@"http://www.nexa.se/ios_support.htm"];
    
    else if ([AppStyleHelper isMegaMan])
        return [[WebVC alloc] initWithURL:@"http://www.lightwaverfhost.co.uk/qrcodes/h/ingenium/"];
    
    return [[WebVC alloc] initWithURL:@"http://www.lightwaverfhost.co.uk/apphelp/ios/lwrf/index.html"];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // Continuously check if using WiFi or Remote
    [[WFLHelper shared] checkState];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
