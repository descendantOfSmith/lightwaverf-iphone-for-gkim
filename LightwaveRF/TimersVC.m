//
//  TimersViewController.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 16/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "TimersVC.h"
#import "Home.h"
#import "LTimer.h"
#import "TimerCell.h"
#import "ScheduleEventVC.h"
#import "Toast+UIView.h"
#import "ButtonsHelper.h"

@interface TimersVC ()
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UINavigationBar *navBar;
@end

@implementation TimersVC

@synthesize tableView, navBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Creation

- (void)setupUI {
    [self setupNavBar];
    [self setupTableView];
}

- (void)setupNavBar {
    if (!self.navBar) {
        self.navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH(), 44)];
        self.navBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:self.navBar];
    }

    UINavigationItem *_titleNavItem = [[UINavigationItem alloc] initWithTitle:NSLocalizedString(@"timers", @"Timers")];
    _titleNavItem.leftBarButtonItem = [self topLeftButton];
    _titleNavItem.rightBarButtonItem = [self topRightButton];
    self.navBar.items = [NSArray arrayWithObjects:_titleNavItem, nil];
}

- (UIBarButtonItem *)topRightButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"add", nil)
                                                       type:kButtonArrowRight
                                                        tag:0
                                                      width:45
                                                     height:18
                                                     target:self
                                                   selector:@selector(add:)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        return [[UIBarButtonItem alloc] initWithCustomView:button];
    } else {
        return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                             target:self
                                                             action:@selector(add:)];
    }
}

- (UIBarButtonItem *)topLeftButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:self.tableView.isEditing ? NSLocalizedString(@"done", nil) : NSLocalizedString(@"edit", nil)
                                                       type:kButtonArrowLeft
                                                        tag:kBtnEdit
                                                      width:45
                                                     height:18
                                                     target:self
                                                   selector:@selector(edit:)];
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithCustomView:button];
        [tmpTopRightBtn setTag:kBtnEdit];
        return tmpTopRightBtn;
        
    } else {
        return [[UIBarButtonItem alloc] initWithTitle:self.tableView.isEditing ? NSLocalizedString(@"done", nil) : NSLocalizedString(@"edit", nil)
                                                style:UIBarButtonItemStyleBordered
                                               target:self
                                               action:@selector(edit:)];
    }
}

- (void)setupTableView {
    float padding = 0.0f;
    self.tableView = [ViewBuilderHelper getTableViewWithWidth:self.scrollView.frame.size.width - (padding * 2)
                                                       height:(self.view.frame.size.height - self.navBar.frame.size.height) - kTabBar.frame.size.height
                                                        style:UITableViewStylePlain
                                                       target:self];
    [self.tableView setFrameX:padding];
    [self.tableView setFrameY:self.navBar.frame.size.height];
    [self.tableView setFrameHeight:self.tableView.frame.size.height + (IS_IPHONE_5() ? 88.0f : 0.0f)];
    self.tableView.separatorStyle= UITableViewCellSeparatorStyleSingleLine;
    [self.view addSubview:self.tableView];
    self.heightOffset += self.tableView.frame.size.height;
    [self.tableView reloadData];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([AppStyleHelper isMegaMan]) {
        UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(self.tableView.frame.origin.x,
                                                                     self.tableView.frame.origin.y,
                                                                     self.tableView.frame.size.width,
                                                                     1.0f)];
        [seperator setBackgroundColor:[UIColor lightGrayColor]];
        [self.view addSubview:seperator];
    }
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tmpTableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    Home *home = [Home shared];
    return home.timers.count;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return [TimerCell getCellHeight];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    const char* className = class_getName([self class]);
    NSString *cellIdentifier = cellIdentifier = [NSString stringWithFormat:@"TimerCell-%s", className];
    TimerCell *cell = (TimerCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell = [self setupInitialCell:cell xibName:@"TimerCell"];
    
    // Get Setup
    Home *home = [Home shared];
    LTimer *_timer = [home.timers objectAtIndex:indexPath.row];
    
    UIImage *_iconImage = nil;
    NSString *_zoneName = nil;
    
    if ([_timer isEvent]) {
        _iconImage = [UIImage imageNamed:@"icon_events.png"];
        
    } else {
        Device *_device = [_timer getDevice];
        _iconImage = [_device getIconImage];
        _zoneName = _device.zone.name;
    }

    // Setup Cell
    [cell setupCellWithTitle:[_timer getName]
                    zoneName:_zoneName
                  detailText:[_timer toString]
                   iconImage:_iconImage];
    
    return cell;
}

- (id)setupInitialCell:(TimerCell *)_cell xibName:(NSString *)_xib {
    if (_cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:_xib owner:self options:nil];
        _cell = [topLevelObjects objectAtIndex:0];
        [_cell setFrameWidth:self.tableView.frame.size.width];
        [_cell.contentView setFrameWidth:self.tableView.frame.size.width];
        [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        _cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellarrow.png"]];
    }
    return _cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    
    Home *home = [Home shared];
    LTimer *_timer = [home.timers objectAtIndex:indexPath.row];
    
    if (_timer.active)
        [cell setBackgroundColor:[UIColor whiteColor]];
    else
        [cell setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0.1f]];
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Home *home = [Home shared];
    LTimer *_timer = [home.timers objectAtIndex:indexPath.row];
    [self showScheduleWithTimer:_timer triggerEdit:NO];
}

- (void)showScheduleWithTimer:(LTimer *)_timer triggerEdit:(BOOL)_triggerEdit {
    ScheduleEventVC *c = [[ScheduleEventVC alloc] initWithNibName:kVCXibName bundle:nil eventName:_timer.event_name timer:_timer];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:c];
    [self presentViewController:nc animated:YES completion:^(void){
        c.isModal = YES;
        
        if (_triggerEdit)
            [c editPressed];
    }];
}

#pragma mark - Editing Table View

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    Home *home = [Home shared];
    LTimer *_timer = [home.timers objectAtIndex:indexPath.row];
    
    if (editingStyle == UITableViewCellEditingStyleDelete){
        
        // Delete from WFL
        [_timer deleteTimer];
        
        // Remove Timer
        [home removeTimer:_timer];
        
        // Save
        [home saveTimers];
        
        // Refresh Table
       [self.tableView reloadData];
    }
}

#pragma mark - Table View (Moving Cells)

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    Home *home = [Home shared];
    LTimer *_timer = [home.timers objectAtIndex:sourceIndexPath.row];
    [home removeTimer:_timer];
    [home insertTimer:_timer atIndex:destinationIndexPath.row];
    [home saveTimers];
    [self.tableView reloadData];
}

#pragma mark - AddTimerVC Delegate

- (void)timerAdded:(AddTimerVC *)_eventActionCell timer:(LTimer *)_timer {
    [self showScheduleWithTimer:_timer triggerEdit:YES];
}

#pragma mark - Actions

- (IBAction)add:(id)sender {
    
    Home *home = [Home shared];
    if ([home isFreeTimerSlots]) {
        
        AddTimerVC *c = [[AddTimerVC alloc] initWithNibName:kVCXibName bundle:nil];
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:c];
        [self presentViewController:nc animated:YES completion:^(void){
            c.isModal = YES;
            c.delegate = self;
        }];
        
    } else {
        [self.view makeToast:[NSString stringWithFormat:NSLocalizedString(@"error_timers_limit", nil), MAX_TIMERS]];
    }
}

- (IBAction)edit:(id)sender {
    [self.tableView setEditing:!self.tableView.editing animated:YES];
    [self setupNavBar];
    [self.tableView reloadData];
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
