//
//  RegisterVC.h
//  LightwaveRF
//
//  Created by Nik Lever on 05/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "AccessoryView.h"

typedef enum
{
    kRegisterVCNameTextField = 1001,
    kRegisterVCEmailTextField,
    kRegisterVCPasswordTextField,
    kRegisterVCWiFiTextField
} RegisterVCTags;

@interface RegisterVC : RootViewController <UIGestureRecognizerDelegate, UITextFieldDelegate, AccessoryViewDelegate>

@property (strong, nonatomic) IBOutlet UINavigationBar *navigaionBar;
@property (strong, nonatomic) IBOutlet UITextField *name_txt;
@property (strong, nonatomic) IBOutlet UITextField *email_txt;
@property (strong, nonatomic) IBOutlet UITextField *password_txt;
@property (strong, nonatomic) IBOutlet UITextField *wifi_txt;
@property (weak, nonatomic) IBOutlet UILabel *name_localisated_lbl;
@property (weak, nonatomic) IBOutlet UILabel *email_localisated_lbl;
@property (weak, nonatomic) IBOutlet UILabel *password_localisated_lbl;
@property (weak, nonatomic) IBOutlet UILabel *description_localisated_lbl;
@property (weak, nonatomic) IBOutlet UILabel *wifiLink_localisated_lbl;
@property (weak, nonatomic) IBOutlet UILabel *wifiPlaceholder_localisated_lbl;

- (IBAction)nextPressed:(id)sender;
- (IBAction)cancelPressed:(id)sender;

@end
