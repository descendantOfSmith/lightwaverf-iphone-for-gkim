//
//  Constants.h
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 22/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import <Foundation/Foundation.h>

extern int const HEAT_ADJUSTMENT;
extern int const DEFAULT_AMBIENT_TEMPERATURE;
extern int const DIMMER_MIN;
extern int const DIMMER_MAX;
extern int const HEATING_MAX;
extern int const HEATING_MIN;
//extern int const MOOD1;
extern int const MAX_ACTIONS_PER_EVENT;
extern int const MAX_ZONES;
extern int const MAX_TIMERS;
extern int const MAX_EVENTS;

// Device types
extern NSString * const LIGHT_SWITCH;
extern NSString * const DIMMER;
extern NSString * const SOCKET;
extern NSString * const HEATING;
extern NSString * const MAG_SWITCH;
extern NSString * const PIR_SENSOR;
extern NSString * const CAMERA;
extern NSString * const MOOD;
extern NSString * const OPEN_CLOSE;
extern NSString * const FSL500W;
extern NSString * const FSL3000W;
extern NSString * const RADIATOR;
extern NSString * const COLOUR_LEDS;

// State types
extern NSString * const POWER;
extern NSString * const DELAY;

typedef enum
{
    // 1 for open    Close 0    Stop 2
    kConstantClose = 0,
    kConstantOpen = 1,
    kConstantStop= 2
} ConstantsIsOpenClose;

typedef enum
{
    kDeviceUnlocked = 0,
    kDeviceLocked = 1,
    kDeviceFullyLocked = 2
} DeviceLockState;

typedef enum
{
    kHeatingWeekday = 0,
    kHeatingWeekend = 1,
    kHeatingHoliday = 2
} HeatingPeriods;

@interface Constants : NSObject

@end
