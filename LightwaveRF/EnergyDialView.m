//
//  EnergyDialView.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 16/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "EnergyDialView.h"
#import "UILabel+MFAutoresizeLabel.h"
#import "UIView+Utilities.h"
#import "UIView+Alignment.h"

@interface EnergyDialView ()
@property (nonatomic, assign) id <EnergyDialViewDataSource> dataSource;
@property (nonatomic, strong) SegmentedDialView *segmentView;
@property (nonatomic, strong) UILabel *segmentValueLabel;
@property (nonatomic, strong) UILabel *segmentPenceLabel;
@property (nonatomic, strong) UILabel *segmentPoundLabel;
@property (nonatomic, strong) UILabel *segmentTitleLabel;
@end

@implementation EnergyDialView

@synthesize dataSource;
@synthesize segmentSize, headingFont;
@synthesize segmentView, segmentValueLabel, segmentPenceLabel, segmentTitleLabel, showPenceLabel, showPoundLabel, segmentPoundLabel;

- (id)initWithFrame:(CGRect)frame segmentSize:(float)tmpSegmentSize showPence:(BOOL)tmpShowPence dataSource:(id<EnergyDialViewDataSource>)tmpDataSource;
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataSource = tmpDataSource;
        self.segmentSize = tmpSegmentSize;
        self.showPenceLabel = tmpShowPence;
        self.showPoundLabel = NO;
        [self initialize];
        [self setupUI];
    }
    return self;
}

- (void)initialize {
    //self.headingFont = [UIFont fontWithName:@"DS-Digital-Italic" size:150.0f];
    self.headingFont = [UIFont fontWithName:@"Helvetica-Bold" size:150.0f];
}

- (void)dealloc  {
    //DebugLog(@"DEALLOC");
}

#pragma mark - Setup UI

- (void)setupUI {
    [self setBackgroundColor:[UIColor clearColor]];
    
    // Segment Dial
    self.segmentView = [[SegmentedDialView alloc] initWithFrame:CGRectMake((self.frame.size.width - self.segmentSize) / 2,
                                                                           (self.frame.size.height - self.segmentSize) / 2,
                                                                           self.segmentSize,
                                                                           self.segmentSize)
                                                     dataSource:self];
    [self addSubview:self.segmentView];
        
    // Segment Value Label
    self.segmentValueLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    //self.segmentValueLabel.layer.borderColor = [UIColor redColor].CGColor;
    //self.segmentValueLabel.layer.borderWidth = 1.0f;
    [self updateValueFrame];
    [self.segmentValueLabel setTextColor:[UIColor colorWithRed:0.188 green:0.356 blue:0.670 alpha:1.0f]];
    [self.segmentValueLabel setBackgroundColor:[UIColor clearColor]];
    [self.segmentValueLabel setNumberOfLines:1];
    [self.segmentValueLabel setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.segmentValueLabel];

    [self setupPenceLabel];
    [self setupPoundLabel];
    
    // Segment Title Label
    self.segmentTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    //self.segmentTitleLabel.layer.borderColor = [UIColor redColor].CGColor;
    //self.segmentTitleLabel.layer.borderWidth = 1.0f;
    [self updateTitleFrame];
    [self.segmentTitleLabel setTextColor:[UIColor colorWithRed:0.188 green:0.356 blue:0.670 alpha:1.0f]];
    [self.segmentTitleLabel setBackgroundColor:[UIColor clearColor]];
    [self.segmentTitleLabel setNumberOfLines:0];
    [self.segmentTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.segmentTitleLabel];
}

#pragma mark -
#pragma mark - Override Setters

- (void)setHeadingFont:(UIFont *)tmpHeadingFont {
    self->headingFont = tmpHeadingFont;
    [self.segmentTitleLabel setFont:self.headingFont];
    [self refreshLabels];
}

- (void)setShowPenceLabel:(BOOL)tmpShowPenceLabel {
    self->showPenceLabel = tmpShowPenceLabel;
    [self setupPenceLabel];
    [self.segmentPenceLabel setHidden:!self.showPenceLabel];
    
    //if (self.showPenceLabel == YES)
    //    [self setShowPoundLabel:NO];
}

- (void)setShowPoundLabel:(BOOL)tmpShowPoundLabel {
    self->showPoundLabel = tmpShowPoundLabel;
    [self setupPoundLabel];
    [self.segmentPoundLabel setHidden:!self.showPoundLabel];
    
    //if (self.showPoundLabel == YES)
    //    [self setShowPenceLabel:NO];
}

- (void)setShowLabels:(BOOL)_showLabels {
    [self setShowPoundLabel:_showLabels];
    [self setShowPenceLabel:_showLabels];
}

#pragma mark - Private

- (void)setupPenceLabel {
    if (self.showPenceLabel) {
        if (!self.segmentPenceLabel) {
            // Segment Value Secondary Label
            self.segmentPenceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            //self.segmentPenceLabel.layer.borderColor = [UIColor redColor].CGColor;
            //self.segmentPenceLabel.layer.borderWidth = 1.0f;
            [self.segmentPenceLabel setText:NSLocalizedString(@"money_unit", @"p")];
            [self.segmentPenceLabel setMinimumFontSize:12.0f];
            [self.segmentPenceLabel setAdjustsFontSizeToFitWidth:YES];
            [self.segmentPenceLabel setTextColor:[UIColor colorWithRed:0.188 green:0.356 blue:0.670 alpha:1.0f]];
            [self.segmentPenceLabel setBackgroundColor:[UIColor clearColor]];
            [self.segmentPenceLabel setNumberOfLines:1];
            [self.segmentPenceLabel setTextAlignment:NSTextAlignmentCenter];
            [self addSubview:self.segmentPenceLabel];
        }
    }
}

- (void)setupPoundLabel {
    if (self.showPoundLabel) {
        if (!self.segmentPoundLabel) {
            // Segment Value Secondary Label
            self.segmentPoundLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            //self.segmentPoundLabel.layer.borderColor = [UIColor redColor].CGColor;
            //self.segmentPoundLabel.layer.borderWidth = 1.0f;
            [self.segmentPoundLabel setText:NSLocalizedString(@"money_unit2", @"£")];
            [self.segmentPoundLabel setMinimumFontSize:12.0f];
            [self.segmentPoundLabel setAdjustsFontSizeToFitWidth:YES];
            [self.segmentPoundLabel setTextColor:[UIColor colorWithRed:0.188 green:0.356 blue:0.670 alpha:1.0f]];
            [self.segmentPoundLabel setBackgroundColor:[UIColor clearColor]];
            [self.segmentPoundLabel setNumberOfLines:1];
            [self.segmentPoundLabel setTextAlignment:NSTextAlignmentCenter];
            [self addSubview:self.segmentPoundLabel];
        }
    }
}

- (void)updateFrames {
    [self updateTitleFrame];
    [self updateValueFrame];
}

- (void)updateTitleFrame {
    [self.segmentTitleLabel setFrameWidth:(self.segmentView.frame.size.width / 100) * 54];
    [self.segmentTitleLabel setFrameHeight:(self.segmentView.frame.size.height / 100) * 20];
    [self.segmentTitleLabel setFrameX:(self.frame.size.width - self.segmentTitleLabel.frame.size.width) / 2];
    [self.segmentTitleLabel setFrameY:(self.segmentValueLabel.frame.origin.y + self.segmentValueLabel.frame.size.height) + 2.0f];
    [self.segmentTitleLabel setFont:self.headingFont];
    [self.segmentTitleLabel setTextAlignment:NSTextAlignmentCenter];
}

- (void)updateValueFrame {
    [self.segmentValueLabel setFrameWidth:(self.segmentView.frame.size.width / 100) * (self.showPenceLabel ? 40 : 60)];
    [self.segmentValueLabel setFrameHeight:(self.segmentView.frame.size.height / 100) * (self.showPenceLabel ? 40 : 40)];
    [self.segmentValueLabel setFrameX:(self.frame.size.width - self.segmentValueLabel.frame.size.width) / 2];
    [self.segmentValueLabel setFrameY:((self.frame.size.height - self.segmentValueLabel.frame.size.height) / 2) - ((self.frame.size.height / 100) * 10)];
    [self.segmentValueLabel setFont:[UIFont fontWithName:@"DS-Digital-Italic" size:150.0f]];
    [self.segmentValueLabel setTextAlignment:NSTextAlignmentCenter];
}

#pragma mark - Public

- (void)refreshData {
    [self updateFrames];
    [self refreshDial];
    [self refreshLabels];
}

- (void)refreshDial {
    [self.segmentView setNeedsDisplay];
}

- (void)refreshLabels {
    [self.segmentTitleLabel setAutoresizedText:[self getSegmentHeading]];
    
    if (self.showPenceLabel) {
        
        // Update Value Label
        //[self.segmentValueLabel setBackgroundColor:[UIColor redColor]];
        [self.segmentValueLabel setAutoresizedText:[self getSegmentValue]];
        [self.segmentValueLabel setText:[self getSegmentValue]];
        [self.segmentValueLabel setFont:self.segmentValueLabel.font];
        [self.segmentValueLabel sizeToFit];
        //[self.segmentValueLabel setFrameX:(self.frame.size.width - self.segmentValueLabel.frame.size.width) / 2];
        [self.segmentValueLabel setFrameX:((self.frame.size.width - self.segmentValueLabel.frame.size.width) / 2) - ((self.frame.size.width / 100) * 3.5)];
        [self.segmentValueLabel setFrameY:((self.frame.size.height - self.segmentValueLabel.frame.size.height) / 2) - ((self.frame.size.height / 100) * 10)];
        
        // Update Secondary Label
        float reduceByPercentage = 60.0f;
        [self.segmentPenceLabel setFont:[UIFont fontWithName:@"DS-Digital-Italic" size:(self.segmentValueLabel.font.pointSize / 100) * reduceByPercentage]];
        [self.segmentPenceLabel sizeToFit];
        [self.segmentPenceLabel alignToRightOfView:self.segmentValueLabel padding:0.0f matchVertical:YES];
        [self.segmentPenceLabel setFrameY:(self.segmentValueLabel.frame.origin.y + self.segmentValueLabel.frame.size.height) - self.segmentPenceLabel.frame.size.height];
        
    } else if (self.showPoundLabel) {
        
        // Update Value Label
        [self.segmentValueLabel setFrameWidth:46.0f];
        [self.segmentValueLabel setFrameHeight:43.0f];
        [self.segmentValueLabel setAutoresizedText:[self getSegmentValue]];
        [self.segmentValueLabel setText:[self getSegmentValue]];
        [self.segmentValueLabel setFont:self.segmentValueLabel.font];
        
        // Update Secondary Label Settings
        [self.segmentPoundLabel setFrameWidth:self.segmentValueLabel.frame.size.width];
        [self.segmentPoundLabel setFrameHeight:self.segmentValueLabel.frame.size.height];
        [self.segmentPoundLabel setFont:[UIFont fontWithName:@"DS-Digital-Italic" size:self.segmentValueLabel.font.pointSize]];
        [self.segmentPoundLabel sizeToFit];
        
        // Update Value Label
        //[self.segmentValueLabel setFrameX:(self.frame.size.width - self.segmentValueLabel.frame.size.width) / 2];
        [self.segmentValueLabel setFrameX:((self.frame.size.width - self.segmentValueLabel.frame.size.width) / 2) + (self.segmentPoundLabel.frame.size.width / 2)];
        [self.segmentValueLabel setFrameY:((self.frame.size.height - self.segmentValueLabel.frame.size.height) / 2) - ((self.frame.size.height / 100) * 10)];
        
        // Update Secondary Label
        [self.segmentPoundLabel alignToLeftOfView:self.segmentValueLabel padding:0.0f matchVertical:NO];
        [self.segmentPoundLabel setCenterY:self.segmentValueLabel.center.y];
        
    } else {
        [self.segmentValueLabel setAutoresizedText:[self getSegmentValue]];
    }
    
    //[self.segmentTitleLabel alignToBottomOfView:self.segmentValueLabel padding:0.0f];
    [self.segmentTitleLabel setFrameY:(self.frame.size.height - self.segmentTitleLabel.frame.size.height) - ((self.frame.size.height / 100) * 20)];
}

#pragma mark - SegmentedDialView DataSource

- (float)percentageComplete:(SegmentedDialView *)tmpSegmentedDialView {
    return [self getProgress];
}

#pragma mark -
#pragma mark - Get DataSource

- (float)getProgress {
    if ([self.dataSource respondsToSelector:@selector(percentageComplete:)])
        return [self.dataSource percentageComplete:self];
    
    return 0;
}

- (NSString *)getSegmentHeading {
    if ([self.dataSource respondsToSelector:@selector(segmentHeading:)])
        return [self.dataSource segmentHeading:self];
    
    return @"";
}

- (NSString *)getSegmentValue {
    if ([self.dataSource respondsToSelector:@selector(segmentValue:)])
        return [self.dataSource segmentValue:self];
    
    return @"";
}

@end
