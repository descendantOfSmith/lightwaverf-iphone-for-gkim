//
//  LTimer.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 26/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Home.h"
#import "Device.h"
#import "Event.h"
#import "Action.h"
#import "Mood.h"

@interface LTimer : NSObject {
    
}

@property (assign, nonatomic) BOOL createdOnWFL;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *cmd;
@property (copy, nonatomic) NSString *time;
@property (copy, nonatomic) NSString *start_date;
@property (copy, nonatomic) NSString *end_date;
@property (copy, nonatomic) NSString *days;
@property (copy, nonatomic) NSString *months;
@property (copy, nonatomic) NSString *event_name;
@property (assign, nonatomic) BOOL active;

- (id)initWithCommand:(NSString *)_command;
-(id) initWithCommand:(NSString *)_command active:(BOOL)_isActive;
- (void)setupWithCommand:(NSString *)_command active:(BOOL)_isActive;
- (void)adjustStartDateToTomorrow;

+ (NSString *)generateTimerName;
- (NSString *)getCommand;
- (NSString *)getTimeString;
- (NSString *)getDayString;
- (NSString *)getMonthsString;
- (NSString *)getDatesString;
- (NSString *)getWhen;
- (NSString *)getEndDateString;
- (NSString *)getStartDateString;
//- (NSDate *)getStartDate;
//- (NSDate *)getEndDate;
- (NSMutableArray *)toDictionary;
- (NSData *)toJSON;
- (Device *)getDevice;
- (BOOL)isEvent;
- (NSString *)getName;
- (NSString *)getActionString;
- (NSString *)toString;
- (void)deleteTimer;

@end
