//
//  ChooseProductCell.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 18/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Profile.h"

typedef enum
{
    kHeatingCellStartTime = 0,
    kHeatingCellEndTime,
    kHeatingCellTemperature
} HeatingCellTags;

@protocol HeatingCellDelegate;

@interface HeatingCell : UITableViewCell {
    
}

@property (nonatomic, assign) id <HeatingCellDelegate> delegate;

+ (float)getCellHeight;

- (void)setupCellWithProfile:(Profile*)_profile
                   indexPath:(NSIndexPath *)_indexPath
                    delegate:(id<HeatingCellDelegate>)_delegate;

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing;

@end

@protocol HeatingCellDelegate <NSObject>

@optional

- (void)chooseProductSwitch:(HeatingCell *)_ChooseProductOnOffCell
                  indexPath:(NSIndexPath *)_indexPath
                       type:(HeatingCellTags)_type;

@end
