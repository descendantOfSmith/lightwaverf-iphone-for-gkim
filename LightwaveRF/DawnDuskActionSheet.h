//
//  DawnDuskActionSheet.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 02/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "LightwaveActionSheet.h"

#define kHourNumberOfRows       24
#define kMinuteNumberOfRows     60

@interface DawnDuskActionSheet : LightwaveActionSheet <UIPickerViewDelegate> {
    
}

@property (nonatomic, strong) DistancePickerView *timePickerView;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
       selectedType:(CustomActionSheetPickerDawnDuskTypes)tmpSelectedType;

@end
