//
//  DB.m
//  LightwaveRF
//
//  Created by Nik Lever on 25/06/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "DB.h"
#import "XMLConfig.h"
#import "NSString+Utilities.h"

DB *myDB = nil;

@implementation DB

@synthesize db;

+ (id)shared {
    if (myDB == nil) {
        //DebugLog(@"DB.shared createdSingleton");
        myDB = [[DB alloc] init];
    }
    return myDB;
}

- (id)init {
    //DebugLog(@"DB.init");
    self = [super init];
    
    if (self) {
        //Check if we have a local database
        NSInteger dbInitialised = [[UserPrefsHelper shared] getDBInitialised];
        //dbInitialised = 0;
        if (dbInitialised == 0){
            //We need to initialise the database
            if ([self initialiseDatabase]){
                [[UserPrefsHelper shared] setDBInitialised:1];
            }
        }
    }
    return self;
}

-(bool) initialiseDatabase{
    if ([self openDB]){
        if ([self createTables]){
            [self closeDB];
            return true;
        }
    }
    return false;
}

-(NSString *) databasePath
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory=[paths objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"lightwavedb.sql"];
}

-(bool)openDB
{
    if(sqlite3_open([[self databasePath]UTF8String], &db) != SQLITE_OK)
    {
        [self closeDB];
        NSAssert(0, @"Database failed to Open");
        return false;
    }
    return true;
}

-(void)closeDB{
    if (db!=nil){
        sqlite3_close(db);
        db = nil;
    }
}

-(bool)createTables{
    if (![self openDB]) return false;
    NSMutableArray *fields = [[NSMutableArray alloc]init];
    
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"accountId INTEGER"];
    [fields addObject:@"zoneId INTEGER"];
    [fields addObject:@"displayId INTEGER"];
    [fields addObject:@"name TEXT"];
    if (![self createTable:@"zones" withFields:fields]) return false;
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"accountId INTEGER"];
    [fields addObject:@"deviceId INTEGER"];
    [fields addObject:@"displayId INTEGER"];
    [fields addObject:@"name TEXT"];
    [fields addObject:@"zoneId INTEGER"];
    [fields addObject:@"type TEXT"];
    [fields addObject:@"paired INTEGER"];
    [fields addObject:@"setting INTEGER"];
    [fields addObject:@"moodId INTEGER"];
    if (![self createTable:@"devices" withFields:fields]) return false;
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"accountId INTEGER"];
    [fields addObject:@"moodId INTEGER"];
    [fields addObject:@"deviceId INTEGER"];
    [fields addObject:@"displayId INTEGER"];
    [fields addObject:@"name TEXT"];
    [fields addObject:@"zoneId INTEGER"];
    if (![self createTable:@"moods" withFields:fields]) return false;
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"eventId INTEGER"];
    [fields addObject:@"accountId INTEGER"];
    [fields addObject:@"displayId INTEGER"];
    [fields addObject:@"name TEXT"];
    [fields addObject:@"timerName TEXT"];
    if (![self createTable:@"events" withFields:fields]) return false;
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"accountId INTEGER"];
    [fields addObject:@"displayId INTEGER"];
    [fields addObject:@"eventId INTEGER"];
    [fields addObject:@"hwKey TEXT"];
    [fields addObject:@"delay TEXT"];
    if (![self createTable:@"actions" withFields:fields]) return false;
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"accountId INTEGER"];
    [fields addObject:@"profileId INTEGER"];
    [fields addObject:@"type INTEGER"];
    [fields addObject:@"zoneId INTEGER"];
    [fields addObject:@"temp REAL"];
    [fields addObject:@"start TEXT"];
    [fields addObject:@"end TEXT"];
    if (![self createTable:@"profiles" withFields:fields]) return false;
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"timerId INTEGER"];
    [fields addObject:@"accountId INTEGER"];
    [fields addObject:@"timerName TEXT"];
    [fields addObject:@"command TEXT"];
    [fields addObject:@"active INTEGER"];
    if (![self createTable:@"LTimers" withFields:fields]) return false;
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"wifiPin TEXT"];
    [fields addObject:@"emailPwd TEXT"];
    [fields addObject:@"mac TEXT"];
    [fields addObject:@"email TEXT"];
    [fields addObject:@"password TEXT"];
    [fields addObject:@"name TEXT"];
    [fields addObject:@"location TEXT"];
    [fields addObject:@"ssid TEXT"];
    [fields addObject:@"timezone TEXT"];
    [fields addObject:@"elecrate REAL"];
    [fields addObject:@"impkwh INTEGER"];
    if (![self createTable:@"accounts" withFields:fields])
        return false;

    [self closeDB];
    
    return true;
}

-(bool)createTable:(NSString *)tableName withFields:(NSMutableArray *)fields{
    if (db==nil)
        return false;
    NSString *fieldsStr = [fields componentsJoinedByString:@","];
    NSString *sqlStr = [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@", tableName];
    sqlite3_exec(db, [sqlStr UTF8String], NULL, NULL, NULL);
    
    sqlStr = [NSString stringWithFormat:@"CREATE TABLE %@ (%@)", tableName, fieldsStr];
    //NSLog(@"Home.createTable sql:%@", sqlStr);
    char *err = 0;
    if(sqlite3_exec(db, [sqlStr UTF8String], NULL, NULL, &err) !=SQLITE_OK)
    {
        DebugLog(@"Table %@ failed to create with error code %s", tableName, err);
        //NSAssert(0, @"Table failed to create");
        sqlite3_free( err );
        return false;
    }
    return true;
}

-(bool)sqlExec:(NSString *)sql{
    
    sql = [XMLConfig localise:sql];
    
    if (![self openDB])
        return false;
    
    bool result = true;
    char *err = 0;
    
    if(sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) !=SQLITE_OK){
        DebugLog(@"DB.sqlExec>>Problem with sqllite3_exec %s sql: %@", err, sql);
        sqlite3_free( err );
        result = false;
    }
    
    [self closeDB];
    
    return result;
}

-(NSMutableArray *)getRows:(NSString *)sql{
    if (![self openDB]) return nil;
    
    sqlite3_stmt *statement;
    NSMutableArray *result = nil;
    
    if(sqlite3_prepare_v2(db, [sql UTF8String], -1, &statement, NULL)!=SQLITE_OK){
        DebugLog(@"DB.getRows>>error %@", sql);
    } else {
        result = [NSMutableArray array];
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *row = [NSMutableDictionary dictionary];
            for (int i=0; i<sqlite3_column_count(statement); i++) {
                int colType = sqlite3_column_type(statement, i);
                NSString *rowName = [NSString stringWithFormat:@"%s", sqlite3_column_name(statement, i)];
                id value;
                if (colType == SQLITE_TEXT) {
                    const unsigned char *col = sqlite3_column_text(statement, i);
                    value = [NSString stringWithFormat:@"%s", col];
                    value = [value gtm_stringByUnescapingFromHTML];
                    
                } else if (colType == SQLITE_INTEGER) {
                    int col = sqlite3_column_int(statement, i);
                    value = [NSNumber numberWithInt:col];
                } else if (colType == SQLITE_FLOAT) {
                    double col = sqlite3_column_double(statement, i);
                    value = [NSNumber numberWithDouble:col];
                } else if (colType == SQLITE_NULL) {
                    value = [NSNull null];
                } else {
                    DebugLog(@"[SQLITE] UNKNOWN DATATYPE");
                }
                [row setObject:value forKey:rowName];
            }
            [result addObject:row];
        }
    }
    
    [self closeDB];
    
    return result;
}

-(void)clear{
    [self createTables];
}

-(void)clearAccount:(NSInteger)_accountId {
    NSString *sql = nil;
    
    NSArray *_array = [NSArray arrayWithObjects:
                       @"zones",
                       @"actions",
                       @"devices",
                       @"events",
                       @"LTimers",
                       @"profiles",
                       @"moods",
                       nil];
    
    for (NSString *_str in _array) {
        sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE accountId=%d", _str, _accountId];
        [self sqlExec:sql];
    }
}

@end
