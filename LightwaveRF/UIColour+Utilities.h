//
//  UIColour+Utilities.h
//  TemplateARC
//
//  Created by imac on 14/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utilities)

+ (UIColor *)colorWithRGBHex:(UInt32)hex;
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;
+ (UIColor *)colorWithDivisionOfRed:(float)r green:(float)g blue:(float)b alpha:(CGFloat)a;
+ (UIColor *)randomColour;

@end
