//
//  NSString+SSEnumTypes
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 * This class is to declare any button and section tags
 * With the added ability to swap between the enum and string equivalent
 */
typedef enum
{
    kBtnHome = 1001,
    kBtnAway,
    kBtnBack,
    kBtnAction,
    kBtnDismiss,
    kBtnShare,
    kBtnAdd,
    kBtnDelete,
    kBtnEdit,
    kBtnCancel,
    kBtnDone,
    kBtnAddAction,
    kBtnAddDelay,
    kBtnOn,
    kBtnOff,
    kBtnStop,
    kBtnPairDevices,
    kBtnStartProfile,
    kBtnLogout,
    kBtnSaveElecRate,
    kBtnSaveImpKwh,
    kBtnGetSettings,
    kBtnSaveSettings,
    kBtnSyncDevice,
    kBtnSendSettings,
    kBtnRegisterWiFiLink,
    kBtnToggle,
    kBtnSetLocation,
    kBtnSwitchLocation,
    kBtnAtHome,
    kBtnTimezone,
    kBtnSSID,
    kBtnLogin,
    kBtnFindInstaller,
    kButtonNew,
    kButtonSkip,
    kBtnProgram,
    kBtnSetTemp,
    kBtnMore,
    kBtnChange,
    kBtnForgottenPassword,
    kBtnChooseLanguage
} ButtonTypes;

@interface NSString (SSEnumTypes)

- (NSDictionary *)returnDictionaryOfButtonTypes;
- (NSString *)stringFromButtonTypeEnum:(ButtonTypes)buttonType;
- (ButtonTypes)buttonTypeEnumFromString;

@end
