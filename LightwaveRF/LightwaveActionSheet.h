//
//  CustomActionSheet.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionSheetPicker.h"

/*************************************
 * ActionSheet Types
 *************************************/

typedef enum
{
    kCustomActionSheetHourMinSecPicker = 0,
    kCustomActionSheetDateTimePicker,
    kCustomActionSheetDawnDusk,
    kCustomActionSheetDayPicker,
    kCustomActionSheetMonthPicker,
    kCustomActionSheetTempPicker,
    kCustomActionSheetButtons
} CustomActionSheetPickerTypes;

/*************************************
 * ActionSheet Toggles
 *************************************/

typedef enum
{
    kCustomActionSheetTimePickerDawn = 0,
    kCustomActionSheetTimePickerDusk,
    kCustomActionSheetTimePickerTime
} CustomActionSheetPickerDawnDuskTypes;

typedef enum
{
    kDawnDuskActionSheetBeforeDawn = 96,
    kDawnDuskActionSheetAfterDawn = 97,
    kDawnDuskActionSheetBeforeDusk = 98,
    kDawnDuskActionSheetAfterDusk = 99,
    kDawnDuskActionSheetTime
} DawnDuskActionSheetTypes;

/*************************************
 * ActionSheet Setup
 *************************************/

typedef enum
{
    kCustomActionSheetButton = 0,
    kCustomActionSheetCancelButton
} CustomActionSheetTags;

typedef enum
{
    kCustomActionButtonStatesLo = 0,
    kCustomActionButtonStatesHi
} CustomActionButtonStates;

typedef enum
{
    kCustomActionSheetBackgroundColour = 0,
    kCustomActionSheetGradientStart,
    kCustomActionSheetGradientEnd
} CustomActionSheetStyleColours;

/*************************************
 * Static Vars
 *************************************/
#define lwActionSheetTopPadding             15.0f
#define lwActionSheetBottomPadding          15.0f
#define lwActionSheetButtonPadding          5.0f
#define lwActionSheetButtonCancelPadding    10.0f

@protocol LightwaveActionSheetDataSource;
@protocol LightwaveActionSheetDelegate;

@interface LightwaveActionSheet : UIView {
    
}

@property (nonatomic, assign) id <LightwaveActionSheetDataSource> dataSource;
@property (nonatomic, assign) id <LightwaveActionSheetDelegate> delegate;
@property (nonatomic, strong) NSArray *buttonTitlesArray;
@property (nonatomic, copy) NSString *cancelTitle;
@property (nonatomic, assign) float heightOffset;
@property (nonatomic, assign) BOOL isHidden;
@property (nonatomic, assign) BOOL isTapToDismissDisabled;
@property (nonatomic, assign) CustomActionSheetPickerTypes showPickerType;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
     showPickerType:(CustomActionSheetPickerTypes)tmpShowPickerType;

// General
- (void)resetPickerValues;
- (void)toggleActionSheetWithDelay:(float)tmpDelay;
- (void)setTitle:(NSString *)_title forButtonAtIndex:(NSUInteger)_index;

// UI Creation
- (void)addButtonType:(CustomActionSheetTags)tmpType title:(NSString *)tmpTitle tag:(NSUInteger)tmpTag;

// Get Datasource
- (UIColor *)getColourForType:(CustomActionSheetStyleColours)_type;
- (UIImage *)getButtonImageForState:(CustomActionButtonStates)_state;
- (UIImage *)getCancelButtonImageForState:(CustomActionButtonStates)_state;
- (float)getButtonHeight;

@end

@protocol LightwaveActionSheetDataSource <NSObject>

@required

- (UIView *)presentedFromView:(LightwaveActionSheet *)tmpCustomActionSheet;
- (UIColor *)actionSheetColourTypes:(LightwaveActionSheet *)tmpCustomActionSheet type:(CustomActionSheetStyleColours)_type;
- (UIImage *)actionSheetButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state;
- (UIImage *)actionSheetCancelButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state;
- (float)actionSheetButtonHeight:(LightwaveActionSheet *)tmpCustomActionSheet;

@end

@protocol LightwaveActionSheetDelegate <NSObject>

@optional

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet
                           index:(NSUInteger)tmpIndex;

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet
                           index:(NSUInteger)tmpIndex
                 selectedOptions:(NSArray *)_selectedOptions;

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet
                           index:(NSUInteger)tmpIndex
                  selectedString:(NSString *)_selectedString;

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet
                           index:(NSUInteger)tmpIndex
                      pickerView:(UIPickerView *)tmpPickerView;

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet
                           index:(NSUInteger)tmpIndex
                      pickerView:(UIPickerView *)tmpPickerView
                            type:(DawnDuskActionSheetTypes)_type;

- (void)didHideActionSheet:(LightwaveActionSheet *)tmpCustomActionSheet;

@end
