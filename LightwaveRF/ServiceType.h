//
//  ServiceType.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 25/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDate+Utilities.h"
#import "Device.h"
#import "Mood.h"
#import "Constants.h"
#import "LTimer.h"
#import "Zone.h"

typedef enum
{
    kServiceUDP = 7895,
    kServiceWeb
} ServiceTypeTags;

typedef enum
{
    kRequestDeleteAllSequencesAndTimers = 101,
    kRequestCreateTimer,
    kRequestCreateTimers,
    kRequestDeleteTimer,
    kRequestCreateEvent,
    kRequestCreateEvents,
    kRequestCancelEvent,
    kRequestDeleteEvent,
    kRequestSetPin,
    kRequestRegisterUser,
    kRequestPullHome,
    kRequestPushHome,
    kRequestConnectDevice,
    kRequestLockDevice,
    kRequestToggleOnOff,
    kRequestOpen,
    kRequestClose,
    kRequestStop,
    kRequestDim,
    kRequestMood,
    kRequestTriggerEvent,
    kRequestEnergyMeter,
    kRequestSaveMood,
    kRequestPollState,
    kRequestLocation,
    kRequestTimeZone,
    kRequestLEDColourChange,
    kRequestLEDAutoCycle,
    kRequestImpKwh
} ServiceRequestTags;

typedef enum
{
    kServiceUDPSwitchingAlert = 1001
} ServiceTypeAlertTags;

// NSNotification for Continous WiFi Checking
#define kUDPCheckStateSuccessNotificationKey @"UDP_Check_State_Success_Notification"
#define kUDPCheckStateFailNotificationKey    @"UDP_Check_State_Fail_Notification"

#define kExpireNumMins              10
#define kPurgeExpiredRequestsTime   (kExpireNumMins * 60)

@interface ServiceType : NSObject {
    
}

// Set/Get Mode
- (void)setServiceType:(ServiceTypeTags)_serviceType;
- (ServiceTypeTags)getServiceType;

// Check Current Mode
- (void)isService:(ServiceTypeTags)_usedServiceType;
- (void)isService:(ServiceTypeTags)_usedServiceType showAlert:(BOOL)_showAlert;

// Override
- (void)cleanupAllRequests;

// Commands
+ (NSString *)getStopAllEventsCommand;
+ (NSString *)getDeleteAllSequencesAndTimersCommand;
+ (NSString *)getPollCommand;
+ (NSString *)getMeterCommand;
+ (NSString *)getLockCommandWithZoneID:(int)_roomID device:(Device *)_device;
+ (NSString *)getFullLockCommandWithZoneID:(int)_roomID device:(Device *)_device;
+ (NSString *)getUnlockCommandWithZoneID:(int)_roomID device:(Device *)_device;
+ (NSString *)getOnOffCommandWithZoneID:(int)_roomID device:(Device *)_device isOn:(BOOL)_isOn;
+ (NSString *)getOpenCloseCommandWithZoneID:(int)_roomID device:(Device *)_device openClose:(ConstantsIsOpenClose)_openClose;
+ (NSString *)getDimCommandWithZoneID:(int)_roomID device:(Device *)_device percentage:(float)_percentage;
+ (NSString *)getLEDCommandWithZoneID:(int)_roomID device:(Device *)_device colourCode:(int)_code;
+ (NSString *)getLEDCycleCommandWithZoneID:(int)_roomID device:(Device *)_device;
+ (NSString *)getMoodCommandWithZoneID:(int)_roomID mood:(Mood *)_mood;
+ (NSString *)getSaveMoodCommandWithMood:(Mood *)_mood;
+ (NSString *)getDeleteTimerCommand:(LTimer *)_timer;
+ (NSString *)getPauseTimerCommand:(LTimer *)_timer;
+ (NSString *)getLatLongCommand:(NSString *)_latLong;
+ (NSString *)getTimeZoneCommandWithValue:(int)_value;
+ (NSString *)getImpKwhCommand:(NSString *)_impKwh;

@end
