//
//  UINavigationController+Utilities.h
//  LimeIT
//
//  Created by Dan's iMac on 24/09/2012.
//
//

#import <Foundation/Foundation.h>

@interface UINavigationController (Utilities)

- (void)replaceControllerAtIndex:(NSInteger)index with:(UIViewController *)controller animated:(BOOL)animated;
- (void)replaceLastWith:(UIViewController *)controller animated:(BOOL)animated;

@end
