//
//  UIView+Utilities.h
//  SSUILibrary
//
//  Created by James Munro on 26/07/2011.
//  Copyright 2011 Sync Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


/** A collection of convenient view management routines.
 
 These routines are designed to make the manipulation of views a bit more convenient, such
 as directly modifying the frame origin and size attributes, which would normally require
 either creating a new frame with CGRectMake or creating a local copy of the view's current
 frame and modifying the individual attributes, followed by re-assigning the modified frame
 to the view.
 
 It is worth noting that most of these will call setFrame: on the target view. If you use
 a selection of these routines on a single view, setFrame: will be called multiple times
 so you should avoid doing heavy/expensive operations in custom views that have a non-standard
 setFrame: implementation.
 */
@interface UIView (Utilities)

/** Forces the view to sit on integral coordinates.
 
 This solves the frequent issue where a view's coordinates are not whole number, causing
 the view to be rendered over sub-pixel boundaries which causes an unwanted blurring effect.
 This function is mostly useful in situations where the layout is produced dynamically.
 */
- (void)enforceIntegralFrame;

/** Sets the X coordinate of the view's origin (top left corner).
 @param x The X coordinate of the origin.
 */
- (void)setFrameX:(CGFloat)x;

/** Sets the Y coordinate of the view's origin (top left corner).
 @param y The Y coordinate of the origin.
 */
- (void)setFrameY:(CGFloat)y;

/** Sets the width of the view.
 @param width The width of the view.
 */
- (void)setFrameWidth:(CGFloat)width;

/** Sets the height of the view.
 @param height The height of the view.
 */
- (void)setFrameHeight:(CGFloat)height;

/** Sets the origin of the view.
 @param origin Origin of the view.
 */
- (void)setFrameOrigin:(CGPoint)origin;

/** Sets the size of the view.
 @param size Size of the view.
 */
- (void)setFrameSize:(CGSize)size;

/** Sets the X coordinate of the view's origin (top left corner).
 @param x The X coordinate of the origin.
 */
- (void)setBoundsX:(CGFloat)x;

/** Sets the Y coordinate of the view's origin (top left corner).
 @param y The Y coordinate of the origin.
 */
- (void)setBoundsY:(CGFloat)y;

/** Sets the width of the view.
 @param width The width of the view.
 */
- (void)setBoundsWidth:(CGFloat)width;

/** Sets the height of the view.
 @param height The height of the view.
 */
- (void)setBoundsHeight:(CGFloat)height;

/** Sets the X center coordinate.
 @param x The X coordinate of the view's center.
 */
- (void)setCenterX:(CGFloat)x;

/** Sets the Y center coordinate.
 @param y The Y coordinate of the view's center.
 */
- (void)setCenterY:(CGFloat)y;

/*
 *
 */
- (CGRect)multiplyRect:(float)factor;

/*
 *
 */
- (CGRect)getBoundingRectAfterRotationInRadians:(CGFloat)angleOfRotation;

/*
 *
 */
- (CGFloat)getRotationInRadians;

/** Finds the view controller whose view contains this object.
 @returns The parent view controller, or nil if one cannot be found.
 */
- (UIViewController *)viewController;

@end
