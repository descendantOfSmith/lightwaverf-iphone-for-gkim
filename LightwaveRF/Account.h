//
//  Account.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 25/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Account : NSObject {
    
}

@property (nonatomic, assign) NSInteger accountId;
@property (nonatomic, copy) NSString *mac;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSString *timezone;
@property (nonatomic, copy) NSString *ssid;
@property (nonatomic, copy) NSString *wifiPin;
@property (nonatomic, copy) NSString *emailPwd;
@property (nonatomic, assign) float elecrate;
@property (nonatomic, assign) NSInteger impkwh;

-(id) initWithEmail:(NSString *)_email password:(NSString *)_password andId:(NSInteger)_id;
- (NSString *)toString;
+ (NSInteger)getNewAccountId;
+ (void)updateCurrentAccount:(NSString *)_email password:(NSString *)_password;

// Timezone
+ (NSString *)getDeviceTimeZone;
+ (NSString *)getDeviceTimeZoneWithoutHours;
+ (void)setTimeZone;

@end
