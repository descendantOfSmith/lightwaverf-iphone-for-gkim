//
//  ChooseProductCell.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 18/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ChooseProductCell.h"

@interface ChooseProductCell ()

@end

@implementation ChooseProductCell

@synthesize containingView, iconImageView, titleLabel, selectedButton, device;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
    }
    return self;
}

- (void)dealloc {
    
}

+ (float)getCellHeight {
    return 44.0f;
}

- (void)setupCellWithTitle:(NSString*)tmpTitle device:(Device *)_device selected:(BOOL)_selected {
    
    self.device = _device;
    
    /*************************************
     *  Containing View
     *************************************/
    if (!self.containingView) {
        self.containingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                                       0.0f,
                                                                       self.contentView.frame.size.width,
                                                                       [ChooseProductCell getCellHeight])];
        [self.containingView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.containingView];
    }
    
    /*************************************
     *  Selected?
     *************************************/
    if (!self.selectedButton) {
        UIImage *loUIImage = [UIImage imageNamed:@"tick_lo.jpg"];
        UIImage *hiUIImage = [UIImage imageNamed:@"tick_hi.jpg"];
        self.selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.selectedButton setSelected:_selected];
        [self.selectedButton setFrame:CGRectMake(0.0f,
                                                 0.0f,
                                                 31.0f,
                                                 31.0f)];
        [self.selectedButton setImage:loUIImage forState:UIControlStateNormal];
        [self.selectedButton setImage:hiUIImage forState:UIControlStateHighlighted];
        [self.selectedButton setImage:hiUIImage forState:UIControlStateSelected];
        [self.containingView addSubview:self.selectedButton];
    } else
        [self.selectedButton setSelected:_selected];
    
    /*************************************
     *  Icon
     *************************************/
    if (!self.iconImageView) {
        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,
                                                                           0.0f,
                                                                           36.0f,
                                                                           36.0f)];
        [self.iconImageView setContentMode:UIViewContentModeScaleAspectFit];
        self.iconImageView.image = self.device ? [self.device getIconImage] : [UIImage imageNamed:@"icon_events.png"];
        [self.containingView addSubview:self.iconImageView];
    }
    
    /*************************************
     *  Title Label
     *************************************/
    if (!self.titleLabel) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.titleLabel setText:tmpTitle];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:[UIColor blackColor]];
        [self.titleLabel setMinimumFontSize:13.0f];
        [self.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
        [self.titleLabel setNumberOfLines:1];
        [self.titleLabel setAdjustsFontSizeToFitWidth:YES];
        [self.titleLabel sizeToFit];
        [self.containingView addSubview:self.titleLabel];
    } else {
        [self.titleLabel setText:tmpTitle];
    }
    
    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    // Selected Image
    [self.selectedButton setFrameX:tmpIsEditing ? 35.0f : 5.0f];
    [self.selectedButton setCenterY:self.containingView.center.y];
    
    // Icon Image
    [self.iconImageView alignToRightOfView:self.selectedButton padding:5.0f];
    [self.iconImageView setCenterY:self.containingView.center.y];
    
    // Title
    [self.titleLabel alignToRightOfView:self.iconImageView padding:5.0f];
    [self.titleLabel setCenterY:self.containingView.center.y];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self positionViewsIsEditing:self.isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

// Prevent the selected button from absorbing the touches
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *hitView = [super hitTest:point withEvent:event];
    
    if (hitView == self.selectedButton) {
        return self;
    }
    
    return hitView;
}

@end
