//
//  DateTimeActionSheet.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 06/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "LightwaveActionSheet.h"

@interface DateTimeActionSheet : LightwaveActionSheet {
    
}

@property (nonatomic, strong) UIPickerView *datePickerView;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
   selectedDateMode:(UIDatePickerMode)tmpDateMode;

@end
