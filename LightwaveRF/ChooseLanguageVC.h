//
//  ChooseLanguageVC.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 10/12/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "RootViewController.h"

@interface ChooseLanguageVC : RootViewController <UITableViewDelegate, UITableViewDataSource> {
    
}

@end
