//
//  SegmentedDialView.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 15/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SegmentedDialViewDataSource;

@interface SegmentedDialView : UIView {
    
}

@property (nonatomic, assign) CGFloat borderWidth;
@property (nonatomic, assign) float totalArc;
@property (nonatomic, strong) UIColor *fillColour;
@property (nonatomic, strong) UIColor *trackColour;
@property (nonatomic, strong) UIColor *progressColour;

- (id)initWithFrame:(CGRect)frame dataSource:(id<SegmentedDialViewDataSource>)tmpDataSource;

@end

@protocol SegmentedDialViewDataSource <NSObject>

@optional

- (float)percentageComplete:(SegmentedDialView *)tmpSegmentedDialView;

@end
