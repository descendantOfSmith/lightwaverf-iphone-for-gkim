//
//  EventDevicesVC.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "Event.h"
#import "ChooseProductCell.h"
#import "ChooseProductOnOffCell.h"
#import "ChooseProductSliderCell.h"
#import "ChooseProductOpenCloseCell.h"
#import "ChooseProductLEDCell.h"

@protocol AddTimerVCDelegate;

@interface AddTimerVC : RootViewController <UITableViewDelegate, UITableViewDataSource, ChooseProductOnOffCellDelegate, ChooseProductOpenCloseCellDelegate> {
    
}

@property (nonatomic, assign) id <AddTimerVCDelegate> delegate;

@end

@protocol AddTimerVCDelegate <NSObject>

@optional

- (void)timerAdded:(AddTimerVC *)_eventActionCell timer:(LTimer *)_timer;

@end
