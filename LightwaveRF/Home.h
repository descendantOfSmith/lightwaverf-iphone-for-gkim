//
//  Home.h
//  LightwaveRF
//
//  Created by Nik Lever on 04/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Zone;
@class Event;
@class Profile;
@class Device;
@class LTimer;
@class Account;

@interface Home : NSObject{
    NSMutableArray *_zones;
    NSMutableArray *_events;
    NSMutableArray *_profiles;
    NSMutableArray *_timers;
    NSMutableArray *_accounts;
}

@property (strong, nonatomic, readonly) NSMutableArray *zones;
@property (strong, nonatomic, readonly) NSMutableArray *events;
@property (strong, nonatomic, readonly) NSMutableArray *profiles;
@property (strong, nonatomic, readonly) NSMutableArray *timers;
@property (strong, nonatomic, readonly) NSMutableArray *accounts;

+ (id)shared;

-(BOOL)loadAndRefreshWFL:(BOOL)_refreshWFL;
-(BOOL)save;
-(BOOL)saveData:(NSString *)table;
-(BOOL)saveZones:(BOOL)withDevices;
-(BOOL)saveEvents:(BOOL)withActions;
-(BOOL)saveProfiles;
-(BOOL)saveTimers;
-(BOOL)saveAccounts;
-(void)saveAll;

-(BOOL)addZone:(Zone *)zone;
-(BOOL)insertZone:(Zone *)zone atIndex:(int)index;
-(void)removeZone:(Zone *)zone;

-(BOOL) addEvent:(Event *)event;
-(BOOL) insertEvent:(Event *)event atIndex:(int)index;
-(void) removeEvent:(Event *)event;
- (BOOL)isFreeEventSlots;
- (int)getFreeEventID;

-(BOOL) addTimer:(LTimer *)timer;
-(BOOL) insertTimer:(LTimer *)timer atIndex:(int)index;
-(void) removeTimer:(LTimer *)_timer;
- (BOOL)isFreeTimerSlots;

-(BOOL) addProfile:(Profile *)profile;
-(BOOL) insertProfile:(Profile *)profile atIndex:(int)index;
-(void) removeProfile:(Profile *)profile;

-(BOOL) addAccount:(Account *)account;
-(BOOL) insertAccount:(Account *)account atIndex:(int)index;
-(void) removeAccount:(Account *)account;
- (Account *)getAccountWithEmail:(NSString *)_email;
- (Account *)getAccountWithID:(NSInteger)_id;

- (Zone *)hasHeating;
- (Event *)getEventByName:(NSString *)_name;
- (Event *)getEventByID:(int)eventID;
- (Device *)getDeviceFromHardwareKey:(NSString *)hwKey;
- (Zone*)getZoneByID:(int)zoneID;
- (LTimer *)getTimerByName:(NSString *)_timerName;
- (LTimer *)getTimerByEventName:(NSString *)_eventName;

- (NSMutableArray *)getProfilesForZoneID:(int)zoneID;
- (NSMutableArray *)getProfilesForType:(int)type;
- (NSMutableArray *)getProfilesForZoneID:(int)zoneID type:(int)type;

@end
