//
//  Zone.h
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 21/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Mood;
@class Device;

extern int const MAX_MOODS;
extern int const MAX_PRODUCTS;

@interface Zone : NSObject{
    NSMutableArray *__devices;
}

@property (nonatomic) int zoneID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic, readwrite) NSMutableArray *devices;

-(id) initWithID:(int) zoneID name:(NSString *) name devices:(NSMutableArray *) devices;
-(id) initWithID:(int)zoneID name:(NSString *)name;
-(id) initWithName:(NSString *)name;
-(BOOL) hasHeating;
-(BOOL) hasAllOff;
-(BOOL) addAllOff;
-(BOOL) addMood:(int)moodID;
-(BOOL)isFreeDeviceSlots;
-(BOOL)isFreeDeviceOnlySlots;
-(void) addDevice:(Device *) device andSave:(BOOL)save;
-(BOOL) ensureUniqueDeviceNames;
-(NSMutableArray *) getDevicesForType:(NSString *) type;
-(Device *) getDeviceByCommand:(NSString *)_command;
-(Device *) getDeviceByID:(int) deviceID;
-(Device *) getDeviceByMoodID:(int) moodID;
-(Mood *) getMoodByID:(int) moodID;
-(void) removeDevice:(Device *) device;
-(void) moveDeviceUp:(Device *) device;
-(int) getDeviceCount;
-(int)getFreeDeviceID;
-(NSDictionary *) toDictionary;
-(NSData *) toJSON;
-(NSString *)toString;
-(NSMutableArray *)getMoods;
-(NSMutableArray *)getDevicesFromDB;
-(void)saveDevicesToDB;
- (NSMutableArray *)unpairedDevices;
- (NSUInteger)numUnpairedDevices;
@end
