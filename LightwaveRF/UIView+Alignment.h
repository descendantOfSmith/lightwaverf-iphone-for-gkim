//
//  UIView+Alignment.h
//  SSUILibrary
//
//  Created by James Munro on 26/07/2011.
//  Copyright 2011 Sync Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

/** A selection of convenient routines designed to dynamic view layouts.
 
 The routines in this category are designed to facilitate making dynamic
 layouts easier through code. Rather than having to manually position elements
 you can essentially set them relative to existing views with a given padding amount
 without having to manually calculate the offset based on each view's dimensions.
 */
@interface UIView (Alignment)

/** Aligns the view to to the left of a specified view, taking into account width.
 @param view View to align to the left of.
 @param padding Optional padding.
 */
- (void)alignToLeftOfView:(UIView *)view padding:(CGFloat)padding;

/** Aligns the view to to the left of a specified view, taking into account width.
 @param view View to align to the left of.
 @param padding Optional padding.
 @param matchVertical Whether the destination view should inherit the source view's vertical origin.
 */
- (void)alignToLeftOfView:(UIView *)view padding:(CGFloat)padding matchVertical:(BOOL)matchVertical;

/** Aligns the view to to the right of a specified view, taking into account width.
 @param view View to align to the right of.
 @param padding Optional padding.
 */
- (void)alignToRightOfView:(UIView *)view padding:(CGFloat)padding;

/** Aligns the view to to the right of a specified view, taking into account width.
 @param view View to align to the right of.
 @param padding Optional padding.
 @param matchVertical Whether the destination view should inherit the source view's vertical origin.
 */
- (void)alignToRightOfView:(UIView *)view padding:(CGFloat)padding matchVertical:(BOOL)matchVertical;

/** Aligns the view to sit above the specified view, taking into account height.
 @param view View to align above of.
 @param padding Optional padding.
 */
- (void)alignToTopOfView:(UIView *)view padding:(CGFloat)padding;

/** Aligns the view to sit above the specified view, taking into account height.
 @param view View to align above of.
 @param padding Optional padding.
 @param matchHorizontal Whether the destination view should inherit the source view's horizontal origin.
 */
- (void)alignToTopOfView:(UIView *)view padding:(CGFloat)padding matchHorizontal:(BOOL)matchHorizontal;

/** Aligns the view to sit below the specified view, taking into account height.
 @param view View to align below.
 @param padding Optional padding.
 */
- (void)alignToBottomOfView:(UIView *)view padding:(CGFloat)padding;

/** Aligns the view to sit below the specified view, taking into account height.
 @param view View to align below.
 @param padding Optional padding.
 @param matchHorizontal Whether the destination view should inherit the source view's horizontal origin.
 */
- (void)alignToBottomOfView:(UIView *)view padding:(CGFloat)padding matchHorizontal:(BOOL)matchHorizontal;

/** Aligns the view to to the left of a specified point, taking into account width.
 @param point Point to align to the left of.
 @param padding Optional padding.
 */
- (void)alignToLeftOfPoint:(CGPoint)point padding:(CGFloat)padding;

/** Aligns the view to to the left of a specified point, taking into account width.
 @param point Point to align to the left of.
 @param padding Optional padding.
 @param matchVertical Whether the destination view should inherit the source point's vertical origin.
 */
- (void)alignToLeftOfPoint:(CGPoint)point padding:(CGFloat)padding matchVertical:(BOOL)matchVertical;

/** Aligns the view to to the right of a specified point, taking into account width.
 @param point Point to align to the right of.
 @param padding Optional padding.
 */
- (void)alignToRightOfPoint:(CGPoint)point padding:(CGFloat)padding;

/** Aligns the view to to the right of a specified point, taking into account width.
 @param point Point to align to the right of.
 @param padding Optional padding.
 @param matchVertical Whether the destination view should inherit the source point's vertical origin.
 */
- (void)alignToRightOfPoint:(CGPoint)point padding:(CGFloat)padding matchVertical:(BOOL)matchVertical;

/** Aligns the view to sit above the specified point, taking into account height.
 @param point Point to align above of.
 @param padding Optional padding.
 */
- (void)alignToTopOfPoint:(CGPoint)point padding:(CGFloat)padding;

/** Aligns the view to sit above the specified point, taking into account height.
 @param point Point to align above of.
 @param padding Optional padding.
 @param matchHorizontal Whether the destination view should inherit the source point's horizontal origin.
 */
- (void)alignToTopOfPoint:(CGPoint)point padding:(CGFloat)padding matchHorizontal:(BOOL)matchHorizontal;

/** Aligns the view to sit below the specified point, taking into account height.
 @param point Point to align below.
 @param padding Optional padding.
 */
- (void)alignToBottomOfPoint:(CGPoint)point padding:(CGFloat)padding;

/** Aligns the view to sit below the specified point, taking into account height.
 @param point Point to align below.
 @param padding Optional padding.
 @param matchHorizontal Whether the destination view should inherit the source point's horizontal origin.
 */
- (void)alignToBottomOfPoint:(CGPoint)point padding:(CGFloat)padding matchHorizontal:(BOOL)matchHorizontal;

@end
