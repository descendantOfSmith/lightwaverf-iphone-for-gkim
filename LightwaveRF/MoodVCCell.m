//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import "MoodVCCell.h"
#import "ButtonsHelper.h"
#import "WFLHelper.h"
#import "ViewBuilderHelper.h"

@interface MoodVCCell ()
@property (nonatomic, strong) UIButton *startMoodButton;
@end

@implementation MoodVCCell

@synthesize mood;
@synthesize name_lbl;
@synthesize icon_img;
@synthesize active_img;
@synthesize fadeActive_tmr;
@synthesize zoneID;
@synthesize startMoodButton;

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    icon_img.frame = CGRectMake(10,0,24,24);
    [icon_img setContentMode:UIViewContentModeScaleAspectFit];
    [icon_img setCenterY:self.contentView.center.y];
    [name_lbl setCenterY:self.contentView.center.y];
    
}

- (void)dealloc {
    if (fadeActive_tmr != nil){
        [fadeActive_tmr invalidate];
    }
}

+ (float)getCellHeight {
    return 44.0f;
}

- (void)setupCellWithMood:(Mood *)_mood zoneID:(int)_zoneID {
    
    self.mood = _mood;
    self.zoneID = _zoneID;
    
    NSString *_titleStr = nil;
    float _tmpWidth = 0.0f;
    
    if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO]) {
        if ([mood isAllOff]) {
            _titleStr = [NSLocalizedString(@"all_off", @"All off") uppercaseString];
            _tmpWidth = 66.0f;
        } else {
            _titleStr = [NSLocalizedString(@"start_mood", @"START MOOD") uppercaseString];
            _tmpWidth = 86.0f;
        }
    } else {
        _titleStr = [NSLocalizedString(@"start_mood", @"START MOOD") uppercaseString];
        _tmpWidth = 86.0f;
    }
    
    if (!self.startMoodButton) {
        self.startMoodButton = [ButtonsHelper getButtonWithText:_titleStr
                                                        type:kButtonGreenPSwitch
                                                         tag:0
                                                       width:_tmpWidth
                                                      height:[ButtonsHelper getIdealHeightForType:kButtonGreenPSwitch]
                                                      target:self
                                                    selector:@selector(setMoodPressed:)];
        [self.contentView addSubview:self.startMoodButton];
        
        self.active_img = [ViewBuilderHelper getImageViewWithFilename:@"active_blue.png" atX:0.0f atY:0.0f];
        [self.active_img setFrameWidth:self.startMoodButton.frame.size.width - 8.0f];
        [self.active_img setFrameX:(self.startMoodButton.frame.size.width - self.active_img.frame.size.width) / 2];
        [self.active_img setFrameY:(self.startMoodButton.frame.size.height - self.active_img.frame.size.height) - 4.0f];
        [self.startMoodButton addSubview:self.active_img];
        self.active_img.hidden = YES;
    }
    
    [self.startMoodButton setTitle:_titleStr forState:UIControlStateNormal];
    [self.startMoodButton setFrameWidth:_tmpWidth];
    [self.startMoodButton setFrameOrigin:CGPointMake((SCREEN_WIDTH() - _tmpWidth) - 9.0f, 7)];
    
    if (self.mood!=nil){
        [self.name_lbl setText:self.mood.name];
        switch(self.mood.moodID){
            case 2:
                [self.icon_img setImage:[UIImage imageNamed:@"icon_mood2"]];
                break;
            case 3:
                [self.icon_img setImage:[UIImage imageNamed:@"icon_mood3"]];
                break;
            case 4:
                [self.icon_img setImage:[UIImage imageNamed:@"icon_mood4"]];
                break;
        }
    }
    
    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self positionViewsIsEditing:self.isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

- (IBAction)setMoodPressed:(id)sender {
    DebugLog(@"Set Mood: %@", mood);
    
    [[WFLHelper shared] sendToUDP:YES
                          command:[UDPService getMoodCommandWithZoneID:self.zoneID mood:mood]
                              tag:kRequestMood];
    
    active_img.hidden = NO;
    active_img.alpha = 1.0;
    if (fadeActive_tmr == nil){
        fadeActive_tmr = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(fadeActive:) userInfo:nil repeats:YES  ];
    }
}

- (void)fadeActive:(NSTimer *)timer{
    active_img.alpha -= 0.1;
    if (active_img.alpha<=0.0){
        [fadeActive_tmr invalidate];
        fadeActive_tmr = nil;
    }
}

@end
