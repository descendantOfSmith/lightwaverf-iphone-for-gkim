//
//  EventDevicesVC.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "AddTimerVC.h"
#import "Home.h"
#import "Zone.h"
#import "Device.h"
#import "LTimer.h"
#import "WFLHelper.h"
#import "Toast+UIView.h"
#import "AppDelegate.h"
#import "ColourLEDsVC.h"
#import "ButtonsHelper.h"

@interface AddTimerVC ()
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableSet *selectedCellsSet;                       // Cell is ticked/selected or not
@property (nonatomic, strong) NSMutableSet *selectedOnOffSwitchCellsSet;            // On/Off selected
@property (nonatomic, strong) NSMutableDictionary *selectedOpenCloseDict;           // Open/Close/Stopped selected
@property (nonatomic, strong) NSMutableDictionary *selectedBrightnessDict;          // Brightness value
@end

@implementation AddTimerVC

@synthesize delegate;
@synthesize tableView;
@synthesize selectedCellsSet, selectedOnOffSwitchCellsSet, selectedOpenCloseDict, selectedBrightnessDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.selectedCellsSet = [[NSMutableSet alloc] init];
        self.selectedOnOffSwitchCellsSet = [[NSMutableSet alloc] init];
        self.selectedOpenCloseDict = [[NSMutableDictionary alloc] init];
        self.selectedBrightnessDict = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Creation

- (void)setupUI {
    [self setupNavBar];
    [self setupTableView];
}

- (void)setupNavBar {
    self.navigationItem.title = NSLocalizedString(@"add_timer", @"Add Timer");
    
    [self setupLeftNavBarButton];
    [self setupRightNavBarButton];
}

- (void)setupLeftNavBarButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"cancel", @"Cancel")
                                                       type:kButtonArrowLeft
                                                        tag:kBtnBack
                                                      width:55
                                                     height:18
                                                     target:self
                                                   selector:@selector(buttonPressed:)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        [backButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backButton;
        
    } else {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self
                                                                      action:@selector(buttonPressed:)];
        [backButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backButton;
    }
}

- (void)setupRightNavBarButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"done", nil)
                                                       type:kButtonArrowRight
                                                        tag:kBtnDone
                                                      width:45
                                                     height:18
                                                     target:self
                                                   selector:@selector(buttonPressed:)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithCustomView:button];
        [tmpTopRightBtn setTag:kBtnDone];
        self.navigationItem.rightBarButtonItem = tmpTopRightBtn;

    } else {
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                        target:self
                                                                                        action:@selector(buttonPressed:)];
        [tmpTopRightBtn setTag:kBtnDone];
        self.navigationItem.rightBarButtonItem = tmpTopRightBtn;
    }
}

- (void)setupTableView {
    self.tableView = [ViewBuilderHelper getTableViewWithWidth:self.scrollView.frame.size.width
                                                       height:self.scrollView.frame.size.height
                                                        style:UITableViewStylePlain
                                                       target:self];
    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView setFrameY:self.heightOffset];
    [self.scrollView addSubview:self.tableView];
    self.heightOffset += self.tableView.frame.size.height;
    [self.tableView reloadData];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Private

- (Zone *)getZoneForSection:(NSInteger)section {
    Home *home = [Home shared];
    Zone *zone = [[home zones] objectAtIndex:section];
    return zone;
}

- (Device *)getDeviceForIndexPath:(NSIndexPath *)indexPath {
    Zone *zone = [self getZoneForSection:indexPath.section - 1];
    Device *device = [zone.devices objectAtIndex:indexPath.row];
    return device;
}

- (NSString *)getKeyForIndexPath:(NSIndexPath *)indexPath {
    return [NSString stringWithFormat:@"%d-%d", indexPath.section, indexPath.row];
}

- (float)getBrightnessPercentageForIndexPath:(NSIndexPath *)indexPath {
    NSNumber *_brightness = [self.selectedBrightnessDict objectForKey:[self getKeyForIndexPath:indexPath]];
    if (_brightness)
        return [_brightness floatValue];
    else
        return 0.0f;
        //return DIMMER_MAX / 2;
}

- (ConstantsIsOpenClose)getIsOpenCloseForIndexPath:(NSIndexPath *)indexPath {
    NSNumber *_isOpenClose = [self.selectedOpenCloseDict objectForKey:[self getKeyForIndexPath:indexPath]];
    if (_isOpenClose)
        return [_isOpenClose intValue];
    else
        return kConstantClose;
}

- (void)setBrightnessPercentage:(float)_brightnessPercentage
                   forIndexPath:(NSIndexPath *)_indexPath
                 showAlertPopup:(BOOL)_showAlert {
    
    [self.selectedBrightnessDict setValue:[NSNumber numberWithFloat:_brightnessPercentage]
                                   forKey:[self getKeyForIndexPath:_indexPath]];
    
    if (_showAlert) {
        [kApplicationDelegate.window makeToast:[NSString stringWithFormat:@"%.0f%%", _brightnessPercentage]
                                      duration:3.0f
                                      position:@"center"];
    }
}

#pragma mark - ChooseProductOnOffCell delegate

- (void)chooseProductSwitch:(ChooseProductOnOffCell *)_ChooseProductOnOffCell indexPath:(NSIndexPath *)_indexPath isOn:(BOOL)_isOn {
    if (_isOn) {
        [self.selectedOnOffSwitchCellsSet addObject:_indexPath];
        
        if ([_ChooseProductOnOffCell.device isDimmer]) {
            [self setBrightnessPercentage:100.0f
                   forIndexPath:_indexPath
                           showAlertPopup:YES];
        }
        
    } else {
        [self.selectedOnOffSwitchCellsSet removeObject:_indexPath];
        
        if ([_ChooseProductOnOffCell.device isDimmer]) {
            [self setBrightnessPercentage:0
                             forIndexPath:_indexPath
                           showAlertPopup:YES];
        }
    }
    
    [self.tableView reloadData];
}

- (void)chooseProductSwitch:(ChooseProductOnOffCell *)_ChooseProductOnOffCell
                  indexPath:(NSIndexPath *)_indexPath
                 percentage:(float)_percentage {
    
    if (_percentage == 0.0f) {
        [self.selectedOnOffSwitchCellsSet removeObject:_indexPath];
        
    } else {
        [self.selectedOnOffSwitchCellsSet addObject:_indexPath];
    }
    
    [self setBrightnessPercentage:_percentage
                     forIndexPath:_indexPath
                   showAlertPopup:NO];
    
    [self.tableView reloadData];
}

- (void)chooseProductButtonPressed:(ChooseProductOnOffCell *)_ChooseProductOnOffCell
                         indexPath:(NSIndexPath *)_indexPath
                               tag:(NSUInteger)_tag {
    
    ColourLEDsVC *c = [[ColourLEDsVC alloc] initWithNibName:kVCXibName
                                                     bundle:nil
                                                     device:[self getDeviceForIndexPath:_indexPath]
                                                       zone:[self getZoneForSection:_indexPath.section]];
    [c setShowChangeCyclingButton:NO];
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:c];
    [self presentViewController:nc animated:YES completion:^(void){
        c.isModal = YES;
    }];
}

#pragma mark - ChooseProductOpenCloseCell delegate

- (void)chooseProductOpenClose:(ChooseProductOpenCloseCell *)_chooseProductOpenCloseCell
                     indexPath:(NSIndexPath *)_indexPath
                   isOpenClose:(ConstantsIsOpenClose)_isOpenClose {
        
    [self.selectedOpenCloseDict setValue:[NSNumber numberWithInt:_isOpenClose]
                                  forKey:[self getKeyForIndexPath:_indexPath]];
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tmpTableView {
    Home *home = [Home shared];
    return [home zones].count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    if (section == 0) {
        Home *home = [Home shared];
        return home.events.count;
    } else {
        Zone *zone = [self getZoneForSection:section - 1];
        return zone.devices.count;
    }
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    if (indexPath.section == 0) {
        return [ChooseProductCell getCellHeight];
        
    } else {
        Device *device = [self getDeviceForIndexPath:indexPath];
        
        if ([device isSocket])
            return [ChooseProductOnOffCell getCellHeight];
        
        else if ([device isDimmer])
            return [ChooseProductSliderCell getCellHeight];
        
        else if ([device isColourLEDS])
            return [ChooseProductLEDCell getCellHeight];
                
        else if ([device isOpenClose])
            return [ChooseProductOpenCloseCell getCellHeight];
        
        else
            return [ChooseProductCell getCellHeight];
    }
}

/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return NSLocalizedString(@"events", @"Events");
        
    } else {
        Zone *zone = [self getZoneForSection:section - 1];
        return zone.name;
    }
}
 */

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 22.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                              0.0f,
                                                              self.tableView.frame.size.width,
                                                              [self tableView:self.tableView heightForHeaderInSection:section])];
    [header setBackgroundColor:[UIColor colorWithDivisionOfRed:230 green:230 blue:230 alpha:1.0f]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor= [UIColor clearColor];
    label.textColor = [AppStyleHelper isMegaMan] ? [UIColor colorWithDivisionOfRed:137 green:197 blue:39 alpha:1.0f] : [UIColor blackColor];
    label.font = [UIFont boldSystemFontOfSize:13.0];
    
    if (section == 0) {
        label.text = NSLocalizedString(@"events", @"Events");
        
    } else {
        Zone *zone = [self getZoneForSection:section - 1];
        label.text = zone.name;
    }
    
    float padding = 5.0f;
    [label setFrameX:5.0f];
    [label setFrameHeight:header.frame.size.height];
    [label setFrameWidth:header.frame.size.width - (padding * 2)];
    [header addSubview:label];
    
    return header;
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // vars
    const char* className = class_getName([self class]);
    NSString *cellIdentifier = nil;
    BOOL _isSelected = [self.selectedCellsSet containsObject:indexPath];
    
    if (indexPath.section == 0) {
        /*****************************************
         * DEFAULT CELL
         *****************************************/
        cellIdentifier = [NSString stringWithFormat:@"ChooseProductCell-%s", className];
        ChooseProductCell *cell = (ChooseProductCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell = [self setupInitialCell:cell xibName:@"ChooseProductCell"];
        
        Home *home = [Home shared];
        Event *_event = [home.events objectAtIndex:indexPath.row];
        
        // Setup Cell
        [cell setupCellWithTitle:_event.name
                          device:nil
                        selected:_isSelected];
        
        return cell;
        
    } else {
        // Get Object
        Device *device = [self getDeviceForIndexPath:indexPath];
  
        // Setup Cells
        if ([device isSocket]) {
            /*****************************************
             * ON | OFF CELL
             *****************************************/
            cellIdentifier = [NSString stringWithFormat:@"ChooseProductOnOffCell-%s", className];
            ChooseProductOnOffCell *cell = (ChooseProductOnOffCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell = [self setupInitialCell:cell xibName:@"ChooseProductOnOffCell"];
            
            // Setup Cell
            [cell setupCellWithTitle:device.name
                              device:device
                            selected:_isSelected
                                isOn:[self.selectedOnOffSwitchCellsSet containsObject:indexPath]
                           indexPath:indexPath
                            delegate:self];
            return cell;
            
        } else if ([device isDimmer]) {
            /*****************************************
             * BRIGHTNESS SLIDER CELL
             *****************************************/
            cellIdentifier = [NSString stringWithFormat:@"ChooseProductSliderCell-%s", className];
            ChooseProductSliderCell *cell = (ChooseProductSliderCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell = [self setupInitialCell:cell xibName:@"ChooseProductSliderCell"];
            
            // Setup Cell
            [cell setupCellWithTitle:device.name
                              device:device
                            selected:_isSelected
                                isOn:[self.selectedOnOffSwitchCellsSet containsObject:indexPath]
                           indexPath:indexPath
                            delegate:self
                          percentage:[self getBrightnessPercentageForIndexPath:indexPath]];
            return cell;
        
        } else if ([device isColourLEDS]) {
            /*****************************************
             * COLOUR LEDS SLIDER CELL
             *****************************************/
            cellIdentifier = [NSString stringWithFormat:@"ChooseProductLEDCell-%s", className];
            ChooseProductLEDCell *cell = (ChooseProductLEDCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell = [self setupInitialCell:cell xibName:@"ChooseProductLEDCell"];
            
            // Setup Cell
            [cell setupLEDCellWithTitle:device.name
                                 device:device
                               selected:_isSelected
                                   isOn:[self.selectedOnOffSwitchCellsSet containsObject:indexPath]
                              indexPath:indexPath
                               delegate:self
                             percentage:[self getBrightnessPercentageForIndexPath:indexPath]];
            return cell;
            
        } else if ([device isOpenClose]) {
            /*****************************************
             * STOP | CLOSE | OPEN CELL
             *****************************************/
            cellIdentifier = [NSString stringWithFormat:@"ChooseProductOpenCloseCell-%s", className];
            ChooseProductOpenCloseCell *cell = (ChooseProductOpenCloseCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell = [self setupInitialCell:cell xibName:@"ChooseProductOpenCloseCell"];
            
            // Setup Cell
            [cell setupCellWithTitle:device.name
                              device:device
                            selected:_isSelected
                         isOpenClose:[self getIsOpenCloseForIndexPath:indexPath]
                           indexPath:indexPath
                            delegate:self];
            
            return cell;
            
        } else if ([device isMood]) {
            /*****************************************
             * DEFAULT CELL
             *****************************************/
            
             cellIdentifier = [NSString stringWithFormat:@"ChooseProductCell-%s", className];
             ChooseProductCell *cell = (ChooseProductCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
             cell = [self setupInitialCell:cell xibName:@"ChooseProductCell"];
             
             // Setup Cell
            [cell setupCellWithTitle:device.name
                              device:device
                            selected:_isSelected];
             return cell;            
            
        } else {
            /*****************************************
             * ON | OFF CELL
             *****************************************/
            cellIdentifier = [NSString stringWithFormat:@"ChooseProductOnOffCell-%s", className];
            ChooseProductOnOffCell *cell = (ChooseProductOnOffCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell = [self setupInitialCell:cell xibName:@"ChooseProductOnOffCell"];
            
            // Setup Cell
            [cell setupCellWithTitle:device.name
                              device:device
                            selected:_isSelected
                                isOn:[self.selectedOnOffSwitchCellsSet containsObject:indexPath]
                           indexPath:indexPath
                            delegate:self];
            return cell;
        }
    }
}

- (id)setupInitialCell:(ChooseProductCell *)_cell xibName:(NSString *)_xib {
    if (_cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:_xib owner:self options:nil];
        _cell = [topLevelObjects objectAtIndex:0];
        [_cell setFrameWidth:self.tableView.frame.size.width];
        [_cell.contentView setFrameWidth:self.tableView.frame.size.width];
        [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return _cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.selectedCellsSet containsObject:indexPath])
        [self.selectedCellsSet removeObject:indexPath];
    else {
        [self.selectedCellsSet removeAllObjects];
        [self.selectedCellsSet addObject:indexPath];
    }
    
    [self.tableView reloadData];
}

#pragma mark - Actions

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnBack:
        {
            [self dismissViewAnimated:YES];
        }
            break;
            
        case kBtnDone:
        {
            Home *home = [Home shared];
            NSString *_timerName = [LTimer generateTimerName];
            LTimer *_newTimer = nil;
            
            for (NSIndexPath *_indexPath in self.selectedCellsSet) {
             
                NSString *_sequenceName = nil;
                
                if (_indexPath.section == 0) {
                    
                    /**********************
                     * EVENT SELECTED
                     **********************/
                    
                    Event *_event = [home.events objectAtIndex:_indexPath.row];
                    _sequenceName = _event.name;
                    
                    // Save
                    _event.timerName = _timerName;
                    [home saveEvents:YES]; //YES refers to saving the actions with the events
                    
                } else {
                    
                    /**********************
                     * TIMER SELECTED
                     **********************/
                    
                    Device *_device = [self getDeviceForIndexPath:_indexPath];
                    //DebugLog(@"Device: %@", _device.name);
                    //DebugLog(@"Hardware Key: %@", _device.hardwareKey);
                    //DebugLog(@"zone name: %@", _device.zone.name);
                    
                    // Get Device Setting
                    ConstantsIsOpenClose _isOpenClose = [self getIsOpenCloseForIndexPath:_indexPath];
                    BOOL _isOnOrOpen = [self.selectedOnOffSwitchCellsSet containsObject:_indexPath];
                    float _brightnessPercentage = [self getBrightnessPercentageForIndexPath:_indexPath];
                    int _brightness = (int)((_brightnessPercentage / 100) * DIMMER_MAX);
                    
                    int _setting = [_device getSetting:_isOpenClose
                                            isOnOrOpen:_isOnOrOpen
                                            brightness:_brightness];
                    
                    //DebugLog(@"_setting: %d", _setting);
                    
                    Action *_action = [[Action alloc] initWithDevice:_device setting:_setting];
                    _sequenceName = _action.hwKey;
                    
                    //DebugLog(@"_sequenceName: %@", _sequenceName);
                    
                    if ([_sequenceName characterAtIndex:0] != '!')
                        _sequenceName = [NSString stringWithFormat:@"!%@", _sequenceName];
                    
                }
                
                // Build Command
                NSString *_command = [NSString stringWithFormat:@"!FiP\"%@\"=!FqP\"%@\"", _timerName, _sequenceName];
                //DebugLog(@"Command: %@", _command);
                _newTimer = [[LTimer alloc] initWithCommand:_command];
                [home addTimer:_newTimer];
                [home saveTimers];

                // Create on WFL
                [[WFLHelper shared] sendToUDP:YES
                                      command:[_newTimer getCommand]
                                          tag:kRequestCreateTimer];
                 
                break;
            }

            if (_newTimer) {
                [self dismissViewAnimated:NO];
                
                if ([self.delegate respondsToSelector:@selector(timerAdded:timer:)])
                    [self.delegate timerAdded:self timer:_newTimer];
            } else
                [self dismissViewAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
