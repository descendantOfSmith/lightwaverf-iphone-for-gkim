//
//  MonthActionSheet.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 02/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "MonthActionSheet.h"
#import "ButtonsHelper.h"

@interface MonthActionSheet ()
@property (nonatomic, strong) NSMutableSet *selectedMonths;
@property (nonatomic, strong) UIButton *janButton;
@property (nonatomic, strong) UIButton *febButton;
@property (nonatomic, strong) UIButton *marButton;
@property (nonatomic, strong) UIButton *aprButton;
@property (nonatomic, strong) UIButton *mayButton;
@property (nonatomic, strong) UIButton *junButton;
@property (nonatomic, strong) UIButton *julButton;
@property (nonatomic, strong) UIButton *augButton;
@property (nonatomic, strong) UIButton *sepButton;
@property (nonatomic, strong) UIButton *octButton;
@property (nonatomic, strong) UIButton *novButton;
@property (nonatomic, strong) UIButton *decButton;
@end

@implementation MonthActionSheet

@synthesize selectedMonths, janButton, febButton, marButton, aprButton, mayButton, junButton, julButton, augButton, sepButton, octButton, novButton, decButton;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
     selectedMonths:(NSString *)tmpSelectedMonths {
    
    [self setSelectedMonthsFromString:tmpSelectedMonths];
    return [self initWithPoint:point
                  buttonTitles:tmpButtonTitles
                   cancelTitle:tmpCancelTitle
                    dataSource:tmpDataSource
                      delegate:tmpDelegate
                showPickerType:kCustomActionSheetMonthPicker];
}

- (void)setSelectedMonthsFromString:(NSString *)_str {
    self.selectedMonths = [[NSMutableSet alloc] init];
    
    for(int i=kMonthActionSheetJan; i<=[_str length]; i++){
        if ([_str characterAtIndex:i-1] != 'x') {
            [self.selectedMonths addObject:[NSNumber numberWithInt:i]];
            
        }
    }
}

- (void)dealloc  {
    //DebugLog(@"DEALLOC");
}

- (void)setupUI {
    [self setBackgroundColor:[self getColourForType:kCustomActionSheetBackgroundColour]];
    
    // Top Highlight
    UIView *_topHighlightView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, 0.0f)];
    [_topHighlightView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_topHighlightView];
    
    // Top Header
    self.heightOffset = 0.0f;
    UIView *_topHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.heightOffset, self.frame.size.width, 265.0f)];
    [_topHeaderView setTag:-1];
    [_topHeaderView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_topHeaderView];
    self.heightOffset += _topHeaderView.frame.size.height;
    
    // Add Day Buttons
    [self addDayButtonsToView:_topHeaderView];
    
    // Select any preselected
    for (NSNumber *_num in self.selectedMonths) {
        UIButton *_button = [self getButtonFromView:_topHeaderView tag:[_num intValue]];
        if (_button)
            [_button setSelected:YES];
    }
    
    float tmpButtonPadding = 10.0f;
    UIButton *_allButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"all", @"All")
                                                          type:kButtonGreenBacking
                                                           tag:-1
                                                         width:self.frame.size.width - (tmpButtonPadding * 2)
                                                        height:28.0f
                                                        target:self
                                                      selector:@selector(allPressed:)];
    [_allButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_allButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0f]];
    [_allButton alignToBottomOfView:self.junButton padding:10.0f];
    [_allButton setFrameX:tmpButtonPadding];
    [_topHeaderView addSubview:_allButton];
    
    UIButton *_SpringButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"spring", @"Spring")
                                                    type:kButtonGreenBacking
                                                     tag:-1
                                                   width:75.0f
                                                  height:28.0f
                                                  target:self
                                                selector:@selector(springPressed:)];
    [_SpringButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_SpringButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0f]];
    [_SpringButton alignToBottomOfView:_allButton padding:5.0f];
    [_SpringButton setFrameX:7.0f];
    [_topHeaderView addSubview:_SpringButton];
    
    UIButton *_summerButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"summer", @"Summer")
                                                           type:kButtonGreenBacking
                                                            tag:-1
                                                          width:75.0f
                                                         height:28.0f
                                                         target:self
                                                       selector:@selector(summerPressed:)];
    [_summerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_summerButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0f]];
    [_summerButton alignToRightOfView:_SpringButton padding:2.0f matchVertical:YES];
    [_topHeaderView addSubview:_summerButton];
    
    UIButton *_autumnButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"autumn", @"Autumn")
                                                          type:kButtonGreenBacking
                                                           tag:-1
                                                         width:75.0f
                                                        height:28.0f
                                                        target:self
                                                      selector:@selector(autumnPressed:)];
    [_autumnButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_autumnButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0f]];
    [_autumnButton alignToRightOfView:_summerButton padding:2.0f matchVertical:YES];
    [_topHeaderView addSubview:_autumnButton];
    
    UIButton *_winterButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"winter", @"Winter")
                                                          type:kButtonGreenBacking
                                                           tag:-1
                                                         width:75.0f
                                                        height:28.0f
                                                        target:self
                                                      selector:@selector(winterPressed:)];
    [_winterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_winterButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0f]];
    [_winterButton alignToRightOfView:_autumnButton padding:2.0f matchVertical:YES];
    [_topHeaderView addSubview:_winterButton];
    
    // Set Highlight Height
    [_topHighlightView setFrameHeight:self.heightOffset + 1.0f];
    
    // Apply Highlight
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _topHighlightView.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[self getColourForType:kCustomActionSheetGradientStart] CGColor],
                       (id)[[self getColourForType:kCustomActionSheetGradientEnd] CGColor],
                       nil];
    [_topHighlightView.layer insertSublayer:gradient atIndex:0];
    
    self.heightOffset = (_winterButton.frame.origin.y + _winterButton.frame.size.height) + 10.0f;
    
    // Create Buttons
    NSUInteger tmpTag = 0;
    for (NSString *tmpTitle in self.buttonTitlesArray) {
        [self addButtonType:kCustomActionSheetButton title:tmpTitle tag:tmpTag];
        tmpTag += 1;
    }
    
    // Create Cancel Button
    self.heightOffset += lwActionSheetButtonCancelPadding;
    [self addButtonType:kCustomActionSheetCancelButton title:self.cancelTitle tag:tmpTag];
    
    self.heightOffset += lwActionSheetBottomPadding;
    [self setFrameHeight:self.heightOffset];
}

- (void)addDayButtonsToView:(UIView *)_view {
    self.janButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull1", @"January")
                                                 tag:kMonthActionSheetJan
                                            selected:NO
                                              toView:_view
                                            selector:@selector(monthButtonPressed:)];
    [self.janButton setFrameX:20.0f];
    [self.janButton setFrameY:10.0f];
    
    self.febButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull2", @"February")
                                                  tag:kMonthActionSheetFeb
                                             selected:NO
                                               toView:_view
                                             selector:@selector(monthButtonPressed:)];
    [self.febButton alignToBottomOfView:self.janButton padding:5.0f matchHorizontal:YES];
    
    self.marButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull3", @"March")
                                                    tag:kMonthActionSheetMar
                                               selected:NO
                                                 toView:_view
                                               selector:@selector(monthButtonPressed:)];
    [self.marButton alignToBottomOfView:self.febButton padding:5.0f matchHorizontal:YES];
    
    self.aprButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull4", @"April")
                                                   tag:kMonthActionSheetApr
                                              selected:NO
                                                toView:_view
                                              selector:@selector(monthButtonPressed:)];
    [self.aprButton alignToBottomOfView:self.marButton padding:5.0f matchHorizontal:YES];
    
    self.mayButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull5", @"May")
                                                   tag:kMonthActionSheetMay
                                              selected:NO
                                                toView:_view
                                              selector:@selector(monthButtonPressed:)];
    [self.mayButton alignToBottomOfView:self.aprButton padding:5.0f matchHorizontal:YES];
    
    self.junButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull6", @"June")
                                                   tag:kMonthActionSheetJun
                                              selected:NO
                                                toView:_view
                                              selector:@selector(monthButtonPressed:)];
    [self.junButton alignToBottomOfView:self.mayButton padding:5.0f matchHorizontal:YES];
    
    self.julButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull7", @"July")
                                                 tag:kMonthActionSheetJul
                                            selected:NO
                                              toView:_view
                                            selector:@selector(monthButtonPressed:)];
    [self.julButton alignToRightOfView:self.janButton padding:10.0f matchVertical:YES];
    
    self.augButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull8", @"August")
                                                   tag:kMonthActionSheetAug
                                              selected:NO
                                                toView:_view
                                              selector:@selector(monthButtonPressed:)];
    [self.augButton alignToBottomOfView:self.julButton padding:5.0f matchHorizontal:YES];
    
    self.sepButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull9", @"September")
                                                 tag:kMonthActionSheetSep
                                            selected:NO
                                              toView:_view
                                            selector:@selector(monthButtonPressed:)];
    [self.sepButton alignToBottomOfView:self.augButton padding:5.0f matchHorizontal:YES];
    
    self.octButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull10", @"October")
                                                 tag:kMonthActionSheetOct
                                            selected:NO
                                              toView:_view
                                            selector:@selector(monthButtonPressed:)];
    [self.octButton alignToBottomOfView:self.sepButton padding:5.0f matchHorizontal:YES];
    
    self.novButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull11", @"November")
                                                   tag:kMonthActionSheetNov
                                              selected:NO
                                                toView:_view
                                              selector:@selector(monthButtonPressed:)];
    [self.novButton alignToBottomOfView:self.octButton padding:5.0f matchHorizontal:YES];
    
    self.decButton = [self addRadioButtonWithText:NSLocalizedString(@"monthFull12", @"December")
                                                 tag:kMonthActionSheetDec
                                            selected:NO
                                              toView:_view
                                            selector:@selector(monthButtonPressed:)];
    [self.decButton alignToBottomOfView:self.novButton padding:5.0f matchHorizontal:YES];
}

- (UIButton *)addRadioButtonWithText:(NSString *)_text
                                 tag:(NSUInteger)_bTag
                            selected:(BOOL)_selected
                              toView:(UIView *)_view
                            selector:(SEL)_selector {
    
    UIButton *_button = [ButtonsHelper getButtonWithText:_text
                                                    type:kButtonRadio
                                                     tag:_bTag
                                                   width:130.0f
                                                  height:26.0f
                                                  target:self
                                                selector:_selector];
    
    if ([AppStyleHelper isMegaMan]) {
        [_button setBackgroundImage:nil forState:UIControlStateNormal];
        [_button setBackgroundImage:nil forState:UIControlStateHighlighted];
        [_button setBackgroundImage:nil forState:UIControlStateSelected];
        
        [_button setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateLo] forState:UIControlStateNormal];
        [_button setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateHighlighted];
        [_button setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateSelected];
    }
    
    [_button setSelected:_selected];
    [_view addSubview:_button];
    return _button;
}

- (UIButton *)getButtonFromView:(UIView *)_searchView tag:(NSUInteger)_bTag {
    return (UIButton *)[_searchView viewWithTag:_bTag];
}

- (void)setSelected:(UIButton *)_button {
    if (_button.selected)
        [self.selectedMonths addObject:[NSNumber numberWithInt:_button.tag]];
    else
        [self.selectedMonths removeObject:[NSNumber numberWithInt:_button.tag]];
}

#pragma mark - Actions

- (IBAction)monthButtonPressed:(id)sender {
    UIButton *_button = (UIButton *)sender;
    [_button setSelected:!_button.selected];
    [self setSelected:_button];
}

- (IBAction)allPressed:(id)sender {
    [self.janButton setSelected:YES];
    [self.febButton setSelected:YES];
    [self.marButton setSelected:YES];
    [self.aprButton setSelected:YES];
    [self.mayButton setSelected:YES];
    [self.junButton setSelected:YES];
    [self.julButton setSelected:YES];
    [self.augButton setSelected:YES];
    [self.sepButton setSelected:YES];
    [self.octButton setSelected:YES];
    [self.novButton setSelected:YES];
    [self.decButton setSelected:YES];
    
    [self updateSelectedButtons];
}

- (IBAction)springPressed:(id)sender {
    [self.janButton setSelected:NO];
    [self.febButton setSelected:NO];
    [self.marButton setSelected:YES];
    [self.aprButton setSelected:YES];
    [self.mayButton setSelected:YES];
    [self.junButton setSelected:NO];
    [self.julButton setSelected:NO];
    [self.augButton setSelected:NO];
    [self.sepButton setSelected:NO];
    [self.octButton setSelected:NO];
    [self.novButton setSelected:NO];
    [self.decButton setSelected:NO];
    
    [self updateSelectedButtons];
}

- (IBAction)summerPressed:(id)sender {
    [self.janButton setSelected:NO];
    [self.febButton setSelected:NO];
    [self.marButton setSelected:NO];
    [self.aprButton setSelected:NO];
    [self.mayButton setSelected:NO];
    [self.junButton setSelected:YES];
    [self.julButton setSelected:YES];
    [self.augButton setSelected:YES];
    [self.sepButton setSelected:NO];
    [self.octButton setSelected:NO];
    [self.novButton setSelected:NO];
    [self.decButton setSelected:NO];
    
    [self updateSelectedButtons];
}

- (IBAction)autumnPressed:(id)sender {
    [self.janButton setSelected:NO];
    [self.febButton setSelected:NO];
    [self.marButton setSelected:NO];
    [self.aprButton setSelected:NO];
    [self.mayButton setSelected:NO];
    [self.junButton setSelected:NO];
    [self.julButton setSelected:NO];
    [self.augButton setSelected:NO];
    [self.sepButton setSelected:YES];
    [self.octButton setSelected:YES];
    [self.novButton setSelected:YES];
    [self.decButton setSelected:NO];
    
    [self updateSelectedButtons];
}

- (IBAction)winterPressed:(id)sender {
    [self.janButton setSelected:YES];
    [self.febButton setSelected:YES];
    [self.marButton setSelected:NO];
    [self.aprButton setSelected:NO];
    [self.mayButton setSelected:NO];
    [self.junButton setSelected:NO];
    [self.julButton setSelected:NO];
    [self.augButton setSelected:NO];
    [self.sepButton setSelected:NO];
    [self.octButton setSelected:NO];
    [self.novButton setSelected:NO];
    [self.decButton setSelected:YES];
    
    [self updateSelectedButtons];
}

- (void)updateSelectedButtons {
    [self setSelected:self.janButton];
    [self setSelected:self.febButton];
    [self setSelected:self.marButton];
    [self setSelected:self.aprButton];
    [self setSelected:self.mayButton];
    [self setSelected:self.junButton];
    [self setSelected:self.julButton];
    [self setSelected:self.augButton];
    [self setSelected:self.sepButton];
    [self setSelected:self.octButton];
    [self setSelected:self.novButton];
    [self setSelected:self.decButton];
}

- (NSString *)getSelectedMonths {
    return [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@",
            self.janButton.selected ? @"j" : @"x",
            self.febButton.selected ? @"f" : @"x",
            self.marButton.selected ? @"m" : @"x",
            self.aprButton.selected ? @"a" : @"x",
            self.mayButton.selected ? @"m" : @"x",
            self.junButton.selected ? @"j" : @"x",
            self.julButton.selected ? @"j" : @"x",
            self.augButton.selected ? @"a" : @"x",
            self.sepButton.selected ? @"s" : @"x",
            self.octButton.selected ? @"o" : @"x",
            self.novButton.selected ? @"n" : @"x",
            self.decButton.selected ? @"d" : @"x"];
}

// Override super
- (IBAction)buttonPressed:(id)sender {
    if (self.showPickerType == kCustomActionSheetMonthPicker) {
        if ([self.delegate respondsToSelector:@selector(actionSheetButtonPressed:index:selectedString:)])
            [self.delegate actionSheetButtonPressed:self
                                              index:[sender tag]
                                     selectedString:[self getSelectedMonths]];
    }
}

@end
