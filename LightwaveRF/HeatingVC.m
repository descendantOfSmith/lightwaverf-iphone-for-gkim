//
//  HeatingVC.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 06/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "HeatingVC.h"
#import "ButtonsHelper.h"
#import "Constants.h"
#import "Home.h"
#import "Toast+UIView.h"

@interface HeatingVC ()
@property (nonatomic, strong) Zone *zone;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *weekdayProfilesArray;
@property (nonatomic, strong) NSMutableArray *weekendProfilesArray;
@property (nonatomic, strong) NSMutableArray *holidayProfilesArray;
@property (nonatomic, strong) LightwaveActionSheet *optionsActionSheet;
@property (nonatomic, strong) DateTimeActionSheet *timeActionSheet;
@property (nonatomic, strong) DateTimeActionSheet *dateActionSheet;
@property (nonatomic, strong) TempActionSheet *tempActionSheet;
@property (nonatomic, strong) Profile *editingProfile;
@property (nonatomic, strong) Profile *addingProfile;
@end

#define kTopHeadingBarHeight 35.0f

@implementation HeatingVC

@synthesize tableView, zone;
@synthesize weekdayProfilesArray, weekendProfilesArray, holidayProfilesArray;
@synthesize optionsActionSheet, timeActionSheet, tempActionSheet, dateActionSheet;
@synthesize editingProfile, addingProfile;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil zone:(Zone *)_zone
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.zone = _zone;
        [self refreshGetProfiles];
        [self refreshAddProfile];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Creation

- (void)setupUI {
    [self setupNavBar];
    [self setupTopBarButtons];
    [self setupTableView];
    [self setupAddProfileOptions];
    [self setupTimeActionsheet];
    [self setupTempActionSheet];
    [self setupStartProfileActionsheet];
}

- (void)setupNavBar {
    self.navigationItem.title = NSLocalizedString(@"heating", @"Heating");
    [self setupRightNavBarButton];
    [self setupLeftNavBarButton];
}

- (void)setupRightNavBarButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"done", @"Done")
                                                           type:kButtonArrowRight
                                                            tag:kBtnDone
                                                          width:50
                                                         height:18
                                                         target:self
                                                       selector:@selector(buttonPressed:)];
        
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithCustomView:button];
        [tmpTopRightBtn setTag:kBtnDone];
        self.navigationItem.rightBarButtonItem = tmpTopRightBtn;
        
    } else {
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(buttonPressed:)];
        [tmpTopRightBtn setTag:kBtnDone];
        self.navigationItem.rightBarButtonItem = tmpTopRightBtn;
    }
}

- (void)setupLeftNavBarButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *homeButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"back", @"Back")
                                                           type:kButtonArrowLeft
                                                            tag:kBtnBack
                                                          width:50
                                                         height:18
                                                         target:self
                                                       selector:@selector(buttonPressed:)];
        
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;
        
    } else {
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"back", @"Back")
                                                                          style:UIBarButtonItemStyleBordered
                                                                         target:self
                                                                         action:@selector(buttonPressed:)];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;
    }
}

- (void)setupTopBarButtons {
    // Add a containing view for the buttons
    UIView *topHeadingBar = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.heightOffset, self.scrollView.frame.size.width, kTopHeadingBarHeight)];
    [topHeadingBar setBackgroundColor:[UIColor lightGrayColor]];
    [topHeadingBar setClipsToBounds:YES];
    [self.view addSubview:topHeadingBar];
    self.heightOffset += topHeadingBar.frame.size.height;
    
    // Add Buttons
    NSUInteger tmpNumButtons = 2;
    float tmpPadding = 5.0f;
    float tmpWidth = (topHeadingBar.frame.size.width / tmpNumButtons) - (tmpPadding * 2);
    float tmpX = 0.0f;
    tmpX = [self addButtonToView:topHeadingBar text:NSLocalizedString(@"edit", @"Edit") tag:kBtnEdit x:tmpX width:tmpWidth padding:tmpPadding];
    [self addButtonToView:topHeadingBar text:NSLocalizedString(@"add", @"Add") tag:kBtnAdd x:tmpX width:tmpWidth padding:tmpPadding];
    
    // Add Vertical Bar
    if ([AppStyleHelper isMegaMan]) {
        UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 2.0f, topHeadingBar.frame.size.height)];
        [bar setBackgroundColor:[UIColor whiteColor]];
        [bar setCenterX:topHeadingBar.center.x];
        [topHeadingBar addSubview:bar];
        
        UIImageView *imageView = [ViewBuilderHelper getImageViewWithFilename:@"add-grey-banner.png" atX:0.0f atY:0.0f];
        [imageView setFrameHeight:bar.frame.size.height];
        [topHeadingBar insertSubview:imageView atIndex:0];
    }
    
    // Reposition ScrollView
    [self.scrollView setFrameY:self.heightOffset];
    [self.scrollView setFrameHeight:self.view.frame.size.height - self.heightOffset];
}

- (void)setupTableView {
    self.tableView = [ViewBuilderHelper getTableViewWithWidth:self.scrollView.frame.size.width
                                                       height:self.scrollView.frame.size.height
                                                        style:UITableViewStylePlain
                                                       target:self];
    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.scrollView addSubview:self.tableView];
    self.heightOffset += self.tableView.frame.size.height;
    [self.tableView reloadData];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (float)addButtonToView:(UIView *)tmpView
                    text:(NSString *)tmpText
                     tag:(NSUInteger)tmpTag
                       x:(float)tmpX
                   width:(float)tmpWidth
                 padding:(float)tmpPadding {
    
    tmpX += tmpPadding;
    UIButton *tmpButton = nil;
    tmpButton = [ButtonsHelper getButtonWithText:tmpText
                                            type:kButtonGreyBacking
                                             tag:tmpTag
                                           width:tmpWidth
                                          height:25.0f
                                          target:self
                                        selector:@selector(buttonPressed:)];

    if ([AppStyleHelper isMegaMan]) {
        [tmpButton setBackgroundImage:nil forState:UIControlStateNormal];
        [tmpButton setBackgroundImage:nil forState:UIControlStateHighlighted];
        [tmpButton setBackgroundImage:nil forState:UIControlStateSelected];
        [tmpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [tmpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    }

    [tmpButton setFrameX:tmpX];
    [tmpButton setFrameY:tmpPadding];
    [tmpView addSubview:tmpButton];
    tmpX += (tmpButton.frame.size.width + tmpPadding);
    return tmpX;
}

- (void)setupAddProfileOptions {
    self.optionsActionSheet = [[LightwaveActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                             buttonTitles:[NSArray arrayWithObjects:
                                                                           NSLocalizedString(@"weekday", @"Weekday"),
                                                                           NSLocalizedString(@"weekend", @"Weekend"),
                                                                           NSLocalizedString(@"holiday", @"Holiday"),
                                                                           nil]
                                                              cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                               dataSource:self
                                                                 delegate:self];
    [self.optionsActionSheet setTag:kHeatingVCAddProfile];
    [self.view addSubview:self.optionsActionSheet];
}

- (void)setupTimeActionsheet {
    self.timeActionSheet = [[DateTimeActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                               buttonTitles:[NSArray arrayWithObjects:
                                                                             NSLocalizedString(@"set_start_time", @"Set Start Time"),
                                                                             nil]
                                                                cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                                 dataSource:self
                                                                   delegate:self
                                                          selectedDateMode:UIDatePickerModeTime];
    [self.view addSubview:self.timeActionSheet];
}

- (void)setupTempActionSheet {
    self.tempActionSheet = [[TempActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                     buttonTitles:[NSArray arrayWithObjects:
                                                                   NSLocalizedString(@"set_temp", @"Set Temp"),
                                                                   nil]
                                                      cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                       dataSource:self
                                                         delegate:self
                                                showNumComponents:1];
    [self.tempActionSheet setTag:kHeatingVCTemperature];
    [self.view addSubview:self.tempActionSheet];
}

- (void)setupStartProfileActionsheet {
    self.dateActionSheet = [[DateTimeActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                         buttonTitles:[NSArray arrayWithObjects:
                                                                       NSLocalizedString(@"set_date", @"Set Date"),
                                                                       nil]
                                                          cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                           dataSource:self
                                                             delegate:self
                                                     selectedDateMode:UIDatePickerModeDate];
    [self.dateActionSheet setTag:kHeatingVCStartProfile];
    [self.view addSubview:self.dateActionSheet];
}

#pragma mark - Private

- (void)refreshAddProfile {
    self.addingProfile = nil;
    self.addingProfile = [[Profile alloc] initWithZoneId:self.zone.zoneID temp:20.0f start:@"09:00" end:@"09:00"];
}

- (void)refreshGetProfiles {
    // Get the heating profiles for this zone
    Home *home = [Home shared];
    self.weekdayProfilesArray = [home getProfilesForZoneID:self.zone.zoneID type:kHeatingWeekday];
    self.weekendProfilesArray = [home getProfilesForZoneID:self.zone.zoneID type:kHeatingWeekend];
    self.holidayProfilesArray = [home getProfilesForZoneID:self.zone.zoneID type:kHeatingHoliday];
    
#if TARGET_IPHONE_SIMULATOR
    
    // Testing purposes.... just show some data...
    
    if (self.weekdayProfilesArray.count <= 0)
        self.weekdayProfilesArray = [home getProfilesForType:kHeatingWeekday];
    
    if (self.weekendProfilesArray.count <= 0)
        self.weekendProfilesArray = [home getProfilesForType:kHeatingWeekend];
    
    if (self.holidayProfilesArray.count <= 0)
        self.holidayProfilesArray = [home getProfilesForType:kHeatingHoliday];
    
#endif
}

- (void)displayTimePicker:(HeatingCellTags)_type {
    // Get values
    NSString *_timeStr = nil;
    
    if (self.editingProfile)
        _timeStr = _type == kHeatingCellStartTime ? self.editingProfile.start : self.editingProfile.end;
    else
        _timeStr = @"09:00";
    
    NSUInteger _tag = _type == kHeatingCellStartTime ? kHeatingVCStartTime : kHeatingVCEndTime;
    NSString *_timeTitle = _type == kHeatingCellStartTime ? NSLocalizedString(@"set_start_time", @"Set Start Time") : NSLocalizedString(@"set_end_time", @"Set End Time");
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    NSDate *_date = [dateFormat dateFromString:_timeStr];
    UIDatePicker *_datePicker = (UIDatePicker *)self.timeActionSheet.datePickerView;
    [_datePicker setDate:_date animated:NO];
    [self.timeActionSheet setTag:_tag];
    [self.timeActionSheet setTitle:_timeTitle forButtonAtIndex:kHeatingVCTimeSet];
    [self.timeActionSheet toggleActionSheetWithDelay:0.0f];
}

#pragma mark - CustomActionSheet Datasource

- (UIView *)presentedFromView:(LightwaveActionSheet *)tmpCustomActionSheet {
    return self.view;
}

- (UIColor *)actionSheetColourTypes:(LightwaveActionSheet *)tmpCustomActionSheet type:(CustomActionSheetStyleColours)_type {
    if (_type == kCustomActionSheetBackgroundColour)
        return [UIColor colorWithDivisionOfRed:96.0f green:101.0f blue:111.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientStart)
        return [UIColor colorWithDivisionOfRed:166.0f green:169.0f blue:175.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientEnd)
        return [UIColor colorWithDivisionOfRed:123.0f green:128.0f blue:136.0f alpha:1.0f];
    else
        return [UIColor clearColor];
}

- (UIImage *)actionSheetButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreenBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (UIImage *)actionSheetCancelButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreyBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (float)actionSheetButtonHeight:(LightwaveActionSheet *)tmpCustomActionSheet {
    return [AppStyleHelper isMegaMan] ? 40.0f : 35.0f;
}

#pragma mark - CustomActionSheet Delegate

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet index:(NSUInteger)tmpIndex {
    if ([tmpCustomActionSheet tag] == kHeatingVCAddProfile) {
        
        /*****************************************
         * ADD PROFILE OPTIONS
         *****************************************/
        
        switch (tmpIndex) {
                
            case kHeatingVCAddProfileWeekday:
            case kHeatingVCAddProfileWeekend:
            case kHeatingVCAddProfileHoliday:
            {
                /*********************************
                 * Add Profile
                 *********************************/
                self.addingProfile.type = tmpIndex == kHeatingVCAddProfileWeekday ? kHeatingWeekday : (tmpIndex == kHeatingVCAddProfileWeekend ? kHeatingWeekend : kHeatingHoliday);
                [self.optionsActionSheet toggleActionSheetWithDelay:0.0f];
                
                // Delay
                float _closeTime = 0.4f;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, _closeTime * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                    self.editingProfile = nil;
                    [self displayTimePicker:kHeatingCellStartTime];
                    
                });
            }
                break;
                
            case kHeatingVCAddProfileCancel:
            {
                [self.optionsActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
        
    }
}

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet
                           index:(NSUInteger)tmpIndex
                      pickerView:(UIPickerView *)tmpPickerView {
    
    if ([tmpCustomActionSheet tag] == kHeatingVCStartTime ||
        [tmpCustomActionSheet tag] == kHeatingVCEndTime) {
        
        /*****************************************
         * START || END TIME OPTIONS
         *****************************************/
        
        switch (tmpIndex) {
                
            case kHeatingVCTimeSet:
            {
                UIDatePicker *_datePicker = (UIDatePicker *)tmpPickerView;
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"HH:mm"];
                NSString *_timeString = [dateFormat stringFromDate:_datePicker.date];
                
                if (self.editingProfile) {
                    
                    /*********************************
                     * Editing Start || End Time
                     *********************************/
                    
                    if ([tmpCustomActionSheet tag] == kHeatingVCStartTime)
                        self.editingProfile.start = _timeString;
                    
                    else if ([tmpCustomActionSheet tag] == kHeatingVCEndTime)
                        self.editingProfile.end = _timeString;
                    
                    // Save
                    Home *home = [Home shared];
                    [home saveProfiles];
                    
                    [self.tableView reloadData];
                    self.editingProfile = nil;
                    [self.timeActionSheet toggleActionSheetWithDelay:0.0f];
                    
                } else {
                    
                    /*********************************
                     * Adding Start || End Time
                     *********************************/
                    
                    [self.timeActionSheet toggleActionSheetWithDelay:0.0f];
                    
                    if ([tmpCustomActionSheet tag] == kHeatingVCStartTime) {
                        self.addingProfile.start = _timeString;
                        
                        // Delay
                        float _closeTime = 0.4f;
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, _closeTime * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                            
                            self.editingProfile = nil;
                            [self displayTimePicker:kHeatingCellEndTime];
                            
                        });
                    }
                    
                    else if ([tmpCustomActionSheet tag] == kHeatingVCEndTime) {
                        self.addingProfile.end = _timeString;
                        
                        // Delay
                        float _closeTime = 0.4f;
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, _closeTime * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                            
                            self.editingProfile = nil;
                            [self.tempActionSheet toggleActionSheetWithDelay:0.0f];
                            
                        });
                    }
                }
            }
                break;
                
            case kHeatingVCTimeCancel:
            {
                self.editingProfile = nil;
                [self.timeActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
        
    } else if ([tmpCustomActionSheet tag] == kHeatingVCTemperature) {
        /*****************************************
         * SET TEMP
         *****************************************/
        switch (tmpIndex) {
                
            case kHeatingVCTemperatureSet:
            {
                NSUInteger _selectedRow = [tmpPickerView selectedRowInComponent:0];
                NSString *_selectedTitle = [[tmpPickerView delegate] pickerView:tmpPickerView titleForRow:_selectedRow forComponent:0];
                
                if (self.editingProfile) {
                    /*********************************
                     * Editing Temperature
                     *********************************/
                    self.editingProfile.temp = [_selectedTitle floatValue];

                    // Save
                    Home *home = [Home shared];
                    [home saveProfiles];
                    
                    [self.tableView reloadData];
                    self.editingProfile = nil;
                    
                } else {
                    /*********************************
                     * Adding Temperature
                     *********************************/
                    self.addingProfile.temp = [_selectedTitle floatValue];

                    // Save
                    Home *home = [Home shared];
                    [home addProfile:self.addingProfile];
                    [home saveProfiles];
                    
                    [self refreshGetProfiles];
                    [self.tableView reloadData];
                    [self refreshAddProfile];
                    
                    // Alert User
                    [self.view makeToast:NSLocalizedString(@"add_heating_profile", @"Added Heating Profile")];
                }
                
                [self.tempActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            case kHeatingVCTemperatureCancel:
            {
                self.editingProfile = nil;
                [self.tempActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
        
    } else if ([tmpCustomActionSheet tag] == kHeatingVCStartProfile) {
        /*****************************************
         * SET DATE (Start Profile)
         *****************************************/
        switch (tmpIndex) {
                
            case kHeatingVCStartProfileSetDate:
            {
                UIDatePicker *_datePicker = (UIDatePicker *)tmpPickerView;
                DebugLog(@"Date = %@", _datePicker.date);
                [self.dateActionSheet toggleActionSheetWithDelay:0.0f];
                
          
                
#warning TODO - Heating
                

                // Not sure what should happen from here....
                // Android app crashes at this point.....
                
                
                
            }
                break;
                
            case kHeatingVCStartProfileCancel:
            {
                [self.dateActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - HeatingCell delegate

- (void)chooseProductSwitch:(HeatingCell *)_ChooseProductOnOffCell
                  indexPath:(NSIndexPath *)_indexPath
                       type:(HeatingCellTags)_type {
    
    Profile *_profile = nil;
    
    switch (_indexPath.section) {
            
        case kHeatingWeekday:
            _profile = [self.weekdayProfilesArray objectAtIndex:_indexPath.row];
            break;
            
        case kHeatingWeekend:
            _profile = [self.weekendProfilesArray objectAtIndex:_indexPath.row];
            break;
            
        case kHeatingHoliday:
            _profile = [self.holidayProfilesArray objectAtIndex:_indexPath.row];
            break;
    }
    
    self.editingProfile = _profile;
    
    switch (_type) {
            
        case kHeatingCellStartTime:
        case kHeatingCellEndTime:
        {
            [self displayTimePicker:_type];
        }
            break;
            
        case kHeatingCellTemperature:
        {
            NSUInteger _currentValue = (NSUInteger)_profile.temp;
            NSUInteger _selectRow = _currentValue - kTempMin;

            if ([self.tempActionSheet.tempPickerView numberOfRowsInComponent:0] > _selectRow)
                [self.tempActionSheet.tempPickerView selectRow:_selectRow inComponent:0 animated:NO];
            
            [self.tempActionSheet toggleActionSheetWithDelay:0.0f];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tmpTableView {
    return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                              0.0f,
                                                              self.tableView.frame.size.width,
                                                              [self tableView:self.tableView heightForHeaderInSection:section])];
    [header setBackgroundColor:[UIColor darkGrayColor]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor= [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont boldSystemFontOfSize:17.0];
    
    switch (section) {
            
        case kHeatingWeekday:
            label.text = NSLocalizedString(@"weekdays", @"Weekdays");
            break;
            
        case kHeatingWeekend:
            label.text = NSLocalizedString(@"weekends", @"Weekends");
            break;
            
        case kHeatingHoliday:
        {
            label.text = NSLocalizedString(@"holidays", @"Holidays");
            
            // Add Start Profile Button
            UIButton *_button = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"start_profile", @"Start Profile") uppercaseString]
                                                            type:kButtonGreenBacking
                                                             tag:section
                                                           width:120.0f
                                                          height:header.frame.size.height - 4.0f
                                                          target:self
                                                        selector:@selector(startProfilePressed:)];
            [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_button setFrameX:(header.frame.size.width - _button.frame.size.width) - 5.0f];
            [_button setCenterY:header.center.y];
            [header addSubview:_button];
        }
            break;
    }
    
    [label sizeToFit];
    [label setFrameX:5.0f];
    [label setCenterY:header.center.y];
    [header addSubview:label];
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    switch (section) {
            
        case kHeatingWeekday:
            return self.weekdayProfilesArray.count;
            break;
            
        case kHeatingWeekend:
            return self.weekendProfilesArray.count;
            break;
            
        case kHeatingHoliday:
            return self.holidayProfilesArray.count;
            break;
            
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return [HeatingCell getCellHeight];
}

#pragma mark - Table view data source

- (id)setupInitialCell:(HeatingCell *)_cell xibName:(NSString *)_xib {
    if (_cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:_xib owner:self options:nil];
        _cell = [topLevelObjects objectAtIndex:0];
        [_cell setFrameWidth:self.tableView.frame.size.width];
        [_cell.contentView setFrameWidth:self.tableView.frame.size.width];
        [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return _cell;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // vars
    const char* className = class_getName([self class]);
    NSString *cellIdentifier = nil;
    
    // Initialize Cell
    cellIdentifier = [NSString stringWithFormat:@"HeatingCell-%s", className];
    HeatingCell *cell = (HeatingCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell = [self setupInitialCell:cell xibName:@"HeatingCell"];
    
    Profile *_profile = nil;
    
    switch (indexPath.section) {
            
        case kHeatingWeekday:
            _profile = [self.weekdayProfilesArray objectAtIndex:indexPath.row];
            break;
            
        case kHeatingWeekend:
            _profile = [self.weekendProfilesArray objectAtIndex:indexPath.row];
            break;
            
        case kHeatingHoliday:
            _profile = [self.holidayProfilesArray objectAtIndex:indexPath.row];
            break;
    }
    
    // Setup Cell
    [cell setupCellWithProfile:_profile
                     indexPath:indexPath
                      delegate:self];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark -
#pragma mark Table view editing

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Profile *_profile = nil;
        
        switch (indexPath.section) {
                
            case kHeatingWeekday:
                _profile = [self.weekdayProfilesArray objectAtIndex:indexPath.row];
                break;
                
            case kHeatingWeekend:
                _profile = [self.weekendProfilesArray objectAtIndex:indexPath.row];
                break;
                
            case kHeatingHoliday:
                _profile = [self.holidayProfilesArray objectAtIndex:indexPath.row];
                break;
        }
                
        // Remove from global profiles array
        Home *home = [Home shared];
        [home removeProfile:_profile];
        
        // Save!
        [home saveProfiles];

        [self refreshGetProfiles];
        [self.tableView reloadData];
    }
}

#pragma mark - Actions

- (void)dismissView {
    if (self.navigationController != nil)
        [[self navigationController] dismissModalViewControllerAnimated:YES];
}

- (IBAction)startProfilePressed:(id)sender {
    //UIButton *_button = (UIButton *)sender;
    //DebugLog(@"Tag = %d", _button.tag);
    
    [self.dateActionSheet toggleActionSheetWithDelay:0.0f]; 
}

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnBack:
        {
            [self dismissView];
        }
            break;
            
        case kBtnDone:
        {
            Home *home = [Home shared];
            [home saveProfiles];
            [self dismissView];
        }
            break;
            
        case kBtnAdd:
        {
            [self.optionsActionSheet toggleActionSheetWithDelay:0.0f];
        }
            break;
            
        case kBtnEdit:
        {
            if ([self.tableView isEditing]) {
                [self.tableView setEditing:NO animated:YES];
                [sender setTitle:NSLocalizedString(@"edit", @"Edit") forState:UIControlStateNormal];

            } else {
                [self.tableView setEditing:YES animated:YES];
                [sender setTitle:NSLocalizedString(@"done", @"Done") forState:UIControlStateNormal];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
