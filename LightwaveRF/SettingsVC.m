//
//  SettingsVC.m
//  LightwaveRF
//
//  Created by Nik Lever on 03/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "SettingsVC.h"
#import "AppDelegate.h"
#import "XMLConfig.h"
#import "RemoteSettings.h"
#import "LWErrorView.h"
#import "Toast+UIView.h"
#import "WFLHelper.h"
#import "DB.h"
#import "Home.h"
#import "NSString+Utilities.h"
#import "UINavigationController+Utilities.h"
#import "CurrentLocationHelper.h"
#import "WFLHelper.h"
#import "Account.h"
#import "ButtonsHelper.h"
#import "ChooseLanguageVC.h"

#define kAlertLocationKey  @"kAlertLocationKey"

@interface SettingsVC ()
@property (nonatomic, assign) BOOL logoutAfterSaveSettings;
@property (strong, nonatomic) UILabel *version_lbl;
@property (strong, nonatomic) UILabel *location_lbl;
@property (strong, nonatomic) UITextField *name_txt;
@property (strong, nonatomic) UITextField *email_txt;
//@property (strong, nonatomic) UITextField *pin_txt;
@property (strong, nonatomic) UITextField *elec_txt;
@property (strong, nonatomic) UITextField *imp_txt;
@property (strong, nonatomic) UILabel *wifi_lbl;
@property (strong, nonatomic) UILabel *ssid_lbl;
@property (strong, nonatomic) UILabel *current_ssid_lbl;
@property (strong, nonatomic) UILabel *timezone_lbl;
@property (strong, nonatomic) UISwitch *useWifiName_btn;
@property (strong, nonatomic) UISwitch *testWifi_btn;
@property (strong, nonatomic) UISwitch *showVideo_btn;
@property (strong, nonatomic) UISwitch *loginAuto_btn;
@property (strong, nonatomic) UISwitch *enableWFLAlers_btn;
@property (strong, nonatomic) UISwitch *showFooter_btn;
@end

@implementation SettingsVC

@synthesize logoutAfterSaveSettings;
@synthesize version_lbl;
@synthesize location_lbl;
@synthesize name_txt;
@synthesize email_txt;
//@synthesize pin_txt;
@synthesize elec_txt;
@synthesize imp_txt;
@synthesize wifi_lbl;
@synthesize testWifi_btn, useWifiName_btn;
@synthesize showVideo_btn;
@synthesize loginAuto_btn;
@synthesize enableWFLAlers_btn;
@synthesize showFooter_btn;
@synthesize ssid_lbl, current_ssid_lbl;
@synthesize timezone_lbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.logoutAfterSaveSettings = NO;
    }
    return self;
}

- (void)dealloc {
    DebugLog(@"DEALLOC");
    [self endLocationListeners];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    if ([AppStyleHelper isMegaMan])
//        [self.scrollView setBackgroundColor:[UIColor colorWithDivisionOfRed:204 green:204 blue:204 alpha:1]];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
    gestureRecognizer.numberOfTapsRequired = 1;
    gestureRecognizer.enabled = YES;
    gestureRecognizer.cancelsTouchesInView = NO;
    gestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:gestureRecognizer];
    
    if (self.navigationController != nil){
        UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(shutDown)];
        self.navigationItem.leftBarButtonItem = closeButton;
    }
    
    [self setupUI];
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.heightOffset)];
    [self startLocationListeners];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Do any additional setup after loading the view from its nib.
    
    NSString *name = [[UserPrefsHelper shared] getUserName];
    if (name!=nil)
        self.name_txt.text = name;
    
    NSString *email = [[UserPrefsHelper shared] getUserEmail];
    if (email!=nil)
        self.email_txt.text = email;
    
    /*
    NSString *pwd = [[UserPrefsHelper shared] getUserPassword];
    if (pwd!=nil)
        self.pin_txt.text = pwd;
    */
    
    NSString *rateStr = [[UserPrefsHelper shared] getElecRate];
    if (rateStr!=nil) {
        float rate = [rateStr floatValue];
        self.elec_txt.text = [NSString stringWithFormat:@"%.2f", rate];
    }
    
    NSString *impKwhStr = [[UserPrefsHelper shared] getImpKwh];
    if (impKwhStr!=nil) {
        int impKwh = [impKwhStr intValue];
        self.imp_txt.text = [NSString stringWithFormat:@"%d", impKwh];
    }
    
    NSString *mac = [[UserPrefsHelper shared] getUserMac];
    if (mac!=nil)
        self.wifi_lbl.text = mac;
    
    [self.useWifiName_btn setOn:[[UserPrefsHelper shared] getUseWiFiName] animated:NO];
    [self.showVideo_btn setOn:![[UserPrefsHelper shared] getVideoShown] animated:NO];
    [self.testWifi_btn setOn:[[UserPrefsHelper shared] getTestWiFi] animated:NO];
    [self.loginAuto_btn setOn:[[UserPrefsHelper shared] getLoginAuto] animated:NO];
    [self.enableWFLAlers_btn setOn:[[UserPrefsHelper shared] getEnableWFLPopups] animated:NO];
    [self.showFooter_btn setOn:[[UserPrefsHelper shared] getShowHomeFooter] animated:NO];
    
    
    
    
    
    
    // BETA TestFlights
    NSString *_str = [NSString stringWithFormat:NSLocalizedString(@"beta_version", nil), kAppVersion];
    
    
    
    // AppStore
    //NSString *_str = [NSString stringWithFormat:NSLocalizedString(@"release_version", nil), kAppVersion, @"0071"];
    
    [self.version_lbl setText:_str];
    
    
    
    
    
    
    
    if ([[UserPrefsHelper shared] getLocationInfo].length > 0)
        [self.location_lbl setText:[[UserPrefsHelper shared] getLocationInfo]];
    
    if ([[UserPrefsHelper shared] getTimeZoneInfoWithoutHours].length > 0)
        [self.timezone_lbl setText:[[UserPrefsHelper shared] getTimeZoneInfoWithoutHours]];
    
    if ([[UserPrefsHelper shared] getSSID].length > 0)
        [self.ssid_lbl setText:[[UserPrefsHelper shared] getSSID]];
    
    if ([[UserPrefsHelper shared] getCurrentWiFi].length > 0)
        [self.current_ssid_lbl setText:[[UserPrefsHelper shared] getCurrentWiFi]];

    self.logoutAfterSaveSettings = NO;
    
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -
#pragma Current Location

- (void)startLocationListeners {
    [self addObserverWithName:kCurrentLocationSuccessNotificationKey selector:@selector(receivedLocationSuccessNotification:)];
    [self addObserverWithName:kCurrentLocationFailedNotificationKey selector:@selector(receivedLocationFailedNotification:)];
}

- (void)endLocationListeners {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCurrentLocationSuccessNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCurrentLocationFailedNotificationKey object:nil];
}

- (void)receivedLocationSuccessNotification:(NSNotification *)_notification {
    [[CurrentLocationHelper shared] stopSearchingLocation];
    [self removeSpinner];
    
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_region = [_dict objectForKey:@"region"];
    NSString *_latLong = [_dict objectForKey:@"latlong"];
    NSString * userMac = [[UserPrefsHelper shared] getUserMac]; // @"74:0A:BC:FF:FF:00";
    
    LWErrorView *alert = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"set_wifi_location", @"set_wifi_location")
                                                    message:[NSString stringWithFormat:NSLocalizedString(@"set_location_message", nil),
                                                             _latLong,
                                                             _region,
                                                             _region,
                                                             userMac.length > 0 ? userMac : NSLocalizedString(@"Unknown", @"Unknown")]
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel")
                                          otherButtonTitles:NSLocalizedString(@"yes", @"Yes"), nil];
    [alert setTag:kSettingsVCSetLocationAlert];
    objc_setAssociatedObject(alert, kAlertLocationKey, _dict, OBJC_ASSOCIATION_RETAIN);
    [alert show];
}

- (void)receivedLocationFailedNotification:(NSNotification *)_notification {
    [[CurrentLocationHelper shared] stopSearchingLocation];
    [self removeSpinner];
    
    NSString *_errorStr = (NSString *)[_notification object];
    [self showError:_errorStr];
}

#pragma mark - UI Creation

- (void)setupUI {
    self.heightOffset = 0.0f;
    [self addBuildLabel];
    [self addFields];
    [self addButtons];
    [self addSwitches];
    self.heightOffset += ([AppStyleHelper isMegaMan] ? 25.0f : 15.0f);
}

- (void)addBuildLabel {
    float padding = [AppStyleHelper isMegaMan] ? 10.0f : 5.0f;
    self.heightOffset += 5.0f;
    
    if (![[UserPrefsHelper shared] getDemoAccount]) {
        // Build
        
        UIView *bgView = [self addBackView];
        self.version_lbl = [self getLabelWithFrame:CGRectMake(padding, self.heightOffset, self.scrollView.frame.size.width - (padding * 2), 20.0f)];
        [self.version_lbl setFont:[UIFont fontWithName:@"Helvetica" size:11.0f]];
        [self.scrollView addSubview:self.version_lbl];
        [bgView setFrameHeight:(self.version_lbl.frame.origin.y + self.version_lbl.frame.size.height) - bgView.frame.origin.y];
        
        self.heightOffset += self.version_lbl.frame.size.height;
        self.heightOffset += 10.0f;
    }
}

- (UIView *)addBackView {
    float padding = 5.0f;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(padding, self.heightOffset, self.scrollView.frame.size.width - (padding * 2), 0.0f)];
    
    if ([AppStyleHelper isMegaMan]) {
        [view setBackgroundColor:[UIColor whiteColor]];
        view.layer.cornerRadius = 5.0f;
    } else {
        [view setBackgroundColor:[UIColor clearColor]];
    }
    
    [self.scrollView insertSubview:view atIndex:0];
    return view;
}

- (void)showLocationLabel {
    if (![[UserPrefsHelper shared] getDemoAccount]) {
        if ([[UserPrefsHelper shared] getLocationInfo].length > 0) {
            
            UIView *bgView = [self addBackView];

            [self addLabelWithText:NSLocalizedString(@"account_location", nil)
                              font:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
            
            if (self.location_lbl) {
                [self.location_lbl setText:[[UserPrefsHelper shared] getLocationInfo]];
                
            } else {
                self.location_lbl = [self addLabelWithText:[[UserPrefsHelper shared] getLocationInfo]
                                                      font:[UIFont fontWithName:@"Helvetica" size:13.0f]];
            }
            
            [bgView setFrameHeight:(self.location_lbl.frame.origin.y + self.location_lbl.frame.size.height) - bgView.frame.origin.y];
        } 
    }
}

- (void)showTimeZoneLabel {
    if (![[UserPrefsHelper shared] getDemoAccount]) {
        
        UIView *bgView = [self addBackView];
        
        [self addLabelWithText:NSLocalizedString(@"current_timezone", nil)
                          font:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
        
        UILabel *label = [self addLabelWithText:[Account getDeviceTimeZoneWithoutHours]
                                           font:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        
        if ([[UserPrefsHelper shared] getTimeZoneInfoWithoutHours].length > 0) {
            
            [self addLabelWithText:NSLocalizedString(@"account_timezone", nil)
                              font:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
            
            if (self.timezone_lbl) {
                [self.timezone_lbl setText:[[UserPrefsHelper shared] getTimeZoneInfoWithoutHours]];
                
            } else {
                self.timezone_lbl = [self addLabelWithText:[[UserPrefsHelper shared] getTimeZoneInfoWithoutHours]
                                                      font:[UIFont fontWithName:@"Helvetica" size:13.0f]];
            }
            
            [bgView setFrameHeight:(self.timezone_lbl.frame.origin.y + self.timezone_lbl.frame.size.height) - bgView.frame.origin.y];
        } else {
            [bgView setFrameHeight:(label.frame.origin.y + label.frame.size.height) - bgView.frame.origin.y];
        }
    }
}

/*
- (NSString *)getSSIDText {
    NSString *_currentConnectedWiFiName = [[UserPrefsHelper shared] getCurrentWiFi];
    NSString *_currentSavedWiFiName = [[UserPrefsHelper shared] getSSID];
    NSString *_currentConnectedStr = [NSString stringWithFormat:NSLocalizedString(@"wifi_current", nil), _currentConnectedWiFiName];
    NSString *_currentSavedStr = [NSString stringWithFormat:NSLocalizedString(@"account", nil), _currentSavedWiFiName];
    NSString *_text = nil;
    
    // SSID = Current Saved WiFi Name
    if (_currentSavedWiFiName.length > 0 && _currentConnectedWiFiName.length > 0) {
        _text = [NSString stringWithFormat:@"%@ %@", _currentConnectedStr, _currentSavedStr];
        
    } else if (_currentSavedWiFiName.length > 0) {
        _text = _currentSavedStr;
        
    } else if (_currentConnectedWiFiName.length > 0) {
        _text = _currentConnectedStr;
    }
    
    return _text;
}
 */

- (void)showSSIDLabel {
    if (![[UserPrefsHelper shared] getDemoAccount]) {
        
        if ([[UserPrefsHelper shared] getCurrentWiFi].length > 0) {
            
            UIView *bgView = [self addBackView];
            
            [self addLabelWithText:NSLocalizedString(@"current_wifi", nil)
                              font:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
            
            if (self.current_ssid_lbl) {
                [self.current_ssid_lbl setText:[[UserPrefsHelper shared] getCurrentWiFi]];
                
            } else {
                self.current_ssid_lbl = [self addLabelWithText:[[UserPrefsHelper shared] getCurrentWiFi]
                                                          font:[UIFont fontWithName:@"Helvetica" size:13.0f]];
            }
            
            [bgView setFrameHeight:(self.current_ssid_lbl.frame.origin.y + self.current_ssid_lbl.frame.size.height) - bgView.frame.origin.y];
        }
        
        if ([[UserPrefsHelper shared] getSSID].length > 0) {
            
            UIView *bgView = [self addBackView];
            
            [self addLabelWithText:NSLocalizedString(@"account_wifi", nil)
                              font:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
            
            if (self.ssid_lbl) {
                [self.ssid_lbl setText:[[UserPrefsHelper shared] getSSID]];
                
            } else {
                self.ssid_lbl = [self addLabelWithText:[[UserPrefsHelper shared] getSSID]
                                                  font:[UIFont fontWithName:@"Helvetica" size:13.0f]];
            }
            
            [bgView setFrameHeight:(self.ssid_lbl.frame.origin.y + self.ssid_lbl.frame.size.height) - bgView.frame.origin.y];
        }
    }
}

- (void)addFields {
    if ([[UserPrefsHelper shared] getDemoAccount]) {
        [self addButtonWithText:NSLocalizedString(@"log_out", @"Log Out") tag:kBtnLogout];
    } else {
                
        [self addButtonWithText:NSLocalizedString(@"log_out", @"Log Out") tag:kBtnLogout];
        
        [self showSeperator];

        UIView *bgView = [self addBackView];
        
        if ([AppStyleHelper isMegaMan])
            self.heightOffset += 5.0f;
        
        self.name_txt = [self addSection:NSLocalizedString(@"name", @"Name")
                                     tag:kSettingsVCNameTextField
                            keyboardType:UIKeyboardTypeDefault];
        self.heightOffset += self.name_txt.frame.size.height + 5.0f;
        
        self.email_txt = [self addSection:NSLocalizedString(@"email", @"Email")
                                      tag:kSettingsVCEmailTextField
                             keyboardType:UIKeyboardTypeEmailAddress];
        [self.email_txt setBorderStyle:UITextBorderStyleNone];
        [self.email_txt setEnabled:NO];
        self.heightOffset += self.email_txt.frame.size.height + 10.0f;
 
        /*
        self.pin_txt = [self addSection:NSLocalizedString(@"pin", @"Pin")
                                    tag:kSettingsVCPinTextField
                           keyboardType:UIKeyboardTypeNumberPad
                             buttonText:NSLocalizedString(@"log_out", @"Log Out")
                              buttonTag:kBtnLogout];
        [self.pin_txt setSecureTextEntry:YES];
        [self.pin_txt setEnabled:NO];
        [self.pin_txt setBorderStyle:UITextBorderStyleNone];
        self.heightOffset += self.pin_txt.frame.size.height + 15.0f;
        */
        
        if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isKlikaan] || [AppStyleHelper isCOCO]) {
            self.elec_txt = [self addSection:NSLocalizedString(@"elec_rate", @"Elec Rate")
                                         tag:kSettingsVCElecRateTextField
                                keyboardType:UIKeyboardTypeDecimalPad
                                  buttonText:[NSString stringWithFormat:@"%@", NSLocalizedString(@"save_elec_rate", @"save_elec_rate")]
                                   buttonTag:kBtnSaveElecRate];
            self.heightOffset += self.elec_txt.frame.size.height + 15.0f;
        }
        
        if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO]) {

            if ([[UserPrefsHelper shared] isLockedToPrivateAccounts]) {
                self.imp_txt = [self addSection:NSLocalizedString(@"impkwh", @"Imp/Kwh")
                                            tag:kSettingsVCimpKwhTextField
                                   keyboardType:UIKeyboardTypeNumberPad
                                     buttonText:NSLocalizedString(@"save_impkwh", @"Save Imp/Kwh")
                                      buttonTag:kBtnSaveImpKwh];
                self.heightOffset += self.imp_txt.frame.size.height + 15.0f;
            }
            
        } else if ([AppStyleHelper isKlikaan]) {
            
            self.imp_txt = [self addSection:NSLocalizedString(@"impkwh", @"Imp/Kwh")
                                        tag:kSettingsVCimpKwhTextField
                               keyboardType:UIKeyboardTypeNumberPad
                                 buttonText:NSLocalizedString(@"save_impkwh", @"Save Imp/Kwh")
                                  buttonTag:kBtnSaveImpKwh];
            self.heightOffset += self.imp_txt.frame.size.height + 15.0f;
        }
        
        self.wifi_lbl = [self addLabel:NSLocalizedString(@"wifi_link", @"wifi_link") tag:kSettingsVCWiFiLabel];
        [self.wifi_lbl setText:[NSString stringWithFormat:@"%@%@-%@-%@",
                                NSLocalizedString(@"hub_beginning", @"hub_beginning"),
                                NSLocalizedString(@"hub_setting_default", @"XX"),
                                NSLocalizedString(@"hub_setting_default", @"XX"),
                                NSLocalizedString(@"hub_setting_default", @"XX")]];
        
        [bgView setFrameHeight:(self.wifi_lbl.frame.origin.y + self.wifi_lbl.frame.size.height) - bgView.frame.origin.y];
        self.heightOffset += self.wifi_lbl.frame.size.height + 5.0f;
    }
}

- (void)addButtons {
    if ([[UserPrefsHelper shared] getDemoAccount]) {
        
    } else {
        [self showSeperator];
        
        [self addButtonWithText:NSLocalizedString(@"get_settings_from_web_server", @"get_settings_from_web_server") tag:kBtnGetSettings];
        [self showSeperator];
        
        if (![AppStyleHelper isFSLDemo]) {
            [self addButtonWithText:NSLocalizedString(@"save_settings_to_web_server", @"save_settings_to_web_server") tag:kBtnSaveSettings];
            [self showSeperator];
        }
        
        if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo]  || [AppStyleHelper isNexa] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO]) {
            [self addButtonWithText:NSLocalizedString(@"sync_device_events", @"sync_device_events") tag:kBtnSyncDevice];
            [self showSeperator];
            
            
            [self addButtonWithText:NSLocalizedString(@"wifilink_register", @"wifilink_register") tag:kBtnRegisterWiFiLink];
            [self showSeperator];
        }
        
        
//#if TARGET_IPHONE_SIMULATOR
//        
//        
//        [self addButtonWithText:NSLocalizedString(@"choose_language", @"choose_language") tag:kBtnChooseLanguage];
//        [self showSeperator];
//        
//        
//#endif
        
        [self showLocationLabel];
        [self addButtonWithText:NSLocalizedString(@"change_wifi_location", @"change_wifi_location") tag:kBtnSetLocation];
        [self showSeperator];
        
        [self showTimeZoneLabel];
        [self addButtonWithText:NSLocalizedString(@"set_timezone", @"set_timezone") tag:kBtnTimezone];
        [self showSeperator];
        
        [self showSSIDLabel];
        [self addButtonWithText:NSLocalizedString(@"set_ssid", @"set_ssid") tag:kBtnSSID];
        
        [self showSeperator];
    }
}

- (void)addSwitches {
    if ([[UserPrefsHelper shared] getDemoAccount]) {
        
    } else {
        self.heightOffset += 5.0f;
        
        UIView *bgView = [self addBackView];
        
        if ([AppStyleHelper isMegaMan])
            self.heightOffset += 5.0f;
        
        self.useWifiName_btn = [self addSwitchWithText:NSLocalizedString(@"use_wifi_name", @"use_wifi_name")
                                                   tag:kSettingsVCUseWiFiNameSwitch
                                                   sel:@selector(useWiFiNameChanged:)];
        
        self.testWifi_btn = [self addSwitchWithText:NSLocalizedString(@"stay_in_3G", @"stay_in_3G")
                                                tag:kSettingsVCStayIn3GSwitch
                                                sel:@selector(testWifiChanged:)];
        
        if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo]  || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO])  {
            self.showVideo_btn = [self addSwitchWithText:NSLocalizedString(@"show_video", @"show_video")
                                                     tag:kSettingsVCShowVideoSwitch
                                                     sel:@selector(showVideoChanged:)];
        }
        
        self.loginAuto_btn = [self addSwitchWithText:NSLocalizedString(@"auto_login", @"auto_login")
                                                 tag:kSettingsVCLoginAutoSwitch
                                                 sel:@selector(loginAutoChanged:)];
        
        self.enableWFLAlers_btn = [self addSwitchWithText:NSLocalizedString(@"wifi_popups", @"WiFi/3G Popup Notifications")
                                                      tag:kSettingsVCPopupsSwitch
                                                      sel:@selector(disablePopupsChanged:)];
        
        self.showFooter_btn = [self addSwitchWithText:NSLocalizedString(@"show_footer", @"show_footer")
                                                  tag:kSettingsVCShowFooterSwitch
                                                  sel:@selector(showFooterChanged:)];
        
        [bgView setFrameHeight:(self.showFooter_btn.frame.origin.y + self.showFooter_btn.frame.size.height) - bgView.frame.origin.y];
        [bgView setFrameHeight:bgView.frame.size.height + 5.0f];
    }
}

- (void)showSeperator {
    float padding = 5.0f;
    self.heightOffset += 10.0f;
    UIView *_seperator = [[UIView alloc] initWithFrame:CGRectMake(padding, self.heightOffset, self.scrollView.frame.size.width - (padding * 2), 1.0f)];
    [_seperator setBackgroundColor:[AppStyleHelper isMegaMan] ? [UIColor whiteColor] : [UIColor blackColor]];
    [self.scrollView addSubview:_seperator];
    self.heightOffset += _seperator.frame.size.height;
    self.heightOffset += 10.0f;
}

- (UILabel *)addLabelWithText:(NSString *)_text font:(UIFont *)_font {
    float padding = [AppStyleHelper isMegaMan] ? 15.0f : 10.0f;
    UILabel *_titleLabel = [self getLabelWithFrame:CGRectMake(padding, self.heightOffset, self.scrollView.frame.size.width - (padding * 2), 0.0f)];
    [_titleLabel setText:_text];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];

    [_titleLabel setFont:_font];
    CGSize size = [_text
                   sizeWithFont:_titleLabel.font
                   constrainedToSize:CGSizeMake(_titleLabel.frame.size.width, CGFLOAT_MAX)
                   lineBreakMode:NSLineBreakByWordWrapping];
    [_titleLabel setFrameHeight:size.height];
    [_titleLabel setNumberOfLines:0];
    
    [self.scrollView addSubview:_titleLabel];
    self.heightOffset += (_titleLabel.frame.size.height + 5.0f);
    return _titleLabel;
}

- (void)addButtonWithText:(NSString *)_text tag:(NSUInteger)_tag {
    float padding = 5.0f;
    UIButton *_button = [ButtonsHelper getButtonWithText:_text
                                                    type:[AppStyleHelper isMegaMan] ? kButtonGreyBacking : kButtonGreenBacking
                                                     tag:_tag
                                                   width:self.scrollView.frame.size.width - (padding * 2)
                                                  height:[AppStyleHelper isMegaMan] ? 43.0f : 35.0f
                                                  target:self
                                                selector:@selector(buttonPressed:)];
    [_button setFrameY:self.heightOffset];
    [_button setCenterX:self.scrollView.center.x];
    
    if (![AppStyleHelper isMegaMan]) {
        [_button.titleLabel setTextColor:[UIColor whiteColor]];
        [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    [self.scrollView addSubview:_button];
    self.heightOffset += (_button.frame.size.height + 0.0f);
}

- (UISwitch *)addSwitchWithText:(NSString *)_text tag:(NSUInteger)_tag sel:(SEL)_selector {
    UISwitch *_switch = [[UISwitch alloc] init];
    [_switch setFrameX:[AppStyleHelper isMegaMan] ? 10.0f : 5.0f];
    [_switch setFrameY:self.heightOffset];
    [_switch setOn:NO];
    [_switch setTag:_tag];
    [_switch addTarget:self action:_selector forControlEvents:UIControlEventValueChanged];
    [self.scrollView addSubview:_switch];
    
    float padding = 5.0f;
    UILabel *_titleLabel = [self getLabelWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    [_titleLabel alignToRightOfView:_switch padding:5.0f];
    [_titleLabel setFrameWidth:(self.scrollView.frame.size.width - _titleLabel.frame.origin.x) - padding];
    [_titleLabel setFrameHeight:35.0f];
    [_titleLabel setCenterY:_switch.center.y];
    [_titleLabel setText:_text];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
    [_titleLabel setMinimumFontSize:10.0f];
    [self.scrollView addSubview:_titleLabel];
    
    self.heightOffset += (_switch.frame.size.height + 10.0f);
    return _switch;
}

- (UILabel *)addLabel:(NSString *)_text
                  tag:(NSUInteger)_tag {
    
    float padding = 5.0f;
    UILabel *_titleLabel = [self getLabelWithFrame:CGRectMake(padding, self.heightOffset, 80.0f, 20.0f)];
    [_titleLabel setText:_text];
    [_titleLabel setTextAlignment:NSTextAlignmentRight];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    [self.scrollView addSubview:_titleLabel];
    
    UILabel *_textLabel = [self getLabelWithFrame:CGRectZero];
    [_textLabel alignToRightOfView:_titleLabel padding:5.0f];
    [_textLabel setFrameWidth:(self.scrollView.frame.size.width - _textLabel.frame.origin.x) - padding];
    [_textLabel setFrameHeight:25.0f];
    [_textLabel setCenterY:_titleLabel.center.y];
    [_textLabel setBackgroundColor:[UIColor clearColor]];
    [self.scrollView addSubview:_textLabel];
    return _textLabel;
}

- (UITextField *)addSection:(NSString *)_text
                        tag:(NSUInteger)_tag
               keyboardType:(UIKeyboardType)_keyboardType {
    
    float padding = [AppStyleHelper isMegaMan] ? 10.0f : 5.0f;
    UILabel *_titleLabel = [self getLabelWithFrame:CGRectMake(padding, self.heightOffset, 80.0f, 20.0f)];
    [_titleLabel setText:_text];
    [_titleLabel setTextAlignment:NSTextAlignmentRight];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    [self.scrollView addSubview:_titleLabel];
    
    UITextField *_textField = [ViewBuilderHelper getTextFieldWithText:@""
                                                          placeholder:@""
                                                         keyboardType:_keyboardType
                                                                 font:[UIFont fontWithName:@"Helvetica" size:13.0f]
                                                                  atX:0.0f
                                                                  atY:0.0f
                                                                width:0.0f
                                                               height:0.0f
                                                                  tag:_tag
                                                               target:self];
    [_textField alignToRightOfView:_titleLabel padding:5.0f];
    [_textField setFrameWidth:(self.scrollView.frame.size.width - _textField.frame.origin.x) - padding];
    [_textField setFrameHeight:25.0f];
    [_textField setCenterY:_titleLabel.center.y];
    [self.scrollView addSubview:_textField];
    return _textField;
}

- (UITextField *)addSection:(NSString *)_text
                        tag:(NSUInteger)_tag
               keyboardType:(UIKeyboardType)_keyboardType
                 buttonText:(NSString *)_buttonText
                  buttonTag:(NSUInteger)_buttonTag {
    
    float padding = 5.0f;
    UITextField *_textField = [self addSection:_text tag:_tag keyboardType:_keyboardType];
    [_textField setFrameWidth:_textField.frame.size.width - 140.0f];
    
    UIButton *_button = [ButtonsHelper getButtonWithText:_buttonText
                                                    type:kButtonGreenBacking
                                                     tag:_buttonTag
                                                   width:self.scrollView.frame.size.width - (padding * 2)
                                                  height:35.0f
                                                  target:self
                                                selector:@selector(buttonPressed:)];
    [_button alignToRightOfView:_textField padding:5.0f];
    [_button setFrameWidth:(self.scrollView.frame.size.width - _button.frame.origin.x) - padding];
    [_button setCenterY:_textField.center.y];
    [_button.titleLabel setTextColor:[UIColor whiteColor]];
    [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.scrollView addSubview:_button];

    return _textField;
}

- (UILabel *)getLabelWithFrame:(CGRect)_frame {
    UILabel *_label = [[UILabel alloc] initWithFrame:_frame];
    [_label setBackgroundColor:[UIColor clearColor]];
    [_label setTextColor:[UIColor blackColor]];
    return _label;
}

#pragma mark -
#pragma mark Text Field Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self scrollToTargetView:textField];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField.tag == kSettingsVCNameTextField &&
        textField.text.length > 0) {
        
        [self saveUsernameFromTextField:textField];
    }
    
    return YES;
}

- (void)saveUsernameFromTextField:(UITextField *)_textField {
    [[UserPrefsHelper shared] setUserName:_textField.text];
}

#pragma mark -

-(void)shutDown{
    if (self.navigationController != nil)
        [[self navigationController] dismissModalViewControllerAnimated:YES];
}

- (IBAction)useWiFiNameChanged:(id)sender {
    BOOL on = self.useWifiName_btn.on;

    [[UserPrefsHelper shared] setUseWiFiName:on];
    
    if (on) {
        if (![[UserPrefsHelper shared] isAtCurrentLocation]) {

            // If not at home, revert to remote mode
            [[UserPrefsHelper shared] setTestWifi:YES];
            
            // Change Indicator to Remote
            [[UDPService shared] setServiceType:kServiceWeb];
            
            // Update Remote Switch
            [self.testWifi_btn setOn:YES animated:YES];
        }
    
        NSString *_ssid = [[UserPrefsHelper shared] getSSID];
        NSString *_ssidText = [NSString stringWithFormat:@" (<%@>)", _ssid];
        [self showAlert:[NSString stringWithFormat:NSLocalizedString(@"wifi_name_info", nil),
                         _ssid.length > 0 ? _ssidText : @""
                         ]];
    }
}

- (IBAction)testWifiChanged:(id)sender {
    BOOL on = self.testWifi_btn.on;
    [[UserPrefsHelper shared] setTestWifi:on];
    
    // If trying to go UDP, but is using wifi name to check current location
    // Disable action
    if (!on) {
        if ([[UserPrefsHelper shared] getUseWiFiName] && ![[UserPrefsHelper shared] isAtCurrentLocation]) {
            // Change back to remote
            [self.testWifi_btn setOn:YES animated:YES];
        }
    }
}

- (IBAction)showVideoChanged:(id)sender {
    BOOL on = !self.showVideo_btn.on;
    [[UserPrefsHelper shared] setVideoShown:on];
}

- (IBAction)loginAutoChanged:(id)sender {
    BOOL on = self.loginAuto_btn.on;
    [[UserPrefsHelper shared] setLoginAuto:on];
}

- (IBAction)disablePopupsChanged:(id)sender {
    BOOL on = self.enableWFLAlers_btn.on;
    [[UserPrefsHelper shared] setEnableWFLPopups:on];
}

- (IBAction)showFooterChanged:(id)sender {
    BOOL on = self.showFooter_btn.on;
    [[UserPrefsHelper shared] setShowHomeFooter:on];
}

- (void)saveSettingsAndLogout:(BOOL)_logout {
    self.logoutAfterSaveSettings = _logout;
    
    [self saveUsernameFromTextField:self.name_txt];
    
    [self showSpinnerWithText:@""];
    RemoteSettings *remote = [RemoteSettings shared];
    [remote pushHomeWithCallingClass:[self getClassName:[self class]]
                                 tag:kRequestPushHome];
}

- (void)performLogout {
    [[WFLHelper shared] cleanupAllRequests];
    
    // Clear all defaults
    [[UserPrefsHelper shared] setRegistered:NO];
    [[UserPrefsHelper shared] setDemoAccount:NO];
    
    // Clear Home
    Home *home = [Home shared];
    if (home!=nil)
        [home loadAndRefreshWFL:NO];
    
    // Show Login Screen
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app switchView:@"Login"];
    [self shutDown];
    
    // Get a new allocated root VC
    [self.navigationController replaceLastWith:[kApplicationDelegate setupFifthTabController] animated:NO];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch ([alertView tag]) {
            
        case kSettingsVCLogoutAlert:
        {
            if (buttonIndex == 0) { // No
                [self performLogout];
                
            } else if (buttonIndex == 1) { // Yes
                [self saveSettingsAndLogout:YES];
                
            }
        }
            break;
            
        case kSettingsVCGetSettingsAlert:
        {
            if (buttonIndex == 0) { // No
                
                
            } else if (buttonIndex == 1) { // Yes
                
                [[UserPrefsHelper shared] setUserEmail:self.email_txt.text];
                //[[UserPrefsHelper shared] setUserPassword:self.pin_txt.text];
                [[UserPrefsHelper shared] setUserPassword:[[UserPrefsHelper shared] getUserPassword]];
                
                
                [self showSpinnerWithText:@""];
                RemoteSettings *remote = [RemoteSettings shared];
                /*
                [remote pullHomeWithCallingClass:[self getClassName:[self class]]
                                             tag:kRequestPullHome
                                       withEmail:self.email_txt.text
                                     andPassword:self.pin_txt.text];
                 */
                
                [remote pullHomeWithCallingClass:[self getClassName:[self class]]
                                             tag:kRequestPullHome
                                       withEmail:self.email_txt.text
                                     andPassword:[[UserPrefsHelper shared] getUserPassword]];
                
            }
        }
            break;
            
        case kSettingsVCSaveSettingsAlert:
        {
            if (buttonIndex == 0) { // No
                
                
            } else if (buttonIndex == 1) { // Yes
                [self saveSettingsAndLogout:NO];
            }
        }
            break;
            
        case kSettingsVCSetLocationAlert:
        {
            if (buttonIndex == 0) { // No
                
                
            } else if (buttonIndex == 1) { // Yes
                NSDictionary *_dict = objc_getAssociatedObject(alertView, kAlertLocationKey);
                NSString *_region = [_dict objectForKey:@"region"];
                NSString *_latLong = [_dict objectForKey:@"latlong"];
                
                // Cleanup bad characters in region, e.g. '
                _region = [_region stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]];
                NSCharacterSet *_doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"'"];
                _region = [[_region componentsSeparatedByCharactersInSet:_doNotWant] componentsJoinedByString: @""];

                // Save
                [[UserPrefsHelper shared] setLocationInfo:[NSString stringWithFormat:@"%@ | %@", _region, _latLong]];
                
                // Send Command
                [[WFLHelper shared] sendToUDP:YES command:[ServiceType getLatLongCommand:_latLong] tag:kRequestLocation];
                
                // Alert
                [self.view makeToast:NSLocalizedString(@"wifi_location_saved", @"WiFiLink saved")];
                
                // Update Location Label
                if ([[UserPrefsHelper shared] getLocationInfo].length > 0 && self.location_lbl)
                    [self.location_lbl setText:[[UserPrefsHelper shared] getLocationInfo]];
                
                // Save Settings?
                UIButton *_saveSettingsButton = (UIButton *)[self.view viewWithTag:kBtnSaveSettings];
                [_saveSettingsButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            }
        }
            break;
            
        case kSettingsVCTimeZoneAlert:
        {
            if (buttonIndex == 0) { // No
                
                
            } else if (buttonIndex == 1) { // Yes
                // Set
                [Account setTimeZone];
                
                // Alert User
                [self.view makeToast:NSLocalizedString(@"timezone_set", @"timezone_set")];
            }
        }
            break;
            
        case kSettingsVCSSIDAlert:
        {
            if (buttonIndex == 0) { // No
                
                
            } else if (buttonIndex == 1) { // Yes
                // Set
                [[UserPrefsHelper shared] setSSID:[[UserPrefsHelper shared] getCurrentWiFi]];
                
                // Alert User
                [self.view makeToast:NSLocalizedString(@"ssid_set", @"ssid_set")];
            }
        }
            break;
            
        case kSettingsVCSyncEventsAlert:
        {
            if (buttonIndex == 0) { // No
                
                
            } else if (buttonIndex == 1) { // Yes
                [[WFLHelper shared] updateWFL];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Actions

- (IBAction)buttonPressed:(id)sender {
    
    switch ([sender tag]) {
            
        case kBtnSSID:
        {
            LWErrorView *alert = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"warning", @"warning")
                                                            message:[NSString stringWithFormat:NSLocalizedString(@"set_ssid_message", nil),
                                                                     [[UserPrefsHelper shared] getSSID],
                                                                     [[UserPrefsHelper shared] getUserMac],
                                                                     [[UserPrefsHelper shared] getCurrentWiFi]]
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                  otherButtonTitles:NSLocalizedString(@"yes", @"Yes"), nil];
            [alert setTag:kSettingsVCSSIDAlert];
            [alert show];
        }
            break;
 
        case kBtnTimezone:
        {
            LWErrorView *alert = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"warning", @"warning")
                                                            message:[NSString stringWithFormat:NSLocalizedString(@"set_timezone_message", nil),
                                                                     [[UserPrefsHelper shared] getUserName],
                                                                     [[UserPrefsHelper shared] getUserMac],
                                                                     [Account getDeviceTimeZoneWithoutHours]]
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                  otherButtonTitles:NSLocalizedString(@"yes", @"Yes"), nil];
            [alert setTag:kSettingsVCTimeZoneAlert];
            [alert show];
        }
            break;
            
        case kBtnLogout:
        {
            LWErrorView *alert = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"log_out", @"Log out")
                                                            message:NSLocalizedString(@"log_out_message", @"You may have unsaved settings, would you like to save your settings to the server?")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"no", @"No")
                                                  otherButtonTitles:NSLocalizedString(@"yes", @"Yes"), nil];
            [alert setTag:kSettingsVCLogoutAlert];
            [alert show];
        }
            break;
            
        case kBtnSaveElecRate:
        {
            [self hideKeyBoard:nil];
            
            [[UserPrefsHelper shared] setElecRate:self.elec_txt.text];
            
            [self.view makeToast:NSLocalizedString(@"settings_saved", @"Settings saved")];
        }
            break;
            
        case kBtnSaveImpKwh:
        {
            [self hideKeyBoard:nil];
            
            if ([self.imp_txt.text intValue] > 32000)
                [[UserPrefsHelper shared] setImpKwh:@"32000"];
            else
                [[UserPrefsHelper shared] setImpKwh:self.imp_txt.text];

            // Send Command
            [[WFLHelper shared] sendToUDP:YES
                                  command:[ServiceType getImpKwhCommand:[[UserPrefsHelper shared] getImpKwh]]
                                      tag:kRequestImpKwh];

            [self.view makeToast:NSLocalizedString(@"settings_saved", @"Settings saved")];
        }
            break;
            
        case kBtnGetSettings:
        {
            LWErrorView *alert = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"get_settings", @"Get Settings")
                                                            message:NSLocalizedString(@"get_settings_msg", @"get settings?")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"no", @"No")
                                                  otherButtonTitles:NSLocalizedString(@"yes", @"Yes"), nil];
            [alert setTag:kSettingsVCGetSettingsAlert];
            [alert show];
        }
            break;
            
        case kBtnSaveSettings:
        {
            LWErrorView *alert = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"save_settings", @"Save Settings")
                                                            message:NSLocalizedString(@"save_settings_msg", @"save settings?")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"no", @"No")
                                                  otherButtonTitles:NSLocalizedString(@"yes", @"Yes"), nil];
            [alert setTag:kSettingsVCSaveSettingsAlert];
            [alert show];
        }
            break;
            
        case kBtnSyncDevice:
        {
            LWErrorView *alert = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"warning", @"warning")
                                                            message:NSLocalizedString(@"sync_events", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"no", @"No")
                                                  otherButtonTitles:NSLocalizedString(@"yes", @"Yes"), nil];
            [alert setTag:kSettingsVCSyncEventsAlert];
            [alert show];
        }
            break;
            
        case kBtnRegisterWiFiLink:
        {
            [[UDPService shared] pollWithTag:kRequestPollState
                                callingClass:[self getClassName:[self class]]];
        }
            break;
            
        case kBtnSetLocation:
        {
            [self showSpinnerWithText:@""];
            [[CurrentLocationHelper shared] getLocation];
        }
            break;
            
        case kBtnChooseLanguage:
        {
            ChooseLanguageVC *c = [[ChooseLanguageVC alloc] initWithNibName:kVCXibName bundle:nil];
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:c];
            [self presentViewController:nc animated:YES completion:^(void){
                c.isModal = YES;
            }];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - UDP Delegate

- (void)receivedUDPServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kUDPClassNameKey];
    NSUInteger _tag = [[_dict objectForKey:kUDPTagKey] intValue];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        switch (_tag) {
                
            case kRequestPollState:
            {
                [self receivedUDPPollHandler:_notification];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - UDP Handlers

- (void)receivedUDPPollHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    //NSString *_command = [_dict objectForKey:kUDPCommandKey];
    NSString *_response = [_dict objectForKey:kUDPResponseKey];
    //DebugLog(@"response = %@", _response);
    
    if ([[_notification name] isEqualToString:kUDPErrorNotificationKey] ||
        [[_notification name] isEqualToString:kUDPFailedNotificationKey])
    {
        /*************************
         * Error || Failed
         *************************/
        
        [self.view makeToast:NSLocalizedString(@"udp_error11", @"udp_error11")];
    }
    
    else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        
        if ([_response containsString:@"ERR"]) {
            
            // Not yet registered... check wifi link
            [self.view makeToast:NSLocalizedString(@"settings_wifi_register", @"settings_wifi_register")];
            
        } else {
            
            // Already registered
            [self.view makeToast:NSLocalizedString(@"already_registered", @"already_registered")];
            
        }
        
    }
}

#pragma mark - Remote Delegate

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kRemoteClassNameKey];
    NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        switch (_tag) {
                
            case kRequestPullHome:
                [self handleRemotePullHome:_notification];
                break;
                
            case kRequestPushHome:
                [self handleRemotePushHome:_notification];
                break;
                
            default:
                break;
        }
    }
}

- (void)handleRemotePushHome:(NSNotification *)_notification {
    
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
    {
        /*************************
         * Error
         *************************/
        NSError *error = [_dict objectForKey:kRemoteErrorKey];
        [self showError:[error localizedDescription]];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        
        [kApplicationDelegate.window makeToast:NSLocalizedString(@"settings_saved", @"Settings Saved")
                                      duration:3.0f
                                      position:@"center"];
        
        if (self.logoutAfterSaveSettings)
            [self performLogout];
        
    }
    
    else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Failed
         *************************/
        NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
        [self showAlert:_failed];
    }
    
}

- (void)handleRemotePullHome:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
    {
        /*************************
         * Error
         *************************/
        NSError *error = [_dict objectForKey:kRemoteErrorKey];
        [self showError:[error localizedDescription]];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        NSString *data = [_dict objectForKey:kRemoteResponseKey];
        data = [data stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        //DebugLog(@"SettingsVC webServiceGotData %@", data);
        XMLConfig *xmlConfig = [XMLConfig shared];
        
        if ([data isEqualToString:@"no"]) {
            [self showError:NSLocalizedString(@"login_error2", @"We cannot log you in. Please check email address and PIN are correct or setup an account if you are a new customer.")];
            
        } else if ([xmlConfig xmlToHome:data]){
            
            [[UserPrefsHelper shared] setUserEmail:self.email_txt.text];
            [[UserPrefsHelper shared] setUserPassword:[[UserPrefsHelper shared] getUserPassword]];
            [[UserPrefsHelper shared] setRegistered:YES];
            
            [self.view makeToast:NSLocalizedString(@"message3", @"Settings received")];
        }
    }
    
    else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Failed
         *************************/
        NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
        [self showAlert:_failed];
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
