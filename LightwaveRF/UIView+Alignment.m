//
//  UIView+Alignment.m
//  SSUILibrary
//
//  Created by James Munro on 26/07/2011.
//  Copyright 2011 Sync Studios. All rights reserved.
//

#import "UIView+Alignment.h"
#import "UIView+Utilities.h"


@implementation UIView (Alignment)

#pragma mark -
#pragma mark Relative view alignment

- (void)alignToLeftOfView:(UIView *)view padding:(CGFloat)padding {
    [self setFrameX:(view.frame.origin.x - self.frame.size.width) - padding];
}

- (void)alignToLeftOfView:(UIView *)view padding:(CGFloat)padding matchVertical:(BOOL)matchVertical {
    [self setFrameX:(view.frame.origin.x - self.frame.size.width) - padding];
    
    if (matchVertical)
        [self setFrameY:view.frame.origin.y];
}

- (void)alignToRightOfView:(UIView *)view padding:(CGFloat)padding {
    [self setFrameX:(view.frame.origin.x + view.frame.size.width) + padding];
}

- (void)alignToRightOfView:(UIView *)view padding:(CGFloat)padding matchVertical:(BOOL)matchVertical {
    [self setFrameX:(view.frame.origin.x + view.frame.size.width) + padding];
    
    if (matchVertical)
        [self setFrameY:view.frame.origin.y];
}

- (void)alignToTopOfView:(UIView *)view padding:(CGFloat)padding {
    [self setFrameY:(view.frame.origin.y - self.frame.size.height) - padding];
}

- (void)alignToTopOfView:(UIView *)view padding:(CGFloat)padding matchHorizontal:(BOOL)matchHorizontal {
    [self setFrameY:(view.frame.origin.y - self.frame.size.height) - padding];
    
    if (matchHorizontal)
        [self setFrameX:view.frame.origin.x];
}

- (void)alignToBottomOfView:(UIView *)view padding:(CGFloat)padding {
    [self setFrameY:(view.frame.origin.y + view.frame.size.height) + padding];
}

- (void)alignToBottomOfView:(UIView *)view padding:(CGFloat)padding matchHorizontal:(BOOL)matchHorizontal {
    [self setFrameY:(view.frame.origin.y + view.frame.size.height) + padding];
    
    if (matchHorizontal)
        [self setFrameX:view.frame.origin.x];
}

#pragma mark -
#pragma mark Relative point alignment

- (void)alignToLeftOfPoint:(CGPoint)point padding:(CGFloat)padding {
    [self setFrameX:(point.x - self.frame.size.width) - padding];
}

- (void)alignToLeftOfPoint:(CGPoint)point padding:(CGFloat)padding matchVertical:(BOOL)matchVertical {
    [self setFrameX:(point.x - self.frame.size.width) - padding];
    
    if (matchVertical)
        [self setFrameY:point.y];
}

- (void)alignToRightOfPoint:(CGPoint)point padding:(CGFloat)padding {
    [self setFrameX:point.x + padding];
}

- (void)alignToRightOfPoint:(CGPoint)point padding:(CGFloat)padding matchVertical:(BOOL)matchVertical {
    [self setFrameX:point.x + padding];
    
    if (matchVertical)
        [self setFrameY:point.y];
}

- (void)alignToTopOfPoint:(CGPoint)point padding:(CGFloat)padding {
    [self setFrameY:(point.y - self.frame.size.height) - padding];
}

- (void)alignToTopOfPoint:(CGPoint)point padding:(CGFloat)padding matchHorizontal:(BOOL)matchHorizontal {
    [self setFrameY:(point.y - self.frame.size.height) - padding];
    
    if (matchHorizontal)
        [self setFrameX:point.x];
}

- (void)alignToBottomOfPoint:(CGPoint)point padding:(CGFloat)padding {
    [self setFrameY:point.y + padding];
}

- (void)alignToBottomOfPoint:(CGPoint)point padding:(CGFloat)padding matchHorizontal:(BOOL)matchHorizontal {
    [self setFrameY:point.y + padding];
    
    if (matchHorizontal)
        [self setFrameX:point.x];
}

@end

