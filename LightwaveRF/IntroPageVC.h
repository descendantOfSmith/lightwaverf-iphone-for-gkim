//
//  IntroPageVC.h
//  LightwaveRF
//
//  Created by Nik Lever on 06/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroPageVC : UIViewController{
    int pageNumber;
}
@property (strong, nonatomic) IBOutlet UIImageView *intro_img;
- (id)initWithPageNumber:(int)page;

@end
