//
//  ButtonsHelper.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    kButtonGreenBacking = 10001,
    kButtonGreyBacking,
    kButtonGreySwitch,
    kButtonGreenPSwitch,
    kButtonGreenOSwitch,
    kButtonRadio,
    kButtonLock,
    kButtonArrowLeft,
    kButtonArrowRight,
    kButtonNone
    
} ButtonIconTypes;

typedef enum
{
    kButtonStateLo = 10001,
    kButtonStateHi
    
} ButtonIconStates;

@interface ButtonsHelper : NSObject

+ (UIImage *)getImageForType:(ButtonIconTypes)tmpType state:(ButtonIconStates)tmpState;

+ (void)setTextPositionForButton:(UIButton *)button forType:(ButtonIconTypes)tmpType;

+ (float)getIdealHeightForType:(ButtonIconTypes)tmpType;

+ (UIButton *)getButtonWithText:(NSString *)tmpText
                           type:(ButtonIconTypes)tmpType
                            tag:(ButtonTypes)tmpTag
                          width:(float)tmpWidth
                         height:(float)tmpHeight
                         target:(id)tmpTarget
                       selector:(SEL)tmpSelector;

+ (UIButton *)getButtonWithDynamicWidthText:(NSString *)tmpText
                                       type:(ButtonIconTypes)tmpType
                                        tag:(ButtonTypes)tmpTag
                               widthPadding:(float)tmpWidthPadding
                                     height:(float)tmpHeight
                                     target:(id)tmpTarget
                                   selector:(SEL)tmpSelector;

@end
