//
//  UDPService.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 24/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncUdpSocket.h"
#import "ServiceType.h"


#define kPollID                             100
#define kMeterID                            999
#define kMinTransactionID                   120
#define kMaxTransactionID                   990
#define kPortSend                           9760
#define kPortListen                         9761
#define kTimeout                            1.5f
#define kNumRetrys                          3
#define kHost                               @"255.255.255.255"

// NSNotification Center Delegates
#define kUDPErrorNotificationKey            @"UDP_Error_Notification"
#define kUDPSentNotificationKey             @"UDP_Sent_Notification"
#define kUDPFailedNotificationKey           @"UDP_Failed_Notification"

// Data Keys
#define kUDPDateKey                         @"UDP_Date"
#define kUDPCommandKey                      @"UDP_Command"
#define kUDPTransactionIDKey                @"UDP_TransactionID"
#define kUDPTagKey                          @"UDP_Tag"
#define kUDPClassNameKey                    @"UDP_ClassName"
#define kUDPErrorKey                        @"UDP_Error"
#define kUDPResponseKey                     @"UDP_ResponseString"
#define kUDPDidCompleteKey                  @"UDP_Did_Complete"
#define kUDPAllowSwitchToRemoteKey          @"UDP_Allow_Switch"
#define kUDPNumRetrysKey                    @"UDP_Num_Retrys"

@interface UDPService : ServiceType {
}

+ (id)shared;

// Main
- (void)send:(NSString *)_msg tag:(NSUInteger)_tag callingClass:(NSString *)_className;

// Helpers
- (void)pollWithTag:(NSUInteger)_tag callingClass:(NSString *)_className;
- (void)getMeterWithTag:(NSUInteger)_tag callingClass:(NSString *)_className;
- (void)stopAllEventsWithTag:(NSUInteger)_tag callingClass:(NSString *)_className;

@end
