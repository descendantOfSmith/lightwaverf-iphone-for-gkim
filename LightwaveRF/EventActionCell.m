//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import "EventActionCell.h"
#import "ButtonsHelper.h"

@interface EventActionCell ()
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, strong) UIButton *setDelayEditModeButton;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, assign) id <EventActionDelayCellDelegate> delegate;
@end

@implementation EventActionCell

@synthesize containingView, iconImageView, titleLabel, subTitleLabel, detailLabel;
@synthesize delayImageView, delayTitleLabel, delayDetailLabel, delayEditModeLabel, editDelayEditModeButton, setDelayEditModeButton;
@synthesize indexPath, delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
    }
    return self;
}

- (void)dealloc {
    
}

+ (float)getCellHeight {
    return 44.0f;
}

- (void)setupContainingView {
    /*************************************
     *  Containing View
     *************************************/
    if (!self.containingView) {
        self.containingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                                       0.0f,
                                                                       self.contentView.frame.size.width,
                                                                       [EventActionCell getCellHeight])];
        [self.containingView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.containingView];
    }
}

- (void)setupCellWithTitle:(NSString*)tmpTitle
                  subtitle:(NSString *)tmpSubtitle
                detailText:(NSString *)tmpDetailText
                    device:(Device *)_device
                 indexPath:(NSIndexPath *)_indexPath
                  delegate:(id<EventActionDelayCellDelegate>)_delegate {
    
    self.delegate = _delegate;
    self.indexPath = _indexPath;
    
    [self setupContainingView];

    /*************************************
     *  Icon
     *************************************/
    if (!self.iconImageView) {
        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,
                                                                           0.0f,
                                                                           36.0f,
                                                                           36.0f)];
        [self.iconImageView setContentMode:UIViewContentModeScaleAspectFit];
        self.iconImageView.image = [_device getIconImage];
        [self.containingView addSubview:self.iconImageView];
    }

    /*************************************
     *  Title Label
     *************************************/
    if (!self.titleLabel) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 18.0f)];
        [self.titleLabel setText:tmpTitle];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:[UIColor blackColor]];
        [self.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
        [self.containingView addSubview:self.titleLabel];
    } else {
        [self.titleLabel setText:tmpTitle];
    }
    
    /*************************************
     *  Subtitle Label
     *************************************/
    if (!self.subTitleLabel) {
        self.subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 18.0f)];
        [self.subTitleLabel setText:tmpSubtitle];
        [self.subTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self.subTitleLabel setTextColor:[UIColor darkGrayColor]];
        [self.subTitleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        [self.containingView addSubview:self.subTitleLabel];
    } else {
        [self.subTitleLabel setText:tmpSubtitle];
    }
    
    /*************************************
     *  Set Delay Edit Mode Button
     *************************************/
    if (!self.setDelayEditModeButton) {
        self.setDelayEditModeButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"set_delay", @"Set Delay")
                                                                   type:kButtonGreyBacking
                                                                    tag:kBtnAddDelay
                                                                  width:70.0f
                                                                 height:30.0f
                                                                 target:self
                                                               selector:@selector(buttonPressed:)];
        [self.containingView addSubview:self.setDelayEditModeButton];
    }
    
    /*************************************
     *  Detail Label
     *************************************/
    if (!self.detailLabel) {
        self.detailLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.detailLabel setText:tmpDetailText];
        [self.detailLabel setBackgroundColor:[UIColor clearColor]];
        [self.detailLabel setTextColor:[UIColor lightGrayColor]];
        [self.detailLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        [self.detailLabel sizeToFit];
        [self.containingView addSubview:self.detailLabel];
    } else {
        [self.detailLabel setText:tmpDetailText];
        [self.detailLabel sizeToFit];
    }
    
    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (void)setupCellWithDelay:(NSString *)_delay {
    
    [self setupContainingView];
    
    /*************************************
     *  Delay Icon
     *************************************/
    if (!self.delayImageView) {
        self.delayImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,
                                                                            0.0f,
                                                                            36.0f,
                                                                            36.0f)];
        [self.delayImageView setContentMode:UIViewContentModeScaleAspectFit];
        self.delayImageView.image = [UIImage imageNamed:@"icon-time.png"];
        [self.containingView addSubview:self.delayImageView];
    }
    
    /*************************************
     *  Title Label
     *************************************/
    if (!self.delayTitleLabel) {
        self.delayTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.delayTitleLabel setText:NSLocalizedString(@"delay", @"Delay")];
        [self.delayTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self.delayTitleLabel setTextColor:[UIColor blackColor]];
        [self.delayTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
        [self.delayTitleLabel sizeToFit];
        [self.containingView addSubview:self.delayTitleLabel];
    }
    
    /*************************************
     *  Delay Edit Mode Label
     *************************************/
    NSString *_delayDetailText = [_delay stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@ ", NSLocalizedString(@"up_to", @"Up to")]
                                                                   withString:@""];
    if (!self.delayEditModeLabel) {
        self.delayEditModeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.delayEditModeLabel setAlpha:0.0f];
        [self.delayEditModeLabel setText:_delayDetailText];
        [self.delayEditModeLabel setBackgroundColor:[UIColor clearColor]];
        [self.delayEditModeLabel setTextColor:[UIColor lightGrayColor]];
        [self.delayEditModeLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        [self.delayEditModeLabel sizeToFit];
        [self.containingView addSubview:self.delayEditModeLabel];
    } else {
        [self.delayEditModeLabel setText:_delayDetailText];
        [self.delayEditModeLabel sizeToFit];
    }

    /*************************************
     *  Set Delay Edit Mode Button
     *************************************/
    if (!self.editDelayEditModeButton) {
        self.editDelayEditModeButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"edit_delay", @"Edit Delay")
                                                                  type:kButtonGreyBacking
                                                                   tag:kBtnAddDelay
                                                                 width:70.0f
                                                                height:30.0f
                                                                target:self
                                                              selector:@selector(buttonPressed:)];
        //[self.containingView addSubview:self.setDelayEditModeButton];
        [self.contentView addSubview:self.editDelayEditModeButton];
    }

    /*************************************
     *  Detail Label
     *************************************/
    if (!self.delayDetailLabel) {
        self.delayDetailLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.delayDetailLabel setText:_delay];
        [self.delayDetailLabel setBackgroundColor:[UIColor clearColor]];
        [self.delayDetailLabel setTextColor:[UIColor lightGrayColor]];
        [self.delayDetailLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        [self.delayDetailLabel sizeToFit];
        [self.containingView addSubview:self.delayDetailLabel];
    } else {
        [self.delayDetailLabel setText:_delay];
        [self.delayDetailLabel sizeToFit];
    }
    
    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    float tmpRemainingSpace = (self.containingView.frame.size.height - self.titleLabel.frame.size.height) - self.subTitleLabel.frame.size.height;
    tmpRemainingSpace = tmpRemainingSpace / 2;
    
    // Icon Image
    [self.iconImageView setFrameX:tmpIsEditing ? 35.0f : 5.0f];
    [self.iconImageView setCenterY:self.containingView.center.y];
    
    // Title
    [self.titleLabel alignToRightOfView:self.iconImageView padding:5.0f];
    if (self.subTitleLabel.text.length > 0) {
        [self.titleLabel setFrameY:tmpRemainingSpace];
    } else {
        [self.titleLabel setCenterY:self.containingView.center.y];
    }
    
    // Subtitle
    [self.subTitleLabel alignToBottomOfView:self.titleLabel padding:0.0f matchHorizontal:YES];
    
    // Detail
    float padding = 10.0f;
    [self.detailLabel setFrameX:(self.containingView.frame.size.width - self.detailLabel.frame.size.width) - padding];
    [self.detailLabel setCenterY:self.containingView.center.y];
    [self.detailLabel setAlpha:tmpIsEditing ? 0.0f : 1.0f];
    
    // Set Delay Edit Mode Button
    if (self.setDelayEditModeButton && !self.editDelayEditModeButton) {
        [self.setDelayEditModeButton setFrameX:179.0f];
        [self.setDelayEditModeButton setCenterY:self.iconImageView.center.y];
        [self.setDelayEditModeButton setAlpha:tmpIsEditing ? 1.0f : 0.0f];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self positionViewsIsEditing:self.isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

#pragma mark - Actions

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnAddDelay:
        {
            if ([self.delegate respondsToSelector:@selector(setDelayPressed:indexPath:)])
                [self.delegate setDelayPressed:self indexPath:self.indexPath];
        }
            break;
            
        default:
            break;
    }
}

@end
