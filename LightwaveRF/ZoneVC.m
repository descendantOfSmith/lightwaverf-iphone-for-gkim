//
//  ZoneVC.m
//  LightwaveRF
//
//  Created by Nicholas Lever on 09/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "ZoneVC.h"
#import "AppDelegate.h"
#import "Zone.h"
#import "Device.h"
#import "LWErrorView.h"
#import "NSString+RangeOfCharacters.h"
#import "Toast+UIView.h"
#import "ActionSheetPicker.h"
#import "Home.h"
#import "Event.h"
#import "Action.h"
#import <objc/runtime.h>
#import "ButtonsHelper.h"
#import "HeatingVC.h"
#import "WFLHelper.h"
#import "NSString+Utilities.h"
#import "ColourLEDsVC.h"
#import "MoodVCCell.h"
#import "DimmerCellVCCell.h"
#import "DeviceCellVCCell.h"

@interface ZoneVC ()
@property (nonatomic, copy) NSString *addDeviceName;
@property (nonatomic, strong) NSIndexPath *connectingIndexPath;
@property (nonatomic, assign) BOOL showUnpairedDevices;
@property (nonatomic, strong) TempActionSheet *tempActionSheet;
@end

@implementation ZoneVC

@synthesize zone;
@synthesize tempActionSheet;
@synthesize devices_tbl;
@synthesize addDeviceName;
@synthesize connectingIndexPath;
@synthesize showUnpairedDevices;
@synthesize wifi_btn;

#define kAlertConnectIDKey          @"kAlertConnectIDKey"
#define kAlertDeviceIndexPathKey    @"kAlertDeviceIndexPathKey"

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.connectingIndexPath = nil;
        self.showUnpairedDevices = NO;
    }
    return self;
}

- (id)initWithZone:(Zone *)_zone
{
    self = [super initWithNibName:@"ZoneVC" bundle:nil];
    if (self) {
        // Custom initialization
        self.zone = _zone;
        [self setupView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    [devices_tbl setBackgroundColor:[AppStyleHelper getViewBackgroundColor]];
    
    self.navigationItem.title = zone.name;

    [self setupLeftNavBarButtons];
    [self setupRightNavBarButtons];
   
    _newdevice_txt = [[UITextField alloc] initWithFrame:CGRectMake(5,5,260,30)];
    _newdevice_txt.placeholder = NSLocalizedString(@"add_new_device", @"Add new device");
    _newdevice_txt.borderStyle = UITextBorderStyleRoundedRect;
    [_newdevice_txt addTarget:self action:@selector(addDeviceDonePressed:) forControlEvents:UIControlEventEditingDidEndOnExit];
    _newdevice_txt.returnKeyType = UIReturnKeyDone;
    
    // Help popup
    [self showOneOffAlertForLocalisedKey:@"help2" duration:5.0f];
    
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Check WiFi State
    [self checkWifiState];
    
    // Reload
    [self reloadViews];
}

- (void)viewDidDisappear:(BOOL)animated{
}

- (void)viewDidUnload
{
    [self setDevices_tbl:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UI Creation

- (void)setupUI {
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self setupTempActionSheet];
    
    if ([self.devices_tbl respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.devices_tbl setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)setupTempActionSheet {
    self.tempActionSheet = [[TempActionSheet alloc] initWithPoint:CGPointMake(0.0f, SCREEN_HEIGHT())
                                                     buttonTitles:[NSArray arrayWithObjects:
                                                                   NSLocalizedString(@"set_temp", @"Set Temp"),
                                                                   nil]
                                                      cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                       dataSource:self
                                                         delegate:self
                                                showNumComponents:3];
    [self.tempActionSheet setTag:kZoneVCSetTemp];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self.tempActionSheet];
}

- (void)checkWifiState {
    ServiceTypeTags _currentType = [[UDPService shared] getServiceType];
    
    if ([[WFLHelper shared] isCheckingState]) {
        if (_currentType == kServiceUDP)
            [self.wifi_btn setSelected:YES];
        
        else if  (_currentType == kServiceWeb)
            [self.wifi_btn setSelected:NO];
    } else
        [self.wifi_btn setSelected:NO];
}

#pragma mark - Nav Bar Buttons



- (void)setupLeftNavBarButtons {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *homeButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"home", nil)
                                                           type:kButtonArrowLeft
                                                            tag:0
                                                          width:50
                                                         height:18
                                                         target:self
                                                       selector:@selector(backPressed:)];
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    }
}

- (void)setupRightNavBarButtons {
    self.navigationItem.rightBarButtonItems = @[[self getNavBarEditButton], [self getNavBarAddButton]];
}

- (UIBarButtonItem *)getNavBarAddButton {
    if ([AppStyleHelper isMegaMan]) {
        
        UIButton *addButton = [ButtonsHelper getButtonWithDynamicWidthText:@"+"
                                                                      type:kButtonNone
                                                                       tag:0
                                                              widthPadding:10
                                                                    height:30
                                                                    target:self
                                                                  selector:@selector(add)];
        [addButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [addButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:24.0f]];
        return [[UIBarButtonItem alloc] initWithCustomView:addButton];
        
    } else {
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"+", @"+")
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(add)];
        
        return addButton;
    }
}

- (UIBarButtonItem *)getNavBarEditButton {
    if ([AppStyleHelper isMegaMan]) {
        
        UIButton *editButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"edit", nil)
                                                           type:kButtonArrowLeft
                                                            tag:0
                                                          width:40
                                                         height:18
                                                         target:self
                                                       selector:@selector(edit)];
        return [[UIBarButtonItem alloc] initWithCustomView:editButton];
        
    } else {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit", nil)
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self
                                                                      action:@selector(edit)];
        
        return editButton;
    }
}

- (UIBarButtonItem *)getNavBarDoneButton {
    if ([AppStyleHelper isMegaMan]) {
        
        UIButton *doneButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"done", nil)
                                                           type:kButtonArrowLeft
                                                            tag:0
                                                          width:50
                                                         height:18
                                                         target:self
                                                       selector:@selector(edit)];
        return [[UIBarButtonItem alloc] initWithCustomView:doneButton];
        
    } else {
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(edit)];
        return doneButton;
    }
}

#pragma mark -

- (NSMutableArray *)getDevices {
    
    // First of all, remove the Heating Control Device
    NSMutableArray *_devices = [zone.devices mutableCopy];
    NSString *_propertyToSearch2 = @"isHeating";
    NSPredicate *_removeHeatingPredicate = [NSPredicate predicateWithFormat:@"%K == %d", _propertyToSearch2, NO];
    _devices = [[_devices filteredArrayUsingPredicate:_removeHeatingPredicate] mutableCopy];

    if (self.showUnpairedDevices) {
        
        NSMutableArray *predicatesArray = [[NSMutableArray alloc] init];
        
        NSString *_propertyToSearch1 = @"paired";
        NSPredicate *_predicate1 = [NSPredicate predicateWithFormat:@"%K == %d", _propertyToSearch1, NO];
        [predicatesArray addObject:_predicate1];
        
        NSString *_propertyToSearch2 = @"isMood";
        NSPredicate *_predicate2 = [NSPredicate predicateWithFormat:@"%K == %d", _propertyToSearch2, NO];
        [predicatesArray addObject:_predicate2];
        
        NSPredicate *_predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicatesArray];

        return [[_devices filteredArrayUsingPredicate:_predicate] mutableCopy];
    } else
        return _devices;
}

-(void)setZone:(Zone *)_zone{
    self->zone = _zone;
    [self setupView];
}

- (void)setupView {
    self.navigationItem.title = zone.name;
    [self reloadViews];
}

-(void)reloadViews{
    [devices_tbl reloadData];
}

- (void)deviceNameDonePressed:(id)sender{
    UITextField *textField = (UITextField*)sender;
    [textField resignFirstResponder];
}

- (void)addDeviceDonePressed:(id)sender{
    [_newdevice_txt resignFirstResponder];
}

- (void)selectDeviceType {
    // Options
    NSString *_moodOne = NSLocalizedString(@"device_type_friendly4", @"Mood 1");
    NSString *_moodTwo = NSLocalizedString(@"device_type_friendly5", @"Mood 2");
    NSString *_moodThree = NSLocalizedString(@"device_type_friendly6", @"Mood 3");
    NSString *_lightDimmer = NSLocalizedString(@"device_type_friendly1", @"Light (dimmer)");
    NSString *_plugSocket = NSLocalizedString(@"device_type_friendly2", @"Plug socket");
    NSString *_openClose = NSLocalizedString(@"device_type_friendly3", @"Open Close");
    NSString *_heatingControl = NSLocalizedString(@"device_type_friendly7", @"Heating control");
    NSString *_colourLEDs = NSLocalizedString(@"device_type_friendly11", @"Colour LEDs");
    NSString *_fsl500W = @"FSL Demo 500W";
    NSString *_fsl3000W = @"FSL Demo 3000W";
    
    // Only show moods that arent already in a zone
    NSMutableArray *_moods = [NSMutableArray arrayWithObjects:
                              _moodOne,
                              _moodTwo,
                              _moodThree,
                              nil];
        
    for (Device *_device in zone.devices) {
        if ([_device isMood]) {
            if (_device.mood.moodID == 1)
                [_moods removeObject:_moodOne];
            else if (_device.mood.moodID == 2)
                [_moods removeObject:_moodTwo];
            else if (_device.mood.moodID == 3)
                [_moods removeObject:_moodThree];
        }
    }
    
    NSMutableArray *_types = nil;
    if ([AppStyleHelper isMegaMan])  {
        if ([zone isFreeDeviceOnlySlots]) {
            _types = [NSMutableArray arrayWithObjects:
                      _lightDimmer,
                      _plugSocket,
                      nil];
        }
    } else {
        if ([zone isFreeDeviceOnlySlots]) {
            _types = [NSMutableArray arrayWithObjects:
                      _lightDimmer,
                      _plugSocket,
                      _openClose,
                      nil];
        }
    }
    
    if ([AppStyleHelper isFSLDemo]) {
        NSArray *_fslTypes = [NSArray arrayWithObjects:
                              _fsl500W,
                              _fsl3000W,
                              nil];
        _types = [[_types arrayByAddingObjectsFromArray:_fslTypes] mutableCopy];
    }
    
    if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo]  || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO])  {
        
        // Only one heating control is allowed in a Zone. If the user already has a heating, then do not show it in the picker.
        BOOL _hideHeatingControl = [zone hasHeating];
        
        NSArray *_heatingTypes = [NSArray arrayWithObjects:
                                  _heatingControl,
                                  nil];
        
        if ([[UserPrefsHelper shared] isLockedToPrivateAccounts]) {
        
            [_types addObject:_colourLEDs];
            
            // Show Heating Control?
            if (!_hideHeatingControl)
                _types = [[_types arrayByAddingObjectsFromArray:_heatingTypes] mutableCopy];
        
        }
        
        // Show Moods?
        if (_moods.count > 0)
            _types = _types ? [[_types arrayByAddingObjectsFromArray:_moods] mutableCopy] : _moods;
    
    }
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        //DebugLog(@"Selected = %@", selectedValue);
        
        if ([selectedValue isEqualToString:_moodOne])
        {
            Mood *_mood = [[Mood alloc] initWithID:1 name:self.addDeviceName zone:zone];
            Device *_device = [[Device alloc] initWithMood:_mood];
            [zone addDevice:_device andSave:YES];
        }
        else if ([selectedValue isEqualToString:_moodTwo])
        {
            Mood *_mood = [[Mood alloc] initWithID:2 name:self.addDeviceName zone:zone];
            Device *_device = [[Device alloc] initWithMood:_mood];
            [zone addDevice:_device andSave:YES];
        }
        else if ([selectedValue isEqualToString:_moodThree])
        {
            Mood *_mood = [[Mood alloc] initWithID:3 name:self.addDeviceName zone:zone];
            Device *_device = [[Device alloc] initWithMood:_mood];
            [zone addDevice:_device andSave:YES];
        }
        else if ([selectedValue isEqualToString:_lightDimmer])
        {
            Device *_device = [[Device alloc] initWithName:self.addDeviceName deviceID:[zone getFreeDeviceID] type:@"D" lastOnState:0 paired:NO zone:zone];
            [zone addDevice:_device andSave:YES];
        }
        else if ([selectedValue isEqualToString:_plugSocket])
        {
            Device *_device = [[Device alloc] initWithName:self.addDeviceName deviceID:[zone getFreeDeviceID] type:@"O" zone:zone];
            [zone addDevice:_device andSave:YES];
        }
        else if ([selectedValue isEqualToString:_openClose])
        {
            Device *_device = [[Device alloc] initWithName:self.addDeviceName deviceID:[zone getFreeDeviceID] type:@"P" zone:zone];
            [zone addDevice:_device andSave:YES];
        }
        else if ([selectedValue isEqualToString:_heatingControl])
        {
            Device *_device = [[Device alloc] initWithName:self.addDeviceName deviceID:[zone getFreeDeviceID] type:@"H" zone:zone];
            [zone addDevice:_device andSave:YES];
        }
        else if ([selectedValue isEqualToString:_colourLEDs])
        {
            Device *_device = [[Device alloc] initWithName:self.addDeviceName deviceID:[zone getFreeDeviceID] type:@"L" zone:zone];
            [zone addDevice:_device andSave:YES];
        }
        else if ([selectedValue isEqualToString:_fsl500W])
        {
            Device *_device = [[Device alloc] initWithName:self.addDeviceName deviceID:[zone getFreeDeviceID] type:@"Q" zone:zone];
            [zone addDevice:_device andSave:YES];
        }
        else if ([selectedValue isEqualToString:_fsl3000W])
        {
            Device *_device = [[Device alloc] initWithName:self.addDeviceName deviceID:[zone getFreeDeviceID] type:@"R" zone:zone];
            [zone addDevice:_device andSave:YES];
        }
        
        [self reloadViews];
    };
    
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        DebugLog(@"Block Picker Cancelled");
    };
    
    if (_types.count > 0) {
        [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"type", @"Type")
                                                rows:_types
                                    initialSelection:0
                                           doneBlock:done
                                         cancelBlock:cancel
                                              origin:self.view];
    }
}

#pragma mark - CustomActionSheet Datasource

- (UIView *)presentedFromView:(LightwaveActionSheet *)tmpCustomActionSheet {
    return self.view;
}

- (UIColor *)actionSheetColourTypes:(LightwaveActionSheet *)tmpCustomActionSheet type:(CustomActionSheetStyleColours)_type {
    if (_type == kCustomActionSheetBackgroundColour)
        return [UIColor colorWithDivisionOfRed:96.0f green:101.0f blue:111.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientStart)
        return [UIColor colorWithDivisionOfRed:166.0f green:169.0f blue:175.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientEnd)
        return [UIColor colorWithDivisionOfRed:123.0f green:128.0f blue:136.0f alpha:1.0f];
    else
        return [UIColor clearColor];
}

- (UIImage *)actionSheetButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreenBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (UIImage *)actionSheetCancelButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreyBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (float)actionSheetButtonHeight:(LightwaveActionSheet *)tmpCustomActionSheet {
    return [AppStyleHelper isMegaMan] ? 40.0f : 35.0f;
}

#pragma mark - CustomActionSheet Delegate

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet index:(NSUInteger)tmpIndex pickerView:(UIPickerView *)tmpPickerView {
    if ([tmpCustomActionSheet tag] == kZoneVCSetTemp) {
        /*****************************************
         * SET TEMP
         *****************************************/
        switch (tmpIndex) {
                
            case kZoneVCSetTempSetTemp:
            {
                NSString *_selectedTemp = [[tmpPickerView delegate] pickerView:tmpPickerView
                                                                   titleForRow:[tmpPickerView selectedRowInComponent:0]
                                                                  forComponent:0];
                
                NSString *_selectedHours = [[tmpPickerView delegate] pickerView:tmpPickerView
                                                                   titleForRow:[tmpPickerView selectedRowInComponent:1]
                                                                  forComponent:1];
                
                NSString *_selectedMins = [[tmpPickerView delegate] pickerView:tmpPickerView
                                                                   titleForRow:[tmpPickerView selectedRowInComponent:2]
                                                                  forComponent:2];
                
                DebugLog(@"Temp: %@", _selectedTemp);
                DebugLog(@"Hours: %@", _selectedHours);
                DebugLog(@"Mins: %@", _selectedMins);
                
#warning - TODO Heating
                
                [self.tempActionSheet toggleActionSheetWithDelay:0.0f];
                [self reloadViews];
            }
                break;
                
            case kZoneVCSetTempCancel:
            {
                [self.tempActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - UITextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField*)textfield {
    CGPoint pointInTable = [textfield.superview convertPoint:textfield.frame.origin toView:self.devices_tbl];
    CGPoint contentOffset = self.devices_tbl.contentOffset;

    CGPoint pnt = [self.devices_tbl convertPoint:textfield.bounds.origin fromView:textfield];
    NSIndexPath *_indexPath = [self.devices_tbl indexPathForRowAtPoint:pnt];
    float _offset = [self tableView:devices_tbl heightForRowAtIndexPath:_indexPath];
    contentOffset.y = ((pointInTable.y - textfield.inputAccessoryView.frame.size.height) - _offset);
        
    [self.devices_tbl setContentOffset:contentOffset animated:YES];
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = (UITableViewCell*)textField.superview.superview;
        NSIndexPath *indexPath = [self.devices_tbl indexPathForCell:cell];
        
        [self.devices_tbl scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    //DebugLog(@"%@", textField.text);
    CGPoint pnt = [devices_tbl convertPoint:textField.bounds.origin fromView:textField];
    NSIndexPath* path = [devices_tbl indexPathForRowAtPoint:pnt];
    Device *device = [[self getDevices] objectAtIndex:path.row];
    device.name = textField.text;
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch ([alertView tag]) {
            
        case kZoneVCDeleteDeviceAlert:
        {
            if (buttonIndex != 0) { // Ok
                NSIndexPath *_indexPath = objc_getAssociatedObject(alertView, kAlertDeviceIndexPathKey);
                [self deleteDeviceForIndexPath:_indexPath];
            }
        }
            break;
            
        case kZoneVCStartPairingAlert:
        {
            if (buttonIndex != 0) { // Ok
                NSNumber *_connectID = objc_getAssociatedObject(alertView, kAlertConnectIDKey);
                self.connectingIndexPath = [NSIndexPath indexPathForRow:[_connectID intValue] inSection:0];
                Device *_device = [[self getDevices] objectAtIndex:self.connectingIndexPath.row];
                
               [self connectDeviceUsingUDP:[NSNumber numberWithBool:YES]
                                    command:[NSString stringWithFormat:@"!R%dD%dF1|%@|%@",
                                             zone.zoneID,
                                             _device.deviceID,
                                             NSLocalizedString(@"device", @"device"),
                                             NSLocalizedString(@"connected", @"Connected")]];
            }
        }
            break;
            
        case kZoneVCAddDeviceAlert:
        {
            NSString *text = [[alertView textFieldAtIndex:0] text];
            if (buttonIndex != 0) { // Ok
                NSString *_validationStr = [self isValidDeviceName:text];
                //DebugLog(@"_validationStr: %@", _validationStr);
                
                if (_validationStr) {
                    [self showError:_validationStr];
                    
                } else {
                    
                    // Make sure is unique
                    self.addDeviceName = [self getValidDeviceName:text];
                    SEL _selector = @selector(selectDeviceType);
                    if ([self respondsToSelector:_selector]) {
                        [self performSelector:_selector withObject:nil afterDelay:0.2f];
                    }
                    
                }
            }
        }
            break;
            
        default:
            break;
    }
}

- (NSString *)isValidDeviceName:(NSString *)_text {
    
    NSMutableString *_validationStr = [NSMutableString string];
    
    if (_text.length <= 0) {
        [_validationStr appendString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"message17", @"Enter a valid device name.")]];
        
    } else if (_text.length > 16) {
        [_validationStr appendString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"message21", @"16 char limit.")]];
        
    }
    
    // Device naming must be restricted to alphanumeric characters and -, _ and space. 
    NSCharacterSet *_charSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_ "];
    _charSet = [_charSet invertedSet];
    NSRange r = [_text rangeOfCharacterFromSet:_charSet];
    if (r.location != NSNotFound) {
        [_validationStr appendString:[NSString stringWithFormat:@"%@\n", NSLocalizedString(@"message18", @"restricted to alphanumeric characters and -, _ and space.")]];
    }
        
    return _validationStr.length > 0 ? _validationStr : nil;
}

- (NSString *)getValidDeviceName:(NSString *)_text {
    // Names of devices in a Zone MUST be unique.
    // If the name is not unique then append with an integer starting with 1.
    for (Device *_device in zone.devices) {
        if ([_device.name isEqualToString:_text]) {
            
            // Name already exists, append with an integer
            // First of all check if there is already a number at the end of the string
            NSInteger _appendInt = 1;
            NSString *_number = [_text substringFromSet:[NSCharacterSet decimalDigitCharacterSet]
                                                options:NSBackwardsSearch|NSAnchoredSearch];
            if (_number != nil) {
                // Found number at end, so add 1
                _appendInt = [_number intValue] + 1;
                
                // Trim the old number off the end
                _text = [_text substringToIndex:[_text length] - [_number length]];
            }

            _text = [NSString stringWithFormat:@"%@%d", _text, _appendInt];
            return [self getValidDeviceName:_text];
        }
    }

    return _text;
}

#pragma mark - Actions

-(void)add{
    if (self.editing)
        return;
    
    if ([zone isFreeDeviceSlots]) {

        NSString *_alertTitle = nil;
        
        if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo]  || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO])
            _alertTitle = NSLocalizedString(@"device_mood", @"Device/Mood");
        else
            _alertTitle = NSLocalizedString(@"device", @"Device");
        
        LWErrorView *alert = [[LWErrorView alloc] initWithTitle:_alertTitle
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel")
                                              otherButtonTitles:NSLocalizedString(@"ok", @"Ok"), nil];
        [alert setTag:kZoneVCAddDeviceAlert];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField * alertTextField = [alert textFieldAtIndex:0];
        alertTextField.keyboardType = UIKeyboardTypeDefault;
        alertTextField.placeholder = NSLocalizedString(@"name", @"Name");
        alertTextField.text = @"";
        [alertTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        [alert show];
        
    } else {
        [self.view makeToast:[NSString stringWithFormat:NSLocalizedString(@"error_devices_limit", nil), MAX_PRODUCTS]];
    }
}

- (void)edit{
    
    if (self.editing){
        // DONE Pressed
        [self disableEditing];
        
    } else {
        // EDIT Pressed
        [self enableEditing];
    }
}

- (void)disableEditing {
    self.showUnpairedDevices = NO;
    self.connectingIndexPath = nil;
    [zone saveDevicesToDB];
    [super setEditing:NO animated:NO];
    [devices_tbl setEditing:NO animated:NO];
    [devices_tbl reloadData];
    
    self.navigationItem.rightBarButtonItem = [self getNavBarEditButton];
    
    // Enable add button
    UIBarButtonItem *_addButton = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
    [_addButton setEnabled:YES];    
}

- (void)enableEditing {
    [super setEditing:YES animated:YES];
    [devices_tbl setEditing:YES animated:YES];
    [devices_tbl reloadData];
    
    UIBarButtonItem *doneButton = [self getNavBarDoneButton];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    // Disable add button
    UIBarButtonItem *_addButton = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
    [_addButton setEnabled:NO];
}

- (IBAction)connectPressed:(id)sender{
    UIButton *btn = (UIButton *)sender;    
    LWErrorView *alert = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"start_pairing", @"Start Pairing")
                                                    message:NSLocalizedString(@"device_pairing_start", @"Put your product in pairing mode and press 'OK'")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel")
                                          otherButtonTitles:NSLocalizedString(@"ok", @"Ok"), nil];
    [alert setTag:kZoneVCStartPairingAlert];
    objc_setAssociatedObject(alert, kAlertConnectIDKey, [NSNumber numberWithInt:btn.tag - kZoneVCConnectButton], OBJC_ASSOCIATION_RETAIN);
    [alert show];
}

- (IBAction)saveMoodPressed:(id)sender {
    UIButton *btn = (UIButton *)sender;
    Device *_device = [[self getDevices] objectAtIndex:btn.tag - kZoneVCSaveMoodButton];
    
    if (!self.editing) {
        if ([zone hasHeating])
            _device = [[self getDevices] objectAtIndex:(btn.tag - kZoneVCSaveMoodButton) - 1];
    }
    
    DebugLog(@"SAVE MOOD: %@", _device.name);
    
    [self saveMoodUsingUDP:[NSNumber numberWithBool:YES]
                   command:[UDPService getSaveMoodCommandWithMood:_device.mood]];
}

- (IBAction)lockPressed:(id)sender{
    UIButton *btn = (UIButton *)sender;
    //DebugLog(@"Lock pressed %d", btn.tag);
    
    // Get the device
    Device *_device = [[self getDevices] objectAtIndex:btn.tag - kZoneVCLockButton];
    
    if (!self.editing) {
        if ([zone hasHeating])
            _device = [[self getDevices] objectAtIndex:(btn.tag - kZoneVCLockButton) - 1];
    }
    
    DebugLog(@"LOCK: %@", _device.name);
    
    // Lock
    [self toggleLockDeviceUsingUDP:[NSNumber numberWithBool:YES]
                           command:[UDPService getLockCommandWithZoneID:zone.zoneID device:_device]];
}

- (IBAction)fullLockPressed:(id)sender{
    UIButton *btn = (UIButton *)sender;
    Device *_device = [[self getDevices] objectAtIndex:btn.tag - kZoneVCFullLockButton];
    
    if (!self.editing) {
        if ([zone hasHeating])
            _device = [[self getDevices] objectAtIndex:(btn.tag - kZoneVCFullLockButton) - 1];
    }
    
    DebugLog(@"FULL LOCK: %@", _device.name);
    
    // FULL Lock
    [self toggleLockDeviceUsingUDP:[NSNumber numberWithBool:YES]
                           command:[UDPService getFullLockCommandWithZoneID:zone.zoneID device:_device]];
}

- (IBAction)unlockPressed:(id)sender{
    UIButton *btn = (UIButton *)sender;
    Device *_device = [[self getDevices] objectAtIndex:btn.tag - kZoneVCUnlockButton];
    
    if (!self.editing) {
        if ([zone hasHeating])
            _device = [[self getDevices] objectAtIndex:(btn.tag - kZoneVCUnlockButton) - 1];
    }
    
    DebugLog(@"UNLOCK LOCK: %@", _device.name);

    // Unlock
    [self toggleLockDeviceUsingUDP:[NSNumber numberWithBool:YES]
                           command:[UDPService getUnlockCommandWithZoneID:zone.zoneID device:_device]];
}

- (IBAction)linkPressed:(id)sender {
    DebugLog(@"linkPressed");
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cellButtonAction:(id)sender{
    UIButton *btn = (UIButton *)sender;
    DebugLog(@"Cell button pressed %i", btn.tag);
}

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnPairDevices:
        {
            self.showUnpairedDevices = YES;
            [self enableEditing];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - HomeHeatingCell Delegate

- (void)heatingPressed:(HomeHeatingCell *)_heatingCellVC
                option:(HeatingCellVCOptions)_option
             indexPath:(NSIndexPath *)_indexPath {
        
    switch (_option) {
            
        case kHeatingCellVCBoost:
        {
#warning - TODO Heating
        }
            break;
            
        case kHeatingCellVCStandby:
        {
#warning - TODO Heating 
        }
            break;
            
        case kHeatingCellVCProgram:
        {
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[[HeatingVC alloc] initWithNibName:kVCXibName bundle:nil zone:zone]];
            [self presentViewController:nc animated:YES completion:^(void){
                
            }];
        }
            break;
            
        case kHeatingCellVCSetTemp:
        {
            [self.tempActionSheet toggleActionSheetWithDelay:0.0f];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - UITableView

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Device *device;
        
    if (self.editing){
        return 80;
    }else{
        if (zone!=nil){
            int row = indexPath.row;
            BOOL hasHeating = [zone hasHeating];
            
            if (indexPath.section==0 && indexPath.row==0){
                if (hasHeating )
                    return 96;
            }
            
            if (hasHeating)
                row--;//Heating goes in first slot
            
            device = [[self getDevices] objectAtIndex:row];
            
            if (device == nil){
                return 44;
                
            } else if (device.locked == kDeviceFullyLocked || device.locked == kDeviceLocked) {
                return 50;
                
            } else if ([device isDimmer] || [device isColourLEDS]) {
                return 92;
                
            } else{
                return 50;
            }
        }else{
            return 44;
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSString *)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (zone==nil)
        return 0;
    
    //return [zone getDeviceCount];
    
    if ([self getDevices] == nil)
        return 0;
    
    int count = [[self getDevices] count];
    
    if (self.editing)
        return count;
    
    if ([zone hasHeating])
        count++;
    
    return count;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    if ([zone numUnpairedDevices] > 0 && !self.showUnpairedDevices)
        return 50.0f;
    else
        return 0.0f;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    NSUInteger _numUnpaired = [zone numUnpairedDevices];
    
    if (_numUnpaired > 0 && !self.showUnpairedDevices) {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, devices_tbl.frame.size.width, [self tableView:tableView heightForFooterInSection:section])];
        [footerView setBackgroundColor:[UIColor darkGrayColor]];

        UIButton *_button = nil;
        NSString *_str = [NSString stringWithFormat:@"%d %@ %@",
                          _numUnpaired, _numUnpaired > 1 ? NSLocalizedString(@"devices", @"devices") : NSLocalizedString(@"device", @"device"),
                          NSLocalizedString(@"unpaired", @"unpaired")];
        
        _button = [ButtonsHelper getButtonWithText:[_str lowercaseString]
                                                type:kButtonGreyBacking
                                                 tag:kBtnPairDevices
                                               width:260.0f
                                              height:30.0f
                                              target:self
                                            selector:@selector(buttonPressed:)];
        [_button setCenterX:footerView.center.x];
        [_button setCenterY:footerView.center.y];
        [footerView addSubview:_button];
        return footerView;
        
    } else {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    const char* className = class_getName([self class]);
    
    if (zone != nil){
        if (self.editing){
            NSString *cellIdentifier = [NSString stringWithFormat:@"UITableViewCell-%s-Editing", className];
            
            // Slightly hacky but eventually need to fix this to start reusing the cell rather
            // than creating it everytime. Currently with the re-use code, layouts arent correct of buttons etc
            
            //UITableViewCell *cell = [devices_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
            UITableViewCell *cell = nil;
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                
                UITextField *device_txt = [[UITextField alloc] initWithFrame:CGRectMake(5,5,230,30)];
                device_txt.tag = indexPath.row + kZoneVCDeviceTextField;
                device_txt.borderStyle = UITextBorderStyleRoundedRect;
                device_txt.delegate = self;
                [device_txt addTarget:self action:@selector(deviceNameDonePressed:) forControlEvents:UIControlEventEditingDidEndOnExit];
                device_txt.returnKeyType = UIReturnKeyDone;
                cell.textLabel.hidden = YES;
                [cell.contentView addSubview:device_txt];
            }
            
            Device *device = [[self getDevices] objectAtIndex:indexPath.row];
            
            UITextField *device_txt = (UITextField *)[cell.contentView viewWithTag:indexPath.row + kZoneVCDeviceTextField];
            UIButton *saveMood_btn = (UIButton *)[cell.contentView viewWithTag:kZoneVCSaveMoodButton + indexPath.row];
            UIButton *connect_btn = (UIButton *)[cell.contentView viewWithTag:kZoneVCConnectButton + indexPath.row];
            UIButton *fullLock_btn = (UIButton *)[cell.contentView viewWithTag:kZoneVCFullLockButton + indexPath.row];
            UIButton *lock_btn = (UIButton *)[cell.contentView viewWithTag:kZoneVCLockButton + indexPath.row];
            UIButton *unlock_btn = (UIButton *)[cell.contentView viewWithTag:kZoneVCUnlockButton + indexPath.row];
            UIButton *link_btn = (UIButton *)[cell.contentView viewWithTag:kZoneVCLinkButton + indexPath.row];

            device_txt.text = device.name;
            [saveMood_btn setHidden:YES];
            [connect_btn setHidden:YES];
            [fullLock_btn setHidden:YES];
            [lock_btn setHidden:YES];
            [unlock_btn setHidden:YES];
            [link_btn setHidden:YES];
            
            if ([device isMood]) {
                
                if (device.mood.moodID == 1 ||
                    device.mood.moodID == 2 ||
                    device.mood.moodID == 3) {
                    
                    if (!saveMood_btn) {
                        saveMood_btn = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"save_mood", @"Save Mood") uppercaseString]
                                                                   type:kButtonGreyBacking
                                                                    tag:(NSInteger)kZoneVCSaveMoodButton + indexPath.row
                                                                  width:90
                                                                 height:40
                                                                 target:self
                                                               selector:@selector(saveMoodPressed:)];
                        [saveMood_btn setFrameX:140];
                        [saveMood_btn setFrameY:38];
                        [cell.contentView addSubview:saveMood_btn];
                    }
                    
                    [saveMood_btn setHidden:NO];
                    
                } else {
                    [device_txt setFrameY:([self tableView:tableView heightForRowAtIndexPath:indexPath] - device_txt.frame.size.height) / 2];
                    
                    // if ALL OFF, disable editing name
                    if ([device.mood isAllOff])
                        [device_txt setEnabled:NO];
                }
                
            } else {
                if (![self.connectingIndexPath isEqual:indexPath]) {
                    
                    if (!connect_btn) {
                       
                        if ([AppStyleHelper isMegaMan])  {
                            
                            /*
                            connect_btn = [UIButton buttonWithType:UIButtonTypeCustom];
                            [connect_btn setImage:[UIImage imageNamed:@"connect_btn"] forState:UIControlStateNormal];
                            [connect_btn setImage:[UIImage imageNamed:@"connect_btn"] forState:UIControlStateSelected];
                            connect_btn.frame = CGRectMake(187,38,38,40);
                            connect_btn.tag = (NSInteger)kZoneVCConnectButton + indexPath.row;
                            [connect_btn addTarget:self action:@selector(connectPressed:) forControlEvents:UIControlEventTouchUpInside];
                             */
                            
                            connect_btn = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"connect", @"Connect") uppercaseString]
                                                                      type:kButtonArrowRight
                                                                       tag:(NSInteger)kZoneVCConnectButton + indexPath.row
                                                                     width:80
                                                                    height:18
                                                                    target:self
                                                                  selector:@selector(connectPressed:)];
                            [connect_btn setFrameX:7];
                            [connect_btn setFrameY:50];
                            
                        } else {
                            connect_btn = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"connect", @"Connect") uppercaseString]
                                                                      type:kButtonGreyBacking
                                                                       tag:(NSInteger)kZoneVCConnectButton + indexPath.row
                                                                     width:85
                                                                    height:40
                                                                    target:self
                                                                  selector:@selector(connectPressed:)];
                            [connect_btn setFrameX:145];
                            [connect_btn setFrameY:36];
                        }
                        
                        [cell.contentView addSubview:connect_btn];
                    }
                    
                    [connect_btn setHidden:NO];
                    
                    if ([device isPaired]) {
                        if ([[connect_btn layer] animationForKey:@"flash"])
                            [[connect_btn layer] removeAnimationForKey:@"flash"];
                    } else {
                        CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"opacity"];
                        [anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                        [anim setFromValue:[NSNumber numberWithFloat:0.5]];
                        [anim setToValue:[NSNumber numberWithFloat:1.0]];
                        [anim setAutoreverses:YES];
                        [anim setDuration:0.5];
                        [anim setRepeatCount:HUGE_VALF];
                        [[connect_btn layer] addAnimation:anim forKey:@"flash"];
                    }
                }
                
                if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo]  || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO])  {

                    if ([device isPaired]) {
                        
                        if (!fullLock_btn) {
                            fullLock_btn = [UIButton buttonWithType:UIButtonTypeCustom];
                            [fullLock_btn setImage:[UIImage imageNamed:@"full_lock"] forState:UIControlStateNormal];
                            [fullLock_btn setImage:[UIImage imageNamed:@"full_lock"] forState:UIControlStateSelected];
                            fullLock_btn.frame = CGRectMake(0.0f,38,38,40);
                            fullLock_btn.tag = (NSInteger)kZoneVCFullLockButton + indexPath.row;
                            [fullLock_btn addTarget:self action:@selector(fullLockPressed:) forControlEvents:UIControlEventTouchUpInside];
                            
                            if ([AppStyleHelper isMegaMan])
                                [fullLock_btn alignToRightOfView:connect_btn padding:20.0f];
                            else
                                [fullLock_btn setFrameX:7.0f];
                            
                            [cell.contentView addSubview:fullLock_btn];
                        }
                        
                        [fullLock_btn setHidden:NO];
                        
                        if (!lock_btn) {
                            lock_btn = [UIButton buttonWithType:UIButtonTypeCustom];
                            [lock_btn setImage:[UIImage imageNamed:@"lock_btn"] forState:UIControlStateNormal];
                            [lock_btn setImage:[UIImage imageNamed:@"lock_btn"] forState:UIControlStateSelected];
                            lock_btn.frame = CGRectMake(0,38,38,40);
                            lock_btn.tag = (NSInteger)kZoneVCLockButton + indexPath.row;
                            [lock_btn addTarget:self action:@selector(lockPressed:) forControlEvents:UIControlEventTouchUpInside];
                            [lock_btn alignToRightOfView:fullLock_btn padding:7.0f matchVertical:YES];
                            [cell.contentView addSubview:lock_btn];
                        }
                        
                        [lock_btn setHidden:NO];
                        
                        if (!unlock_btn) {
                            unlock_btn = [UIButton buttonWithType:UIButtonTypeCustom];
                            [unlock_btn setImage:[UIImage imageNamed:@"unlock_btn"] forState:UIControlStateNormal];
                            [unlock_btn setImage:[UIImage imageNamed:@"unlock_btn"] forState:UIControlStateSelected];
                            unlock_btn.frame = CGRectMake(0,38,38,40);
                            unlock_btn.tag = (NSInteger)kZoneVCUnlockButton + indexPath.row;
                            [unlock_btn addTarget:self action:@selector(unlockPressed:) forControlEvents:UIControlEventTouchUpInside];
                            [unlock_btn alignToRightOfView:lock_btn padding:7.0f matchVertical:YES];
                            [cell.contentView addSubview:unlock_btn];
                        }
                        
                        [unlock_btn setHidden:NO];
                        
                        /*
                        if ([AppStyleHelper isMegaMan])  {
                            if (!link_btn) {
                                link_btn = [UIButton buttonWithType:UIButtonTypeCustom];
                                [link_btn setImage:[UIImage imageNamed:@"link_btn"] forState:UIControlStateNormal];
                                [link_btn setImage:[UIImage imageNamed:@"link_btn"] forState:UIControlStateSelected];
                                link_btn.frame = CGRectMake(0,38,38,40);
                                link_btn.tag = (NSInteger)kZoneVCLinkButton + indexPath.row;
                                [link_btn addTarget:self action:@selector(linkPressed:) forControlEvents:UIControlEventTouchUpInside];
                                [link_btn alignToRightOfView:unlock_btn padding:7.0f matchVertical:YES];
                                [cell.contentView addSubview:link_btn];
                            }
                            
                            [link_btn setHidden:NO];
                        }
                         */
                    }
                
                }
            }
            
            return cell;
            
        } else {
            
            if ([zone hasHeating] && indexPath.row == 0){
                
                NSString *cellIdentifier = cellIdentifier = [NSString stringWithFormat:@"HomeHeatingCell-%s", className];
                HomeHeatingCell *cell = (HomeHeatingCell*)[devices_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
                cell = [self setupInitialCell:cell xibName:@"HomeHeatingCell"];
                
                // Setup Cell
                [cell setupCellWithZone:zone
                              indexPath:indexPath
                               delegate:self];
                
                return cell;
                
            } else {
                
                Device *device = nil;
                if ([zone hasHeating])
                    device = [[self getDevices] objectAtIndex:indexPath.row - 1];
                else
                    device = [[self getDevices] objectAtIndex:indexPath.row];
                
                if (device.locked == kDeviceFullyLocked || device.locked == kDeviceLocked) {
                    
                    NSString *cellIdentifier = [NSString stringWithFormat:@"UITableViewCell-%s-LockedCell", className];
                    UITableViewCell *cell = [devices_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
                    
                    if (cell == nil) {
                        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
                        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                        [cell setAccessoryType:UITableViewCellAccessoryNone];
                        
                        UIImageView *_imageView = [[UIImageView alloc] initWithFrame:CGRectMake(6.0f, 10.0f, 30.0f, 30.0f)];
                        [_imageView setTag:kZoneVCDeviceImageView];
                        [_imageView setContentMode:UIViewContentModeScaleAspectFit];
                        [_imageView setAlpha:0.3f];
                        [cell.contentView addSubview:_imageView];
                        
                        UILabel *_label = [[UILabel alloc] initWithFrame:CGRectZero];
                        [_label setTag:kZoneVCDeviceLabel];
                        [_label setBackgroundColor:[UIColor clearColor]];
                        [_label setAlpha:0.3f];
                        [cell.contentView addSubview:_label];
                        
                        UIButton *unlock_btn = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"unlock", @"UNLOCK") uppercaseString]
                                                                           type:kButtonLock
                                                                            tag:(NSInteger)kZoneVCUnlockButton + indexPath.row
                                                                          width:85
                                                                         height:34
                                                                         target:self
                                                                       selector:@selector(unlockPressed:)];
                        [unlock_btn setFrameX:223];
                        [unlock_btn setCenterY:_imageView.center.y];
                        [cell.contentView addSubview:unlock_btn];
                    }
                    
                    UIImageView *_imageView = (UIImageView *)[cell.contentView viewWithTag:kZoneVCDeviceImageView];
                    [_imageView setImage:[device getIconImage]];
                    
                    UILabel *_label = (UILabel *)[cell.contentView viewWithTag:kZoneVCDeviceLabel];
                    [_label setText:device.name];
                    [_label sizeToFit];
                    [_label alignToRightOfView:_imageView padding:3.0f];
                    [_label setCenterY:_imageView.center.y];
                    
                    return cell;
                    
                } else {
                    if ([device isMood]){
                        
                        NSString *cellIdentifier = cellIdentifier = [NSString stringWithFormat:@"MoodVCCell-%s", className];
                        MoodVCCell *cell = (MoodVCCell*)[devices_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
                        cell = [self setupInitialCell:cell xibName:@"MoodVCCell"];
                        
                        // Setup Cell
                        [cell setupCellWithMood:[device getMood]
                                         zoneID:zone.zoneID];
                        
                        return cell;
                        
                    } else {
                        if ([device isDimmer]){
                            
                            NSString *cellIdentifier = cellIdentifier = [NSString stringWithFormat:@"DimmerCellVCCell-%s", className];
                            DimmerCellVCCell *cell = (DimmerCellVCCell*)[devices_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
                            cell = [self setupInitialCell:cell xibName:@"DimmerCellVCCell"];
                            
                            // Setup Cell
                            [cell setupCellWithDevice:device
                                               zoneID:zone.zoneID];
                            
                            return cell;
                            
                        } else if ([device isColourLEDS]) {
                            
                            NSString *cellIdentifier = cellIdentifier = [NSString stringWithFormat:@"ColourLEDsCellVCCell-%s", className];
                            ColourLEDsCellVCCell *cell = (ColourLEDsCellVCCell*)[devices_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
                            cell = [self setupInitialCell:cell xibName:@"ColourLEDsCellVCCell"];
                            
                            // Setup Cell
                            [cell setupCellWithDevice:device
                                               zoneID:zone.zoneID
                                            indexPath:indexPath
                                             delegate:self];
                            
                            return cell;
                            
                        } else {
                            
                            NSString *cellIdentifier = cellIdentifier = [NSString stringWithFormat:@"DeviceCellVCCell-%s", className];
                            DeviceCellVCCell *cell = (DeviceCellVCCell*)[devices_tbl dequeueReusableCellWithIdentifier:cellIdentifier];
                            cell = [self setupInitialCell:cell xibName:@"DeviceCellVCCell"];
                            
                            // Setup Cell
                            [cell setupCellWithDevice:device
                                               zoneID:zone.zoneID];
                            
                            return cell;
                            
                        }
                    }
                }
            }
            
        }
    }
    
    return nil;
}

- (id)setupInitialCell:(id)_cell xibName:(NSString *)_xib {
    if (_cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:_xib owner:self options:nil];
        _cell = [topLevelObjects objectAtIndex:0];
        [_cell setFrameWidth:devices_tbl.frame.size.width];
        [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [_cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return _cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //DebugLog(@"Device selected");
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];

    if (self.editing) {
        
        // Heating doesn't show in Editing mode...
        // So get device from indexpath row
        
        Device *_device = [[self getDevices] objectAtIndex:indexPath.row];
        
        if (_device.locked == kDeviceFullyLocked) {
            [cell setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0.1f]];
            
        } else if (_device.locked == kDeviceLocked) {
            [cell setBackgroundColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:0.1f]];
            
        } else if (_device.locked == kDeviceUnlocked) {
            //[cell setBackgroundColor:[UIColor clearColor]];
            
        }
    } else {

        Device *_device = nil;
        
        // Heating shows at indexpath 0
        // So check the indexpath when getting the device
        
        if ([zone hasHeating] && indexPath.row == 0) {
            //[cell setBackgroundColor:[UIColor clearColor]];
        } else {
            
            if ([zone hasHeating])
                _device = [[self getDevices] objectAtIndex:indexPath.row - 1];
            else
                _device = [[self getDevices] objectAtIndex:indexPath.row];
            
            if (_device.didSetSucceed) {
                //[cell setBackgroundColor:[UIColor clearColor]];
            } else
                [cell setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0.1f]];
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!self.editing || !indexPath)
        return UITableViewCellEditingStyleNone;
    
    NSUInteger _count = [self getDevices].count;
    
    Device *_device = nil;
    if (_count > indexPath.row)
        _device = [[self getDevices] objectAtIndex:indexPath.row];

    if (_device && [_device.mood isAllOff])
        return UITableViewCellEditingStyleNone;
        
    BOOL insert = NO;
    
    if (indexPath.row == [[self getDevices] count])
        insert = YES;
    
    if (insert){
        return UITableViewCellEditingStyleInsert;
    }else{
        if ([self.connectingIndexPath isEqual:indexPath])
            return UITableViewCellEditingStyleNone;
        else
            return UITableViewCellEditingStyleDelete;
    }
    
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
#if TARGET_IPHONE_SIMULATOR
      
        Device *_device = [[self getDevices] objectAtIndex:indexPath.row];
        [self deleteDevice:_device];
        
#else
        
        LWErrorView *alert = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"delete_device", @"Delete Device")
                                                        message:NSLocalizedString(@"delete_device_info", @"Deleting does not remove the pairing information...")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel")
                                              otherButtonTitles:NSLocalizedString(@"ok", @"Ok"), nil];
        [alert setTag:kZoneVCDeleteDeviceAlert];
        objc_setAssociatedObject(alert, kAlertDeviceIndexPathKey, indexPath, OBJC_ASSOCIATION_RETAIN);
        [alert show];
        
#endif

    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return (indexPath.row<[zone getDeviceCount]);
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    self.connectingIndexPath = nil;
    Device *device = [zone.devices objectAtIndex:sourceIndexPath.row];
    [zone.devices removeObject:device];
    [zone.devices insertObject:device atIndex:destinationIndexPath.row];
    [zone saveDevicesToDB];
    [devices_tbl reloadData];
}

- (void)deleteDeviceForIndexPath:(NSIndexPath *)_indexPath {
    self.connectingIndexPath = nil;
    
    // Device to delete
    Device *_device = [[self getDevices] objectAtIndex:_indexPath.row];
    
    // Send the same command as what connect would (RXDYF1) to remove pairing
    [self connectDeviceUsingUDP:[NSNumber numberWithBool:YES]
                        command:[NSString stringWithFormat:@"!R%dD%dF1|%@|%@",
                                 zone.zoneID,
                                 _device.deviceID,
                                 NSLocalizedString(@"device", @"device"),
                                 NSLocalizedString(@"disconnected", @"disconnected")]];
    
    [self deleteDevice:_device];
}

- (void)deleteDevice:(Device *)_device {
    // When deleting, interate through all Event Actions and ensure that any use of this Device is removed.
    Home *_home = [Home shared];
    for (Event *_event in _home.events) {
        
        NSMutableArray *_removeActionsArray = [[NSMutableArray alloc] init];
        
        for (Action *_action in _event.actions) {
            if ([[_action getDevice] isEqual:_device]) {
                [_removeActionsArray addObject:_action];
            }
        }
        
        for (Action *_action in _removeActionsArray)
            [_event removeAction:_action];
        
        if (_removeActionsArray.count > 0) {
            [_removeActionsArray removeAllObjects];
            [_event saveActionsToDB];
        }
    }
    
    // Delete Device
    [zone removeDevice:_device];
    [zone saveDevicesToDB];
    [self reloadViews];
}

#pragma mark - ColourLEDsCellVC Delegate

- (void)colourLEDMorePressed:(ColourLEDsCellVCCell *)_ColourLEDsCellVC
                      device:(Device *)_device {

    ColourLEDsVC *c = [[ColourLEDsVC alloc] initWithNibName:kVCXibName bundle:nil device:_device zone:zone];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:c];
    [self presentViewController:nc animated:YES completion:^(void){
        c.isModal = YES;
    }];
}

#pragma mark - Connect Device

- (void)connectDeviceUsingUDP:(NSNumber *)_useUDP command:(NSString *)_command {
    [self showSpinnerWithText:@""];
    
    if ([_useUDP boolValue]) {
        [[UDPService shared] send:_command tag:kRequestConnectDevice callingClass:[self getClassName:[self class]]];
    } else {
        [[RemoteSettings shared] sendCommandWithCallingClass:[self getClassName:[self class]] tag:kRequestConnectDevice command:_command];
    }
}

- (void)successConnected:(Device *)_device {   
    [_device setPaired:YES];
    [zone saveDevicesToDB];
    [self.view makeToast:NSLocalizedString(@"device_pairing_complete", @"Device pairing complete")];
    
    if ([zone numUnpairedDevices] == 0 && self.showUnpairedDevices)
        [self disableEditing];
    
    else if (self.showUnpairedDevices)
        self.connectingIndexPath = nil;
    
    [devices_tbl reloadData];
}

#pragma mark - Lock Device

- (void)toggleLockDeviceUsingUDP:(NSNumber *)_useUDP command:(NSString *)_command {
    [self showSpinnerWithText:@""];
    
    if ([_useUDP boolValue]) {
        [[UDPService shared] send:_command tag:kRequestLockDevice callingClass:[self getClassName:[self class]]];
    } else {
        [[RemoteSettings shared] sendCommandWithCallingClass:[self getClassName:[self class]] tag:kRequestLockDevice command:_command];
    }
}

- (void)successLockedWithCommand:(NSString *)_fullCommand {
    Device *_device = [zone getDeviceByCommand:_fullCommand];
    //DebugLog(@"GOT DEVICE: %@", _device.name);
    
    if ([_fullCommand containsString:NSLocalizedString(@"device_locked", @"device locked")]) {
        [_device setLocked:kDeviceLocked];
        [self.view makeToast:NSLocalizedString(@"device_locked", @"Device locked")];
        
    } else if ([_fullCommand containsString:NSLocalizedString(@"device_fully_locked", @"device fully locked")]) {
        [_device setLocked:kDeviceFullyLocked];
        [self.view makeToast:NSLocalizedString(@"device_fully_locked", @"Device fully locked")];
        
    } else if ([_fullCommand containsString:NSLocalizedString(@"device_unlocked", @"device unlocked")]) {
        [_device setLocked:kDeviceUnlocked];
        [self.view makeToast:NSLocalizedString(@"device_unlocked", @"Device unlocked")];
        
    }

    [devices_tbl reloadData];
}

#pragma mark - Save Mood

- (void)saveMoodUsingUDP:(NSNumber *)_useUDP command:(NSString *)_command {
    [self showSpinnerWithText:@""];
    
    if ([_useUDP boolValue]) {
        [[UDPService shared] send:_command tag:kRequestSaveMood callingClass:[self getClassName:[self class]]];
    } else {
        [[RemoteSettings shared] sendCommandWithCallingClass:[self getClassName:[self class]] tag:kRequestSaveMood command:_command];
    }
}

- (void)successMoodSaved {
    [self.view makeToast:NSLocalizedString(@"mood_saved", @"Mood Saved")];
    [devices_tbl reloadData];
}

#pragma mark - Remote Delegate

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kRemoteClassNameKey];
    NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
    
    [self receivedRemoteCheckDeviceHandler:_notification];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        switch (_tag) {
                
            case kRequestConnectDevice:
            {
                [self receivedRemoteConnectDeviceHandler:_notification];
            }
                break;
                
            case kRequestLockDevice:
            {
                [self receivedRemoteLockDeviceHandler:_notification];
            }
                break;
                
            case kRequestSaveMood:
            {
                [self receivedRemoteSaveMoodHandler:_notification];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - Remote Handlers

- (void)receivedRemoteCheckDeviceHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kRemoteCommandKey];
        
    Home *home = [Home shared];
    Device *_device = [home getDeviceFromHardwareKey:_command];
    
    if (_device) {
        if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey] ||
            [[_notification name] isEqualToString:kRemoteFailedNotificationKey])
        {
            // Set to BG red / pink
            _device.didSetSucceed = NO;
        }
        
        else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
        {
            // Set to BG clear
            _device.didSetSucceed = YES;
        }
        
        [devices_tbl reloadData];
    }
}

- (void)receivedRemoteConnectDeviceHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kRemoteCommandKey];
    
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
    {
        /*************************
         * Error
         *************************/
        NSError *error = [_dict objectForKey:kRemoteErrorKey];
        //NSString *message = NSLocalizedString(@"web_service_error", @"There was a problem connecting to the Lightwave server");
        //NSString *str = [NSString stringWithFormat:@"%@\n%@", message, [error localizedDescription]];
        [self showError:[error localizedDescription]];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self successConnected:[zone getDeviceByCommand:_command]];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Failed
         *************************/
        NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
        [self showError:_failed];
    }
}

- (void)receivedRemoteLockDeviceHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kRemoteCommandKey];
    
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
    {
        /*************************
         * Error
         *************************/
        NSError *error = [_dict objectForKey:kRemoteErrorKey];
        //NSString *message = NSLocalizedString(@"web_service_error", @"There was a problem connecting to the Lightwave server");
        //NSString *str = [NSString stringWithFormat:@"%@\n%@", message, [error localizedDescription]];
        [self showError:[error localizedDescription]];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self successLockedWithCommand:_command];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Failed
         *************************/
        NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
        [self showError:_failed];
    }
}

- (void)receivedRemoteSaveMoodHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
    {
        /*************************
         * Error
         *************************/
        NSError *error = [_dict objectForKey:kRemoteErrorKey];
        //NSString *message = NSLocalizedString(@"web_service_error", @"There was a problem connecting to the Lightwave server");
        //NSString *str = [NSString stringWithFormat:@"%@\n%@", message, [error localizedDescription]];
        [self showError:[error localizedDescription]];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self successMoodSaved];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Failed
         *************************/
        NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
        [self showError:_failed];
    }
}

#pragma mark - UDP Delegate

- (void)receivedUDPServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kUDPClassNameKey];
    NSUInteger _tag = [[_dict objectForKey:kUDPTagKey] intValue];
    
    [self receivedUDPCheckDeviceHandler:_notification];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        switch (_tag) {
                
            case kRequestConnectDevice:
            {
                [self receivedUDPConnectDeviceHandler:_notification];
            }
                break;
                
            case kRequestLockDevice:
            {
                [self receivedUDPLockDeviceHandler:_notification];
            }
                break;
                
            case kRequestSaveMood:
            {
                [self receivedUDPSaveMoodHandler:_notification];
            }
                break;
                
            default:
                break;
        }
    }
}

// Overridden
- (void)receivedUDPCheckedStateSuccessNotification:(NSNotification *)_notification {
    [self.wifi_btn setSelected:YES];
}

// Overridden
- (void)receivedUDPCheckedStateFailNotification:(NSNotification *)_notification {
    [self.wifi_btn setSelected:NO];
}

#pragma mark - UDP Handlers

- (void)receivedUDPCheckDeviceHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kUDPCommandKey];

    Home *home = [Home shared];
    Device *_device = [home getDeviceFromHardwareKey:_command];
    
    if (_device) {
        if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
        {
            // Set to BG clear
            _device.didSetSucceed = YES;
        }
        
        [devices_tbl reloadData];
    }
}

- (void)receivedUDPConnectDeviceHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kUDPCommandKey];
    
    if ([[_notification name] isEqualToString:kUDPErrorNotificationKey] ||
        [[_notification name] isEqualToString:kUDPFailedNotificationKey])
    {
        /*************************
         * Error || Failed
         *************************/
        BOOL _attemptRemote = [[_dict objectForKey:kUDPAllowSwitchToRemoteKey] boolValue];
        
        if (_attemptRemote) {
            // Attempt the request using RemoteSettings...
            [self connectDeviceUsingUDP:[NSNumber numberWithBool:NO] command:_command];
        } else {
            DebugLog(@"Attempt Remote Disabled...");
        }
    }
    
    else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self successConnected:[zone getDeviceByCommand:_command]];
    }
}

- (void)receivedUDPLockDeviceHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kUDPCommandKey];
    
    if ([[_notification name] isEqualToString:kUDPErrorNotificationKey] ||
        [[_notification name] isEqualToString:kUDPFailedNotificationKey])
    {
        /*************************
         * Error || Failed
         *************************/
        BOOL _attemptRemote = [[_dict objectForKey:kUDPAllowSwitchToRemoteKey] boolValue];
        
        if (_attemptRemote) {
            // Attempt the request using RemoteSettings...
            [self toggleLockDeviceUsingUDP:[NSNumber numberWithBool:NO] command:_command];
        } else {
            DebugLog(@"Attempt Remote Disabled...");
        }
    }
    
    else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self successLockedWithCommand:_command];
    }
}

- (void)receivedUDPSaveMoodHandler:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kUDPCommandKey];
    
    if ([[_notification name] isEqualToString:kUDPErrorNotificationKey] ||
        [[_notification name] isEqualToString:kUDPFailedNotificationKey])
    {
        /*************************
         * Error || Failed
         *************************/
        BOOL _attemptRemote = [[_dict objectForKey:kUDPAllowSwitchToRemoteKey] boolValue];
        
        if (_attemptRemote) {
            // Attempt the request using RemoteSettings...
            [self saveMoodUsingUDP:[NSNumber numberWithBool:NO] command:_command];
        } else {
            DebugLog(@"Attempt Remote Disabled...");
        }
    }
    
    else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self successMoodSaved];
    }
}

#pragma mark - NSNotification General Delegate

// Settings retrieved afresh from server... reload data
- (void)receivedRefreshTableViewNotification:(NSNotification *)_notification {
    Zone *_tmpZone = [[Home shared] getZoneByID:zone.zoneID];
    if (_tmpZone) {
        zone = _tmpZone;
    }
    
    // Reload
    [self reloadViews];
}

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
