//
//  CustomActionSheet.h
//  ActionSheetPicker
//
//  Created by Dan Hillman on 12/01/2013.
//
//

#import <UIKit/UIKit.h>

@protocol CustomActionSheetDelegate;

@interface CustomActionSheet : UIActionSheet {
    
}

@property (nonatomic, assign) id <CustomActionSheetDelegate> customDelegate;

- (void)triggerTapCallback;

@end

@protocol CustomActionSheetDelegate <NSObject>

@required

@optional
- (void)tappedToDismissActionSheet:(CustomActionSheet *)tmpCustomActionSheet;

@end
