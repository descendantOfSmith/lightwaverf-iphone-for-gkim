//
//  CodeVC.m
//  LightwaveRF
//
//  Created by Nik Lever on 05/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "CodeVC.h"
#import "AppDelegate.h"
#import "Toast+UIView.h"
#import "HomeVC.h"
#import "Account.h"
#import "ButtonsHelper.h"

@interface CodeVC ()

@end

@implementation CodeVC

@synthesize code;
@synthesize navigaionBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [navigaionBar setBackgroundColor:[AppStyleHelper getNavBarBackgroundColor]];
    
    [self setupUI];
    
    // Do any additional setup after loading the view from its nib.
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    NSMutableAttributedString *nocode_txt = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"nocode", @"I didn't recieve a code")];
    [nocode_txt addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [nocode_txt length])];
    [_nocode_btn setAttributedTitle:nocode_txt forState:UIControlStateNormal];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
    gestureRecognizer.numberOfTapsRequired = 1;
    gestureRecognizer.enabled = YES;
    gestureRecognizer.cancelsTouchesInView = NO;
    gestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:gestureRecognizer];
    
    [self.code_txt setTag:kCodeVCCodeTextField];
}

- (void)setupUI {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self.navigaionBar setFrameY:0.0f];
        [self.scrollView alignToBottomOfView:self.navigaionBar padding:0.0f];
        [self.scrollView setFrameHeight:self.view.frame.size.height - self.scrollView.frame.origin.y];
    }
    
    [self setupLocalisedStrings];
}

- (void)setupLocalisedStrings {
    [self.navigaionBar.topItem setTitle:NSLocalizedString(@"wifi_link", @"WiFi Link")];
    [self.navigaionBar.topItem.leftBarButtonItem setTitle:NSLocalizedString(@"back", @"back")];
    [self.navigaionBar.topItem.rightBarButtonItem setTitle:NSLocalizedString(@"next", @"next")];
    
    if ([AppStyleHelper isMegaMan])  {
        self.navigaionBar.topItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[ButtonsHelper getButtonWithText:NSLocalizedString(@"back", @"back")
                                                                                                                              type:kButtonArrowLeft
                                                                                                                               tag:0
                                                                                                                             width:45
                                                                                                                            height:18
                                                                                                                            target:self
                                                                                                                          selector:@selector(backPressed:)]];
        
        
        self.navigaionBar.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[ButtonsHelper getButtonWithText:NSLocalizedString(@"next", @"next")
                                                                                                                               type:kButtonArrowRight
                                                                                                                                tag:0
                                                                                                                              width:45
                                                                                                                             height:18
                                                                                                                             target:self
                                                                                                                           selector:@selector(nextPressed:)]];
    }
    
    [self.description_localised_lbl setText:NSLocalizedString(@"enter_link", @"enter_link")];
    [self.code_txt setPlaceholder:NSLocalizedString(@"required", @"required")];
    [self.code_localised_lbl setText:NSLocalizedString(@"code", @"code")];
    [self.nocode_btn setTitle:NSLocalizedString(@"nocode", @"nocode") forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // Set Code on WFL
    self.code = nil;
    [self setCode];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)animationDidStop:(NSString*)animationID finished:(BOOL)finished context:(void *)context {
    if ([animationID isEqualToString:@"BackAnim"])
        [self.view removeFromSuperview];
}

- (void)viewDidUnload {
    [self setCode_txt:nil];
    [self setNocode_btn:nil];
    [super viewDidUnload];
}

#pragma mark -
#pragma mark Text Field Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self scrollToTargetView:textField];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Actions

- (IBAction)backPressed:(id)sender {
    [self hideKeyBoard:nil];
    
    self.code = nil;
    [self setCode_txt:nil];
    
    CGRect frame = self.view.frame;

    [UIView animateWithDuration:0.3
                          delay:0.0f
                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         self.view.frame = CGRectMake(320, frame.origin.y, frame.size.width, frame.size.height);
                     }
                     completion:^(BOOL completed2) {
                         [self.view removeFromSuperview];
                     }];
}

- (IBAction)nextPressed:(id)sender {
    [self hideKeyBoard:nil];
    
    if ([_code_txt.text length]!=4){
        [self showError:NSLocalizedString(@"register_error6", @"The code number will have 4 digits")];
        return;
    }
    
    if ([_code_txt.text isEqualToString:self.code]){

        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        [Account updateCurrentAccount:app.tmpEmail
                             password:app.tmpPin];

        [[UserPrefsHelper shared] setUserName:app.tmpName];
        [[UserPrefsHelper shared] setUserEmail:app.tmpEmail];
        [[UserPrefsHelper shared] setUserPassword:app.tmpPin];
        [[UserPrefsHelper shared] setUserMac:app.tmpMac];
        [[UserPrefsHelper shared] setRegistered:YES];
        [[UserPrefsHelper shared] setEnableWFLPopups:YES];
        [[UserPrefsHelper shared] setCode:self.code];
        [[UserPrefsHelper shared] setDemoAccount:NO];
        
        // Refresh Data
        Home *home = [Home shared];
        if (home!=nil)
            [home loadAndRefreshWFL:YES];
        
//#if TARGET_IPHONE_SIMULATOR
//        [self accountCreated];
//        return;
//#endif
        
        [self showSpinnerWithText:@""];
        
        RemoteSettings *web_service = [RemoteSettings shared];
        [web_service pushHomeWithCallingClass:[self getClassName:[self class]] tag:kRequestRegisterUser];
        
    } else {
        [self showError:NSLocalizedString(@"register_error7", @"There was a problem logging on to the server. Check the code you entered matches the one on the Wifi Link screen or press the button to resend the code")];
    }
}

- (IBAction)nocodePressed:(id)sender {
    [self hideKeyBoard:nil];
    self.code = nil;
    [self setCode];
}

#pragma mark -
#pragma mark Remote Settings

- (void)setCode {
    if (!self.code) {
        int val = 1000 + arc4random() % 8999;
        self.code = [NSString stringWithFormat:@"%d", val];
        DebugLog(@"val=%d, code=%@", val, self.code);
        
    #if TARGET_IPHONE_SIMULATOR
        _code_txt.text = self.code;
    #endif
        
        [self showSpinnerWithText:@""];
        RemoteSettings *web_service = [RemoteSettings shared];
        [web_service setPinWithCallingClass:[self getClassName:[self class]] tag:kRequestSetPin withPin:self.code];
    }
}

#pragma mark - Remote Services Delegate

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kRemoteClassNameKey];
    NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        switch (_tag) {
                
            case kRequestSetPin:
            {
                [self handleRemoteServiceSetPin:_notification];
            }
                break;
                
            case kRequestRegisterUser:
            {
                [self handleRemoteServiceRegisterUser:_notification];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - Remote Services Handlers

- (void)handleRemoteServiceSetPin:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
    {
        /*************************
         * Error
         *************************/
        NSError *error = [_dict objectForKey:kRemoteErrorKey];
        DebugLog(@"%@ %@", [error localizedDescription], [error localizedFailureReason]);
        [self showError:[error localizedDescription]];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        //NSString *data = [_dict objectForKey:kRemoteResponseKey];
        //DebugLog(@"%@", data);
        [self.view makeToast:NSLocalizedString(@"enter_link", @"Enter the code on the display of your Wifi-Link.")
                    duration:3.0f
                    position:@"center"];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Failed
         *************************/
        NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
        [self showAlert:_failed];
    }
}

- (void)accountCreated {
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app switchView:@"Intro"];
    
    // Reload the tableview data on HomeVC
    [app.nav1Controller popToRootViewControllerAnimated:NO];
    [app.lwController setSelectedIndex:0];
    
    for (UIViewController *_c in app.nav1Controller.viewControllers) {
        if ([_c isKindOfClass:[HomeVC class]]) {
            HomeVC *_homeVC = (HomeVC *)_c;
            [_homeVC.zone_tbl reloadData];
            [_homeVC.view makeToast:NSLocalizedString(@"message3", @"Settings received")];
            
            // // Delay 3 seconds
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                if (_homeVC.view)
                    [_homeVC.view makeToast:NSLocalizedString(@"help1", @"help1 text") duration:5.0f position:@"bottom"];
            });
        }
    }
}

- (void)handleRemoteServiceRegisterUser:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
    {
        /*************************
         * Error
         *************************/
        NSError *error = [_dict objectForKey:kRemoteErrorKey];
        DebugLog(@"%@ %@", [error localizedDescription], [error localizedFailureReason]);
        [self showError:[error localizedDescription]];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        NSString *_response = [[_dict objectForKey:kRemoteResponseKey] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        //DebugLog(@"%@", _response);
        
        if ([_response isEqualToString:@"Your new account has been created"] ||
            [_response isEqualToString:@"Your settings have been updated"]) {
            [self accountCreated];
        } else {
            [self showAlert:_response];
        }
    }
    
    else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Failed
         *************************/
        NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
        [self showAlert:_failed];
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
