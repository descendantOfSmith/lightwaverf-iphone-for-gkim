//
//  IntroVC.h
//  LightwaveRF
//
//  Created by Nik Lever on 05/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#define PAGECOUNT 4

@interface IntroVC : UIViewController<UIScrollViewDelegate>{
    UIScrollView *scrollView;
    UIPageControl *padeControl;
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *viewControllers;
@property (strong, nonatomic) IBOutlet UIImageView *btnbg_img;
@property (strong, nonatomic) IBOutlet UIImageView *logo_img;

- (IBAction)skipPressed:(id)sender;

@end
