//
// App Constants
//

#define kUserDefaults                        [NSUserDefaults standardUserDefaults]
#define kSharedApplication                   [UIApplication sharedApplication]
#define kBundle                              [NSBundle mainBundle]
#define kMainScreen                          [UIScreen mainScreen]
#define kAppName                             [kBundle objectForInfoDictionaryKey:@"CFBundleName"]
#define kAppVersion                          [kBundle objectForInfoDictionaryKey:@"CFBundleVersion"]
#define kApplicationDelegate                 ((AppDelegate *)[kSharedApplication delegate])

#define kStopRespondingToTouches             [kSharedApplication beginIgnoringInteractionEvents];
#define kResumeRespondingToTouches           [kSharedApplication endIgnoringInteractionEvents];
#define kNavBar                              self.navigationController.navigationBar
#define kTabBar                              self.tabBarController.tabBar
#define kNavBarHeight                        self.navigationController.navigationBar.bounds.size.height

#define kDateComponents                      NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
#define kTimeComponents                      NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit

// XIB Definition
#define kVCXibName                           @"ScrollViewController"
