//
//  EventsVC.h
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

typedef enum
{
    kEventsVCAddEventAlert = 1001
} EventsVCTags;

@interface EventsVC : RootViewController <UITableViewDelegate, UITableViewDataSource> {
    
}

@property (strong, nonatomic) IBOutlet UINavigationBar *nav_bar;
@property (strong, nonatomic) IBOutlet UITableView *events_tbl;
@property (strong, nonatomic) IBOutlet UIView *headerBarView;

- (IBAction)stopAllPressed:(id)sender;

@end
