//
//  XMLConfig.m
//  LightwaveRF
//
//  Created by Nik Lever on 07/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "XMLConfig.h"
#import "Home.h"
#import "Zone.h"
#import "Device.h"
#import "Mood.h"
#import "Event.h"
#import "DB.h"
#import "LTimer.h"
#import "Profile.h"
#import "NSString+Utilities.h"
#import "OrderedDictionary.h"
#import "GTMNSString+HTML.h"

static XMLConfig *xmlConfig = nil;

@implementation XMLConfig
@synthesize elements;
@synthesize items;
@synthesize key;

//Init this singleton class
+ (id)shared {
    @synchronized(self) {
        if(xmlConfig == nil)
            xmlConfig = [[super allocWithZone:NULL] init];
    }
    return xmlConfig;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self shared];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)init {
    if (self = [super init]) {
        elements = nil;
        NUM_DEVICES_PER_ROOM = 10;
    }
    return self;
}

//End of initialisation stuff


-(bool)xmlToHome:(NSString *)str{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
    NSString *tmp = [str stringByReplacingOccurrencesOfString:@"<int>" withString:@"<integer>"];
    NSString *xml = [tmp stringByReplacingOccurrencesOfString:@"</int>" withString:@"</integer>"];
    NSError *error;
    
    //DebugLog(@"XMLConfig xmlToHome:\n%@", xml);
    BOOL succeed = [xml writeToFile:[documentsDirectory stringByAppendingPathComponent:@"home.plist"]
                         atomically:YES
                           encoding:NSUTF8StringEncoding
                              error:&error];
    if (succeed){
        // Process dictionary
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:[documentsDirectory stringByAppendingPathComponent:@"home.plist"]];
        NSString *sql;
        
        if (dict != nil){
            NSMutableArray *roomNames = [dict objectForKey:@"rooms"];
            DB *db = [DB shared];
            [db clearAccount:[[UserPrefsHelper shared] getCurrentAccountId]];
            
            if ([roomNames count] > 0)
            {
                //DebugLog(@"xmlToHome - Data is a data dictionary");
                NSMutableArray *roomStatus          = [dict objectForKey:@"roomStatus"];
                NSMutableArray *deviceNames         = [dict objectForKey:@"deviceNames"];
                NSMutableArray *deviceStatuses      = [dict objectForKey:@"deviceStatus"];
                NSMutableArray *sequences           = [dict objectForKey:@"sequences"];
                NSMutableArray *timers              = [dict objectForKey:@"timers"];
                NSMutableArray *settings            = [dict objectForKey:@"settings"];
                NSMutableArray *profiles            = [dict objectForKey:@"heatingProfiles"];
                NSMutableDictionary *settingsv2     = [dict objectForKey:@"settingsv2"];
                
                int id=0;
                for(NSString *str in roomStatus){
                    if ([str isEqualToString:@"A"]){
                        //This is an active room
                        sql = [NSString stringWithFormat:@"INSERT INTO zones (name, zoneId, displayId, accountId) VALUES (\'%@\', %d, %d, %d)", [roomNames objectAtIndex:id], (id+1), id, [[UserPrefsHelper shared] getCurrentAccountId]];
                        [db sqlExec:sql];
                        int deviceOffset = id * NUM_DEVICES_PER_ROOM;
                        NSString *deviceStatus;
                        int deviceId=1;
                        int moodId=1;
                        for(int i=0; i<NUM_DEVICES_PER_ROOM; i++){
                            deviceStatus = [deviceStatuses objectAtIndex:(deviceOffset + i)];
                            //DebugLog(@"Status = %@", deviceStatus);
                            if (i<6){
                                
                                //Devices
                                if ([deviceStatus isEqualToString:@"P"] ||
                                    [deviceStatus isEqualToString:@"D"] ||
                                    [deviceStatus isEqualToString:@"O"] ||
                                    [deviceStatus isEqualToString:@"R"] ||
                                    [deviceStatus isEqualToString:@"L"] ||
                                    [deviceStatus isEqualToString:@"H"]){
                                    
                                    sql = [NSString stringWithFormat:@"INSERT INTO devices (name, zoneId, deviceId, displayId, paired, setting, moodId, type, accountId) VALUES (\'%@\', %d, %d, %d, 1, 0, -1, \'%@\', %d)",
                                           [deviceNames objectAtIndex:(deviceOffset + i)], (id+1), deviceId, deviceId, deviceStatus, [[UserPrefsHelper shared] getCurrentAccountId]];
                                    [db sqlExec:sql];
                                    deviceId++;
                                } else if ([deviceStatus isEqualToString:@"I"]){
                                    //DebugLog(@"Status = %@", deviceStatus);
                                    deviceId++;
                                }
                            }else{
                                //Moods
                                if ([deviceStatus isEqualToString:@"M"]){
                                    sql = [NSString stringWithFormat:@"INSERT INTO devices (name, zoneId, deviceId, displayId, paired, setting, moodId, type, accountId) VALUES (\'%@\', %d, %d, %d, 1, 0, %d, \'%@\', %d)",
                                           [deviceNames objectAtIndex:(deviceOffset + i)], (id+1), deviceId, deviceId, moodId, deviceStatus, [[UserPrefsHelper shared] getCurrentAccountId]];
                                    [db sqlExec:sql];
                                    deviceId++;
                                    moodId++;
                                }
                            }
                        }
                    }
                    id++;
                }
                
                /*************************************************************
                 * Settings
                 *************************************************************/

//                   DebugLog(@"Settings: %@", settings);
                    
                    
//                    "nik@catalystpics.co.uk",
//                    4085,
//                    "10.0",
//                    "Amsterdam, NL|52.48, -01.48",
//                    "GMT 1 Central Europe Time|1",
//                    "74:0A:BC:03:1C:9A",
//                    1111,
//                    "nik@catalystpics.co.uk4085"
                                    
                int i=0;
                for(NSString *_setting in settings){
                    //DebugLog(@"%d: %@", i, _setting);
                    switch(i){
                        case kEMAIL_SETTING:
                            [[UserPrefsHelper shared] setUserEmail:_setting];
                            break;
                            
                        case kPIN_SETTING:
                            [[UserPrefsHelper shared] setUserPassword:_setting];
                            break;
                            
                        case kCOST_SETTING:
                            [[UserPrefsHelper shared] setElecRate:_setting];
                            break;
                            
                        case kLATLONG_SETTING:
                            [[UserPrefsHelper shared] setLocationInfo:_setting];
                            break;
                            
                        case kTIME_ZONE_SETTING:
                            [[UserPrefsHelper shared] setTimeZoneInfo:_setting];
                            break;
                            
                        case kMAC_SETTING:
                            [[UserPrefsHelper shared] setUserMac:_setting];
                            break;
                            
                        case kWIFI_PIN_SETTING:
                            [[UserPrefsHelper shared] setWiFiPin:_setting];
                            break;
                            
                        case kTEMP_EMAIL_SETTING:
                            [[UserPrefsHelper shared] setEmailPwd:_setting];
                            break;
                    }
                    i++;
                }
                
                /*************************************************************
                 * Settings V2
                 *************************************************************/
                
                /*
                <key>settingsv2</key>
                <dict>
                    <key>imp_kwh</key>
                    <integer>1234</integer>
                 
                    <key>name</key>
                    <string>Nik Home</string>
                 
                    <key>ssid</key>
                    <string>Cisco07493</string>
                </dict>
                */
                
                if (settingsv2) {
                    NSString *_name = [settingsv2 objectForKey:@"name"];
                    NSString *_impKWH = [[settingsv2 objectForKey:@"imp_kwh"] stringValue];
                    NSString *_ssid = [settingsv2 objectForKey:@"ssid"];
                    
                    if (_name.length > 0)
                        [[UserPrefsHelper shared] setUserName:_name];
                    
                    if (_impKWH.length > 0)
                        [[UserPrefsHelper shared] setImpKwh:_impKWH];
                    
                    if (_ssid.length > 0)
                        [[UserPrefsHelper shared] setSSID:_ssid];
                }
                
                /*************************************************************
                 * Timers
                 *************************************************************/

                if (timers != nil) {
                    for (NSArray *_timerArray in timers) {
                        //DebugLog(@"Array = %@", _timerArray);
                        
                        //
                        // Timers require 5 params
                        //
                        
                        if (_timerArray.count >= 5) {
                            
                            /*
                             20130726112249,
                                "Each MTWTFSS @ 06:00 from  until ",
                             "!FiP\"20130726112249\"=!FqP\"Home\",T06:00,Dmtwtfss,Mjfmamjjasond",
                                " 06:00",
                             "true"
                             */
                            
                            NSString *_sequenceName = [_timerArray objectAtIndex:0];
                            //NSString *_when = [_timerArray objectAtIndex:1];
                            NSString *_command = [_timerArray objectAtIndex:2];
                            //NSString *_startDateTime = [_timerArray objectAtIndex:3];
                            
                            //DebugLog(@"Save Command: %@", _command);
                            
                            // Check for the 5th param 'Active'
                            BOOL _isActive = YES;
                            NSString *_isActiveStr = [_timerArray objectAtIndex:4];
                            if ([_isActiveStr isEqualToString:@"false"])
                                _isActive = NO;
                            
                            //DebugLog(@"SQL ADD TIMER: %@", _sequenceName);
                            
                            sql = [NSString stringWithFormat:@"INSERT INTO LTimers (timerName, command, active, accountId) VALUES (\'%@\', \'%@\', %d, %d)",
                                   _sequenceName,
                                   _command,
                                   _isActive,
                                   [[UserPrefsHelper shared] getCurrentAccountId]];
                            [db sqlExec:sql];
                            
                        } else {
                            
                            /*
                             
                             <string>Each _TWT___ @ 08:10 from 14/03/13 until 31/12/19|Jims Room ALL OFF in Jims Room Set|Timer Set</string>
                             <string>!FiP"T20130312235238"=!FqP"!R2Fa,T08:10,S13/03/13,Dxtwtxxx,E31/12/19|Jims Room ALL OFF in Jims Room Set|Timer Set",T08:10,S14/03/13,E31/12/19|Jims Room ALL OFF in Jims Room Set|Timer Set,Dxtwtxxx,Mjfmamjjasond</string>
                             <string>14/03/13 08:10</string>
                             
                             ... or
                             
                             <string>Each MTWTFSS @ 06:00</string>
                             <string>!FiP"20130920111404"=!FqP"!R2D4F0",T06:00,Dmtwtfss,Mjfmamjjasond</string>
                             <string>06:00</string>
                             <string>true</string>
                             
                             */
                            
                            // Timer created from old IOS app/old web manager have only 3 params
                            // We need to locate the command string, unfortunately from my tests this keeps appearing in different indexes
                            // Safest option is to check each index for a string that contains FiP and FqP, as this will normally be the command
                            
                            
                            NSString *_command = nil;
                            
                            // Check for FiP AND FqP
                            for (NSString *_str in _timerArray) {
                                if ([_str containsString:@"FiP"] && [_str containsString:@"FqP"]) {
                                    _command = _str;
                                    break;
                                    
                                }
                            }
                            
                            // No FiP AND FqP.... just check for !FiP
                            if (_command.length <= 0) {
                                for (NSString *_str in _timerArray) {
                                    if ([_str containsString:@"!FiP"]) {
                                        _command = _str;
                                        break;
                                    }
                                }
                            }
                            
                            // No !FiP.... just check for FiP
                            if (_command.length <= 0) {
                                for (NSString *_str in _timerArray) {
                                    if ([_str containsString:@"FiP"]) {
                                        _command = _str;
                                        break;
                                    }
                                }
                            }
                            
                            //DebugLog(@"Command: %@", _command);
                            
                            // If we found a command, add it!
                            if (_command.length > 0) {
                                LTimer *_tmpTimer = [[LTimer alloc] initWithCommand:_command];
                                sql = [NSString stringWithFormat:@"INSERT INTO LTimers (timerName, command, active, accountId) VALUES (\'%@\', \'%@\', %d, %d)",
                                       _tmpTimer.name,
                                       _command,
                                       YES,
                                       [[UserPrefsHelper shared] getCurrentAccountId]];
                                [db sqlExec:sql];
                            }
                        }
                    }
                }
                
                // Get Added Timers
                NSMutableArray *_getTimers = [db getRows:[NSString stringWithFormat:@"SELECT * FROM LTimers WHERE accountId=%d", [[UserPrefsHelper shared] getCurrentAccountId]]];
                
                /*************************************************************
                 * Events / Sequences
                 *************************************************************/

                int nextEventId=2;
                int eventId;
                for(NSArray *seq in sequences){
                    int actionId=1;
                    for(NSString *str in seq){
                        if (actionId==1){
                            if ([str isEqualToString:NSLocalizedString(@"home", @"home")]){
                                eventId = 0;
                            }else if ([str isEqualToString:NSLocalizedString(@"away", @"away")]){
                                eventId = 1;
                            }else{
                                eventId = nextEventId;
                                nextEventId++;
                            }
                            
                            // Check if any of these events we are about to add have a timer associated with it.
                            // If so get a reference
                            NSString *_timerName = @"";
                            for (NSMutableDictionary *dict in _getTimers){
                                NSString *_command = [dict objectForKey:@"command"];
                                //DebugLog(@"Get Command: %@", _command);
                                LTimer *_timer = [[LTimer alloc] initWithCommand:_command];
                                
                                if ([_timer.event_name isEqualToString:str]) {
                                    // Match
                                    _timerName = _timer.name;
                                    //DebugLog(@"SQL Event: %@ Found Timer: %@", str, _timerName);
                                    break;
                                }
                            }
                            
                            sql = [NSString stringWithFormat:@"INSERT INTO events (name, eventId, displayId, timerName, accountId) VALUES (\'%@\', %d, %d, \'%@\', %d)", str, eventId, eventId, _timerName, [[UserPrefsHelper shared] getCurrentAccountId]];
                            
                        }else{
                            NSArray *tokens = [str componentsSeparatedByString:@","];
                            NSString *hwKey = str;
                            NSString *delay = @"00:00:00";
                            if ([tokens count]>=2){
                                hwKey = [tokens objectAtIndex:0];
                                delay = [tokens objectAtIndex:1];
                            }
                            sql = [NSString stringWithFormat:@"INSERT INTO actions (eventId, hwKey, delay, displayId, accountId) VALUES (%d, \'%@\', \'%@\', %d, %d)",
                                   eventId, hwKey, delay, actionId, [[UserPrefsHelper shared] getCurrentAccountId]];
                        }
                        [db sqlExec:sql];
                        actionId++;
                    }
                }
                
                /*************************************************************
                 * Heating Profiles
                 *************************************************************/
                if (profiles != nil){
                    for (NSDictionary *dict in profiles){
                        int profileId = [[dict valueForKey:@"id"] intValue];
                        int type = [[dict valueForKey:@"type"] intValue];
                        int zoneId = [[dict valueForKey:@"zoneId"] intValue];
                        float setting = [[dict valueForKey:@"temp"] floatValue];
                        NSString *start = [dict objectForKey:@"start"];
                        NSString *end = [dict objectForKey:@"end"];
                        sql = [NSString stringWithFormat:@"INSERT INTO profiles (profileId, type, zoneId, temp, start, end, accountId) VALUES ( %d, %d, %d, %3.1f, \'%@\', \'%@\', %d)", profileId, type, zoneId, setting, start, end, [[UserPrefsHelper shared] getCurrentAccountId]];
                        [db sqlExec:sql];
                    }
                }
                
                Home *home = [Home shared];
                if (home!=nil)
                    [home loadAndRefreshWFL:YES];//This loads the Zones, Events and Profiles arrays into the Home class. Each Zone then loads it's own devices, each Event loads its own Actions
                
                /*************************************************************
                 * Settings V2
                 *************************************************************/
                if (settingsv2) {
                    NSArray *_zoneOrder = [settingsv2 objectForKey:@"zonesDisplayOrderById"];
                    if (_zoneOrder) {
                        Home *_home = [Home shared];
                        Device *_device = nil;
                        int n = 1;
                        for (NSDictionary *_zoneOrderDict in _zoneOrder) {
                            NSUInteger _zoneID = [[_zoneOrderDict objectForKey:@"id"] intValue];
                            NSString *_zoneName = [_zoneOrderDict objectForKey:@"name"];
                            Zone *_zone = [_home getZoneByID:_zoneID];
                            
                            /*
                             * Add New Zone and All of its Devices
                             */
                            if (!_zone) {
                                _zone = [[Zone alloc] initWithID:_zoneID name:_zoneName];
                                [home addZone:_zone];

                                NSArray *_deviceOrderArray = [_zoneOrderDict objectForKey:@"devices"];
                                for (NSDictionary *deviceDict in _deviceOrderArray) {
                                    _device = nil;
                                    NSString *_deviceName = [deviceDict objectForKey:@"name"];
                                    NSString *_deviceType = [deviceDict objectForKey:@"type"];
                                    int _deviceId = [[deviceDict objectForKey:@"id"] intValue];
                                    
                                    //DebugLog(@"ID: %d, Name: %@", _deviceId, _deviceName);

                                    if ([[_deviceType uppercaseString] isEqualToString:@"M"] || [[_deviceType uppercaseString] isEqualToString:@"A"]) {
                                        Mood *_newMood = _newMood = [[Mood alloc] initWithID:_deviceId name:_deviceName zone:_zone];
                                        Device *_newDevice = [[Device alloc] initWithMood:_newMood];
                                        [_zone addDevice:_newDevice andSave:YES];
                                        
                                    } else {
                                        Device *_newDevice = [[Device alloc] initWithName:_deviceName deviceID:_deviceId type:_deviceType zone:_zone];
                                        [_zone addDevice:_newDevice andSave:YES];
                                    }
                                }
                                
                                [home saveZones:YES];
                                
                                // Skip update since we've just added correctly
                                n+=1;
                                continue;
                            }
                            
                            /*
                             * Update Existing Zone and All of its Devices
                             */
                            
                            // Update zone order in DB
                            sql = [NSString stringWithFormat:@"UPDATE zones SET displayId=%d WHERE accountId=%d AND zoneId=%d",
                                   n,
                                   [[UserPrefsHelper shared] getCurrentAccountId],
                                   _zone.zoneID];
                            [db sqlExec:sql];
                            
//                            if (_zone) {
//                                DebugLog(@"\n\n\n GOT ZONE \n\n\n");
//                            } else {
//                                DebugLog(@"\n\n\n ZONE NOT FOUND (ID: %d) \n\n\n", _zoneID);
//                            }
                            
                            // Get Device Order
                            if (_zone) {
                                
                                // Update zone order in array
                                [_home.zones removeObject:_zone];
                                [_home.zones insertObject:_zone atIndex:n-1];
                                
                                // LESS THAN 6 DEVICES
                                NSArray *_deviceOrderArray = [_zoneOrderDict objectForKey:@"deviceDisplayOrderById"];
                                int _deviceOrder = 1;
                                for(int k=0; k < _deviceOrderArray.count; k+=2){
                                    _device = nil;
                                    NSString *_deviceType = [_deviceOrderArray objectAtIndex:k];
                                    int _deviceId = [[_deviceOrderArray objectAtIndex:k+1] intValue];
                                    
                                    // Locate Device
                                    if ([_deviceType containsString:@"device"]) {
                                        _device = [_zone getDeviceByID:_deviceId];
                                        
                                    } else if ([_deviceType containsString:@"mood"]) {
                                        _device = [_zone getDeviceByMoodID:_deviceId];
                                    }
                                    
//                                    if (_device) {
//                                        DebugLog(@"\n\n\n GOT DEVICE \n\n\n");
//                                    } else {
//                                        DebugLog(@"\n\n\n DEVICE NOT FOUND (Type: %@, Device ID: %d, Zone ID: %d) \n\n\n", _deviceType, _deviceId, _zoneID);
//                                    }
                                    
                                    if (_device) {
                                        // Update device order in DB
                                        sql = [NSString stringWithFormat:@"UPDATE devices SET displayId=%d WHERE accountId=%d AND zoneId=%d AND deviceId=%d",
                                               _deviceOrder,
                                               [[UserPrefsHelper shared] getCurrentAccountId],
                                               _zone.zoneID,
                                               _device.deviceID];
                                        [db sqlExec:sql];
                                        
                                        // Update device order in array
                                        [_zone.devices removeObject:_device];
                                        [_zone.devices insertObject:_device atIndex:_deviceOrder-1];
                                        
                                        _deviceOrder += 1;
                                    }
                                }
                                
                                // MORE THAN 6 DEVICES
                                if (_deviceOrderArray.count == 0) {
                                    _deviceOrderArray = [_zoneOrderDict objectForKey:@"devices"];
                                    _deviceOrder = 1;
                                    
                                    for (NSDictionary *deviceDict in _deviceOrderArray) {
                                        _device = nil;
                                        //NSString *_deviceName = [deviceDict objectForKey:@"name"];
                                        //NSString *_deviceType = [deviceDict objectForKey:@"type"];
                                        int _deviceId = [[deviceDict objectForKey:@"id"] intValue];
                                        
                                        // Locate Device
                                        _device = [_zone getDeviceByID:_deviceId];
                                        
                                        if (_device) {
                                            // Update device order in DB
                                            sql = [NSString stringWithFormat:@"UPDATE devices SET displayId=%d WHERE accountId=%d AND zoneId=%d AND deviceId=%d",
                                                   _deviceOrder,
                                                   [[UserPrefsHelper shared] getCurrentAccountId],
                                                   _zone.zoneID,
                                                   _device.deviceID];
                                            [db sqlExec:sql];
                                            
                                            // Update device order in array
                                            [_zone.devices removeObject:_device];
                                            [_zone.devices insertObject:_device atIndex:_deviceOrder-1];
                                            
                                        } else {
                                            // DH: Not required. These devices are moods and do actually exist....
                                            // Add New
//                                            DebugLog(@"Device Name: %@", _deviceName);
//                                            _device = [[Device alloc] initWithName:_deviceName deviceID:_deviceId type:_deviceType zone:_zone];
//                                            if (_device!=nil) {
//                                                [_zone addDevice:_device andSave:YES];
//                                            }
                                        }
                                        
                                        _deviceOrder += 1;
                                    }
                                }
                            }// Got Zone
                            n+=1;
                        } // End Zone Order For Loop
                    } // End Zone Order Exists Check
                } // End SettingsV2
            } 
        }
    }
    
    
    return succeed;
}

-(NSString *)homeToXML{
    
    Home *home = [Home shared];
    
    NSMutableArray *rooms = [[NSMutableArray alloc]init];
    NSMutableArray *roomStatus = [[NSMutableArray alloc]init];
    NSMutableArray *deviceNames = [[NSMutableArray alloc]init];
    NSMutableArray *deviceStatus = [[NSMutableArray alloc]init];
    NSMutableArray *events = [[NSMutableArray alloc] init];
    NSMutableArray *timers = [[NSMutableArray alloc] init];
    NSMutableArray *heatingProfiles = [[NSMutableArray alloc] init];
    NSMutableArray *settings = [[NSMutableArray alloc] init];
    NSMutableDictionary *settingsv2 = [[NSMutableDictionary alloc] init];
    
    for(int hello=0; hello<8; hello++){
        BOOL didFind = YES;

        Zone *zone = [home getZoneByID:hello+1];
        
        if (zone) {
            if (zone!=nil){
                [rooms addObject:zone.name ? zone.name : @"Zone"];
                [roomStatus addObject:@"A"];
                //Add empty devices and moods
                //int idx=0;
                Device *device;
                for(int j=1; j<=6; j++){
                    device = [zone getDeviceByID:j];
                    if (device!=nil){
                        [deviceNames addObject:device.name ? device.name : @"Device"];
                        [deviceStatus addObject:device.type];
                    }else{
                        [deviceNames addObject:[NSString stringWithFormat:@"Device %d", j]];
                        [deviceStatus addObject:@"I"];
                    }
                }

                Mood *mood;
                for(int j=1; j<=3; j++){
                    mood = [zone getMoodByID:j];
                    if (mood != nil){
                        [deviceNames addObject:mood.name ? mood.name : @"Mood"];
                        [deviceStatus addObject:@"M"];
                    }else{
                        [deviceNames addObject:[NSString stringWithFormat:@"Mood %d", j]];
                        [deviceStatus addObject:@"m"];
                    }
                }
                
                mood = [zone getMoodByID:4];
                [deviceNames addObject:@"All Off"];
                [deviceStatus addObject:@"o"];
            }else{
                didFind = NO;
            }
        } else {
            didFind = NO;
        }
        
        if (!didFind) {
            [rooms addObject:[NSString stringWithFormat:@"Room %d", (hello+1)]];
            [roomStatus addObject:@"I"];

            //Add empty devices and moods
            for(int j=1; j<=6; j++){
                [deviceNames addObject:[NSString stringWithFormat:@"Device %d", j]];
                [deviceStatus addObject:@"I"];
            }
            
            for(int j=1; j<=3; j++){
                [deviceNames addObject:[NSString stringWithFormat:@"Mood %d", j]];
                [deviceStatus addObject:@"m"];
            }
            
            [deviceNames addObject:@"All Off"];
            [deviceStatus addObject:@"o"];
        }
    }
    
    // Add Sequences/Events
    for(Event *event in home.events){

        NSMutableArray *_eventDetails = [[NSMutableArray alloc] init];
        
        if (event.name)
            [_eventDetails addObject:event.name];
        
        NSMutableArray *_eventActions = [event getActionBreakdown];
        for (NSString *_command in _eventActions) {
            [_eventDetails addObject:_command];
        }
        
        [events addObject:_eventDetails];
    }
    
    // Add Timers
    for (LTimer *_timer in home.timers) {
        [timers addObject:[_timer toDictionary]];
    }
    
    // Add Heating Profiles
    for (Profile *_profile in home.profiles) {
        NSMutableDictionary *_heatingDict = [[NSMutableDictionary alloc] init];
        [_heatingDict setValue:[NSNumber numberWithInt:_profile.profileID] forKey:@"id"];
        [_heatingDict setValue:[NSNumber numberWithInt:_profile.type] forKey:@"type"];
        [_heatingDict setValue:[NSNumber numberWithInt:_profile.zoneId] forKey:@"zoneId"];
        [_heatingDict setValue:[NSNumber numberWithInt:(int)_profile.temp] forKey:@"temp"];
        [_heatingDict setValue:_profile.start forKey:@"start"];
        [_heatingDict setValue:_profile.end forKey:@"end"];
        [heatingProfiles addObject:_heatingDict];
    }

    // Settings
    NSString *_userEmail = nil;
    NSString *_userPassword = nil;
    
    if ([[UserPrefsHelper shared] getUserEmail].length > 0)
        _userEmail = [[UserPrefsHelper shared] getUserEmail];
    else
        _userEmail = @"";
    
    [settings addObject:_userEmail];
    
    
    if ([[UserPrefsHelper shared] getUserPassword].length > 0)
        _userPassword = [[UserPrefsHelper shared] getUserPassword];
    else
        _userPassword = @"";
    
    [settings addObject:_userPassword];
    
    
    if ([[UserPrefsHelper shared] getElecRate].length > 0)
        [settings addObject:[[UserPrefsHelper shared] getElecRate]];
    else
        [settings addObject:@""];
    
    
    if ([[UserPrefsHelper shared] getLocationInfo].length > 0)
        [settings addObject:[[UserPrefsHelper shared] getLocationInfo]];
    else
        [settings addObject:@"Birmingham, UK|52.48,-01.86"];
    
    
    if ([[UserPrefsHelper shared] getTimeZoneInfo].length > 0)
        [settings addObject:[[UserPrefsHelper shared] getTimeZoneInfo]];
    else
        [settings addObject:@"GMT Greenwich Mean Time|0"];
    
    
    if ([[UserPrefsHelper shared] getUserMac].length > 0)
        [settings addObject:[[UserPrefsHelper shared] getUserMac]];
    else
        [settings addObject:@""];
    
    
    if ([[UserPrefsHelper shared] getCode].length > 0)
        [settings addObject:[[UserPrefsHelper shared] getCode]];
    else
        [settings addObject:@"1111"];
    
    
    [settings addObject:[NSString stringWithFormat:@"%@%@", _userEmail, _userPassword]];
    
    /*************************************************************
     * Settings V2
     *************************************************************/
    
    if ([[UserPrefsHelper shared] getUserName])
        [settingsv2 setObject:[[UserPrefsHelper shared] getUserName] forKey:@"name"];
    
    if ([[UserPrefsHelper shared] getImpKwh])
        [settingsv2 setValue:[NSNumber numberWithInt:[[[UserPrefsHelper shared] getImpKwh] intValue]]  forKey:@"imp_kwh"];
    
    if ([[UserPrefsHelper shared] getSSID])
        [settingsv2 setObject:[[UserPrefsHelper shared] getSSID] forKey:@"ssid"];

    // Zone AND Device Ordering....
    NSMutableArray *_zoneOrderArray = [[NSMutableArray alloc] init];
    for (Zone *_zone in home.zones) {
        
        //DebugLog(@"Name: %@", _zone.name);
        
        if (_zone.devices.count > 6 && _zone.zoneID > 8) {
            NSMutableDictionary *_zoneDict = [[NSMutableDictionary alloc] init];
            [_zoneDict setValue:[NSNumber numberWithInt:_zone.zoneID] forKey:@"id"];
            [_zoneDict setValue:_zone.name forKey:@"name"];
            
            NSMutableArray *_deviceOrderArray = [[NSMutableArray alloc] init];
            
            for (Device *_device in _zone.devices) {
                NSMutableDictionary *_deviceDict = [[NSMutableDictionary alloc] init];
                
                if ([_device isMood])
                    [_deviceDict setValue:[NSNumber numberWithInt:_device.mood.moodID] forKey:@"id"];
                else
                    [_deviceDict setValue:[NSNumber numberWithInt:_device.deviceID] forKey:@"id"];
                
                [_deviceDict setValue:_device.name forKey:@"name"];
                [_deviceDict setValue:_device.type forKey:@"type"];
                [_deviceOrderArray addObject:_deviceDict];
            }
            
            [_zoneDict setObject:_deviceOrderArray forKey:@"devices"];
            [_zoneOrderArray addObject:_zoneDict];
            
        } else {
            NSMutableDictionary *_zoneDict = [[NSMutableDictionary alloc] init];
            [_zoneDict setValue:[NSNumber numberWithInt:_zone.zoneID] forKey:@"id"];
            [_zoneDict setValue:_zone.name forKey:@"name"];
            
            NSMutableArray *_deviceOrderArray = [[NSMutableArray alloc] init];
            
            for (Device *_device in _zone.devices) {
                if ([_device isMood]) {
                    [_deviceOrderArray addObject:@"mood"];
                    [_deviceOrderArray addObject:[NSNumber numberWithInt:_device.mood.moodID]];
                } else {
                    [_deviceOrderArray addObject:@"device"];
                    [_deviceOrderArray addObject:[NSNumber numberWithInt:_device.deviceID]];
                }
            }
            
            [_zoneDict setObject:_deviceOrderArray forKey:@"deviceDisplayOrderById"];
            [_zoneOrderArray addObject:_zoneDict];
        }
    }
    
    if (_zoneOrderArray.count > 0)
        [settingsv2 setObject:_zoneOrderArray forKey:@"zonesDisplayOrderById"];

    /*************************************************************
     * Build PLIST
     *************************************************************/

    // Arrange in Order
    OrderedDictionary *dict = [[OrderedDictionary alloc] initWithCapacity:9];
    [dict insertObject:deviceNames forKey:@"deviceNames" atIndex:0];
    [dict insertObject:deviceStatus forKey:@"deviceStatus" atIndex:1];
    [dict insertObject:roomStatus forKey:@"roomStatus" atIndex:2];
    [dict insertObject:rooms forKey:@"rooms" atIndex:3];
    [dict insertObject:events forKey:@"sequences" atIndex:4];
    [dict insertObject:settings forKey:@"settings" atIndex:5];
    [dict insertObject:timers forKey:@"timers" atIndex:6];
    [dict insertObject:heatingProfiles forKey:@"heatingProfiles" atIndex:7];
    [dict insertObject:settingsv2 forKey:@"settingsv2" atIndex:8];
    //DebugLog(@"dict = %@", dict);

    // Build XML
    NSMutableString *_xmlString = [NSMutableString string];
    [_xmlString appendString:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"];
    [_xmlString appendString:@"\n"];
    [_xmlString appendString:@"<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">"];
    [_xmlString appendString:@"\n"];
    [_xmlString appendString:@"<plist version=\"1.0\">"];
    [_xmlString appendString:@"\n"];
    [_xmlString appendString:@"<dict>"];
    [_xmlString appendString:@"\n"];

    for (int n = 0; n < dict.count; n++) {
        NSString *_key = [dict keyAtIndex:n];
        NSError *error = nil;
        NSData *plistData = [NSPropertyListSerialization dataWithPropertyList:[dict objectForKey:_key]
                                                                       format:NSPropertyListXMLFormat_v1_0
                                                                      options:0
                                                                        error:&error];
        NSString *_xml = [[NSString alloc] initWithData:plistData encoding:NSUTF8StringEncoding];
        
        [_xmlString appendFormat:@"<key>%@</key>", _key];
        [_xmlString appendString:@"\n"];

        NSRange _range = [_xml rangeOfString:@"<plist version=\"1.0\">"];
        NSString *_substring = [[_xml substringFromIndex:NSMaxRange(_range)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        _substring = [[_substring stringByReplacingOccurrencesOfString:@"</plist>" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [_xmlString appendString:_substring];
        [_xmlString appendString:@"\n"];
    }
    
    [_xmlString appendString:@"</dict>"];
    [_xmlString appendString:@"\n"];
    [_xmlString appendString:@"</plist>"];
    
    _xmlString = [[XMLConfig localise:_xmlString] mutableCopy];
    DebugLog(@"_xmlString = %@", _xmlString);
    
    return _xmlString;
    
    
   // OLD WAY
/*
    // Convert to plist XML to send to webservice
    NSError *error = nil;
    NSData *plistData = [NSPropertyListSerialization dataWithPropertyList:dict
                                                                   format:NSPropertyListXMLFormat_v1_0
                                                                  options:0
                                                                    error:&error];
    NSString *_xml = [[NSString alloc] initWithData:plistData encoding:NSUTF8StringEncoding];
    //DebugLog(@"xml = %@", _xml);
    
    return _xml;
 */
}


// For Sending plist only
+ (NSString *)localise:(NSString *)str {

    NSString * str1 = @"";
    
    str1 = [str stringByReplacingOccurrencesOfString:@"Å" withString:@"&#197;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"å" withString:@"&#229;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Á" withString:@"&#193;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"É" withString:@"&#201;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Í" withString:@"&#205;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ó" withString:@"&#211;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ú" withString:@"&#218;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ý" withString:@"&#221;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"á" withString:@"&#225;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"é" withString:@"&#233;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"í" withString:@"&#237;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ó" withString:@"&#243;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ú" withString:@"&#250;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ý" withString:@"&#253;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ä" withString:@"&#196;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ë" withString:@"&#203;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ï" withString:@"&#207;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ö" withString:@"&#214;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ü" withString:@"&#220;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ÿ" withString:@"&#376;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ä" withString:@"&#228;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ë" withString:@"&#235;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ï" withString:@"&#239;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ö" withString:@"&#246;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ü" withString:@"&#252;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ÿ" withString:@"&#255;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"À" withString:@"&#192;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"È" withString:@"&#200;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ì" withString:@"&#204;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ò" withString:@"&#210;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ù" withString:@"&#217;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"à" withString:@"&#224;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"è" withString:@"&#232;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ì" withString:@"&#236;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ò" withString:@"&#242;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ù" withString:@"&#249;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Â" withString:@"&#194;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ê" withString:@"&#202;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Î" withString:@"&#206;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ô" withString:@"&#212;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Û" withString:@"&#219;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"â" withString:@"&#226;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ê" withString:@"&#234;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"î" withString:@"&#238;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ô" withString:@"&#244;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"û" withString:@"&#251;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Æ" withString:@"&#198;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"æ" withString:@"&#230;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"Ø" withString:@"&#216;"];
    str1 = [str1 stringByReplacingOccurrencesOfString:@"ø" withString:@"&#248;"];
    
    return str1;
}

@end
