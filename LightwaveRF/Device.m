//
//  Device.m
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 21/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import "Device.h"
#import "Mood.h"
#import "Zone.h"

@interface Device()

@property (strong, nonatomic, readwrite) NSString *hardwareKey;
@property (nonatomic, readwrite) int roomID;
@property (strong, nonatomic, readwrite) Device *unknown;
@property (strong, nonatomic) NSDictionary *deviceCommands;

@end

@implementation Device

@synthesize name = _name;
@synthesize type = _type;
@synthesize assumedState = _assumedState;
@synthesize paired = _paired;
@synthesize locked = _locked;
@synthesize lastOnState = _lastOnState;
@synthesize deviceID = _deviceID;
@synthesize mood = _mood;
@synthesize zone= _zone;
@synthesize deviceCommands = _deviceCommands;
@synthesize didSetSucceed;

-(void) setName:(NSString *) name
{
    if ([name length] > 0)
    {
        _name = name;
    }
}

-(void) setAssumedState:(int)setting
{
    NSString *message;
    
    if ([self isMood])
    {
        if ([self.mood isAllOff])
            message = [NSString stringWithFormat:@"R%dFm%d", self.mood.zoneID, self.mood.moodID];
        else
            message = [NSString stringWithFormat:@"R%dFa", self.mood.zoneID];
        
    } else if ([self isOpenClose] ||
               [self isFSL500W] ||
               [self isFSL3000W]) {
        message = [NSString stringWithFormat:@"%@F", self.hardwareKey];
        _assumedState = setting;
        
        switch(setting){
			case 0://Close
				[message stringByAppendingString:@")|Close|"];
				break;
			case 1://Open
				[message stringByAppendingString:@"(|Open|"];
				break;
			case 2://Stop
				[message stringByAppendingString:@"^|Stop|"];
				break;
        }
        [message stringByAppendingString:self.name];
    }
    else
    {
        _assumedState = setting;
        if (setting != 0)
        {
            self.lastOnState = setting;
        }
        else
        {
            if ([self isDimmer]) {
                if (self.lastOnState == 0)
                    self.lastOnState = 16;
            }
        }
    
        if (setting == 0)
        {
            if (!self.zone)
                message = [NSString stringWithFormat:@"%@F0|Set %@|to off", self.hardwareKey, self.name];
            else
                message = [NSString stringWithFormat:@"%@F0|%@|%@ off", self.hardwareKey, self.zone.name, self.name];
            
        }
        else
        {
            if ([self.type isEqualToString:SOCKET])
            {
                if (!self.zone)
                    message = [NSString stringWithFormat:@"%@%@|Set %@|to on", self.hardwareKey, [self getCommand:setting], self.name];
                else
                    message = [NSString stringWithFormat:@"%@%@|%@|%@ on", self.hardwareKey, [self getCommand:setting], self.zone.name, self.name];
            }
            else
            {
                if (!self.zone)
                {
                    message = [NSString stringWithFormat:@"%@%@|Set %@|to %d%%", self.hardwareKey, [self getCommand:setting], self.name, (int)((setting/32.0) * 100.0)];
                }
                else
                {
                    message = [NSString stringWithFormat:@"%@%@|%@|%@ %d%%", self.hardwareKey, [self getCommand:setting], self.zone.name, self.name, (int)((setting/32.0) * 100.0)];
                }
            }
        }
    }
        
    //DebugLog(@"Device set assumed state to: %@", message);
    //UDPService *udp = [UDPService shared];
    //[udp send:message tag:0 callingClass:@"Device"];
}

-(void) setPaired:(BOOL) paired
{
    _paired = paired;
    if ([self.type isEqualToString:DIMMER])
    {
        self.lastOnState = 32;
    }
}

-(NSString *) hardwareKey
{
    if (!self.zone)
        return @"";
    
    NSString *hardwareKey = [NSString stringWithFormat:@"!R%d", self.zone.zoneID];
    
    if ([self isMood])
    {
        if ([self.mood isAllOff])
        {
            hardwareKey = [hardwareKey stringByAppendingString:@"Fa"];
        }
        else
        {
            hardwareKey = [hardwareKey stringByAppendingFormat:@"FmP%d", self.mood.moodID];
        }
    }
    else
    {
        hardwareKey = [hardwareKey stringByAppendingFormat:@"D%d", self.deviceID];
    }
    
    return hardwareKey;
}

-(int) roomID
{
    if (!self.zone)
    {
        return -1;
    }
    else
    {
        return self.zone.zoneID;
    }
}

-(Device *) unknown
{
    self.unknown = [[Device alloc] initWithName:@"Not found" deviceID:0 type:@"Unknown" lastOnState:0 paired:YES zone:nil];
    return self.unknown;
}

-(NSString *) zoneName
{
    if (self.zone)
        return self.zone.name;
    
    return @"";
}

-(NSDictionary *) deviceCommands
{
    if (!_deviceCommands)
    {
        
        _deviceCommands = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"FdP", DIMMER,
                           @"F", LIGHT_SWITCH,
                           @"FdP", HEATING,
                           @"F", SOCKET,
                           @"", RADIATOR,
                           @"", COLOUR_LEDS,
                           @"", MAG_SWITCH,
                           @"", PIR_SENSOR,
                           @"", CAMERA,
                            nil];
    }
    return _deviceCommands;
}

-(id) initWithName:(NSString *) name deviceID:(int) deviceID type:(NSString *) type lastOnState:(int) lastOnState paired:(BOOL) paired mood:(Mood *) mood zone:(Zone *) zone
{
    if (self = [super init])
    {
        self.name = name;
        self.deviceID = deviceID;
        self.type = type;
        self.lastOnState = lastOnState;
        self.paired = paired;
        self.mood = mood;
        self.zone = zone;
        self.locked = kDeviceUnlocked;
        self.didSetSucceed = YES;
    }
    return self;
}

-(id) initWithName:(NSString *) name deviceID:(int) deviceID type:(NSString *) type lastOnState:(int) lastOnState paired:(BOOL) paired zone:(Zone *) zone
{
    return [self initWithName:name deviceID:deviceID type:type lastOnState:lastOnState paired:paired mood:nil zone:zone];
}

-(id) initWithName:(NSString *) name deviceID:(int) deviceID type:(NSString *) type zone:(Zone *) zone
{
    return [self initWithName:name deviceID:deviceID type:type lastOnState:0 paired:NO mood:nil zone:zone];
}

-(id) initWithMood:(Mood *)mood
{
    return [self initWithName:mood.name deviceID:-1 type:@"M" lastOnState:0 paired:NO mood:mood zone:mood.zone];
}

-(NSString *) getCommand:(int) setting
{
        
    if ([self isOpenClose] ||
        [self isFSL500W] ||
        [self isFSL3000W]) {
        
        switch(setting){
			case 0://Close
				return @"F)";
				break;
			case 1://Open
				return @"F(";
				break;
			case 2://Stop
				return @"F^";
                break;
            default:
                return @"";
                break;
        }
    } else {
        if (self.type == DIMMER && setting==0){
            return @"F0";
        }else if (self.type == DIMMER && setting==-2){
            return @"Fo";
        }else
            return [[self.deviceCommands valueForKey:self.type] stringByAppendingString:[NSString stringWithFormat:@"%d", setting]];
    }
}

-(BOOL) isRadiator
{
    if ([self.type length]==1){
        return [self.type isEqualToString:@"R"];
    }
    return [self.type isEqualToString:RADIATOR];
}

-(BOOL) isColourLEDS
{
    if ([self.type length]==1){
        return [self.type isEqualToString:@"L"];
    }
    return [self.type isEqualToString:COLOUR_LEDS];
}

-(BOOL) isSocket
{
    if ([self.type length]==1){
        return [self.type isEqualToString:@"O"];
    }
    return [self.type isEqualToString:SOCKET];
}

-(BOOL) isDimmer
{
    if ([self.type length]==1){
        return [self.type isEqualToString:@"D"];
    }
    return [self.type isEqualToString:DIMMER];
}

-(BOOL) isLightSwitch
{
    return [self.type isEqualToString:LIGHT_SWITCH];
}

-(BOOL) isOpenClose{
    if ([self.type length]==1){
        return [self.type isEqualToString:@"P"];
    }
    return [self.type isEqualToString:OPEN_CLOSE];
}

-(BOOL) isFSL500W {
    if ([self.type length]==1){
        return [self.type isEqualToString:@"Q"];
    }
    return [self.type isEqualToString:FSL500W];
}

-(BOOL) isFSL3000W {
    if ([self.type length]==1){
        return [self.type isEqualToString:@"R"];
    }
    return [self.type isEqualToString:FSL3000W];
}

-(BOOL) isHeating
{
    if ([self.type length]==1){
        return [self.type isEqualToString:@"H"];
    }
    return [self.type isEqualToString:HEATING];
}

-(BOOL) isMagSwitch
{
    return [self.type isEqualToString:MAG_SWITCH];
}

-(BOOL) isPir;
{
    return [self.type isEqualToString:PIR_SENSOR];
}

-(BOOL) isCamera
{
    return [self.type isEqualToString:CAMERA];
}

-(BOOL) isMood
{
    if ([self.type length]==1){
        return [self.type isEqualToString:@"M"];
    }
    return [self.type isEqualToString:MOOD];
}

-(BOOL) isActiveDevice
{
    return !self.isPassiveDevice;
}

-(BOOL) isPassiveDevice;
{
    return [self.type isEqualToString:MAG_SWITCH] || [self.type isEqualToString:PIR_SENSOR] || [self.type isEqualToString:CAMERA] || [self.type isEqualToString:MOOD];
}

- (UIImage *)getIconImage {
    //DebugLog(@"name = %@, type = %@", self.name, self.type);
    //    D - dimmer
    //    P - Open Close
    //    O - Socket
    //    M - Mood
    //    H - Heating
    
    if ([self isLightSwitch]) {
        return [UIImage imageNamed:@"icon_light.png"];
    }
    
    else if ([self isDimmer]) {
        return [UIImage imageNamed:@"icon_light.png"];
    }
    
    else if ([self isHeating]) {
        return [UIImage imageNamed:@"icon_heating.png"];
    }

    else if ([self isMood]) {
        if ([self getMood].moodID == 1)
            return [UIImage imageNamed:@"icon_mood1.png"];
        
        else if ([self getMood].moodID == 2)
            return [UIImage imageNamed:@"icon_mood2.png"];
        
        else if ([self getMood].moodID == 3)
            return [UIImage imageNamed:@"icon_mood3.png"];
                
        else if ([self getMood].moodID == 4)
            return [UIImage imageNamed:@"icon_mood4.png"];
        
        else
            return [UIImage imageNamed:@"icon_mood1.png"];
    }

    else if ([self isOpenClose] ||
             [self isFSL500W] ||
             [self isFSL3000W]) {
        
        return [UIImage imageNamed:@"icon_curtain.png"];
    }
    
    else if ([self isSocket]) {
        return [UIImage imageNamed:@"icon_power.png"];
    }
     
    else if ([self isRadiator]) {
        return [UIImage imageNamed:@"icon-power.png"];
    }
    
    else if ([self isColourLEDS]) {
        return [UIImage imageNamed:@"icon_led.png"];
    }
    
    else if ([self isMagSwitch]) {
        return [UIImage imageNamed:@"icon-power.png"];
    }
    
    else if ([self isPir]) {
        return [UIImage imageNamed:@"icon-power.png"];
    }
    
    else if ([self isCamera]) {
        return [UIImage imageNamed:@"icon-power.png"];
    }
    
    else if ([self isActiveDevice]) {
        return [UIImage imageNamed:@"icon-power.png"];
    }
    
    else if ([self isPassiveDevice]) {
        return [UIImage imageNamed:@"icon-power.png"];
        
    //} else if ([self isDelay]) {
    //    return [UIImage imageNamed:@"icon-time.png"];
        
    } else
        return nil;
}

-(int) turnOn
{
    DebugLog(@"Device turn on, last on state = %d", self.lastOnState);
    if (self.lastOnState > 0)
    {
        self.assumedState = self.lastOnState;
    }
    else if (self.isDimmer)
    {
        self.lastOnState = DIMMER_MAX;
        self.assumedState = self.lastOnState;
    }
    else if (self.isHeating)
    {
        self.lastOnState = DEFAULT_AMBIENT_TEMPERATURE;
        self.assumedState = self.lastOnState;
    }
    else
    {
        self.assumedState = 1;
    }
    return self.assumedState;
}

-(int) turnOff
{
    DebugLog (@"Device turn off");
    self.assumedState = 0;
    return 0;
}

-(BOOL) isPaired
{
    return self.paired;
}

-(void) makePairing
{
    self.paired = YES;
}

-(void) openPairing
{
    self.paired = NO;
}

-(Mood *)getMood{
    if ([self isMood]){
        return self.mood;
    }
    return nil;
}

- (int)getSetting:(ConstantsIsOpenClose)_isOpenClose
       isOnOrOpen:(BOOL)_isOnOrOpen
       brightness:(int)_brightness {
    
    // Get Device Setting
    int _setting = _isOnOrOpen ? 1 : 0;
    
    if ([self isSocket]) {
        
        // Socket       =       1 for On      0 for Off
        _setting = _isOnOrOpen ? 1 : 0;
        
    } else if ([self isOpenClose] ||
               [self isFSL500W] ||
               [self isFSL3000W]) {
        
        // Open         =       1 for open    Close 0    Stop 2
        _setting = _isOpenClose;
        
    } else if ([self isDimmer] || [self isColourLEDS]) {
        
        // Dimmer       =       Off is 0    On is -2     nothing set is -1       dim 1-32 (NOT 0)
        _setting = _isOnOrOpen ? _brightness : 0;
        
    } else if ([self isMood]) {
        
        // Mood         =       always 1
        _setting = 1;
    }
        
    return _setting;
}

-(NSDictionary *) toDictionary
{
    NSDictionary *deviceDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:self.deviceID] ,@"id",
                                      self.type, @"type",
                                      self.name, @"name",
                                      [NSNumber numberWithInt:self.assumedState],@"assumedState",
                                      [NSNumber numberWithInt:self.lastOnState], @"lastOnState",
                                      [NSNumber numberWithInt:self.paired], @"isPaired",
                                      nil];
    return deviceDictionary;
}

-(NSData *) toJSON
{
    NSError *error = nil;
    NSDictionary *jsonDevice = self.toDictionary;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDevice options:NSJSONWritingPrettyPrinted error:&error];
    if (!error)
    {
        //DebugLog([NSString stringWithFormat:@"JSON device data:\n%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]);
        return jsonData;
    }
    else
    {
        return nil;
    }
}

-(void) renameFrom:(NSString *)fromName to:(NSString *)toName
{
    if ([fromName isEqualToString:self.name] && !self.zone)
    {
        self.zone.name = toName;
    }
}

-(void) deleteDeviceNamed:(NSString *)deviceName
{
    if ([deviceName isEqualToString:self.name] && !self.zone)
    {
        [self.zone removeDevice:self];
    }
}

@end
