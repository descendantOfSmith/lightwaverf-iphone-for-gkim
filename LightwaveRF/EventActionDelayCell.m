//
//  ChooseProductSliderCell.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 19/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "EventActionDelayCell.h"

@interface EventActionDelayCell ()
@property (nonatomic, strong) UIView *delaySeperator;
@end

@implementation EventActionDelayCell

@synthesize delaySeperator;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
    }
    return self;
}

- (void)dealloc {
    
}

+ (float)getCellHeight {
    return 88.0f;
}

- (void)setupCellWithTitle:(NSString*)tmpTitle
                  subtitle:(NSString *)tmpSubtitle
                detailText:(NSString *)tmpDetailText
                    device:(Device *)_device
                     delay:(NSString *)_delay
                 indexPath:(NSIndexPath *)_indexPath
                  delegate:(id<EventActionDelayCellDelegate>)_delegate {

    // Setup Initial Cell
    [self setupCellWithTitle:tmpTitle
                    subtitle:tmpSubtitle
                  detailText:tmpDetailText
                      device:_device
                   indexPath:_indexPath
                    delegate:_delegate];
    
    /*************************************
     *  Seperator View
     *************************************/
    if (!self.delaySeperator) {
        self.delaySeperator = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                                       0.0f,
                                                                       self.containingView.frame.size.width,
                                                                       1.0f)];
        [self.delaySeperator setBackgroundColor:[UIColor colorWithDivisionOfRed:239.0f green:239.0f blue:239.0f alpha:1.0f]];
        [self.containingView addSubview:self.delaySeperator];
    }
    
    [self setupCellWithDelay:_delay];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    [super positionViewsIsEditing:tmpIsEditing];
    
    if (self.delaySeperator) {
        // Delay Seperator
        [self.delaySeperator setCenterX:self.containingView.center.x];
        [self.delaySeperator alignToBottomOfView:self.subTitleLabel padding:4.0f];
        [self.delaySeperator setAlpha:tmpIsEditing ? 0.0f : 1.0f];
        
        // Delay Icon
        [self.delayImageView setFrameX:self.iconImageView.frame.origin.x];
        [self.delayImageView alignToBottomOfView:self.delaySeperator padding:5.0f];
    } else {
        // Delay Icon
        [self.delayImageView setFrameX:tmpIsEditing ? 35.0f : 5.0f];
        [self.delayImageView setCenterY:self.containingView.center.y];
    }
    
    // Delay Title
    if (tmpIsEditing)
        [self.delayTitleLabel alignToRightOfView:self.delayImageView padding:5.0f matchVertical:YES];
    else {
        [self.delayTitleLabel alignToRightOfView:self.delayImageView padding:5.0f matchVertical:YES];
        [self.delayTitleLabel setCenterY:self.delayImageView.center.y];
    }
    
    // Delay Edit Mode Label
    if (self.delayEditModeLabel) {
        [self.delayEditModeLabel alignToBottomOfView:self.delayTitleLabel padding:0.0f matchHorizontal:YES];
        [self.delayEditModeLabel setAlpha:tmpIsEditing ? 1.0f : 0.0f];
    }
    
    // Delay Detail
    float padding = 10.0f;
    [self.delayDetailLabel setFrameX:(self.containingView.frame.size.width - self.delayDetailLabel.frame.size.width) - padding];
    [self.delayDetailLabel setCenterY:self.delayTitleLabel.center.y];
    [self.delayDetailLabel setAlpha:tmpIsEditing ? 0.0f : 1.0f];

    // Set Delay Edit Mode Button
    if (self.editDelayEditModeButton && self.delayEditModeLabel) {

        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [self.editDelayEditModeButton setFrameX:141.0f];
        } else {
            [self.editDelayEditModeButton setFrameX:148.0f];
        }
        
        [self.editDelayEditModeButton setCenterY:self.delayImageView.center.y];
        [self.editDelayEditModeButton setAlpha:tmpIsEditing ? 1.0f : 0.0f];
    }
}

@end
