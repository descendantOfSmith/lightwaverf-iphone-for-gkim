//
//  AccessoryView.m
//  Prologis
//
//  Created by Dan's iMac on 14/01/2013.
//
//

#import "AccessoryView.h"

@implementation AccessoryView

@synthesize view, delegate, previousBtn, nextBtn, toolbar, doneBtn;

- (id)init
{
    self = [super init];
    if (self) {
        self.view = [[[NSBundle mainBundle] loadNibNamed:@"AccessoryView" owner:self options:nil] lastObject];
        [self.previousBtn setTitle:NSLocalizedString(@"previous", @"Previous")];
        [self.nextBtn setTitle:NSLocalizedString(@"next", @"Next")];
        [self.doneBtn setTitle:NSLocalizedString(@"done", @"Done")];
    }
    return self;
}

- (void)dealloc {
    
    DebugLog(@"DEALLOC");
    
    self.view = nil;
    self.delegate = nil;
    self.previousBtn = nil;
    self.nextBtn = nil;
    self.toolbar = nil;
    self.doneBtn = nil;
}

- (void)hidePreviousButton {
    NSMutableArray *items = [self.toolbar.items mutableCopy];
    [items removeObject: self.previousBtn];
    self.toolbar.items = items;
}

- (void)hideNextButton {
    NSMutableArray *items = [self.toolbar.items mutableCopy];
    [items removeObject: self.nextBtn];
    self.toolbar.items = items;
}

#pragma mark -
#pragma mark Text Field Actions

- (IBAction)doneTextFieldPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(doneTextFieldPressed:)])
        [self.delegate doneTextFieldPressed:self];
}

- (IBAction)previousTextFieldPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(previousTextFieldPressed:)])
        [self.delegate previousTextFieldPressed:self];
}

- (IBAction)nextTextFieldPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(nextTextFieldPressed:)])
        [self.delegate nextTextFieldPressed:self];
}

@end
