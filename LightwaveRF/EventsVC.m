//
//  EventsVC.m
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "EventsVC.h"
#import "Home.h"
#import "Event.h"
#import "AppDelegate.h"
#import "EventVC.h"
#import "Toast+UIView.h"
#import "EventActionsVC.h"
#import "EventDevicesVC.h"
#import "LWErrorView.h"
#import "ButtonsHelper.h"

@interface EventsVC ()

@end

@implementation EventsVC

@synthesize nav_bar;
@synthesize events_tbl;
@synthesize headerBarView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[self.view setBackgroundColor:[UIColor lightGrayColor]];
    [self.events_tbl setBackgroundColor:[AppStyleHelper getViewBackgroundColor]];
    [nav_bar setBackgroundColor:[AppStyleHelper getNavBarBackgroundColor]];

    nav_bar.topItem.title = NSLocalizedString(@"events", @"Events");
    [self setupTopRightButton];
    [self setupTopLeftButton];
    
    [events_tbl setDataSource:self];
        
    // Help popup
    [self showOneOffAlertForLocalisedKey:@"help3" duration:5.0f];
    
    [self setupUI];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        self.events_tbl.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 20.0f, 0.0f);
    
    if ([self.events_tbl respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.events_tbl setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.events_tbl.editing) {
        [self edit];
    }
}

- (void)setupUI {
    [self setupTopButton];
    
    if ([self.events_tbl respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.events_tbl setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)setupTopButton {
    UIButton *_button = nil;
    
    if ([AppStyleHelper isMegaMan]) {
        UIImageView *imageView = [ViewBuilderHelper getImageViewWithFilename:@"add-grey-banner.png" atX:0.0f atY:0.0f];
        [imageView setFrameHeight:self.headerBarView.frame.size.height];
        [self.headerBarView insertSubview:imageView atIndex:0];
        
        _button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"stop_all_events", @"stop_all_events")
                                                    type:kButtonGreyBacking
                                                     tag:kBtnStop
                                                   width:313
                                                  height:35
                                                  target:self
                                                selector:@selector(stopAllPressed:)];
        [_button setBackgroundImage:nil forState:UIControlStateNormal];
        [_button setBackgroundImage:nil forState:UIControlStateHighlighted];
        [_button setBackgroundImage:nil forState:UIControlStateSelected];
        [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
    } else {
        _button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"stop_all_events", @"stop_all_events")
                                              type:kButtonGreyBacking
                                               tag:kBtnStop
                                             width:313
                                            height:35
                                            target:self
                                          selector:@selector(stopAllPressed:)];
    }

    [_button setFrameX:3];
    [_button setFrameY:5];
    [self.headerBarView addSubview:_button];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [events_tbl reloadData];
}

- (void)setupTopRightButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"add", nil)
                                                       type:kButtonArrowRight
                                                        tag:0
                                                      width:45
                                                     height:18
                                                     target:self
                                                   selector:@selector(add)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        nav_bar.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];

    } else {
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add)];
        nav_bar.topItem.rightBarButtonItem = addButton;
    }
}

- (void)setupTopLeftButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:events_tbl.isEditing ? NSLocalizedString(@"done2", nil) : NSLocalizedString(@"edit", nil)
                                                       type:kButtonArrowLeft
                                                        tag:0
                                                      width:45
                                                     height:18
                                                     target:self
                                                   selector:@selector(edit)];
        nav_bar.topItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        
    } else {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:events_tbl.isEditing ? NSLocalizedString(@"done2", nil) : NSLocalizedString(@"edit", nil)
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self
                                                                      action:@selector(edit)];
        nav_bar.topItem.leftBarButtonItem = editButton;
    }
}

- (void)viewDidUnload
{
    [self setNav_bar:nil];
    [self setEvents_tbl:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSString *)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    Home *home = [Home shared];
    return [home.events count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"EventCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        UIView *v = [[UIView alloc] init];
        v.backgroundColor = [UIColor colorWithRed:(140.0/255.0) green:(179.0/255.0) blue:(80.0/255.0) alpha:1.0]; // any color of your choice.
        cell.selectedBackgroundView = v;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    Home *home = [Home shared];
    Event *event = [home.events objectAtIndex:indexPath.row];
    
    cell.textLabel.text = event.name;
    
    if ([AppStyleHelper isMegaMan])
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellarrow.png"]];
    else
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Home *home = [Home shared];
    Event *event = [home.events objectAtIndex:indexPath.row];
    
    
    // There is a bug in iOS7 when using 'presentViewController' in 'didSelectRowAtIndexPath'
    // where the view has a delay before it is pushed. To fix this delay so that it pushes automatically,
    // we auto call it on the main thread, see below.
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[[EventVC alloc] initWithNibName:kVCXibName bundle:nil event:event]];
        [self presentViewController:nc animated:YES completion:^(void){
            
        }];
        
    });
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
}

#pragma mark - Editing Table View

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    Home *home = [Home shared];
    Event *_event = [home.events objectAtIndex:indexPath.row];
    if (_event.eventID == 0 || _event.eventID == 1) {
        // Disable Deleting Home & Away Events
        return UITableViewCellEditingStyleNone;
    }
    
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    Home *home = [Home shared];
    if (editingStyle == UITableViewCellEditingStyleDelete){
        Event *_event = [home.events objectAtIndex:indexPath.row];
        [_event deleteEvent];
        [home removeEvent:_event];
        [home saveEvents:YES];//NO refers to not saving the actions with the events
        [events_tbl reloadData];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    Home *home = [Home shared];
    return (indexPath.row != [home.events count]);
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    Home *home = [Home shared];
    Event *event = [home.events objectAtIndex:sourceIndexPath.row];
    [home.events removeObject:event];
    [home insertEvent:event atIndex:destinationIndexPath.row];
    [home saveEvents:NO];
    [events_tbl reloadData];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch ([alertView tag]) {
        case kEventsVCAddEventAlert:
        {
            NSString *text = [[alertView textFieldAtIndex:0] text];
            if (buttonIndex != 0) { // Continue
                if (text.length > 0) {
                    if (text.length > 16) {
                        [self showError:NSLocalizedString(@"message21", @"16 char limit.")];
                        
                    } else {
                        Home *home = [Home shared];
                        Event *_event = [[Event alloc] initWithName:text andID:[home getFreeEventID]];
                        [home addEvent:_event];
                        [home saveData:@"Events"];
                        [events_tbl reloadData];
                        
                        // Choose Actions for new Event
                        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[[EventActionsVC alloc] initWithNibName:kVCXibName
                                                                                                                                                 bundle:nil
                                                                                                                                                  event:_event]];
                        [self presentViewController:nc animated:NO completion:^(void){
                            [nc pushViewController:[[EventDevicesVC alloc] initWithNibName:kVCXibName bundle:nil event:_event] animated:YES];
                        }];
                    }

                } else {
                    [self showError:NSLocalizedString(@"message16", @"Enter a valid event name.")];
                }
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Actions

- (void)edit {
    [events_tbl setEditing:!events_tbl.editing animated:YES];
    [events_tbl reloadData];
    [self setupTopLeftButton];
}

- (void)add{

    Home *home = [Home shared];
    if ([home isFreeEventSlots]) {
        
        LWErrorView *alert = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"add_event", @"Add Event")
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel")
                                              otherButtonTitles:NSLocalizedString(@"continue", @"Continue"), nil];
        [alert setTag:kEventsVCAddEventAlert];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField * alertTextField = [alert textFieldAtIndex:0];
        alertTextField.keyboardType = UIKeyboardTypeDefault;
        alertTextField.placeholder = NSLocalizedString(@"name", @"Name");
        alertTextField.text = @"";
        [alertTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        [alert show];
        
    } else {
        [self.view makeToast:[NSString stringWithFormat:NSLocalizedString(@"error_events_limit", nil), MAX_EVENTS]];
    }
}

- (IBAction)stopAllPressed:(id)sender {
    [self stopAllEventsUsingUDP:YES];
}

#pragma mark - Stop All Events

- (void)stopAllEventsUsingUDP:(BOOL)_useUDP {
    [self showSpinnerWithText:@""];
    
    if (_useUDP) {
        
        // UDP
        UDPService *udp = [UDPService shared];
        [udp stopAllEventsWithTag:0 callingClass:[self getClassName:[self class]]];
        
    } else {
    
        // Remote Settings
        RemoteSettings *remote = [RemoteSettings shared];
        [remote stopAllEventsWithCallingClass:[self getClassName:[self class]] tag:0];
        
    }
}

- (void)allEventsStopped {
    [self.view makeToast:NSLocalizedString(@"stop_all_events_message", @"All events stopped")];
}

#pragma mark - Remote Delegate

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kRemoteClassNameKey];
    //NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
        {
            /*************************
             * Error
             *************************/
            NSError *error = [_dict objectForKey:kRemoteErrorKey];
            //NSString *message = NSLocalizedString(@"web_service_error", @"There was a problem connecting to the Lightwave server");
            //NSString *str = [NSString stringWithFormat:@"%@\n%@", message, [error localizedDescription]];
            [self showError:[error localizedDescription]];
        }
        
        else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
        {
            /*************************
             * Success
             *************************/
            [self allEventsStopped];
        }
        
        else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
        {
            /*************************
             * Failed
             *************************/
            NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
            [self showError:_failed];
        }
    }
}

#pragma mark - UDP Delegate

- (void)receivedUDPServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kUDPClassNameKey];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        if ([[_notification name] isEqualToString:kUDPErrorNotificationKey])
        {
            /*************************
             * Error
             *************************/
            // Attempt the request using RemoteSettings...
            [self stopAllEventsUsingUDP:NO];
        }
        
        else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
        {
            /*************************
             * Success
             *************************/
            [self allEventsStopped];
        }
        
        else if ([[_notification name] isEqualToString:kUDPFailedNotificationKey])
        {
            /*************************
             * Failed
             *************************/
            
            BOOL _attemptRemote = [[_dict objectForKey:kUDPAllowSwitchToRemoteKey] boolValue];
            
            if (_attemptRemote) {
                // Attempt the request using RemoteSettings...
                [self stopAllEventsUsingUDP:NO];
            } else {
                DebugLog(@"Attempt Remote Disabled...");
            }
        }
    } 
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
