//
//  IntroPageVC.m
//  LightwaveRF
//
//  Created by Nik Lever on 06/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "IntroPageVC.h"

@interface IntroPageVC ()

@end

@implementation IntroPageVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithPageNumber:(int)page {
    if (self = [super initWithNibName:@"IntroPageVC" bundle:nil])    {
        pageNumber = page;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _intro_img.image = [UIImage imageNamed:[NSString stringWithFormat:@"intro%d", (pageNumber + 1)]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setIntro_img:nil];
    [super viewDidUnload];
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
