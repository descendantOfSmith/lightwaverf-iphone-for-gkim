//
//  ColourLEDsVC.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 29/10/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "RootViewController.h"
#import "Device.h"
#import "Zone.h"

@interface ColourLEDsVC : RootViewController {
    
}

@property (nonatomic, assign) BOOL showChangeCyclingButton;
    
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               device:(Device *)_device
                 zone:(Zone *)_zone;

@end
