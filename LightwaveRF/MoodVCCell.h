//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Mood.h"

@interface MoodVCCell : UITableViewCell {

}

@property (nonatomic, strong) Mood *mood;
@property (nonatomic, assign) int zoneID;
@property (strong, nonatomic) IBOutlet UILabel *name_lbl;
@property (strong, nonatomic) IBOutlet UIImageView *icon_img;
@property (strong, nonatomic) IBOutlet UIImageView *active_img;
@property (strong, nonatomic) NSTimer *fadeActive_tmr;

+ (float)getCellHeight;

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing;

- (void)setupCellWithMood:(Mood *)_mood zoneID:(int)_zoneID;

- (IBAction)setMoodPressed:(id)sender;

@end
