//
//  ChooseLanguageVC.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 10/12/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ChooseLanguageVC.h"
#import "UINavigationController+Utilities.h"
#import "AppDelegate.h"
#import "ButtonsHelper.h"

@interface ChooseLanguageVC ()
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *availableLanguages;
@end

@implementation ChooseLanguageVC

@synthesize tableView, availableLanguages;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.availableLanguages = [[UserPrefsHelper shared] getAvailableLanguages];
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)setupUI {
    [self setupNavBar];
    [self setupTableView];
}

- (void)setupNavBar {
    self.navigationItem.title = NSLocalizedString(@"choose_language", @"choose_language");
    [self setupLeftNavBarButton];
}

- (void)setupLeftNavBarButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"cancel", @"Cancel")
                                                       type:kButtonArrowLeft
                                                        tag:kBtnBack
                                                      width:55
                                                     height:18
                                                     target:self
                                                   selector:@selector(buttonPressed:)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        [backButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backButton;
        
    } else {
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                                          style:UIBarButtonItemStyleBordered
                                                                         target:self
                                                                         action:@selector(buttonPressed:)];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;
    }
}

- (void)setupTableView {
    float padding = 0.0f;
    self.tableView = [ViewBuilderHelper getTableViewWithWidth:self.scrollView.frame.size.width - (padding * 2)
                                                       height:SCREEN_HEIGHT() - (kNavBar.frame.size.height + STATUS_BAR_HEIGHT)
                                                        style:UITableViewStylePlain
                                                       target:self];
    [self.tableView setFrameX:padding];
    [self.tableView setFrameY:0.0f];
    self.tableView.separatorStyle= UITableViewCellSeparatorStyleSingleLine;
    [self.view addSubview:self.tableView];
    self.heightOffset += self.tableView.frame.size.height;
    [self.tableView reloadData];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tmpTableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    return self.availableLanguages.count;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 44.0f;
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    const char* className = class_getName([self class]);
    NSString *cellIdentifier = cellIdentifier = [NSString stringWithFormat:@"UITableViewCell-%s", className];
    UITableViewCell *cell = (UITableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell = [self setupInitialCell:cell reuseIdentifier:cellIdentifier];
    
    NSString *_languageCode = [self.availableLanguages objectAtIndex:indexPath.row];
    NSString *_language = [[NSLocale currentLocale] displayNameForKey:NSLocaleLanguageCode value:_languageCode];
    [cell.textLabel setText:_language];
    
    if ([[[UserPrefsHelper shared] getCurrentLanguageCode] isEqualToString:_languageCode]) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return cell;
}

- (id)setupInitialCell:(UITableViewCell *)_cell reuseIdentifier:(NSString *)_reuseIdentifier {
    if (_cell == nil) {
        _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:_reuseIdentifier];
        [_cell setFrameWidth:self.tableView.frame.size.width];
        [_cell.contentView setFrameWidth:self.tableView.frame.size.width];
        [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [_cell.textLabel setFont:[UIFont fontWithName:@"Helvetica" size:12.0f]];
        [_cell.textLabel setTextColor:[UIColor blackColor]];
        
        [_cell.detailTextLabel setFont:[UIFont fontWithName:@"Helvetica" size:12.0f]];
        [_cell.detailTextLabel setTextColor:[UIColor lightGrayColor]];
    }
    
    return _cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *_language = [self.availableLanguages objectAtIndex:indexPath.row];
    if (![[[UserPrefsHelper shared] getCurrentLanguageCode] isEqualToString:_language]) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:_language, nil]
                                                  forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.tableView reloadData];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kChangeLanguageNotificationKey object:nil];
    }
}

#pragma mark - Actions

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnBack:
        {
            [self dismissViewAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    [self setupNavBar];
}

@end
