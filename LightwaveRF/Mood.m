//
//  Mood.m
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 21/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import "Mood.h"
#import "Zone.h"

@interface Mood()

@end

@implementation Mood

@synthesize moodID = _moodID;
@synthesize listingPosition = _listingPosition;
@synthesize name = _name;
@synthesize zone = _zone;
@synthesize allOff = _allOff;
@synthesize paired = _paired;
@synthesize zoneID = _zoneID;

-(int) zoneID
{
    if (!self.zone)
    {
        return -1;
    }
    else
    {
        return self.zone.zoneID;
    }
}

-(id) initWithID:(int)moodID listingPosition:(int)listingPosition name:(NSString *)name zone:(Zone *)zone allOff:(BOOL)allOff
{
    if (self = [super init])
    {
        _moodID = moodID;
        _listingPosition = listingPosition;
        _name = name;
        _zone = zone;
        _allOff = allOff;
        if (moodID==4)
            _allOff = YES;
        self.paired = YES;
    }
    return self;
}

-(id) initWithID:(int)moodID listingPosition:(int)listingPosition name:(NSString *)name
{
    return [self initWithID:moodID listingPosition:listingPosition name:name zone:nil allOff:NO];
}

-(id) initWithID:(int)moodID name:(NSString *)name zone:(Zone *)zone
{
    return [self initWithID:moodID listingPosition:0 name:name zone:zone allOff:NO];
}

-(id) initAllOff:(Zone *)zone
{
    return [self initWithID:4 listingPosition:0 name:NSLocalizedString(@"all_off", @"") zone:zone allOff:YES];
}

-(BOOL) isAllOff
{
    return self.allOff;
}

-(BOOL) isPaired
{
    return self.paired;
}

-(NSDictionary *) toDictionary
{
    NSDictionary *moodDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    self.name, @"name",
                                    [NSNumber numberWithInt:self.moodID] ,@"id",
                                    [NSNumber numberWithInt:self.listingPosition],@"listingPosition",
                                    [NSNumber numberWithInt:self.allOff], @"allOff",
                                    [NSNumber numberWithInt:self.paired], @"paired",
                                    nil];
    return moodDictionary;
}

-(NSData *) toJSON
{
    NSError *error = nil;
    NSDictionary *jsonMood = self.toDictionary;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonMood options:NSJSONWritingPrettyPrinted error:&error];
    if (!error)
    {
        //DebugLog([NSString stringWithFormat:@"JSON mood data:\n%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]);
        return jsonData;
    }
    else
    {
        return nil;
    }
}

-(NSString *) toString
{
    return [NSString stringWithFormat:@"%@ id:%d zoneID:%d allOff:%d paired:%d", self.name, self.moodID + 1, self.zoneID + 1, self.allOff, self.paired];
}

@end
