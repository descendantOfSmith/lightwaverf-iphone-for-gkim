//
//  UILabel+MFAutoresizeLabel.h
//  Iftach Orr
//
//  Created by admin on 7/4/12.
//  Copyright (c) 2012 Iftach Orr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UILabel (MFAutoresizeLabel)

-(void) setAutoresizedText:(NSString *) text;

@end