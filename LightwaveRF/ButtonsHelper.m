//
//  ButtonsHelper.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ButtonsHelper.h"

@implementation ButtonsHelper

+ (UIImage *)getImageForType:(ButtonIconTypes)tmpType state:(ButtonIconStates)tmpState {
    
    UIImage *tmpImage = nil;
    
    switch (tmpType) {
            
        case kButtonGreenBacking:
        {
            if (tmpState == kButtonStateLo) {
                tmpImage = [UIImage imageNamed:@"btn-green.png"];
                
            } else if (tmpState == kButtonStateHi) {
                tmpImage = [UIImage imageNamed:@"btn-green-hi.png"];
            } 
        }
            break;
            
        case kButtonGreyBacking:
        {
            if (tmpState == kButtonStateLo) {
                tmpImage = [UIImage imageNamed:@"btn-grey.png"];
                
            } else if (tmpState == kButtonStateHi) {
                tmpImage = [UIImage imageNamed:@"btn-grey-hi.png"];
            }
        }
            break;
          
        case kButtonGreenPSwitch:
        case kButtonGreenOSwitch:
        {
            if (tmpState == kButtonStateLo) {
                tmpImage = [UIImage imageNamed:@"btn_green_off.png"];
                
            } else if (tmpState == kButtonStateHi) {
                
                if (tmpType == kButtonGreenOSwitch)
                    tmpImage = [UIImage imageNamed:@"btn_green_orange_on.png"];
                else if (tmpType == kButtonGreenPSwitch)
                    tmpImage = [UIImage imageNamed:@"btn_green_blue_on.png"];
            }
        }
            break;
            
        case kButtonGreySwitch:
        {
            if (tmpState == kButtonStateLo) {
                tmpImage = [UIImage imageNamed:@"btn_grey_off.png"];
                
            } else if (tmpState == kButtonStateHi) {
                tmpImage = [UIImage imageNamed:@"btn_grey_orange_on.png"];
            }
        }
            break;
            
        case kButtonRadio:
        {
            if (tmpState == kButtonStateLo) {
                tmpImage = [UIImage imageNamed:@"radio.png"];
                
            } else if (tmpState == kButtonStateHi) {
                tmpImage = [UIImage imageNamed:@"radio-on.png"];
            }
        }
            break;
            
        case kButtonLock:
        {
            if (tmpState == kButtonStateLo) {
                tmpImage = [UIImage imageNamed:@"lock_off.png"];
                
            } else if (tmpState == kButtonStateHi) {
                tmpImage = [UIImage imageNamed:@"lock_off.png"];
            }
        }
            break;
            
        case kButtonArrowLeft:
        {
            if (tmpState == kButtonStateLo) {
                tmpImage = [UIImage imageNamed:@"left-green-arrow.png"];
                
            } else if (tmpState == kButtonStateHi) {
                tmpImage = [UIImage imageNamed:@"left-green-arrow.png"];
            }
        }
            break;
            
        case kButtonArrowRight:
        {
            if (tmpState == kButtonStateLo) {
                tmpImage = [UIImage imageNamed:@"right-green-arrow.png"];
                
            } else if (tmpState == kButtonStateHi) {
                tmpImage = [UIImage imageNamed:@"right-green-arrow.png"];
            }
        }
            break;

        default:
            break;
    }
    
    if (tmpImage) {
        switch (tmpType) {
            case kButtonGreenBacking:
            case kButtonGreyBacking:
            {
                if ([AppStyleHelper isMegaMan]) {
                    if (tmpType == kButtonGreenBacking) {
                        return [tmpImage resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                                                      10.0f,
                                                                                      7.0f,
                                                                                      10.0f,
                                                                                      7.0f)];
                    } else if (tmpType == kButtonGreyBacking) {
                        return [tmpImage resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                                                      19.0f,
                                                                                      10.0f,
                                                                                      19.0f,
                                                                                      10.0f)];
                    }
                }else {
                    return [tmpImage resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                                                  10.0f,
                                                                                  7.0f,
                                                                                  10.0f,
                                                                                  7.0f)];
                }
            }
                break;
                
            case kButtonGreenPSwitch:
            case kButtonGreenOSwitch:
            case kButtonGreySwitch:
            {
                return [tmpImage resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                                              18.0f,
                                                                              22.0f,
                                                                              18.0f,
                                                                              22.0f)];
            }
                break;
                
            case kButtonRadio:
            {
                return [tmpImage resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                                              [AppStyleHelper isMegaMan] ? 16.0f : 24.0f,
                                                                              [AppStyleHelper isMegaMan] ? 15.0f : 24.0f,
                                                                              [AppStyleHelper isMegaMan] ? 16.0f : 24.0f,
                                                                              0.0f)];
            }
                break;
                
            case kButtonLock:
            {
                return [tmpImage resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                                              0.0f,
                                                                              19.0f,
                                                                              0.0f,
                                                                              3.0f)];
            }
                break;
                
            case kButtonArrowLeft:
            {
                return [tmpImage resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                                              8.0f,
                                                                              10.0f,
                                                                              8.0f,
                                                                              0.0f)];
            }
                break;
                
            case kButtonArrowRight:
            {
                return [tmpImage resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                                              8.0f,
                                                                              0.0f,
                                                                              8.0f,
                                                                              10.0f)];
            }
                break;
                
            default:
                break;
        }
    }

    return tmpImage;
}

+ (void)setTextPositionForButton:(UIButton *)button forType:(ButtonIconTypes)tmpType {
    switch (tmpType) {
             
        case kButtonGreenBacking:   
        case kButtonGreyBacking:
        {
            [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
            
            // Anchor to a Corner
            //[button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            //[button setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
        }
            break;
            
        case kButtonGreenPSwitch:
        case kButtonGreenOSwitch:
        case kButtonGreySwitch:
        {
            [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
            [button setTitleEdgeInsets:UIEdgeInsetsMake(-5.0f, 0.0f, 0.0f, 0.0f)];
        }
            break;
            
        case kButtonRadio:
        {
            [button setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, [AppStyleHelper isMegaMan] ? 9.0f : 28.0f, 0.0f, 0.0f)];
        }
            break;
            
        case kButtonLock:
        {
            [button setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 23.0f, 0.0f, 0.0f)];
        }
            break;
            
        case kButtonArrowLeft:
        {
            [button setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 12.0f, 0.0f, 0.0f)];
        }
            break;
            
        case kButtonArrowRight:
        {
            [button setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 12.0f)];
        }
            break;
            
        default:
            [button setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
            break;
    }
}

+ (float)getIdealHeightForType:(ButtonIconTypes)tmpType {
    switch (tmpType) {
            
        case kButtonGreenBacking:     
        case kButtonGreyBacking:
        {
            return 35.0f;
        }
            break;
            
        case kButtonGreenPSwitch:
        case kButtonGreenOSwitch:
        case kButtonGreySwitch:
        {
            return 38.0f;
        }
            break;
            
        case kButtonRadio:
        {
            return 25.0f;
        }
            break;
            
        case kButtonLock:
        {
            return 34.0f;
        }
            break;
            
        case kButtonArrowLeft:
        case kButtonArrowRight:
        {
            return 18.0f;
        }
            break;
            
        default:
            break;
    }
    
    return 0.0f;
}

+ (UIButton *)getButtonWithText:(NSString *)tmpText
                           type:(ButtonIconTypes)tmpType
                            tag:(ButtonTypes)tmpTag
                          width:(float)tmpWidth
                         height:(float)tmpHeight
                         target:(id)tmpTarget
                       selector:(SEL)tmpSelector {
    
    return [ButtonsHelper getButtonWithText:tmpText
                                       type:tmpType
                                        tag:tmpTag
                                  isDynamic:NO
                                      width:tmpWidth
                               widthPadding:0.0f
                                     height:tmpHeight
                                     target:tmpTarget
                                   selector:tmpSelector];
}

+ (UIButton *)getButtonWithDynamicWidthText:(NSString *)tmpText
                                       type:(ButtonIconTypes)tmpType
                                        tag:(ButtonTypes)tmpTag
                               widthPadding:(float)tmpWidthPadding
                                     height:(float)tmpHeight
                                     target:(id)tmpTarget
                                   selector:(SEL)tmpSelector {
    
    return [ButtonsHelper getButtonWithText:tmpText
                                       type:tmpType
                                        tag:tmpTag
                                  isDynamic:YES
                                      width:0.0f
                               widthPadding:tmpWidthPadding
                                     height:tmpHeight
                                     target:tmpTarget
                                   selector:tmpSelector];
}

+ (UIButton *)getButtonWithText:(NSString *)tmpText
                           type:(ButtonIconTypes)tmpType
                            tag:(ButtonTypes)tmpTag
                      isDynamic:(BOOL)_isDynamic
                          width:(float)tmpWidth
                   widthPadding:(float)tmpWidthPadding
                         height:(float)tmpHeight
                         target:(id)tmpTarget
                       selector:(SEL)tmpSelector {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0.0f, 0.0f, tmpWidth, tmpHeight)];
    [button setBackgroundImage:[ButtonsHelper getImageForType:tmpType state:kButtonStateLo] forState:UIControlStateNormal];
    [button setBackgroundImage:[ButtonsHelper getImageForType:tmpType state:kButtonStateHi] forState:UIControlStateHighlighted];
    [button setBackgroundImage:[ButtonsHelper getImageForType:tmpType state:kButtonStateHi] forState:UIControlStateSelected];
    [button setTitle:tmpText forState:UIControlStateNormal];
    
    switch (tmpType) {
            
        case kButtonGreenBacking:
        case kButtonGreyBacking:
        {
            if ([AppStyleHelper isMegaMan]) {
                [button setTitleColor:[UIColor colorWithDivisionOfRed:69 green:69 blue:69 alpha:1] forState:UIControlStateNormal];
                [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
            } else {
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
            }
        }
            break;
            
        case kButtonGreenPSwitch:
        case kButtonGreenOSwitch:
        case kButtonGreySwitch:
        {
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0f]];
        }
            break;
            
        case kButtonRadio:
        {
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
        }
            break;
            
        case kButtonLock:
        {
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
        }
            break;
            
        case kButtonArrowLeft:
        case kButtonArrowRight:
        {
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        }
            break;
            
        default:
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
            break;
    }
    
    // Offset Title Label
    [ButtonsHelper setTextPositionForButton:button forType:tmpType];
    
    // Dynamic Width
    if (_isDynamic) {
        CGSize size = [tmpText
                       sizeWithFont:button.titleLabel.font
                       constrainedToSize:CGSizeMake(CGFLOAT_MAX, button.titleLabel.frame.size.height)
                       lineBreakMode:NSLineBreakByWordWrapping];
        [button setFrameWidth:size.width + tmpWidthPadding];
    }

    [button setTag:tmpTag];
    [button addTarget:tmpTarget action:tmpSelector forControlEvents:UIControlEventTouchUpInside];
    return button;
}

@end
