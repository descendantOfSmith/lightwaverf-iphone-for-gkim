//
//  LoginVC.m
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginVC.h"
#import "LWErrorView.h"
#import "XMLConfig.h"
#import "Home.h"
#import <QuartzCore/QuartzCore.h>
#import "HomeVC.h"
#import "Toast+UIView.h"
#import "NSString+Utilities.h"
#import "DB.h"
#import "Account.h"
#import "WFLHelper.h"
#import "ButtonsHelper.h"
#import "WebVC.h"

@interface LoginVC ()
@property (strong, nonatomic) UIButton *nw_btn;
@end

#define kTestAccountUsername    @"demo"
#define kTestAccountPassword    @"0000"

@implementation LoginVC

@synthesize login_btn, nw_btn;
@synthesize email_txt;
@synthesize password_txt;
@synthesize moviePlayer;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    
    if ([AppStyleHelper isNexa] || [AppStyleHelper isKlikaan] || [AppStyleHelper isMegaMan])
        [[UserPrefsHelper shared] setVideoShown:YES];
    
    NSString *email = [[UserPrefsHelper shared] getUserEmail];
    if (email!=nil)
        email_txt.text = email;
    
    NSString *pwd = [[UserPrefsHelper shared] getUserPassword];
    if (pwd!=nil)
        password_txt.text = pwd;
    
    videoShown = [[UserPrefsHelper shared] getVideoShown];
    //videoShown = NO;
    
    if (videoShown){
        //Hide the video button and new label
        [_playvideo_btn setHidden:YES];
        [_nw_lbl setHidden:YES];
    }else{
        login_frm = self.login_btn.frame;
        self.login_btn.frame = self.nw_btn.frame;
        [self.nw_btn setHidden:YES];
    }
    // Do any additional setup after loading the view, typically from a nib.
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    email_txt.leftView = paddingView;
    email_txt.leftViewMode = UITextFieldViewModeAlways;
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    password_txt.leftView = paddingView2;
    password_txt.leftViewMode = UITextFieldViewModeAlways;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
    gestureRecognizer.numberOfTapsRequired = 1;
    gestureRecognizer.enabled = YES;
    gestureRecognizer.cancelsTouchesInView = NO;
    gestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:gestureRecognizer];
 
#if TARGET_IPHONE_SIMULATOR
    
    UIButton *_button = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _button.backgroundColor = [UIColor whiteColor];
    _button.layer.borderColor = [UIColor blackColor].CGColor;
    _button.layer.borderWidth = 0.5f;
    _button.layer.cornerRadius = 10.0f;
    [_button setFrame:CGRectMake(0, 50, 100, 25)];
    [_button setTitle:@"Dan" forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(danPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_button];
    
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _button.backgroundColor = [UIColor whiteColor];
    _button.layer.borderColor = [UIColor blackColor].CGColor;
    _button.layer.borderWidth = 0.5f;
    _button.layer.cornerRadius = 10.0f;
    [_button setFrame:CGRectMake(200, 50, 100, 25)];
    [_button setTitle:@"Nik" forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(nikPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_button];
    
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _button.backgroundColor = [UIColor whiteColor];
    _button.layer.borderColor = [UIColor blackColor].CGColor;
    _button.layer.borderWidth = 0.5f;
    _button.layer.cornerRadius = 10.0f;
    [_button setFrame:CGRectMake(0, 80, 100, 25)];
    [_button setTitle:@"Demo" forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(demoPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_button];
    
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _button.backgroundColor = [UIColor whiteColor];
    _button.layer.borderColor = [UIColor blackColor].CGColor;
    _button.layer.borderWidth = 0.5f;
    _button.layer.cornerRadius = 10.0f;
    [_button setFrame:CGRectMake(200, 80, 100, 25)];
    [_button setTitle:@"JSJS" forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(jsjsPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_button];

    
    email_txt.text = @"nik@catalystpics.co.uk";
    password_txt.text = @"4085";
    
    
    email_txt.text = @"info@klikaanklikuit.nl";
    password_txt.text = @"6600";
    
    //email_txt.text = @"gkoene@gkweb.eu";
    //password_txt.text = @"7542";
    
    
    //email_txt.text = @"Jeremyhutchins@btinternet.com";
    //password_txt.text = @"1991";
    
    
    
    //email_txt.text = @"rhlangdon@email.com";
    //password_txt.text = @"1807";
    
    
    //email_txt.text = @"Hayleyatoakview@gmail.com";
    //password_txt.text = @"6007";
    
    
    //email_txt.text = @"lightwaverf@dawes.net";
    //password_txt.text = @"5050";
    
    
    //email_txt.text = @"info@klikaanklikuit.nl";
    //password_txt.text = @"6600";
    
    
    
    //email_txt.text = @"elaine@malcan.co.uk";
    //password_txt.text = @"2308";
    
    
    

    //email_txt.text = @"jo@kim.com";
    //password_txt.text = @"1111";
    
    
    //email_txt.text = @"john@work.com";
    //email_txt.text = @"lwrf@demo.com";
    //email_txt.text = @"dan5678@syncinteractive.co.uk";
    //email_txt.text = @"jsjs@office.com";
    //password_txt.text = @"2434";
    
    
    
    
    //email_txt.text = @"rpanman@hetnet.nl";
    //password_txt.text = @"1122";

    
    
    //email_txt.text = @"jim@crawley.com";
    //password_txt.text = @"1234";
    
    
    
    //email_txt.text = @"jamesakimberley@gmail.com";
    //password_txt.text = @"2811";
    
    
    
    
//    email_txt.text = @"e.c.yntema@ziggo.nl";
//    password_txt.text = @"3830";
    
    
    
//    gcjverzee@home.nl - 6293
//    ari.en.wil@upcmail.nl - 0706
//    e.c.yntema@ziggo.nl - 3830
    
    
#endif
    
}

- (IBAction)demoPressed:(id)sender {
    email_txt.text = kTestAccountUsername;
    password_txt.text = kTestAccountPassword;
}

- (IBAction)jsjsPressed:(id)sender {
    email_txt.text = @"jsjs@office.com";
    password_txt.text = @"2434";
}

- (IBAction)danPressed:(id)sender {
    email_txt.text = @"test@syncinteractive.co.uk";
    password_txt.text = @"2434";
}

- (IBAction)nikPressed:(id)sender {
    email_txt.text = @"nik@catalystpics.co.uk";
    password_txt.text = @"4085";
}

- (void)setupUI {
    [self setupLocalisedStrings];
    [self setupButtons];

    if ([AppStyleHelper isMegaMan])
        [self setupMegaMan];
}

- (void)setupMegaMan {
    [self.login_btn setFrameHeight:39.0f];
    [self.nw_btn setFrameHeight:39.0f];
    
    float _adjust = 0.0f;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        _adjust = 0.0f;
    else
        _adjust = 15.0f;

    [self.email_txt setFrameY:self.email_txt.frame.origin.y + 60.0f + _adjust];
    [self.password_txt alignToBottomOfView:self.email_txt padding:0.0f matchHorizontal:YES];
    [self.login_btn alignToBottomOfView:self.password_txt padding:10.0f matchHorizontal:NO];
    
    float _padding = 10.0f + _adjust;
    [self.logoImageView setFrameY:_padding];
    [self.logoImageView setFrameHeight:(self.email_txt.frame.origin.y - _padding) - 10.0f];
}

- (void)setupLocalisedStrings {
    [self.nw_lbl setText:NSLocalizedString(@"new_to_lightwave_watch", @"new_to_lightwave_watch")];
    [self.email_txt setPlaceholder:NSLocalizedString(@"email_address", @"email_address")];
    [self.password_txt setPlaceholder:NSLocalizedString(@"pin", @"pin")];
}

- (void)setupButtons {
    
    if ([AppStyleHelper isMegaMan])  {
        self.nw_btn = [ButtonsHelper getButtonWithText:NSLocalizedString(@"new_to_lightwave", @"new_to_lightwave")
                                                  type:kButtonGreyBacking
                                                   tag:kButtonNew
                                                 width:280
                                                height:43
                                                target:self
                                              selector:@selector(newPressed:)];
        [self.nw_btn setFrameX:20];
        [self.nw_btn setFrameY:345];
        [self.view addSubview:self.nw_btn];
    } else {
        self.nw_btn = [ButtonsHelper getButtonWithText:NSLocalizedString(@"new_to_lightwave", @"new_to_lightwave")
                                                  type:kButtonGreenBacking
                                                   tag:kButtonNew
                                                 width:280
                                                height:43
                                                target:self
                                              selector:@selector(newPressed:)];
        [self.nw_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.nw_btn setFrameX:20];
        [self.nw_btn setFrameY:345];
        [self.view addSubview:self.nw_btn];
    }
    
    /*
    if ([AppStyleHelper isLightWaveRF] ||
        [AppStyleHelper isFSLDemo] ||
        [AppStyleHelper isCOCO]) {

        UIButton *_button = nil;
        _button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"find_installer", @"find_installer")
                                              type:kButtonGreenBacking
                                               tag:kBtnFindInstaller
                                             width:280
                                            height:43
                                            target:self
                                          selector:@selector(findPressed:)];
        [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_button setFrameX:20];
        [_button setFrameY:294];
        [self.view addSubview:_button];
    }
    */
    
    if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO]) {
        self.login_btn = [ButtonsHelper getButtonWithText:NSLocalizedString(@"login", @"Login")
                                                     type:kButtonGreyBacking
                                                      tag:kBtnLogin
                                                    width:280
                                                   height:45
                                                   target:self
                                                 selector:@selector(loginPressed:)];
    } else {
        self.login_btn = [ButtonsHelper getButtonWithText:NSLocalizedString(@"login", @"Login")
                                                     type:kButtonGreenBacking
                                                      tag:kBtnLogin
                                                    width:280
                                                   height:45
                                                   target:self
                                                 selector:@selector(loginPressed:)];
        [self.login_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    [self.login_btn setFrameX:20];
    [self.login_btn setFrameY:204];
    [self.view addSubview:self.login_btn];
    
    if ([AppStyleHelper isNexa] ||
        [AppStyleHelper isKlikaan]) {
        
        UIButton *_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button setFrame:CGRectMake(0, 0, 280, 43)];
        [_button addTarget:self action:@selector(forgottenPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_button setTag:kBtnForgottenPassword];
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
            NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"forgotten_password", @"forgotten_password")];
            
            [commentString addAttribute:NSUnderlineStyleAttributeName
                                  value:[NSNumber numberWithInteger:NSUnderlineStyleSingle]
                                  range:NSMakeRange(0, [commentString length])];
            
            [commentString addAttribute:NSForegroundColorAttributeName
                                  value:[UIColor whiteColor]
                                  range:NSMakeRange(0, commentString.length)];
            
            [_button setAttributedTitle:commentString forState:UIControlStateNormal];
        } else {
            [_button setTitle:NSLocalizedString(@"forgotten_password", @"forgotten_password") forState:UIControlStateNormal];
            [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        
        [_button alignToBottomOfView:self.login_btn padding:15.0f matchHorizontal:YES];
        [self.view addSubview:_button];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)loginPressed:(id)sender {
    [self hideKeyBoard:nil];
    
    // Set credentials to lowercase
    [self.email_txt setText:[self.email_txt.text lowercaseString]];
    [self.password_txt setText:[self.password_txt.text lowercaseString]];
    
    if (!_playvideo_btn.hidden){
        [UIView beginAnimations:@"ToggleViews" context:nil];
        [UIView setAnimationDuration:1.0];

        self.login_btn.frame = login_frm;
        [_playvideo_btn setHidden:YES];
        [_nw_lbl setHidden:YES];
        [self.nw_btn setHidden:NO];
        
        [UIView commitAnimations];
        return;
    }
    
    NSString *str = email_txt.text;
    
    if ([str isEqualToString:@""]){
        [self showError:NSLocalizedString(@"login_error1", @"Please enter your email address and password")];
        return;
    }
    
    str = password_txt.text;
    
    if ([str isEqualToString:@"" ]){
        [self showError:NSLocalizedString(@"login_error1", @"Please enter your email address and password")];
        return;
    }
    
    Home *home = [Home shared];
    NSString *email = [[UserPrefsHelper shared] getUserEmail];
    NSString *password = [[UserPrefsHelper shared] getUserPassword];
    
    if ([kTestAccountUsername isEqualToString:email_txt.text] &&
        [kTestAccountPassword isEqualToString:password_txt.text]) {
                
        /*
         * Backdoor Test Account
         */

        [Account updateCurrentAccount:email_txt.text
                             password:password_txt.text];
        
        [[UserPrefsHelper shared] setDemoAccount:YES];
        [[UserPrefsHelper shared] setUserEmail:email_txt.text];
        [[UserPrefsHelper shared] setUserPassword:password_txt.text];
        [[UserPrefsHelper shared] setUserName:nil];
        [[UserPrefsHelper shared] setUserMac:nil];
        [[UserPrefsHelper shared] setRegistered:YES];
        [[UserPrefsHelper shared] setEnableWFLPopups:YES];
        
        // Continuously check if using WiFi or Remote
        [[WFLHelper shared] checkState];
        
        // Clear DB
        DB *db = [DB shared];
        [db clear];
        
        if (home != nil)
            [home loadAndRefreshWFL:YES];
        [self.view removeFromSuperview];
        
        
    } else if ([email isEqualToString:email_txt.text] && [password isEqualToString:password_txt.text]){
                        
        if (home != nil)
            [home loadAndRefreshWFL:YES];
        [self.view removeFromSuperview];
        
        [Account updateCurrentAccount:email_txt.text
                             password:password_txt.text];
        
        [[UserPrefsHelper shared] setRegistered:YES];
        [[UserPrefsHelper shared] setEnableWFLPopups:YES];
        [[UserPrefsHelper shared] setDemoAccount:NO];
        
        // Continuously check if using WiFi or Remote
        [[WFLHelper shared] checkState];

    }else{
                        
        //Need to see if this account exists
        RemoteSettings *remote = [RemoteSettings shared];
        [remote pullHomeWithCallingClass:[self getClassName:[self class]]
                                     tag:kRequestPullHome
                               withEmail:email_txt.text
                             andPassword:password_txt.text];
        [self showSpinnerWithText:@""];
    }
}

- (IBAction)forgottenPressed:(id)sender {
    
    if ([AppStyleHelper isKlikaan]) {
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[[WebVC alloc] initWithURL:@"http://www.coco-technology.nl/app-help/KAKU/"]];
        [self presentViewController:nc animated:YES completion:^(void){
            
        }];
        
    } else if ([AppStyleHelper isNexa]) {
        NSURL *_url = [NSURL URLWithString:@"mailto:support@nexa.se"];
        if ([[UIApplication sharedApplication] canOpenURL:_url])
            [[UIApplication sharedApplication] openURL:_url];
        else
            DebugLog(@"No Valid Email Address");
    }    
}

- (IBAction)findPressed:(id)sender {
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[[WebVC alloc] initWithURL:@"http://www.lightwaverf.com/find-an-installer"]];
    [self presentViewController:nc animated:YES completion:^(void){
        
    }];
}

- (IBAction)newPressed:(id)sender {
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app switchView:@"Register"];
}

- (IBAction)videoPressed:(id)sender {
    [[UserPrefsHelper shared] setVideoShown:YES];
    
    self.login_btn.frame = login_frm;
    [_playvideo_btn setHidden:YES];
    [_nw_lbl setHidden:YES];
    [self.nw_btn setHidden:NO];

    NSURL *url = [NSURL URLWithString:@"http://catalystpics.co.uk/files/lightwave_mobile.mp4"];
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(exitedFullscreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:moviePlayer];
        
    moviePlayer.controlStyle = MPMovieControlStyleDefault;
    moviePlayer.shouldAutoplay = YES;
    [self.view addSubview:moviePlayer.view];
    [moviePlayer setFullscreen:YES animated:YES];
}

- (void)moviePlayBackDidFinish:(NSNotification *)_notification {
    [moviePlayer setFullscreen:NO animated:YES];
}

- (void)exitedFullscreen:(NSNotification*)notification {
    [moviePlayer.view removeFromSuperview];
    self.moviePlayer = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)emailEditBegin:(id)sender {
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
    self.navigationItem.rightBarButtonItem = doneButton;
}

- (void)done{
    self.navigationItem.rightBarButtonItem = nil;
    [email_txt resignFirstResponder];
    [password_txt resignFirstResponder];
}

#pragma mark - Remote Delegate

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kRemoteClassNameKey];
    //NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
    
    //DebugLog(@"Received Callback....!");
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
        {
            /*************************
             * Error
             *************************/
            NSError *error = [_dict objectForKey:kRemoteErrorKey];
            //NSString *message = NSLocalizedString(@"login_error3", @"Please create a new account");
            //NSString *str = [NSString stringWithFormat:@"%@\n%@", message, [error localizedDescription]];
            [self showError:[error localizedDescription]];
        }
        
        else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
        {
            /*************************
             * Success
             *************************/
            NSString *data = [_dict objectForKey:kRemoteResponseKey];
            data = [data stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            //DebugLog(@"data = %@", data);
            
            XMLConfig *xmlConfig = [XMLConfig shared];
            
            if ([data isEqualToString:@"no"]){
                [self showError:NSLocalizedString(@"login_error2", @"We cannot log you in. Please check email address and PIN are correct or setup an account if you are a new customer.")];
                
            } else {
                
                Home *home = [Home shared];

                // Add Account (if it doesn't already exist)
                // Set the current account Id
                // Note this must be called before 'xmlToHome'
                [Account updateCurrentAccount:email_txt.text
                                     password:password_txt.text];
                
                if ([xmlConfig xmlToHome:data]){
                    // Set User Preferences
                    [[UserPrefsHelper shared] setUserEmail:email_txt.text];
                    [[UserPrefsHelper shared] setUserPassword:password_txt.text];
                    [[UserPrefsHelper shared] setRegistered:YES];
                    [[UserPrefsHelper shared] setEnableWFLPopups:YES];
                    [[UserPrefsHelper shared] setDemoAccount:NO];
                    
                    // Continuously check if using WiFi or Remote
                    [[WFLHelper shared] checkState];
                    
                    // Update View
                    [_busy_act stopAnimating];
                    [self.view removeFromSuperview];
                    
                    // Reload the tableview data on HomeVC
                    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                    [app.nav1Controller popToRootViewControllerAnimated:NO];
                    [app.lwController setSelectedIndex:0];
                    
                    for (UIViewController *_c in app.nav1Controller.viewControllers) {
                        if ([_c isKindOfClass:[HomeVC class]]) {
                            HomeVC *_homeVC = (HomeVC *)_c;
                            [_homeVC.zone_tbl reloadData];
                            [_homeVC.view makeToast:NSLocalizedString(@"message3", @"Settings received")];
                            
                            // // Delay 3 seconds
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                                if (_homeVC.view)
                                    [_homeVC.view makeToast:NSLocalizedString(@"help1", @"help1 text") duration:5.0f position:@"bottom"];
                            });
                        }
                    }
                } else {
                    Account *_account = [home getAccountWithEmail:email_txt.text];
                    if (_account) {
                        [home removeAccount:_account];
                        [home saveAccounts];
                    }
                    
                    [self showError:NSLocalizedString(@"login_error2", @"We cannot log you in. Please check email address and PIN are correct or setup an account if you are a new customer.")];
                }
            }
        }
        
        else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
        {
            /*************************
             * Failed
             *************************/
            NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
            [self showAlert:_failed];
        }
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
