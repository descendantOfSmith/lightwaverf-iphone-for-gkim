//
//  ChooseProductOnOff.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 19/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ChooseProductCell.h"

@protocol ChooseProductOnOffCellDelegate;

@interface ChooseProductOnOffCell : ChooseProductCell {
    
}

@property (nonatomic, strong) UIButton *onButton;
@property (nonatomic, strong) UIButton *offButton;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, assign) id <ChooseProductOnOffCellDelegate> delegate;

- (void)setupCellWithTitle:(NSString*)tmpTitle
                    device:(Device *)_device
                  selected:(BOOL)_selected
                      isOn:(BOOL)_isOn
                 indexPath:(NSIndexPath *)_indexPath
                  delegate:(id<ChooseProductOnOffCellDelegate>)_delegate;

- (IBAction)buttonPressed:(id)sender;

@end

@protocol ChooseProductOnOffCellDelegate <NSObject>

@optional

- (void)chooseProductSwitch:(ChooseProductOnOffCell *)_ChooseProductOnOffCell
                  indexPath:(NSIndexPath *)_indexPath
                       isOn:(BOOL)_isOn;

- (void)chooseProductSwitch:(ChooseProductOnOffCell *)_ChooseProductOnOffCell
                  indexPath:(NSIndexPath *)_indexPath
                 percentage:(float)_percentage;

- (void)chooseProductButtonPressed:(ChooseProductOnOffCell *)_ChooseProductOnOffCell
                         indexPath:(NSIndexPath *)_indexPath
                               tag:(NSUInteger)_tag;

@end
