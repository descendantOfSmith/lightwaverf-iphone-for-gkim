//
//  CodeVC.h
//  LightwaveRF
//
//  Created by Nik Lever on 05/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

typedef enum
{
    kCodeVCCodeTextField = 1001
} CodeVCTags;

@interface CodeVC : RootViewController <UIGestureRecognizerDelegate> {

}

@property (strong, nonatomic) IBOutlet UINavigationBar *navigaionBar;
@property (weak, nonatomic) IBOutlet UITextField *code_txt;
@property (weak, nonatomic) IBOutlet UIButton *nocode_btn;
@property (weak, nonatomic) IBOutlet UILabel *description_localised_lbl;
@property (weak, nonatomic) IBOutlet UILabel *code_localised_lbl;
@property (nonatomic, copy) NSString *code;

- (IBAction)backPressed:(id)sender;
- (IBAction)nextPressed:(id)sender;
- (IBAction)nocodePressed:(id)sender;
@end
