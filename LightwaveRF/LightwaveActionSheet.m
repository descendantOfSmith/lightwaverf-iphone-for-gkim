//
//  CustomActionSheet.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "LightwaveActionSheet.h"
#import "DistancePickerView.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface LightwaveActionSheet ()
@property (nonatomic, strong) UIView *dismissView;
@end

@implementation LightwaveActionSheet

@synthesize delegate, dataSource, buttonTitlesArray, cancelTitle, heightOffset, isHidden, showPickerType, dismissView, isTapToDismissDisabled;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate {
    
    return [self initWithPoint:point
                  buttonTitles:tmpButtonTitles
                   cancelTitle:tmpCancelTitle
                    dataSource:tmpDataSource
                      delegate:tmpDelegate
                showPickerType:kCustomActionSheetButtons];
}

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
     showPickerType:(CustomActionSheetPickerTypes)tmpShowPickerType {
    
    self.dataSource = tmpDataSource;
    self.showPickerType = tmpShowPickerType;
    self = [super initWithFrame:CGRectMake(point.x, point.y, SCREEN_WIDTH(), 0.0f)];
    if (self) {
        self.delegate = tmpDelegate;
        self.dataSource = tmpDataSource;
        self.buttonTitlesArray = tmpButtonTitles;
        self.cancelTitle = tmpCancelTitle;
        self.showPickerType = tmpShowPickerType;
        self.heightOffset = lwActionSheetTopPadding;
        self.isHidden = YES;
        self.isTapToDismissDisabled = NO;
        [self setupUI];
    }
    return self;
}

- (void)dealloc  {
    //DebugLog(@"DEALLOC");
}

- (void)setupUI {
    [self setBackgroundColor:[self getColourForType:kCustomActionSheetBackgroundColour]];
    
    // Top Highlight
    UIView *_topHighlightView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, 0.0f)];
    [_topHighlightView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_topHighlightView];
    
    // Set Highlight Height
    [_topHighlightView setFrameHeight:self.heightOffset + 1.0f];
    
    // Apply Highlight
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _topHighlightView.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[self getColourForType:kCustomActionSheetGradientStart] CGColor],
                       (id)[[self getColourForType:kCustomActionSheetGradientEnd] CGColor],
                       nil];
    [_topHighlightView.layer insertSublayer:gradient atIndex:0];
    
    // Create Buttons
    NSUInteger tmpTag = 0;
    for (NSString *tmpTitle in self.buttonTitlesArray) {
        [self addButtonType:kCustomActionSheetButton title:tmpTitle tag:tmpTag];
        tmpTag += 1;
    }
    
    // Create Cancel Button
    self.heightOffset += lwActionSheetButtonCancelPadding;
    [self addButtonType:kCustomActionSheetCancelButton title:self.cancelTitle tag:tmpTag];
    
    self.heightOffset += lwActionSheetBottomPadding;
    [self setFrameHeight:self.heightOffset];
}

- (void)addButtonType:(CustomActionSheetTags)tmpType title:(NSString *)tmpTitle tag:(NSUInteger)tmpTag {
    float tmpButtonPadding = 10.0f;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(tmpButtonPadding, self.heightOffset, self.frame.size.width - (tmpButtonPadding * 2), [self getButtonHeight])];

    switch (tmpType) {
            
        case kCustomActionSheetButton:
        {
            [button setBackgroundImage:[self getButtonImageForState:kCustomActionButtonStatesLo] forState:UIControlStateNormal];
            [button setBackgroundImage:[self getButtonImageForState:kCustomActionButtonStatesHi] forState:UIControlStateHighlighted];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
        }
            break;
            
        case kCustomActionSheetCancelButton:
        {
            [button setBackgroundImage:[self getCancelButtonImageForState:kCustomActionButtonStatesLo] forState:UIControlStateNormal];
            [button setBackgroundImage:[self getCancelButtonImageForState:kCustomActionButtonStatesHi] forState:UIControlStateHighlighted];
            [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
        }
            break;
            
        default:
            break;
    }
    
    [button setTitle:tmpTitle forState:UIControlStateNormal];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [button setTag:tmpTag];
    [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    self.heightOffset += button.frame.size.height;
    self.heightOffset += lwActionSheetButtonPadding;
}

#pragma mark - Tap to dismiss actionsheet

- (void)tapOut:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self];
    if (p.y < 0) {
        [self toggleActionSheetWithDelay:0.0f];
    }
}

- (void)setupDismissViewToggle {
    if (!self.isTapToDismissDisabled) {
        if (!self.dismissView) {
            
            UIView *topView = [self getPresentedFromView];
            
            self.dismissView = [[UIView alloc] initWithFrame:topView.bounds];
            [self.dismissView setBackgroundColor:[UIColor blackColor]];
            [self.dismissView setAlpha:0.5f];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOut:)];
            tap.cancelsTouchesInView = NO;
            [self.dismissView addGestureRecognizer:tap];
            
            [topView insertSubview:self.dismissView belowSubview:self];
        } else {
            
            [self.dismissView removeFromSuperview];
            self.dismissView = nil;
        }
    }
}

#pragma mark -
#pragma mark - Get DataSource

- (UIView *)getPresentedFromView {
    if ([self.dataSource respondsToSelector:@selector(presentedFromView:)])
        return [self.dataSource presentedFromView:self];
    
    return nil;
}

- (UIColor *)getColourForType:(CustomActionSheetStyleColours)_type {
    if ([self.dataSource respondsToSelector:@selector(actionSheetColourTypes:type:)])
        return[self.dataSource actionSheetColourTypes:self type:_type];
    
    return [UIColor lightGrayColor];
}

- (UIImage *)getButtonImageForState:(CustomActionButtonStates)_state {
    if ([self.dataSource respondsToSelector:@selector(actionSheetButtonImage:state:)])
        return [self.dataSource actionSheetButtonImage:self state:_state];
    
    return nil;
}

- (UIImage *)getCancelButtonImageForState:(CustomActionButtonStates)_state {
    if ([self.dataSource respondsToSelector:@selector(actionSheetCancelButtonImage:state:)])
        return [self.dataSource actionSheetCancelButtonImage:self state:_state];
    
    return nil;
}

- (float)getButtonHeight {
    if ([self.dataSource respondsToSelector:@selector(actionSheetButtonHeight:)])
        return[self.dataSource actionSheetButtonHeight:self];
    
    return 0.0f;
}

#pragma mark - Public

- (void)resetPickerValues {
    // Children overide
}

- (void)setTitle:(NSString *)_title forButtonAtIndex:(NSUInteger)_index {
    for (UIView *_view in self.subviews) {
        if ([_view isKindOfClass:[UIButton class]] && _view.tag == _index) {
            UIButton *_button = (UIButton *)_view;
            [_button setTitle:_title forState:UIControlStateNormal];
            break;
        }
    }
}

- (void)toggleActionSheetWithDelay:(float)tmpDelay {
    
    [self setupDismissViewToggle];
    
    [UIView animateWithDuration:0.3
                          delay:tmpDelay
                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         if (self.isHidden)
                             // Show (Move Up)
                             [self setFrameY:self.frame.origin.y - self.frame.size.height];
                         else
                             // Hide (Move Down)
                             [self setFrameY:self.frame.origin.y + self.frame.size.height];
                     }
                     completion:^(BOOL completed2) {
                         if (completed2) {
                             self.isHidden = !self.isHidden;
                             
                             if (self.isHidden) {
                                 if ([self.delegate respondsToSelector:@selector(didHideActionSheet:)])
                                     [self.delegate didHideActionSheet:self];
                             }
                         }
                     }];
}

#pragma mark - Actions

- (IBAction)buttonPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(actionSheetButtonPressed:index:)])
        [self.delegate actionSheetButtonPressed:self index:[sender tag]];
}

@end
