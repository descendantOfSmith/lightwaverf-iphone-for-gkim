//
//  ChooseProductOpenCloseCell.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 19/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ChooseProductOpenCloseCell.h"
#import "ButtonsHelper.h"

@interface ChooseProductOpenCloseCell ()
@property (nonatomic, strong) UIButton *openButton;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIButton *stopButton;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, assign) id <ChooseProductOpenCloseCellDelegate> delegate;
@end

@implementation ChooseProductOpenCloseCell

@synthesize openButton, closeButton, stopButton, indexPath, delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
    }
    return self;
}

- (void)dealloc {
    
}

+ (float)getCellHeight {
    return 44.0f;
}

- (void)setupCellWithTitle:(NSString*)tmpTitle
                    device:(Device *)_device
                  selected:(BOOL)_selected
               isOpenClose:(ConstantsIsOpenClose)_isOpenClose
                 indexPath:(NSIndexPath *)_indexPath
                  delegate:(id<ChooseProductOpenCloseCellDelegate>)_delegate {
    
    self.delegate = _delegate;
    self.indexPath = _indexPath;
    
    // Setup Initial Cell
    [self setupCellWithTitle:tmpTitle device:_device selected:_selected];
    
    /*************************************
     *  Open Button
     *************************************/
    if (!self.openButton) {
        self.openButton = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"open", @"Open") uppercaseString]
                                                    type:kButtonGreenPSwitch
                                                     tag:kBtnOn
                                                   width:45.0f
                                                  height:38.0f
                                                  target:self
                                                selector:@selector(buttonPressed:)];
        [self.openButton setSelected:_isOpenClose == kConstantOpen ? YES : NO];
        [self.containingView addSubview:self.openButton];
    } else
        [self.openButton setSelected:_isOpenClose == kConstantOpen ? YES : NO];
    
    /*************************************
     *  Close Button
     *************************************/
    if (!self.closeButton) {
        self.closeButton = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"close", @"Close") uppercaseString]
                                                     type:kButtonGreenOSwitch
                                                      tag:kBtnOff
                                                    width:47.0f
                                                   height:38.0f
                                                   target:self
                                                 selector:@selector(buttonPressed:)];
        [self.closeButton setSelected:_isOpenClose == kConstantClose ? YES : NO];
        [self.containingView addSubview:self.closeButton];
    } else
        [self.closeButton setSelected:_isOpenClose == kConstantClose ? YES : NO];
    
    /*************************************
     *  Stop Button
     *************************************/
    if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO]) {
        if (!self.stopButton) {
            self.stopButton = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"stop", @"Stop") uppercaseString]
                                                          type:kButtonGreySwitch
                                                           tag:kBtnStop
                                                         width:45.0f
                                                        height:38.0f
                                                        target:self
                                                      selector:@selector(buttonPressed:)];
            [self.stopButton setSelected:_isOpenClose == kConstantStop ? YES : NO];
            [self.containingView addSubview:self.stopButton];
        } else
            [self.stopButton setSelected:_isOpenClose == kConstantStop ? YES : NO];
    }
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    [super positionViewsIsEditing:tmpIsEditing];
    
    // Title Label
    [self.titleLabel setFrameWidth:88.0f];
    
    // Open Button
    [self.openButton setFrameX:(self.containingView.frame.size.width - self.openButton.frame.size.width) - 5.0f];
    [self.openButton setCenterY:self.containingView.center.y];
    
    // Close Button
    [self.closeButton alignToLeftOfView:self.openButton padding:5.0f];
    [self.closeButton setCenterY:self.containingView.center.y];
    
    // Stop Button
    if (self.stopButton) {
        [self.stopButton alignToLeftOfView:self.closeButton padding:5.0f];
        [self.stopButton setCenterY:self.containingView.center.y];
    }
}

- (ConstantsIsOpenClose)getOpenCloseState {
    
    if (self.closeButton.selected)
        return kConstantClose;
    
    else if (self.openButton.selected)
        return kConstantOpen;
    
    else if (self.stopButton.selected)
        return kConstantStop;
    
    else
        return kConstantClose;
}

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnStop:
        case kBtnOn:
        case kBtnOff:
        {
            [self.openButton setSelected:NO];
            [self.closeButton setSelected:NO];
            [self.stopButton setSelected:NO];
            [(UIButton *)sender setSelected:YES];
            
            if ([self.delegate respondsToSelector:@selector(chooseProductOpenClose:indexPath:isOpenClose:)]) {
                [self.delegate chooseProductOpenClose:self indexPath:self.indexPath isOpenClose:[self getOpenCloseState]];
            }
        }
            break;
            
        default:
            break;
    }
}

@end
