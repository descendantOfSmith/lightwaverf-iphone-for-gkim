//
//  ColourLEDsVC.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 29/10/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ColourLEDsVC.h"
#import "WFLHelper.h"
#import "Toast+UIView.h"
#import "ButtonsHelper.h"

@interface ColourLEDsVC ()
@property (nonatomic, strong) Device *device;
@property (nonatomic, strong) Zone *zone;
@property (nonatomic, strong) UIButton *changeCyclingButton;
@end

@implementation ColourLEDsVC

@synthesize device, zone, changeCyclingButton, showChangeCyclingButton;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               device:(Device *)_device
                 zone:(Zone *)_zone
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.device = _device;
        self.zone = _zone;
        self.showChangeCyclingButton = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Creation

- (void)setupUI {
    [self setupNavBar];
    [self setupColourHeading];
    [self setupBackground];
    [self setupColourButtons];
    [self setupChangeCyclingButton];
}

- (void)setupNavBar {
    self.navigationItem.title = self.device.name;
        
    [self setupLeftNavBarButton];
}

- (void)setupLeftNavBarButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *homeButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"back", @"Back")
                                                           type:kButtonArrowLeft
                                                            tag:kBtnBack
                                                          width:50
                                                         height:18
                                                         target:self
                                                       selector:@selector(buttonPressed:)];
        
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;

    } else {
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"back", @"Back")
                                                                          style:UIBarButtonItemStyleBordered
                                                                         target:self
                                                                         action:@selector(buttonPressed:)];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;
    }
}

- (void)setupColourHeading {
    UIView *_containingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.heightOffset, self.scrollView.frame.size.width, 30.0f)];
    [_containingView setBackgroundColor:[UIColor grayColor]];
    [self.scrollView addSubview:_containingView];
    self.heightOffset += _containingView.frame.size.height;
    
    UILabel *label = [[UILabel alloc] initWithFrame:_containingView.bounds];
    label.backgroundColor= [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont boldSystemFontOfSize:13.0];
    [label setAdjustsFontSizeToFitWidth:YES];
    label.minimumFontSize = 9.0f;
    label.text = NSLocalizedString(@"select_a_colour", @"select_a_colour");
    float _padding = 5.0f;
    [label setFrameX:_padding];
    [label setFrameWidth:label.frame.size.width - (_padding * 2)];
    [_containingView addSubview:label];
}

- (void)setupBackground {
    self.heightOffset += 15.0f;
    UIImageView *imageView = [ViewBuilderHelper getImageViewWithFilename:@"LED-bg" atX:0.0f atY:self.heightOffset];
    [imageView setCenterX:self.scrollView.center.x];
    [self.scrollView addSubview:imageView];
}

- (void)setupColourButtons {
    NSUInteger _numColumns = 5;
    NSUInteger _numRows = 4;
    float _buttonWidth = 55.0f;
    float _buttonHeight = 63.0f;
    float _padding = 0.0f;
    float _xStart = 22.0f;
    self.heightOffset += 11.0f;
    
    int _tag = 1;
    for (int _column = 0; _column < _numColumns; _column++) {
        
        for (int _row = 0; _row < _numRows; _row++) {
         
            [self addColourLEDButtonAtX:_xStart + (_column * (_buttonWidth + _padding))
                                      y:self.heightOffset + (_row * (_buttonHeight + _padding))
                                  width:_buttonWidth
                                 height:_buttonHeight
                                    tag:_tag
                               filename:[NSString stringWithFormat:@"led_btns00%02d.png", _tag]
                       selectedFilename:[NSString stringWithFormat:@"led_btns00%02d-hi.png", _tag]]; //  + (_numColumns * _numRows)
            
            _tag += 1;
        }
    }
    
    self.heightOffset += (_numRows * (_buttonHeight + _padding));
}

- (void)setupChangeCyclingButton {
    if (self.showChangeCyclingButton) {
        self.heightOffset += 38.0f;
        self.changeCyclingButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"change_cycling", @"change_cycling")
                                                               type:kButtonGreenBacking
                                                                tag:kBtnChange
                                                              width:260.0f
                                                             height:35.0f
                                                             target:self
                                                           selector:@selector(buttonPressed:)];
        [self.changeCyclingButton setFrameY:self.heightOffset];
        [self.changeCyclingButton setCenterX:self.scrollView.center.x];
        [self.changeCyclingButton.titleLabel setTextColor:[UIColor whiteColor]];
        [self.changeCyclingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.scrollView addSubview:self.changeCyclingButton];
        self.heightOffset += (self.changeCyclingButton.frame.size.height + 0.0f);
    }
}

- (UIButton *)addColourLEDButtonAtX:(float)x
                                  y:(float)y
                              width:(float)width
                             height:(float)height
                                tag:(NSUInteger)tag
                           filename:(NSString *)_filename
                   selectedFilename:(NSString *)_selectedFilename {
    UIButton *_button = nil;
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button setBackgroundImage:[UIImage imageNamed:_filename] forState:UIControlStateNormal];
    [_button setBackgroundImage:[UIImage imageNamed:_selectedFilename] forState:UIControlStateSelected];
    
#if TARGET_IPHONE_SIMULATOR
    
    [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_button setTitle:[NSString stringWithFormat:@"%d", tag] forState:UIControlStateNormal];
    
#endif
    
    [_button setFrameX:x];
    [_button setFrameY:y];
    [_button setFrameWidth:width];
    [_button setFrameHeight:height];
    [_button setTag:tag];
    [_button addTarget:self action:@selector(ledPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:_button];
    return _button;
}

#pragma mark - Private

- (void)unselectAll {
    for (UIView *_tmpView in self.scrollView.subviews) {
        if ([_tmpView isKindOfClass:[UIButton class]]) {
            UIButton *_button = (UIButton *)_tmpView;
            [_button setSelected:NO];
        }
    }
}

#pragma mark - Actions

- (IBAction)ledPressed:(id)sender {
    [self unselectAll];
    
    UIButton *_button = (UIButton *)sender;
    [_button setSelected:YES];
    
    // Set Colour, e.g. !R1D1FCP2 (Sets colour code 2 for Green White)
    [[WFLHelper shared] sendToUDP:YES
                          command:[UDPService getLEDCommandWithZoneID:self.zone.zoneID device:self.device colourCode:_button.tag]
                              tag:kRequestLEDColourChange];
    
    //[self.view makeToast:@""];
}

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnBack:
        {
            [self dismissViewAnimated:YES];
        }
            break;
            
        case kBtnChange:
        {
            
            // Set Auto Cycle within palette: !R1D1FY
            [[WFLHelper shared] sendToUDP:YES
                                  command:[UDPService getLEDCycleCommandWithZoneID:self.zone.zoneID device:self.device]
                                      tag:kRequestLEDAutoCycle];
            
            //[self.view makeToast:@""];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
