//
//  SegmentedDialView.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 15/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "SegmentedDialView.h"
#import <QuartzCore/QuartzCore.h>

@interface SegmentedDialView ()
@property (nonatomic, assign) id <SegmentedDialViewDataSource> dataSource;
@end

@implementation SegmentedDialView

@synthesize dataSource;
@synthesize borderWidth, fillColour, trackColour, progressColour, totalArc;

#define kMinPercentage 0
#define kMaxPercentage 100

- (id)initWithFrame:(CGRect)frame dataSource:(id<SegmentedDialViewDataSource>)tmpDataSource
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataSource = tmpDataSource;
        [self initialize];
        [self setupUI];
    }
    return self;
}

- (void)initialize {
    self.totalArc = 270.0f;
    self.borderWidth = 15.0f;
    self.fillColour = [UIColor clearColor];
    self.trackColour = [UIColor colorWithRed:0.188 green:0.356 blue:0.670 alpha:1.0f];
    self.progressColour = [UIColor whiteColor];
}

- (void)dealloc  {
    //DebugLog(@"DEALLOC");
}

- (void)setupUI {
    [self setBackgroundColor:[UIColor clearColor]];
}

#pragma mark -
#pragma mark - Get DataSource

- (float)getProgress {
    if ([self.dataSource respondsToSelector:@selector(percentageComplete:)]) {
        float tmpPercentage = [self.dataSource percentageComplete:self];
        
        if (tmpPercentage < kMinPercentage)
            return kMinPercentage;
        else if (tmpPercentage > kMaxPercentage)
            return kMaxPercentage;
        else
            return tmpPercentage;
    }
    
    return 0;
}

#pragma mark - Draw Circle Background

- (void)drawRect:(CGRect)rect {
    // Generic Vars
    CGFloat d = rect.size.width - self.borderWidth;
    CGFloat clockWise = 0; // 0 or 1
    CGFloat _startDegree = -225;
    CGFloat _endDegree = 45;
    float _endPercentageDegree = _startDegree + ((self.totalArc / 100) * [self getProgress]);

    // Context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Circle
    CGContextAddEllipseInRect(context, rect);
    CGContextSetFillColor(context, CGColorGetComponents(self.fillColour.CGColor));
    CGContextFillPath(context);
    CGContextSetLineWidth(context, self.borderWidth);
    
    // Dashed Lines
    CGFloat dashes[] = {2,4};
    CGContextSetLineDash(context,
                         0.0,       // How far in the dash pattern begins. e.g. 0.0 = immedately
                         dashes,    // The length of the gaps and lines e.g. 2,4 = 2 painted, 4 un-painted
                         2);        // The number of values in the pattern
    
    //
    // TRACK LINE Border
    //
        
    // Draw
    CGContextSetStrokeColorWithColor(context, self.trackColour.CGColor);
    CGContextAddArc(context, rect.size.width/2,rect.size.height/2, d/2, DEGREES_TO_RADIANS(_startDegree), DEGREES_TO_RADIANS(_endDegree), clockWise);
    CGContextStrokePath(context);
    
    //
    // PROGRESS LINE Border
    //
        
    // Min at 0%
    if (_endPercentageDegree < _startDegree)
        _endPercentageDegree = _startDegree;
    
    // Max at 100%
    if (_endPercentageDegree > _endDegree)
        _endPercentageDegree = _endDegree;
    
    // Draw
    CGContextSetStrokeColorWithColor(context, self.progressColour.CGColor);
    CGContextAddArc(context, rect.size.width/2,rect.size.height/2, d/2, DEGREES_TO_RADIANS(_startDegree), DEGREES_TO_RADIANS(_endPercentageDegree), clockWise);
    CGContextStrokePath(context);
    
    [super drawRect:rect];
}

@end
