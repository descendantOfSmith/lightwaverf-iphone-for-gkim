//
//  ZoneVC.h
//  LightwaveRF
//
//  Created by Nicholas Lever on 09/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Zone.h"
#import "RootViewController.h"
#import "HomeHeatingCell.h"
#import "TempActionSheet.h"
#import "ColourLEDsCellVCCell.h"

typedef enum
{
    kZoneVCAddDeviceAlert = 1001,
    kZoneVCStartPairingAlert = 1002,
    kZoneVCDeleteDeviceAlert = 1003,
    
    kZoneVCDeviceTextField = 2001,
    kZoneVCSaveMoodButton = 3001,
    kZoneVCConnectButton = 4001,
    kZoneVCFullLockButton = 5001,
    kZoneVCLockButton = 6001,
    kZoneVCUnlockButton = 7001,
    kZoneVCDeviceImageView = 8001,
    kZoneVCDeviceLabel = 9001,
    kZoneVCLinkButton = 10001
    
} ZoneVCTags;

/*****************************************
 * ActionSheet Types
 *****************************************/
typedef enum
{
    kZoneVCSetTemp = 0
} ZoneVCActionSheetTags;

/*****************************************
 * Set Temp ActionSheet
 *****************************************/
typedef enum
{
    kZoneVCSetTempSetTemp = 0,
    kZoneVCSetTempCancel
} ZoneVCSetTempActionSheetTags;

@interface ZoneVC : RootViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, HomeHeatingCellDelegate, ColourLEDsCellVCDelegate,
                                        LightwaveActionSheetDataSource, LightwaveActionSheetDelegate>{
    
}

@property (nonatomic, strong) Zone *zone;

-(id)initWithZone:(Zone*)_zone;
-(void)setZone:(Zone *)_zone;

@property (strong, nonatomic) IBOutlet UITableView *devices_tbl;
@property (strong, nonatomic) IBOutlet UILabel *message_lbl;
@property (strong, nonatomic) IBOutlet UITextField *newdevice_txt;
@property (weak, nonatomic) IBOutlet UIButton *wifi_btn;

@end
