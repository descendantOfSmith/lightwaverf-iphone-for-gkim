//
//  HomeVC.h
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "TempActionSheet.h"
#import "HomeFooterView.h"
#import "HomeHeatingCell.h"

typedef enum
{
    kHome = 0,
    kAway
} HomeVCEventIDs;

/*****************************************
 * ActionSheet Types
 *****************************************/
typedef enum
{
    kHomeVCSetTemp = 0
} HomeVCActionSheetTags;

/*****************************************
 * Set Temp ActionSheet
 *****************************************/
typedef enum
{
    kHomeVCSetTempSetTemp = 0,
    kHomeVCSetTempCancel
} HomeVCSetTempActionSheetTags;

typedef enum
{
    kHomeEditZoneNameTextField = 1001,
    kHomeNewZoneNameTextField
} HomeViewTags;

@interface HomeVC : RootViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,
                                        LightwaveActionSheetDataSource, LightwaveActionSheetDelegate,
                                        HomeFooterViewDelegate, HomeHeatingCellDelegate> {
    BOOL adding;

}

@property (strong, nonatomic) IBOutlet UITableView *zone_tbl;
@property (strong, nonatomic) IBOutlet UITextField *newzone_txt;
@property (weak, nonatomic) IBOutlet UIButton *wifi_btn;

- (IBAction)homePressed:(id)sender;
- (IBAction)awayPressed:(id)sender;

@end
