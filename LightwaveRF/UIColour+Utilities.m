//
//  UIColour+Utilities.m
//  TemplateARC
//
//  Created by imac on 14/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIColour+Utilities.h"

@implementation UIColor (Utilities)

/*
 * Returns a UIColor by passing a hex colour value
 * @param hex A hex value
 * @returns a UIColour object
 */
+ (UIColor *)colorWithRGBHex:(UInt32)hex {
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithDivisionOfRed:r
                                     green:g
                                      blue:b
                                     alpha:1.0f];
}

/*
 * Returns a UIColor by scanning the string for a hex number and passing that to +[UIColor colorWithRGBHex:]
 * Skips any leading whitespace and ignores any trailing characters
 * @param stringToConvert A hex colour string
 * @returns a UIColour object
 */
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert {
    if ([stringToConvert length] > 0 && [stringToConvert characterAtIndex:0]) {
        // Account for #rrggbb format (instead of 0xrrggbb or rrggbb)
        stringToConvert = [stringToConvert substringFromIndex:1];
    }
    NSScanner *scanner = [NSScanner scannerWithString:stringToConvert];
    unsigned hexNum;
    if (![scanner scanHexInt:&hexNum]) return nil;
    return [UIColor colorWithRGBHex:hexNum];
}

/*
 * Returns a UIColor by RGB Value divided by 255 (rather than calculating manually)
 * @param r 0 - 255 Red
 * @param g 0 - 255 Green
 * @param b 0 - 255 Blue
 * @param a 0 - 1   Alpha
 * @returns a UIColour object
 */
+ (UIColor *)colorWithDivisionOfRed:(float)r green:(float)g blue:(float)b alpha:(CGFloat)a {
    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:a];
}

/*
 * Returns a random UIColor
 * @returns a random UIColor object
 */
+ (UIColor *)randomColour {
    CGFloat red =  (CGFloat)random()/(CGFloat)RAND_MAX;
    CGFloat blue = (CGFloat)random()/(CGFloat)RAND_MAX;
    CGFloat green = (CGFloat)random()/(CGFloat)RAND_MAX;
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

@end
