//
//  MoreVC.h
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface MoreVC : RootViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>{
    NSArray *options;
}
@property (strong, nonatomic) IBOutlet UITableView *more_tbl;
@property (weak, nonatomic) IBOutlet UINavigationBar *nav_bar;

@end
