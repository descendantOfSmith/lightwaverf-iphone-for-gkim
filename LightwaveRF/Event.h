//
//  Event.h
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 25/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Action;
@class Device;

@interface Event : NSObject{
    int idx;
    NSString *_name;
    NSString *_timerName;
    NSMutableArray *_actions;
    BOOL _createdonWFL;
}

@property (assign, nonatomic) BOOL createdOnWFL;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *timerName;
@property (strong, nonatomic) NSMutableArray *actions;
@property (nonatomic) int eventID;

-(id) initWithName:(NSString *) name actions:(NSMutableArray *) actions;
-(id) initWithName:(NSString *)name andID:(int)idx;
-(Action *) addActionForDevice:(Device *) device setting:(int) setting;
//-(BOOL) includesDevice:(Device *) device;
//-(int) getSettingForDevice:(Device *) device;
-(NSString *) triggerEvent:(NSDate *)_delay;
-(void) createEvent;
-(void) cancelEvent;
-(void) deleteEvent;
-(void) clearActions;
-(void) deleteActionAt:(int) position;
- (void)deleteActions;
//-(void) removeDevice:(Device *) device;
-(void) renameFrom:(NSString *) fromName to:(NSString *) toName;
-(NSDictionary *) toDictionary;
-(NSData *) toJSON;
-(NSString *)toString;
-(NSArray *)toArray;
-(NSString *) sumDelays:(NSString *) delay1 delay2:(NSString *) delay2;
-(NSString *) getCommand;
- (NSMutableArray *)getActionBreakdown;

-(BOOL) addAction:(Action *)action;
-(BOOL) insertAction:(Action *)action atIndex:(int)index;
-(void) removeAction:(Action *)action;
-(BOOL)saveActionsToDB;
-(void)saveToDB:(int)displayID;

@end
