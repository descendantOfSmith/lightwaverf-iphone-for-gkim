//
//  LTimer.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 26/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "LTimer.h"
#import "NSDate+Utilities.h"
#import "WFLHelper.h"
#import "NSDate+Utilities.h"

@interface LTimer ()

@end

@implementation LTimer

@synthesize name, cmd, time, start_date, end_date, days, months, event_name, active, createdOnWFL;

-(id) initWithCommand:(NSString *)_command
{
    if (self = [super init])
    {
        return [self initWithCommand:_command active:YES];
    }
    return self;
}

-(id) initWithCommand:(NSString *)_command active:(BOOL)_isActive
{
    if (self = [super init])
    {
        self.createdOnWFL = NO;
        
        // Validate & Fix any potential bad data
        _command = [_command stringByReplacingOccurrencesOfString:@",," withString:@""];
        
        [self setupWithCommand:_command active:_isActive];
    }
    return self;
}

+ (NSString *)generateTimerName {
    // '20130726154722' should be yyyyMMddHHmmss
    
    return [NSString stringWithFormat:@"%04d%02d%02d%02d%02d%02d",
            [[[NSDate date] yearNumber] intValue],
            [[[NSDate date] monthNumber] intValue],
            [[[NSDate date] dayNumber] intValue],
            [[[NSDate date] hourNumber] intValue],
            [[[NSDate date] minuteNumber] intValue],
            [[[NSDate date] secondNumber] intValue]];
    
    /*
     return [NSString stringWithFormat:@"%02d%02d%02d%02d%02d%02d",
     [[[NSDate date] dayNumber] intValue],
     [[[NSDate date] monthNumber] intValue],
     [[[NSDate date] yearNumber] intValue] - 2000,
     [[[NSDate date] hourNumber] intValue],
     [[[NSDate date] minuteNumber] intValue],
     [[[NSDate date] secondNumber] intValue]];
     */
}

- (void)setupWithCommand:(NSString *)_command active:(BOOL)_isActive {
    
    //!FiP"20130726154722"=!FqP"screen close",T15:50,Dmtwtfss,Mjfmamjjasond
    //!FiP"20130726133017"=!FqP"Reset",T08:00,Dmtwtfxx,Mjfmamjjasond
    //!FiP"20130726112249"=!FqP"Home",T06:00,S29/08/13,Dmtwtfss,Mjfmamjjasond
    
    //DebugLog(@"Setup: %@", _command);
    
    self.days = @"mtwtfss";
    self.months = @"jfmamjjasond";

    // Set Default Start Time to One Hour from now
    NSDate *_date = [NSDate date];
    _date = [_date dateByAddingHours:1 minutes:0];
    self.time = [NSString stringWithFormat:@"%02d:%02d", [[_date hourNumber] intValue], [[_date minuteNumber] intValue]];
    
    // Defaults
    
    self.start_date = [NSString stringWithFormat:@"%02d/%02d/%02d",
                       [[_date dayNumber] intValue],
                       [[_date monthNumber] intValue],
                       [[_date yearNumber] intValue] - 2000];
    
    self.name = [NSString stringWithFormat:@"%d%02d%02d%02d%02d%02d",
                 [[_date yearNumber] intValue],
                 [[_date monthNumber] intValue],
                 [[_date dayNumber] intValue],
                 [[_date hourNumber] intValue],
                 [[_date minuteNumber] intValue],
                 [[_date secondNumber] intValue]];
    
    self.cmd = _command;
    self.active = _isActive;
            
    NSUInteger _pos = [_command rangeOfString:@"FiP"].location;
    
    if (_pos == NSNotFound)
        return;
    
    _pos = [_command rangeOfString:@"\""].location;
    if (_pos == NSNotFound)
        return;
    
    _command = [_command substringFromIndex:_pos+1];
    _pos = [_command rangeOfString:@"\""].location;
    self.name = [_command substringToIndex:_pos];
    _pos = [_command rangeOfString:@"="].location;
    _command = [_command substringFromIndex:_pos+1];
    
    //DebugLog(@"Name: %@", self.name);
    
    self.cmd = _command;
    
    NSArray *_tokens = [_command componentsSeparatedByString:@","];
    NSArray *tmp = nil;

    for (NSString *_str in _tokens) {
        NSString *_type = [_str substringToIndex:1];
        
        if ([_type isEqualToString:@"T"]) {
            self.time = [_str substringFromIndex:1];
            
        } else if ([_type isEqualToString:@"S"]) {
            self.start_date = [_str substringFromIndex:1];

            NSString *sep = @"\\|";
            NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:sep];
            tmp = [self.start_date componentsSeparatedByCharactersInSet:set];
            
            if (tmp.count>1) {
                self.start_date = [tmp objectAtIndex:0];
            }
            
        } else if ([_type isEqualToString:@"E"]) {
            self.end_date = [_str substringFromIndex:1];
            
            NSString *sep = @"\\|";
            NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:sep];
            tmp = [self.end_date componentsSeparatedByCharactersInSet:set];
            
            if (tmp.count>1) {
                self.end_date = [tmp objectAtIndex:0];
            }
            
        } else if ([_type isEqualToString:@"W"] || [_type isEqualToString:@"D"]) {
            self.days = [_str substringFromIndex:1];
            
        } else if ([_type isEqualToString:@"M"]) {
            self.months = [_str substringFromIndex:1];
            
        } else {
            _pos = [_str rangeOfString:@"\""].location;
            NSString *_getCommand = @"";
            if (_pos != NSNotFound) {
                NSUInteger _loc = _pos + 1;
                _getCommand = [_str substringWithRange:NSMakeRange(_loc, (_str.length - 1) - _loc)];
                //_getCommand = [_str substringWithRange:NSMakeRange(_loc, _str.length - 1)]; // <-- This crashes
            
            } else {
                _getCommand = _str;
            }
            
            if (![_getCommand isEqualToString:@""]) {
                if (_getCommand.length > 0) {
                    if ([_getCommand characterAtIndex:0] == '!'){
                        self.cmd = _getCommand;
                        //DebugLog(@"Device = %@", self.cmd);
                    } else {
                        self.event_name = _getCommand;
                        //DebugLog(@"Event = %@", self.event_name);
                    }
                }
            } 
        }
    }
}

- (void)adjustStartDateToTomorrow {
    ///////////////////////////////////////////////////////////////////////////////////////
    // When re-adding timers to the app on login, if the start time is earlier than
    // the current time, adjust the start date to tomorrow
    ///////////////////////////////////////////////////////////////////////////////////////
    
    NSDate *now = [NSDate date];
    int year, month, day, hours=0, minutes=0;
    NSArray *tokens = nil;
    
    if (self.start_date!=nil && [self.start_date isEqualToString:@""])
        self.start_date = nil;
    
    if (self.start_date != nil){
        tokens = [self.start_date componentsSeparatedByString:@"/"];
        
        if (tokens.count < 3)
            return;
        
        year = [[tokens objectAtIndex:2] intValue] + 2000;
        month = [[tokens objectAtIndex:1] intValue];
        day = [[tokens objectAtIndex:0] intValue];
        
        if (day==99){
            if ([[now monthNumber] intValue]==2){
                day=28;
            } else if ([[now monthNumber] intValue]==4 || [[now monthNumber] intValue]==6 || [[now monthNumber] intValue]==9 || [[now monthNumber] intValue]==11){
                day=30;
            } else {
                day=31;
            }
        }
        
    } else {
        year = [[now yearNumber] intValue];
        month = [[now monthNumber] intValue];
        day = [[now dayNumber] intValue];
        hours = [[now hourNumber] intValue];
        minutes = [[now minuteNumber] intValue];
    }
    
    if (self.time!=nil){
        tokens = [self.time componentsSeparatedByString:@":"];
        if (tokens.count < 2)
            return;
        
        hours = [[tokens objectAtIndex:0] intValue];
        minutes = [[tokens objectAtIndex:1] intValue];
    }
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.year   = year;
    dateComponents.month  = month;
    dateComponents.day    = day;
    
    //DebugLog(@"Hours: %d", hours);
    
    /*
    
    if (hours == 96 || hours == 97) {
        
        // Dawn
        dateComponents.hour = 06;
        
    } else if (hours == 98 || hours == 99) {
        
        // Dusk
        dateComponents.hour = 16;
        
    } else {
        dateComponents.hour = hours;
    }
     */
    
    dateComponents.hour = hours;
    dateComponents.minute = minutes;
    dateComponents.second = 0;
    
    NSDate *start = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    //DebugLog(@"Start: %@", start);
    
    NSDate *_startTimeAsToday = [NSDate date];
    _startTimeAsToday = [NSDate combineDate:_startTimeAsToday withTime:start];

    //if ([NSDate isTodaysDateAfter:start]) {
    //if ([NSDate isTodaysDateAfter:_startTimeAsToday]) {
    
    if ([NSDate isTodaysDateAfter:start] &&
        [NSDate isTodaysDateAfter:_startTimeAsToday]) {
        
        //Update start date to tomorrow
        NSDate *tomorrow = [[NSDate date] dateByAddingDays:1];
        NSString *tmp = self.start_date;
        self.start_date = [NSString stringWithFormat:@"%02d/%02d/%02d",
                           [[tomorrow dayNumber] intValue],
                           [[tomorrow monthNumber] intValue],
                           [[tomorrow yearNumber] intValue]-2000];
        
        DebugLog(@"\n\n\n adjustStartDateToTomorrow before:%@ after:%@ \n\n\n", tmp, self.start_date);
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
}

- (Device *)getDevice {
    if (self.cmd == nil || [self.cmd isEqualToString:@""])
        return nil;
    
    Home *home = [Home shared];
    Device *_device = [home getDeviceFromHardwareKey:self.cmd];
    return _device;
}

- (BOOL)isEvent {
    BOOL res = YES;
    
    if (self.event_name == nil || [self.event_name isEqualToString:@""])
        res = NO;
    
    return res;
}

- (NSString *)getName {
    Home *home = [Home shared];
    if ([self isEvent]){
        //DebugLog(@"Event Name: %@", self.event_name);
        Event *event = [home getEventByName:self.event_name];
        if (event==nil){
            return NSLocalizedString(@"event", @"Event");
        }else{
            return event.name;
        }
    }else{
        Device *_device = [home getDeviceFromHardwareKey:self.cmd];
        if (_device==nil){
            return NSLocalizedString(@"device", @"Device");
        }else{
            return _device.name;
        }
    }
}

- (NSString *)getActionString {
    //DebugLog(@"Command: %@", self.cmd);
    Action *_action = [[Action alloc] initWithCommand:self.cmd];
    int _setting = _action.setting;
    //DebugLog(@"Setting: %d", _setting);
    
    if ([_action isOpenClose] ||
        [_action isFSL500W] ||
        [_action isFSL3000W]) {
        
        if (_setting==0) {
            return [NSLocalizedString(@"close", @"close") uppercaseString];
        } else if (_setting==1) {
            return [NSLocalizedString(@"open", @"open") uppercaseString];
        } else {
            return [NSLocalizedString(@"stop", @"stop") uppercaseString];
        }
    } else {
        if ([_action isDimmer] || [_action isColourLEDs]) {
            if (_setting==-2){
                return [NSLocalizedString(@"on", @"on") uppercaseString];
            }else{
                return [NSString stringWithFormat:@"%d%%", (int)((_setting/32.0f) * 100)];
            }
        } else if ([_action isMood] || [_action isAllOff]) {
            if ([[[_action getDevice] getMood] isAllOff]) {
                return [NSLocalizedString(@"off", @"off") uppercaseString];
            }else{
                return [NSLocalizedString(@"start_mood", @"start_mood") uppercaseString];
            }
        } else if (_setting==0) {
            return [NSLocalizedString(@"off", @"off") uppercaseString];
        } else {
            return [NSLocalizedString(@"on", @"on") uppercaseString];
        }
    }
}

- (NSString *)toString {
    NSString *str = @"";
    NSString *action = @"";
    
    if (![self isEvent])
        action = [NSString stringWithFormat:@"%@ ", [self getActionString]];
    
    str = [NSString stringWithFormat:@"%@%@ %@", action, NSLocalizedString(@"at", @"at"), [self getTimeString]];
    
    return str;
}

- (NSString *)getCommand {
    
    [self adjustStartDateToTomorrow];
    
    NSString *_str = [NSString stringWithFormat:@"!FiP\"%@\"=", self.name];
        
    if (self.event_name!=nil && ![self.event_name isEqualToString:@""]){

        _str = [NSString stringWithFormat:@"%@!FqP\"", _str];
        
        if (self.event_name.length > 16){
            _str = [NSString stringWithFormat:@"%@%@\"", _str, [self.event_name substringWithRange:NSMakeRange(0, 15)]];
        } else {
            _str = [NSString stringWithFormat:@"%@%@\"", _str, self.event_name];
        }
                
    }else{
        
        if ([self.cmd characterAtIndex:0] != '!') {
            _str = [NSString stringWithFormat:@"%@!", _str];
        }
        
        if (self.cmd.length > 16){
            _str = [NSString stringWithFormat:@"%@%@", _str, [self.cmd substringWithRange:NSMakeRange(0, 15)]];
        }else{
            _str = [NSString stringWithFormat:@"%@%@", _str, self.cmd];
        }
        
    }
        
    if (self.time!=nil && ![self.time isEqualToString:@""]){
        
        if ([_str hasSuffix:@","])
            _str = [NSString stringWithFormat:@"%@T%@", _str, self.time];
        else
            _str = [NSString stringWithFormat:@"%@,T%@", _str, self.time];
                
    }
    
    if (self.start_date!=nil && ![self.start_date isEqualToString:@""]){
        if ([_str hasSuffix:@","])
            _str = [NSString stringWithFormat:@"%@,S%@", _str, self.start_date];
        else
            _str = [NSString stringWithFormat:@"%@,S%@", _str, self.start_date];
        
    }
    
    if (self.end_date!=nil && ![self.end_date isEqualToString:@""]){
        if ([_str hasSuffix:@","])
            _str = [NSString stringWithFormat:@"%@,E%@", _str, self.end_date];
        else
            _str = [NSString stringWithFormat:@"%@,E%@", _str, self.end_date];
        
    }
    
    if (self.days!=nil && ![self.days isEqualToString:@""]){
        if ([_str hasSuffix:@","])
            _str = [NSString stringWithFormat:@"%@,D%@", _str, self.days];
        else
            _str = [NSString stringWithFormat:@"%@,D%@", _str, self.days];
        
    }
    
    if (self.months!=nil && ![self.months isEqualToString:@""]){
        if ([_str hasSuffix:@","])
            _str = [NSString stringWithFormat:@"%@,M%@", _str, self.months];
        else
            _str = [NSString stringWithFormat:@"%@,M%@", _str, self.months];
        
    }
    
    return _str;
}

- (NSString *)getTimeString {
    NSString *_str = @"";
    NSArray *_tokens = [self.time componentsSeparatedByString:@":"];
    
    if (_tokens.count == 2) {
        int _hours = [[_tokens objectAtIndex:0] intValue];
        int _minutes = [[_tokens objectAtIndex:1] intValue];
        
        //DebugLog(@"Tokens: %@", _tokens);
        //DebugLog(@"Minutes: %d", _minutes);
        //DebugLog(@"Minutes: %d", _minutes * 30);
        
        // The WFL takes a number that is 1/30 of the actual
        // So 1 is interpreted as 30
        // Internally 90mins should be passed as 3
        // 96:03 would be 90 mins before dawn
        // 96:90 would be 2700 mins before dawn
        // But it should be capped at plus or minus 120 mins
        // So max value is + or - 4

        switch(_hours){
			case 96:
				_str = [NSString stringWithFormat:@"%d %@ %@ ", _minutes*30, NSLocalizedString(@"minutes", @"Mins"), NSLocalizedString(@"before_dawn", @"before_dawn")];
				break;
			case 97:
				_str = [NSString stringWithFormat:@"%d %@ %@ ", _minutes*30, NSLocalizedString(@"minutes", @"Mins"), NSLocalizedString(@"after_dawn", @"after_dawn")];
				break;
			case 98:
				_str = [NSString stringWithFormat:@"%d %@ %@ ", _minutes*30, NSLocalizedString(@"minutes", @"Mins"), NSLocalizedString(@"before_dusk", @"before_dusk")];
				break;
			case 99:
				_str = [NSString stringWithFormat:@"%d %@ %@ ", _minutes*30, NSLocalizedString(@"minutes", @"Mins"), NSLocalizedString(@"after_dusk", @"after_dusk")];
				break;
			default:
				_str = self.time;
				break;
        }
    }
    
    //DebugLog(@"TIME: %@", _str);
    
    return _str;
}

// If 'x' then not set for this day
- (NSString *)getDayString {
    /*
    NSString *_str = @"";

    BOOL _isEveryDay = YES;
    for(int i=0; i<7; i++){
        if ([self.days characterAtIndex:i] != 'x') {
            
            NSString *_localisedDayKey = [NSString stringWithFormat:@"day%d", i+1];
            
            if ([_str isEqualToString:@""]) {
                _str = NSLocalizedString(_localisedDayKey, @"Short Day Name");
            } else {
                _str = [NSString stringWithFormat:@"%@, %@", _str, NSLocalizedString(_localisedDayKey, @"Short Day Name")];
            }
        } else
            _isEveryDay = NO;
    }
    
    if (_isEveryDay)
        _str = NSLocalizedString(@"every_day", @"Every Day");
    
    return _str;
     */
    
    if ([self.days isEqualToString:@""])
        return @" ";
    
    NSString *str = @"";
    int count = 0;
    NSString *start = (self.start_date==nil) ? @"" : self.start_date;
    NSString *end = (self.end_date==nil) ? @"" : self.end_date;
    NSMutableArray *allowedDays = [[NSMutableArray alloc] initWithCapacity:7];
    
    if (![start isEqualToString:@""] && ![end isEqualToString:@""]){
        
        for(int i=0; i<7; i++){
            //[allowedDays insertObject:[NSNumber numberWithBool:NO] atIndex:i];
            allowedDays[i]=[NSNumber numberWithBool:NO];
        }
        
        NSArray *tokens = [self.start_date componentsSeparatedByString:@"/"];
        NSDate *startCal = nil;
        NSDate *endCal = nil;
        int year1=0, month1=0, day1=0;
        
        if (tokens.count==3){
            @try {
                
                year1 = [[tokens objectAtIndex:2] intValue] + 2000;
                month1 = [[tokens objectAtIndex:1] intValue] - 1;
                day1 = [[tokens objectAtIndex:0] intValue];
                startCal = [NSDate date];
                
                NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
                dateComponents.year   = year1;
                dateComponents.month  = month1;
                dateComponents.day    = day1;
                dateComponents.hour   = [[startCal hourNumber] intValue];
                dateComponents.minute = [[startCal minuteNumber] intValue];
                dateComponents.second = 0;
                
                startCal = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
            }
            @catch(NSException *e){
                DebugLog(@"parseInt failed for start_date: %@", self.start_date);
            }
        }
        
        tokens = [self.end_date componentsSeparatedByString:@"/"];
        int year2=0, month2=0, day2=0;
        
        if (tokens.count==3){
            @try {
                
                year2 = [[tokens objectAtIndex:2] intValue] + 2000;
                month2 = [[tokens objectAtIndex:1] intValue] - 1;
                day2 = [[tokens objectAtIndex:0] intValue];
                endCal = [NSDate date];
                
                NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
                dateComponents.year   = year2;
                dateComponents.month  = month2;
                dateComponents.day    = day2;
                dateComponents.hour   = [[endCal hourNumber] intValue];
                dateComponents.minute = [[endCal minuteNumber] intValue];
                dateComponents.second = 0;
                
                endCal = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
            }
            @catch(NSException *e){
                DebugLog(@"parseInt failed for end_date: %@", self.end_date);
            }
        }
        
        if (startCal!=nil && endCal!=nil){
            NSDate *tmp = [NSDate date];
            
            NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
            dateComponents.year   = year1;
            dateComponents.month  = month1;
            dateComponents.day    = day1;
            dateComponents.hour   = [[tmp hourNumber] intValue];
            dateComponents.minute = [[tmp minuteNumber] intValue];
            dateComponents.second = 0;
            
            tmp = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];

            int day;

            while ([NSDate secondsBetweenDate:tmp andDate:endCal] > 0) {
                day = [tmp weekday] - 2;
                
                if (day<0)
                    day += 7;
                
                //[allowedDays replaceObjectAtIndex:day withObject:[NSNumber numberWithBool:YES]];
                allowedDays[day]=[NSNumber numberWithBool:YES];
                tmp = [tmp dateByAddingDays:1];
            }
        }
    } else {
        for(int i=0; i<7; i++){
            //[allowedDays replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:YES]];
            allowedDays[i]=[NSNumber numberWithBool:YES];
        }
    }
    
    if (self.days!=nil && ![self.days isEqualToString:@""]){
        for(int i=0; i<7; i++){
            
            if (i<self.days.length && [self.days characterAtIndex:i] != 'x' && [allowedDays[i] boolValue]){
                
                NSString *_localisedDayKey = [NSString stringWithFormat:@"day%d", i+1];
                
                if ([str isEqualToString:@""]){
            
                    str = [NSString stringWithFormat:@"%@", NSLocalizedString(_localisedDayKey, @"Short Day Name")];

                } else {
                    
                    str = [NSString stringWithFormat:@"%@, %@", str, NSLocalizedString(_localisedDayKey, @"Short Day Name")];

                }
                
                count++;
            }
        }
    }
    
    if (count==7)
        str = [NSString stringWithFormat:@"%@", NSLocalizedString(@"every_day", nil)];
    
    if (count==0)
        str = [NSString stringWithFormat:@"%@", NSLocalizedString(@"no_days", nil)];
    
    return str;
    
    
}

// If 'x' then not set for this month
- (NSString *)getMonthsString {
    /*
    NSString *_str = @"";
    
    BOOL _isEveryMonth = YES;
    for(int i=0; i<12; i++){
        if ([self.months characterAtIndex:i] != 'x') {
            
            NSString *_localisedMonthKey = [NSString stringWithFormat:@"month%d", i+1];
            
            if ([_str isEqualToString:@""]) {
                _str = NSLocalizedString(_localisedMonthKey, @"Short Month Name");
            } else {
                _str = [NSString stringWithFormat:@"%@, %@", _str, NSLocalizedString(_localisedMonthKey, @"Short Month Name")];
            }
        } else
            _isEveryMonth = NO;
    }
    
    if (_isEveryMonth)
        _str = NSLocalizedString(@"every_month", @"Every Month");
    
    return _str;
     */
    
    if ([self.months isEqualToString:@""])
        return @" ";
    
    NSString * str = @"";
    int count = 0;
    NSString * start = (self.start_date==nil) ? @"" : self.start_date;
    NSString * end = (self.end_date==nil) ? @"" : self.end_date;
    NSMutableArray *allowedMonths = [[NSMutableArray alloc] initWithCapacity:12];
    
    if (![start isEqualToString:@""] && ![end isEqualToString:@""]){
        for(int i=0; i<12; i++){
            allowedMonths[i]=[NSNumber numberWithBool:NO];
        }
        
        NSArray *tokens = [self.start_date componentsSeparatedByString:@"/"];
        NSDate *startCal = nil;
        NSDate *endCal = nil;
        int year1=0, month1=0, day1=0;
        
        if (tokens.count==3){
            @try {
                
                year1 = [[tokens objectAtIndex:2] intValue] + 2000;
                month1 = [[tokens objectAtIndex:1] intValue] - 1;
                day1 = [[tokens objectAtIndex:0] intValue];
                startCal = [NSDate date];
                
                NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
                dateComponents.year   = year1;
                dateComponents.month  = month1;
                dateComponents.day    = day1;
                dateComponents.hour   = [[startCal hourNumber] intValue];
                dateComponents.minute = [[startCal minuteNumber] intValue];
                dateComponents.second = 0;
                
                startCal = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];

            } @catch(NSException *e){
                DebugLog(@"parseInt failed for start_date: %@", self.start_date);
            }
        }
        
        tokens = [self.end_date componentsSeparatedByString:@"/"];
        int year2=0, month2=0, day2=0;
        
        if (tokens.count==3){
            @try {
                
                year2 = [[tokens objectAtIndex:2] intValue] + 2000;
                month2 = [[tokens objectAtIndex:1] intValue] - 1;
                day2 = [[tokens objectAtIndex:0] intValue];
                endCal = [NSDate date];
                
                NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
                dateComponents.year   = year2;
                dateComponents.month  = month2;
                dateComponents.day    = day2;
                dateComponents.hour   = [[endCal hourNumber] intValue];
                dateComponents.minute = [[endCal minuteNumber] intValue];
                dateComponents.second = 0;
                
                endCal = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
                
            } @catch(NSException *e){
                DebugLog(@"parseInt failed for end_date: %@", self.end_date);
            }
        }
        
        if (startCal!=nil && endCal!=nil){
            NSDate *tmp = [NSDate date];
            
            NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
            dateComponents.year   = year1;
            dateComponents.month  = month1;
            dateComponents.day    = day1;
            dateComponents.hour   = [[tmp hourNumber] intValue];
            dateComponents.minute = [[tmp minuteNumber] intValue];
            dateComponents.second = 0;
            
            tmp = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
            
            int month = month1;
            
            while ([NSDate secondsBetweenDate:tmp andDate:endCal] > 0) {
                //[allowedMonths replaceObjectAtIndex:month withObject:[NSNumber numberWithBool:YES]];
                allowedMonths[month]=[NSNumber numberWithBool:YES];
                tmp = [tmp dateByAddingMonths:1];
                month = [[tmp monthNumber] intValue];
            }
        }
    }else{
        for(int i=0; i<12; i++){
            //[allowedMonths replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:YES]];
            allowedMonths[i]=[NSNumber numberWithBool:YES];
        }
    }
    
    for(int i=0; i<12; i++){
        
        if ([self.months characterAtIndex:i] !='x' && [allowedMonths[i] boolValue]){
            
            NSString *_localisedMonthKey = [NSString stringWithFormat:@"month%d", i+1];
        
            if ([str isEqualToString:@""]){
                
                str = [NSString stringWithFormat:@"%@", NSLocalizedString(_localisedMonthKey, @"Short Month Name")];
                
            } else {
                
                str = [NSString stringWithFormat:@"%@, %@", str, NSLocalizedString(_localisedMonthKey, @"Short Month Name")];
                
            }
            
            count++;
        }
    }
    
    if (count==12)
        str = [NSString stringWithFormat:@"%@", NSLocalizedString(@"every_month", nil)];
    
    if (count==0)
        str = [NSString stringWithFormat:@"%@", NSLocalizedString(@"no_months", nil)];
    
    return str;
}

- (NSString *)getDatesString {
    NSString *_str = @"";
    NSString *_start = self.start_date == nil ? @"" : self.start_date;
    NSString *_end = self.end_date == nil ? @"" : self.end_date;
    
    // Strip out text
    NSArray *tokens = [_start componentsSeparatedByString:@"|"];
    _start = [tokens objectAtIndex:0];
    
    tokens = [_end componentsSeparatedByString:@"|"];
    _end = [tokens objectAtIndex:0];
    
    // Add Localisation
    
    if ([self.days isEqualToString:@""] && [self.months isEqualToString:@""])
        _end = _start;
    
    if (![_start isEqualToString:@""] && ![_end isEqualToString:@""]) {
//        str = String.format("From %s until %s", start, end);
        
        if ([_start isEqualToString:_end]) {
            _str = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"once_only_on", @"Once only on"), _start];
        } else {
            _str = [NSString stringWithFormat:@"%@ %@ %@ %@", NSLocalizedString(@"from", @"From"), _start, NSLocalizedString(@"until", @"until"), _end];
        }

    } else if (![_start isEqualToString:@""]) {
//        str = String.format("From %s", start_date);
        _str = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"from", @"From"), _start, NSLocalizedString(@"to_run_forever", @"to_run_forever")];
        
    } else if (![_end isEqualToString:@""]) {
//        str = String.format("Until %s", end_date);
        _str = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"from_now_until", @"from_now_until"), _end];
        
    } else {
//        str = "From now to run forever";
        _str = NSLocalizedString(@"from_now_to_run_forever", @"from_now_to_run_forever");
    }
    
    return _str;
}

- (NSString *)getEndDateString {    
    return [self getValidDateString:self.end_date];
    
}

- (NSString *)getStartDateString {
    return [self getValidDateString:self.start_date];
}

- (NSString *)getValidDateString:(NSString *)_dateStr {
    // Check _dateStr 'dd' is 99, if so its the last day of the month
    if (_dateStr.length > 0) {
        _dateStr = [_dateStr substringToIndex:8]; // make sure we have dd/mm/yy
        NSArray *_tokens = [_dateStr componentsSeparatedByString:@"/"];
        // Is days set to 99?
        if ([[_tokens objectAtIndex:0] isEqualToString:@"99"]) {
            NSCalendar * cal = [NSCalendar currentCalendar];
            NSDateComponents * comps = [[NSDateComponents alloc] init];
            
            if (_tokens.count >= 3) {
                // Get & Set Month
                [comps setMonth:[[_tokens objectAtIndex:1] intValue]];
                
                // Get Last Day
                NSRange range = [cal rangeOfUnit:NSDayCalendarUnit
                                          inUnit:NSMonthCalendarUnit
                                         forDate:[cal dateFromComponents:comps]];
                
                // Get correct date
                _dateStr = [NSString stringWithFormat:@"%d/%d/%d",
                            range.length,
                            [[_tokens objectAtIndex:1] intValue],
                            [[_tokens objectAtIndex:2] intValue]];
            }
        }
    }
    
    return _dateStr;
}

- (NSString *)getWhen {
    //Each MTW___S @ 6:30pm until 31/12/19
    NSString *str = @"";
    NSString *start = (self.start_date==nil) ? @"" : self.start_date;
    NSString *end = (self.end_date==nil) ? @"" : self.end_date;
    
    if (![start isEqualToString:@""] && ![end isEqualToString:@""]) {
                
        str = [NSString stringWithFormat:@"%@ %@ @ %@ %@ %@ %@ %@",
                NSLocalizedString(@"each", @"Each"),
                [[self.days uppercaseString] stringByReplacingOccurrencesOfString:@"X" withString:@"_"],
               [self getTimeString],
               [NSLocalizedString(@"from", @"from") lowercaseString],
               self.start_date,
               [NSLocalizedString(@"until", @"until") lowercaseString],
               self.end_date];
        
        
    } else if (![start isEqualToString:@""]) {
        
        str = [NSString stringWithFormat:@"%@ %@ @ %@ %@ %@ %@",
               NSLocalizedString(@"each", @"Each"),
               [[self.days uppercaseString] stringByReplacingOccurrencesOfString:@"X" withString:@"_"],
               [self getTimeString],
               [NSLocalizedString(@"from", @"from") lowercaseString],
               self.start_date,
               [NSLocalizedString(@"until_cancelled", @"until_cancelled") lowercaseString]];
        
    } else if (![end isEqualToString:@""]) {
                
        str = [NSString stringWithFormat:@"%@ %@ @ %@ %@ %@",
               NSLocalizedString(@"each", @"Each"),
               [[self.days uppercaseString] stringByReplacingOccurrencesOfString:@"X" withString:@"_"],
               [self getTimeString],
               [NSLocalizedString(@"until", @"until") lowercaseString],
               self.end_date];
        
    } else {
        
        str = [NSString stringWithFormat:@"%@ %@ @ %@ %@",
               NSLocalizedString(@"each", @"Each"),
               [[self.days uppercaseString] stringByReplacingOccurrencesOfString:@"X" withString:@"_"],
               [self getTimeString],
               [NSLocalizedString(@"from_now_until_cancelled", @"from_now_until_cancelled") lowercaseString]];
        
    }
    
    return str; 
}

- (void)deleteTimer
{
    [[WFLHelper shared] sendToUDP:YES
                          command:[ServiceType getDeleteTimerCommand:self]
                              tag:kRequestDeleteTimer];
}

- (NSMutableArray *)toDictionary
{
    NSMutableArray *_timerDetails = [[NSMutableArray alloc] init];
    
    // 1
    if ([self getName] && [self getActionString])
        [_timerDetails addObject:[NSString stringWithFormat:@"%@ %@", [self getName], [self getActionString]]];
    else if ([self getName])
        [_timerDetails addObject:[self getName]];
    else
        [_timerDetails addObject:@""];
    
    // 2
    if ([self getWhen])
        [_timerDetails addObject:[self getWhen]];
    else
        [_timerDetails addObject:@""];
    
    // 3
    if (self.getCommand)
        [_timerDetails addObject:[self getCommand]];
    else
        [_timerDetails addObject:@""];
    
    // 4

    /*
    if ([self getEndDateString] && [self getTimeString])
        [_timerDetails addObject:[NSString stringWithFormat:@"%@ %@", [self getEndDateString], [self getTimeString]]];
    
    else if ([self getEndDateString])
        [_timerDetails addObject:[self getEndDateString]];
    
    else if ([self getTimeString])
        [_timerDetails addObject:[self getTimeString]];
    
    else
        [_timerDetails addObject:@""];
     */

    if ([self getEndDateString] && self.time.length > 0)
        [_timerDetails addObject:[NSString stringWithFormat:@"%@ 23:59", [self getEndDateString]]];
    
    else if ([self getEndDateString])
        [_timerDetails addObject:[NSString stringWithFormat:@"%@ 23:59", [self getEndDateString]]];
    
    else if (self.time.length > 0)
        [_timerDetails addObject:@"Forever 23:59"];
    
    else
        [_timerDetails addObject:@"NO END DATE DEFINED"];

    // 5
    [_timerDetails addObject:self.active ? @"true" : @"false"];
        
    return _timerDetails;
}

- (NSData *)toJSON
{
    NSError *error = nil;
    NSMutableDictionary *jsonDevice = [NSMutableDictionary new];
    [jsonDevice setObject:self.toDictionary forKey:@"timers"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDevice options:NSJSONWritingPrettyPrinted error:&error];
    if (!error)
    {
        //DebugLog([NSString stringWithFormat:@"JSON device data:\n%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]);
        return jsonData;
    }
    else
    {
        return nil;
    }
}

@end
