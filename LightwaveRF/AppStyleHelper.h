//
//  AppStyleHelper.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 22/10/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppStyleHelper : NSObject

+ (UIColor *)getNavigationBarTintColor;
+ (UIColor *)getTabBarSelectedImageTintColor;
+ (UIColor *)getTabBarTitleColor;
+ (UIColor *)getIOS7NavBarButtonsTextColor;
+ (UIColor *)getIOS7ToolBarButtonsTextColor;
+ (UIColor *)getViewBackgroundColor;
+ (UIColor *)getScrollViewBackgroundColor;
+ (UIColor *)getNavBarBackgroundColor;
+ (UIColor *)getNavBarTextColor;

+ (BOOL)isLightWaveRF;
+ (BOOL)isNexa;
+ (BOOL)isKlikaan;
+ (BOOL)isFSLDemo;
+ (BOOL)isCOCO;
+ (BOOL)isMegaMan;

@end
