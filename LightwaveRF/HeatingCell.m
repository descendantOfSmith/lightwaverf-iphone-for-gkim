//
//  ChooseProductCell.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 18/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "HeatingCell.h"
#import "ButtonsHelper.h"

@interface HeatingCell ()
@property (nonatomic, strong) UIView *containingView;
@property (nonatomic, strong) UILabel *startTimeValueLabel;
@property (nonatomic, strong) UILabel *startTimeTitleLabel;
@property (nonatomic, strong) UILabel *endTimeValueLabel;
@property (nonatomic, strong) UILabel *endTimeTitleLabel;
@property (nonatomic, strong) UILabel *tempValueLabel;
@property (nonatomic, strong) UILabel *tempTitleLabel;
@property (nonatomic, strong) UIButton *startTimeButton;
@property (nonatomic, strong) UIButton *endTimeButton;
@property (nonatomic, strong) UIButton *tempButton;
@property (nonatomic, strong) NSIndexPath *indexPath;
@end

@implementation HeatingCell

@synthesize delegate, indexPath;
@synthesize containingView;
@synthesize startTimeValueLabel, startTimeTitleLabel;
@synthesize endTimeValueLabel, endTimeTitleLabel;
@synthesize tempValueLabel, tempTitleLabel;
@synthesize startTimeButton, endTimeButton, tempButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
    }
    return self;
}

- (void)dealloc {
    
}

+ (float)getCellHeight {
    return 50.0f;
}

- (void)setupCellWithProfile:(Profile*)_profile
                   indexPath:(NSIndexPath *)_indexPath
                    delegate:(id<HeatingCellDelegate>)_delegate {
    
    self.delegate = _delegate;
    self.indexPath = _indexPath;
    
    /*************************************
     *  Containing View
     *************************************/
    if (!self.containingView) {
        self.containingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                                       0.0f,
                                                                       self.contentView.frame.size.width,
                                                                       [HeatingCell getCellHeight])];
        [self.containingView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.containingView];
    }
    
    
    
    /*************************************
     *  Start Time Value Label
     *************************************/
    if (!self.startTimeValueLabel) {
        self.startTimeValueLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.startTimeValueLabel setText:_profile.start];
        [self.startTimeValueLabel setBackgroundColor:[UIColor clearColor]];
        [self.startTimeValueLabel setTextColor:[UIColor blackColor]];
        [self.startTimeValueLabel setMinimumFontSize:13.0f];
        [self.startTimeValueLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0f]];
        [self.startTimeValueLabel setNumberOfLines:1];
        [self.startTimeValueLabel setAdjustsFontSizeToFitWidth:YES];
        [self.startTimeValueLabel sizeToFit];
        [self.containingView addSubview:self.startTimeValueLabel];
    } else {
        [self.startTimeValueLabel setText:_profile.start];
    }
    
    /*************************************
     *  Start Time Title Label
     *************************************/
    if (!self.startTimeTitleLabel) {
        self.startTimeTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.startTimeTitleLabel setText:NSLocalizedString(@"start_time", @"Start time")];
        [self.startTimeTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self.startTimeTitleLabel setTextColor:[UIColor darkGrayColor]];
        [self.startTimeTitleLabel setMinimumFontSize:13.0f];
        [self.startTimeTitleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        [self.startTimeTitleLabel setNumberOfLines:1];
        [self.startTimeTitleLabel setAdjustsFontSizeToFitWidth:YES];
        [self.startTimeTitleLabel sizeToFit];
        [self.containingView addSubview:self.startTimeTitleLabel];
    }
    
    /*************************************
     *  Start Time Button
     *************************************/
    if (!self.startTimeButton) {
        self.startTimeButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"set", @"Set")
                                                           type:kButtonGreyBacking
                                                            tag:kHeatingCellStartTime
                                                          width:self.startTimeTitleLabel.frame.size.width
                                                         height:self.startTimeTitleLabel.frame.size.height
                                                         target:self
                                                       selector:@selector(buttonPressed:)];
        [self.containingView addSubview:self.startTimeButton];
    }
    
    
    
    
    /*************************************
     *  End Time Value Label
     *************************************/
    if (!self.endTimeValueLabel) {
        self.endTimeValueLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.endTimeValueLabel setText:_profile.end];
        [self.endTimeValueLabel setBackgroundColor:[UIColor clearColor]];
        [self.endTimeValueLabel setTextColor:[UIColor blackColor]];
        [self.endTimeValueLabel setMinimumFontSize:13.0f];
        [self.endTimeValueLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0f]];
        [self.endTimeValueLabel setNumberOfLines:1];
        [self.endTimeValueLabel setAdjustsFontSizeToFitWidth:YES];
        [self.endTimeValueLabel sizeToFit];
        [self.containingView addSubview:self.endTimeValueLabel];
    } else {
        [self.endTimeValueLabel setText:_profile.end];
    }
    
    /*************************************
     *  End Time Title Label
     *************************************/
    if (!self.endTimeTitleLabel) {
        self.endTimeTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.endTimeTitleLabel setText:NSLocalizedString(@"end_time", @"End time")];
        [self.endTimeTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self.endTimeTitleLabel setTextColor:[UIColor darkGrayColor]];
        [self.endTimeTitleLabel setMinimumFontSize:13.0f];
        [self.endTimeTitleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        [self.endTimeTitleLabel setNumberOfLines:1];
        [self.endTimeTitleLabel setAdjustsFontSizeToFitWidth:YES];
        [self.endTimeTitleLabel sizeToFit];
        [self.containingView addSubview:self.endTimeTitleLabel];
    }
    
    /*************************************
     *  End Time Button
     *************************************/
    if (!self.endTimeButton) {
        self.endTimeButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"set", @"Set")
                                                           type:kButtonGreyBacking
                                                            tag:kHeatingCellEndTime
                                                          width:self.startTimeButton.frame.size.width
                                                         height:self.startTimeButton.frame.size.height
                                                         target:self
                                                       selector:@selector(buttonPressed:)];
        [self.containingView addSubview:self.endTimeButton];
    }
    
    
    
    
    /*************************************
     *  Temp Value Label
     *************************************/
    if (!self.tempValueLabel) {
        self.tempValueLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.tempValueLabel setText:[NSString stringWithFormat:@"%.0f°C", _profile.temp]];
        [self.tempValueLabel setBackgroundColor:[UIColor clearColor]];
        [self.tempValueLabel setTextColor:[UIColor blackColor]];
        [self.tempValueLabel setMinimumFontSize:13.0f];
        [self.tempValueLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0f]];
        [self.tempValueLabel setNumberOfLines:1];
        [self.tempValueLabel setAdjustsFontSizeToFitWidth:YES];
        [self.tempValueLabel sizeToFit];
        [self.containingView addSubview:self.tempValueLabel];
    } else {
        [self.tempValueLabel setText:[NSString stringWithFormat:@"%.0f", _profile.temp]];
    }
    
    /*************************************
     *  Temp Title Label
     *************************************/
    if (!self.tempTitleLabel) {
        self.tempTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.tempTitleLabel setText:NSLocalizedString(@"temp", @"Temp")];
        [self.tempTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self.tempTitleLabel setTextColor:[UIColor darkGrayColor]];
        [self.tempTitleLabel setMinimumFontSize:13.0f];
        [self.tempTitleLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        [self.tempTitleLabel setNumberOfLines:1];
        [self.tempTitleLabel setAdjustsFontSizeToFitWidth:YES];
        [self.tempTitleLabel sizeToFit];
        [self.containingView addSubview:self.tempTitleLabel];
    }
    
    /*************************************
     *  Temp Button
     *************************************/
    if (!self.tempButton) {
        self.tempButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"set", @"Set")
                                                      type:kButtonGreyBacking
                                                       tag:kHeatingCellTemperature
                                                     width:self.startTimeButton.frame.size.width
                                                    height:self.startTimeButton.frame.size.height
                                                    target:self
                                                  selector:@selector(buttonPressed:)];
        [self.containingView addSubview:self.tempButton];
    }
    
    
    
    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (IBAction)buttonPressed:(id)sender {
    UIButton *_button = (UIButton *)sender;
    if ([self.delegate respondsToSelector:@selector(chooseProductSwitch:indexPath:type:)])
        [self.delegate chooseProductSwitch:self indexPath:self.indexPath type:_button.tag];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    // Start Time Value
    [self.startTimeValueLabel setFrameX:40.0f];
    [self.startTimeValueLabel setFrameY:5.0f];
    
    // Start Time Title
    [self.startTimeTitleLabel alignToBottomOfView:self.startTimeValueLabel padding:5.0f matchHorizontal:YES];
    
    // Start Time Button
    [self.startTimeButton alignToTopOfView:self.startTimeTitleLabel padding:5.0f matchHorizontal:YES];
    
    // End Time Value
    [self.endTimeValueLabel alignToRightOfView:self.startTimeValueLabel padding:40.0f matchVertical:YES];
    
    // End Time Title
    [self.endTimeTitleLabel alignToBottomOfView:self.endTimeValueLabel padding:5.0f matchHorizontal:YES];
    
    // End Time Button
    [self.endTimeButton alignToTopOfView:self.endTimeTitleLabel padding:5.0f matchHorizontal:YES];
    
    // Temp Value
    [self.tempValueLabel alignToRightOfView:self.endTimeValueLabel padding:40.0f matchVertical:YES];
    
    // Temp Title
    [self.tempTitleLabel alignToBottomOfView:self.tempValueLabel padding:5.0f matchHorizontal:YES];
    
    // End Time Button
    [self.tempButton alignToTopOfView:self.tempTitleLabel padding:5.0f matchHorizontal:YES];
    
    if (tmpIsEditing) {
        [self.startTimeValueLabel setAlpha:0.0f];
        [self.endTimeValueLabel setAlpha:0.0f];
        [self.tempValueLabel setAlpha:0.0f];
        
        [self.startTimeButton setAlpha:1.0f];
        [self.endTimeButton setAlpha:1.0f];
        [self.tempButton setAlpha:1.0f];
    } else {
        [self.startTimeValueLabel setAlpha:1.0f];
        [self.endTimeValueLabel setAlpha:1.0f];
        [self.tempValueLabel setAlpha:1.0f];
        
        [self.startTimeButton setAlpha:0.0f];
        [self.endTimeButton setAlpha:0.0f];
        [self.tempButton setAlpha:0.0f];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self positionViewsIsEditing:self.isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

@end
