//
//  EnergyVC.m
//  LightwaveRF
//
//  Created by Nik Lever on 03/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "EnergyVC.h"
#import "UIView+Utilities.h"
#import "Toast+UIView.h"
#import "WFLHelper.h"

@interface EnergyVC ()
@property (nonatomic, assign) EnergyCostDialToggles energyCostType;
@property (nonatomic, strong) EnergyDialView *segmentViewKWH;
@property (nonatomic, strong) EnergyDialView *segmentViewCPH;
@property (nonatomic, strong) EnergyDialView *segmentViewCFYT;
@property (nonatomic, strong) Energy *energy;
@property (nonatomic, strong) UIButton *wifi_btn;
@property (nonatomic, assign) BOOL checkEnergy;
@property (nonatomic, strong) UILabel *lastUpdateLabel;
@end

@implementation EnergyVC

@synthesize nav_bar, segmentViewCPH, segmentViewCFYT, segmentViewKWH, energyCostType, energy;
@synthesize wifi_btn, checkEnergy, lastUpdateLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.energyCostType = kEnergySelectedKWH;
        self.checkEnergy = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    nav_bar.topItem.title = NSLocalizedString(@"energy", @"Energy");
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Check WiFi State
    [self checkWifiState];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.checkEnergy = YES;
    
    // Cancel Previous Timer
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(requestEnergyDataUsingUDP:)
                                               object:nil];
    
    // Immediately Request Fresh Energy Meter Data
    [self requestEnergyDataUsingUDP:[NSNumber numberWithBool:YES]];
    
    [self.view makeToast:NSLocalizedString(@"tap_to_change", @"tap_to_change/H") duration:6.0f position:@"bottom"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.checkEnergy = NO;
    
    // Cancel Previous Timer
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(requestEnergyDataUsingUDP:)
                                               object:nil];
}

#pragma mark - UI Creation

- (void)setupUI {
    // Setup UI
    [self setupBackground];
    
    // Segment Dials
    [self setupSegmentDialViews];
    
    // WiFi Button
    [self setupWiFiButton];
    
    // Tap screen
    [self setupTapToToggle];
}

- (void)setupBackground {
    UIImageView *imageView = nil;
    imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    UIImage *image = [[UIImage imageNamed:@"energy-bg.png"]
                      resizableImageWithCapInsets:UIEdgeInsetsMake(
                                                                   0.0f,
                                                                   0.0f,
                                                                   479.0f,
                                                                   0.0f)];
    [imageView setImage:image];
    [imageView setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight];
    [self.view insertSubview:imageView atIndex:0];
}

- (void)setupWiFiButton {
    self.wifi_btn = [ViewBuilderHelper getButtonWithLoFileName:@"icon_wifi_off.png"
                                                    hiFilename:@"icon_wifi_off.png"
                                                           atX:0.0f
                                                           atY:45.0f
                                                           tag:0
                                                        target:nil
                                                        action:nil];
    [self.wifi_btn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.wifi_btn setImage:[UIImage imageNamed:@"icon_wifi_on.png"] forState:UIControlStateSelected];
    [self.wifi_btn setFrameSize:CGSizeMake(23.0f, 26.0f)];
    [self.wifi_btn setUserInteractionEnabled:NO];
    [self.view addSubview:self.wifi_btn];
}

- (void)setupSegmentDialViews {
    // Vars
    self.heightOffset = 50.0f;
    float tmpSegmentSize = 0.0f;
    
    // KW/H Segment Dial
    tmpSegmentSize = 200.0f;
    self.segmentViewKWH = [[EnergyDialView alloc] initWithFrame:CGRectMake(0.0f,
                                                                           self.heightOffset,
                                                                           tmpSegmentSize,
                                                                           tmpSegmentSize)
                                                    segmentSize:tmpSegmentSize
                                                      showPence:NO
                                                     dataSource:self];
    [self.segmentViewKWH setTag:kEnergyKWH];
    [self.segmentViewKWH setCenterX:self.view.center.x];
    [self.segmentViewKWH refreshData];
    [self.view addSubview:self.segmentViewKWH];
    self.heightOffset += (self.segmentViewKWH.frame.size.height - 20.0f);
    
    // Cost Per Hour Segment Dial
    tmpSegmentSize = 120.0f;
    self.segmentViewCPH = [[EnergyDialView alloc] initWithFrame:CGRectMake(0.0f,
                                                                           self.heightOffset,
                                                                           self.view.frame.size.width / 2,
                                                                           tmpSegmentSize)
                                                    segmentSize:tmpSegmentSize
                                                      showPence:YES
                                                     dataSource:self];
    [self.segmentViewCPH setTag:kEnergyCPH];
    [self.segmentViewCPH setHeadingFont:[UIFont fontWithName:@"Helvetica-Bold" size:150.0f]];
    [self.segmentViewCPH refreshData];
    [self.view addSubview:self.segmentViewCPH];
    
    // Cost Yesterday/Today Segment Dial
    self.segmentViewCFYT = [[EnergyDialView alloc] initWithFrame:CGRectMake(self.segmentViewCPH.frame.origin.x + self.segmentViewCPH.frame.size.width,
                                                                            self.heightOffset,
                                                                            self.segmentViewCPH.frame.size.width,
                                                                            self.segmentViewCPH.frame.size.height)
                                                     segmentSize:tmpSegmentSize
                                                       showPence:YES
                                                      dataSource:self];
    [self.segmentViewCFYT setTag:kEnergyCFYT];
    [self.segmentViewCFYT setHeadingFont:[UIFont fontWithName:@"Helvetica-Bold" size:150.0f]];
    [self.segmentViewCFYT refreshData];
    [self.view addSubview:self.segmentViewCFYT];
}

- (void)setupTapToToggle {
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedScreen:)];
    gestureRecognizer.numberOfTapsRequired = 1;
    gestureRecognizer.enabled = YES;
    gestureRecognizer.cancelsTouchesInView = NO;
    gestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:gestureRecognizer];
    
    /*
    UILabel *_label = [[UILabel alloc] init];
    [_label setBackgroundColor:[UIColor colorWithRed:(140.0/255.0) green:(179.0/255.0) blue:(80.0/255.0) alpha:1.0]];
    [_label setTextColor:[UIColor blackColor]];
    [_label setText:[NSString stringWithFormat:@" %@ ", NSLocalizedString(@"tap_to_change", @"tap_to_change/H")]];
    [_label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
    [_label sizeToFit];
    [_label setTextAlignment:NSTextAlignmentCenter];
    [_label setFrameWidth:self.view.frame.size.width];
    [_label setCenterX:self.view.center.x];
    [_label alignToBottomOfView:self.segmentViewCPH padding:20.0f];
    [self.view addSubview:_label];
     */
}

- (void)checkWifiState {
    ServiceTypeTags _currentType = [[UDPService shared] getServiceType];
    
    if ([[WFLHelper shared] isCheckingState]) {
        if (_currentType == kServiceUDP)
            [self.wifi_btn setSelected:YES];
        
        else if  (_currentType == kServiceWeb)
            [self.wifi_btn setSelected:NO];
    } else
        [self.wifi_btn setSelected:NO];
}

#pragma -
#pragma Private

- (void)refreshData {
    if (self.energyCostType == kEnergySelectedKWH) {
        
        [self.segmentViewKWH setShowLabels:NO];
        [self.segmentViewCPH setShowLabels:NO];
        [self.segmentViewCFYT setShowLabels:NO];
        
    } else {
        
        [self.segmentViewKWH setShowPenceLabel:YES];
        
        if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO])
            [self.segmentViewCPH setShowPenceLabel:YES];
        
        else if ([AppStyleHelper isKlikaan])
            [self.segmentViewCPH setShowPoundLabel:YES];
        
        else if ([AppStyleHelper isNexa])
            [self.segmentViewCPH setShowPoundLabel:YES];

        [self.segmentViewCFYT setShowPoundLabel:YES];
        
    }
    
    [self.segmentViewKWH refreshData];
    [self.segmentViewCPH refreshData];
    [self.segmentViewCFYT refreshData];
}

- (void)tappedScreen:(UIGestureRecognizer *)gestureRecognizer {
    if (self.energyCostType == kEnergySelectedKWH)
        self.energyCostType = kEnergySelectedCost;
    else
        self.energyCostType = kEnergySelectedKWH;
    
    [self refreshData];
}

#pragma mark - EnergyDialView DataSource

- (float)percentageComplete:(EnergyDialView *)tmpEnergyDialView {
    
    switch ([tmpEnergyDialView tag]) {
            
        case kEnergyKWH:
        {
            return self.energyCostType == kEnergySelectedKWH ? (self.energy ? self.energy.eKWHPercentage : 0) : (self.energy ? self.energy.eCPHPercentage : 0);
        }
            break;
            
        case kEnergyCPH:
        {
            return self.energyCostType == kEnergySelectedKWH ? (self.energy ? self.energy.eKWHTodayPercentage : 0) : (self.energy ? self.energy.eCFTPercentage : 0);
        }
            break;
            
        case kEnergyCFYT:
        {
            return self.energyCostType == kEnergySelectedKWH ? (self.energy ? self.energy.eKWHYesterdayPercentage : 0) : (self.energy ? self.energy.eCFYPercentage : 0);
        }
            break;
            
        default:
            return 0;
            break;
    }
    
}

- (NSString *)segmentHeading:(EnergyDialView *)tmpEnergyDialView {
    switch ([tmpEnergyDialView tag]) {
            
        case kEnergyKWH:
        {
            return self.energyCostType == kEnergySelectedKWH ? NSLocalizedString(@"kW", @"kW") : NSLocalizedString(@"cost_per_hour", @"Cost Per Hour");
        }
            break;
            
        case kEnergyCPH:
        {
            return self.energyCostType == kEnergySelectedKWH ? NSLocalizedString(@"kwh_today", @"kWh For Today") : NSLocalizedString(@"cost_today", @"Cost For Today");
        }
            break;
            
        case kEnergyCFYT:
        {
            return self.energyCostType == kEnergySelectedKWH ? NSLocalizedString(@"kwh_yesterday", @"kWh For Yesterday") : NSLocalizedString(@"cost_yesterday", @"Cost For Yesterday");
        }
            break;
            
        default:
            return @"";
            break;
    }
}

- (NSString *)segmentValue:(EnergyDialView *)tmpEnergyDialView {
    switch ([tmpEnergyDialView tag]) {
            
        case kEnergyKWH:
        {
            return self.energyCostType == kEnergySelectedKWH ? (self.energy ? self.energy.eKWHStr : @"0") : (self.energy ? self.energy.eCPHStr : @"0");
        }
            break;
            
        case kEnergyCPH:
        {
            return self.energyCostType == kEnergySelectedKWH ? (self.energy ? self.energy.eKWHTodayStr : @"0") : (self.energy ? self.energy.eCFTStr : @"0");
        }
            break;
            
        case kEnergyCFYT:
        {
            return self.energyCostType == kEnergySelectedKWH ? (self.energy ? self.energy.eKWHYesterdayStr : @"0") : (self.energy ? self.energy.eCFYStr : @"0");
        }
            break;
            
        default:
            return @"";
            break;
    }
}

#pragma mark - Get Energy Meter

- (void)requestEnergyDataUsingUDP:(NSNumber *)_useUDP {
    if (self.checkEnergy) {
        if (!self.energy)
            [self showSpinnerWithText:NSLocalizedString(@"energy_spinner_request", @"Requesting Energy Meter...")];
            
        if ([_useUDP boolValue]) {
            UDPService *udp = [UDPService shared];
            [udp getMeterWithTag:kRequestEnergyMeter callingClass:[self getClassName:[self class]]];
        } else {
            RemoteSettings *remote = [RemoteSettings shared];
            [remote pullEnergyMeterWithCallingClass:[self getClassName:[self class]] tag:kRequestEnergyMeter];
        }
    }
}

- (void)startEnergyRefreshTimer {
    // Every 5 Seconds Ping server for fresh data
    SEL _selector = @selector(requestEnergyDataUsingUDP:);
    if ([self respondsToSelector:_selector])
        [self performSelector:_selector withObject:[NSNumber numberWithBool:YES] afterDelay:5]; // seconds
}

- (void)successEnergyMeter:(NSString*)data {
    //DebugLog(@"Energy = %@", data);
    self.energy = [EnergyHelper getEnergyFromData:data];
    self.energy.lastUpdate = [NSDate date];
    [self refreshData];
    [self startEnergyRefreshTimer];
    [self updateLastUpdateLabel];
}

- (void)updateLastUpdateLabel {
    
    if (self.energy.lastUpdate) {
        
        
        if (!self.lastUpdateLabel) {
            self.lastUpdateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            [self.lastUpdateLabel setBackgroundColor:[UIColor clearColor]];
            [self.lastUpdateLabel setTextColor:[UIColor whiteColor]];
            [self.view addSubview:self.lastUpdateLabel];
        }
        [self.lastUpdateLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:8.0f]];
        [self.lastUpdateLabel setText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"last_updated", @"last_updated"), [self.energy.lastUpdate returnEnergyDateAndTime]]];
        [self.lastUpdateLabel sizeToFit];
        float padding = 2.0f;
        [self.lastUpdateLabel setFrameX:(self.view.frame.size.width - self.lastUpdateLabel.frame.size.width) - padding];
        [self.lastUpdateLabel setFrameY:padding];
        //[self.lastUpdateLabel setCenterY:nav_bar.center.y];
    }
}

#pragma mark - Remote Delegate

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kRemoteClassNameKey];
    //NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey])
        {
            /*************************
             * Error
             *************************/
            NSError *error = [_dict objectForKey:kRemoteErrorKey];
            //NSString *message = NSLocalizedString(@"web_service_error", @"There was a problem connecting to the Lightwave server");
            //NSString *str = [NSString stringWithFormat:@"%@\n%@", message, [error localizedDescription]];
            [self showError:[error localizedDescription]];
            [self startEnergyRefreshTimer];
        }
        
        else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
        {
            /*************************
             * Success
             *************************/
            NSString *data = [_dict objectForKey:kRemoteResponseKey];
            [self successEnergyMeter:data];
        }
        
        else if ([[_notification name] isEqualToString:kRemoteFailedNotificationKey])
        {
            /*************************
             * Failed
             *************************/
            NSString *_failed = [_dict objectForKey:kRemoteFailedKey];
            [self showError:_failed];
            [self startEnergyRefreshTimer];
        }
    }
}

#pragma mark - UDP Delegate

- (void)receivedUDPServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kUDPClassNameKey];
            
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        [self removeSpinner];
        
        if ([[_notification name] isEqualToString:kUDPErrorNotificationKey])
        {
            /*************************
             * Error
             *************************/
            // Attempt the request using RemoteSettings...
            [self requestEnergyDataUsingUDP:[NSNumber numberWithBool:NO]];
        }
        
        else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
        {
            /*************************
             * Success
             *************************/
            NSString *_responseString = [_dict objectForKey:kUDPResponseKey];
            [self successEnergyMeter:_responseString];
        }
        
        else if ([[_notification name] isEqualToString:kUDPFailedNotificationKey])
        {
            /*************************
             * Failed
             *************************/
            
            BOOL _attemptRemote = [[_dict objectForKey:kUDPAllowSwitchToRemoteKey] boolValue];

            if (_attemptRemote) {
                // Attempt the request using RemoteSettings...
                [self requestEnergyDataUsingUDP:[NSNumber numberWithBool:NO]];
            } else {
                DebugLog(@"Attempt Remote Disabled...");
                [self startEnergyRefreshTimer];
            }
        }
    }
}

// Overridden
- (void)receivedUDPCheckedStateSuccessNotification:(NSNotification *)_notification {
    [self.wifi_btn setSelected:YES];
}

// Overridden
- (void)receivedUDPCheckedStateFailNotification:(NSNotification *)_notification {
    [self.wifi_btn setSelected:NO];
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
