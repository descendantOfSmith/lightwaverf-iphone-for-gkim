//
//  ChooseProductCell.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 18/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"

@interface ChooseProductCell : UITableViewCell {
    
}

@property (nonatomic, strong) UIView *containingView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *selectedButton;
@property (nonatomic, strong) Device *device;

+ (float)getCellHeight;

- (void)setupCellWithTitle:(NSString*)tmpTitle device:(Device *)_device selected:(BOOL)_selected;

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing;

@end
