//
// Platform & Screens
//

#define STATUS_BAR_HEIGHT \
    MIN(RECT_HEIGHT(kSharedApplication.statusBarFrame), \
    RECT_WIDTH(kSharedApplication.statusBarFrame))

#define SCREEN_WIDTH_PORTRAIT() \
    RECT_WIDTH(kMainScreen.bounds)
#define SCREEN_HEIGHT_PORTRAIT() \
    RECT_HEIGHT(kMainScreen.bounds)
#define SCREEN_WIDTH_LANDSCAPE() \
    SCREEN_HEIGHT_PORTRAIT()
#define SCREEN_HEIGHT_LANDSCAPE() \
    SCREEN_WIDTH_PORTRAIT()

#define SCREEN_WIDTH_FOR_ORIENTATION(orientation) \
    ((orientation == UIInterfaceOrientationLandscapeLeft || \
    orientation == UIInterfaceOrientationLandscapeRight) ? \
    SCREEN_WIDTH_LANDSCAPE() : SCREEN_WIDTH_PORTRAIT())

#define SCREEN_HEIGHT_FOR_ORIENTATION(orientation) \
    ((orientation == UIInterfaceOrientationLandscapeLeft || \
    orientation == UIInterfaceOrientationLandscapeRight) ? \
    SCREEN_HEIGHT_LANDSCAPE() : SCREEN_HEIGHT_PORTRAIT())

#define SCREEN_WIDTH() \
    SCREEN_WIDTH_FOR_ORIENTATION((UIInterfaceOrientation)[kSharedApplication statusBarOrientation])

#define SCREEN_HEIGHT() \
    SCREEN_HEIGHT_FOR_ORIENTATION((UIInterfaceOrientation)[kSharedApplication statusBarOrientation])

#define IS_IPAD() \
    ([[UIDevice currentDevice] respondsToSelector:@selector(userInterfaceIdiom)] ? \
    ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) : \
    false)

#define IS_IPHONE_5() \
    (IS_IPAD() ? \
    false : \
    (SCREEN_HEIGHT_PORTRAIT() > 480.0f ? true : false))
