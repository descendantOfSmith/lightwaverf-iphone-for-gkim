//
//  ChooseProductCell.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 18/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Zone.h"

typedef enum
{
    kHeatingCellVCBoost = 1001,
    kHeatingCellVCStandby,
    kHeatingCellVCProgram,
    kHeatingCellVCSetTemp
} HeatingCellVCOptions;

@protocol HomeHeatingCellDelegate;

@interface HomeHeatingCell : UITableViewCell {
    
}

@property (nonatomic, strong) Zone *zone;
@property (strong, nonatomic) IBOutlet UIImageView *active_img;
@property (strong, nonatomic) NSTimer *fadeActive_tmr;
@property (strong, nonatomic) NSTimer *boostFadeActive_tmr;
@property (weak, nonatomic) IBOutlet UIProgressView *current_sld;
@property (weak, nonatomic) IBOutlet UIImageView *boostActive_img;
@property (weak, nonatomic) IBOutlet UILabel *current_lbl;
@property (weak, nonatomic) IBOutlet UILabel *target_lbl;
@property (weak, nonatomic) IBOutlet UILabel *current_localised_lbl;
@property (weak, nonatomic) IBOutlet UILabel *target_localised_lbl;
@property (assign, nonatomic) id<HomeHeatingCellDelegate> delegate;
@property (strong, nonatomic) NSIndexPath *indexPath;

- (IBAction)boostPressed:(id)sender;
- (IBAction)standbyPressed:(id)sender;
- (IBAction)programPressed:(id)sender;
- (IBAction)setTempPressed:(id)sender;

- (void)setupCellWithZone:(Zone*)_zone
                indexPath:(NSIndexPath *)_indexPath
                 delegate:(id<HomeHeatingCellDelegate>)_delegate;

@end

@protocol HomeHeatingCellDelegate <NSObject>

@optional

- (void)heatingPressed:(HomeHeatingCell *)_heatingCellVC
                option:(HeatingCellVCOptions)_option
             indexPath:(NSIndexPath *)_indexPath;

@end
