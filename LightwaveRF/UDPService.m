//
//  UDPService.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 24/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "UDPService.h"
#import "NSString+Utilities.h"
#import "RemoteSettings.h"

static UDPService *myRemote = nil;

@interface UDPService ()
@property (nonatomic, strong) GCDAsyncUdpSocket *udpSocket;
@property (nonatomic, strong) NSMutableArray *queueDictArray;
@end

@implementation UDPService

@synthesize udpSocket, queueDictArray;

+ (id)shared {
    @synchronized(self) {
        if(myRemote == nil) {
            myRemote = [[super allocWithZone:NULL] init];
        }
    }
    return myRemote;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self shared];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

#pragma mark - Setup

- (void)setupSocket {

    if (!self.udpSocket) {
        self.udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
                
        // Enable Broadcasting
        NSError *error = nil;
        if (![self.udpSocket enableBroadcast:YES error:&error])
        {
            DebugLog(@"Error broadcast: %@", [error localizedDescription]);
            [self cleanupAllRequests];
            return;
        }
        
        // Set Listen Port
        if (![self.udpSocket bindToPort:kPortListen error:&error])
        {
            DebugLog(@"Error binding: %@", [error localizedDescription]);
            [self cleanupAllRequests];
            return;
        }
         
        // Begin receiving
        if (![self.udpSocket beginReceiving:&error])
        {
            DebugLog(@"Error receiving: %@", [error localizedDescription]);
            [self cleanupAllRequests];
            return;
        }
        
    }
    
    [self setupQueueArray];
}

- (void)setupQueueArray {
    if (!self.queueDictArray)
        self.queueDictArray = [[NSMutableArray alloc] init];
}

#pragma mark - Send Command

- (void)sendCommandWithTransactionDict:(NSMutableDictionary *)_transactionDict {
    // Setup
    [self setupSocket];
    
    //DebugLog(@"Send Command: %@", [self getCommandFromDict:_transactionDict]);
    
    // Send
    NSUInteger _transactionID = [self getTransactionIDFromDict:_transactionDict];
    NSString *_msg = [NSString stringWithFormat:@"%d,%@\n",
                      _transactionID,
                      [self getCommandFromDict:_transactionDict]];
    
    if (_transactionID != kPollID)
        DebugLog(@"Sending = %@", _msg);
    
    NSData *_data = [
                     _msg
                     dataUsingEncoding:NSUTF8StringEncoding
                     ];
    [self.udpSocket sendData:_data
                      toHost:kHost
                        port:kPortSend
                 withTimeout:-1
                         tag:[self getTransactionIDFromDict:_transactionDict]];
}

- (void)send:(NSString *)_msg
         tag:(NSUInteger)_tag
callingClass:(NSString *)_className
transactionID:(NSUInteger)_transactionID {
    
    NSMutableDictionary *_transactionDict = [self createDictWithMessage:_msg
                                                                    tag:_tag
                                                           callingClass:_className
                                                          transactionID:_transactionID];

    // Remove from Queue if already exisits (Safe-guard)
    [self removeTransactionDictFromQueueWithTransactionID:_transactionID];
    
    // Add Transaction to Queue
    [self addTransactionDictToQueue:_transactionDict];
    
    // Send
    [self sendCommandWithTransactionDict:_transactionDict];
}

- (BOOL)canSendOnUDP:(NSString *)_msg tag:(NSUInteger)_tag callingClass:(NSString *)_className transactionID:(NSUInteger)_transactionID {
    BOOL _useRemote = [[NSUserDefaults standardUserDefaults] boolForKey:@"testWifi"];
    
    if (_useRemote) {
        //DebugLog(@"STAY IN 3G/REMOTE MODE ENABLED....");
        
        // Set current service type
        [self setServiceType:kServiceWeb];
        
        NSMutableDictionary *_transactionDict = [self createDictWithMessage:_msg
                                                                        tag:_tag
                                                               callingClass:_className
                                                              transactionID:_transactionID];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUDPFailedNotificationKey object:_transactionDict];
        return NO;
    } else
        return YES;
}

#pragma mark - Public

- (void)send:(NSString *)_msg tag:(NSUInteger)_tag callingClass:(NSString *)_className {
    NSUInteger _transactionID = [UDPService getTransactionID];
    if ([self canSendOnUDP:_msg
                       tag:_tag
              callingClass:_className
             transactionID:_transactionID]) {
        
        [self send:_msg
               tag:_tag
      callingClass:_className
     transactionID:_transactionID];
    }
}

- (void)pollWithTag:(NSUInteger)_tag callingClass:(NSString *)_className {
    NSUInteger _transactionID = kPollID;
    NSString *_msg = [ServiceType getPollCommand];
    
    if ([self canSendOnUDP:_msg
                       tag:_tag
              callingClass:_className
             transactionID:_transactionID]) {
        
    
        [self send:_msg
               tag:_tag
      callingClass:_className
     transactionID:_transactionID];
        
    }
}

- (void)stopAllEventsWithTag:(NSUInteger)_tag callingClass:(NSString *)_className {
    NSUInteger _transactionID = [UDPService getTransactionID];
    NSString *_msg = [ServiceType getStopAllEventsCommand];
    
    if ([self canSendOnUDP:_msg
                       tag:_tag
              callingClass:_className
             transactionID:_transactionID]) {
        
        [self send:_msg
               tag:_tag
      callingClass:_className
     transactionID:_transactionID];
    }
}

- (void)getMeterWithTag:(NSUInteger)_tag callingClass:(NSString *)_className {
    NSUInteger _transactionID = kMeterID;
    NSString *_msg = [ServiceType getMeterCommand];
    
    if ([self canSendOnUDP:_msg
                       tag:_tag
              callingClass:_className
             transactionID:_transactionID]) {
        
        [self send:_msg
               tag:_tag
      callingClass:_className
     transactionID:_transactionID];
    }
}

#pragma mark - Private

- (NSMutableDictionary *)createDictWithMessage:(NSString *)_msg
                                           tag:(NSUInteger)_tag
                                  callingClass:(NSString *)_className
                                 transactionID:(NSUInteger)_transactionID {
    
    return [[NSMutableDictionary alloc] initWithObjectsAndKeys:
            [NSDate date], kUDPDateKey,
            _msg, kUDPCommandKey,
            [NSNumber numberWithInt:_tag], kUDPTagKey,
            _className, kUDPClassNameKey,
            [NSNumber numberWithInt:_transactionID], kUDPTransactionIDKey,
            [NSNumber numberWithBool:NO], kUDPDidCompleteKey,
            [NSNumber numberWithBool:YES], kUDPAllowSwitchToRemoteKey,
            [NSNumber numberWithInt:0], kUDPNumRetrysKey,
            nil];
}

- (NSMutableDictionary *)getTransactionDictWithTransactionID:(NSUInteger)_transactionID {
    NSPredicate *filter = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %d", kUDPTransactionIDKey, _transactionID]];
    NSArray *filteredQueue = [self.queueDictArray filteredArrayUsingPredicate:filter];
    if (filteredQueue.count > 0)
        return [filteredQueue objectAtIndex:0];
    else
        return nil;
}

- (NSString *)getCommandFromDict:(NSMutableDictionary *)_dict {
    return [_dict objectForKey:kUDPCommandKey];
}

- (NSUInteger)getTagFromDict:(NSMutableDictionary *)_dict {
    return [[_dict objectForKey:kUDPTagKey] intValue];
}

- (NSString *)getClassNameFromDict:(NSMutableDictionary *)_dict {
    return [_dict objectForKey:kUDPClassNameKey];
}

- (NSUInteger)getTransactionIDFromDict:(NSMutableDictionary *)_dict {
    return [[_dict objectForKey:kUDPTransactionIDKey] intValue];
}

- (NSUInteger)getNumRetrysFromDict:(NSMutableDictionary *)_dict {
    return [[_dict objectForKey:kUDPNumRetrysKey] intValue];
}

#pragma mark - Overridden from superclass

- (void)purgeExpiredRequests:(NSTimer *)timer {
    NSMutableArray *_removeRequestsArray = [[NSMutableArray alloc] init];
    for (NSMutableDictionary *_dict in self.queueDictArray) {
        NSDate *_date = [_dict objectForKey:kUDPDateKey];
        NSInteger _numMins = [NSDate minutesBetweenDate:_date andDate:[NSDate date]];
        if (_numMins > kExpireNumMins) {
            [_removeRequestsArray addObject:_dict];
        }
    }
    
    [self.queueDictArray removeObjectsInArray:_removeRequestsArray];
}

- (void)cleanupAllRequests {
    if (self.udpSocket) {
        [self.udpSocket close];
    }
    
    self.udpSocket = nil;
}

#pragma mark - Retrieve a Transaction ID

+ (NSString *)getTransactionIDKey {
    return [NSString stringWithFormat:@"UDPService-%@", NSStringFromSelector(_cmd)];
}

+ (void)setTransactionID:(NSUInteger)_transactionID {
    if (_transactionID < kMinTransactionID || _transactionID > kMaxTransactionID)
        _transactionID = kMinTransactionID;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:[NSNumber numberWithInt:_transactionID] forKey:[self getTransactionIDKey]];
    [userDefaults synchronize];
}

+ (NSUInteger)getTransactionID {
    NSNumber *_transactionID = [[NSUserDefaults standardUserDefaults] objectForKey:[self getTransactionIDKey]];
    NSUInteger _transID = [_transactionID intValue];
    
    if (!_transactionID)
        _transID = kMinTransactionID;
    
    [UDPService setTransactionID:_transID + 1];
    
    return _transID;
}

#pragma mark - UDP Request Queue

- (void)addTransactionDictToQueue:(NSMutableDictionary *)_transactionDict {
    [self setupQueueArray];
    [self.queueDictArray addObject:_transactionDict];
}

- (void)removeTransactionDictFromQueue:(NSMutableDictionary *)_transactionDict {
    if ([self.queueDictArray containsObject:_transactionDict]) {
        [self.queueDictArray removeObject:_transactionDict];
    }
}

- (void)removeTransactionDictFromQueueWithTransactionID:(NSUInteger)_transactionID {
    NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_transactionID];
    [self removeTransactionDictFromQueue:_dict];
}

#pragma mark - Callbacks

- (BOOL)didRetryWithTransactionDict:(NSMutableDictionary *)_transactionDict {
    NSUInteger _numRetrys = [self getNumRetrysFromDict:_transactionDict];
    //DebugLog(@"Num Retrys: %d", _numRetrys);
    
    if (_numRetrys == kNumRetrys) {
        
        return NO;
        
    } else {
        
        // Increase Count
        [_transactionDict setValue:[NSNumber numberWithInt:_numRetrys + 1] forKey:kUDPNumRetrysKey];
        
        // Resend Command
        [self sendCommandWithTransactionDict:_transactionDict];
        
        return YES;
        
    }
}

- (void)failedCallbackForTransactionDict:(NSMutableDictionary *)_transactionDict {
    // Did reach max retrys?
    if (![self didRetryWithTransactionDict:_transactionDict]) {
        
        // Did Complete!
        [self setDidCompleteUDP:YES withTransactionDict:_transactionDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kUDPFailedNotificationKey object:_transactionDict];
        [self removeTransactionDictFromQueue:_transactionDict];
    }
}

- (void)sentCallbackForTransactionDict:(NSMutableDictionary *)_transactionDict {
    // Did Complete!
    [self setDidCompleteUDP:YES withTransactionDict:_transactionDict];
    
    //DebugLog(@"Sent UDP: %@", _transactionDict);
    NSString *_response = [_transactionDict objectForKey:kUDPResponseKey];
    //DebugLog(@"Sent: %@", _response);

    [[NSNotificationCenter defaultCenter] postNotificationName:kUDPSentNotificationKey object:_transactionDict];
    [self removeTransactionDictFromQueue:_transactionDict];
    
    //DebugLog(@"Response: %@", _response);
    NSArray *_tokens = [_response componentsSeparatedByString:@","];
    NSString *_msg = _tokens.count > 0 ? [_tokens objectAtIndex:0] : nil;
    //DebugLog(@"MSG: %@", _msg);
    if ([_msg isEqualToString:@"ERR"]) {
        [self isService:kServiceWeb showAlert:NO];
    } else
        [self isService:kServiceUDP];
}

- (void)errorCallbackForTransactionDict:(NSMutableDictionary *)_transactionDict attemptRetry:(BOOL)_attemptRetry {
    // Did reach max retrys?
    
    BOOL _removeDict = NO;
    
    if (_attemptRetry) {
        
        if (![self didRetryWithTransactionDict:_transactionDict]) {
            _removeDict = YES;
        }
        
    } else {
        _removeDict = YES;
    }
    
    if (_removeDict) {
        // Did Complete!
        [self setDidCompleteUDP:YES withTransactionDict:_transactionDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kUDPErrorNotificationKey object:_transactionDict];
        [self removeTransactionDictFromQueue:_transactionDict];
    }
}

#pragma mark - NSTimer (Check UDP Complete)

- (void)startTimerWithTransactionDict:(NSMutableDictionary *)_transactionDict {
    // Expecting a response from the server...
    [NSTimer scheduledTimerWithTimeInterval:kTimeout
                                     target:self
                                   selector:@selector(checkDidResponseComplete:)
                                   userInfo:_transactionDict
                                    repeats:NO];
}

- (void)setDidCompleteUDP:(BOOL)_didComplete withTransactionDict:(NSMutableDictionary *)_transactionDict {
    [_transactionDict setValue:[NSNumber numberWithBool:_didComplete] forKey:kUDPDidCompleteKey];
}

- (void)checkDidResponseComplete:(NSTimer *)timer {
    // Check if we have received the response...
    // If not, fail callback (reverts to remote settings)
    NSMutableDictionary *_dict = (NSMutableDictionary *)[timer userInfo];
    BOOL _didComplete = _dict ? [[_dict objectForKey:kUDPDidCompleteKey] boolValue] : YES;
    if (!_didComplete)
        [self failedCallbackForTransactionDict:_dict];
}

#pragma mark - GCDAsyncUdpSocket Delegate

/**
 * By design, UDP is a connectionless protocol, and connecting is not needed.
 * However, you may optionally choose to connect to a particular host for reasons
 * outlined in the documentation for the various connect methods listed above.
 *
 * This method is called if one of the connect methods are invoked, and the connection is successful.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didConnectToAddress:(NSData *)address
{
    DebugLog(@"didConnectToAddress");
}

/**
 * By design, UDP is a connectionless protocol, and connecting is not needed.
 * However, you may optionally choose to connect to a particular host for reasons
 * outlined in the documentation for the various connect methods listed above.
 *
 * This method is called if one of the connect methods are invoked, and the connection fails.
 * This may happen, for example, if a domain name is given for the host and the domain name is unable to be resolved.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error
{
    DebugLog(@"didNotConnect: %@", [error localizedDescription]);
    [self cleanupAllRequests];
}

/**
 * Called when the datagram with the given tag has been sent.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    if (tag != kPollID)
        DebugLog(@"didSendDataWithTag: %ld", tag);

    NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:[[NSNumber numberWithLong:tag] intValue]];
    NSString *_command = [self getCommandFromDict:_dict];
    
    // Check if we are waiting for a response from WFL or not
    // '!', '?', '@' commands require a response
    if ([_command characterAtIndex:0] != '!' &&
        [_command characterAtIndex:0] != '?' &&
        [_command characterAtIndex:0] != '@') {
        [self sentCallbackForTransactionDict:_dict];
    } else {
        // We expect a response from the server
        // Which should trigger the 'didReceiveData' delegate
        // If no response after a certain time, revert to web by sending fail callback
        [self startTimerWithTransactionDict:_dict];
    }
}

/**
 * Called if an error occurs while trying to send a datagram.
 * This could be due to a timeout, or something more serious such as the data being too large to fit in a sigle packet.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    DebugLog(@"didNotSendDataWithTag: %ld, dueToError: %@", tag, [error localizedDescription]);
    
    NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:[[NSNumber numberWithLong:tag] intValue]];
    [_dict setValue:error forKey:kUDPErrorKey];
    [self errorCallbackForTransactionDict:_dict attemptRetry:YES];
}

/**
 * Called when the socket has received the requested datagram.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock
   didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    
    if (msg)
    {
        // Require 3 digits, a comma, and status
        if (msg.length >= 5) {
            NSString *_idStr = [msg substringToIndex:3];
            NSUInteger _id = [_idStr intValue];
            NSString *_status = [msg substringFromIndex:4];
            
            if (_id != kPollID) {
                DebugLog(@"didReceiveData: %@", msg);
            }
            
            // Valid ID
            if ([_idStr intValue] != 0) {
                
                _status = [_status stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                if ([_status isEqualToString:@"OK"] || _id == kPollID) {
                    
                    NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_id];
                    NSString *_responseString = [_status stringByReplacingOccurrencesOfString:@"?V=" withString:@""];
                    [_dict setValue:_responseString forKey:kUDPResponseKey];
                    [self sentCallbackForTransactionDict:[self getTransactionDictWithTransactionID:_id]];
                    
                } else if (_id == kMeterID) {
                    
                    NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_id];
                    NSString *_responseString = [_status stringByReplacingOccurrencesOfString:@"?W=" withString:@""];
                    [_dict setValue:_responseString forKey:kUDPResponseKey];
                    [self sentCallbackForTransactionDict:_dict];
                    
                } else {
                                        
                    if ([_status containsString:@"ERR"]) {
                        
                        // 125,ERR,3,"Sequence Missing"
                        NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_id];
                        NSString *_responseString = [_status stringBetweenString:@"\"" andString:@"\""];
                        [_dict setValue:_responseString forKey:kUDPResponseKey];
                        [_dict setObject:[NSNumber numberWithBool:NO] forKey:kUDPAllowSwitchToRemoteKey];
                        [self failedCallbackForTransactionDict:_dict];
                        
                    } else {
                        DebugLog(@"1. Unhandled Response: %@", msg);
                    }
                }
            } else {
              DebugLog(@"2. Unhandled Response: %@", msg);  
            }
        } else {
            DebugLog(@"3. Unhandled Response: %@", msg);
        }
         
    }
    else
    {
        NSString *host = nil;
        uint16_t port = 0;
        [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
        DebugLog(@"Unknown Message: %@:%hu", host, port);
    }
}

/**
 * Called when the socket is closed.
 **/
- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error
{
    DebugLog(@"udpSocketDidClose. Error: %@", [error localizedDescription]);
    
    [self cleanupAllRequests];
    [self didRemoveDictError:error];
}

- (void)didRemoveDictError:(NSError *)error {
    if (self.queueDictArray.count > 0) {
        //DebugLog(@"Removing...");
        NSMutableDictionary *_dict = [self.queueDictArray objectAtIndex:0];
        if (_dict){
            //DebugLog(@"REMOVED: %@", [self getCommandFromDict:_dict]);
            [_dict setValue:error forKey:kUDPErrorKey];
            [self errorCallbackForTransactionDict:_dict attemptRetry:YES];
            
            // Loop through and check for more...
            [self didRemoveDictError:error];
        }
    }
}

@end
