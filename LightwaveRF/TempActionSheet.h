//
//  TempActionSheet.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 06/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "LightwaveActionSheet.h"

#define kTempMin 10
#define kTempMax 31

@interface TempActionSheet : LightwaveActionSheet <UIPickerViewDataSource, UIPickerViewDelegate> {
    
}

@property (nonatomic, strong) DistancePickerView *tempPickerView;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
  showNumComponents:(NSUInteger)tmpNumComponents;

@end
