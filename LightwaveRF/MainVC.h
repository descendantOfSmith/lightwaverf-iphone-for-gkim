//
//  MainVC.h
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (retain, nonatomic) IBOutlet UINavigationBar *nav_bar;
@property (retain, nonatomic) IBOutlet UITableView *list_table;

@end
