//
//  Home.m
//  LightwaveRF
//
//  Created by Nik Lever on 04/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "Home.h"
#import "Zone.h"
#import "Event.h"
#import "Profile.h"
#import "Action.h"
#import "Device.h"
#import "DB.h"
#import "LTimer.h"
#import "WFLHelper.h"
#import "Account.h"
#import "Constants.h"
#import "NSMutableArray-Utilities.h"

Home *myHome = nil;

@implementation Home

@synthesize zones = _zones;
@synthesize events = _events;
@synthesize profiles = _profiles;
@synthesize timers = _timers;
@synthesize accounts = _accounts;

+ (id)shared {
    if (myHome == nil) {
        myHome = [[Home alloc] init];
    }
    return myHome;
}

- (id)init {
    self = [super init];
    
    if (self) {
        _zones = [[NSMutableArray alloc] init];
        _events = [[NSMutableArray alloc] init];
        _profiles = [[NSMutableArray alloc] init];
        _timers = [[NSMutableArray alloc] init];
        _accounts = [[NSMutableArray alloc] init];
        
        [self loadAccounts];
    }
    return self;
}

#pragma mark - Zones

-(BOOL) addZone:(Zone *)zone{
    if (self.zones == nil) return NO;
    [self.zones addObject:zone];
    return YES;
}

-(BOOL) insertZone:(Zone *)zone atIndex:(int)index{
    if (self.zones == nil) return NO;
    [self.zones insertObject:zone atIndex:index];
    return YES;
}


-(void) removeZone:(Zone *)zone{
    if (self.zones == nil)
        return;
    
    // Delete devices in this zone...
    // Delete Timers based on these devices
    
    NSMutableArray *_devicesToRemove = [[NSMutableArray alloc] initWithArray:zone.devices];
    for (Device *_device in _devicesToRemove) {
        [zone removeDevice:_device];
    }
    
    [zone saveDevicesToDB];
    
    // Delete Event Actions related to this zone...
    
    for (Event *_event in self.events){
        
        NSMutableArray *_removeActionsArray = [[NSMutableArray alloc] init];
        
        for (Action *_action in _event.actions) {
            Zone *_zoneForAction = [self getZoneByID:_action.zoneID];
            
            if ([_zoneForAction isEqual:zone]) {
                [_removeActionsArray addObject:_action];
            }
        }
        
        for (Action *_action in _removeActionsArray)
            [_event removeAction:_action];
        
        if (_removeActionsArray.count > 0) {
            [_removeActionsArray removeAllObjects];
            [_event saveActionsToDB];
        }
    }
    
    [self.zones removeObject:zone];
}

#pragma mark - Events

-(BOOL) addEvent:(Event *)event{
    if (self.events == nil) return NO;
    [self.events addObject:event];
    return YES;
}

-(BOOL) insertEvent:(Event *)event atIndex:(int)index{
    if (self.events == nil) return NO;
    [self.events insertObject:event atIndex:index];
    return YES;
}


-(void) removeEvent:(Event *)event{
    if (self.events == nil)
        return;
    
    // Remove associated timer
    LTimer *_timer = [self getTimerByName:event.timerName];
    if (_timer) {
        // Update WFL
        [[WFLHelper shared] sendToUDP:YES command:[ServiceType getDeleteTimerCommand:_timer] tag:kRequestDeleteTimer];
        
        // Remove Timer
        [self removeTimer:_timer];
        
        // Save
        [self saveTimers];
    }
    
    // Delete Actions associated with Event
    [event deleteActions];
    [event saveActionsToDB];
    
    // Remove Event
    [self.events removeObject:event];
}

- (BOOL)isFreeEventSlots {
    if (self.events.count >= MAX_EVENTS) {
        return NO;
    } else
        return YES;
}

#pragma mark - Timers

-(BOOL) addTimer:(LTimer *)timer{
    if (self.timers == nil)
        return NO;
    [self.timers addObject:timer];
    return YES;
}

-(BOOL) insertTimer:(LTimer *)timer atIndex:(int)index{
    if (self.timers == nil)
        return NO;
    [self.timers insertObject:timer atIndex:index];
    return YES;
}

-(void) removeTimer:(LTimer *)_timer {
    if (self.timers == nil)
        return;
    
    [self.timers removeObject:_timer];
}

- (BOOL)isFreeTimerSlots {
    if (self.timers.count >= MAX_TIMERS) {
        return NO;
    } else
        return YES;
}

#pragma mark - Accounts

-(BOOL) addAccount:(Account *)account {
    if (self.accounts == nil)
        return NO;
    [self.accounts addObject:account];
    return YES;
}

-(BOOL) insertAccount:(Account *)account atIndex:(int)index {
    if (self.accounts == nil)
        return NO;
    [self.accounts insertObject:account atIndex:index];
    return YES;
}

-(void) removeAccount:(Account *)account {
    if (self.accounts == nil)
        return;
    
    [self.accounts removeObject:account];
}

- (Account *)getAccountWithEmail:(NSString *)_email {
    for (Account *_account in self.accounts) {
        if ([_account.email isEqualToString:_email]) {
            return _account;
        }
    }
    
    return nil;
}

- (Account *)getAccountWithID:(NSInteger)_id {
    for (Account *_account in self.accounts) {
        if (_account.accountId == _id) {
            return _account;
        }
    }
    
    return nil;
}

#pragma mark - Heating

-(BOOL) addProfile:(Profile *)profile{
    if (self.profiles == nil) return NO;
    [self.profiles addObject:profile];
    return YES;
}

-(BOOL) insertProfile:(Profile *)profile atIndex:(int)index{
    if (self.profiles == nil) return NO;
    [self.profiles insertObject:profile atIndex:index];
    return YES;
}


-(void) removeProfile:(Profile *)profile{
    if (self.profiles == nil) return;
    [self.profiles removeObject:profile];
}

- (Zone *)hasHeating {
    for (Zone *_zone in self.zones) {
        if ([_zone hasHeating])
            return _zone;
    }
    
    return nil;
}

#pragma mark -

-(BOOL) loadAndRefreshWFL:(BOOL)_refreshWFL {
    //Updates the arrays after data changes
    DB *db = [DB shared];
    
    if (db==nil) return NO;
    
    NSMutableArray *rows = [db getRows:[NSString stringWithFormat:@"SELECT * FROM zones WHERE accountId=%d ORDER BY displayId", [[UserPrefsHelper shared] getCurrentAccountId]]];
    _zones = [[NSMutableArray alloc] init];
    if (_zones == nil) return NO;
    
    for (NSMutableDictionary *dict in rows){
        int zoneId = [[dict objectForKey:@"zoneId"] intValue];
        NSString *name = [dict objectForKey:@"name"];
        Zone *zone = [[Zone alloc] initWithID:zoneId  name:name];
        [_zones addObject:zone];
    }
    
    [self checkAllOffMoodForZones];
    
    rows = [db getRows:[NSString stringWithFormat:@"SELECT * FROM events WHERE accountId=%d ORDER BY displayId", [[UserPrefsHelper shared] getCurrentAccountId]]];
    _events = [[NSMutableArray alloc] init];
    if (_events == nil) return NO;
    
    for (NSMutableDictionary *dict in rows){
        NSString *name = [dict objectForKey:@"name"];
        int idx = [[dict valueForKey:@"eventId"] intValue];
        NSString *_timerName = [dict objectForKey:@"timerName"];
        Event *event = [[Event alloc] initWithName:name andID:idx];
        event.timerName = _timerName;
        //DebugLog(@"Created Event: %@", _timerName);
        [_events addObject:event];
    }
    
    [self checkMandatoryEvents];

    rows = [db getRows:[NSString stringWithFormat:@"SELECT * FROM profiles WHERE accountId=%d", [[UserPrefsHelper shared] getCurrentAccountId]]];
    _profiles = [[NSMutableArray alloc] init];
    if (_profiles == nil) return NO;
    
    for (NSMutableDictionary *dict in rows){
        int profileId = [[dict valueForKey:@"profileId"] intValue];
        int type = [[dict valueForKey:@"type"] intValue];
        int zoneId = [[dict valueForKey:@"zoneId"] intValue];
        NSString *start = [dict objectForKey:@"start"];
        NSString *end = [dict objectForKey:@"end"];
        float temp = [[dict valueForKey:@"temp"] floatValue];
        Profile *profile = [[Profile alloc] initWithProfileId:profileId type:type zoneId:zoneId temp:temp start:start end:end];
        [_profiles addObject:profile];
    }

    rows = [db getRows:[NSString stringWithFormat:@"SELECT * FROM LTimers WHERE accountId=%d", [[UserPrefsHelper shared] getCurrentAccountId]]];
    _timers = [[NSMutableArray alloc] init];
    if (_timers == nil) return NO;
    for (NSMutableDictionary *dict in rows){
        //NSString *_timerName = [dict objectForKey:@"timerName"];
        NSString *_command = [dict objectForKey:@"command"];
        BOOL _isActive = [[dict valueForKey:@"active"] intValue];
        //DebugLog(@"Get Command: %@", _command);
        LTimer *_timer = [[LTimer alloc] initWithCommand:_command];
        _timer.active = _isActive;
        //DebugLog(@"Created Timer: %@, for Event: %@", _timer.name, _timer.event_name);

        if ((_timer.event_name == nil || [_timer.event_name isEqualToString:@""]) && (_timer.cmd == nil || [_timer.cmd isEqualToString:@""])) {
            // JUNK
            DebugLog(@"JUNK Timer Detected!");
        } else
            [_timers addObject:_timer];
    }
    
    [self updateZonesAndDevicesAfterLoadEvents];
    
    // Create Events & Timers on the WFL
    if ((_events.count > 0 || _timers.count > 0) && _refreshWFL) {
        WFLHelper *_wflHelper = [WFLHelper shared];
        [_wflHelper updateWFL];
    }
    
    return YES;
}

-(void)checkAllOffMoodForZones{
    for(Zone *zone in self.zones){
        if (![zone hasAllOff])
            [zone addAllOff];
    }
}

- (void) updateZonesAndDevicesAfterLoadEvents{
    //Go through all the events and see if they use Zones or Devices that do not exist in the current data
    BOOL zonesChanged = NO;
    
    for (Event *event in self.events){
        for (Action *action in event.actions){
            int zoneID = action.zoneID;
            if (zoneID==-1)
                continue;//We can't do anything with this, no Zone Id
            Zone *zone = [self getZoneByID:zoneID];
            if (zone==nil){
                zone = [[Zone alloc] initWithID:zoneID name:[NSString stringWithFormat:@"Zone%d", zoneID]];
                [self.zones addObject:zone];
                zonesChanged = YES;
            }
            if (zone!=nil){
                Device *device = action.device;
                //NSString *deviceName;
                
                if (device==nil){//Device wasn't found on initial load
                    if ([action isMood]){
                        int moodId = [action getMoodID];
                        //Mood *mood;
                        if (moodId==-1)
                            continue;
                        if (moodId==4){
                            //All off
                            [zone addAllOff];
                        }else{
                            [zone addMood:moodId];
                        }
                    }else{
                        int deviceID = action.deviceID;
                        if (deviceID==-1)
                            continue;//We can't do anything with this
                        
                        NSString *deviceType = [action getDeviceType];
                        device = [[Device alloc] initWithName:[NSString stringWithFormat:@"%@%d", NSLocalizedString(@"device", @""), deviceID] deviceID:deviceID type:deviceType zone:zone];
                        if (device!=nil)
                            [zone addDevice:device andSave:YES];
                    }
                }
            }
        }
    }
    
    if (zonesChanged)
        [self saveData:@"Zones"];
}

- (void) checkMandatoryEvents{
    Event *event;
    
    event = [self getEventByID:0];
    int idx;
        
    if (event==nil){
        [self addEventNamed:NSLocalizedString(@"home", @"home") andID:0];
    }else if (![event.name isEqualToString:NSLocalizedString(@"home", @"home")]){
        event.name = NSLocalizedString(@"home", @"home");
        idx = [self.events indexOfObject:event];
        [event saveToDB:idx];
    }
    
    event = [self getEventByID:1];
    if (event==nil){
        [self addEventNamed:NSLocalizedString(@"away", @"away") andID:1];
    }else if (![event.name isEqualToString:NSLocalizedString(@"away", @"away")]){
        event.name = NSLocalizedString(@"away", @"away");
        idx = [self.events indexOfObject:event];
        [event saveToDB:idx];
    }
}

-(BOOL)save{
    return YES;
}

-(BOOL)addEventNamed:(NSString *)name{
    Event *event;
    
    int eventID = [self getFreeEventID];

    event = [[Event alloc] initWithName:name andID:eventID];
    
    if (event!=nil){
        [self.events addObject:event];
        [event saveToDB:([self.events count] - 1)];
    }
    
    return YES;
}

-(BOOL)addEventNamed:(NSString *)name andID:(int)eventID{
    Event *event;
    
    event = [[Event alloc] initWithName:name andID:eventID];
    
    if (event!=nil){
        [self.events addObject:event];
        [event saveToDB:([self.events count] - 1)];
    }
    
    return YES;
}

- (int)getFreeEventID {
    int _freeID = 0;
    
    for (Event *_event in self.events) {
        if (_event.eventID > _freeID) {
            _freeID = _event.eventID;
        }
    }
    
    _freeID += 1;
    
    return _freeID;
}
                
-(BOOL)saveData:(NSString *)table{
    DB *db = [DB shared];
    if (db == nil) return NO;
    NSString *sql;
    int i=0;
    
    if ([table isEqualToString:@"Zones"]){
        sql = [NSString stringWithFormat:@"DELETE FROM zones WHERE accountId=%d", [[UserPrefsHelper shared] getCurrentAccountId]];
        [db sqlExec:sql];
        for(Zone *zone in self.zones){
            sql = [NSString stringWithFormat:@"INSERT INTO zones (name, zoneId, displayId, accountId) VALUES(\'%@\', %d, %d, %d)", zone.name, zone.zoneID, i, [[UserPrefsHelper shared] getCurrentAccountId]];
            [db sqlExec:sql];
            i++;
        }

        /*
        If you delete a device then delete any Action that uses this device. 
         It is easy to build the RxDy that is a match to hwKey. 
         And then simply SELECT * FROM actions WHERE hwKey='RxDy'. 
         Here x and y are zoneId and deviceId for the Device that is being deleted. 
         If this returns something then you have an Action that needs deleting.
            */
        
        NSMutableArray *rows = [db getRows:[NSString stringWithFormat:@"SELECT * FROM devices WHERE accountId=%d AND zoneId NOT IN (SELECT zoneId FROM zones WHERE accountId=%d)", [[UserPrefsHelper shared] getCurrentAccountId], [[UserPrefsHelper shared] getCurrentAccountId]]];
        for (NSMutableDictionary *dict in rows){
            int deviceID = [[dict valueForKey:@"deviceId"] intValue];
            int zoneId = [[dict valueForKey:@"zoneId"] intValue];
            
            NSString *_hwKey = [NSString stringWithFormat:@"R%dD%d", zoneId, deviceID];
            sql = [NSString stringWithFormat:@"DELETE FROM actions WHERE accountId=%d AND hwKey='%@'", [[UserPrefsHelper shared] getCurrentAccountId], _hwKey];
            [db sqlExec:sql];
        }
        
        sql = [NSString stringWithFormat:@"DELETE FROM devices WHERE accountId=%d AND zoneId NOT IN (SELECT zoneId FROM zones WHERE accountId=%d)", [[UserPrefsHelper shared] getCurrentAccountId], [[UserPrefsHelper shared] getCurrentAccountId]];
        [db sqlExec:sql];
        
        //sql = @"DELETE FROM actions WHERE deviceId NOT IN (SELECT id FROM devices)";
        //[db sqlExec:sql];
        
        [self checkAllOffMoodForZones];
        
    }else if([table isEqualToString:@"Events"]){
        sql = [NSString stringWithFormat:@"DELETE FROM events WHERE accountId=%d", [[UserPrefsHelper shared] getCurrentAccountId]];
        [db sqlExec:sql];
        for(Event *event in self.events){
            sql = [NSString stringWithFormat:@"INSERT INTO events (name, eventId, displayId, timerName, accountId) VALUES (\'%@\', %d, %d, \'%@\', %d)", event.name, event.eventID, i, event.timerName, [[UserPrefsHelper shared] getCurrentAccountId]];
            DebugLog(@"SQL: %@", sql);
            [db sqlExec:sql];
            [event saveActionsToDB];
            i++;
        }
    }
    //NSString *sql = [NSString stringWithFormat:@""]
    return YES;
}

-(BOOL)saveZones:(BOOL)withDevices{
    DB *db = [DB shared];
    if (db == nil) return NO;
    
    NSString *sql;
    sql = [NSString stringWithFormat:@"DELETE FROM zones WHERE accountId=%d", [[UserPrefsHelper shared] getCurrentAccountId]];
    [db sqlExec:sql];
    int i=0;
    for(Zone *zone in self.zones){
        sql = [NSString stringWithFormat:@"INSERT INTO zones (name, zoneId, displayId, accountId) VALUES (\'%@\', %d, %d, %d)", zone.name, zone.zoneID, i, [[UserPrefsHelper shared] getCurrentAccountId]];
        [db sqlExec:sql];
        if (withDevices)
            [zone saveDevicesToDB];
        i++;
    }
    
    [self checkAllOffMoodForZones];
    
    return YES;
}

-(BOOL)saveEvents:(BOOL)withActions{
    DB *db = [DB shared];
    if (db == nil)
        return NO;
    
    NSString *sql;
    sql = [NSString stringWithFormat:@"DELETE FROM events WHERE accountId=%d", [[UserPrefsHelper shared] getCurrentAccountId]];
    [db sqlExec:sql];
    int i=0;
    for(Event *event in self.events){
        sql = [NSString stringWithFormat:@"INSERT INTO events (name, eventId, displayId, timerName, accountId) VALUES (\'%@\', %d, %d, \'%@\', %d)", event.name, event.eventID, i, event.timerName, [[UserPrefsHelper shared] getCurrentAccountId]];
        [db sqlExec:sql];
        if (withActions)
            [event saveActionsToDB];
        i++;
    }
    
    return YES;
}

-(BOOL)saveTimers {
    DB *db = [DB shared];
    if (db == nil) return NO;
    
    NSString *sql;
    sql = [NSString stringWithFormat:@"DELETE FROM LTimers WHERE accountId=%d", [[UserPrefsHelper shared] getCurrentAccountId]];
    [db sqlExec:sql];
    int i=0;
    for(LTimer *_timer in self.timers){
        sql = [NSString stringWithFormat:@"INSERT INTO LTimers (timerId, timerName, command, active, accountId) VALUES (%d, \'%@\', \'%@\', %d, %d)",
               i,
               _timer.name,
               [_timer getCommand],
               _timer.active,
               [[UserPrefsHelper shared] getCurrentAccountId]];
        [db sqlExec:sql];
        i++;
    }
    
    return YES;
}

- (void)saveAll {
    [self saveZones:YES];
    
    for (Zone *_zone in self.zones) {
        [_zone saveDevicesToDB];
    }
    
    [self saveEvents:YES];
    [self saveTimers];
    [self saveProfiles];
}

-(BOOL)saveProfiles {
    DB *db = [DB shared];
    if (db == nil) return NO;
    
    NSString *sql;
    sql = [NSString stringWithFormat:@"DELETE FROM profiles WHERE accountId=%d", [[UserPrefsHelper shared] getCurrentAccountId]];
    [db sqlExec:sql];
    int i=0;
    for(Profile *_profile in self.profiles){
        sql = [NSString stringWithFormat:@"INSERT INTO profiles (profileId, type, zoneId, temp, start, end, accountId) VALUES ( %d, %d, %d, %3.1f, \'%@\', \'%@\', %d)", _profile.profileID, _profile.type, _profile.zoneId, _profile.temp, _profile.start, _profile.end, [[UserPrefsHelper shared] getCurrentAccountId]];
        [db sqlExec:sql];
        i++;
    }
    
    return YES;
}

-(BOOL)saveAccounts {    
    DB *db = [DB shared];
    if (db == nil)
        return NO;
    
    NSString *sql;
    sql = [NSString stringWithFormat:@"DELETE FROM accounts"];
    [db sqlExec:sql];
    //int i=0;
    
    [self.accounts sortArrayOfDictionarysWithKey:@"accountId" ascending:YES];
    
    for(Account *_account in self.accounts){

//        sql = [NSString stringWithFormat:@"INSERT INTO accounts (id, emailPwd, wifiPin, mac, email, password, name, location, timezone, ssid, elecrate, impkwh) VALUES (%d, \'%@\', \'%@\', \'%@\', \'%@\', \'%@\', \'%@\', \'%@\', \'%@\', \'%@\', %f, %d)",
//               _account.accountId,//i,
//               _account.emailPwd,
//               _account.wifiPin,
//               _account.mac,
//               _account.email,
//               _account.password,
//               _account.name,
//               _account.location,
//               _account.timezone,
//               _account.ssid,
//               _account.elecrate,
//               _account.impkwh];
        
        sql = [NSString stringWithFormat:@"INSERT INTO accounts (emailPwd, wifiPin, mac, email, password, name, location, timezone, ssid, elecrate, impkwh) VALUES (\'%@\', \'%@\', \'%@\', \'%@\', \'%@\', \'%@\', \'%@\', \'%@\', \'%@\', %f, %d)",
               _account.emailPwd,
               _account.wifiPin,
               _account.mac,
               _account.email,
               _account.password,
               _account.name,
               _account.location,
               _account.timezone,
               _account.ssid,
               _account.elecrate,
               _account.impkwh];
        
        //DebugLog(@"\n\nSaving: %@\n\n", sql);
        [db sqlExec:sql];
        //i++;
    }
    
    return YES;
}

-(BOOL)loadAccounts{
    DB *db = [DB shared];
    
    if (db==nil)
        return NO;
    
    NSMutableArray *rows = [db getRows:[NSString stringWithFormat:@"SELECT * FROM accounts"]];
    _accounts = [[NSMutableArray alloc] init];
    
    if (_accounts == nil)
        return NO;
    
    for (NSMutableDictionary *dict in rows){
        int idx = [[dict valueForKey:@"id"] intValue];
        NSString *_email = [dict objectForKey:@"email"];
        NSString *_password = [dict objectForKey:@"password"];
        
        Account *_account = [[Account alloc] initWithEmail:_email password:_password andId:idx];
        _account.mac = [dict objectForKey:@"mac"];
        _account.emailPwd = [dict objectForKey:@"emailPwd"];
        _account.wifiPin = [dict objectForKey:@"wifiPin"];
        _account.name = [dict objectForKey:@"name"];
        _account.location = [dict objectForKey:@"location"];
        _account.ssid = [dict objectForKey:@"ssid"];
        _account.timezone = [dict objectForKey:@"timezone"];
        _account.elecrate = (double)[[dict objectForKey:@"elecrate"] floatValue];
        _account.impkwh = [[dict objectForKey:@"impkwh"] intValue];
        //DebugLog(@"Created Account: %@", _account);
        [_accounts addObject:_account];
    }
    
    return YES;
}

-(Device *)getDeviceFromHardwareKey:(NSString *)hwKey{
    
    //DebugLog(@"Cmd: %@", hwKey);
    
    //
    // Check for | messages sent in a command
    //
    NSArray *_tokens = [hwKey componentsSeparatedByString:@"|"];
    if (_tokens.count > 0) {
        hwKey = [_tokens objectAtIndex:0];
    }
    
    //
    // Check for , sent in a command
    //
    _tokens = [hwKey componentsSeparatedByString:@","];
    if (_tokens.count > 0) {
        hwKey = [_tokens objectAtIndex:0];
    }
    //
    //
    //
        
    if ([hwKey characterAtIndex:0] == '!'){
        hwKey = [hwKey substringFromIndex:2];
    }else{
        hwKey = [hwKey substringFromIndex:1];
    }
        
    int fpos = [hwKey rangeOfString:@"F"].location;
    int dpos = [hwKey rangeOfString:@"D"].location;
    
    int zoneID = -1;
    int deviceID = -1;
    Zone *zone;
    Device *device;
    
    if (dpos == NSNotFound){
        if (fpos == NSNotFound)
            return nil;
        zoneID = [[hwKey substringToIndex:fpos] intValue];
        hwKey = [hwKey substringFromIndex:fpos + 1];

    }else{
        zoneID = [[hwKey substringToIndex:dpos] intValue];
                
        hwKey = [hwKey substringFromIndex:dpos + 1];
        
        fpos = [hwKey rangeOfString:@"F"].location;
        if (fpos != NSNotFound) {
            NSString *_tmpStr = [hwKey substringToIndex:fpos];
            deviceID = [_tmpStr intValue];
        }
    }

    zone = [self getZoneByID:zoneID];
    if (zone == nil)
        return nil;
    
    if (deviceID==-1){
        int moodID=-1;
        BOOL allOff = NO;
                
        int pos = [hwKey rangeOfString:@"m"].location;
        if (pos != NSNotFound){
            moodID = [[hwKey substringFromIndex:(pos+2)] intValue];
        } else {
            pos = [hwKey rangeOfString:@"a"].location;
            allOff = (pos!=NSNotFound);
        }
        
        if (moodID==-1 && !allOff)
            return nil;
        
        if (allOff)
            moodID = 4;
        
        device = [zone getDeviceByMoodID:moodID];
    }else{
        device = [zone getDeviceByID:deviceID];
    }
    
    return device;
}

- (Event *)getEventByName:(NSString *)_name {
    Event *event = nil;
    
    for(Event *e in self.events){
        if ([e.name isEqualToString:_name]){
            event = e;
            break;
        }
    }
    
    return event;
}

-(Event *)getEventByID:(int)eventID{
    Event *event = nil;
    
    for(Event *e in self.events){
        if (e.eventID == eventID){
            event = e;
            break;
        }
    }
    
    return event;
}

-(Zone *)getZoneByID:(int)zoneID{
    Zone *zone = nil;
    
    for(Zone *z in self.zones){
        if (z.zoneID == zoneID){
            zone = z;
            break;
        }
    }
    
    return zone;
}

-(LTimer *)getTimerByName:(NSString *)_timerName {
    for (LTimer *_timer in self.timers) {
        if ([_timer.name isEqualToString:_timerName])
            return _timer;
    }
    
    return nil;
}

-(LTimer *)getTimerByEventName:(NSString *)_eventName {
    for (LTimer *_timer in self.timers) {
        if ([_timer.event_name isEqualToString:_eventName])
            return _timer;
    }
    
    return nil;
}

- (NSMutableArray *)getProfilesForZoneID:(int)zoneID {
    NSString *_propertyToSearch = @"zoneId";
    NSPredicate *_predicate = [NSPredicate predicateWithFormat:@"%K == %d", _propertyToSearch, zoneID];
    NSMutableArray *_foundProfilesArray = [[self.profiles filteredArrayUsingPredicate:_predicate] mutableCopy];
    return _foundProfilesArray;
}

- (NSMutableArray *)getProfilesForType:(int)type {
    NSString *_propertyToSearch = @"type";
    NSPredicate *_predicate = [NSPredicate predicateWithFormat:@"%K == %d", _propertyToSearch, type];
    NSMutableArray *_foundProfilesArray = [[self.profiles filteredArrayUsingPredicate:_predicate] mutableCopy];
    return _foundProfilesArray;
}

- (NSMutableArray *)getProfilesForZoneID:(int)zoneID type:(int)type {
    NSMutableArray *predicatesArray = [[NSMutableArray alloc] init];
    
    NSString *_propertyToSearch1 = @"zoneId";
    NSPredicate *_predicate1 = [NSPredicate predicateWithFormat:@"%K == %d", _propertyToSearch1, zoneID];
    [predicatesArray addObject:_predicate1];
    
    NSString *_propertyToSearch2 = @"type";
    NSPredicate *_predicate2 = [NSPredicate predicateWithFormat:@"%K == %d", _propertyToSearch2, type];
    [predicatesArray addObject:_predicate2];
    
    NSPredicate *_predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicatesArray];
    NSMutableArray *_foundProfilesArray = [[self.profiles filteredArrayUsingPredicate:_predicate] mutableCopy];
    return _foundProfilesArray;
}

-(void)dealloc{
    if (_zones != nil){
        [_zones removeAllObjects];
    }
    
    if (_events != nil){
        [_events removeAllObjects];
    }
    
    if (_profiles != nil){
        [_profiles removeAllObjects];
    }
    
    if (_timers != nil){
        [_timers removeAllObjects];
    }
    
    if (_accounts != nil){
        [_accounts removeAllObjects];
    }
}

@end
