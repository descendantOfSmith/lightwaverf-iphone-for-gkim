//
//  EnergyVC.h
//  LightwaveRF
//
//  Created by Nik Lever on 03/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "EnergyDialView.h"
#import "Energy.h"
#import "EnergyHelper.h"

typedef enum
{
    kEnergyKWH = 1001,
    kEnergyCPH,
    kEnergyCFYT
} EnergyViewTags;

typedef enum
{
    kEnergySelectedKWH = 1001,
    kEnergySelectedCost
} EnergyCostDialToggles;

@interface EnergyVC : RootViewController <EnergyDialViewDataSource, UIGestureRecognizerDelegate> {
    
}

@property (strong, nonatomic) IBOutlet UINavigationBar *nav_bar;

@end
