//
//  IntroVC.m
//  LightwaveRF
//
//  Created by Nik Lever on 05/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "IntroVC.h"
#import "IntroPageVC.h"
#import "AppDelegate.h"
#import "LoginVC.h"
#import "ButtonsHelper.h"
#import "AppStyleHelper.h"

@interface IntroVC ()
@property (strong, nonatomic) UIButton *skip_btn;
@end

@implementation IntroVC

@synthesize scrollView;
@synthesize pageControl;
@synthesize viewControllers;
@synthesize skip_btn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([AppStyleHelper isMegaMan]) {
        self.logo_img.hidden = YES;
        [self.pageControl setPageIndicatorTintColor:[UIColor blackColor]];
        [self.pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithDivisionOfRed:137 green:197 blue:39 alpha:1.0f]];
    }
    
    [self setupUI];
    
    // Do any additional setup after loading the view from its nib.
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < PAGECOUNT; i++) {
        [controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * PAGECOUNT, self.scrollView.frame.size.height);
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = [UIColor clearColor];
    
    self.pageControl.numberOfPages = PAGECOUNT;
    self.pageControl.currentPage = 0;
    
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
    int height = [[UIScreen mainScreen] bounds].size.height;
    CGRect frame = _btnbg_img.frame;
    int newY = height - 40;
    frame.origin.y = newY;
    _btnbg_img.frame = frame;
    frame = self.skip_btn.frame;
    frame.origin.y = newY + 3;
    self.skip_btn.frame = frame;
    
    if (height<500){
        frame = self.scrollView.frame;
        frame.origin.y -= 20;
        self.scrollView.frame = frame;
        frame = self.pageControl.frame;
        frame.origin.y = newY - 34;
        self.pageControl.frame = frame;
    }else{
        frame = self.scrollView.frame;
        frame.origin.y += 20;
        self.scrollView.frame = frame;
        frame = self.pageControl.frame;
        frame.origin.y -= 27;
        self.pageControl.frame = frame;
        frame = _logo_img.frame;
        frame.origin.y += 27;
        _logo_img.frame = frame;
    }
}

- (void)setupUI {
    [_btnbg_img setBackgroundColor:[AppStyleHelper getNavigationBarTintColor]];
    
    [self setupButtons];
}

- (void)setupButtons {
    self.skip_btn = [ButtonsHelper getButtonWithText:NSLocalizedString(@"skip", @"skip")
                                              type:kButtonGreenBacking
                                               tag:kButtonSkip
                                             width:58
                                            height:33
                                            target:self
                                          selector:@selector(skipPressed:)];
    [self.skip_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.skip_btn setFrameX:254];
    [self.skip_btn setFrameY:527];
    [self.view addSubview:self.skip_btn];
}

- (void)loadScrollViewWithPage:(int)page {
    if (page < 0 || page >= PAGECOUNT) return;
    
    IntroPageVC *controller = [viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null]) {
        controller = [[IntroPageVC alloc] initWithPageNumber:page];
        [viewControllers replaceObjectAtIndex:page withObject:controller];
    }
    
    if (nil == controller.view.superview) {
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [scrollView addSubview:controller.view];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (pageControlUsed) {
        return;
    }
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
    
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    CGRect bounds = self.skip_btn.bounds;
    
    if (page==3){
        [self.skip_btn setTitle:NSLocalizedString(@"done", @"done") forState:UIControlStateNormal];
    }else{
        [self.skip_btn setTitle:NSLocalizedString(@"skip", @"skip") forState:UIControlStateNormal];
    }
    
    self.skip_btn.bounds = bounds;
    self.skip_btn.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

- (IBAction)changePage:(id)sender {
    int page = pageControl.currentPage;
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
    pageControlUsed = YES;
    
    CGRect bounds = self.skip_btn.bounds;

    if (page==3){
        [self.skip_btn setTitle:NSLocalizedString(@"done", @"done") forState:UIControlStateNormal];
    }else{
        [self.skip_btn setTitle:NSLocalizedString(@"skip", @"skip") forState:UIControlStateNormal];
    }
    
    self.skip_btn.bounds = bounds;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)skipPressed:(id)sender {
    [self.view removeFromSuperview];
    
    if (kApplicationDelegate.loginController) {
        kApplicationDelegate.loginController.email_txt.text = [[UserPrefsHelper shared] getUserEmail];
        kApplicationDelegate.loginController.password_txt.text = [[UserPrefsHelper shared] getUserPassword];
        [kApplicationDelegate.loginController.login_btn sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
