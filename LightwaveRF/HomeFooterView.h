//
//  HomeFooterView.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 24/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeFooterViewDelegate;

@interface HomeFooterView : UIView {
    
}

@property (nonatomic, assign) id <HomeFooterViewDelegate> delegate;
@property (nonatomic, strong) UIButton *selectedButton;

- (id)initWithPoint:(CGPoint)point;

+ (float)getViewHeight;

@end

@protocol HomeFooterViewDelegate <NSObject>

@optional

- (void)switchLocationPressed:(HomeFooterView *)_homeFooterView;

@end
