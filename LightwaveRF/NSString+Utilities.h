//
//  NSString+Utilities.h
//  TemplateARC
//
//  Created by imac on 13/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (Utilities)

- (BOOL)isEmpty;
- (BOOL)containsString:(NSString *)string;
- (BOOL)containsString:(NSString *)string options:(NSStringCompareOptions)options;
- (BOOL)isURL;
- (BOOL)isValidEmail;
- (NSString*)stringBetweenString:(NSString*)start andString:(NSString*)end;
- (NSString *)repeatStringNumberOfTimes:(NSUInteger)times;
- (NSString *)reverseString;
- (NSString *)inverseCapitalisationString;
- (NSString *)removeNumbersFromString;
- (NSString *)stringByTrimmingPrefix:(NSString *)strPrefix;
- (NSString *)stringByTrimmingLeadingWhitespace;
- (NSString *)getSpelledOutNumber:(NSInteger)num; // e.g. one, two, three
- (NSString *)removeLastCharOfString;
- (NSString *)getSpelledOutOrdinalNumber:(NSInteger)num; // e.g. first, second, third
- (void)getPhoneNumber;

@end
