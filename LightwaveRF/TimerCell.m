//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import "TimerCell.h"
#import "ButtonsHelper.h"

@interface TimerCell ()
@property (nonatomic, strong) UIView *containingView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *zoneLabel;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *detailLabel;
@end

@implementation TimerCell

@synthesize containingView, iconImageView, titleLabel, detailLabel, zoneLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
    }
    return self;
}

- (void)dealloc {
    
}

+ (float)getCellHeight {
    return 44.0f;
}

- (void)setupContainingView {
    /*************************************
     *  Containing View
     *************************************/
    if (!self.containingView) {
        self.containingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                                       0.0f,
                                                                       self.contentView.frame.size.width,
                                                                       [TimerCell getCellHeight])];
        [self.containingView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.containingView];
    }
}

- (void)setupCellWithTitle:(NSString*)tmpTitle
                  zoneName:(NSString *)_zoneName
                detailText:(NSString *)tmpDetailText
                 iconImage:(UIImage *)_iconImage {
    
    [self setupContainingView];

    /*************************************
     *  Icon
     *************************************/
    if (!self.iconImageView) {
        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,
                                                                           0.0f,
                                                                           36.0f,
                                                                           36.0f)];
        [self.iconImageView setContentMode:UIViewContentModeScaleAspectFit];
        self.iconImageView.image = _iconImage;
        [self.containingView addSubview:self.iconImageView];
    }

    /*************************************
     *  Title Label
     *************************************/
    if (!self.titleLabel) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 18.0f)];
        [self.titleLabel setText:tmpTitle];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:[UIColor blackColor]];
        [self.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0f]];
        [self.titleLabel setAdjustsFontSizeToFitWidth:YES];
        [self.titleLabel setMinimumFontSize:9.0f];
        [self.containingView addSubview:self.titleLabel];
    } else {
        [self.titleLabel setText:tmpTitle];
    }
    
    /*************************************
     *  Zone Label
     *************************************/
    if (!self.zoneLabel) {
        self.zoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 18.0f)];
        [self.zoneLabel setText:_zoneName];
        [self.zoneLabel setBackgroundColor:[UIColor clearColor]];
        [self.zoneLabel setTextColor:[UIColor lightGrayColor]];
        [self.zoneLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        [self.zoneLabel setAdjustsFontSizeToFitWidth:YES];
        [self.zoneLabel setMinimumFontSize:9.0f];
        [self.containingView addSubview:self.zoneLabel];
    } else {
        [self.zoneLabel setText:_zoneName];
    }
    
    /*************************************
     *  Detail Label
     *************************************/
    if (!self.detailLabel) {
        self.detailLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.detailLabel setNumberOfLines:2];
        [self.detailLabel setBackgroundColor:[UIColor clearColor]];
        [self.detailLabel setTextColor:[UIColor lightGrayColor]];
        [self.detailLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        [self.containingView addSubview:self.detailLabel];
    }
    
    if (![tmpDetailText isEqualToString:self.detailLabel.text]) {
        [self.detailLabel setText:tmpDetailText];
        CGSize _size = [tmpDetailText sizeWithFont:self.detailLabel.font
                                 constrainedToSize:CGSizeMake(135.0f, CGFLOAT_MAX)
                                     lineBreakMode:NSLineBreakByWordWrapping];
        [self.detailLabel setFrameWidth:_size.width];
        [self.detailLabel setFrameHeight:_size.height];
    }
    
    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    
    // Icon Image
    [self.iconImageView setFrameX:tmpIsEditing ? 45.0f : 5.0f];
    [self.iconImageView setCenterY:self.containingView.center.y];
    
    // Title & Zone Name
    if (self.zoneLabel.text.length > 0) {
        [self.zoneLabel setHidden:NO];
        
        // Title
        [self.titleLabel alignToRightOfView:self.iconImageView padding:5.0f matchVertical:YES];
        [self.zoneLabel alignToBottomOfView:self.titleLabel padding:0.0f matchHorizontal:YES];
        
    } else {
        [self.zoneLabel setHidden:YES];
        
        // Title
        [self.titleLabel alignToRightOfView:self.iconImageView padding:5.0f];
        [self.titleLabel setCenterY:self.containingView.center.y];
    }
    
    // Detail
    [self.detailLabel alignToRightOfView:self.titleLabel padding:5.0f];
    [self.detailLabel setCenterY:self.containingView.center.y];
    [self.detailLabel setAlpha:tmpIsEditing ? 0.0f : 1.0f];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self positionViewsIsEditing:self.isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

@end
