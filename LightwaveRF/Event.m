//
//  Event.m
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 25/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import "Event.h"
#import "Action.h"
#import "Device.h"
#import "Zone.h"
#import "Home.h"
#import "DB.h"
#import "NSDate+Utilities.h"
#import "WFLHelper.h"

@implementation Event

@synthesize name = _name;
@synthesize actions = _actions;
@synthesize eventID = _idx;
@synthesize timerName = _timerName;
@synthesize createdOnWFL = _createdonWFL;

-(id) initWithName:(NSString *)name actions:(NSMutableArray *)actions
{
    if (self = [super init])
    {
        self.createdOnWFL = NO;
        _name = name;
        _actions = actions;
    }
    return self;
}

-(id) initWithName:(NSString *)name andID:(int)eventID
{
    self.createdOnWFL = NO;
    self.eventID = eventID;
    _actions = [[NSMutableArray alloc] init];
    
    DB *db = [DB shared];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM ACTIONS WHERE accountId=%d AND eventId=%d", [[UserPrefsHelper shared] getCurrentAccountId], eventID];
    NSArray *rows = [db getRows:sql];
    //Home *home = [Home shared];
    
    for(NSDictionary *dict in rows){
        NSString *hwKey = [dict objectForKey:@"hwKey"];
        NSString *delay = [dict objectForKey:@"delay"];
        Action *action;
        action = [[Action alloc] initWithHardwareKey:hwKey delay:delay];
        if (action!=nil)
            [_actions addObject:action];
        //DebugLog(@"Action %@, %@ - %@ created by Event %@\n\n", hwKey, delay, [action shortString], name);
    }
    
    return [self initWithName:name actions:self.actions];
}

-(Action *) addActionForDevice:(Device *)device setting:(int)setting
{
    //DebugLog(@"Add device hardware key = %@", device.hardwareKey);
    Action *action = [[Action alloc] initWithDevice:device setting:setting];
    [self.actions addObject:action];
    return action;
}

#pragma mark - Actions

-(BOOL) addAction:(Action *)action{
    if (self.actions == nil) return NO;
    [self.actions addObject:action];
    return YES;
}

-(BOOL) insertAction:(Action *)action atIndex:(int)index{
    if (self.actions == nil) return NO;
    [self.actions insertObject:action atIndex:index];
    return YES;
}

-(void) removeAction:(Action *)action{
    if (self.actions == nil) return;
    [self.actions removeObject:action];
}

- (BOOL)saveActionsToDB {
    DB *db = [DB shared];
    [db sqlExec:[NSString stringWithFormat:@"DELETE FROM actions WHERE accountId=%d AND eventId=%i", [[UserPrefsHelper shared] getCurrentAccountId], self.eventID]];
    
    int i=0;
    for(Action *action in self.actions){
        NSString *sql = nil;
        
        if (action.device!=nil)
            [action updateHwKey];
        
       //DebugLog(@"Event ID = %d hwKey = %@ delay = %@ displayID = %d", self.eventID, action.hwKey, action.delay, i);
        
        sql = [NSString stringWithFormat:@"INSERT INTO actions (eventId, hwKey, delay, displayId, accountId) VALUES (%d, \'%@\', \'%@\', %d, %d)",
                   self.eventID, action.hwKey, action.delay, i, [[UserPrefsHelper shared] getCurrentAccountId]];
        
        DebugLog(@"Event saveActionsToDB %@", sql);
        
        [db sqlExec:sql];
        
        i++;
    }

    return YES;    
}

- (NSString *)getTimeString:(NSDate *)_date {
    NSInteger _year = [[_date yearNumber] intValue];
    NSInteger _month = [[_date monthNumber] intValue];
    NSInteger _day = [[_date dayNumber] intValue];
    NSInteger _hours = [[_date hourNumber] intValue];
    NSInteger _mins = [[_date minuteNumber] intValue];
    
    NSString *_result = nil;

    if ([_date isToday]) {
        _result = [NSString stringWithFormat:@"T%02d:%02d", _hours, _mins];
        
    } else {
        _result = [NSString stringWithFormat:@"T%02d:%02d,S%02d/%02d/%02d", _hours, _mins, _day, _month, _year - 2000];
        
    }
    
    return _result;
}

-(NSString *) triggerEvent:(NSDate *)_delay
{
    NSString *_delayStr = nil;
    NSString *_commandString = nil;
    
    if (_delay) {
        
        _delayStr = [self getTimeString:_delay];
        
        NSString *_newTimerName = [LTimer generateTimerName];
        
        _commandString = [NSString stringWithFormat:@"!FiP\"%@\"=!FqP\"%@\",%@", _newTimerName, self.name, _delayStr];
        
    } else {
        _commandString = [NSString stringWithFormat:@"!FqP\"%@\"|Start Sequence|", self.name];
    }
    
    //DebugLog(@"Event triggered with command string: %@", _commandString);
    
    return _commandString;
}

-(NSString *) sumDelays:(NSString *)delay1 delay2:(NSString *)delay2
{
    NSArray *tokens1 = [delay1 componentsSeparatedByString:@":"];
    NSArray *tokens2 = [delay2 componentsSeparatedByString:@":"];
    if ([tokens1 count] !=3 || [tokens2 count] != 3) return @"";
    int values[] =
    {
        [[tokens1 objectAtIndex:0] intValue] + [[tokens2 objectAtIndex:0] intValue],
        [[tokens1 objectAtIndex:1] intValue] + [[tokens2 objectAtIndex:1] intValue],
        [[tokens1 objectAtIndex:2] intValue] + [[tokens2 objectAtIndex:2] intValue]
    };
    while (values[2] > 59)
    {
        values[2] -= 60;
        values[1]++;
    }
    while (values[1] > 60)
    {
        values[1] -= 60;
        values[0]++;
    }
    while (values[0] > 23)
    {
        values[0] = 23;
    }
    return [NSString stringWithFormat:@"%02d:%02d:%02d", values[0], values[1], values[2]];
}

-(void) createEvent {
    NSString *_command = [self getCommand];
    
    if (_command) {
        DebugLog(@"Create Event with Command: %@", _command);
        
        WFLHelper *_wflHelper = [WFLHelper shared];
        [_wflHelper sendToUDP:YES command:_command tag:kRequestCreateEvent];
        
    } else {
        DebugLog(@"Event not created on WFL... no actions found");
    }
}

-(void) cancelEvent
{
    
    NSString *message = [NSString stringWithFormat:@"!FcP\"%@\"|%@|%@",
                         self.name,
                         NSLocalizedString(@"cancel_sequence", @"Cancel Sequence"),
                         self.name];
    
    //DebugLog(@"Command: %@", message);
    DebugLog(@"Cancel Event with Command: %@", message);
    WFLHelper *_wflHelper = [WFLHelper shared];
    [_wflHelper sendToUDP:YES command:message tag:kRequestCancelEvent];
}

-(void) deleteEvent
{
    NSString *message = [NSString stringWithFormat:@"!FxP\"%@\"|%@|%@",
                         self.name,
                         NSLocalizedString(@"deleting_sequence", @"Deleting Sequence"),
                         self.name];
    //DebugLog(@"Command: %@", message);
    DebugLog(@"Delete Event with Command: %@", message);
    WFLHelper *_wflHelper = [WFLHelper shared];
    [_wflHelper sendToUDP:YES command:message tag:kRequestDeleteEvent];
}

-(void) clearActions
{
    [self.actions removeAllObjects];
}

-(void) deleteActionAt:(int) position
{
    if ([self.actions count] > position)
        [self.actions removeObjectAtIndex:position];
}

- (void)deleteActions {
    DB *db = [DB shared];
    [db sqlExec:[NSString stringWithFormat:@"DELETE FROM actions WHERE accountId=%d AND eventId=%i", [[UserPrefsHelper shared] getCurrentAccountId], self.eventID]];
    
    [self.actions removeAllObjects];
}

-(void) renameFrom:(NSString *) fromName to:(NSString *) toName
{
    if ([fromName isEqualToString:self.name]) self.name = toName;
}

-(int)getActionCount{
    if (self.actions == nil) return 0;
    return [self.actions count];
}

/*
-(BOOL) includesDevice:(Device *) device {
    
}

-(int) getSettingForDevice:(Device *) device {
    
}

-(void) removeDevice:(Device *) device {
    
}
 */

-(NSDictionary *) toDictionary
{
    NSMutableDictionary *eventDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.name, @"name", nil];
    NSMutableArray *eventActions = [[NSMutableArray alloc] init];
    for (Action *action in self.actions)
    {
        [eventActions addObject:[action toDictionary]];
    }
    [eventDictionary setValue:eventActions forKey:@"actions"];
    return eventDictionary;
}

-(NSData *) toJSON
{
    NSError *error = nil;
    NSMutableDictionary *jsonEvent = [self.toDictionary mutableCopy];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonEvent options:NSJSONWritingPrettyPrinted error:&error];
    if (!error)
    {
        DebugLog(@"JSON event data:\n%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
        return jsonData;
    }
    else
    {
        return nil;
    }
}

-(NSArray *) toArray{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    [array addObject:self.name];
    
    for (int i=0; i < [self.actions count]; i++) {
        Action *action = [self.actions objectAtIndex:i];
        NSString *delay = action.delay;
        if ([delay isEqualToString:@""]) delay = @"00:00:00";
        NSString *command = [NSString stringWithFormat:@"%@,%@", action.hwKey, delay];
        [array addObject:command];
    }
    
    return array;
}

- (NSString *)getCommand {
    NSMutableArray *_commands = [self getActionBreakdown];
    if (_commands.count > 0) {
        NSMutableString *_command = [NSMutableString string];
        [_command appendFormat:@"!FeP\"%@\"%@", self.name, _commands ? @"=" : @""];
        
        int n = 0;
        for (NSString *_str in _commands) {
            
            if (n>0)
                [_command appendString:@","];
            
            [_command appendString:_str];
            
            n++;
        }
        
        return _command;
    } else
        return nil;
}

- (NSMutableArray *)getActionBreakdown {
    NSMutableArray *commands = [[NSMutableArray alloc] init];
    if ([self.actions count]==0) {
        // If no actions, just "Store the sequence" as room 1 device 1 = off
        //[commands addObject:@"!R1D1F0,00:00:03"];
        return commands;
    } else {
    
        for (int i=0; i < [self.actions count]; i++) {
            Action *action = [self.actions objectAtIndex:i];
            //DebugLog(@"hwKey: %@", action.hwKey);
            
            if ([action.hwKey length] > 0) {
                NSMutableString *command = [NSMutableString string];
                
                if ([action.hwKey characterAtIndex:0] == '!')
                    [command appendFormat:@"%@", action.hwKey];
                else
                    [command appendFormat:@"!%@", action.hwKey];
                
                
                // Check if Open or Close
                
                if ([AppStyleHelper isNexa] ||
                    [AppStyleHelper isKlikaan]) {
                    
                    if ([[action getDevice] isOpenClose]) {
                        command = [[command stringByReplacingOccurrencesOfString:@"F(" withString:@"F1"] mutableCopy];
                        command = [[command stringByReplacingOccurrencesOfString:@"F)" withString:@"F0"] mutableCopy];
                    }
                }

                NSString *delay = action.delay;
                
                if ([delay isEqualToString:@""] || [delay isEqualToString:@"00:00:00"])
                    delay = @"00:00:03";
                
                [command appendFormat:@",%@",delay];

                [commands addObject:command];
            } else
                DebugLog(@"Error: No hwKey found for %@", action.name);
        }
    
        return commands;
    }
}

-(NSString *)toString{
    return [NSString stringWithFormat:@"Event name:%@ actionCount:%d", self.name, [self getActionCount]];
}

-(void)saveToDB:(int)displayID{
    DB *db = [DB shared];
    NSString *sql = [NSString stringWithFormat:@"SELECT eventId FROM events WHERE accountId=%d AND eventId=%d", [[UserPrefsHelper shared] getCurrentAccountId], self.eventID];
    NSArray *rows = [db getRows:sql];
    if ([rows count]==0){
        sql = [NSString stringWithFormat:@"INSERT INTO events (name, eventId, displayId, timerName, accountId) VALUES(\'%@\', %d, %d, \'%@\', %d)", self.name, self.eventID, displayID, self.timerName, [[UserPrefsHelper shared] getCurrentAccountId]];
    }else{
        NSDictionary *dict = [rows objectAtIndex:0];
        int eventID = [[dict valueForKey:@"eventId"] intValue];
        sql = [NSString stringWithFormat:@"UPDATE events SET name=\'%@\', displayId=%d WHERE accountId=%d AND eventId=%d", self.name, displayID, [[UserPrefsHelper shared] getCurrentAccountId], eventID];
    }
    [db sqlExec:sql];
}
@end
