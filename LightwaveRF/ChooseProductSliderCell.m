//
//  ChooseProductSliderCell.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 19/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ChooseProductSliderCell.h"

@interface ChooseProductSliderCell ()
@property (nonatomic, strong) ANPopoverSlider *brightnessSlider;
@property (nonatomic, strong) UIImageView *brightnessLoImageView;
@property (nonatomic, strong) UIImageView *brightnessHiImageView;
@end

@implementation ChooseProductSliderCell

@synthesize brightnessSlider, brightnessLoImageView, brightnessHiImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
    }
    return self;
}

- (void)dealloc {
    
}

+ (float)getCellHeight {
    return 95.0f;
}

- (void)setupCellWithTitle:(NSString*)tmpTitle
                    device:(Device *)_device
                  selected:(BOOL)_selected
                      isOn:(BOOL)_isOn
                 indexPath:(NSIndexPath *)_indexPath
                  delegate:(id<ChooseProductOnOffCellDelegate>)_delegate
                percentage:(float)_percentage {
    
    // Setup Initial Cell
    [self setupCellWithTitle:tmpTitle
                      device:_device
                    selected:_selected
                        isOn:_isOn
                   indexPath:_indexPath
                    delegate:_delegate];
        
    /*************************************
     * Brightness Slider
     *************************************/
    
    float _brightness = (_percentage / 100) * DIMMER_MAX;
    
    if (!self.brightnessSlider) {
        self.brightnessSlider = [[ANPopoverSlider alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.containingView.bounds.size.width - 60.0f, 60.0f)];
        [self.brightnessSlider setDataSource:self];
        [self.brightnessSlider addTarget:self action:@selector(dimmerStart:) forControlEvents:UIControlEventTouchDown];
        [self.brightnessSlider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.brightnessSlider addTarget:self action:@selector(dimmerDrag:) forControlEvents:UIControlEventTouchDragInside];
        [self.brightnessSlider setBackgroundColor:[UIColor clearColor]];
        self.brightnessSlider.minimumValue = DIMMER_MIN;
        self.brightnessSlider.maximumValue = DIMMER_MAX;
        self.brightnessSlider.continuous = YES;
        self.brightnessSlider.value = _brightness;

        if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO])
            [self.brightnessSlider setMinimumTrackTintColor:[UIColor colorWithDivisionOfRed:38.0f green:116.0f blue:32.0f alpha:1.0f]];
        else
            [self.brightnessSlider setMinimumTrackTintColor:[UIColor colorWithDivisionOfRed:68 green:32 blue:105 alpha:1.0f]];
        
        [self addSubview:self.brightnessSlider];

    } else
        self.brightnessSlider.value = _brightness;
    
    /*************************************
     *  Brightness Low ImageView
     *************************************/
    if (!self.brightnessLoImageView) {
        self.brightnessLoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,
                                                                                   0.0f,
                                                                                   24.0f,
                                                                                   24.0f)];
        [self.brightnessLoImageView setContentMode:UIViewContentModeScaleAspectFit];
        self.brightnessLoImageView.image = [UIImage imageNamed:@"icon_dimmer_low.png"];
        [self.containingView addSubview:self.brightnessLoImageView];
    }
    
    /*************************************
     *  Brightness High ImageView
     *************************************/
    if (!self.brightnessHiImageView) {
        self.brightnessHiImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,
                                                                                   0.0f,
                                                                                   24.0f,
                                                                                   24.0f)];
        [self.brightnessHiImageView setContentMode:UIViewContentModeScaleAspectFit];
        self.brightnessHiImageView.image = [UIImage imageNamed:@"icon_dimmer_low.png"];
        [self.containingView addSubview:self.brightnessHiImageView];
    }
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    [super positionViewsIsEditing:tmpIsEditing];
    
    // Slider
    [self.brightnessSlider setCenterX:self.containingView.center.x];
    [self.brightnessSlider alignToBottomOfView:self.onButton padding:0.0f];
    
    // Low Brightness ImageView
    [self.brightnessLoImageView setCenterY:self.brightnessSlider.center.y];
    [self.brightnessLoImageView alignToLeftOfView:self.brightnessSlider padding:0.0f];
    
    // High Brightness ImageView
    [self.brightnessHiImageView setCenterY:self.brightnessSlider.center.y];
    [self.brightnessHiImageView alignToRightOfView:self.brightnessSlider padding:0.0f];
}




- (float)getPercentage {
    return (self.brightnessSlider.value / DIMMER_MAX) * 100;
}

/*
- (void)roundSettingToClosestValue {
    // Set Slider to closest value from 0-32
    int closestSetting = (int)self.brightnessSlider.value;
    [self.brightnessSlider setValue:(float)closestSetting animated:NO];
}
 */

- (IBAction)dimmerStart:(id)sender {
    //[self roundSettingToClosestValue];
    //[self showPopoverPercentage];
}

- (IBAction)dimmerDrag:(id)sender {
    //[self roundSettingToClosestValue];
    //[self showPopoverPercentage];
}

- (IBAction)sliderAction:(id)sender {
    //[self roundSettingToClosestValue];
     //[self showPopoverPercentage];

    if ([self.delegate respondsToSelector:@selector(chooseProductSwitch:indexPath:percentage:)])
        [self.delegate chooseProductSwitch:self indexPath:self.indexPath percentage:[self getPercentage]];
}

#pragma mark - ANPopoverSlider Datasource

- (NSString *)getPopoverText:(ANPopoverSlider *)_slider {
    return [NSString stringWithFormat:@"%.0f%%", [self getPercentage]];
}

@end
