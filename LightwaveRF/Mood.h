//
//  Mood.h
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 21/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Zone;

@interface Mood : NSObject
@property (nonatomic) int moodID;
@property (nonatomic) int listingPosition;
@property (weak, nonatomic) NSString *name;
@property (weak, nonatomic) Zone *zone;
@property (nonatomic) BOOL allOff;
@property (nonatomic) BOOL paired;
@property (nonatomic, readonly) int zoneID;

-(id) initWithID:(int) moodID listingPosition:(int) listingPosition name:(NSString *) name zone:(Zone *) zone allOff:(BOOL) allOff;
-(id) initWithID:(int) moodID listingPosition:(int) listingPosition name:(NSString *) name;
-(id) initWithID:(int) moodID name:(NSString *) name zone:(Zone *) zone;
-(id) initAllOff:(Zone *) zone;
-(BOOL) isAllOff;
-(BOOL) isPaired;
-(NSDictionary *) toDictionary;
-(NSData *) toJSON;
-(NSString *) toString;

@end
