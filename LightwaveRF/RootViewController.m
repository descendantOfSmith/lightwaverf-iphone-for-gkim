//
//  RootViewController
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "RootViewController.h"
#import "LWErrorView.h"
#import "Toast+UIView.h"

@interface RootViewController ()
@property (nonatomic, strong) MBProgressHUD *progressSpinner;
@property (nonatomic, assign) BOOL keyboardIsShown;
@end

@implementation RootViewController

@synthesize scrollView, heightOffset;
@synthesize progressSpinner, keyboardIsShown, isModal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.heightOffset = 0.0f;
        self.keyboardIsShown = NO;
        self.isModal = NO;
    }
    return self;
}

- (void)dealloc {
    //DebugLog(@"DEALLOC");
    
    [self endUDPListeners];
    [self endRemoteListeners];
    [self endUDPCheckStateListeners];
    [self endGeneralListeners];
    [self removeKeyboardNotifications];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self iOS7Fix];
    
    [self startUDPListeners];
    [self startRemoteListeners];
    [self startUDPCheckStateListeners];
    [self startGeneralListeners];
    [self registerKeyboardNotifications];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    [self endUDPListeners];
    [self endRemoteListeners];
    [self endUDPCheckStateListeners];
    [self endGeneralListeners];
    [self removeKeyboardNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - iOS7 Layout Fixes

- (void)iOS7Fix {
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        
        // Status Bar with White Text
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
        
        // Status Bar on iOS7 overlaps view so we fake it where we can, set the view BG to Black
        [self.view setBackgroundColor:[UIColor blackColor]];
        [self.scrollView setBackgroundColor:[AppStyleHelper getScrollViewBackgroundColor]];
        
        // iOS7 Fixes
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
    } else {
        [self.view setBackgroundColor:[AppStyleHelper getViewBackgroundColor]];
        [self.scrollView setBackgroundColor:[AppStyleHelper getScrollViewBackgroundColor]];
    }
}

- (void)viewDidLayoutSubviews {
    // iOS7 Fixes for Maintaining View Positions
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        CGRect viewBounds = self.view.bounds;
        CGFloat topBarOffset = self.topLayoutGuide.length;
        viewBounds.origin.y = topBarOffset * -1;
        self.view.bounds = viewBounds;
    }
}

/*
 
 Also;
 
 in Info.plist set 'View controller-based status bar appearance' to NO
 
 AND
 
 Disable Auto Layout in xib inspector
 
 AND
 
 Nav Bar on iOS7 incorporates the status bar.
 
 e.g.   iOS7 Height: 64px     iOS6 Height: 44px
 
 So if setting a default image, include a ios7 image in the resources, but when setting the appearance, just refer to the normal one
 
 // Nav Bar Image (navbar-ios7.png added in resources also)
 [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
 
 */

#pragma -
#pragma Orientation

- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma -

- (NSString *)getClassName:(Class)_class {
    const char* className = class_getName(_class);
    return [NSString stringWithFormat:@"%s", className];
}

- (void)addObserverWithName:(NSString *)_name selector:(SEL)_selector {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:_selector
                                                 name:_name
                                               object:nil];
}

#pragma -
#pragma General

- (void)startGeneralListeners {
    [self addObserverWithName:kRemoteRefreshTableViewNotificationKey selector:@selector(receivedRefreshTableViewNotification:)];
    [self addObserverWithName:kChangeLanguageNotificationKey selector:@selector(receivedChangedLanguageNotification:)];
}

- (void)endGeneralListeners {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRemoteRefreshTableViewNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kChangeLanguageNotificationKey object:nil];
}

- (void)receivedRefreshTableViewNotification:(NSNotification *)_notification {
    // Overridden by children
}

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    // Overridden by children
}

#pragma -
#pragma WiFi/Remote Check

- (void)startUDPCheckStateListeners {
    [self addObserverWithName:kUDPCheckStateSuccessNotificationKey selector:@selector(receivedUDPCheckedStateSuccessNotification:)];
    [self addObserverWithName:kUDPCheckStateFailNotificationKey selector:@selector(receivedUDPCheckedStateFailNotification:)];
}

- (void)endUDPCheckStateListeners {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDPCheckStateSuccessNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDPCheckStateFailNotificationKey object:nil];
}

- (void)receivedUDPCheckedStateSuccessNotification:(NSNotification *)_notification {
    // Overridden by children
}

- (void)receivedUDPCheckedStateFailNotification:(NSNotification *)_notification {
    // Overridden by children
}

#pragma -
#pragma UDP Service

- (void)startUDPListeners {
    [self addObserverWithName:kUDPErrorNotificationKey selector:@selector(receivedUDPServiceNotification:)];
    [self addObserverWithName:kUDPSentNotificationKey selector:@selector(receivedUDPServiceNotification:)];
    [self addObserverWithName:kUDPFailedNotificationKey selector:@selector(receivedUDPServiceNotification:)];
}

- (void)endUDPListeners {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDPErrorNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDPSentNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDPFailedNotificationKey object:nil];
}

- (void)receivedUDPServiceNotification:(NSNotification *)_notification {
    // Overridden by children
}

#pragma -
#pragma Remote Service

- (void)startRemoteListeners {
    [self addObserverWithName:kRemoteErrorNotificationKey selector:@selector(receivedRemoteServiceNotification:)];
    [self addObserverWithName:kRemoteSentNotificationKey selector:@selector(receivedRemoteServiceNotification:)];
    [self addObserverWithName:kRemoteFailedNotificationKey selector:@selector(receivedRemoteServiceNotification:)];
}

- (void)endRemoteListeners {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRemoteErrorNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRemoteSentNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRemoteFailedNotificationKey object:nil];
}

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    // Overridden by children
}

#pragma mark -
#pragma mark Keyboard Notifications

- (void)registerKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHideHandler:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
}

- (void)removeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
}

#pragma mark - One Off Alerts

- (NSString *)getAlertKey:(NSString *)_localisedKey {
    return [NSString stringWithFormat:@"%@-%@", NSStringFromClass([self class]), _localisedKey];
}

- (void)setViewedAlert:(BOOL)_didView localisedKey:(NSString *)_localisedKey {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:[NSNumber numberWithBool:_didView] forKey:[self getAlertKey:_localisedKey]];
    [userDefaults synchronize];
}

- (BOOL)didViewAlertForLocalisedKey:(NSString *)_localisedKey {
    NSNumber *_viewedAlert = [[NSUserDefaults standardUserDefaults] objectForKey:[self getAlertKey:_localisedKey]];
    return [_viewedAlert boolValue];
}

- (void)showOneOffAlertForLocalisedKey:(NSString *)_localisedKey duration:(float)_duration {
    if (![self didViewAlertForLocalisedKey:_localisedKey]) {
        [self setViewedAlert:YES localisedKey:_localisedKey];
        [self.view makeToast:NSLocalizedString(_localisedKey, @"text") duration:_duration position:@"bottom"];
    }
}

#pragma mark -
#pragma mark UIKeyboard Handlers
/*
 * Used for increasing/descresing scrollview content size when a keyboard is presented
 */
- (void)keyboardWillHideHandler:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    viewFrame.size.height += (keyboardSize.height);
    
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [UIView setAnimationDuration:0.3f];
//    [self.scrollView setFrame:viewFrame];
//    [UIView commitAnimations];
    
    self.keyboardIsShown = NO;
    [self.scrollView setContentOffset:CGPointZero];
}

- (void)keyboardWasShown:(NSNotification*)notification {
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (self.keyboardIsShown) {
        return;
    }
    
    NSDictionary* userInfo = [notification userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    viewFrame.size.height -= (keyboardSize.height);
    
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [UIView setAnimationDuration:0.3f];
//    [self.scrollView setFrame:viewFrame];
//    [UIView commitAnimations];
    
    self.keyboardIsShown = YES;
}

- (void)scrollToTargetView:(UIView *)tmpView {
    CGPoint pt;
    CGRect rc = [tmpView bounds];
    rc = [tmpView convertRect:rc toView:self.scrollView];
    pt = rc.origin;
    pt.x = 0;
    float _offset = 110.0f;
    if (pt.y > _offset)
        pt.y -= _offset;
    else
        pt.y = 0.0f;
    [self.scrollView setContentOffset:pt animated:YES];
}

#pragma -
#pragma MBProgressHUD

- (void)showSpinnerWithText:(NSString *)tmpText {
    if (!self.progressSpinner) {
        self.progressSpinner = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:self.progressSpinner];
        self.progressSpinner.delegate = self;
        self.progressSpinner.labelText = tmpText;
        [self.progressSpinner show:YES];
    }
}

- (void)removeSpinner {
    if (self.progressSpinner) {
        [self.progressSpinner show:NO];
        [self.progressSpinner removeFromSuperview];
    }
    
    self.progressSpinner = nil;
}

#pragma -
#pragma MBProgressHUD Delegate

- (void)hudWasHidden {
    
}

#pragma -
#pragma Error Logging

- (void)showError:(NSString *)message {
    LWErrorView *lwErrorView = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"error", @"Error")
                                                          message:message
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                                                otherButtonTitles:nil,nil];
    [lwErrorView show];
}

- (void) showMessage:(NSString *)messageID{
    NSString *message = NSLocalizedString(messageID, @"Look in the localized strings file");
    LWErrorView *lwErrorView = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"info", @"Info")
                                                          message:message
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                                                otherButtonTitles:nil,nil];
    [lwErrorView show];
}

- (void) showAlert:(NSString *)alert{
    LWErrorView *lwErrorView = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"info", @"Info")
                                                          message:alert
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                                                otherButtonTitles:nil,nil];
    [lwErrorView show];
}

#pragma - Public

- (void) hideKeyBoard:(id) sender
{
    [self.view endEditing:YES];
}

- (void)dismissViewAnimated:(BOOL)_animated {
    if ([self isModal])
        [[self navigationController] dismissModalViewControllerAnimated:_animated];
    else
        [self.navigationController popViewControllerAnimated:_animated];
}

@end
