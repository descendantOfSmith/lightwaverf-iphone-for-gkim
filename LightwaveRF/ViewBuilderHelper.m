//
//  ViewBuilderHelper.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ViewBuilderHelper.h"

@implementation ViewBuilderHelper

+ (UIColor *)getBackgroundByPattern:(NSString *)filename {
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:filename]];
    return background;
}

+ (UITableView *)getTableViewWithWidth:(float)width
                                height:(float)height
                                 style:(UITableViewStyle)tmpStyle
                                target:(id)target {
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, width, height)
                                                          style:tmpStyle];
    [tableView setDataSource:target];
    [tableView setDelegate:target];
    [tableView setBackgroundView:nil];
    [tableView setBackgroundColor:[UIColor clearColor]];
    [tableView setScrollEnabled:YES];
    [tableView setMultipleTouchEnabled:YES];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLineEtched];
    return tableView;
}

+ (UIImageView *)getImageViewWithFilename:(NSString *)filename 
                                      atX:(float)x 
                                      atY:(float)y {
    UIImage *loUIImage = [UIImage imageNamed:filename];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, loUIImage.size.width, loUIImage.size.height)];
    imageView.image = loUIImage;
    
    return imageView;
}


+ (UIButton *)getButtonWithLoFileName:(NSString *)loFilename 
                           hiFilename:(NSString *)hiFilename 
                                  atX:(float)x 
                                  atY:(float)y 
                                  tag:(NSUInteger)tag 
                               target:(id)target 
                               action:(SEL)a {
    UIImage *loUIImage = [UIImage imageNamed:loFilename];
    UIImage *hiUIImage = [UIImage imageNamed:hiFilename];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(x, 
                                y, 
                                loUIImage.size.width, 
                                loUIImage.size.height)];
    [button setTag:tag];
    [button setImage:loUIImage forState:UIControlStateNormal];
    [button setImage:hiUIImage forState:UIControlStateHighlighted];
    [button addTarget:target action:a forControlEvents:UIControlEventTouchUpInside];
    return button;
}

+ (UIBarButtonItem *)createSquareBarButtonItemWithLoFileName:(NSString *)loFilename 
                                                  hiFilename:(NSString *)hiFilename 
                                                      target:(id)tgt 
                                                      action:(SEL)a 
                                                    selected:(BOOL)isSelected {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *buttonImage = [UIImage imageNamed:loFilename];
    UIImage *buttonPressedImage = [UIImage imageNamed:hiFilename];
    
    CGRect buttonFrame = [button frame];
    buttonFrame.size.width = buttonImage.size.width;
    buttonFrame.size.height = buttonImage.size.height;
    [button setFrame:buttonFrame];
    
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
    [button setBackgroundImage:buttonPressedImage forState:UIControlStateSelected];
    [button addTarget:tgt action:a forControlEvents:UIControlEventTouchUpInside];
    
    [button setSelected:isSelected];
    
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    return buttonItem;
}

+ (UIBarButtonItem *)createSquareBarButtonItemWithTitle:(NSString *)t 
                                             loFileName:(NSString *)loFilename 
                                             hiFilename:(NSString *)hiFilename 
                                                 target:(id)tgt 
                                                 action:(SEL)a {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    // Since the buttons can be any width we use a thin image with a stretchable center point
    // @"SquareButton.png"
    // @"SquareButton_pressed.png"
    UIImage *buttonImage = [[UIImage imageNamed:loFilename] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    UIImage *buttonPressedImage = [[UIImage imageNamed:hiFilename] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    
    [[button titleLabel] setFont:[UIFont boldSystemFontOfSize:12.0]];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [button setTitleShadowColor:[UIColor colorWithWhite:1.0 alpha:0.7] forState:UIControlStateNormal];
    [button setTitleShadowColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    [[button titleLabel] setShadowOffset:CGSizeMake(0.0, 1.0)];
    
    CGRect buttonFrame = [button frame];
    buttonFrame.size.width = [t sizeWithFont:[UIFont boldSystemFontOfSize:12.0]].width + 24.0;
    buttonFrame.size.height = buttonImage.size.height > 0 ? buttonImage.size.height : 25.0f;
    [button setFrame:buttonFrame];
    
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
    [button setTitle:t forState:UIControlStateNormal];
    [button addTarget:tgt action:a forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    return buttonItem;
}

+ (UITextField *)getTextFieldWithText:(NSString *)text 
                          placeholder:(NSString *)placeholder 
                         keyboardType:(UIKeyboardType)keyboardType 
                                 font:(UIFont *)font
                                  atX:(float)x 
                                  atY:(float)y 
                                width:(float)width 
                               height:(float)height
                                  tag:(NSUInteger)tag 
                               target:(id)target {
    
    UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(x, 
                                                                           y, 
                                                                           width, 
                                                                           height)];
    [textfield setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [textfield setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [textfield setPlaceholder:placeholder];
    [textfield setText:text];
    [textfield setTag:tag];
    [textfield setTextColor:[UIColor blackColor]];
    [textfield setBackgroundColor:[UIColor clearColor]];
    [textfield setFont:font];
    [textfield setBorderStyle:UITextBorderStyleRoundedRect];
    [textfield setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textfield setKeyboardType:keyboardType];
    [textfield setReturnKeyType:UIReturnKeyDone];
    [textfield setClearButtonMode:UITextFieldViewModeWhileEditing]; // has a clear 'x' button to the right
    [textfield setDelegate:target];
    
    return textfield;
}

@end
