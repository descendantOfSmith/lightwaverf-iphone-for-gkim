//
//  Account.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 25/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "Account.h"
#import "DB.h"
#import "Home.h"
#import "WFLHelper.h"

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

@interface Account ()

@end

@implementation Account

@synthesize accountId, mac, email, password, name, location, timezone, ssid, elecrate, impkwh, wifiPin, emailPwd;

-(id) initWithEmail:(NSString *)_email password:(NSString *)_password andId:(NSInteger)_id
{
    if (self = [super init])
    {
        self.accountId = _id;
        self.email = _email;
        self.password = _password;
        
        //Defaults
        self.mac = @"";             // Mac address
        self.name = @"";            // Name is a name you can assocaite with the account and can be setting in SettingsVC.
        self.location = @"";        // location is like Birmingham, UK|54.123, -1.123   - 3 decimal places
        self.timezone = @"";        // timezone is something like GMT Greenwch Mean Time|0
        self.ssid = @"";            // ssid is the name of the current Wifi account. ie your router not the WFL
        self.wifiPin = @"";
        self.emailPwd = @"";
        self.elecrate = 1.0;
        self.impkwh = 500;
    }
    return self;
}

- (NSString *)toString {
    return [NSString stringWithFormat:@"%@ (%@)", self.email, self.password];
}

#pragma mark - Class Functions

+ (NSInteger)getNewAccountId {
    DB *db = [DB shared];
    NSString *_key = @"max(id)";
    NSMutableArray *rows = [db getRows:[NSString stringWithFormat:@"SELECT %@ FROM accounts", _key]];
    if (rows.count > 0) {
        NSMutableDictionary *dict = [rows objectAtIndex:0];
        if (dict.count > 0) {
            NSInteger _currentId = [NULL_TO_NIL([dict objectForKey:_key]) intValue];
            return _currentId + 1;
        } else
            return 1;
    } else
        return 1;
}

+ (void)updateCurrentAccount:(NSString *)_email password:(NSString *)_password {
    // Set the current account Id
    Home *home = [Home shared];
    Account *_account = [home getAccountWithEmail:_email];
    if (!_account) {
       _account = [[Account alloc] initWithEmail:_email
                                         password:_password
                                            andId:[Account getNewAccountId]];
        [home addAccount:_account];
        [home saveAccounts];
    }
    
    [[UserPrefsHelper shared] setCurrentAccountId:_account.accountId];
}

+ (NSString *)getDeviceTimeZone {
    NSArray *tokens = nil;
    NSString *str = nil;
    
    NSTimeZone *localTime = [NSTimeZone systemTimeZone];
//    DebugLog(@"Current local timezone is %@",[localTime name]);
//    DebugLog(@"Current local timezone is %@",localTime);
//    DebugLog(@"Current local timezone is %@",[localTime localizedName:NSTimeZoneNameStyleStandard locale:[NSLocale currentLocale]]);
//    DebugLog(@"Current local timezone is %@",[localTime localizedName:NSTimeZoneNameStyleGeneric locale:[NSLocale currentLocale]]);
//    DebugLog(@"Current local timezone is %@",[localTime localizedName:NSTimeZoneNameStyleDaylightSaving locale:[NSLocale currentLocale]]);
    
    NSInteger secondsFromGMT = [localTime secondsFromGMT];
//    DebugLog(@"secondsFromGMT %d", secondsFromGMT);
    NSInteger hoursFromGMT = secondsFromGMT / (60 * 60);
//    DebugLog(@"hoursFromGMT %d", hoursFromGMT);
    
    /****************************************************************
     * I need to manually remove the Daylight Saving Offset
     ****************************************************************/
    
//    DebugLog(@"%lf", [localTime daylightSavingTimeOffset]);
    NSInteger ti = (NSInteger)[localTime daylightSavingTimeOffset];
    NSInteger _hours = (ti / 3600);
//    DebugLog(@"_hours: %d", _hours);
    hoursFromGMT = hoursFromGMT - _hours;
    
    /****************************************************************
     * End
     ****************************************************************/
    
    NSString *name = [localTime localizedName:NSTimeZoneNameStyleStandard locale:[NSLocale currentLocale]];
//    DebugLog(@"name %@", name);
    NSString *abbre = @"";
    tokens = [name componentsSeparatedByString:@" "];
    for (NSString *token in tokens)
        abbre = [NSString stringWithFormat:@"%@%C", abbre, [token characterAtIndex:0]];
    
    str = [NSString stringWithFormat:@"%@ %@|%d", abbre, name, hoursFromGMT];
    DebugLog(@"Device Timezone: %@", str);
    
    return str;
}

+ (NSString *)getDeviceTimeZoneWithoutHours {
    NSArray *tokens = [[Account getDeviceTimeZone] componentsSeparatedByString:@"|"];
    return [tokens objectAtIndex:0];
}

/*
 * This function will automatically update the current user account object
 * and save the changes.
 */
+ (void)setTimeZone {
    // Save Timezone for User
    [[UserPrefsHelper shared] setTimeZoneInfo:[Account getDeviceTimeZone]];
    
    // Send to WFL
    NSArray *tokens = [[[UserPrefsHelper shared] getTimeZoneInfo] componentsSeparatedByString:@"|"];
    if (tokens.count == 2) {
        int val = [[tokens objectAtIndex:1] intValue];
        if (val < 0 )
            val = 24 + val;

        // Send Command
        [[WFLHelper shared] sendToUDP:YES
                              command:[ServiceType getTimeZoneCommandWithValue:val]
                                  tag:kRequestTimeZone];
    }
}

@end
