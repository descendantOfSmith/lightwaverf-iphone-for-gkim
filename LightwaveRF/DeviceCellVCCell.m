//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import "DeviceCellVCCell.h"
#import "Device.h"
#import "WFLHelper.h"
#import "ButtonsHelper.h"
#import "ViewBuilderHelper.h"

@interface DeviceCellVCCell ()
@property (strong, nonatomic) UIButton *on_btn;
@property (strong, nonatomic) UIImageView *onActive_img;
@property (strong, nonatomic) UIButton *off_btn;
@property (strong, nonatomic) UIImageView *active_img;
@property (strong, nonatomic) UIButton *stop_btn;
@property (strong, nonatomic) UIImageView *stopActive_img;
@end

@implementation DeviceCellVCCell

@synthesize zoneID;
@synthesize icon_img;
@synthesize name_lbl;
@synthesize device;
@synthesize active_img;
@synthesize onActive_img;
@synthesize stopActive_img;
@synthesize stop_btn;
@synthesize fadeActive_tmr;
@synthesize onFadeActive_tmr;
@synthesize stopFadeActive_tmr;
@synthesize on_btn;

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [icon_img setContentMode:UIViewContentModeScaleAspectFit];
    [icon_img setCenterY:self.contentView.center.y];
    [name_lbl setCenterY:self.contentView.center.y];
    
    active_img.hidden = YES;
    onActive_img.hidden = YES;
    stopActive_img.hidden = YES;
    
}

- (void)dealloc {
    if (fadeActive_tmr != nil){
        [fadeActive_tmr invalidate];
    }
    if (onFadeActive_tmr != nil){
        [onFadeActive_tmr invalidate];
    }
    if (stopFadeActive_tmr != nil){
        [stopFadeActive_tmr invalidate];
    }
}

+ (float)getCellHeight {
    return 50.0f;
}

- (void)setupCellWithDevice:(Device *)_device
                     zoneID:(int)_zoneID {
    
    self.device = _device;
    self.zoneID = _zoneID;
    
    [self setupOnButton];
    [self setupOffButton];
    [self setupStopButton];
    
    if (device!=nil)
        [name_lbl setText:device.name];
    
    [icon_img setImage:[self.device getIconImage]];
    
    if ([device isOpenClose] ||
        [device isFSL500W] ||
        [device isFSL3000W]) {
        
        [self.on_btn setTitle:[NSLocalizedString(@"open", @"OPEN") uppercaseString] forState:UIControlStateNormal];
        [self.off_btn setTitle:[NSLocalizedString(@"close", @"CLOSE") uppercaseString] forState:UIControlStateNormal];
        [self.off_btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:9.0f]];
        
    } else {
        stop_btn.hidden = YES;
        [self.off_btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0f]];
    }
    
    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self positionViewsIsEditing:self.isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

- (void)setupOnButton {
    if (!self.on_btn) {
        self.on_btn = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"on", @"ON") uppercaseString]
                                                  type:kButtonGreenPSwitch
                                                   tag:kBtnOn
                                                 width:40
                                                height:38
                                                target:self
                                              selector:@selector(onPressed:)];
        [self.contentView addSubview:self.on_btn];
    }
    
    if (!self.onActive_img) {
        self.onActive_img = [ViewBuilderHelper getImageViewWithFilename:@"active_blue.png"
                                                                    atX:0
                                                                    atY:0];
        [self.onActive_img setFrameWidth:27];
        [self.onActive_img setFrameHeight:8];
        [self.contentView addSubview:self.onActive_img];
        self.onActive_img.hidden = YES;
    }
    
    if ([AppStyleHelper isNexa]) {
        [self.on_btn setFrameX:224];
        [self.on_btn setFrameY:6];
        
        [self.onActive_img setFrameX:231];
        [self.onActive_img setFrameY:33];
    } else {
        [self.on_btn setFrameX:272];
        [self.on_btn setFrameY:6];
        
        [self.onActive_img setFrameX:279];
        [self.onActive_img setFrameY:33];
    }
}





- (void)setupOffButton {
    if (!self.off_btn) {
        self.off_btn = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"off", @"OFF") uppercaseString]
                                                   type:kButtonGreenOSwitch
                                                    tag:kBtnOff
                                                  width:40
                                                 height:38
                                                 target:self
                                               selector:@selector(offPressed:)];
        [self.contentView addSubview:self.off_btn];
    }
    
    if (!self.active_img) {
        self.active_img = [ViewBuilderHelper getImageViewWithFilename:@"active.png"
                                                                  atX:0
                                                                  atY:0];
        [self.active_img setFrameWidth:27];
        [self.active_img setFrameHeight:8];
        [self.contentView addSubview:self.active_img];
        self.active_img.hidden = YES;
    }
    
    if ([AppStyleHelper isNexa]) {
        [self.off_btn setFrameX:272];
        [self.off_btn setFrameY:6];
        
        [self.active_img setFrameX:279];
        [self.active_img setFrameY:33];
    } else {
        [self.off_btn setFrameX:224];
        [self.off_btn setFrameY:6];
        
        [self.active_img setFrameX:231];
        [self.active_img setFrameY:33];
    }
}








- (void)setupStopButton {
    if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO]) {
        if ([self.device isOpenClose]) {

            if (!self.stop_btn) {
                self.stop_btn = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"stop", @"STOP") uppercaseString]
                                                            type:kButtonGreySwitch
                                                             tag:kBtnStop
                                                           width:40
                                                          height:38
                                                          target:self
                                                        selector:@selector(stopPressed:)];
                [self.stop_btn setFrameX:178];
                [self.stop_btn setFrameY:6];
                [self.contentView addSubview:self.stop_btn];
            }
            
            if (!self.stopActive_img) {
                self.stopActive_img = [ViewBuilderHelper getImageViewWithFilename:@"active.png"
                                                                              atX:185
                                                                              atY:33];
                [self.stopActive_img setFrameWidth:27];
                [self.stopActive_img setFrameHeight:8];
                [self.contentView addSubview:self.stopActive_img];
                self.stopActive_img.hidden = YES;
            }
            
        }
    }
}






- (IBAction)onPressed:(id)sender {
    DebugLog(@"On pressed");
    
    if ([device isOpenClose]) {
        
        [[WFLHelper shared] sendToUDP:YES
                              command:[UDPService getOpenCloseCommandWithZoneID:self.zoneID device:device openClose:kConstantOpen]
                                  tag:kRequestOpen];
        
    } else if ([device isFSL500W]) {
        
        // FSL Demo 500W should send F( then wait 3s and then F^ for Open.
        [[WFLHelper shared] sendToUDP:YES
                              command:[NSString stringWithFormat:@"!R%dD%dF(", self.zoneID, device.deviceID]
                                  tag:kRequestOpen];
        
        // Delay 3 seconds
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            [[WFLHelper shared] sendToUDP:YES
                                  command:[NSString stringWithFormat:@"!R%dD%dF^", self.zoneID, device.deviceID]
                                      tag:kRequestOpen];
        });
    
    } else if ([device isFSL3000W]) {
        
        // FSL Demo 3000W should send R_D7F1, wait 3s then F0 for Open.
        [[WFLHelper shared] sendToUDP:YES
                              command:[NSString stringWithFormat:@"!R%dD7F1", self.zoneID] //[NSString stringWithFormat:@"!R%dD%dF1", self.zoneID, device.deviceID]
                                  tag:kRequestOpen];
        
        // Delay 3 seconds
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            [[WFLHelper shared] sendToUDP:YES
                                  command:[NSString stringWithFormat:@"!R%dD7F0", self.zoneID] //[NSString stringWithFormat:@"!R%dD%dF0", self.zoneID, device.deviceID]
                                      tag:kRequestOpen];
        });
        
    } else {
        
        [[WFLHelper shared] sendToUDP:YES
                              command:[UDPService getOnOffCommandWithZoneID:self.zoneID device:device isOn:YES]
                                  tag:kRequestToggleOnOff];
        
    }
    
    onActive_img.hidden = NO;
    onActive_img.alpha = 1.0;
    if (onFadeActive_tmr == nil){
        onFadeActive_tmr = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(onFadeActive:) userInfo:nil repeats:YES  ];
    }
}

- (void)onFadeActive:(NSTimer *)timer{
    onActive_img.alpha -= 0.1;
    if (onActive_img.alpha<=0.0){
        [onFadeActive_tmr invalidate];
        onFadeActive_tmr = nil;
        onActive_img.hidden = YES;
    }
}




- (IBAction)offPressed:(id)sender {
    DebugLog(@"Off pressed");
    
    if ([device isOpenClose]) {
        
        [[WFLHelper shared] sendToUDP:YES
                              command:[UDPService getOpenCloseCommandWithZoneID:self.zoneID device:device openClose:kConstantClose]
                                  tag:kRequestClose];
        
    } else if ([device isFSL500W]) {
        
        // FSL Demo 500W should send F) then 3s before F^ for Close.
        [[WFLHelper shared] sendToUDP:YES
                              command:[NSString stringWithFormat:@"!R%dD%dF)", self.zoneID, device.deviceID]
                                  tag:kRequestClose];
        
        // Delay 3 seconds
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            [[WFLHelper shared] sendToUDP:YES
                                  command:[NSString stringWithFormat:@"!R%dD%dF^", self.zoneID, device.deviceID]
                                      tag:kRequestClose];
        });
        
    } else if ([device isFSL3000W]) {
        
        // FSL Demo 3000W should send R_D8F1, wait 3s then F0 for Close.
        [[WFLHelper shared] sendToUDP:YES
                              command:[NSString stringWithFormat:@"!R%dD8F1", self.zoneID] //[NSString stringWithFormat:@"!R%dD%dF1", self.zoneID, device.deviceID]
                                  tag:kRequestClose];
        
        // Delay 3 seconds
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            [[WFLHelper shared] sendToUDP:YES
                                  command:[NSString stringWithFormat:@"!R%dD8F0", self.zoneID] //[NSString stringWithFormat:@"!R%dD%dF0", self.zoneID, device.deviceID]
                                      tag:kRequestClose];
        });
        
    } else {
        
        [[WFLHelper shared] sendToUDP:YES
                              command:[UDPService getOnOffCommandWithZoneID:self.zoneID device:device isOn:NO]
                                  tag:kRequestToggleOnOff];
    }
    
    active_img.hidden = NO;
    active_img.alpha = 1.0;
    if (fadeActive_tmr == nil){
        fadeActive_tmr = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(fadeActive:) userInfo:nil repeats:YES  ];
    }
}

- (void)fadeActive:(NSTimer *)timer{
    //DebugLog(@"ALPHA: %f", active_img.alpha);
    active_img.alpha -= 0.1;
    //DebugLog(@"ALPHA: %f", active_img.alpha);
    if (active_img.alpha<=0.0){
        [fadeActive_tmr invalidate];
        fadeActive_tmr = nil;
        active_img.hidden = YES;
    }
}







- (IBAction)stopPressed:(id)sender {
    DebugLog(@"Stop pressed");
    
    [[WFLHelper shared] sendToUDP:YES
                          command:[UDPService getOpenCloseCommandWithZoneID:self.zoneID device:device openClose:kConstantStop]
                              tag:kRequestStop];
    
    stopActive_img.hidden = NO;
    stopActive_img.alpha = 1.0;
    if (stopFadeActive_tmr == nil){
        stopFadeActive_tmr = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(stopFadeActive:) userInfo:nil repeats:YES  ];
    }
}

- (void)stopFadeActive:(NSTimer *)timer{
    stopActive_img.alpha -= 0.1;
    if (stopActive_img.alpha<=0.0){
        [stopFadeActive_tmr invalidate];
        stopFadeActive_tmr = nil;
        stopActive_img.hidden = YES;
    }
}


@end
