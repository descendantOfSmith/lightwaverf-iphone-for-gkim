//
//  AccessoryView.h
//  Prologis
//
//  Created by Dan's iMac on 14/01/2013.
//
//

#import <UIKit/UIKit.h>

@protocol AccessoryViewDelegate;

@interface AccessoryView : UIView {
    
}

@property (nonatomic, assign) id <AccessoryViewDelegate> delegate;
@property (nonatomic, retain) IBOutlet UIView *view;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *previousBtn;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *nextBtn;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *doneBtn;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;

- (void)hidePreviousButton;
- (void)hideNextButton;

@end

@protocol AccessoryViewDelegate <NSObject>

@required

- (void)doneTextFieldPressed:(AccessoryView *)tmpAccessoryView;
- (void)previousTextFieldPressed:(AccessoryView *)tmpAccessoryView;
- (void)nextTextFieldPressed:(AccessoryView *)tmpAccessoryView;

@end
