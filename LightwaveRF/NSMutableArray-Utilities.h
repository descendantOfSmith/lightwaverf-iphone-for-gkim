//
//  NSMutableArray-Utilities.h
//  ICCEasyLife
//
//  Created by imac on 07/08/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Utilities)

- (void)reverseArray; // Reverse the order of an array
- (void)shuffle; // Randomly shuffle array
- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to;
- (void)sortArrayOfDictionarysWithKey:(NSString *)key ascending:(BOOL)isAscending; // Sort dictionary objects by a specific key
- (NSArray *)sortAscending:(BOOL)isAscending;

@end
