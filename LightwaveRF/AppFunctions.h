//
// App Functions
//

#define RGB(r, g, b)                          [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define RGBA(r, g, b, a)                      [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define RECT_X(f)                             f.origin.x
#define RECT_Y(f)                             f.origin.y
#define RECT_WIDTH(f)                         f.size.width
#define RECT_HEIGHT(f)                        f.size.height
#define RECT_CENTER(f)                        CGPointMake(NSOriginX(rect) + NSWidth(rect) / 2, \
NSOriginY(rect) + NSHeight(rect) / 2)

#define FRAME_X(v)                            RECT_X(v.frame)
#define FRAME_Y(v)                            RECT_Y(v.frame)
#define FRAME_WIDTH(v)                        RECT_WIDTH(v.frame)
#define FRAME_HEIGHT(v)                       RECT_HEIGHT(v.frame)

#define DEGREES_TO_RADIANS(x)                 (M_PI * x / 180.0)
#define RADIANS_TO_DEGREES(x)                 (x * 180 / M_PI)

#define FLUSH_POOL(p)                         [p drain]; p = [[NSAutoreleasePool alloc] init]

#define SHOW_STATUS_BAR()                     kSharedApplication.statusBarHidden = YES
#define HIDE_STATUS_BAR()                     kSharedApplication.statusBarHidden = NO

#define SHOW_NETWORK_ACTIVITY_INDICATOR()     kSharedApplication.networkActivityIndicatorVisible = YES
#define HIDE_NETWORK_ACTIVITY_INDICATOR()     kSharedApplication.networkActivityIndicatorVisible = NO
#define NETWORK_ACTIVITY_INDICATOR(value)     kSharedApplication.networkActivityIndicatorVisible = x

#define DIAL_PHONE_NUMBER(s)                  [kSharedApplication \
openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", s]]];

#define RANDOM_RANGE(a, b)                    ((((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * (b - a)) + a)

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
