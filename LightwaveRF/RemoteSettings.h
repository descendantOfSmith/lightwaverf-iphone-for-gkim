//
//  RemoteSettings.h
//  LightwaveRF
//
//  Created by Nik Lever on 06/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceType.h"

// NSNotification Center Delegates
#define kRemoteErrorNotificationKey            @"Remote_Error_Notification"
#define kRemoteSentNotificationKey             @"Remote_Sent_Notification"
#define kRemoteFailedNotificationKey           @"Remote_Failed_Notification"

// General NSNotifications
#define kRemoteRefreshTableViewNotificationKey @"Remote_Refresh_Tableview_Notification"

// Data Keys
#define kRemoteDateKey                         @"Remote_Date"
#define kRemoteCommandKey                      @"Remote_Command"
#define kRemoteTransactionIDKey                @"Remote_TransactionID"
#define kRemoteTagKey                          @"Remote_Tag"
#define kRemoteClassNameKey                    @"Remote_ClassName"
#define kRemoteErrorKey                        @"Remote_Error"
#define kRemoteFailedKey                       @"Remote_Failed"
#define kRemoteResponseKey                     @"Remote_ResponseString"

#define kRemoteTimeoutTime                     8.0f

@interface RemoteSettings : ServiceType {
    NSString *AMAZON_WEBSERVER;
    NSString *ONEANDONE_WEBSERVER;
    NSString *ACTIVE_WEBSERVER;
    NSString *PUSH_HOME_RESOURCE_URL;
    NSString *PULL_HOME_RESOURCE_URL;
    NSString *SEND_COMMAND_URL;
    NSString *GET_METER_URL;
    NSString *SET_PIN_URL;
}

@property (nonatomic, strong) NSMutableArray *queueDictArray;

+ (id)shared;

- (void)pullHomeWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag withEmail:(NSString*)email andPassword:(NSString*)password;
- (void)pushHomeWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag;
- (void)pullEnergyMeterWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag;
- (void)setPinWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag withPin:(NSString *)pin;
- (void)stopAllEventsWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag;
- (void)sendCommandWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag command:(NSString *)_command;

@end
