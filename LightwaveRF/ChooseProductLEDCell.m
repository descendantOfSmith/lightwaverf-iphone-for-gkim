//
//  ChooseProductSliderCell.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 19/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ChooseProductLEDCell.h"

@interface ChooseProductLEDCell ()
@property (nonatomic, strong) UIButton *moreButton;
@end

@implementation ChooseProductLEDCell

@synthesize moreButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
    }
    return self;
}

- (void)dealloc {
    
}

+ (float)getCellHeight {
    return 95.0f;
}

- (void)setupLEDCellWithTitle:(NSString*)tmpTitle
                       device:(Device *)_device
                     selected:(BOOL)_selected
                         isOn:(BOOL)_isOn
                    indexPath:(NSIndexPath *)_indexPath
                     delegate:(id<ChooseProductOnOffCellDelegate>)_delegate
                   percentage:(float)_percentage {
    
    // Setup Initial Cell
    [self setupCellWithTitle:tmpTitle
                      device:_device
                    selected:_selected
                        isOn:_isOn
                   indexPath:_indexPath
                    delegate:_delegate
                  percentage:_percentage];
    
    /*************************************
     * MORE Button
     *************************************/
    if (!self.moreButton) {
        self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.moreButton setBackgroundImage:[UIImage imageNamed:@"led_more.png"] forState:UIControlStateNormal];
        [self.moreButton setFrameWidth:41.0f];
        [self.moreButton setFrameHeight:38.0f];
        [self.moreButton setTag:kBtnMore];
        [self.moreButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.containingView addSubview:self.moreButton];
    }
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    [super positionViewsIsEditing:tmpIsEditing];
    
    // More Button
    [self.moreButton alignToLeftOfView:self.offButton padding:5.0f];
    [self.moreButton setCenterY:self.containingView.center.y];
}

- (IBAction)buttonPressed:(id)sender {
    [super buttonPressed:sender];
    
    switch ([sender tag]) {
            
        case kBtnMore:
        {
            if ([self.delegate respondsToSelector:@selector(chooseProductButtonPressed:indexPath:tag:)])
                [self.delegate chooseProductButtonPressed:self indexPath:self.indexPath tag:[sender tag]];
        }
            break;
            
        default:
            break;
    }
}

@end
