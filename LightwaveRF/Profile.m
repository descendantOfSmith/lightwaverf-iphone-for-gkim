//
//  Profile.m
//  LightwaveRF
//
//  Created by Nik Lever on 25/06/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "Profile.h"

@implementation Profile

@synthesize profileID, type, zoneId, temp, start, end;

-(id) initWithProfileId:(int)_profileId type:(int)_type zoneId:(int)_zoneId temp:(float)_temp start:(NSString*)_start end:(NSString*)_end {
    self.profileID = _profileId;
    self.type = _type;
    
    return [self initWithZoneId:_zoneId temp:_temp start:_start end:_end];
}

-(id) initWithZoneId:(int)_zoneId temp:(float)_temp start:(NSString*)_start end:(NSString*)_end {
    if (self = [super init])
    {
        self.zoneId = _zoneId;
        self.start = _start;
        self.end = _end;
        self.temp = _temp;
    }
    return self;
}

- (double)getStartTime {
    if (self.start==nil)
        return 0;
    
    NSArray *_tokens = [self.start componentsSeparatedByString:@":"];
    double hours = [[_tokens objectAtIndex:0] floatValue];
    int mins = [[_tokens objectAtIndex:1] intValue];
    hours += (double)(mins/60.0);
    return hours;
}

- (double)getEndTime {
    if (self.end==nil)
        return 0;
    
    NSArray *_tokens = [self.end componentsSeparatedByString:@":"];
    double hours = [[_tokens objectAtIndex:0] floatValue];
    int mins = [[_tokens objectAtIndex:1] intValue];
    hours += (double)(mins/60.0);
    return hours;
}

- (NSString *)getCommand {
    return [NSString stringWithFormat:@"P%02f,S:%@,E%@", self.temp, self.start, self.end];
}

- (NSString *)toString {
    return [NSString stringWithFormat:@"id=%d, zoneId=%d, type=%d, temp=%02fºC, start=%@, end=%@", self.profileID, self.zoneId, self.type, self.temp, self.start, self.end];
}

-(NSDictionary *) toDictionary
{
    NSDictionary *deviceDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:self.profileID] ,@"id",
                                      [NSNumber numberWithInt:self.type] ,@"type",
                                      [NSNumber numberWithInt:self.zoneId] ,@"zoneId",
                                      [NSNumber numberWithInt:self.temp] ,@"temp",
                                      self.start, @"start",
                                      self.end, @"end",
                                      nil];
    return deviceDictionary;
}

-(NSData *) toJSON
{
    NSError *error = nil;
    NSDictionary *jsonDevice = self.toDictionary;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDevice options:NSJSONWritingPrettyPrinted error:&error];
    if (!error)
    {
        //DebugLog([NSString stringWithFormat:@"JSON device data:\n%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]);
        return jsonData;
    }
    else
    {
        return nil;
    }
}

@end
