//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "ANPopoverSlider.h"

@class Device;

@interface DimmerCellVCCell : UITableViewCell <ANPopoverSliderDataSource> {

}

@property (nonatomic, strong) Device *device;
@property (nonatomic, assign) int zoneID;
@property (strong, nonatomic) IBOutlet UIImageView *icon_img;
@property (strong, nonatomic) IBOutlet UILabel *name_lbl;
@property (strong, nonatomic) IBOutlet ANPopoverSlider *dimmer_sdr;
@property (strong) NSIndexPath *indexPath;
@property (strong, nonatomic) IBOutlet UIImageView *dimmerbg_img;
@property (strong, nonatomic) IBOutlet UIImageView *dimmerUpbg_img;
@property (strong, nonatomic) NSTimer *fadeActive_tmr;
@property (strong, nonatomic) NSTimer *onFadeActive_tmr;
@property (strong, nonatomic) IBOutlet UIImageView *highlight_img;

+ (float)getCellHeight;

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing;

- (void)setupCellWithDevice:(Device *)_device zoneID:(int)_zoneID;

- (IBAction)onPressed:(id)sender;
- (IBAction)offPressed:(id)sender;
- (void)setIndexPathWithRow:(NSInteger)row andSection:(NSInteger)section;
- (IBAction)dimmerDrag:(id)sender;
- (IBAction)dimmerEditEnd:(id)sender;
- (IBAction)dimmerEditStart:(id)sender;

@end
