//
//  NSString+URLEncoding.h
//  LightwaveRF
//
//  Created by Nik Lever on 02/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//
//#define NSUTF8StringEncoding  4

#import <Foundation/Foundation.h>
@interface NSString (URLEncoding)
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
@end