//
//  DB.h
//  LightwaveRF
//
//  Created by Nik Lever on 25/06/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DB : NSObject

@property (readwrite) sqlite3 *db;

+ (id)shared;

-(bool)sqlExec:(NSString *)sql;
-(NSMutableArray *)getRows:(NSString *)sql;
-(void)clear;
-(void)clearAccount:(NSInteger)_accountId;

@end
