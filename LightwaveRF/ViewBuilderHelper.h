//
//  ViewBuilderHelper.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewBuilderHelper : NSObject {
    
}

+ (UIColor *)getBackgroundByPattern:(NSString *)filename;

+ (UITableView *)getTableViewWithWidth:(float)width
                                height:(float)height
                                 style:(UITableViewStyle)tmpStyle
                                target:(id)target;

+ (UIImageView *)getImageViewWithFilename:(NSString *)filename
                                      atX:(float)x
                                      atY:(float)y;

+ (UIButton *)getButtonWithLoFileName:(NSString *)loFilename
                           hiFilename:(NSString *)hiFilename
                                  atX:(float)x
                                  atY:(float)y
                                  tag:(NSUInteger)tag
                               target:(id)target
                               action:(SEL)a;

+ (UIBarButtonItem *)createSquareBarButtonItemWithLoFileName:(NSString *)loFilename
                                                  hiFilename:(NSString *)hiFilename
                                                      target:(id)tgt
                                                      action:(SEL)a
                                                    selected:(BOOL)isSelected;

+ (UIBarButtonItem *)createSquareBarButtonItemWithTitle:(NSString *)t
                                             loFileName:(NSString *)loFilename
                                             hiFilename:(NSString *)hiFilename
                                                 target:(id)tgt
                                                 action:(SEL)a;

+ (UITextField *)getTextFieldWithText:(NSString *)text
                          placeholder:(NSString *)placeholder
                         keyboardType:(UIKeyboardType)keyboardType
                                 font:(UIFont *)font
                                  atX:(float)x
                                  atY:(float)y
                                width:(float)width
                               height:(float)height
                                  tag:(NSUInteger)tag
                               target:(id)target;

@end
