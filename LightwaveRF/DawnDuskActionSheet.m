//
//  DawnDuskActionSheet.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 02/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "DawnDuskActionSheet.h"
#import "ButtonsHelper.h"

@interface DawnDuskActionSheet ()
@property (nonatomic, assign) CustomActionSheetPickerDawnDuskTypes selectedPickerType;
@property (nonatomic, assign) DawnDuskActionSheetTypes selectedDuskDawnType;
@property (nonatomic, strong) UIButton *dawnButton;
@property (nonatomic, strong) UIButton *beforeDawnButton;
@property (nonatomic, strong) UIButton *afterDawnButton;
@property (nonatomic, strong) UIButton *duskButton;
@property (nonatomic, strong) UIButton *beforeDuskButton;
@property (nonatomic, strong) UIButton *afterDuskButton;
@property (nonatomic, strong) UIButton *timeButton;
@end

@implementation DawnDuskActionSheet

@synthesize timePickerView;
@synthesize selectedPickerType, selectedDuskDawnType, dawnButton, duskButton, timeButton;
@synthesize beforeDawnButton, afterDawnButton, beforeDuskButton, afterDuskButton;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
       selectedType:(CustomActionSheetPickerDawnDuskTypes)tmpSelectedType {
    
    self.selectedPickerType = tmpSelectedType;
    return [self initWithPoint:point
                  buttonTitles:tmpButtonTitles
                   cancelTitle:tmpCancelTitle
                    dataSource:tmpDataSource
                      delegate:tmpDelegate
                showPickerType:kCustomActionSheetDawnDusk];
}

- (void)dealloc  {
    //DebugLog(@"DEALLOC");
}

- (void)setupUI {
    [self setBackgroundColor:[self getColourForType:kCustomActionSheetBackgroundColour]];
    
    // Top Highlight
    UIView *_topHighlightView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, 0.0f)];
    [_topHighlightView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_topHighlightView];
    
    // Add Radio Buttons
    float x = 0.0f;

if ([AppStyleHelper isKlikaan])
    x = 25.0f;
else if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO])
    x = 45.0f;
else if ([AppStyleHelper isNexa])
    x = 15.0f;
    
    self.heightOffset = 0.0f;
    
    // Top Header
    UIView *_topHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.heightOffset, self.frame.size.width, 30.0f)];
    [_topHeaderView setTag:-1];
    //_topHeaderView.layer.borderColor = [UIColor blackColor].CGColor;
    //_topHeaderView.layer.borderWidth = 1.0f;
    [_topHeaderView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_topHeaderView];
    self.heightOffset += _topHeaderView.frame.size.height;
    
    x = [self addRadioButtonWithText:NSLocalizedString(@"dawn", @"Dawn")
                                 tag:kCustomActionSheetTimePickerDawn
                            selected:self.selectedPickerType == kCustomActionSheetTimePickerDawn ? YES : NO
                              toView:_topHeaderView
                                 atX:x
                            selector:@selector(headerOptionPressed:)];
    
    x += 15.0f;
    
    x = [self addRadioButtonWithText:NSLocalizedString(@"dusk", @"Dusk")
                                 tag:kCustomActionSheetTimePickerDusk
                            selected:self.selectedPickerType == kCustomActionSheetTimePickerDusk ? YES : NO
                              toView:_topHeaderView
                                 atX:x
                            selector:@selector(headerOptionPressed:)];
    
    x += 15.0f;
    
    [self addRadioButtonWithText:NSLocalizedString(@"time", @"Time")
                             tag:kCustomActionSheetTimePickerTime
                        selected:self.selectedPickerType == kCustomActionSheetTimePickerTime ? YES : NO
                          toView:_topHeaderView
                             atX:x
                        selector:@selector(headerOptionPressed:)];
    
    // Get refs
    self.dawnButton = (UIButton *)[self getButtonFromView:_topHeaderView tag:kCustomActionSheetTimePickerDawn];
    self.duskButton = (UIButton *)[self getButtonFromView:_topHeaderView tag:kCustomActionSheetTimePickerDusk];
    self.timeButton = (UIButton *)[self getButtonFromView:_topHeaderView tag:kCustomActionSheetTimePickerTime];
    
    // ActionSheet
    if (self.showPickerType == kCustomActionSheetDawnDusk) {
        [self createPicker];
        self.heightOffset += self.timePickerView.frame.size.height;
        self.heightOffset += lwActionSheetButtonPadding;
    }
    
    // Set Highlight Height
    [_topHighlightView setFrameHeight:self.heightOffset + 1.0f];
    
    // Apply Highlight
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _topHighlightView.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[self getColourForType:kCustomActionSheetGradientStart] CGColor],
                       (id)[[self getColourForType:kCustomActionSheetGradientEnd] CGColor],
                       nil];
    [_topHighlightView.layer insertSublayer:gradient atIndex:0];
    
    // Create Buttons
    NSUInteger tmpTag = 0;
    for (NSString *tmpTitle in self.buttonTitlesArray) {
        [self addButtonType:kCustomActionSheetButton title:tmpTitle tag:tmpTag];
        tmpTag += 1;
    }
    
    // Create Cancel Button
    self.heightOffset += lwActionSheetButtonCancelPadding;
    [self addButtonType:kCustomActionSheetCancelButton title:self.cancelTitle tag:tmpTag];
    
    self.heightOffset += lwActionSheetBottomPadding;
    [self setFrameHeight:self.heightOffset];
}

- (float)addRadioButtonWithText:(NSString *)_text
                            tag:(NSUInteger)_bTag
                       selected:(BOOL)_selected
                         toView:(UIView *)_view
                            atX:(float)_x
                       selector:(SEL)_selector {
    
    UIButton *_button = [ButtonsHelper getButtonWithText:_text
                                                    type:kButtonRadio
                                                     tag:_bTag
                                                   width:65.0f
                                                  height:26.0f
                                                  target:self
                                                selector:_selector];
    
    CGSize size = [_text
                   sizeWithFont:_button.titleLabel.font
                   constrainedToSize:CGSizeMake(CGFLOAT_MAX, _button.titleLabel.frame.size.height)
                   lineBreakMode:NSLineBreakByWordWrapping];
    [_button setFrameWidth:size.width + 30.0f];
    [_button setFrameX:_x];
    [_button setCenterY:_view.center.y];

    if ([AppStyleHelper isMegaMan]) {
        [_button setBackgroundImage:nil forState:UIControlStateNormal];
        [_button setBackgroundImage:nil forState:UIControlStateHighlighted];
        [_button setBackgroundImage:nil forState:UIControlStateSelected];
        
        [_button setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateLo] forState:UIControlStateNormal];
        [_button setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateHighlighted];
        [_button setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateSelected];
    }
    
    [_button setSelected:_selected];
    [_view addSubview:_button];
    _x += _button.frame.size.width;
    return _x;
}

- (UIButton *)getButtonFromView:(UIView *)_searchView tag:(NSUInteger)_bTag {
    return (UIButton *)[_searchView viewWithTag:_bTag];
}

- (void)createPicker {
    CGRect _frame = CGRectMake(0.0f, self.heightOffset, 0.0f, 180.0f);
    if (self.timePickerView) {
        _frame = self.timePickerView.frame;
        [self.timePickerView removeFromSuperview];
        self.timePickerView = nil;
    }
    
    self.timePickerView = [[DistancePickerView alloc] initWithFrame:_frame];
    self.timePickerView.delegate = self;
    self.timePickerView.showsSelectionIndicator = YES;
    self.timePickerView.layer.borderColor = [UIColor blackColor].CGColor;
    self.timePickerView.layer.borderWidth = 1.0f;
    
    // Set Width
    [self setupPickerWidth];
    
    // Add Labels
    [self addLabelsToPicker];
    
    [self addSubview:self.timePickerView];
    
    // Create / Remove Radio Buttons
    [self setupDuskDawnButtons];
}

- (void)setupDuskDawnButtons {
    [self removeDuskDawnButtons];
    
    switch (self.selectedPickerType) {
            
        case kCustomActionSheetTimePickerDawn:
        {
            [self addDawnButtons];
        }
            break;
            
        case kCustomActionSheetTimePickerDusk:
        {
            [self addDuskButtons];
        }
            break;
            
        case kCustomActionSheetTimePickerTime:
        {
            
            if ([self numberOfComponentsInPickerView:self.timePickerView] > 0) {
                NSInteger _row = 16368;
                if (_row % kHourNumberOfRows == 0) {
                    [self.timePickerView selectRow:_row inComponent:0 animated:NO];
                }
            }
            
            if ([self numberOfComponentsInPickerView:self.timePickerView] > 1) {
                NSInteger _row = 16380;
                if (_row % kMinuteNumberOfRows == 0) {
                    [self.timePickerView selectRow:_row inComponent:1 animated:NO];
                }
            }

        }
            break;
            
        default:
            break;
    }

    [self setType];
}

- (void)setType {
    switch (self.selectedPickerType) {
            
        case kCustomActionSheetTimePickerDawn:
        {
            self.selectedDuskDawnType = self.beforeDawnButton.selected ? kDawnDuskActionSheetBeforeDawn : kDawnDuskActionSheetAfterDawn;
        }
            break;
            
        case kCustomActionSheetTimePickerDusk:
        {
            self.selectedDuskDawnType = self.beforeDuskButton.selected ? kDawnDuskActionSheetBeforeDusk : kDawnDuskActionSheetAfterDusk;
        }
            break;
            
        case kCustomActionSheetTimePickerTime:
        {
            self.selectedDuskDawnType = kDawnDuskActionSheetTime;
        }
            break;
            
        default:
            break;
    }
}

- (void)addDawnButtons {
    self.beforeDawnButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"before_dawn", @"Before Dawn")
                                                        type:kButtonRadio
                                                         tag:kDawnDuskActionSheetBeforeDawn
                                                       width:120.0f
                                                      height:26.0f
                                                      target:self
                                                    selector:@selector(dawnOptionPressed:)];
    
    if ([AppStyleHelper isMegaMan]) {
        [self.beforeDawnButton setBackgroundImage:nil forState:UIControlStateNormal];
        [self.beforeDawnButton setBackgroundImage:nil forState:UIControlStateHighlighted];
        [self.beforeDawnButton setBackgroundImage:nil forState:UIControlStateSelected];
        
        [self.beforeDawnButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateLo] forState:UIControlStateNormal];
        [self.beforeDawnButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateHighlighted];
        [self.beforeDawnButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateSelected];
    }
    
    CGSize size = [self.beforeDawnButton.titleLabel.text
                   sizeWithFont:self.beforeDawnButton.titleLabel.font
                   constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.beforeDawnButton.titleLabel.frame.size.height)
                   lineBreakMode:NSLineBreakByWordWrapping];
    [self.beforeDawnButton setFrameWidth:size.width + 30.0f];
    
    [self.beforeDawnButton alignToRightOfView:self.timePickerView padding:10.0f];
    [self.beforeDawnButton setCenterY:self.timePickerView.center.y];
    [self.beforeDawnButton setSelected:YES];
    [self addSubview:self.beforeDawnButton];
    
    self.afterDawnButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"after_dawn", @"After Dawn")
                                                        type:kButtonRadio
                                                         tag:kDawnDuskActionSheetAfterDawn
                                                       width:120.0f
                                                      height:26.0f
                                                      target:self
                                                    selector:@selector(dawnOptionPressed:)];
    
    if ([AppStyleHelper isMegaMan]) {
        [self.afterDawnButton setBackgroundImage:nil forState:UIControlStateNormal];
        [self.afterDawnButton setBackgroundImage:nil forState:UIControlStateHighlighted];
        [self.afterDawnButton setBackgroundImage:nil forState:UIControlStateSelected];
        
        [self.afterDawnButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateLo] forState:UIControlStateNormal];
        [self.afterDawnButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateHighlighted];
        [self.afterDawnButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateSelected];
    }
    
    size = [self.afterDawnButton.titleLabel.text
            sizeWithFont:self.afterDawnButton.titleLabel.font
            constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.afterDawnButton.titleLabel.frame.size.height)
            lineBreakMode:NSLineBreakByWordWrapping];
    [self.afterDawnButton setFrameWidth:size.width + 30.0f];
    
    [self.afterDawnButton alignToBottomOfView:self.beforeDawnButton padding:10.0f matchHorizontal:YES];
    [self addSubview:self.afterDawnButton];
}

- (void)addDuskButtons {
    self.beforeDuskButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"before_dusk", @"Before Dusk")
                                                        type:kButtonRadio
                                                         tag:kDawnDuskActionSheetBeforeDusk
                                                       width:120.0f
                                                      height:26.0f
                                                      target:self
                                                    selector:@selector(duskOptionPressed:)];
    
    if ([AppStyleHelper isMegaMan]) {
        [self.beforeDuskButton setBackgroundImage:nil forState:UIControlStateNormal];
        [self.beforeDuskButton setBackgroundImage:nil forState:UIControlStateHighlighted];
        [self.beforeDuskButton setBackgroundImage:nil forState:UIControlStateSelected];
        
        [self.beforeDuskButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateLo] forState:UIControlStateNormal];
        [self.beforeDuskButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateHighlighted];
        [self.beforeDuskButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateSelected];
    }
    
    CGSize size = [self.beforeDuskButton.titleLabel.text
                   sizeWithFont:self.beforeDuskButton.titleLabel.font
                   constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.beforeDuskButton.titleLabel.frame.size.height)
                   lineBreakMode:NSLineBreakByWordWrapping];
    [self.beforeDuskButton setFrameWidth:size.width + 30.0f];
    
    [self.beforeDuskButton alignToRightOfView:self.timePickerView padding:10.0f];
    [self.beforeDuskButton setCenterY:self.timePickerView.center.y];
    [self.beforeDuskButton setSelected:YES];
    [self addSubview:self.beforeDuskButton];
    
    self.afterDuskButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"after_dusk", @"After Dusk")
                                                       type:kButtonRadio
                                                        tag:kDawnDuskActionSheetAfterDusk
                                                      width:120.0f
                                                     height:26.0f
                                                     target:self
                                                   selector:@selector(duskOptionPressed:)];
    
    if ([AppStyleHelper isMegaMan]) {
        [self.afterDuskButton setBackgroundImage:nil forState:UIControlStateNormal];
        [self.afterDuskButton setBackgroundImage:nil forState:UIControlStateHighlighted];
        [self.afterDuskButton setBackgroundImage:nil forState:UIControlStateSelected];
        
        [self.afterDuskButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateLo] forState:UIControlStateNormal];
        [self.afterDuskButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateHighlighted];
        [self.afterDuskButton setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateSelected];
    }
    
    size = [self.afterDuskButton.titleLabel.text
            sizeWithFont:self.afterDuskButton.titleLabel.font
            constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.afterDuskButton.titleLabel.frame.size.height)
            lineBreakMode:NSLineBreakByWordWrapping];
    [self.afterDuskButton setFrameWidth:size.width + 30.0f];
    
    [self.afterDuskButton alignToBottomOfView:self.beforeDuskButton padding:10.0f matchHorizontal:YES];
    [self addSubview:self.afterDuskButton];
}

- (void)removeDuskDawnButtons {
    if (self.beforeDawnButton) {
        [self.beforeDawnButton removeFromSuperview];
        self.beforeDawnButton = nil;
    }
    
    if (self.afterDawnButton) {
        [self.afterDawnButton removeFromSuperview];
        self.afterDawnButton = nil;
    }
    
    if (self.beforeDuskButton) {
        [self.beforeDuskButton removeFromSuperview];
        self.beforeDuskButton = nil;
    }
    
    if (self.afterDuskButton) {
        [self.afterDuskButton removeFromSuperview];
        self.afterDuskButton = nil;
    }
}

- (void)addLabelsToPicker {
    switch (self.selectedPickerType) {
            
        case kCustomActionSheetTimePickerDawn:
        {
            NSString *_str = NSLocalizedString(@"minutes", @"Mins");
            [self.timePickerView addLabel:_str forComponent:0 forLongestString:_str];
        }
            break;
            
        case kCustomActionSheetTimePickerDusk:
        {
            NSString *_str = NSLocalizedString(@"minutes", @"Mins");
            [self.timePickerView addLabel:_str forComponent:0 forLongestString:_str];
        }
            break;
            
        case kCustomActionSheetTimePickerTime:
        {
            [self.timePickerView addLabel:NSLocalizedString(@"hours", @"Hours") forComponent:0 forLongestString:NSLocalizedString(@"hours", @"Hours")];
            [self.timePickerView addLabel:NSLocalizedString(@"minutes", @"Mins") forComponent:1 forLongestString:NSLocalizedString(@"minutes", @"Mins")];
        }
            break;
            
        default:
            break;
    }
}

- (void)setupPickerWidth {
    switch (self.selectedPickerType) {
            
        case kCustomActionSheetTimePickerDawn:
        {
            [self.timePickerView setFrameWidth:SCREEN_WIDTH() / 2];
        }
            break;
            
        case kCustomActionSheetTimePickerDusk:
        {
            [self.timePickerView setFrameWidth:SCREEN_WIDTH() / 2];
        }
            break;
            
        case kCustomActionSheetTimePickerTime:
        {
            [self.timePickerView setFrameWidth:SCREEN_WIDTH()];
        }
            break;
            
        default:
            break;
    }
}

- (void)refreshPickerWithSelection {
    [self createPicker];
    [self.timePickerView reloadAllComponents];
}

#pragma mark - UIPickerView Delegate

- (void)pickerView:(UIPickerView *)tmpPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    // For Time use
    // NSInteger actualRow = row % kHourNumberOfRows;
    // NSInteger actualRow = row % kMinuteNumberOfRows;
}

- (NSString *)pickerView:(UIPickerView *)tmpPickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    switch (self.selectedPickerType) {
            
        case kCustomActionSheetTimePickerDawn:
        {
            return [NSString stringWithFormat:@"%d", row * 30];
        }
            break;
            
        case kCustomActionSheetTimePickerDusk:
        {
            return [NSString stringWithFormat:@"%d", row * 30];
        }
            break;
            
        case kCustomActionSheetTimePickerTime:
        {
            //return [NSString stringWithFormat:@"%02d", row];
            
            switch (component) {
                case 0:
                    return [NSString stringWithFormat:@"%02d", row % kHourNumberOfRows];
                    break;
                    
                case 1:
                    return [NSString stringWithFormat:@"%02d", row % kMinuteNumberOfRows];
                    break;
                    
                default:
                    return 0;
                    break;
            }

        }
            break;
            
        default:
            return @"";
            break;
    }
}

/*
 
 iOS 7 fix to use the return view delegate as it offers more functionality in styling the row
 
 */

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                               0,
                                                               pickerView.frame.size.width / ([self numberOfComponentsInPickerView:pickerView] + 1),
                                                               44)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    label.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return label;
}

#pragma mark - UIPickerView DataSource

- (NSInteger)pickerView:(UIPickerView *)tmpPickerView numberOfRowsInComponent:(NSInteger)component {
    switch (self.selectedPickerType) {
            
        case kCustomActionSheetTimePickerDawn:
        {
            switch (component) {
                case 0:
                    return 5;
                    break;
                    
                default:
                    return 0;
                    break;
            }
        }
            break;
            
        case kCustomActionSheetTimePickerDusk:
        {
            switch (component) {
                case 0:
                    return 5;
                    break;
                    
                default:
                    return 0;
                    break;
            }
        }
            break;
            
        case kCustomActionSheetTimePickerTime:
        {
            switch (component) {
                case 0:
                    //return 24;
                    return INT16_MAX;
                    break;
                    
                case 1:
                    //return 60;
                    return INT16_MAX;
                    break;
                    
                default:
                    return 0;
                    break;
            }
        }
            break;
            
        default:
            return 0;
            break;
    }
}

- (CGFloat)pickerView:(UIPickerView *)tmpPickerView widthForComponent:(NSInteger)component {
    return (self.timePickerView.frame.size.width - 20.0f) / [self numberOfComponentsInPickerView:tmpPickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)tmpPickerView {
    switch (self.selectedPickerType) {
            
        case kCustomActionSheetTimePickerDawn:
        {
            return 1;
        }
            break;
            
        case kCustomActionSheetTimePickerDusk:
        {
            return 1;
        }
            break;
            
        case kCustomActionSheetTimePickerTime:
        {
            return 2;
        }
            break;
            
        default:
            return 0;
            break;
    }
}

#pragma mark - Actions

- (IBAction)headerOptionPressed:(id)sender {
    [self.dawnButton setSelected:NO];
    [self.duskButton setSelected:NO];
    [self.timeButton setSelected:NO];
    
    UIButton *_button = (UIButton *)sender;
    [_button setSelected:YES];
    
    self.selectedPickerType = _button.tag;
    
    [self refreshPickerWithSelection];
}

- (IBAction)dawnOptionPressed:(id)sender {
    [self.beforeDawnButton setSelected:NO];
    [self.afterDawnButton setSelected:NO];
    
    UIButton *_button = (UIButton *)sender;
    [_button setSelected:YES];
    
    [self setType];
}

- (IBAction)duskOptionPressed:(id)sender {
    [self.beforeDuskButton setSelected:NO];
    [self.afterDuskButton setSelected:NO];
    
    UIButton *_button = (UIButton *)sender;
    [_button setSelected:YES];
    
    [self setType];
}

// Override super
- (IBAction)buttonPressed:(id)sender {
    if (self.showPickerType == kCustomActionSheetDawnDusk) {
        if ([self.delegate respondsToSelector:@selector(actionSheetButtonPressed:index:pickerView:type:)])
            [self.delegate actionSheetButtonPressed:self
                                              index:[sender tag]
                                         pickerView:self.timePickerView
                                               type:self.selectedDuskDawnType];
    }
}

@end
