//
//  SettingsVC.h
//  LightwaveRF
//
//  Created by Nik Lever on 03/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

typedef enum
{
    kSettingsVCLogoutAlert = 1001,
    kSettingsVCSaveSettingsAlert,
    kSettingsVCGetSettingsAlert,
    kSettingsVCSetLocationAlert,
    kSettingsVCTimeZoneAlert,
    kSettingsVCSSIDAlert,
    kSettingsVCSyncEventsAlert
} SettingsVCAlertTags;

typedef enum
{
    kSettingsVCNameTextField = 2001,
    kSettingsVCEmailTextField,
    kSettingsVCPinTextField,
    kSettingsVCElecRateTextField,
    kSettingsVCimpKwhTextField,
    kSettingsVCWiFiLabel,
    kSettingsVCUseWiFiNameSwitch,
    kSettingsVCStayIn3GSwitch,
    kSettingsVCShowVideoSwitch,
    kSettingsVCLoginAutoSwitch,
    kSettingsVCShowFooterSwitch,
    kSettingsVCPopupsSwitch
} SettingsVCTags;

@interface SettingsVC : RootViewController <UIGestureRecognizerDelegate> {

}

- (IBAction)testWifiChanged:(id)sender;
- (IBAction)showVideoChanged:(id)sender;
- (IBAction)loginAutoChanged:(id)sender;

@end
