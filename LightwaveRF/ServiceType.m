//
//  ServiceType.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 25/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ServiceType.h"
#import "LWErrorView.h"

@interface ServiceType ()
@property (nonatomic, assign) BOOL isShowingPopup;
@end

@implementation ServiceType

@synthesize isShowingPopup;

- (id)init {
    if (self = [super init]) {
        self.isShowingPopup = NO;
        [NSTimer scheduledTimerWithTimeInterval:kPurgeExpiredRequestsTime
                                         target:self
                                       selector:@selector(purgeExpiredRequests:)
                                       userInfo:nil
                                        repeats:YES];
    }
    return self;
}

- (void)purgeExpiredRequests:(NSTimer *)timer {
    // Overrideen by children
}

- (void)cleanupAllRequests {
    // Overrideen by children
}

#pragma mark - Retrieve current service mode

- (NSString *)getServiceModeKey {
    return [NSString stringWithFormat:@"ServiceType-%@", NSStringFromSelector(_cmd)];
}

- (void)setServiceType:(ServiceTypeTags)_serviceType {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *_key = [self getServiceModeKey];
    //DebugLog(@"Set Service Mode To: %@, For Key: %@", _serviceType == kServiceUDP ? @"UDP" : @"Web", _key);
    [userDefaults setInteger:_serviceType forKey:_key];
    [userDefaults synchronize];
    
    // Update State Listers
    if (_serviceType == kServiceUDP)
        [[NSNotificationCenter defaultCenter] postNotificationName:kUDPCheckStateSuccessNotificationKey object:nil];
    else
        [[NSNotificationCenter defaultCenter] postNotificationName:kUDPCheckStateFailNotificationKey object:nil];
}

- (ServiceTypeTags)getServiceType {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *_key = [self getServiceModeKey];
    NSInteger _int = [userDefaults integerForKey:_key];
    
    ServiceTypeTags _serviceMode = kServiceUDP;
    
    if (_int > 0) {
        _serviceMode = _int;
    }

    return _serviceMode;
}

#pragma mark - Toggled Service? : UDP | Web

- (void)isService:(ServiceTypeTags)_usedServiceType {
    [self isService:_usedServiceType showAlert:YES];
}

- (void)isService:(ServiceTypeTags)_usedServiceType showAlert:(BOOL)_showAlert {
    ServiceTypeTags _currentType = [self getServiceType];
    
    //DebugLog(@"Check Service is %@", _usedServiceType == kServiceUDP ? @"UDP" : @"Web");
    
    if (_currentType != _usedServiceType) {
        
        // Set current service type
        [self setServiceType:_usedServiceType];
        
        if (_showAlert) {
            // Alert user of the change
            if (!self.isShowingPopup) {
                
                BOOL enableWFLPopups = [[UserPrefsHelper shared] getEnableWFLPopups];
                
                if (enableWFLPopups) {
                    self.isShowingPopup = YES;
                    LWErrorView *lwErrorView = [[LWErrorView alloc] initWithTitle:NSLocalizedString(@"info", @"Info")
                                                                          message:_usedServiceType == kServiceUDP ?
                                                NSLocalizedString(@"remote_error5", @"use udp") : NSLocalizedString(@"remote_error4", @"use remote")
                                                                         delegate:self
                                                                cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                                                                otherButtonTitles:nil];
                    [lwErrorView setTag:kServiceUDPSwitchingAlert];
                    [lwErrorView show];
                } else {
                    self.isShowingPopup = NO;
                }
            }
        }
    } else {
        
        // Set current service type
        [self setServiceType:_usedServiceType];
    }
}

#pragma mark - LWErrorView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch ([alertView tag]) {
            
        case kServiceUDPSwitchingAlert:
        {
            self.isShowingPopup = NO;
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Commands

+ (NSString *)getStopAllEventsCommand {
    // stop_all_events
    return [NSString stringWithFormat:@"!FcP\"*\"|%@", NSLocalizedString(@"stop_all_events", @"Stop all events")];
}

+ (NSString *)getDeleteAllSequencesAndTimersCommand {
    return [NSString stringWithFormat:@"!FxP\"*\"|%@ %@|%@",
            NSLocalizedString(@"delete", @"Delete"),
            NSLocalizedString(@"all", @"all"),
            NSLocalizedString(@"commands", @"commands")];
}

+ (NSString *)getPollCommand {
    return @"@?v";
}

+ (NSString *)getMeterCommand {
    return @"@?W";
}

+ (NSString *)getLockCommandWithZoneID:(int)_roomID device:(Device *)_device {
    if (_device) {
       return [NSString stringWithFormat:@"!R%dD%dFl|%@|%@",
               _roomID,
               _device.deviceID,
               _device.name,
               NSLocalizedString(@"device_locked", @"device locked")];
    } else
        return nil;
}

+ (NSString *)getFullLockCommandWithZoneID:(int)_roomID device:(Device *)_device {
    if (_device) {
        return [NSString stringWithFormat:@"!R%dD%dFk|%@|%@",
                _roomID,
                _device.deviceID,
                _device.name,
                NSLocalizedString(@"device_fully_locked", @"device fully locked")];
    } else
        return nil;
}

+ (NSString *)getUnlockCommandWithZoneID:(int)_roomID device:(Device *)_device {
    if (_device) {
        return [NSString stringWithFormat:@"!R%dD%dFu|%@|%@",
                _roomID,
                _device.deviceID,
                _device.name,
                NSLocalizedString(@"device_unlocked", @"device unlocked")];
    } else
        return nil;
}

+ (NSString *)getOnOffCommandWithZoneID:(int)_roomID device:(Device *)_device isOn:(BOOL)_isOn {
    if (_device) {
        return [NSString stringWithFormat:@"!R%dD%dF%d|%@|%@",
                _roomID,
                _device.deviceID,
                _isOn,
                _device.name,
                _isOn ? NSLocalizedString(@"on", @"on") : NSLocalizedString(@"off", @"off")];
    } else
        return nil;
}

+ (NSString *)getOpenCloseCommandWithZoneID:(int)_roomID device:(Device *)_device openClose:(ConstantsIsOpenClose)_openClose {
    NSString *_str = _openClose == kConstantOpen ? NSLocalizedString(@"open", @"open") : (_openClose == kConstantClose ? NSLocalizedString(@"close", @"close") : NSLocalizedString(@"stop", @"stop"));
    
    if (_device) {
        if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO]) {
            return [NSString stringWithFormat:@"!R%dD%dF%@|%@|%@",
                    _roomID,
                    _device.deviceID,
                    //_openClose == kConstantOpen ? @"(" : (_openClose == kConstantClose ? @")" : @"^"),
                    _openClose == kConstantOpen ? @"1" : (_openClose == kConstantClose ? @"0" : @"^"),
                    _device.name,
                    [_str lowercaseString]];
        }else {
            return [NSString stringWithFormat:@"!R%dD%dF%@|%@|%@",
                    _roomID,
                    _device.deviceID,
                    _openClose == kConstantOpen ? @"1" : (_openClose == kConstantClose ? @"0" : @"^"),
                    _device.name,
                    [_str lowercaseString]];
        }
        
    } else
        return nil;            
}

+ (NSString *)getDimCommandWithZoneID:(int)_roomID device:(Device *)_device percentage:(float)_percentage {
    // _percentage = 0% - 100%
    NSUInteger _dimValue = (_percentage / 100) * DIMMER_MAX;
    if (_dimValue < DIMMER_MIN)
        _dimValue = DIMMER_MIN;
    else if (_dimValue > DIMMER_MAX)
        _dimValue = DIMMER_MAX;
    
    if (_device) {
        return [NSString stringWithFormat:@"!R%dD%dFdP%d|%@|%.0f%%",
                _roomID,
                _device.deviceID,
                _dimValue,
                _device.name,
                _percentage];
    } else
        return nil;
}

+ (NSString *)getLEDCommandWithZoneID:(int)_roomID device:(Device *)_device colourCode:(int)_code {
    return [NSString stringWithFormat:@"!R%dD%dF*cP%d|%@|%d",
            _roomID,
            _device.deviceID,
            _code,
            _device.name,
            _code];
}

+ (NSString *)getLEDCycleCommandWithZoneID:(int)_roomID device:(Device *)_device {
    return [NSString stringWithFormat:@"!R%dD%dF*y|%@|%@",
            _roomID,
            _device.deviceID,
            _device.name,
            NSLocalizedString(@"change_cycling", @"change_cycling")];
}

+ (NSString *)getMoodCommandWithZoneID:(int)_roomID mood:(Mood *)_mood {
    // Mood 1-3, 4 = off
    
    if (_mood) {
        if ([_mood isAllOff]) {
            return [NSString stringWithFormat:@"!R%dFa|%@|%@",
                    _roomID,
                    NSLocalizedString(@"all_off", @"All off"),
                    _mood.zone.name];
            
        } else {
            return [NSString stringWithFormat:@"!R%dFmP%d|%@|%@",
                    _roomID,
                    _mood.moodID,
                    NSLocalizedString(@"start_mood", @"start mood"),
                    _mood.name];
        }
    } else
        return nil;
}

+ (NSString *)getSaveMoodCommandWithMood:(Mood *)_mood {
    if (_mood) {
        return [NSString stringWithFormat:@"!R%dFsP%d|%@|%@",
                _mood.zoneID,
                _mood.moodID,
                NSLocalizedString(@"save_mood", @"save mood"),
                _mood.name];
    } else
        return nil;
}

+ (NSString *)getPauseTimerCommand:(LTimer *)_timer {
    if (_timer) {
        return [NSString stringWithFormat:@"!FxP\"%@\"|%@",
                _timer.name,
                NSLocalizedString(@"pausing_timer", @"Pausing Timer")];
    } else
        return nil;
}

+ (NSString *)getDeleteTimerCommand:(LTimer *)_timer {
    if (_timer) {
        return [NSString stringWithFormat:@"!FxP\"%@\"|%@",
                _timer.name,
                NSLocalizedString(@"deleting_timer", @"Deleting Timer")];
    } else
        return nil;
}

+ (NSString *)getLatLongCommand:(NSString *)_latLong {
    //!FpP"+-ooo.oo,+-lll.ll"
    //!FpP"+-< longtitude decimal format >,+-< latitude decimal format >"
    //!FpP”-005.20,035.56” = Birmingham
    
    if (_latLong.length > 0) {
        return [NSString stringWithFormat:@"!FpP\"%@\"|%@",
                _latLong,
                NSLocalizedString(@"location_set", @"Location Set")];
    } else
        return nil;
    
}

+ (NSString *)getTimeZoneCommandWithValue:(int)_value {
    return [NSString stringWithFormat: @"!FzP%d",
            _value,
            NSLocalizedString(@"timezone_set", @"timezone_set")];
}

+ (NSString *)getImpKwhCommand:(NSString *)_impKwh {
    if (_impKwh.length > 0) {
        return [NSString stringWithFormat:@"!ReD1F*kP%@|%@",
                _impKwh,
                NSLocalizedString(@"impkwh", @"Imp/Kwh")];
    } else
        return nil;
}

@end
