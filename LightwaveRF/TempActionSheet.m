//
//  TempActionSheet.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 06/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "TempActionSheet.h"

@interface TempActionSheet ()
@property (nonatomic, assign) NSUInteger numComponents;
@end

@implementation TempActionSheet

@synthesize tempPickerView;
@synthesize numComponents;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
  showNumComponents:(NSUInteger)tmpNumComponents
{
    self.numComponents = tmpNumComponents;
    
    return [self initWithPoint:point
                  buttonTitles:tmpButtonTitles
                   cancelTitle:tmpCancelTitle
                    dataSource:tmpDataSource
                      delegate:tmpDelegate
                showPickerType:kCustomActionSheetTempPicker];
}

- (void)dealloc  {
    //DebugLog(@"DEALLOC");
}

- (void)setupUI {
    [self setBackgroundColor:[self getColourForType:kCustomActionSheetBackgroundColour]];
    
    // Top Highlight
    UIView *_topHighlightView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, 0.0f)];
    [_topHighlightView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_topHighlightView];
    
    if (self.showPickerType == kCustomActionSheetTempPicker) {
        // Show Time Picker?
        self.heightOffset = 0.0f;
        self.tempPickerView = [[DistancePickerView alloc] initWithFrame:CGRectMake(0.0f, self.heightOffset, SCREEN_WIDTH(), 180.0f)];
        self.tempPickerView.delegate = self;
        self.tempPickerView.showsSelectionIndicator = YES;
        [self addSubview:self.tempPickerView];
        
        if (self.numComponents >= 1)
            [self.tempPickerView addLabel:@"°C" forComponent:0 forLongestString:@"°C"];
            //[self.tempPickerView addLabel:NSLocalizedString(@"temp", @"Temp") forComponent:0 forLongestString:NSLocalizedString(@"temp", @"Temp")];
        
        if (self.numComponents >= 2)
            [self.tempPickerView addLabel:NSLocalizedString(@"hours", @"Hours") forComponent:1 forLongestString:NSLocalizedString(@"hours", @"Hours")];
        
        if (self.numComponents >= 3)
            [self.tempPickerView addLabel:NSLocalizedString(@"minutes", @"Mins") forComponent:2 forLongestString:NSLocalizedString(@"minutes", @"Mins")];
        
        self.heightOffset += self.tempPickerView.frame.size.height;
        self.heightOffset += lwActionSheetButtonPadding;
    }
    
    // Set Highlight Height
    [_topHighlightView setFrameHeight:self.heightOffset + 1.0f];
    
    // Apply Highlight
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _topHighlightView.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[self getColourForType:kCustomActionSheetGradientStart] CGColor],
                       (id)[[self getColourForType:kCustomActionSheetGradientEnd] CGColor],
                       nil];
    [_topHighlightView.layer insertSublayer:gradient atIndex:0];
    
    // Create Buttons
    NSUInteger tmpTag = 0;
    for (NSString *tmpTitle in self.buttonTitlesArray) {
        [self addButtonType:kCustomActionSheetButton title:tmpTitle tag:tmpTag];
        tmpTag += 1;
    }
    
    // Create Cancel Button
    self.heightOffset += lwActionSheetButtonCancelPadding;
    [self addButtonType:kCustomActionSheetCancelButton title:self.cancelTitle tag:tmpTag];
    
    self.heightOffset += lwActionSheetBottomPadding;
    [self setFrameHeight:self.heightOffset];
}

#pragma mark - Public

- (void)resetPickerValues {
    if (self.showPickerType == kCustomActionSheetTempPicker) {
        if (self.numComponents >= 1)
            [self.tempPickerView selectRow:0 inComponent:0 animated:NO];
        
        if (self.numComponents >= 2)
            [self.tempPickerView selectRow:0 inComponent:1 animated:NO];
        
        if (self.numComponents >= 3)
            [self.tempPickerView selectRow:0 inComponent:2 animated:NO];
    }
}

#pragma mark - UIPickerView Delegate

- (void)pickerView:(UIPickerView *)tmpPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
}

- (NSString *)pickerView:(UIPickerView *)tmpPickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0)
        return [NSString stringWithFormat:@"%d", row + kTempMin];
    
    else if (component == 1)
        return [NSString stringWithFormat:@"%d", row];
    
    else
        return [NSString stringWithFormat:@"%d", row * 15];
}

/*
 
 iOS 7 fix to use the return view delegate as it offers more functionality in styling the row
 
 */

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                               0,
                                                               pickerView.frame.size.width / ([self numberOfComponentsInPickerView:pickerView] + 1),
                                                               44)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    label.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return label;
}

#pragma mark - UIPickerView DataSource

- (NSInteger)pickerView:(UIPickerView *)tmpPickerView numberOfRowsInComponent:(NSInteger)component {
    switch (component) {
        case 0:
            return kTempMax - kTempMin;
            break;
            
        case 1:
            return 24;
            break;
            
        case 2:
            return 4;
            break;
            
        default:
            return 0;
            break;
    }
}

- (CGFloat)pickerView:(UIPickerView *)tmpPickerView widthForComponent:(NSInteger)component {
    return 300 / [self numberOfComponentsInPickerView:tmpPickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)tmpPickerView {
    return self.numComponents;
}

#pragma mark - Actions

- (IBAction)buttonPressed:(id)sender {
    if (self.showPickerType == kCustomActionSheetTempPicker) {
        if ([self.delegate respondsToSelector:@selector(actionSheetButtonPressed:index:pickerView:)])
            [self.delegate actionSheetButtonPressed:self index:[sender tag] pickerView:self.tempPickerView];
        
    }
}

@end
