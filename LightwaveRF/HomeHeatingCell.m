//
//  ChooseProductCell.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 18/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
// 

#import "HomeHeatingCell.h"
#import "ButtonsHelper.h"

@interface HomeHeatingCell ()
@property (nonatomic, strong) UIButton *programButton;
@property (nonatomic, strong) UIButton *setTempButton;
@end

@implementation HomeHeatingCell

@synthesize zone;
@synthesize active_img;
@synthesize fadeActive_tmr;
@synthesize boostFadeActive_tmr;
@synthesize delegate;
@synthesize indexPath;
@synthesize programButton, setTempButton;

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    _boostActive_img.hidden = YES;
    active_img.hidden = YES;
    
    if ([AppStyleHelper isMegaMan]) {
        //[self.current_sld setTrackTintColor:[UIColor colorWithDivisionOfRed:137 green:197 blue:39 alpha:1.0f]];
        [self.current_sld setProgressTintColor:[UIColor colorWithDivisionOfRed:137 green:197 blue:39 alpha:1.0f]];
    }
    
    [self.current_localised_lbl setText:[NSLocalizedString(@"current", @"current") lowercaseString]];
    [self.target_localised_lbl setText:[NSLocalizedString(@"target", @"target") lowercaseString]];
    
}

- (void)dealloc {
    if (fadeActive_tmr != nil){
        [fadeActive_tmr invalidate];
    }
    
    if (boostFadeActive_tmr != nil){
        [boostFadeActive_tmr invalidate];
    }
}

+ (float)getCellHeight {
    return 96.0f;
}

- (void)setupCellWithZone:(Zone*)_zone
                indexPath:(NSIndexPath *)_indexPath
                 delegate:(id<HomeHeatingCellDelegate>)_delegate {
    
    self.zone = _zone;
    self.delegate = _delegate;
    self.indexPath = _indexPath;
    
    /*************************************
     *  Button
     *************************************/
    if (!self.programButton) {
        self.programButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"program", @"Program")
                                                         type:kButtonGreyBacking
                                                          tag:kBtnProgram
                                                        width:89
                                                       height:38
                                                       target:self
                                                     selector:@selector(programPressed:)];
        [self.programButton setFrameX:127];
        [self.programButton setFrameY:54];
        [self.contentView addSubview:self.programButton];
    }
    
    /*************************************
     *  Button
     *************************************/
    if (!self.setTempButton) {
        self.setTempButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"set_temp", @"set_temp")
                                                         type:kButtonGreenBacking
                                                          tag:kBtnSetTemp
                                                        width:89
                                                       height:38
                                                       target:self
                                                     selector:@selector(setTempPressed:)];
        [self.setTempButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.setTempButton setFrameX:222];
        [self.setTempButton setFrameY:54];
        [self.contentView addSubview:self.setTempButton];
    }

    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self positionViewsIsEditing:self.isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

- (IBAction)boostPressed:(id)sender {
    //DebugLog(@"Boost pressed");
    _boostActive_img.hidden = NO;
    _boostActive_img.alpha = 1.0;
    if (boostFadeActive_tmr == nil){
        boostFadeActive_tmr = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(boostFadeActive:) userInfo:nil repeats:YES  ];
    }
    
    if ([self.delegate respondsToSelector:@selector(heatingPressed:option:indexPath:)])
        [self.delegate heatingPressed:self option:kHeatingCellVCBoost indexPath:self.indexPath];
}

- (IBAction)standbyPressed:(id)sender {
    //DebugLog(@"Standby pressed");
    active_img.hidden = NO;
    active_img.alpha = 1.0;
    if (fadeActive_tmr == nil){
        fadeActive_tmr = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(fadeActive:) userInfo:nil repeats:YES  ];
    }
    
    if ([self.delegate respondsToSelector:@selector(heatingPressed:option:indexPath:)])
        [self.delegate heatingPressed:self option:kHeatingCellVCStandby indexPath:self.indexPath];
}

- (IBAction)programPressed:(id)sender {
    //DebugLog(@"program pressed");
    
    if ([self.delegate respondsToSelector:@selector(heatingPressed:option:indexPath:)])
        [self.delegate heatingPressed:self option:kHeatingCellVCProgram indexPath:self.indexPath];
}

- (IBAction)setTempPressed:(id)sender {
    //DebugLog(@"set temp pressed");
    
    if ([self.delegate respondsToSelector:@selector(heatingPressed:option:indexPath:)])
        [self.delegate heatingPressed:self option:kHeatingCellVCSetTemp indexPath:self.indexPath];
}

- (void)boostFadeActive:(NSTimer *)timer{
    _boostActive_img.alpha -= 0.1;
    if (_boostActive_img.alpha<=0.0){
        [boostFadeActive_tmr invalidate];
        boostFadeActive_tmr = nil;
    }
}

- (void)fadeActive:(NSTimer *)timer{
    active_img.alpha -= 0.1;
    if (active_img.alpha<=0.0){
        [fadeActive_tmr invalidate];
        fadeActive_tmr = nil;
    }
}

@end
