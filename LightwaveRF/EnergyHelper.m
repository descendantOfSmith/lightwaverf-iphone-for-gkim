//
//  EnergyHelper.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "EnergyHelper.h"

@implementation EnergyHelper

// Static var
float MAXCURRENT = 3000.0f;

+ (Energy *)initializeEnergy {
    return [[Energy alloc] init];
}

+ (NSArray *)getMeterValuesFromString:(NSString *)_str {
    NSMutableArray *tmpValuesArray = [[_str componentsSeparatedByString:@","] mutableCopy];
    if (tmpValuesArray.count == 5) {
        
        int pos = [[tmpValuesArray objectAtIndex:1] rangeOfString:@"="].location;
        
        if (pos != NSNotFound)
            [tmpValuesArray replaceObjectAtIndex:1 withObject:[[tmpValuesArray objectAtIndex:1] substringFromIndex:(pos + 1)]];
        
        pos = [[tmpValuesArray objectAtIndex:4] rangeOfString:@";"].location;
        
        if (pos != NSNotFound)
            [tmpValuesArray replaceObjectAtIndex:4 withObject:[[tmpValuesArray objectAtIndex:4] substringToIndex:(pos)]];
        
        NSArray *_meterValues = [[NSArray alloc] initWithObjects:
                                 [tmpValuesArray objectAtIndex:1],
                                 [tmpValuesArray objectAtIndex:2],
                                 [tmpValuesArray objectAtIndex:3],
                                 [tmpValuesArray objectAtIndex:4],
                                 nil];
        return _meterValues;
    }
    
    return tmpValuesArray;
}

+ (Energy *)getEnergyFromData:(NSString *)data {
    //DebugLog(@"Response: %@", data);
    
    //Response is: 124, ?W=350,3200,1700,9660;?V=2.20;
    //Readings in watts:
        // 1st is current
        // 2nd is not used (maximum today)
        // 3rd is total today
        // 4th is total yesterday
        // V = software version number (2.20)
    
    // Mine displays:
    // KWH =                35
    // Cost per hour =      3p
    // Cost for today =     15p
    // Cost for yesterday = 87p
    
    // Should display:
    // KWH =                0.19
    // Cost per hour =      2p
    // Cost for today =     £0.23
    // Cost for yesterday = £0.24
  
#if TARGET_IPHONE_SIMULATOR
    data = @"190,4440,11280,00";
    DebugLog(@"TEST Response: %@", data);
#endif

    NSArray *tmpValuesArray = [EnergyHelper getMeterValuesFromString:data];
    if (tmpValuesArray.count == 4) {
        
        float rate = [[[UserPrefsHelper shared] getElecRate] floatValue];
        float current = [[tmpValuesArray objectAtIndex:0] floatValue];
        //float maximumToday = [[tmpValuesArray objectAtIndex:1] floatValue];
        float today = [[tmpValuesArray objectAtIndex:2] floatValue];
        float yesterday = [[tmpValuesArray objectAtIndex:3] floatValue];
        
        float kw = current/MAXCURRENT;// (maxCurrent is set to 3000) ­
        // cap kw between 0 and 1.0 by limiting the max value to 1.0
        
        if (kw>1.0f)
            kw = 1.0f;
        
        //rate is the value in pence of the cost per unit
        
        float maxPence = rate * MAXCURRENT/1000;
        
        float maxToday = rate * 25;
        
        float pence = (current/1000.0f) * rate;
        
        float per_hour = pence/maxPence;//cap this between 0 and 1.0
        
		if (per_hour>1.0f)
            per_hour = 1.0f;
        
        float cost_today = (today/1000.0f) * rate;
        
        float cost_today2 = cost_today/maxToday;// cap this between 0 and 1
        
		if (cost_today2>1.0f)
            cost_today2 = 1.0f;
        
        float cost_yesterday = (yesterday/1000.0f) * rate;
        
        float cost_yesterday2 = cost_yesterday/maxToday;//cap between 0 and 1
        
		if (cost_yesterday2>1.0f)
            cost_yesterday2 = 1.0f;

        Energy *tmpEnergy = [EnergyHelper initializeEnergy];

        /**********************************************
         *      Dial Percentages from 0% - 100%
         **********************************************/
        
        /*
        DebugLog(@"\n\n");
        
        DebugLog(@"per_hour: %f", per_hour);
        DebugLog(@"cost_today2: %f", cost_today2);
        DebugLog(@"cost_yesterday2: %f", cost_yesterday2);
        DebugLog(@"kw: %f", kw);
        
        DebugLog(@"\n\n");
         */
    
        tmpEnergy.eCPHPercentage = per_hour * 100;
        tmpEnergy.eCFTPercentage = cost_today2 * 100;
        tmpEnergy.eCFYPercentage = cost_yesterday2 * 100;
        
        tmpEnergy.eKWHPercentage = kw * 100;
        tmpEnergy.eKWHTodayPercentage = (tmpEnergy.eKWHPercentage + tmpEnergy.eCFTPercentage) / 2;
        tmpEnergy.eKWHYesterdayPercentage = (tmpEnergy.eKWHPercentage + tmpEnergy.eCFYPercentage) / 2;
        
        /**********************************************
         *          Dial String Values
         **********************************************/
        
        // KW/H
        tmpEnergy.eKWHStr = [NSString stringWithFormat:@"%4.2f", current/1000.0f];
        
        // KW/H SO FAR TODAY
        tmpEnergy.eKWHTodayStr = [NSString stringWithFormat:@"%4.2f", today / 1000];
        
        // KW/HFOR YESTERDAY
        tmpEnergy.eKWHYesterdayStr = [NSString stringWithFormat:@"%4.2f", yesterday / 1000];
        
        // Cost Per Hour
        tmpEnergy.eCPHStr = [NSString stringWithFormat:@"%4.1f", pence];
        
        if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isMegaMan] || [AppStyleHelper isCOCO])
            // Cost so far today
            tmpEnergy.eCFTStr = [NSString stringWithFormat:@"%4.1f", cost_today];
        else
            // Cost so far today
            tmpEnergy.eCFTStr = [NSString stringWithFormat:@"%3.2f", cost_today / 100];
        
        // Cost for yesterday
        tmpEnergy.eCFYStr = [NSString stringWithFormat:@"%3.2f", cost_yesterday/100.0f];
        
        
        tmpEnergy.lastUpdate = [NSDate date];
        
        return tmpEnergy;
        
    } else
        return [EnergyHelper initializeEnergy];
}

@end
