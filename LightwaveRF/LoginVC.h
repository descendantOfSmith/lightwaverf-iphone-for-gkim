//
//  LoginVC.h
//  LightwaveRF
//
//  Created by Nicholas Lever on 08/08/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "RootViewController.h"

@interface LoginVC : RootViewController <UIGestureRecognizerDelegate, UITextFieldDelegate>
{
    MPMoviePlayerController *moviePlayer;
    CGRect login_frm;
    BOOL videoShown;
}

@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UITextField *email_txt;
@property (strong, nonatomic) IBOutlet UITextField *password_txt;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) UIButton *login_btn;
@property (strong, nonatomic) IBOutlet UIButton *playvideo_btn;
@property (strong, nonatomic) IBOutlet UILabel *nw_lbl;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *busy_act;

- (IBAction)loginPressed:(id)sender;
- (IBAction)forgottenPressed:(id)sender;
- (IBAction)findPressed:(id)sender;
- (IBAction)newPressed:(id)sender;
- (IBAction)videoPressed:(id)sender;
- (IBAction)emailEditBegin:(id)sender;

@end
