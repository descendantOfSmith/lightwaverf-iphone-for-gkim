//
//  EnergyDialView.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 16/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SegmentedDialView.h"

@protocol EnergyDialViewDataSource;

@interface EnergyDialView : UIView <SegmentedDialViewDataSource> {
    
}

@property (nonatomic, assign) float segmentSize;
@property (nonatomic, strong) UIFont *headingFont;
@property (nonatomic, assign) BOOL showPenceLabel;
@property (nonatomic, assign) BOOL showPoundLabel;

- (id)initWithFrame:(CGRect)frame segmentSize:(float)tmpSegmentSize showPence:(BOOL)tmpShowPence dataSource:(id<EnergyDialViewDataSource>)tmpDataSource;
- (void)refreshData;
- (void)refreshDial;
- (void)refreshLabels;
- (void)setShowPenceLabel:(BOOL)tmpShowPenceLabel;
- (void)setShowPoundLabel:(BOOL)tmpShowPoundLabel;
- (void)setShowLabels:(BOOL)_showLabels;

@end

@protocol EnergyDialViewDataSource <NSObject>

@optional

- (float)percentageComplete:(EnergyDialView *)tmpEnergyDialView;
- (NSString *)segmentHeading:(EnergyDialView *)tmpEnergyDialView;
- (NSString *)segmentValue:(EnergyDialView *)tmpEnergyDialView;

@end
