//
//  Device.h
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 21/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@class Mood;
@class Zone;

@interface Device : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *type;
@property (nonatomic) int assumedState;
@property (nonatomic) BOOL paired;
@property (nonatomic) DeviceLockState locked;
@property (nonatomic) int lastOnState;
@property (nonatomic) int deviceID;
@property (strong, nonatomic) Mood *mood;
@property (strong, nonatomic) Zone *zone;
@property (strong, nonatomic, readonly) NSString *hardwareKey;
@property (nonatomic, readonly) int roomID;
@property (strong, nonatomic, readonly) Device *unknown;
@property (strong, nonatomic, readonly) NSString* zoneName;
@property (nonatomic, assign) BOOL didSetSucceed;

-(id) initWithName:(NSString *) name deviceID:(int) deviceID type:(NSString *) type lastOnState:(int) lastOnState paired:(BOOL) paired mood:(Mood *) mood zone:(Zone *) zone;
-(id) initWithName:(NSString *) name deviceID:(int) deviceID type:(NSString *) type lastOnState:(int) lastOnState paired:(BOOL) paired zone:(Zone *) zone;
-(id) initWithName:(NSString *) name deviceID:(int) deviceID type:(NSString *) type zone:(Zone *) zone;
-(id) initWithMood: (Mood *) mood;
-(NSString *) getCommand:(int) setting;
-(BOOL) isRadiator;
-(BOOL) isColourLEDS;
-(BOOL) isSocket;
-(BOOL) isDimmer;
-(BOOL) isLightSwitch;
-(BOOL) isHeating;
-(BOOL) isMagSwitch;
-(BOOL) isPir;
-(BOOL) isCamera;
-(BOOL) isMood;
-(BOOL) isActiveDevice;
-(BOOL) isPassiveDevice;
-(BOOL) isOpenClose;
-(BOOL) isFSL500W;
-(BOOL) isFSL3000W;
-(int) turnOn;
-(int) turnOff;
-(BOOL) isPaired;
-(void) makePairing;
-(void) openPairing;
-(NSDictionary *) toDictionary;
-(NSData *) toJSON;
//-(void) sendPairing
-(void) renameFrom:(NSString *) fromName to:(NSString *) toName;
-(void) deleteDeviceNamed:(NSString *) deviceName;
-(Mood *)getMood;
//-(void) moveUp
- (UIImage *)getIconImage;
- (int)getSetting:(ConstantsIsOpenClose)_isOpenClose isOnOrOpen:(BOOL)_isOnOrOpen brightness:(int)_brightness;

@end
