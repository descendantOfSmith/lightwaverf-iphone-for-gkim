//
//  Energy.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "Energy.h"

@implementation Energy

@synthesize eKWHPercentage;
@synthesize eKWHTodayPercentage;
@synthesize eKWHYesterdayPercentage;

@synthesize eCPHPercentage;
@synthesize eCFYPercentage;
@synthesize eCFTPercentage;

@synthesize eKWHStr;
@synthesize eKWHTodayStr;
@synthesize eKWHYesterdayStr;

@synthesize eCPHStr;
@synthesize eCFYStr;
@synthesize eCFTStr;

@synthesize lastUpdate;

- (id)init {
    self = [super init];
    
    if (self) {
        self.eKWHPercentage = 0;
        self.eKWHTodayPercentage = 0;
        self.eKWHYesterdayPercentage = 0;
        
        self.eCPHPercentage = 0;
        self.eCFYPercentage = 0;
        self.eCFTPercentage = 0;
        
        self.eKWHStr = @"0";
        self.eKWHTodayStr = @"0";
        self.eKWHYesterdayStr = @"0";        
        
        self.eCPHStr = @"0";
        self.eCFYStr = @"0";
        self.eCFTStr = @"0";
        
        self.lastUpdate = nil;
    }
    return self;
}

- (void)dealloc  {
    //DebugLog(@"DEALLOC");
}

@end
