//
//  ChooseProductSliderCell.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 19/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventActionCell.h"

@interface EventActionDelayCell : EventActionCell {
    
}

- (void)setupCellWithTitle:(NSString*)tmpTitle
                  subtitle:(NSString *)tmpSubtitle
                detailText:(NSString *)tmpDetailText
                    device:(Device *)_device
                     delay:(NSString *)_delay
                 indexPath:(NSIndexPath *)_indexPath
                  delegate:(id<EventActionDelayCellDelegate>)_delegate;

@end
