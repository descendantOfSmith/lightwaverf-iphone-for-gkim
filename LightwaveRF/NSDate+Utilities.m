//
//  NSDateAdditions.m
//  Created by Devin Ross on 7/28/09.
//
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */
#import "NSDate+Utilities.h"

@implementation NSDate (Utilities)

- (TKDateInformation) dateInformationWithTimeZone:(NSTimeZone*)tz{
	
	
	TKDateInformation info;
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	[gregorian setTimeZone:tz];
	NSDateComponents *comp = [gregorian components:(NSMonthCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | 
													NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit) 
										  fromDate:self];
	info.day = [comp day];
	info.month = [comp month];
	info.year = [comp year];
	
	info.hour = [comp hour];
	info.minute = [comp minute];
	info.second = [comp second];
	
	info.weekday = [comp weekday];
	
	return info;
	
}

- (TKDateInformation) dateInformation{
	
	TKDateInformation info;
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSMonthCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | 
													NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit) 
										  fromDate:self];
	info.day = [comp day];
	info.month = [comp month];
	info.year = [comp year];
	
	info.hour = [comp hour];
	info.minute = [comp minute];
	info.second = [comp second];
	
	info.weekday = [comp weekday];
	

	return info;
}

+ (NSDate*) dateFromDateInformation:(TKDateInformation)info timeZone:(NSTimeZone*)tz{
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	[gregorian setTimeZone:tz];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:[NSDate date]];
	
	[comp setDay:info.day];
	[comp setMonth:info.month];
	[comp setYear:info.year];
	[comp setHour:info.hour];
	[comp setMinute:info.minute];
	[comp setSecond:info.second];
	[comp setTimeZone:tz];
	
	return [gregorian dateFromComponents:comp];
}

+ (NSDate*) dateFromDateInformation:(TKDateInformation)info{
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:[NSDate date]];
	
	[comp setDay:info.day];
	[comp setMonth:info.month];
	[comp setYear:info.year];
	[comp setHour:info.hour];
	[comp setMinute:info.minute];
	[comp setSecond:info.second];
	//[comp setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	return [gregorian dateFromComponents:comp];
}

- (int) daysBetweenDate:(NSDate*)d{
	NSTimeInterval time = [self timeIntervalSinceDate:d];
	return abs(time / 60 / 60/ 24);
}

// ------------------
- (NSString*) month{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];	
	[dateFormatter setDateFormat:@"MMMM"];
	return [dateFormatter stringFromDate:self];
}
- (NSString*) year{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];	
	[dateFormatter setDateFormat:@"yyyy"];
	return [dateFormatter stringFromDate:self];
}
// ------------------

+ (NSDate*) firstOfCurrentMonth{
	
	NSDate *day = [NSDate date];
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:day];
	[comp setDay:1];
	return [gregorian dateFromComponents:comp];
	
}

+ (NSDate*) lastOfCurrentMonth{
	NSDate *day = [NSDate date];
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:day];
	[comp setDay:0];
	[comp setMonth:comp.month+1];
	return [gregorian dateFromComponents:comp];
}

- (NSDate*) timelessDate {
	NSDate *day = self;
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:day];
	return [gregorian dateFromComponents:comp];
}

- (NSDate*) monthlessDate {
	NSDate *day = self;
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:day];
	return [gregorian dateFromComponents:comp];
}

- (NSDate*) firstOfCurrentMonthForDate {
	
	NSDate *day = self;
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:day];
	[comp setDay:1];
	return [gregorian dateFromComponents:comp];
	
}

- (NSDate*) firstOfNextMonthForDate {
	NSDate *day = self;
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:day];
	[comp setDay:1];
	[comp setMonth:comp.month+1];
	return [gregorian dateFromComponents:comp];
}

- (NSNumber*) yearNumber{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear
                                                                   fromDate:self];
    return [NSNumber numberWithInt:[components year]];
}

- (NSNumber*) monthNumber{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth
                                                                   fromDate:self];
    return [NSNumber numberWithInt:[components month]];
}

- (NSNumber*) dayNumber{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay
                                                                   fromDate:self];
    return [NSNumber numberWithInt:[components day]];
    
//	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];	
//	[dateFormatter setDateFormat:@"d"];
//	return [NSNumber numberWithInt:[[dateFormatter stringFromDate:self] intValue]];
}

- (NSNumber*) hourNumber {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour
                                                                   fromDate:self];
    return [NSNumber numberWithInt:[components hour]];
    
//	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//	[dateFormatter setDateFormat:@"HH a"];
//	return [NSNumber numberWithInt:[[dateFormatter stringFromDate:self] intValue]];
}

- (NSNumber*) minuteNumber {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMinute
                                                                   fromDate:self];
    return [NSNumber numberWithInt:[components minute]];
    
//	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//	[dateFormatter setDateFormat:@"mm"];
//	return [NSNumber numberWithInt:[[dateFormatter stringFromDate:self] intValue]];
}

- (NSNumber*) secondNumber {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitSecond
                                                                   fromDate:self];
    return [NSNumber numberWithInt:[components second]];
    
//	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//	[dateFormatter setDateFormat:@"ss"];
//	return [NSNumber numberWithInt:[[dateFormatter stringFromDate:self] intValue]];
}

- (NSString*) hourString {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];	
	[dateFormatter setDateFormat:@"h a"];
	return [dateFormatter stringFromDate:self];
}

- (NSString*) monthString{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];	
	[dateFormatter setDateFormat:@"MMMM"];
	return [dateFormatter stringFromDate:self];
}

- (NSString*) yearString{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];	
	[dateFormatter setDateFormat:@"yyyy"];
	return [dateFormatter stringFromDate:self];
}

- (NSString*) monthYearString{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];	
	[dateFormatter setDateFormat:@"MMMM yyyy"];
	return [dateFormatter stringFromDate:self];
}

- (int) weekday{
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comps = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSWeekdayCalendarUnit) fromDate:self];
	int weekday = [comps weekday];
	return weekday;
}

- (int) weekdayWithMondayFirst{
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setFirstWeekday:2]; // Sunday == 1, Saturday == 7
    NSUInteger adjustedWeekdayOrdinal = [gregorian ordinalityOfUnit:NSWeekdayCalendarUnit inUnit:NSWeekCalendarUnit forDate:self];
    return adjustedWeekdayOrdinal;
}

- (int) differenceInDaysTo:(NSDate *)toDate{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [gregorian components:NSDayCalendarUnit
                                                fromDate:self
                                                  toDate:toDate
                                                 options:0];
    NSInteger days = [components day];
    return days;
}

- (int) differenceInMonthsTo:(NSDate *)toDate{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [gregorian components:NSMonthCalendarUnit
                                                fromDate:[self monthlessDate]
                                                  toDate:[toDate monthlessDate]
                                                 options:0];
    NSInteger months = [components month];
    return months;
}

- (BOOL) isSameDay:(NSDate*)anotherDate {
	NSCalendar* calendar = [NSCalendar currentCalendar];
	NSDateComponents* components1 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];
	NSDateComponents* components2 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:anotherDate];
	return ([components1 year] == [components2 year] && [components1 month] == [components2 month] && [components1 day] == [components2 day]);
} 

- (BOOL)isSameDateTime:(NSDate *)tmpDateTime {
	NSComparisonResult result = [self compare:tmpDateTime];
	
	switch (result) {
		case NSOrderedAscending: 
			return NO; 
			break;
			
		case NSOrderedDescending: 
			return NO;
			break;
			
		case NSOrderedSame: 
			return YES; 
			break;
			
		default: 
			return NO; 
			break;
	}
}

+ (BOOL)isTodaysDateAfter:(NSDate *)tmpDate {
	NSDate *now = [NSDate date];
	NSComparisonResult result = [now compare:tmpDate];
	
	switch (result) {
		case NSOrderedAscending: 
			return NO; 
			break;
            
		case NSOrderedDescending: 
			return YES; 
			break;
            
		case NSOrderedSame: 
			return NO; 
			break;
            
		default: 
			return NO; 
			break;
	}
}

- (BOOL) isToday{
	return [self isSameDay:[NSDate date]];
} 

- (NSString*) dateDescription{
	return [[self description] substringToIndex:10];
}

- (NSDate *) dateByAddingHours:(NSUInteger)hours minutes:(NSUInteger)minutes {
	NSDateComponents *c = [[NSDateComponents alloc] init];
	c.hour = hours;
    c.minute = minutes;
	return [[NSCalendar currentCalendar] dateByAddingComponents:c toDate:self options:0];
}

- (NSDate *) dateByAddingDays:(NSUInteger)days {
	NSDateComponents *c = [[NSDateComponents alloc] init];
	c.day = days;
	return [[NSCalendar currentCalendar] dateByAddingComponents:c toDate:self options:0];
}

- (NSDate *) dateByAddingWeeks:(NSUInteger)weeks {
	NSDateComponents *c = [[NSDateComponents alloc] init];
	c.week = weeks;
	return [[NSCalendar currentCalendar] dateByAddingComponents:c toDate:self options:0];
}

- (NSDate *) dateByAddingMonths:(NSUInteger)months {
	NSDateComponents *c = [[NSDateComponents alloc] init];
	c.month = months;
	return [[NSCalendar currentCalendar] dateByAddingComponents:c toDate:self options:0];
}

+ (NSDate *) dateWithDatePart:(NSDate *)aDate andTimePart:(NSDate *)aTime {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"dd/MM/yyyy"];
	NSString *datePortion = [dateFormatter stringFromDate:aDate];
	
	[dateFormatter setDateFormat:@"HH:mm"];
	NSString *timePortion = [dateFormatter stringFromDate:aTime];
	
	[dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
	NSString *dateTime = [NSString stringWithFormat:@"%@ %@",datePortion,timePortion];
	return [dateFormatter dateFromString:dateTime];
}

+ (NSString*) dateInformationDescriptionWithInformation:(TKDateInformation)info{
	return [NSString stringWithFormat:@"%d %d %d %d:%d:%d",info.month,info.day,info.year,info.hour,info.minute,info.second];
}

+ (NSInteger)secondsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSSecondCalendarUnit
                                               fromDate:fromDateTime
                                                 toDate:toDateTime
                                                options:0];
    return [components second];
}

+ (NSInteger)minutesBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSMinuteCalendarUnit
                                               fromDate:fromDateTime
                                                 toDate:toDateTime
                                                options:0];
    return [components minute];
}

+ (NSInteger)hoursBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSHourCalendarUnit
                                               fromDate:fromDateTime
                                                 toDate:toDateTime
                                                options:0];
    return [components hour];
}

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDateTime
                                                 toDate:toDateTime
                                                options:0];
    return [components day];
}

+ (NSInteger)weeksBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSWeekCalendarUnit
                                               fromDate:fromDateTime
                                                 toDate:toDateTime
                                                options:0];
    return [components week];
}

+ (NSInteger)monthsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSMonthCalendarUnit
                                               fromDate:fromDateTime
                                                 toDate:toDateTime
                                                options:0];
    return [components month];
}

+ (NSDate*)returnDateByNumberOfWeeks:(int)tmpNumWeeks 
                              toDate:(NSDate *)tmpToDate {
    
    return [NSDate returnDateByNumberOfSeconds:0 
                              numberOMinutes:0 
                               numberOfHours:0 
                                numberOfDays:0 
                               numberOfWeeks:tmpNumWeeks 
                              numberOfMonths:0 
                               numberOfYears:0 
                                      toDate:tmpToDate];
}

+ (NSDate*)returnDateByNumberOfDays:(int)tmpNumDays 
                      numberOfWeeks:(int)tmpNumWeeks
                     numberOfMonths:(int)tmpNumMonths 
                      numberOfYears:(int)tmpNumYears 
                             toDate:(NSDate *)tmpToDate {
    
    return [NSDate returnDateByNumberOfSeconds:0 
                              numberOMinutes:0 
                               numberOfHours:0 
                                numberOfDays:tmpNumDays 
                               numberOfWeeks:tmpNumWeeks 
                              numberOfMonths:tmpNumMonths 
                               numberOfYears:tmpNumYears 
                                      toDate:tmpToDate];
}

+ (NSDate*)returnDateByNumberOfSeconds:(int)tmpNumSeconds 
                        numberOMinutes:(int)tmpNumMins 
                         numberOfHours:(int)tmpNumHours 
                          numberOfDays:(int)tmpNumDays 
                         numberOfWeeks:(int)tmpNumWeeks
                        numberOfMonths:(int)tmpNumMonths 
                         numberOfYears:(int)tmpNumYears 
                                toDate:(NSDate *)tmpToDate {
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setSecond:tmpNumSeconds];
    [components setMinute:tmpNumMins];
    [components setHour:tmpNumHours];
    [components setDay:tmpNumDays];
    [components setWeek:tmpNumWeeks];
    [components setMonth:tmpNumMonths];
    [components setYear:tmpNumYears];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDate *newDate2 = [gregorian dateByAddingComponents:components toDate:tmpToDate options:0];
    
    return newDate2;
}

+ (NSDate *)combineDate:(NSDate *)date withTime:(NSDate *)time {
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents *dateComponents = [gregorian components:unitFlagsDate fromDate:date];
    unsigned unitFlagsTime = NSHourCalendarUnit | NSMinuteCalendarUnit |  NSSecondCalendarUnit;
    NSDateComponents *timeComponents = [gregorian components:unitFlagsTime fromDate:time];
    
    [dateComponents setSecond:[timeComponents second]];
    [dateComponents setHour:[timeComponents hour]];
    [dateComponents setMinute:[timeComponents minute]];
    
    NSDate *combDate = [gregorian dateFromComponents:dateComponents];   
    
    
    return combDate;
}

- (NSString *)returnDateWithFormat:(NSString *)_format {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:_format];
    NSString *dateString = [dateFormat stringFromDate:self];
    
    return dateString;
}

- (NSString *)returnSimpleDate {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:self];
    
    return dateString;
}

- (NSString *)returnSimpleDateTime {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy HH mm ss"];
    NSString *dateString = [dateFormat stringFromDate:self];
    
    return dateString;
}

- (NSString *)returnDate {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE, dd MMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:self];
    
    return dateString;
}

- (NSString *)returnTime {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"hh:mm a"];
    NSString *timeString = [dateFormat stringFromDate:self];
    
    return timeString;
}

- (NSString *)returnDateAndTime {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE, dd MMM hh:mm a"];
    NSString *dateString = [dateFormat stringFromDate:self];
    
    return dateString;
}

- (NSString *)returnEnergyDateAndTime {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM HH:mm:ss"];
    NSString *dateString = [dateFormat stringFromDate:self];
    
    return dateString;
}

- (NSString *)returnDateAndTimeWithYear {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE, dd MMM yyyy hh:mm a"];
    NSString *dateString = [dateFormat stringFromDate:self];
    
    return dateString;
}

- (NSDate *)floorTimeComponentOfDate {
    // Returns an NSDate that is the same day as the receiver, but
    // with a time component of 00:00:00
    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:flags fromDate:self];
    NSDate* dateOnly = [calendar dateFromComponents:components];
    return dateOnly;
}

- (NSDate *)ceilTimeComponentOfDate {
    // Returns an NSDate that is the same day as the receiver, but
    // with a time component of 11:59:59
    NSDate *floorDate = [self floorTimeComponentOfDate];
    NSTimeInterval secondsInADay = 86400;
    NSDate *ceilDate = [floorDate dateByAddingTimeInterval:secondsInADay-1];
    return ceilDate;
}

+ (NSDateComponents *)returnDifference:(NSDate *)startDate fromDate:(NSDate *)toThisDate components:(NSUInteger)_components {
    NSDate *fromDate;
    NSDate *toDate;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar rangeOfUnit:_components startDate:&fromDate
                 interval:NULL forDate:startDate];
    [calendar rangeOfUnit:_components startDate:&toDate
                 interval:NULL forDate:toThisDate];
    NSDateComponents *difference = [calendar components:_components
                                               fromDate:fromDate
                                                 toDate:toDate
                                                options:0];
    return difference;
}

@end
