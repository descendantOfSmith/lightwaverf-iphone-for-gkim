//
//  Zone.m
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 21/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import "Zone.h"
#import "Mood.h"
#import "Device.h"
#import "DB.h"
#import "Home.h"
#import "LTimer.h"

int const MAX_MOODS = 3;
int const MAX_PRODUCTS = 16;

@implementation Zone

@synthesize zoneID;
@synthesize name;
@synthesize devices = __devices;

-(int) getDeviceCount
{
    if (self.devices == nil) return 0;
    int count = [self.devices count];
    if ([self hasHeating]) count++;
    return count;
}

-(int) freeMoodID
{
    BOOL used[MAX_MOODS];
    for (int i = 0; i < MAX_MOODS; i++)
    {
        used[i] = NO;
    }
    for (int i = 0; i < MAX_MOODS; i++)
    {
        if (!used[i]) return i;
    }
    return -1;
}

-(int) freeDeviceID
{
    BOOL used[MAX_PRODUCTS];
    for (int i = 0; i < MAX_PRODUCTS; i++)
    {
        used[i] = NO;
    }
    for (Device *device in self.devices)
    {
        if (device.deviceID >= 0 && device.deviceID < MAX_PRODUCTS) used[device.deviceID] = YES;
    }
    for (int i = 0; i < MAX_PRODUCTS; i++)
    {
        if (!used[i]) return i;
    }
    return -1;
}

-(id) initWithID:(int)_zoneID name:(NSString *)_name devices:(NSMutableArray *)_devices
{
    if (self = [super init])
    {
        self.zoneID = _zoneID;
        self.name = _name;
        self.devices = _devices;
    }
    return self;
}

-(id) initWithID:(int)_zoneID name:(NSString *)_name
{
    if (self = [super init])
    {
        self.zoneID = _zoneID;
        self.name = _name;
        self.devices = [self getDevicesFromDB];
    }
    return self;
}

-(id) initWithName:(NSString *)_name;
{
    if (self = [super init])
    {
        self.zoneID = -1;
        self.name = _name;
        self.devices = [self getDevicesFromDB];
    }
    return self;
}

-(int)getFreeDeviceID{
    // Updated on 9/12/2013, request By Jim.
    // New IDs should find the first free ID from a range e.g. 1-6
    // Not simply the highest id add one (see old code below)
    for (int n = 1; n <= MAX_PRODUCTS; n++) {
        
        BOOL _didFind = NO;
        
        for(Device *_device in self.devices) {
            
            if (_device.deviceID == n) {
                _didFind = YES;
                break;
            }
        }
        
        if (!_didFind) {
            return n;
        }
    }
    
    // Old Code, leave in as backup for above
    int idx = 0;
    
    for(Device *_device in self.devices){
        if (_device.deviceID>idx)
            idx=_device.deviceID;
    }
    idx++;
    return idx;
}

-(NSMutableArray *)getDevicesFromDB{
    DB *db = [DB shared];
    NSMutableArray *rows = [db getRows:[NSString stringWithFormat:@"SELECT * FROM devices WHERE accountId=%d AND zoneId=%i ORDER BY displayId", [[UserPrefsHelper shared] getCurrentAccountId], zoneID]];
    NSMutableArray *_devices = [[NSMutableArray alloc] init];
    if (rows!=nil){
        for(NSDictionary *dict in rows){
            NSString *_name = [dict objectForKey:@"name"];
            int deviceID = [[dict valueForKey:@"deviceId"] intValue];
            BOOL paired = [[dict valueForKey:@"paired"] intValue];
            int setting = [[dict valueForKey:@"setting"] intValue];
            NSString *type = [dict objectForKey:@"type"];
            Device *device;
            if ([type isEqualToString:@"M"]){
                int moodID = [[dict valueForKey:@"moodId"] intValue];
                Mood *mood;
                if (moodID==4){
                    mood = [[Mood alloc] initAllOff:self];
                }else{
                    mood = [[Mood alloc] initWithID:moodID name:_name zone:self];
                }
                if (mood != nil) device = [[Device alloc] initWithMood:mood];
            }else{
                device = [[Device alloc] initWithName:_name deviceID:deviceID type:type lastOnState:setting paired:paired zone:self];
            }
            if (device != nil) [_devices addObject:device];
        }
    }
    return _devices;
}

-(void)saveDevicesToDB{
    DB *db = [DB shared];

    NSMutableArray *rows = [db getRows:[NSString stringWithFormat:@"SELECT * FROM devices WHERE accountId=%d AND zoneId=%i", [[UserPrefsHelper shared] getCurrentAccountId], self.zoneID]];
    for (NSMutableDictionary *dict in rows){
        int deviceID = [[dict valueForKey:@"deviceId"] intValue];
        int zoneId = [[dict valueForKey:@"zoneId"] intValue];
        
        NSString *_hwKey = [NSString stringWithFormat:@"R%dD%d", zoneId, deviceID];
        [db sqlExec:[NSString stringWithFormat:@"DELETE FROM actions WHERE accountId=%d AND hwKey='%@'", [[UserPrefsHelper shared] getCurrentAccountId], _hwKey]];

    }

    [db sqlExec:[NSString stringWithFormat:@"DELETE FROM devices WHERE accountId=%d AND zoneId=%i", [[UserPrefsHelper shared] getCurrentAccountId], self.zoneID]];

    int i=0;
    for(Device *device in self.devices){
        NSString *sql;
        if ([device isMood]){
            Mood *mood = [device getMood];
            int moodId = mood.moodID;
            mood.name = device.name;
            sql = [NSString stringWithFormat:@"INSERT INTO devices (deviceId, displayId, name, zoneId, type, paired, setting, moodId, accountId) VALUES(%d, %d, '%@', %d, '%@', %d, %d, %d, %d)",
                device.deviceID, i, mood.name, zoneID, device.type, device.paired, device.lastOnState, moodId, [[UserPrefsHelper shared] getCurrentAccountId]];
        }else{
            sql = [NSString stringWithFormat:@"INSERT INTO devices (deviceId, displayId, name, zoneId, type, paired, setting, accountId) VALUES(%d, %d, '%@', %d, '%@', %d, %d, %d)",
                device.deviceID, i, device.name, zoneID, device.type, device.paired, device.lastOnState, [[UserPrefsHelper shared] getCurrentAccountId]];
        }
        
        //DebugLog(@"Zone saveDevicesToDB %@", sql);
        
        [db sqlExec:sql];
        
        i++;
    }
}

-(BOOL) hasAllOff{
    for(Device *device in self.devices){
        if ([device isMood]){
            Mood *mood = [device getMood];
            if ([mood isAllOff]) return YES;
        }
    }
    return NO;
}

-(BOOL) addAllOff{
    Mood *mood = [[Mood alloc] initAllOff:self];
    if (mood == nil)
        return NO;
    
    Device *device = [[Device alloc] initWithMood:mood];
    if (device==nil)
        return NO;

    //[self.devices addObject:device];
    [self.devices insertObject:device atIndex:0];
    [self saveDevicesToDB];

    return YES;
}

-(BOOL)addMood:(int)moodID{
    Mood *mood = [[Mood alloc] initWithID:moodID name:NSLocalizedString(@"mood", @"") zone:self];
    if (mood==nil) return NO;
    Device *device = [[Device alloc] initWithMood:mood];
    if (device==nil) return NO;
    [self.devices addObject:device];
    [self saveDevicesToDB];
    return YES;
}

-(void) addDevice:(Device *) device andSave:(BOOL)save
{
    [self.devices addObject:device];
    if (save)
        [self saveDevicesToDB];
}

-(BOOL) ensureUniqueDeviceNames
{
    BOOL changed = NO;
    for (Device *deviceA in self.devices)
    {
        NSString *nameOfDeviceA = deviceA.name;
        int numberOfDuplicatesFound = 1;
        for (Device *deviceB in self.devices)
        {
            if (deviceA == deviceB) continue;
            NSString *nameOfDeviceB = deviceB.name;
            if ([nameOfDeviceB isEqualToString:nameOfDeviceA])
            {
                [nameOfDeviceB stringByAppendingFormat:@" %d", numberOfDuplicatesFound];
                deviceB.name = nameOfDeviceB;
                changed = YES;
                numberOfDuplicatesFound++;
            }
        }
    }
    return changed;
}

-(NSMutableArray *) getDevicesForType:(NSString *)type
{
    NSMutableArray *subset = [[NSMutableArray alloc] initWithCapacity:5];
    for (Device *device in self.devices)
    {
        if ([device.type isEqualToString:type]) [subset addObject:device];
    }
    return subset;
}

-(Device *) getDeviceByCommand:(NSString *)_command
{
    // e.g. !R4D5F1|Device|Connected
    // Device ID = 5
        
    _command = [_command substringFromIndex:[_command rangeOfString:@"D"].location + [@"D" length]];
    _command = [_command substringToIndex:[_command rangeOfString:@"F"].location];
    
    //DebugLog(@"ID COMMAND: %@", _command);

    return [self getDeviceByID:[_command intValue]];
}

-(Device *) getDeviceByID:(int)deviceID
{
    Device *foundDevice = nil;
    for (Device *device in self.devices)
    {
        if ([device isMood]) continue;
        if (device.deviceID == deviceID)
        {
            foundDevice = device;
            break;
        }
    }
    return foundDevice;
}

-(Device *) getDeviceByMoodID:(int)moodID
{
    Device *foundDevice = nil;
    for (Device *device in self.devices)
    {
        if ([device isMood]){
            Mood *mood = [device getMood];
            if (mood.moodID == moodID)
            {
                foundDevice = device;
                break;
            }
        }
    }
    return foundDevice;
}


-(Mood *) getMoodByID:(int) moodID{
    Mood *foundMood = nil;
    for (Device *device in self.devices)
    {
        if ([device isMood]){
            Mood *mood = [device getMood];
            if (mood.moodID == moodID){
                foundMood = mood;
                break;
            }
        }
    }
    return foundMood;
}

-(void) removeDevice:(Device *) device
{
    
    // Remove any Device Timers for this device
    
    NSMutableArray *_timersToRemoveArray = [NSMutableArray new];
    Home *_home = [Home shared];
    for (LTimer *_timer in _home.timers) {
        if ([device isEqual:[_timer getDevice]]) {
            [_timersToRemoveArray addObject:_timer];
        }
    }
    
    // Delete Timers for this device
    
    for (LTimer *_timer in _timersToRemoveArray) {

        // Delete from WFL
        [_timer deleteTimer];
        
        // Remove Timer
        [_home removeTimer:_timer];
        
        // Save
        [_home saveTimers];

    }

    [self.devices removeObject:device];
}


-(void) moveDeviceUp:(Device *) device
{
    int foundAt = [self.devices indexOfObject:device];
    if (foundAt > 0)
    {
        [self.devices removeObject:device];
        [self.devices insertObject:device atIndex:foundAt -1];
    }
}

-(BOOL)hasHeating{

    if ([[UserPrefsHelper shared] isLockedToPrivateAccounts])
        return NO;
    
    if (self.devices != nil){
        for(Device *device in self.devices){
            if ([device isHeating])
                return YES;
        }
    }
    return NO;
}

- (BOOL)isFreeDeviceOnlySlots {
    NSUInteger _countDevices = 0;
    for (Device *_device in self.devices) {
        if (![_device isMood])
            _countDevices += 1;
    }
    
    if (_countDevices < MAX_PRODUCTS) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isFreeDeviceSlots {
    NSUInteger _countDevices = 0;
    NSUInteger _countMoods = 0;
    for (Device *_device in self.devices) {
        if ([_device isMood])
            _countMoods += 1;
        else
            _countDevices += 1;
    }
   
#if defined(IS_LIGHTWAVERF) || defined(IS_FSLDEMO) || defined(IS_MEGAMAN) || defined(IS_COCO)
    
    if (_countDevices < MAX_PRODUCTS || _countMoods < 4){
        
#else
        
    if (_countDevices < MAX_PRODUCTS){
        
#endif
        return YES;
    }else{
        return NO;
    }
}

-(NSMutableArray *)getMoods{
    NSMutableArray *moods = [[NSMutableArray alloc] init];
    
    if (self.devices != nil){
        for(Device *device in self.devices){
            if ([device isMood]) [moods addObject:device];
        }
    }
    
    return moods;
}

- (NSMutableArray *)unpairedDevices {
    NSMutableArray *_unPairedDevicesArray = [[NSMutableArray alloc] init];
    
    for (Device *_device in self.devices) {
        if (![_device isMood] && ![_device isHeating]) {
            if (![_device isPaired]) {
                [_unPairedDevicesArray addObject:_device];
            }
        }
    }
    return _unPairedDevicesArray;
}

- (NSUInteger)numUnpairedDevices {
    return [self unpairedDevices].count;
}

-(NSDictionary *) toDictionary
{
    NSMutableDictionary *zoneDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.name, @"name", [NSNumber numberWithInt:self.zoneID] ,@"id", nil];
    NSMutableArray *zoneDevices = [[NSMutableArray alloc] init];
    if ([self.devices count] > 0)
    {
        for (int i = 0; i < [self.devices count]; i++)
        {
            Device *device = [self.devices objectAtIndex:i];
            if ([device isMood]) device.mood.listingPosition = i;
            else [zoneDevices addObject:[device toDictionary]];
        }
    }
    [zoneDictionary setValue:zoneDevices forKey:@"devices"];
    return zoneDictionary;
}

-(NSData *) toJSON
{
    NSError *error = nil;
    NSDictionary *jsonZone = self.toDictionary;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonZone options:NSJSONWritingPrettyPrinted error:&error];
    if (!error)
    {
        NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        DebugLog(@"JSON zone data:\n%@", str);
        return jsonData;
    }
    else
    {
        return nil;
    }
}

-(NSString *) toString{
    return [NSString stringWithFormat:@"Zone name:%@ id:%d deviceCount:%d", self.name, self.zoneID, [self getDeviceCount]];
}

@end
