//
//  MonthActionSheet.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 02/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "LightwaveActionSheet.h"

typedef enum
{
    kMonthActionSheetJan = 1,
    kMonthActionSheetFeb = 2,
    kMonthActionSheetMar = 3,
    kMonthActionSheetApr = 4,
    kMonthActionSheetMay = 5,
    kMonthActionSheetJun = 6,
    kMonthActionSheetJul = 7,
    kMonthActionSheetAug = 8,
    kMonthActionSheetSep = 9,
    kMonthActionSheetOct = 10,
    kMonthActionSheetNov = 11,
    kMonthActionSheetDec = 12
} MonthActionSheetTypes;

@interface MonthActionSheet : LightwaveActionSheet {
    
}

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
     selectedMonths:(NSString *)tmpSelectedMonths;

@end
