//
//  XMLConfig.h
//  LightwaveRF
//
//  Created by Nik Lever on 07/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    kEMAIL_SETTING = 0,
    kPIN_SETTING = 1,
    kCOST_SETTING = 2,
    kLATLONG_SETTING = 3,
    kTIME_ZONE_SETTING = 4,
    kMAC_SETTING = 5,
    kWIFI_PIN_SETTING = 6,
    kTEMP_EMAIL_SETTING = 7
} XMLConfigSettingOptions;

@interface XMLConfig : NSObject {
    NSMutableString *currentElementValue;
    // array of user objects
    NSMutableDictionary *elements;
    NSMutableArray *items;
    NSString *key;
    int NUM_DEVICES_PER_ROOM;
}

@property (nonatomic, strong) NSMutableDictionary *elements;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSString *key;
+ (id)shared;

-(bool)xmlToHome:(NSString *)xml;
-(NSString *)homeToXML;
+ (NSString *)localise:(NSString *)str;

@end
