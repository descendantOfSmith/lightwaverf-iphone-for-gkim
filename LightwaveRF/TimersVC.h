//
//  TimersViewController.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 16/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "AddTimerVC.h"

@interface TimersVC : RootViewController <UITableViewDelegate, UITableViewDataSource, AddTimerVCDelegate> {
    
}

@end
