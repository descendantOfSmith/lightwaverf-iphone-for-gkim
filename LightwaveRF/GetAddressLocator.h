//
//  GetAddressLocator.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 20/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONKit.h"

@protocol GetAddressLocatorDelegate;

@interface GetAddressLocator : NSObject {
    
}

@property (nonatomic, assign) id <GetAddressLocatorDelegate> delegate;

- (id)init;
- (void)getAddressForLat:(float)tmpLat longitude:(float)tmpLong;
- (void)getLongitudeAndLattitude:(NSString*)postcode;
- (void)cancelRequest;

- (double)getDistanceBetweenLocationsWithSrcLat:(double)srcLat
                                    withSrcLong:(double)srcLng
                                     withDstLat:(double)dstLat
                                     withDstLng:(double)dstLng;

@end

@protocol GetAddressLocatorDelegate <NSObject>


@optional

- (void)addressRequestDidCancel:(GetAddressLocator *)getAddressLocator;
- (void)recievedLocationComplete:(GetAddressLocator *)getAddressLocator results:(NSDictionary*)tmpResults;
- (void)recievedLocalDidFail:(GetAddressLocator *)getAddressLocator;

- (void)addressRequestDidBegin:(GetAddressLocator *)getAddressLocator;
- (void)addressRequestDidFail:(GetAddressLocator *)getAddressLocator;
- (void)addressDidComplete:(GetAddressLocator *)getAddressLocator results:(NSMutableArray *)tmpResults;


@end

