//
//  EventDevicesVC.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "Event.h"
#import "ChooseProductCell.h"
#import "ChooseProductOnOffCell.h"
#import "ChooseProductSliderCell.h"
#import "ChooseProductOpenCloseCell.h"
#import "ChooseProductLEDCell.h"

@interface EventDevicesVC : RootViewController <UITableViewDelegate, UITableViewDataSource, ChooseProductOnOffCellDelegate, ChooseProductOpenCloseCellDelegate> {
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil event:(Event *)tmpEvent;

@end
