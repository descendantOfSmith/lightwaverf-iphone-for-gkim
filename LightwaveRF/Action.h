//
//  Action.h
//  lightwave-rf-iphone-model
//
//  Created by Daniel Nuttall on 25/11/2012.
//  Copyright (c) 2012 Daniel Nuttall. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Device;

typedef enum
{
    ACTION_ERROR = 1,
    ACTION_DIMMER,
    ACTION_OPENCLOSE,
    ACTION_SOCKET,
    ACTION_MOOD,
    ACTION_ALLOFF,
    ACTION_FSL500W,
    ACTION_FSL3000W
    
} ActionTypes;

@interface Action : NSObject

@property (strong, nonatomic) Device *device;
@property (nonatomic) int setting;
@property (nonatomic) int assumedState;
@property (nonatomic, readonly) int lastOnState;
@property (nonatomic, readonly) int zoneID;
@property (nonatomic, readonly) int deviceID;
@property (strong, nonatomic) NSString *delay;
@property (strong, nonatomic, readonly) NSString *hardwareKey;
@property (strong, nonatomic, readonly) NSString *udp;
@property (strong, nonatomic, readonly) NSString *shortString;
@property (strong, nonatomic, readonly) NSString *name;
@property (strong, nonatomic, readonly) NSString *type;
@property (strong, nonatomic, readonly) NSString *zoneName;
@property (strong, nonatomic) NSString *hwKey;

-(NSArray *) getValuesFrom:(NSString *) command;
-(id) initWithDevice:(Device *) device setting:(int) setting delay:(NSString *) delay hardwareKey:(NSString *) hardwareKey;
-(id) initWithHardwareKey:(NSString *) hardwareKey;
-(id) initWithDevice:(Device *)device setting:(int)setting;
-(id) initWithHardwareKey:(NSString *)hardwareKey delay:(NSString *) delay;
-(id) initWithCommand:(NSString *) command;
-(Device*)getDevice;
-(BOOL) isDimmer;
-(BOOL) isColourLEDs;
-(BOOL) isLightSwitch;
-(BOOL) isHeating;
-(BOOL) isMagSwitch;
-(BOOL) isPir;
-(BOOL) isCamera;
-(BOOL) isMood;
-(BOOL) isSocket;
-(BOOL) isOpenClose;
-(BOOL) isFSL500W;
-(BOOL) isFSL3000W;
-(BOOL) isAllOff;
-(BOOL) isActiveDevice;
-(BOOL) isPassiveDevice;
-(int) turnOn;
-(int) turnOff;
-(BOOL) isPaired;
-(NSString *) toString;
-(NSDictionary *) toDictionary;
-(NSData *) toJSON;
-(NSString *)settingToKeyString;

- (BOOL)isDelayed;
- (int)getDelaySecs;
- (int)getMoodID;
- (NSString *)getDeviceType;
- (int)getDelaySecsPortion;
- (int)getDelayMinsPortion;
- (int)getDelayHoursPortion;
- (int)getSettingFromHardwareKey:(NSString *)hwKey;
- (NSString *)getDelay;
- (NSString *)getType;
- (BOOL)updateHwKey;

@end
