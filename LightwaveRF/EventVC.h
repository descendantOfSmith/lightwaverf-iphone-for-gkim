//
//  EventVC.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "Event.h"
#import "LightwaveActionSheet.h"

typedef enum
{
    kEnergyVCNameCell = 0,
    kEnergyVCEditActionsCell
} EventVCTags;

typedef enum
{
    kEnergyVCStartNow = 0,
    kEnergyVCStartAtTime,
    kEnergyVCScheduleEvent,
    kEnergyVCCancel
} EventVCActionSheetTags;

@interface EventVC : RootViewController <UITableViewDelegate, UITableViewDataSource, LightwaveActionSheetDataSource, LightwaveActionSheetDelegate> {
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil event:(Event *)tmpEvent;

@end
