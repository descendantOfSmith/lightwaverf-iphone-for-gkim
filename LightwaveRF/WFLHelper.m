//
//  TimerHelper.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 26/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "WFLHelper.h"
#import "Home.h"
#import "LTimer.h"
#import "Event.h"
#import "Toast+UIView.h"
#import "AppDelegate.h"
#import "NSString+Utilities.h"

@interface WFLHelper ()
@property (nonatomic, assign) BOOL isUpdatingWFL;
@end

static WFLHelper *wflHelper = nil;

@implementation WFLHelper

@synthesize isCheckingState;
@synthesize isUpdatingWFL;

+ (id)shared {
    @synchronized(self) {
        if(wflHelper == nil)
            wflHelper = [[super allocWithZone:NULL] init];
    }
    return wflHelper;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self shared];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)init {
    if (self = [super init]) {
        
        self.isCheckingState = YES;
        self.isUpdatingWFL = NO;
        
        // UDP
        [self endUDPListeners];
        [self startUDPListeners];
        
        // Remote
        [self endRemoteListeners];
        [self startRemoteListeners];
    }
    return self;
}

- (void)dealloc {
    //DebugLog(@"DEALLOC");
    [self endUDPListeners];
    [self endRemoteListeners];
}

#pragma mark - WiFi/Remote Continuous Check

// Override setter
- (void)setIsCheckingState:(BOOL)_isCheckingState {
    self->isCheckingState = _isCheckingState;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:[self checkStateSel]
                                               object:nil];
}

- (SEL)checkStateSel {
    return @selector(checkStateFire);
}

- (void)checkState {
    //DebugLog(@"checkState");
    self.isCheckingState = YES;
    
    if ([self respondsToSelector:[self checkStateSel]])
        [self performSelector:[self checkStateSel]
                   withObject:nil
                   afterDelay:6.0f];
}

- (void)checkStateFire {
    //DebugLog(@"checkStateFire");
    
    BOOL _sendPollCommand = YES;
    
    /**********************************
     * Check if at account location
     **********************************/
    if ([[UserPrefsHelper shared] getUseWiFiName]) {
        
        if ([[UserPrefsHelper shared] isAtCurrentLocation]) {
            
            //DebugLog(@"GO TO UDP!");
            
            // If at home, revert to UDP mode
            [[UserPrefsHelper shared] setTestWifi:NO];
            
            // Change Indicator to UDP
            [[UDPService shared] setServiceType:kServiceUDP];
            
            // Enable Polling
            _sendPollCommand = YES;
            
        } else {
            
            //DebugLog(@"GO TO REMOTE!");
            
            // If not at home, revert to remote mode
            [[UserPrefsHelper shared] setTestWifi:YES];
            
            // Change Indicator to Remote
            [[UDPService shared] setServiceType:kServiceWeb];
            
            // Disable Polling
            _sendPollCommand = NO;
        }
    }

    /**********************************
     * Check for UDP/Remote State
     **********************************/
    if ([[RemoteSettings shared] queueDictArray].count > 0) {
        // If a remote request is in the queue, don't poll just yet.
        // Wait for it to complete and then re-start polling
        [self checkState];
        
    } else {
        if (_sendPollCommand) {
            [[UDPService shared] pollWithTag:kRequestPollState
                                callingClass:[self getClassName:[self class]]];
        } else {
            // Try again...
            [self checkState];
        }
    }
}

#pragma mark - Public

- (void)updateWFL {
    // First remove ALL existing Sequences and timers from WFL
    // When this is complete, we will loop through ALL sequences & timers to re-create on WFL.

    if (!self.isUpdatingWFL) {
        self.isUpdatingWFL = YES;
        [kApplicationDelegate.window makeToast:NSLocalizedString(@"updating_wfl", @"Updating WFL events & timers")
                                      duration:3.0f
                                      position:@"center"];
    }
    
    [self deleteAllSequencesAndTimersUsingUDP:YES];
}

- (void)cleanupAllRequests {
    [[UDPService shared] cleanupAllRequests];
    [[RemoteSettings shared] cleanupAllRequests];
    [self setIsUpdatingWFL:NO];
}

- (void)sendToUDP:(BOOL)_useUDP command:(NSString *)_command tag:(ServiceRequestTags)_tag {
    
    if (_command.length > 0) {
        BOOL useRemote = [[NSUserDefaults standardUserDefaults] boolForKey:@"testWifi"];
         
        if (_useUDP && !useRemote) {
            
            // UDP
            UDPService *udp = [UDPService shared];
            [udp send:_command
                  tag:_tag
         callingClass:[self getClassName:[self class]]];
            
        } else {
            
            // Remote Settings
            RemoteSettings *remote = [RemoteSettings shared];
            [remote sendCommandWithCallingClass:[self getClassName:[self class]]
                                            tag:_tag
                                        command:_command];
            
        }
    } else
        // No Command, just remove from queue
        [self createdWithCommand:_command tag:_tag];
}

#pragma mark - Notifications

- (NSString *)getClassName:(Class)_class {
    const char* className = class_getName(_class);
    return [NSString stringWithFormat:@"%s", className];
}

- (void)addObserverWithName:(NSString *)_name selector:(SEL)_selector {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:_selector
                                                 name:_name
                                               object:nil];
}

#pragma - UDP Service

- (void)startUDPListeners {
    [self addObserverWithName:kUDPErrorNotificationKey selector:@selector(receivedUDPServiceNotification:)];
    [self addObserverWithName:kUDPSentNotificationKey selector:@selector(receivedUDPServiceNotification:)];
    [self addObserverWithName:kUDPFailedNotificationKey selector:@selector(receivedUDPServiceNotification:)];
}

- (void)endUDPListeners {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDPErrorNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDPSentNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDPFailedNotificationKey object:nil];
}

#pragma - Remote Service

- (void)startRemoteListeners {
    [self addObserverWithName:kRemoteErrorNotificationKey selector:@selector(receivedRemoteServiceNotification:)];
    [self addObserverWithName:kRemoteSentNotificationKey selector:@selector(receivedRemoteServiceNotification:)];
    [self addObserverWithName:kRemoteFailedNotificationKey selector:@selector(receivedRemoteServiceNotification:)];
}

- (void)endRemoteListeners {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRemoteErrorNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRemoteSentNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRemoteFailedNotificationKey object:nil];
}

#pragma mark - Delete All Timers

- (void)deleteAllSequencesAndTimersUsingUDP:(BOOL)_useUDP {
    //DebugLog(@"Deleting All");
    
    [self sendToUDP:_useUDP
            command:[ServiceType getDeleteAllSequencesAndTimersCommand]
                tag:kRequestDeleteAllSequencesAndTimers];
}

#pragma mark - Retry

- (void)checkCreatedAllSequencesAndTimers {
    if (self.isUpdatingWFL == NO) {
        return;
    }
    
    Home *home = [Home shared];
    
    BOOL _isCreating = NO;
    
    // Loop through events array and create on WFL.
    for (Event *_event in home.events) {
        //DebugLog(@"Checking: %@, ID: %d, created? %d", _event.name, _event.eventID, _event.createdOnWFL);
        if (_event.createdOnWFL == NO) {
            
            // Create Event on WFL
            if ([_event getCommand]) {
                _isCreating = YES;
                //DebugLog(@"CREATE = %@", [_event getCommand]);
                [self sendToUDP:YES command:[_event getCommand] tag:kRequestCreateEvents];
                
                break;
            } else {
                _event.createdOnWFL = YES;
            }
        }
    }
    
    // Loop through timers array and create on WFL.
    if (!_isCreating) {
        for (LTimer *_timer in home.timers) {
            if (_timer.createdOnWFL == NO) {
                
                // Create Timer on WFL
                _isCreating = YES;
                
                // Adjust start time if required
                [_timer adjustStartDateToTomorrow];
                
                // Start
                [self sendToUDP:YES command:[_timer getCommand] tag:kRequestCreateTimers];

                break;
            }
        }
    }
    
    if (!_isCreating) {
        // Complete!
        //DebugLog(@"Created all sequences & timers on WFL");
        self.isUpdatingWFL = NO;
        [kApplicationDelegate.window makeToast:NSLocalizedString(@"updating_wfl_complete", @"Updating WFL complete")
                                      duration:3.0f
                                      position:@"center"]; 
    }
}

#pragma mark - Completion Handlers

- (void)createdWithCommand:(NSString *)_fullCommand tag:(NSUInteger)_tag {
    //DebugLog(@"Created %@", _tag == kRequestCreateTimers ? @"Timer" : @"Event");

    // Record the Timer as created on the WFL
    [self recordTimerAsCreatedWithCommand:_fullCommand tag:_tag];
    
    // Record the Event as created on the WFL
    [self recordEventAsCreatedWithCommand:_fullCommand tag:_tag];

    // Leave 2 seconds between each UDP call.
    SEL _selector = @selector(checkCreatedAllSequencesAndTimers);
    if ([self respondsToSelector:_selector])
        [self performSelector:_selector withObject:nil afterDelay:2.0f];
}

- (void)recordTimerAsCreatedWithCommand:(NSString *)_fullCommand tag:(NSUInteger)_tag {
    if (_tag == kRequestCreateTimers || _tag == kRequestCreateTimer) {
        Home *home = [Home shared];
        NSString *_propertyToSearch = @"getCommand";
        NSPredicate *_predicate = [NSPredicate predicateWithFormat:@"%K contains %@", _propertyToSearch, _fullCommand];
        NSArray *_foundTimerArray = [home.timers filteredArrayUsingPredicate:_predicate];
        if (_foundTimerArray.count > 0) {
            LTimer *_timer = [_foundTimerArray objectAtIndex:0];
            [_timer setCreatedOnWFL:YES];
        }
    }
}

- (void)recordEventAsCreatedWithCommand:(NSString *)_fullCommand tag:(NSUInteger)_tag {
    if (_tag == kRequestCreateEvents || _tag == kRequestCreateEvent) {
        Home *home = [Home shared];
        for (Event *_event in home.events) {
            if ([[_event getCommand] isEqualToString:_fullCommand]) {
                [_event setCreatedOnWFL:YES];
            }
        }
    }
}

- (void)creatingSequenceOrTimerFailed {
    //DebugLog(@"creating sequence or timer failed!");
    
    // Retry create in 30 seconds
    SEL _selector = @selector(checkCreatedAllSequencesAndTimers);
    if ([self respondsToSelector:_selector])
        [self performSelector:_selector withObject:nil afterDelay:30.0f];
}

- (void)allSequencesAndTimersDeleted {
    //DebugLog(@"All Sequences & Timers Deleted!");
    
    // Mark all Sequences & Timers as not created on WFL
    Home *home = [Home shared];
    
    for (Event *_event in home.events)
        [_event setCreatedOnWFL:NO];
    
    for (LTimer *_timer in home.timers)
        [_timer setCreatedOnWFL:NO];
    
    // Now create any available
    [self checkCreatedAllSequencesAndTimers];
}

- (void)deleteSequencesAndTimersFailed {
    //DebugLog(@"Failed: Delete All Sequences And Timers");
    
    // Retry delete timers in 30 seconds
    // After delete, we can create on the WFL
    SEL _selector = @selector(updateWFL);
    if ([self respondsToSelector:_selector])
        [self performSelector:_selector withObject:nil afterDelay:30.0f];
}

#pragma mark - Remote Delegate

- (void)receivedRemoteServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kRemoteClassNameKey];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
        switch (_tag) {
                
            case kRequestDeleteAllSequencesAndTimers:
            {
                [self handleRemoteDeleteAll:_notification];
            }
                break;
                
            case kRequestCreateEvents:
            case kRequestCreateTimers:
            {
                [self handleRemoteCreateEventOrTimer:_notification];
            }
                break;
                
            case kRequestCancelEvent:  
            case kRequestDeleteEvent:
            case kRequestDeleteTimer:
            case kRequestToggleOnOff:
            case kRequestStop:
            case kRequestDim:
            case kRequestMood:
            case kRequestCreateEvent:
            case kRequestCreateTimer:
            case kRequestLocation:
            case kRequestTimeZone:
            case kRequestLEDColourChange:
            case kRequestLEDAutoCycle:
            case kRequestImpKwh:
            {
                [self handleRemoteGeneric:_notification];
            }
                break;
                
            default:
                DebugLog(@"FIX: Unhandled Remote Request Tag: %d", _tag);
                break;
        }
    }
}

#pragma -
#pragma Remote handlers

- (void)handleRemoteDeleteAll:(NSNotification *)_notification {
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey] ||
        [[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Error || Fail
         *************************/
        [self deleteSequencesAndTimersFailed];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self allSequencesAndTimersDeleted];
    }
}

- (void)handleRemoteCreateEventOrTimer:(NSNotification *)_notification {
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey] ||
        [[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Error || Fail
         *************************/
        [self creatingSequenceOrTimerFailed];
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
        NSString *_timerCommand = [_dict objectForKey:kRemoteCommandKey];
        NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
        [self createdWithCommand:_timerCommand tag:_tag];
    }
}

- (void)handleRemoteGeneric:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kRemoteCommandKey];
    NSUInteger _tag = [[_dict objectForKey:kRemoteTagKey] intValue];
    
    if ([[_notification name] isEqualToString:kRemoteErrorNotificationKey] ||
        [[_notification name] isEqualToString:kRemoteFailedNotificationKey])
    {
        /*************************
         * Error || Fail
         *************************/        
        if (_tag == kRequestCancelEvent)
            DebugLog(@"Failed Cancelling Event");
        else if (_tag == kRequestDeleteEvent)
            DebugLog(@"Failed Deleting Event");
        else if (_tag == kRequestDeleteTimer)
            DebugLog(@"Failed Deleting Timer");
        else if (_tag == kRequestToggleOnOff)
            DebugLog(@"Failed Toggling On/Off");
        else if (_tag == kRequestStop)
            DebugLog(@"Failed Stopped");
        else if (_tag == kRequestDim)
            DebugLog(@"Failed Dimmed");
        else if (_tag == kRequestMood)
            DebugLog(@"Failed to start Mood");
        else if (_tag == kRequestCreateEvent)
            DebugLog(@"Failed to create Event");
        else if (_tag == kRequestCreateTimer)
            DebugLog(@"Failed to create Timer");
        else if (_tag == kRequestLocation)
            DebugLog(@"Failed to store Location");
        else if (_tag == kRequestTimeZone)
            DebugLog(@"Failed to set Timezone");
        else if (_tag == kRequestLEDColourChange)
            DebugLog(@"Failed to set LED Colour");
        else if (_tag == kRequestLEDAutoCycle)
            DebugLog(@"Failed to set LED Auto Cycle");
        else if (_tag == kRequestImpKwh)
            DebugLog(@"Failed to store imp/KWH");
    }
    
    else if ([[_notification name] isEqualToString:kRemoteSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        if (_tag == kRequestCancelEvent)
            DebugLog(@"Cancelled Event");
        else if (_tag == kRequestDeleteEvent)
            DebugLog(@"Deleted Event");
        else if (_tag == kRequestDeleteTimer)
            DebugLog(@"Deleted Timer");
        else if (_tag == kRequestToggleOnOff)
            DebugLog(@"Toggled On/Off");
        else if (_tag == kRequestStop)
            DebugLog(@"Stopped");
        else if (_tag == kRequestDim)
            DebugLog(@"Dimmed");
        else if (_tag == kRequestMood)
            DebugLog(@"Started Mood");
        else if (_tag == kRequestCreateEvent) {
            DebugLog(@"Created Event");
            
            // Record the Event as created on the WFL
            [self recordEventAsCreatedWithCommand:_command tag:_tag];
            
        } else if (_tag == kRequestCreateTimer) {
            DebugLog(@"Created Timer");
            
            // Record the Timer as created on the WFL
            [self recordTimerAsCreatedWithCommand:_command tag:_tag];
            
        } else if (_tag == kRequestLocation)
            DebugLog(@"Stored Location");
        
        else if (_tag == kRequestTimeZone)
            DebugLog(@"Timezone Set");
        
        else if (_tag == kRequestLEDColourChange)
            DebugLog(@"LED Colour Set");
        
        else if (_tag == kRequestLEDAutoCycle)
            DebugLog(@"LED Auto Cycle Set");
        
        else if (_tag == kRequestImpKwh)
            DebugLog(@"imp/KWH Set");
    }
}

#pragma -
#pragma UDP Delegate

- (void)receivedUDPServiceNotification:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_callingClassName = [_dict objectForKey:kUDPClassNameKey];
    NSUInteger _tag = [[_dict objectForKey:kUDPTagKey] intValue];
        
    // Continuously check WiFi/Remote State
    if (_tag != kRequestPollState)
        [self handleUDPCheckState:_notification];
    
    // Check this VC called the service
    if ([_callingClassName isEqualToString:[self getClassName:[self class]]]) {
        
        switch (_tag) {
                
            case kRequestDeleteAllSequencesAndTimers:
            {
                [self handleUDPDeleteAll:_notification];
            }
                break;
                
            case kRequestCreateEvents:
            case kRequestCreateTimers:
            {
                [self handleUDPCreateEventOrTimer:_notification];
            }
                break;
                
            case kRequestCancelEvent:   
            case kRequestDeleteEvent:
            case kRequestDeleteTimer:
            case kRequestToggleOnOff:
            case kRequestStop:
            case kRequestOpen:
            case kRequestClose:
            case kRequestDim:
            case kRequestMood:
            case kRequestCreateEvent:
            case kRequestCreateTimer:
            case kRequestLocation:
            case kRequestTimeZone:
            case kRequestLEDColourChange:
            case kRequestLEDAutoCycle:
            case kRequestImpKwh:
            {
                [self handleUDPGeneric:_notification];
            }
                break;
                
            case kRequestPollState:
            {
                NSString *_response = [_dict objectForKey:kUDPResponseKey];
                //DebugLog(@"response = %@", _response);
                
                if ([_response containsString:@"ERR"]) {
                    
                    // Not yet registered... check wifi link
                    // Disable continuously 3 second checking for the time being...
                    self.isCheckingState = NO;
                    [[NSNotificationCenter defaultCenter] postNotificationName:kUDPCheckStateFailNotificationKey object:nil];
                    [kApplicationDelegate.window makeToast:NSLocalizedString(@"settings_wifi_register", @"Check WiFiLink and press 'Yes' to register this device.")
                                                  duration:3.0f
                                                  position:@"bottom"];

                } else
                    // Got version... continue checking...
                    [self handleUDPCheckState:_notification];
                
            }
                break;
                
            default:
            {
                DebugLog(@"FIX: Unhandled UDP Request Tag: %d", _tag);
            }
                break;
        }   
    }
}

#pragma -
#pragma UDP handlers

- (void)handleUDPCheckState:(NSNotification *)_notification {

    if ([[_notification name] isEqualToString:kUDPErrorNotificationKey] ||
        [[_notification name] isEqualToString:kUDPFailedNotificationKey])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kUDPCheckStateFailNotificationKey object:nil];
    }
    
    else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kUDPCheckStateSuccessNotificationKey object:nil];
    } else
        
        DebugLog(@"FIX: Unhandled Response Name: %@", [_notification name]);
    
    [self checkState];
}

- (void)handleUDPDeleteAll:(NSNotification *)_notification {
    if ([[_notification name] isEqualToString:kUDPErrorNotificationKey] ||
        [[_notification name] isEqualToString:kUDPFailedNotificationKey])
    {
        /*************************
         * Error
         *************************/
        // Attempt the request using RemoteSettings...
        [self deleteAllSequencesAndTimersUsingUDP:NO];
    }
    
    else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self allSequencesAndTimersDeleted];
    }
}

- (void)handleUDPCreateEventOrTimer:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kUDPCommandKey];
    NSUInteger _tag = [[_dict objectForKey:kUDPTagKey] intValue];
    
    if ([[_notification name] isEqualToString:kUDPErrorNotificationKey] ||
        [[_notification name] isEqualToString:kUDPFailedNotificationKey])
    {
        /*************************
         * Error || Fail
         *************************/
        // Attempt the request using RemoteSettings...
        [self sendToUDP:NO command:_command tag:_tag];
    }
    
    else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        [self createdWithCommand:_command tag:_tag];
    }
}

- (void)handleUDPGeneric:(NSNotification *)_notification {
    NSMutableDictionary *_dict = (NSMutableDictionary *)[_notification object];
    NSString *_command = [_dict objectForKey:kUDPCommandKey];
    NSUInteger _tag = [[_dict objectForKey:kUDPTagKey] intValue];
    
    if ([[_notification name] isEqualToString:kUDPErrorNotificationKey] ||
        [[_notification name] isEqualToString:kUDPFailedNotificationKey])
    {
        /*************************
         * Error || Fail
         *************************/
        // Attempt the request using RemoteSettings...
        [self sendToUDP:NO command:_command tag:_tag];
    }
    
    else if ([[_notification name] isEqualToString:kUDPSentNotificationKey])
    {
        /*************************
         * Success
         *************************/
        if (_tag == kRequestCancelEvent)
            DebugLog(@"Canceled Event");
        else if (_tag == kRequestDeleteEvent)
            DebugLog(@"Deleted Event");
        else if (_tag == kRequestDeleteTimer)
            DebugLog(@"Deleted Timer");
        else if (_tag == kRequestToggleOnOff)
            DebugLog(@"Toggled On/Off");
        else if (_tag == kRequestStop)
            DebugLog(@"Stopped");
        else if (_tag == kRequestOpen)
            DebugLog(@"Opened");
        else if (_tag == kRequestClose)
            DebugLog(@"Closed");
        else if (_tag == kRequestDim)
            DebugLog(@"Dimmed");
        else if (_tag == kRequestMood)
            DebugLog(@"Started Mood");
        else if (_tag == kRequestCreateEvent) {
            DebugLog(@"Created Event");
            
            // Record the Event as created on the WFL
            [self recordEventAsCreatedWithCommand:_command tag:_tag];
            
        } else if (_tag == kRequestCreateTimer) {
            DebugLog(@"Created Timer");
            
            // Record the Timer as created on the WFL
            [self recordTimerAsCreatedWithCommand:_command tag:_tag];
            
        }
        
        else if (_tag == kRequestLocation)
            DebugLog(@"Stored Location");
        else if (_tag == kRequestTimeZone)
            DebugLog(@"Timezone Set");
        else if (_tag == kRequestLEDColourChange)
            DebugLog(@"LED Colour Set");
        else if (_tag == kRequestLEDAutoCycle)
            DebugLog(@"LED Auto Cycle Set");
        else if (_tag == kRequestImpKwh)
            DebugLog(@"Stored Imp/kWH");
        
        
        
        
        
        
 
    }
}

@end
