//
//  RemoteSettings.m
//  LightwaveRF
//
//  Created by Nik Lever on 06/12/2012.
//  Copyright (c) 2012 Nicholas Lever. All rights reserved.
//

#import "RemoteSettings.h"
#import "XMLConfig.h"
#import "AppDelegate.h"
#import "NSString+MD5.h"
#import "NSString+URLEncoding.h"
#import "GTMNSString+HTML.h"

static RemoteSettings *myRemote = nil;

@interface RemoteSettings ()

@end

@implementation RemoteSettings

@synthesize queueDictArray;

+ (id)shared {
    @synchronized(self) {
        if(myRemote == nil)
            myRemote = [[super allocWithZone:NULL] init];
    }
    return myRemote;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self shared];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)init {
    if (self = [super init]) {
        AMAZON_WEBSERVER = @"lrfcontrol.com";
        
        if ([AppStyleHelper isNexa]) {
            ONEANDONE_WEBSERVER = @"www.lrfcontrol.com";
        } else {
            ONEANDONE_WEBSERVER = @"www.lightwaverfhost.co.uk";
        }
        
        ACTIVE_WEBSERVER = ONEANDONE_WEBSERVER;
        PUSH_HOME_RESOURCE_URL = [NSString stringWithFormat:@"http://%@/manager/saveuserdata.php", ACTIVE_WEBSERVER];
        PULL_HOME_RESOURCE_URL = [NSString stringWithFormat:@"http://%@/getsettingsxml.php", ACTIVE_WEBSERVER];
        SEND_COMMAND_URL = [NSString stringWithFormat:@"http://%@/writerecord.php", ACTIVE_WEBSERVER];
        GET_METER_URL = [NSString stringWithFormat:@"http://%@/mobile/meter_free_load.php", ACTIVE_WEBSERVER];
        SET_PIN_URL = [NSString stringWithFormat:@"http://%@/setpin.php", ACTIVE_WEBSERVER];
    }
    return self;
}

#pragma mark - Setup

// returns transactionID
- (NSUInteger)setupWithTag:(NSUInteger)_tag
              callingClass:(NSString *)_className  {
    
    NSUInteger _transactionID = [RemoteSettings getTransactionID];
    
    NSMutableDictionary *_transactionDict = [self createDictWithTag:_tag
                                                           callingClass:_className
                                                          transactionID:_transactionID];
    
    // Setup
    if (!self.queueDictArray)
        self.queueDictArray = [[NSMutableArray alloc] init];
    
    // Remove from Queue if already exisits (Safe-guard)
    [self removeTransactionDictFromQueueWithTransactionID:_transactionID];
    
    // Add Transaction to Queue
    [self addTransactionDictToQueue:_transactionDict];
    
    return _transactionID;
}

#pragma mark - Private

- (NSMutableDictionary *)createDictWithTag:(NSUInteger)_tag
                              callingClass:(NSString *)_className
                             transactionID:(NSUInteger)_transactionID {
    
    return [[NSMutableDictionary alloc] initWithObjectsAndKeys:
            [NSDate date], kRemoteDateKey,
            [NSNumber numberWithInt:_tag], kRemoteTagKey,
            _className, kRemoteClassNameKey,
            [NSNumber numberWithInt:_transactionID], kRemoteTransactionIDKey,
            nil];
}

- (NSMutableDictionary *)getTransactionDictWithTransactionID:(NSUInteger)_transactionID {
    NSPredicate *filter = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %d", kRemoteTransactionIDKey, _transactionID]];
    NSArray *filteredQueue = [self.queueDictArray filteredArrayUsingPredicate:filter];
    if (filteredQueue.count > 0)
        return [filteredQueue objectAtIndex:0];
    else
        return nil;
}

- (NSUInteger)getTagFromDict:(NSMutableDictionary *)_dict {
    return [[_dict objectForKey:kRemoteTagKey] intValue];
}

- (NSString *)getClassNameFromDict:(NSMutableDictionary *)_dict {
    return [_dict objectForKey:kRemoteClassNameKey];
}

- (NSUInteger)getTransactionIDFromDict:(NSMutableDictionary *)_dict {
    return [[_dict objectForKey:kRemoteTransactionIDKey] intValue];
}

- (NSString *)getTimeStamp {
    // Should return a UNIX timestamp, i.e. "1222738875"
    return @"1351178968";
    //return [self dateInFormat:@"%s"];
}

- (NSString *)getRawSecret {
    return [NSString stringWithFormat:@"jsiphone2434%@", [self getTimeStamp]];
}

- (NSString *)getSecret {
    return @"4beef6e4354fdeb384a8c5259e089623";
    //return [NSString stringWithFormat:@"%@", [[self getRawSecret] MD5]];
}

- (BOOL)haveCredentials {
	NSString * userEmail            = [[UserPrefsHelper shared] getUserEmail];        // @"david @w5h.com";
	NSString * userPassword         = [[UserPrefsHelper shared] getUserPassword];     // @"1595";
	NSString * userMac              = [[UserPrefsHelper shared] getUserMac];          // @"74:0A:BC:FF:FF:00";
    
    DebugLog(@"userEmail: %@", userEmail);
    DebugLog(@"userPassword: %@", userPassword);
    DebugLog(@"userMac: %@", userMac);
    
    if (userEmail && userPassword && userMac)
        return YES;
    else
        return NO;
}

- (NSString *)dateInFormat:(NSString*)stringFormat {
	char buffer[80];
	const char *format = [stringFormat UTF8String];
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer, 80, format, timeinfo);
	return [NSString  stringWithCString:buffer encoding:NSUTF8StringEncoding];
}

#pragma mark - Overridden from superclass

- (void)purgeExpiredRequests:(NSTimer *)timer {
    NSMutableArray *_removeRequestsArray = [[NSMutableArray alloc] init];
    for (NSMutableDictionary *_dict in self.queueDictArray) {
        NSDate *_date = [_dict objectForKey:kRemoteDateKey];
        NSInteger _numMins = [NSDate minutesBetweenDate:_date andDate:[NSDate date]];
        if (_numMins > kExpireNumMins) {
            [_removeRequestsArray addObject:_dict];
        }
    }
    
    [self.queueDictArray removeObjectsInArray:_removeRequestsArray];
}

- (void)cleanupAllRequests {
    [self.queueDictArray removeAllObjects];    
    [[NSOperationQueue mainQueue] cancelAllOperations];
}

#pragma mark - Retrieve a Transaction ID

+ (NSString *)getTransactionIDKey {
    return [NSString stringWithFormat:@"RemoteSettings-%@", NSStringFromSelector(_cmd)];
}

+ (void)setTransactionID:(NSUInteger)_transactionID {
    if (_transactionID >= NSUIntegerMax)
        _transactionID = 0;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:[NSNumber numberWithInt:_transactionID] forKey:[self getTransactionIDKey]];
    [userDefaults synchronize];
}

+ (NSUInteger)getTransactionID {
    NSNumber *_transactionID = [[NSUserDefaults standardUserDefaults] objectForKey:[self getTransactionIDKey]];
    NSUInteger _transID = [_transactionID intValue];
    
    if (!_transactionID)
        _transID = 0;
    
    [RemoteSettings setTransactionID:_transID + 1];
    
    return _transID;
}

#pragma mark - Remote Request Queue

- (void)addTransactionDictToQueue:(NSMutableDictionary *)_transactionDict {
    [self.queueDictArray addObject:_transactionDict];
}

- (void)removeTransactionDictFromQueue:(NSMutableDictionary *)_transactionDict {
    if ([self.queueDictArray containsObject:_transactionDict])
        [self.queueDictArray removeObject:_transactionDict];
}

- (void)removeTransactionDictFromQueueWithTransactionID:(NSUInteger)_transactionID {
    NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_transactionID];
    [self removeTransactionDictFromQueue:_dict];
}

#pragma mark - Demo Account

/*
 * If logged in with demo account, disable remote calls...
 */
- (BOOL)checkCanSendWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag {
    
//#if TARGET_IPHONE_SIMULATOR
//    return NO;
//#endif
    
    BOOL _canSend = [[UserPrefsHelper shared] getDemoAccount] ? NO : YES;
    
    if (!_canSend) {
        NSUInteger _transactionID = [RemoteSettings getTransactionID];
        NSMutableDictionary *_transactionDict = [self createDictWithTag:_tag
                                                           callingClass:_className
                                                          transactionID:_transactionID];
        
        NSString *_errorMsg = NSLocalizedString(@"test_account", @"test_account");
        NSMutableDictionary *_errorDetail = [NSMutableDictionary dictionary];
        [_errorDetail setValue:_errorMsg
                       forKey:NSLocalizedDescriptionKey];
        NSError *_error = [NSError errorWithDomain:kAppName code:100 userInfo:_errorDetail];
        
        [_transactionDict setValue:_error forKey:kRemoteErrorKey];
        [self isService:kServiceWeb];
        [self errorCallbackForTransactionDict:_transactionDict];
    }

    return _canSend;
}

#pragma mark - Public

- (void)setPinWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag withPin:(NSString *)pin {
    if ([self checkCanSendWithCallingClass:_className tag:_tag]) {
        
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        NSString * _urlString = [NSString stringWithFormat:@"%@?action=P&pin=%@&mac=%@&email=%@", SET_PIN_URL, pin, app.tmpMac, app.tmpEmail];
        _urlString = [_urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //DebugLog(@"[Remote] Set Pin: %@", _urlString);
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]
                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                             timeoutInterval:kRemoteTimeoutTime];
        NSUInteger _transactionID = [self setupWithTag:_tag callingClass:_className];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   
                                   NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_transactionID];
                                   if (error){
                                       //DebugLog(@"Error: %@", [error localizedDescription]);
                                       [_dict setValue:error forKey:kRemoteErrorKey];
                                       [self errorCallbackForTransactionDict:_dict];
                                       
                                   }else{
                                       NSString *replyMessage = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                       [_dict setValue:replyMessage forKey:kRemoteResponseKey];
                                       //DebugLog(@"Success: %@", replyMessage);
                                       [self sentCallbackForTransactionDict:_dict checkService:NO];
                                   }
                               }];
    }
}


- (void)pullHomeWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag withEmail:(NSString*)email andPassword:(NSString*)password {
    if ([self checkCanSendWithCallingClass:_className tag:_tag]) {
        
        NSString * _urlString = [NSString stringWithFormat:@"%@?action=D&email=%@&pin=%@", PULL_HOME_RESOURCE_URL, email, password];
        _urlString = [_urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        DebugLog(@"[Remote] Pull Home: %@", _urlString);
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]
                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                             timeoutInterval:kRemoteTimeoutTime];
        NSUInteger _transactionID = [self setupWithTag:_tag callingClass:_className];

        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_transactionID];
                                   if (error){
                                       DebugLog(@"Error: %@", [error localizedDescription]);
                                       [_dict setValue:error forKey:kRemoteErrorKey];
                                       [self errorCallbackForTransactionDict:_dict];
                                   }else{
                                       //NSString *replyMessage = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                       NSString *replyMessage = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                       //replyMessage = [replyMessage gtm_stringByUnescapingFromHTML];
                                       
                                       // remove whitespace and new line chars...
                                       replyMessage = [replyMessage stringByTrimmingCharactersInSet:
                                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                       
                                       [_dict setValue:replyMessage forKey:kRemoteResponseKey];
                                       DebugLog(@"Success: %@", replyMessage);
                                       
                                       if (replyMessage.length > 0) {
                                           [self sentCallbackForTransactionDict:_dict checkService:NO];
                                           [self refreshData];
                                           
                                       } else {
                                           NSString *_errorMsg = NSLocalizedString(@"login_error2", @"login_error2");
                                           NSMutableDictionary *_errorDetail = [NSMutableDictionary dictionary];
                                           [_errorDetail setValue:_errorMsg
                                                           forKey:NSLocalizedDescriptionKey];
                                           NSError *_error = [NSError errorWithDomain:kAppName code:100 userInfo:_errorDetail];
                                           DebugLog(@"Error: %@", [_error localizedDescription]);
                                           [_dict setValue:_error forKey:kRemoteErrorKey];
                                           [self errorCallbackForTransactionDict:_dict];
                                       }
                                   }
                               }];
    }
}

- (void)pushHomeWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag {
    if ([self checkCanSendWithCallingClass:_className tag:_tag]) {
        
        NSString *userdata = [[[XMLConfig shared] homeToXML] urlEncodeUsingEncoding:NSUTF8StringEncoding];
        //DebugLog(@"userdata = %@", userdata);
         
         NSString * userEmail   = [[UserPrefsHelper shared] getUserEmail];        // @"david @w5h.com";
         NSString * userPin     = [[UserPrefsHelper shared] getUserPassword];     // @"1595";
         NSString * userMac     = [[UserPrefsHelper shared] getUserMac];          // @"74:0A:BC:FF:FF:00";
        
        /*NSString *_urlString = [NSString stringWithFormat:@"%@?email=%@&pin=%@&mac=%@",
                               PUSH_HOME_RESOURCE_URL,
                               userEmail,
                               userPin,
                               userMac];
        
        _urlString = [_urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];*/
        
        //DebugLog(@"[Remote] Push Home: %@", _urlString);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:PUSH_HOME_RESOURCE_URL]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:kRemoteTimeoutTime];

        // Send POST Data
        NSString *_body = [NSString stringWithFormat:@"email=%@&pin=%@&settings=%@&signature=JSiPhone&secret=%@&timestamp=%@&mac=%@",
                             userEmail,
                             userPin,
                             userdata,
                             [self getSecret],
                             [self getTimeStamp],
                             userMac];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:[_body dataUsingEncoding:NSASCIIStringEncoding]];
        DebugLog(@"[Remote] POST Contents: %@", _body);
        
        NSUInteger _transactionID = [self setupWithTag:_tag callingClass:_className];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_transactionID];
                                   if (error){
                                       DebugLog(@"Error: %@", [error localizedDescription]);
                                       [_dict setValue:error forKey:kRemoteErrorKey];
                                       [self errorCallbackForTransactionDict:_dict];
                                   }else{
                                       NSString *replyMessage = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];                                   
                                       [_dict setValue:replyMessage forKey:kRemoteResponseKey];
                                       DebugLog(@"Success: %@", replyMessage);
                                       [self sentCallbackForTransactionDict:_dict checkService:NO];
                                   }
                               }];
    }
}


- (void)pullEnergyMeterWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag {
    if ([self checkCanSendWithCallingClass:_className tag:_tag]) {

        NSString * _urlString = [NSString stringWithFormat:@"http://%@/mobile/meter_free_load.php?email=%@&mac=%@",
                                ONEANDONE_WEBSERVER,
                                [[UserPrefsHelper shared] getUserEmail],
                                [[UserPrefsHelper shared] getUserMac]];
        _urlString = [_urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        DebugLog(@"[Remote] Pull Energy Meter: %@", _urlString);
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]
                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                             timeoutInterval:kRemoteTimeoutTime];
        NSUInteger _transactionID = [self setupWithTag:_tag callingClass:_className];
            
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_transactionID];
                                   if (error){
                                       DebugLog(@"Error: %@", [error localizedDescription]);
                                       [_dict setValue:error forKey:kRemoteErrorKey];
                                       [self errorCallbackForTransactionDict:_dict];
                                   }else{
                                       NSString *replyMessage = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                       [_dict setValue:replyMessage forKey:kRemoteResponseKey];
                                       DebugLog(@"Success: %@", replyMessage);
                                       [self sentCallbackForTransactionDict:_dict];
                                   }
                               }];
    }
}

- (void)stopAllEventsWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag {
    [self sendCommandWithCallingClass:_className tag:_tag command:[ServiceType getStopAllEventsCommand]];
}

- (void)sendCommandWithCallingClass:(NSString *)_className tag:(NSUInteger)_tag command:(NSString *)_command {
    if ([self checkCanSendWithCallingClass:_className tag:_tag]) {
        
        NSUInteger _transactionID = [self setupWithTag:_tag callingClass:_className];
        
        // Store command
        NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_transactionID];
        [_dict setValue:_command forKey:kRemoteCommandKey];
        
        if ([self haveCredentials]) {
                        
            NSString * userEmail    = [[UserPrefsHelper shared] getUserEmail];
            NSString * userMac      = [[UserPrefsHelper shared] getUserMac];
            
            // Fix any whitespace or new lines in command
            _command = [_command stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSMutableString *_urlString = [NSMutableString string];
            [_urlString appendString:SEND_COMMAND_URL];
            [_urlString appendString:@"?action=I"];
            [_urlString appendString:@"&signature=JSiPhone"];
            [_urlString appendFormat:@"&email=%@", userEmail];
            [_urlString appendFormat:@"&name=%@", userMac];
            [_urlString appendFormat:@"&secret=%@", [self getSecret]];
            [_urlString appendFormat:@"&timestamp=%@", [self getTimeStamp]];
            [_urlString appendFormat:@"&commandstring=%@", _command];
            
            // Escape URL
            [_urlString setString:[_urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            DebugLog(@"[Remote] Send Command: %@", _urlString);
                    
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]
                                                     cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                 timeoutInterval:kRemoteTimeoutTime];
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                       if (error) {
                                           //DebugLog(@"ERROR = %@", [error localizedDescription]);
                                           [_dict setValue:error forKey:kRemoteErrorKey];
                                           [self errorCallbackForTransactionDict:_dict];
                                       } else {
                                           NSString *replyMessage = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                           //DebugLog(@"SUCCESS = %@", replyMessage);
                                           [_dict setValue:replyMessage forKey:kRemoteResponseKey];
                                           [self sentCallbackForTransactionDict:_dict];
                                       }
                                   }];
        } else {
            NSMutableDictionary *_dict = [self getTransactionDictWithTransactionID:_transactionID];
            [_dict setValue:NSLocalizedString(@"message10", @"Have Credentials Failed") forKey:kRemoteFailedKey];
            [self failedCallbackForTransactionDict:_dict];
        }
    }
}

#pragma mark - Callbacks

- (void)sentCallbackForTransactionDict:(NSMutableDictionary *)_transactionDict {
    [self sentCallbackForTransactionDict:_transactionDict checkService:YES];
}

- (void)sentCallbackForTransactionDict:(NSMutableDictionary *)_transactionDict checkService:(BOOL)_checkService {
    [[NSNotificationCenter defaultCenter] postNotificationName:kRemoteSentNotificationKey object:_transactionDict];
    [self removeTransactionDictFromQueue:_transactionDict];
    
    if (_checkService)
        [self isService:kServiceWeb];
}

- (void)failedCallbackForTransactionDict:(NSMutableDictionary *)_transactionDict {
    [[NSNotificationCenter defaultCenter] postNotificationName:kRemoteFailedNotificationKey object:_transactionDict];
    [self removeTransactionDictFromQueue:_transactionDict];
}

- (void)errorCallbackForTransactionDict:(NSMutableDictionary *)_transactionDict {
    [[NSNotificationCenter defaultCenter] postNotificationName:kRemoteErrorNotificationKey object:_transactionDict];
    [self removeTransactionDictFromQueue:_transactionDict];
}

- (void)refreshData {
    [[NSNotificationCenter defaultCenter] postNotificationName:kRemoteRefreshTableViewNotificationKey object:nil];
}

@end
