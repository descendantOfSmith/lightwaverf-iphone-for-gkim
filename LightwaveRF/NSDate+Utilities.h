//
//  NSDateAdditions.h
//  Created by Devin Ross on 7/28/09.
//
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */
#import <Foundation/Foundation.h>

@interface NSDate (Utilities)

struct TKDateInformation {
	int day;
	int month;
	int year;
	
	int weekday;
	
	int minute;
	int hour;
	int second;
	
};
typedef struct TKDateInformation TKDateInformation;

- (TKDateInformation) dateInformation;
- (TKDateInformation) dateInformationWithTimeZone:(NSTimeZone*)tz;
+ (NSDate*) dateFromDateInformation:(TKDateInformation)info;
+ (NSDate*) dateFromDateInformation:(TKDateInformation)info timeZone:(NSTimeZone*)tz;

@property (readonly,nonatomic) NSString *month;
@property (readonly,nonatomic) NSString *year;
@property (readonly,nonatomic) int weekdayWithMondayFirst;
@property (readonly,nonatomic) BOOL isToday;

- (int)daysBetweenDate:(NSDate *)d;
- (NSString *)month;
- (NSString *)year;
+ (NSDate *)firstOfCurrentMonth;
+ (NSDate *)lastOfCurrentMonth;
- (NSDate *)timelessDate;
- (NSDate *)monthlessDate;
- (NSDate *)firstOfCurrentMonthForDate;
- (NSDate *)firstOfNextMonthForDate;
- (NSNumber*) yearNumber;
- (NSNumber*) monthNumber;
- (NSNumber *)dayNumber;
- (NSNumber*)hourNumber;
- (NSNumber*) minuteNumber;
- (NSNumber*) secondNumber;
- (NSString *)hourString;
- (NSString *)monthString;
- (NSString *)yearString;
- (NSString *)monthYearString;
- (int)weekday;
- (int)weekdayWithMondayFirst;
- (int)differenceInDaysTo:(NSDate *)toDate;
- (int)differenceInMonthsTo:(NSDate *)toDate;
- (BOOL)isSameDay:(NSDate*)anotherDate;
- (BOOL)isSameDateTime:(NSDate *)tmpDateTime;
+ (BOOL)isTodaysDateAfter:(NSDate *)tmpDate;
- (BOOL)isToday;
- (NSString *)dateDescription;
- (NSDate *) dateByAddingHours:(NSUInteger)hours minutes:(NSUInteger)minutes;
- (NSDate *)dateByAddingDays:(NSUInteger)days;
- (NSDate *) dateByAddingWeeks:(NSUInteger)weeks;
- (NSDate *) dateByAddingMonths:(NSUInteger)months;
+ (NSDate *)dateWithDatePart:(NSDate *)aDate andTimePart:(NSDate *)aTime;
+ (NSString *)dateInformationDescriptionWithInformation:(TKDateInformation)info;
+ (NSInteger)secondsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSInteger)minutesBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSInteger)hoursBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSInteger)weeksBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSInteger)monthsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSDate *)combineDate:(NSDate *)date withTime:(NSDate *)time;
+ (NSDate*)returnDateByNumberOfWeeks:(int)tmpNumWeeks 
                              toDate:(NSDate *)tmpToDate;
+ (NSDate*)returnDateByNumberOfDays:(int)tmpNumDays 
                      numberOfWeeks:(int)tmpNumWeeks
                     numberOfMonths:(int)tmpNumMonths 
                      numberOfYears:(int)tmpNumYears 
                             toDate:(NSDate *)tmpToDate;
+ (NSDate*)returnDateByNumberOfSeconds:(int)tmpNumSeconds 
                        numberOMinutes:(int)tmpNumMins 
                         numberOfHours:(int)tmpNumHours 
                          numberOfDays:(int)tmpNumDays 
                         numberOfWeeks:(int)tmpNumWeeks
                        numberOfMonths:(int)tmpNumMonths 
                         numberOfYears:(int)tmpNumYears 
                                toDate:(NSDate *)tmpToDate;
- (NSString *)returnDateWithFormat:(NSString *)_format;
- (NSString *)returnSimpleDate;
- (NSString *)returnSimpleDateTime;
- (NSString *)returnDate;
- (NSString *)returnTime;
- (NSString *)returnDateAndTime;
- (NSString *)returnDateAndTimeWithYear;
- (NSString *)returnEnergyDateAndTime;
- (NSDate *)floorTimeComponentOfDate;
- (NSDate *)ceilTimeComponentOfDate;
+ (NSDateComponents *)returnDifference:(NSDate *)startDate fromDate:(NSDate *)toThisDate components:(NSUInteger)_components;

@end
