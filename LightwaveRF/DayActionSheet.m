//
//  DayActionSheet.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 02/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "DayActionSheet.h"
#import "ButtonsHelper.h"

@interface DayActionSheet ()
@property (nonatomic, strong) NSMutableSet *selectedDays;
@property (nonatomic, strong) UIButton *mondayButton;
@property (nonatomic, strong) UIButton *tuesdayButton;
@property (nonatomic, strong) UIButton *wednesdayButton;
@property (nonatomic, strong) UIButton *thursdayButton;
@property (nonatomic, strong) UIButton *fridayButton;
@property (nonatomic, strong) UIButton *saturdayButton;
@property (nonatomic, strong) UIButton *sundayButton;
@end

@implementation DayActionSheet

@synthesize selectedDays;
@synthesize mondayButton, tuesdayButton, wednesdayButton, thursdayButton, fridayButton, saturdayButton, sundayButton;

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
       selectedDays:(NSString *)tmpSelectedDays {
    
    [self setSelectedDaysFromString:tmpSelectedDays];
    return [self initWithPoint:point
                  buttonTitles:tmpButtonTitles
                   cancelTitle:tmpCancelTitle
                    dataSource:tmpDataSource
                      delegate:tmpDelegate
                showPickerType:kCustomActionSheetDayPicker];
}

- (void)setSelectedDaysFromString:(NSString *)_str {
    self.selectedDays = [[NSMutableSet alloc] init];
    
    for(int i=kDayActionSheetMonday; i<=[_str length]; i++){
        if ([_str characterAtIndex:i-1] != 'x') {
            [self.selectedDays addObject:[NSNumber numberWithInt:i]];
            
        }
    }
}

- (void)dealloc  {
    //DebugLog(@"DEALLOC");
}

- (void)setupUI {
    [self setBackgroundColor:[self getColourForType:kCustomActionSheetBackgroundColour]];
    
    // Top Highlight
    UIView *_topHighlightView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, 0.0f)];
    [_topHighlightView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_topHighlightView];
    
    // Top Header
    self.heightOffset = 0.0f;
    UIView *_topHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.heightOffset, self.frame.size.width, 180.0f)];
    [_topHeaderView setTag:-1];
    [_topHeaderView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_topHeaderView];
    self.heightOffset += _topHeaderView.frame.size.height;
    
    // Add Day Buttons
    [self addDayButtonsToView:_topHeaderView];
    
    // Select any preselected
    for (NSNumber *_num in self.selectedDays) {
        UIButton *_button = [self getButtonFromView:_topHeaderView tag:[_num intValue]];
        if (_button)
            [_button setSelected:YES];
    }
    
    UIButton *_button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"weekdays", @"Weekdays")
                                                    type:kButtonGreenBacking
                                                     tag:-1
                                                   width:130.0f
                                                  height:35.0f
                                                  target:self
                                                selector:@selector(weekdaysPressed:)];
    [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0f]];
    [_button alignToBottomOfView:self.thursdayButton padding:10.0f matchHorizontal:YES];
    [_topHeaderView addSubview:_button];
    
    UIButton *_weekendButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"weekend", @"Weekend")
                                                           type:kButtonGreenBacking
                                                            tag:-1
                                                          width:130.0f
                                                         height:35.0f
                                                         target:self
                                                       selector:@selector(weekendPressed:)];
    [_weekendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_weekendButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:11.0f]];
    [_weekendButton alignToRightOfView:_button padding:18.0f matchVertical:YES];
    [_topHeaderView addSubview:_weekendButton];
    
    // Set Highlight Height
    [_topHighlightView setFrameHeight:self.heightOffset + 1.0f];
    
    // Apply Highlight
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _topHighlightView.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[self getColourForType:kCustomActionSheetGradientStart] CGColor],
                       (id)[[self getColourForType:kCustomActionSheetGradientEnd] CGColor],
                       nil];
    [_topHighlightView.layer insertSublayer:gradient atIndex:0];
    
    // Create Buttons
    NSUInteger tmpTag = 0;
    for (NSString *tmpTitle in self.buttonTitlesArray) {
        [self addButtonType:kCustomActionSheetButton title:tmpTitle tag:tmpTag];
        tmpTag += 1;
    }
    
    // Create Cancel Button
    self.heightOffset += lwActionSheetButtonCancelPadding;
    [self addButtonType:kCustomActionSheetCancelButton title:self.cancelTitle tag:tmpTag];
    
    self.heightOffset += lwActionSheetBottomPadding;
    [self setFrameHeight:self.heightOffset];
}

- (void)addDayButtonsToView:(UIView *)_view {
    self.mondayButton = [self addRadioButtonWithText:NSLocalizedString(@"dayFull1", @"Monday")
                                                 tag:kDayActionSheetMonday
                                            selected:NO
                                              toView:_view
                                            selector:@selector(dayButtonPressed:)];
    [self.mondayButton setFrameX:20.0f];
    [self.mondayButton setFrameY:10.0f];
    
    self.tuesdayButton = [self addRadioButtonWithText:NSLocalizedString(@"dayFull2", @"Tuesday")
                                                  tag:kDayActionSheetTuesday
                                             selected:NO
                                               toView:_view
                                             selector:@selector(dayButtonPressed:)];
    [self.tuesdayButton alignToBottomOfView:self.mondayButton padding:5.0f matchHorizontal:YES];
    
    self.wednesdayButton = [self addRadioButtonWithText:NSLocalizedString(@"dayFull3", @"Wednesday")
                                                  tag:kDayActionSheetWednesday
                                             selected:NO
                                               toView:_view
                                             selector:@selector(dayButtonPressed:)];
    [self.wednesdayButton alignToBottomOfView:self.tuesdayButton padding:5.0f matchHorizontal:YES];
    
    self.thursdayButton = [self addRadioButtonWithText:NSLocalizedString(@"dayFull4", @"Thursday")
                                                    tag:kDayActionSheetThursday
                                               selected:NO
                                                 toView:_view
                                               selector:@selector(dayButtonPressed:)];
    [self.thursdayButton alignToBottomOfView:self.wednesdayButton padding:5.0f matchHorizontal:YES];
    
    self.fridayButton = [self addRadioButtonWithText:NSLocalizedString(@"dayFull5", @"Friday")
                                                   tag:kDayActionSheetFriday
                                              selected:NO
                                                toView:_view
                                              selector:@selector(dayButtonPressed:)];
    [self.fridayButton alignToRightOfView:self.mondayButton padding:10.0f matchVertical:YES];
    
    self.saturdayButton = [self addRadioButtonWithText:NSLocalizedString(@"dayFull6", @"Saturday")
                                                   tag:kDayActionSheetSaturday
                                              selected:NO
                                                toView:_view
                                              selector:@selector(dayButtonPressed:)];
    [self.saturdayButton alignToBottomOfView:self.fridayButton padding:5.0f matchHorizontal:YES];
    
    self.sundayButton = [self addRadioButtonWithText:NSLocalizedString(@"dayFull7", @"Sunday")
                                                   tag:kDayActionSheetSunday
                                              selected:NO
                                                toView:_view
                                              selector:@selector(dayButtonPressed:)];
    [self.sundayButton alignToBottomOfView:self.saturdayButton padding:5.0f matchHorizontal:YES];
}

- (UIButton *)addRadioButtonWithText:(NSString *)_text
                            tag:(NSUInteger)_bTag
                       selected:(BOOL)_selected
                         toView:(UIView *)_view
                       selector:(SEL)_selector {
    
    UIButton *_button = [ButtonsHelper getButtonWithText:_text
                                                    type:kButtonRadio
                                                     tag:_bTag
                                                   width:130.0f
                                                  height:26.0f
                                                  target:self
                                                selector:_selector];
    if ([AppStyleHelper isMegaMan]) {
        [_button setBackgroundImage:nil forState:UIControlStateNormal];
        [_button setBackgroundImage:nil forState:UIControlStateHighlighted];
        [_button setBackgroundImage:nil forState:UIControlStateSelected];
        
        [_button setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateLo] forState:UIControlStateNormal];
        [_button setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateHighlighted];
        [_button setImage:[ButtonsHelper getImageForType:kButtonRadio state:kButtonStateHi] forState:UIControlStateSelected];
    }
    
    [_button setSelected:_selected];
    [_view addSubview:_button];
    return _button;
}

- (UIButton *)getButtonFromView:(UIView *)_searchView tag:(NSUInteger)_bTag {
    return (UIButton *)[_searchView viewWithTag:_bTag];
}

- (void)setSelected:(UIButton *)_button {
    if (_button.selected)
        [self.selectedDays addObject:[NSNumber numberWithInt:_button.tag]];
    else
        [self.selectedDays removeObject:[NSNumber numberWithInt:_button.tag]];
}

#pragma mark - Actions

- (IBAction)dayButtonPressed:(id)sender {    
    UIButton *_button = (UIButton *)sender;
    [_button setSelected:!_button.selected];
    [self setSelected:_button];
}

- (IBAction)weekdaysPressed:(id)sender {
    [self.mondayButton setSelected:YES];
    [self.tuesdayButton setSelected:YES];
    [self.wednesdayButton setSelected:YES];
    [self.thursdayButton setSelected:YES];
    [self.fridayButton setSelected:YES];
    [self.saturdayButton setSelected:NO];
    [self.sundayButton setSelected:NO];
    
    [self updateSelectedButtons];
}

- (IBAction)weekendPressed:(id)sender {
    [self.mondayButton setSelected:NO];
    [self.tuesdayButton setSelected:NO];
    [self.wednesdayButton setSelected:NO];
    [self.thursdayButton setSelected:NO];
    [self.fridayButton setSelected:NO];
    [self.saturdayButton setSelected:YES];
    [self.sundayButton setSelected:YES];
    
    [self updateSelectedButtons];
}

- (void)updateSelectedButtons {
    [self setSelected:self.mondayButton];
    [self setSelected:self.tuesdayButton];
    [self setSelected:self.wednesdayButton];
    [self setSelected:self.thursdayButton];
    [self setSelected:self.fridayButton];
    [self setSelected:self.saturdayButton];
    [self setSelected:self.sundayButton];
}

- (NSString *)getSelectedDays {
    return [NSString stringWithFormat:@"%@%@%@%@%@%@%@",
            self.mondayButton.selected ? @"m" : @"x",
            self.tuesdayButton.selected ? @"t" : @"x",
            self.wednesdayButton.selected ? @"w" : @"x",
            self.thursdayButton.selected ? @"t" : @"x",
            self.fridayButton.selected ? @"f" : @"x",
            self.saturdayButton.selected ? @"s" : @"x",
            self.sundayButton.selected ? @"s" : @"x"];
}

// Override super
- (IBAction)buttonPressed:(id)sender {
    if (self.showPickerType == kCustomActionSheetDayPicker) {
        if ([self.delegate respondsToSelector:@selector(actionSheetButtonPressed:index:selectedString:)])
            [self.delegate actionSheetButtonPressed:self
                                              index:[sender tag]
                                     selectedString:[self getSelectedDays]];
    }
}

@end
