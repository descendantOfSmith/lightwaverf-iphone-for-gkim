//
//  CustomActionSheet.m
//  ActionSheetPicker
//
//  Created by Dan Hillman on 12/01/2013.
//
//

#import "CustomActionSheet.h"

@implementation CustomActionSheet

@synthesize customDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc {
    self.customDelegate = nil;
    
    [super dealloc];
}

// For detecting taps outside of the alert view
- (void)tapOut:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self];
    if (p.y < 0) { // They tapped outside
        //NSLog(@"Dismiss...");
        
        [self dismissWithClickedButtonIndex:0 animated:YES];
        
        [self triggerTapCallback];
    }
}

- (void)triggerTapCallback {
    if ([self.customDelegate respondsToSelector:@selector(tappedToDismissActionSheet:)])
        [self.customDelegate tappedToDismissActionSheet:self];
}

- (void)addTapRecogniserToDismissActionSheet {
    //NSLog(@"Adding tap recogniser...");
    // Capture taps outside the bounds of this alert view
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOut:)];
    tap.cancelsTouchesInView = NO; // So that legit taps on the table bubble up to the tableview
    [self.superview addGestureRecognizer:tap];
    [tap release];
}

- (void)showFromTabBar:(UITabBar *)view {
    [super showFromTabBar:view];
    [self addTapRecogniserToDismissActionSheet];
}

- (void)showInView:(UIView *)view {
    [super showInView:view];
    [self addTapRecogniserToDismissActionSheet];
}

@end
