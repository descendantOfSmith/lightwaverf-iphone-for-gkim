//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"

@protocol EventActionDelayCellDelegate;

@interface EventActionCell : UITableViewCell {

}

@property (nonatomic, strong) UIView *containingView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subTitleLabel;
@property (nonatomic, strong) UIImageView *iconImageView;

// Delay
@property (nonatomic, strong) UIImageView *delayImageView;
@property (nonatomic, strong) UILabel *delayTitleLabel;
@property (nonatomic, strong) UILabel *delayDetailLabel;
@property (nonatomic, strong) UILabel *delayEditModeLabel;
@property (nonatomic, strong) UIButton *editDelayEditModeButton;

+ (float)getCellHeight;
- (void)positionViewsIsEditing:(BOOL)tmpIsEditing;

- (void)setupCellWithDelay:(NSString *)_delay;

- (void)setupCellWithTitle:(NSString*)tmpTitle
                  subtitle:(NSString *)tmpSubtitle
                detailText:(NSString *)tmpDetailText
                    device:(Device *)_device
                 indexPath:(NSIndexPath *)_indexPath
                  delegate:(id<EventActionDelayCellDelegate>)_delegate;

- (IBAction)buttonPressed:(id)sender;

@end

@protocol EventActionDelayCellDelegate <NSObject>

@optional

- (void)setDelayPressed:(EventActionCell *)_eventActionCell
              indexPath:(NSIndexPath *)_indexPath;

@end
