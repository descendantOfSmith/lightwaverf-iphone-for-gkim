//
//  ChooseProductSliderCell.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 19/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ChooseProductOnOffCell.h"
#import "ANPopoverSlider.h"

@interface ChooseProductSliderCell : ChooseProductOnOffCell <ANPopoverSliderDataSource> {
    
}

- (void)setupCellWithTitle:(NSString*)tmpTitle
                    device:(Device *)_device
                  selected:(BOOL)_selected
                      isOn:(BOOL)_isOn
                 indexPath:(NSIndexPath *)_indexPath
                  delegate:(id<ChooseProductOnOffCellDelegate>)_delegate
                percentage:(float)_percentage;

@end
