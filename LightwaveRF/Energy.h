//
//  Energy.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 17/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Energy : NSObject {
    
}

@property (nonatomic, assign) float eKWHPercentage;
@property (nonatomic, assign) float eKWHTodayPercentage;
@property (nonatomic, assign) float eKWHYesterdayPercentage;

@property (nonatomic, assign) float eCPHPercentage;
@property (nonatomic, assign) float eCFYPercentage;
@property (nonatomic, assign) float eCFTPercentage;

@property (nonatomic, copy) NSString *eKWHStr;
@property (nonatomic, copy) NSString *eKWHTodayStr;
@property (nonatomic, copy) NSString *eKWHYesterdayStr;

@property (nonatomic, copy) NSString *eCPHStr;
@property (nonatomic, copy) NSString *eCFYStr;
@property (nonatomic, copy) NSString *eCFTStr;

@property (nonatomic, strong) NSDate *lastUpdate;

@end
