//
//  ChooseProductOnOff.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 19/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ChooseProductOnOffCell.h"
#import "ButtonsHelper.h"

@interface ChooseProductOnOffCell ()

@end

@implementation ChooseProductOnOffCell

@synthesize onButton, offButton, indexPath, delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
    }
    return self;
}

- (void)dealloc {
    
}

+ (float)getCellHeight {
    return 44.0f;
}

- (void)setupCellWithTitle:(NSString*)tmpTitle
                    device:(Device *)_device
                  selected:(BOOL)_selected
                      isOn:(BOOL)_isOn
                 indexPath:(NSIndexPath *)_indexPath
                  delegate:(id<ChooseProductOnOffCellDelegate>)_delegate {
    
    self.delegate = _delegate;
    self.indexPath = _indexPath;
    
    // Setup Initial Cell
    [self setupCellWithTitle:tmpTitle device:_device selected:_selected];

    /*************************************
     *  On Button
     *************************************/
    if (!self.onButton) {
        self.onButton = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"on", @"On") uppercaseString]
                                                    type:kButtonGreenPSwitch
                                                     tag:kBtnOn
                                                   width:45.0f
                                                  height:38.0f
                                                  target:self
                                                selector:@selector(buttonPressed:)];
        [self.onButton setSelected:_isOn];
        [self.containingView addSubview:self.onButton];
    } else
        [self.onButton setSelected:_isOn];
    
    /*************************************
     *  Off Button
     *************************************/
    if (!self.offButton) {
        self.offButton = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"off", @"Off") uppercaseString]
                                                     type:kButtonGreenOSwitch
                                                      tag:kBtnOff
                                                    width:45.0f
                                                   height:38.0f
                                                   target:self
                                                 selector:@selector(buttonPressed:)];
        [self.offButton setSelected:!_isOn];
        [self.containingView addSubview:self.offButton];
    } else
        [self.offButton setSelected:!_isOn];
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    [super positionViewsIsEditing:tmpIsEditing];
    
    // Title Label
    [self.titleLabel setFrameWidth:140.0f];
    
    if ([AppStyleHelper isNexa]) {
        // Off Button
        [self.offButton setFrameX:(self.containingView.frame.size.width - self.offButton.frame.size.width) - 5.0f];
        [self.offButton setCenterY:self.containingView.center.y];
        
        // On Button
        [self.onButton alignToLeftOfView:self.offButton padding:5.0f];
        [self.onButton setCenterY:self.containingView.center.y];
    } else {
        // On Button
        [self.onButton setFrameX:(self.containingView.frame.size.width - self.onButton.frame.size.width) - 5.0f];
        [self.onButton setCenterY:self.containingView.center.y];
        
        // Off Button
        [self.offButton alignToLeftOfView:self.onButton padding:5.0f];
        [self.offButton setCenterY:self.containingView.center.y];
    }
}

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnOn:
        case kBtnOff:
        {
            [self.onButton setSelected:!self.onButton.selected];
            [self.offButton setSelected:!self.offButton.selected];
            
            if ([self.delegate respondsToSelector:@selector(chooseProductSwitch:indexPath:isOn:)])
                [self.delegate chooseProductSwitch:self indexPath:self.indexPath isOn:self.onButton.selected];
        }
            break;
            
        default:
            break;
    }
}

@end
