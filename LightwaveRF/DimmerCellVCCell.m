//
//
//  LightwaveRF
//
//  Created by Dan Hillman on 10/05/2012.
//  Copyright 2010 Sync Studios. All rights reserved.
//

#import "DimmerCellVCCell.h"
#import "WFLHelper.h"
#import "Toast+UIView.h"
#import "AppDelegate.h"
#import "ButtonsHelper.h"
#import "Device.h"

@interface DimmerCellVCCell ()
@property (strong, nonatomic) UIImageView *onActive_img;
@property (strong, nonatomic) UIImageView *active_img;
@property (strong, nonatomic) UIButton *onButton;
@property (strong, nonatomic) UIButton *offButton;
@end

@implementation DimmerCellVCCell

@synthesize icon_img;
@synthesize name_lbl;
@synthesize device;
@synthesize dimmer_sdr;
@synthesize indexPath;
@synthesize active_img;
@synthesize onActive_img;
@synthesize dimmerbg_img;
@synthesize fadeActive_tmr;
@synthesize onFadeActive_tmr;
@synthesize highlight_img;
@synthesize zoneID;
@synthesize onButton, offButton;

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    onActive_img.hidden = YES;
    active_img.hidden = YES;
    
    [icon_img setContentMode:UIViewContentModeScaleAspectFit];
    
    self.dimmer_sdr.minimumValue = DIMMER_MIN;
    self.dimmer_sdr.maximumValue = DIMMER_MAX;
    
    if ([AppStyleHelper isLightWaveRF] || [AppStyleHelper isFSLDemo] || [AppStyleHelper isCOCO])
        [self.dimmer_sdr setMinimumTrackTintColor:[UIColor colorWithDivisionOfRed:38.0f green:116.0f blue:32.0f alpha:1.0f]];
    
    else if ([AppStyleHelper isMegaMan]) {
        [self.dimmer_sdr setMinimumTrackTintColor:[UIColor colorWithDivisionOfRed:137 green:197 blue:39 alpha:1.0f]];
        [self.dimmerbg_img setHidden:YES];
        [self.dimmerUpbg_img setHidden:YES];
    
    } else
        [self.dimmer_sdr setMinimumTrackTintColor:[UIColor colorWithDivisionOfRed:68 green:32 blue:105 alpha:1.0f]];
    
    [self.dimmer_sdr setDataSource:self];
}

- (void)dealloc {
    if (fadeActive_tmr != nil){
        [fadeActive_tmr invalidate];
    }
    
    if (onFadeActive_tmr != nil){
        [onFadeActive_tmr invalidate];
    }
}

+ (float)getCellHeight {
    return 92.0f;
}

- (void)setIndexPathWithRow:(NSInteger)row andSection:(NSInteger)section{
    self.indexPath = [NSIndexPath indexPathForRow:row inSection:section];
}

- (void)setupCellWithDevice:(Device *)_device zoneID:(int)_zoneID {
    self.device = _device;
    self.zoneID = _zoneID;
    
    [self setupOnButton];
    
    [self setupOffButton];
    
    [self.contentView bringSubviewToFront:self.dimmer_sdr];
    
    if (self.device!=nil)
        [self.name_lbl setText:self.device.name];
    
    if (self.device.lastOnState != 0)
        //[dimmer_sdr setValue:(float)self.device.assumedState / 32 animated:NO];
        [self.dimmer_sdr setValue:(float)self.device.assumedState animated:NO];
    else {
        //[dimmer_sdr setValue:0.5f animated:NO];
        [self.dimmer_sdr setValue:DIMMER_MAX / 2 animated:NO];
    }
    
    /*************************************
     *  Position Views
     *************************************/
    [self positionViewsIsEditing:NO];
}

- (void)setupOnButton {
    if (!self.onButton) {
        self.onButton = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"on", @"ON") uppercaseString]
                                              type:kButtonGreenPSwitch
                                               tag:kBtnOn
                                             width:40
                                            height:38
                                            target:self
                                          selector:@selector(onPressed:)];
        [self.contentView addSubview:self.onButton];
        
        self.onActive_img = [ViewBuilderHelper getImageViewWithFilename:@"active_blue.png"
                                                                    atX:0
                                                                    atY:0];
        [self.onActive_img setFrameWidth:27];
        [self.onActive_img setFrameHeight:8];
        [self.contentView addSubview:self.onActive_img];
        self.onActive_img.hidden = YES;
    }
    
    if ([AppStyleHelper isNexa]) {
        [self.onButton setFrameX:225];
        [self.onButton setFrameY:5];
        
        [self.onActive_img setFrameX:232];
        [self.onActive_img setFrameY:32];
    } else {
        [self.onButton setFrameX:272];
        [self.onButton setFrameY:5];
        
        [self.onActive_img setFrameX:279];
        [self.onActive_img setFrameY:32];
    }
}

- (void)setupOffButton {
    if (!self.offButton) {
        self.offButton = [ButtonsHelper getButtonWithText:[NSLocalizedString(@"off", @"OFF") uppercaseString]
                                              type:kButtonGreenOSwitch
                                               tag:kBtnOff
                                             width:40
                                            height:38
                                            target:self
                                          selector:@selector(offPressed:)];
        [self.contentView addSubview:self.offButton];
        
        self.active_img = [ViewBuilderHelper getImageViewWithFilename:@"active.png"
                                                                  atX:0
                                                                  atY:0];
        [self.active_img setFrameWidth:27];
        [self.active_img setFrameHeight:8];
        [self.contentView addSubview:self.active_img];
        self.active_img.hidden = YES;
    }
    
    if ([AppStyleHelper isNexa]) {
        [self.offButton setFrameX:272];
        [self.offButton setFrameY:5];
        
        [self.active_img setFrameX:279];
        [self.active_img setFrameY:32];
    } else {
        [self.offButton setFrameX:225];
        [self.offButton setFrameY:5];
        
        [self.active_img setFrameX:232];
        [self.active_img setFrameY:32];
    }
}

- (void)positionViewsIsEditing:(BOOL)tmpIsEditing {
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self positionViewsIsEditing:self.isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

- (float)getPercentage {
    return (self.dimmer_sdr.value / DIMMER_MAX) * 100;
}

- (void)alertUserDimmerPercentage:(NSString *)_percentage {
    [kApplicationDelegate.window makeToast:_percentage
                                  duration:3.0f
                                  position:@"center"];
}
- (IBAction)dimmerEditStart:(id)sender {
    //[self roundSettingToClosestValue];
    
    //[self showPopoverPercentage];
}

- (IBAction)dimmerDrag:(id)sender {
    //[self roundSettingToClosestValue];
    
    //DebugLog(@"Dimmer drag %4.2f", self.dimmer_sdr.value);
    //self.highlight_img.alpha = self.dimmer_sdr.value;
    self.highlight_img.alpha = [self getPercentage] / 100;
    
    // Slider Popover
    //[self showPopoverPercentage];
}

- (IBAction)dimmerEditEnd:(id)sender {
    //[self roundSettingToClosestValue];
    
    //[self showPopoverPercentage];
    //[self alertUserDimmerPercentage];
    
    //DebugLog(@"Dimmer end %4.2f", self.dimmer_sdr.value);
    //self.highlight_img.alpha = self.dimmer_sdr.value;
    self.highlight_img.alpha = [self getPercentage] / 100;
    
    // Set Percentage
    //[self.device setAssumedState:(int)(([self getPercentage] / 100) * 32)];
    [self.device setAssumedState:(int)self.dimmer_sdr.value];
    
    [[WFLHelper shared] sendToUDP:YES
                          command:[UDPService getDimCommandWithZoneID:self.zoneID device:device percentage:[self getPercentage]]
                              tag:kRequestDim];
}

#pragma mark - ANPopoverSlider Datasource

- (NSString *)getPopoverText:(ANPopoverSlider *)_slider {
    return [NSString stringWithFormat:@"%.0f%%", [self getPercentage]];
}

- (IBAction)onPressed:(id)sender {
    DebugLog(@"On pressed");
    
    if (self.device.lastOnState != 0) {
        //DebugLog(@"Turn On to Last State");
        
        //[dimmer_sdr setValue:(float)self.device.lastOnState / 32 animated:YES];
        //[self.device setAssumedState:self.device.lastOnState];
        
        [dimmer_sdr setValue:(float)self.device.lastOnState animated:YES];
        [self.device setAssumedState:self.device.lastOnState];
        
        [[WFLHelper shared] sendToUDP:YES
                              command:[UDPService getOnOffCommandWithZoneID:self.zoneID device:device isOn:YES]
                                  tag:kRequestToggleOnOff];
    } else {
        //DebugLog(@"Turn On to FULL (no last state)");
        
        //[dimmer_sdr setValue:1.0f animated:YES];
        //[self.device setAssumedState:32];
        
        [dimmer_sdr setValue:DIMMER_MAX animated:YES];
        [self.device setAssumedState:DIMMER_MAX];
        
        [[WFLHelper shared] sendToUDP:YES
                              command:[UDPService getDimCommandWithZoneID:self.zoneID device:device percentage:[self getPercentage]]
                                  tag:kRequestDim];
    }
    
    [self alertUserDimmerPercentage:[NSString stringWithFormat:@"%.0f%%", [self getPercentage]]];
    
    onActive_img.hidden = NO;
    onActive_img.alpha = 1.0;
    if (onFadeActive_tmr == nil){
        onFadeActive_tmr = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(onFadeActive:) userInfo:nil repeats:YES  ];
    }
}

- (void)onFadeActive:(NSTimer *)timer{
    onActive_img.alpha -= 0.1;
    if (onActive_img.alpha<=0.0){
        [onFadeActive_tmr invalidate];
        onFadeActive_tmr = nil;
        onActive_img.hidden = YES;
    }
}

- (IBAction)offPressed:(id)sender {
    DebugLog(@"Off pressed");
    
    [dimmer_sdr setValue:0.0f animated:YES];
    [self.device setAssumedState:0];
    [[WFLHelper shared] sendToUDP:YES
                          command:[UDPService getOnOffCommandWithZoneID:self.zoneID device:device isOn:NO]
                              tag:kRequestToggleOnOff];
    
    [self alertUserDimmerPercentage:[NSString stringWithFormat:@"0%%"]];
    
    active_img.hidden = NO;
    active_img.alpha = 1.0;
    if (fadeActive_tmr == nil){
        fadeActive_tmr = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(fadeActive:) userInfo:nil repeats:YES  ];
    }
}

- (void)fadeActive:(NSTimer *)timer{
    active_img.alpha -= 0.1;
    if (active_img.alpha<=0.0){
        [fadeActive_tmr invalidate];
        fadeActive_tmr = nil;
        active_img.hidden = YES;
    }
}

@end
