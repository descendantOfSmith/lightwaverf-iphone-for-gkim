//
//  ScheduleEventVC.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 01/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "ScheduleEventVC.h"
#import "Home.h"
#import "LTimer.h"
#import "ButtonsHelper.h"
#import "NSDate+Utilities.h"
#import "WFLHelper.h"
#import "Toast+UIView.h"

@interface ScheduleEventVC ()
@property (nonatomic, strong) Event *event;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) LTimer *timer;
@property (nonatomic, strong) LTimer *undoChangesTimer;
@property (nonatomic, strong) LightwaveActionSheet *optionsActionSheet;
@property (nonatomic, strong) DateTimeActionSheet *startDateActionSheet;
@property (nonatomic, strong) DateTimeActionSheet *endDateActionSheet;
@property (nonatomic, strong) DawnDuskActionSheet *dawnDuskActionSheet;
@property (nonatomic, strong) DayActionSheet *dayActionSheet;
@property (nonatomic, strong) MonthActionSheet *monthActionSheet;
@end

#define kTableViewFont  ([UIFont fontWithName:@"Helvetica" size:12.0f])

@implementation ScheduleEventVC

@synthesize event, timer, undoChangesTimer;
@synthesize tableView, optionsActionSheet, startDateActionSheet, endDateActionSheet, dawnDuskActionSheet, dayActionSheet, monthActionSheet;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil event:(Event *)_event
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.event = _event;
        Home *home = [Home shared];
        LTimer *_timer = [home getTimerByName:self.event.timerName];
        
        if (_timer) {
            self.timer = _timer;
        
        } else {
            NSString *_command = [NSString stringWithFormat:@"!FiP\"%@\"=!FqP\"%@\"", [LTimer generateTimerName], self.event.name];
            self.timer = [[LTimer alloc] initWithCommand:_command active:NO];
        }
        
        [self setupUndoTimer:self.timer];
        
        //DebugLog(@"Name = %@", self.timer.name);
        //DebugLog(@"Start Date = %@", self.timer.start_date);
        //DebugLog(@"End Date = %@", self.timer.end_date);
        //DebugLog(@"Command = %@", [self.timer getCommand]);
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil eventName:(NSString *)_eventName timer:(LTimer *)_timer
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        Home *home = [Home shared];
        self.event = [home getEventByName:_eventName];
        self.timer = _timer;
        [self setupUndoTimer:self.timer];
    }
    return self;
}

- (void)setupUndoTimer:(LTimer *)_timer {
    self.undoChangesTimer = [[LTimer alloc] initWithCommand:[_timer getCommand]
                                                     active:_timer.active];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Creation

- (void)setupUI {
    [self setupNavBar];
    [self setupTableView];
    [self setupButtons];
    [self setupEditOptions];
    [self setupStartDateActionSheet];
    [self setupEndDateActionSheet];
    [self setupDuskDawnActionSheet];
    [self setupDayActionSheet];
    [self setupMonthActionSheet];
}

- (void)setupNavBar {
    self.navigationItem.title = NSLocalizedString(@"event_info", @"Event Info");
    [self setupLeftNavBarButton];
    [self setupRightNavBarButton];
}

- (void)setupLeftNavBarButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"cancel", @"Cancel")
                                                       type:kButtonArrowLeft
                                                        tag:kBtnBack
                                                      width:55
                                                     height:18
                                                     target:self
                                                   selector:@selector(buttonPressed:)];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        [backButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backButton;
        
    } else {
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                                          style:UIBarButtonItemStyleBordered
                                                                         target:self
                                                                         action:@selector(buttonPressed:)];
        [backBarButton setTag:kBtnBack];
        self.navigationItem.leftBarButtonItem = backBarButton;
    }
}

- (void)setupRightNavBarButton {
    if ([AppStyleHelper isMegaMan]) {
        UIButton *editButton = [ButtonsHelper getButtonWithText:NSLocalizedString(@"edit", nil)
                                                           type:kButtonArrowLeft
                                                            tag:kBtnEdit
                                                          width:40
                                                         height:18
                                                         target:self
                                                       selector:@selector(buttonPressed:)];
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithCustomView:editButton];
        [tmpTopRightBtn setTag:kBtnEdit];
        self.navigationItem.rightBarButtonItem = tmpTopRightBtn;
        
    } else {
        UIBarButtonItem *tmpTopRightBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit", nil)
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(buttonPressed:)];
        [tmpTopRightBtn setTag:kBtnEdit];
        self.navigationItem.rightBarButtonItem = tmpTopRightBtn;
    }
}

- (void)setupTableView {
    float padding = 5.0f;
    self.tableView = [ViewBuilderHelper getTableViewWithWidth:self.scrollView.frame.size.width - (padding * 2)
                                                       height:0.0f
                                                        style:UITableViewStyleGrouped
                                                       target:self];
    
    // iOS7
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self.tableView setFrameX:padding];
    [self.tableView setFrameY:self.heightOffset];
    [self.tableView setScrollEnabled:NO];
    [self.scrollView addSubview:self.tableView];
    self.heightOffset += self.tableView.frame.size.height;
    [self reloadData];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)updateTableViewBG {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if ([AppStyleHelper isMegaMan]) {
            [self.tableView setClipsToBounds:NO];
            
            UIView *view = [self.tableView viewWithTag:123456];
            
            if (!view) {
                view = [[UIView alloc] initWithFrame:self.tableView.bounds];
                [view setTag:123456];
                [view setBackgroundColor:[UIColor whiteColor]];
                view.layer.cornerRadius = 10.0f;
                [self.tableView insertSubview:view atIndex:0];
            }
            
            [view setFrame:self.tableView.bounds];
            float padding = 15.0f;
            [view setFrameY:view.frame.origin.y + padding];
            [view setFrameHeight:view.frame.size.height - (padding * 2)];
        }
    }
}

- (void)setupButtons {
    UIButton *_button = [ButtonsHelper getButtonWithText:NSLocalizedString(@"done", @"Done")
                                                    type:[AppStyleHelper isMegaMan] ? kButtonGreyBacking : kButtonGreenBacking
                                                     tag:kBtnDone
                                                   width:300.0f
                                                  height:[AppStyleHelper isMegaMan] ? 45.0f : 30.0f
                                                  target:self
                                                selector:@selector(buttonPressed:)];
    if (![AppStyleHelper isMegaMan])
        [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_button setCenterX:self.scrollView.center.x];
    [_button setFrameY:(self.scrollView.frame.size.height - _button.frame.size.height) - kNavBarHeight - 20.0f];
    [self.scrollView addSubview:_button];
    
    if (self.timer) {
        // Show Pause/Activate timer
        NSString *_buttonTitle = self.timer.active ? NSLocalizedString(@"pause", @"Pause") : NSLocalizedString(@"activate", @"Activate");
        UIButton *_pauseActivateButton = [ButtonsHelper getButtonWithText:_buttonTitle
                                                              type:[AppStyleHelper isMegaMan] ? kButtonGreyBacking : kButtonGreenBacking
                                                               tag:kBtnToggle
                                                             width:300.0f
                                                            height:[AppStyleHelper isMegaMan] ? 45.0f : 30.0f
                                                            target:self
                                                          selector:@selector(buttonPressed:)];
        [_pauseActivateButton setSelected:self.timer.active];
        
        if ([AppStyleHelper isMegaMan])
            [_pauseActivateButton setBackgroundImage:nil forState:UIControlStateSelected];
        else
            [_pauseActivateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_pauseActivateButton alignToTopOfView:_button padding:5.0f matchHorizontal:YES];
        [self.scrollView addSubview:_pauseActivateButton];
    }
}

- (void)setupEditOptions {
    self.optionsActionSheet = [[LightwaveActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                                buttonTitles:[NSArray arrayWithObjects:
                                                                              NSLocalizedString(@"set_start_time", @"Set Start Time"),
                                                                              NSLocalizedString(@"set_start_date", @"Set Start Date"),
                                                                              NSLocalizedString(@"set_end_date", @"Set End Date"),
                                                                              NSLocalizedString(@"set_days", @"Set Days"),
                                                                              NSLocalizedString(@"set_months", @"Set Months"),
                                                                              nil]
                                                                 cancelTitle:NSLocalizedString(@"close", @"Close")
                                                                  dataSource:self
                                                                    delegate:self];
    [self.optionsActionSheet setTag:kScheduleEventVCMainOptions];
    [self.view addSubview:self.optionsActionSheet];
}

- (void)setupStartDateActionSheet {
    self.startDateActionSheet = [[DateTimeActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                                  buttonTitles:[NSArray arrayWithObjects:
                                                                                NSLocalizedString(@"set_date", @"Set Date"),
                                                                                nil]
                                                                   cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                           dataSource:self
                                                             delegate:self
                                                     selectedDateMode:UIDatePickerModeDate];
    [self.startDateActionSheet setTag:kScheduleEventVCSetStartDate];
    [self.view addSubview:self.startDateActionSheet];
}

- (void)setupEndDateActionSheet {
    self.endDateActionSheet = [[DateTimeActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                         buttonTitles:[NSArray arrayWithObjects:
                                                                       NSLocalizedString(@"set_date", @"Set Date"),
                                                                       NSLocalizedString(@"until_forever", @"Until Forever"),
                                                                       nil]
                                                          cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                           dataSource:self
                                                             delegate:self
                                                     selectedDateMode:UIDatePickerModeDate];
    [self.endDateActionSheet setTag:kScheduleEventVCSetEndDate];
    [self.view addSubview:self.endDateActionSheet];
}

- (void)setupDuskDawnActionSheet {
    self.dawnDuskActionSheet = [[DawnDuskActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                             buttonTitles:[NSArray arrayWithObjects:
                                                                           NSLocalizedString(@"done", @"Done"),
                                                                           nil]
                                                              cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                               dataSource:self
                                                                 delegate:self
                                                             selectedType:kCustomActionSheetTimePickerTime];
    [self.dawnDuskActionSheet setTag:kScheduleEventVCSetDuskDawn];
    [self.view addSubview:self.dawnDuskActionSheet];
}

- (void)setupDayActionSheet {
    self.dayActionSheet = [[DayActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                   buttonTitles:[NSArray arrayWithObjects:
                                                                 NSLocalizedString(@"done", @"Done"),
                                                                 nil]
                                                    cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                     dataSource:self
                                                       delegate:self
                                                   selectedDays:self.timer.days];
    [self.dayActionSheet setTag:kScheduleEventVCSetDays];
    [self.view addSubview:self.dayActionSheet];
}

- (void)setupMonthActionSheet {
    self.monthActionSheet = [[MonthActionSheet alloc] initWithPoint:CGPointMake(0.0f, (SCREEN_HEIGHT() - kNavBarHeight) - STATUS_BAR_HEIGHT)
                                                   buttonTitles:[NSArray arrayWithObjects:
                                                                 NSLocalizedString(@"done", @"Done"),
                                                                 nil]
                                                    cancelTitle:NSLocalizedString(@"cancel", @"Cancel")
                                                     dataSource:self
                                                       delegate:self
                                                   selectedMonths:self.timer.months];
    [self.monthActionSheet setTag:kScheduleEventVCSetMonths];
    [self.view addSubview:self.monthActionSheet];
}

#pragma mark - Private

- (void)updateTableHeight {
    // Get the exact height required for the tableview to fill the scrollview.
    self.heightOffset -= self.tableView.frame.size.height;
    [self.tableView layoutIfNeeded];
    [self.tableView setFrameHeight:[self.tableView contentSize].height];
    self.heightOffset += self.tableView.frame.size.height;
    [self updateTableViewBG];
}

- (void)reloadData {
    [self.tableView reloadData];
    [self updateTableHeight];
}

- (NSString *)getStringForIndex:(NSUInteger)_index {
    switch (_index) {
            
        case kScheduleEventCellTime:
            return [self.timer getTimeString];
            break;
            
        case kScheduleEventCellDates:
            return [self.timer getDatesString];
            break;
            
        case kScheduleEventCellDays:
        {
            NSString *_dayString = [self.timer getDayString];
            return _dayString;
            
//            if ([_dayString isEqualToString:@""] || [_dayString isEqualToString:@" "]) {
//                
//                return NSLocalizedString(@"no_days", nil);
//                
//            } else {
//                
//                return _dayString;
//                
//            }
        }
            break;
            
        case kScheduleEventCellMonths:
        {
            NSString *_monthString = [self.timer getMonthsString];
            return _monthString;
            
//            if ([_monthString isEqualToString:@""] || [_monthString isEqualToString:@" "]) {
//                
//                return NSLocalizedString(@"no_months", nil);
//                
//            } else {
//                
//                return _monthString;
//                
//            }
        }
            break;
            
        default:
            return nil;
            break;
    } 
}

#pragma mark - CustomActionSheet Datasource

- (UIView *)presentedFromView:(LightwaveActionSheet *)tmpCustomActionSheet {
    return self.view;
}

- (UIColor *)actionSheetColourTypes:(LightwaveActionSheet *)tmpCustomActionSheet type:(CustomActionSheetStyleColours)_type {
    if (_type == kCustomActionSheetBackgroundColour)
        return [UIColor colorWithDivisionOfRed:96.0f green:101.0f blue:111.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientStart)
        return [UIColor colorWithDivisionOfRed:166.0f green:169.0f blue:175.0f alpha:1.0f];
    else if (_type == kCustomActionSheetGradientEnd)
        return [UIColor colorWithDivisionOfRed:123.0f green:128.0f blue:136.0f alpha:1.0f];
    else
        return [UIColor clearColor];
}

- (UIImage *)actionSheetButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreenBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (UIImage *)actionSheetCancelButtonImage:(LightwaveActionSheet *)tmpCustomActionSheet state:(CustomActionButtonStates)_state {
    return [ButtonsHelper getImageForType:kButtonGreyBacking
                                    state:_state == kCustomActionButtonStatesLo ? kButtonStateLo : kButtonStateHi];
}

- (float)actionSheetButtonHeight:(LightwaveActionSheet *)tmpCustomActionSheet {
    return [AppStyleHelper isMegaMan] ? 40.0f : 35.0f;
}

#pragma mark - CustomActionSheet Delegate

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet index:(NSUInteger)tmpIndex {
    if ([tmpCustomActionSheet tag] == kScheduleEventVCMainOptions) {
        /*****************************************
         * MENU OPTIONS
         *****************************************/
        
        float _closeTime = 0.0f;//0.4f;
        
        switch (tmpIndex) {
                
            case kScheduleEventVCOptionsSetStartTime:
            {
                //[self.optionsActionSheet toggleActionSheetWithDelay:0.0f];
                
                // Delay
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, _closeTime * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                    
                    [self.dawnDuskActionSheet.timePickerView setTag:tmpIndex];
                    [self.dawnDuskActionSheet toggleActionSheetWithDelay:0.0f];
                    
                    NSArray *_tokens = [self.timer.time componentsSeparatedByString:@":"];
                    if (_tokens.count == 2) {
                    
                        NSUInteger indexZero = [[_tokens objectAtIndex:0] intValue];
                        NSUInteger indexOne = [[_tokens objectAtIndex:1] intValue];

                        if ([self.dawnDuskActionSheet.timePickerView numberOfComponents] == 2) {
                            NSUInteger maxZero = [self.dawnDuskActionSheet.timePickerView numberOfRowsInComponent:0];
                            NSUInteger maxOne = [self.dawnDuskActionSheet.timePickerView numberOfRowsInComponent:1];
    
                            if (indexZero < maxZero && indexOne < maxOne) {
                                [self.dawnDuskActionSheet.timePickerView selectRow:indexZero inComponent:0 animated:NO];
                                [self.dawnDuskActionSheet.timePickerView selectRow:indexOne inComponent:1 animated:NO];
                            }
                        }
  
                    }
                    
                });
            }
                break;
                
            case kScheduleEventVCOptionsSetStartDate:
            {
                //[self.optionsActionSheet toggleActionSheetWithDelay:0.0f];
                
                // Delay
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, _closeTime * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                    
                    // Preselect existing date if it exists
                    NSString *_dateStr = [self.timer getStartDateString];
                    
                    // Convert string date to nsdate
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"dd/MM/yy"];
                    NSDate *_date = [dateFormat dateFromString:_dateStr];
                    UIDatePicker *_datePicker = (UIDatePicker *)self.startDateActionSheet.datePickerView;
                    [_datePicker setDate:self.timer ? (_date ? _date : [NSDate date]) : [NSDate date] animated:YES];
                    
                    [self.startDateActionSheet.datePickerView setTag:tmpIndex];
                    [self.startDateActionSheet toggleActionSheetWithDelay:0.0f];
                });
            }
                break;
                
            case kScheduleEventVCOptionsSetEndDate:
            {
                //[self.optionsActionSheet toggleActionSheetWithDelay:0.0f];
                
                // Delay
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, _closeTime * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                    
                    // Preselect existing date if it exists
                    NSString *_dateStr = [self.timer getEndDateString];

                    // Convert string date to nsdate
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"dd/MM/yy"];
                    NSDate *_date = [dateFormat dateFromString:_dateStr];
                    UIDatePicker *_datePicker = (UIDatePicker *)self.endDateActionSheet.datePickerView;
                    [_datePicker setDate:self.timer ? (_date ? _date : [NSDate date]) : [NSDate date] animated:YES];
                    
                    [self.endDateActionSheet.datePickerView setTag:tmpIndex];
                    [self.endDateActionSheet toggleActionSheetWithDelay:0.0f];
                });
            }
                break;
                
            case kScheduleEventVCOptionsSetDays:
            {
                //[self.optionsActionSheet toggleActionSheetWithDelay:0.0f];
                
                // Delay
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, _closeTime * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                    [self.dayActionSheet toggleActionSheetWithDelay:0.0f];
                });
            }
                break;
                
            case kScheduleEventVCOptionsSetMonths:
            {
                //[self.optionsActionSheet toggleActionSheetWithDelay:0.0f];
                
                // Delay
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, _closeTime * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                    [self.monthActionSheet toggleActionSheetWithDelay:0.0f];
                });
            }
                break;
                
            case kScheduleEventVCOptionsClose:
            {
                [self.optionsActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;

            default:
                break;
        }
        
    }
}

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet
                           index:(NSUInteger)tmpIndex
                  selectedString:(NSString *)_selectedString {
    
    if ([tmpCustomActionSheet tag] == kScheduleEventVCSetDays) {
        /*****************************************
         * SET DAYS
         *****************************************/
        switch (tmpIndex) {
                
            case kScheduleEventVCDaysDone:
            {
                self.timer.days = _selectedString;
                [self.tableView reloadData];
                [self.dayActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            case kScheduleEventVCDaysCancel:
            {
                [self.dayActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
    } else if ([tmpCustomActionSheet tag] == kScheduleEventVCSetMonths) {
        /*****************************************
         * SET MONTHS
         *****************************************/
        switch (tmpIndex) {
                
            case kScheduleEventVCMonthsDone:
            {
                self.timer.months = _selectedString;
                [self.tableView reloadData];
                [self.monthActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            case kScheduleEventVCMonthsCancel:
            {
                [self.monthActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
    }
}

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet index:(NSUInteger)tmpIndex pickerView:(UIPickerView *)tmpPickerView {
    if ([tmpCustomActionSheet tag] == kScheduleEventVCSetStartDate) {
        /*****************************************
         * SET START DATE
         *****************************************/
        switch (tmpIndex) {
                
            case kScheduleEventVCStartDateSet:
            {
                UIDatePicker *_datePicker = (UIDatePicker *)tmpPickerView;
                [self.timer setStart_date:[_datePicker.date returnDateWithFormat:@"dd/MM/yy"]];
                [self.tableView reloadData];
                [self.startDateActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            case kScheduleEventVCStartDateCancel:
            {
                [self.startDateActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
    } else if ([tmpCustomActionSheet tag] == kScheduleEventVCSetEndDate) {
        /*****************************************
         * SET END DATE
         *****************************************/
        switch (tmpIndex) {
                
            case kScheduleEventVCEndDateSet:
            {
                UIDatePicker *_datePicker = (UIDatePicker *)tmpPickerView;
                
                // Limit the end year
                NSUInteger _maxYear = 2050;
                if ([[_datePicker.date yearNumber] intValue] >= _maxYear) {
                    [self showAlert:[NSString stringWithFormat:NSLocalizedString(@"end_date_alert", nil),
                                     _maxYear
                                     ]];
                } else {
                    [self.timer setEnd_date:[_datePicker.date returnDateWithFormat:@"dd/MM/yy"]];
                    [self.tableView reloadData];
                    [self.endDateActionSheet toggleActionSheetWithDelay:0.0f];
                }
            }
                break;
    
            case kScheduleEventVCEndDateUntilForever:
            {
                [self.timer setEnd_date:nil];
                [self.tableView reloadData];
                [self.endDateActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            case kScheduleEventVCEndDateCancel:
            {
                [self.endDateActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
    }
}

- (void)actionSheetButtonPressed:(LightwaveActionSheet *)tmpCustomActionSheet
                           index:(NSUInteger)tmpIndex
                      pickerView:(UIPickerView *)tmpPickerView
                            type:(DawnDuskActionSheetTypes)_type {
        
    if ([tmpCustomActionSheet tag] == kScheduleEventVCSetDuskDawn) {
        /*****************************************
         * SET TIME: DUSK | DAWN | TIME
         *****************************************/
        switch (tmpIndex) {
                
            case kScheduleEventVCDuskDawnDone:
            {
                switch (_type) {
                        
                    case kDawnDuskActionSheetTime:
                    {
                        // Infinite scroll, so use modulous to get exact value
                        
                        NSUInteger _numHours = [tmpPickerView selectedRowInComponent:0] % kHourNumberOfRows;
                        NSUInteger _numMins = [tmpPickerView selectedRowInComponent:1] % kMinuteNumberOfRows;
                        
                        //DebugLog(@"Hours: %02d", _numHours);
                        //DebugLog(@"Mins: %02d", _numMins);
                        
                        self.timer.time = [NSString stringWithFormat:@"%02d:%02d", _numHours, _numMins];
                    }
                        break;
                        
                    case kDawnDuskActionSheetBeforeDawn:   
                    case kDawnDuskActionSheetAfterDawn:  
                    case kDawnDuskActionSheetBeforeDusk:   
                    case kDawnDuskActionSheetAfterDusk:
                    {
                        // WFL works in multiples of 30. So if 60 mins, then this should be 2 (eg 2 * 30 = 60)
                        NSUInteger _selectedRow = [tmpPickerView selectedRowInComponent:0];
                        NSString *_selectedTitle = [[tmpPickerView delegate] pickerView:tmpPickerView titleForRow:_selectedRow forComponent:0];
                        NSInteger _minutes = [_selectedTitle intValue] / 30;
                        self.timer.time = [NSString stringWithFormat:@"%02d:%02d", _type, _minutes];
                    }
                        break;
                        
                    default:
                        break;
                }
                
                [self.tableView reloadData];
                [self.dawnDuskActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            case kScheduleEventVCDuskDawnCancel:
            {
                [self.dawnDuskActionSheet toggleActionSheetWithDelay:0.0f];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tmpTableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    return 4;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    CGSize size = [[self getStringForIndex:indexPath.row]
                   sizeWithFont:kTableViewFont
                   constrainedToSize:CGSizeMake(260, CGFLOAT_MAX)
                   lineBreakMode:NSLineBreakByWordWrapping];
    
    float _totalHeight = size.height + 10;
    
    if (_totalHeight <= 20.0f)
        return 20.0f;
    else
        return _totalHeight;
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    const char* className = class_getName([self class]);
    NSString *cellIdentifier = [NSString stringWithFormat:@"UITableViewCell-%s", className];
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [cell.textLabel setFont:kTableViewFont];
        [cell.textLabel setTextColor:[UIColor blackColor]];
        
        [cell.detailTextLabel setFont:kTableViewFont];
        [cell.detailTextLabel setTextColor:[AppStyleHelper isMegaMan] ? [UIColor darkGrayColor] : [UIColor lightGrayColor]];
        [cell.detailTextLabel setNumberOfLines:0];
        [cell.detailTextLabel setLineBreakMode:NSLineBreakByWordWrapping];

        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }

    switch (indexPath.row) {
            
        case kScheduleEventCellTime:
        {
            [cell.textLabel setText:NSLocalizedString(@"time", @"Time")];
            [cell.detailTextLabel setText:[self getStringForIndex:indexPath.row]];
        }
            break;
            
        case kScheduleEventCellDates:
        {
            [cell.textLabel setText:NSLocalizedString(@"dates", @"Dates")];
            [cell.detailTextLabel setText:[self getStringForIndex:indexPath.row]];
        }
            break;
            
        case kScheduleEventCellDays:
        {
            [cell.textLabel setText:NSLocalizedString(@"days", @"Days")];
            [cell.detailTextLabel setText:[self getStringForIndex:indexPath.row]];
        }
            break;
            
        case kScheduleEventCellMonths:
        {
            [cell.textLabel setText:NSLocalizedString(@"months", @"Months")];
            [cell.detailTextLabel setText:[self getStringForIndex:indexPath.row]];
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[[cell textLabel] setBackgroundColor:[UIColor clearColor]];
	[[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - Actions

- (void)editPressed {
    [self.optionsActionSheet toggleActionSheetWithDelay:0.0f];
}

- (IBAction)buttonPressed:(id)sender {
    switch ([sender tag]) {
            
        case kBtnBack:
        {
            [self.timer setupWithCommand:[self.undoChangesTimer getCommand]
                                  active:self.undoChangesTimer.active];

            [self dismissViewAnimated:YES];
        }
            break;
            
        case kBtnEdit:
        {
            [self editPressed];
        }
            break;
            
        case kBtnToggle:
        {
            if (self.timer) {
                Home *home = [Home shared];
                
                // Toggle Button
                UIButton *_button = (UIButton *)sender;
                [_button setSelected:!_button.selected];
                
                NSString *_buttonTitle = _button.selected ? NSLocalizedString(@"pause", @"Pause") : NSLocalizedString(@"activate", @"Activate");
                [_button setTitle:_buttonTitle forState:UIControlStateNormal];
                
                // Set Timer is Active
                [self.timer setActive:_button.selected];

                // If activitating... update to tomorrow if necessary
                if (self.timer.active) {
                    [self.timer adjustStartDateToTomorrow];
                }
                
                // Check if timer is stored locally, if not, add it
                if (![home.timers containsObject:self.timer]) {
                    [home addTimer:self.timer];
                    
                    // Save Events
                    if (self.event) {
                        self.event.timerName = self.timer.name;
                        [home saveEvents:YES];
                    }
                }
                
                // Save Changes
                [home saveTimers];
                
                // Update WFL
                if (self.timer.active) {
                    
                    // Alert
                    [self.view makeToast: NSLocalizedString(@"timer_activated", @"Timer Activated")];
                                        
                    // Create on WFL
                    [[WFLHelper shared] sendToUDP:YES
                                          command:[self.timer getCommand]
                                              tag:kRequestCreateTimer];
                    
                } else {
                    
                    // Alert
                    [self.view makeToast: NSLocalizedString(@"timer_paused", @"Timer Paused")];
                    
                    // Pause from WFL
                    [[WFLHelper shared] sendToUDP:YES
                                          command:[ServiceType getPauseTimerCommand:self.timer]
                                              tag:kRequestDeleteTimer];
                }
            }
        }
            break;
            
        case kBtnDone:
        {
            // Update WFL
            if (self.timer.active) {

                // Create/Store on WFL
                [[WFLHelper shared] sendToUDP:YES
                                      command:[self.timer getCommand]
                                          tag:kRequestCreateTimer];
                
            }
            
            // Save Changes
            Home *home = [Home shared];
            [home saveTimers];
            
            [self dismissViewAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
