//
//  GetAddressLocator.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 20/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "GetAddressLocator.h"
#import <MapKit/MapKit.h>

#define kAddressLocatorTimeoutTime 8.0f
#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

@implementation GetAddressLocator

@synthesize delegate;

- (id)init {
    if ((self = [super init])) {
    }
    
    return self;
}

- (void)dealloc {
    [self cancelRequest];
    
    self.delegate = nil;
}

- (void)cancelRequest {
    [[NSOperationQueue mainQueue] cancelAllOperations];
}

#pragma mark - 

- (void)getLongitudeAndLattitude:(NSString*)postcode {
    
    NSString *postCodeEscaped = [postcode stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *_urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=false",
                        postCodeEscaped];
    
    //DebugLog(@"Attempting request = %@", _urlString);

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:kAddressLocatorTimeoutTime];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if (error) {
                                   //DebugLog(@"Error: %@", [error localizedDescription]);
                                   
                                   if([self.delegate respondsToSelector:@selector(recievedLocalDidFail:)])
                                       [self.delegate recievedLocalDidFail:self];
                                   
                               } else {
                                   NSString *replyMessage = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                   //DebugLog(@"Success: %@", replyMessage);
                                   
                                   NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
                                   int responseStatusCode = [httpResponse statusCode];
                                   
                                   if (responseStatusCode == 200) {
                                       NSDictionary *dict = [replyMessage mutableObjectFromJSONString];
                                       [self getPostcodeSuccess:dict];
                                       
                                   } else {
                                       if([self.delegate respondsToSelector:@selector(recievedLocalDidFail:)])
                                           [self.delegate recievedLocalDidFail:self];
                                   }
                               }
                           }];
    
    
    if ([self.delegate respondsToSelector:@selector(addressRequestDidBegin:)])
        [self.delegate addressRequestDidBegin:self];
}

- (void)getAddressForLat:(float)tmpLat longitude:(float)tmpLong {
    [self cancelRequest];
    
    NSString *_urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false",
                        tmpLat,
                        tmpLong];
    
    //DebugLog(@"Attempting request = %@", _urlString);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:kAddressLocatorTimeoutTime];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if (error) {
                                   //DebugLog(@"Error: %@", [error localizedDescription]);
                                   
                                   if([self.delegate respondsToSelector:@selector(addressRequestDidFail:)])
                                       [self.delegate addressRequestDidFail:self];
                                   
                               } else {
                                   //NSString *replyMessage = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                   
                                   NSString *replyMessage = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   
                                   //DebugLog(@"Success: %@", replyMessage);
                                   
                                   NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
                                   int responseStatusCode = [httpResponse statusCode];
                                   
                                   if (responseStatusCode == 200) {
                                       NSDictionary *dict = [replyMessage mutableObjectFromJSONString];
                                       [self getAddressSuccess:dict];
                                   } else {
                                       if([self.delegate respondsToSelector:@selector(addressRequestDidFail:)])
                                           [self.delegate addressRequestDidFail:self];
                                   }
                               }
                           }];
    
    if ([self.delegate respondsToSelector:@selector(addressRequestDidBegin:)])
        [self.delegate addressRequestDidBegin:self];
}

#pragma mark -
#pragma mark Handlers

- (void)getPostcodeSuccess:(NSDictionary *)dict {
    [self cancelRequest];
    
    //DebugLog(@"keys = %@", [dict allKeys]);
    NSArray *tmpArray = [dict objectForKey:@"results"];
    //DebugLog(@"tmpArray = %@", tmpArray);
    
    if (tmpArray.count > 0) {
        NSDictionary *dict = [tmpArray objectAtIndex:0];
        NSDictionary *geometry = [dict objectForKey:@"geometry"];
        NSDictionary *location = [geometry objectForKey:@"location"];
        DebugLog(@"location = %@", location);
        
        if([self.delegate respondsToSelector:@selector(recievedLocationComplete:results:)])
            [self.delegate recievedLocationComplete:self results:location];
    } else {
        if([self.delegate respondsToSelector:@selector(recievedLocationComplete:results:)])
            [self.delegate recievedLocationComplete:self results:nil];
    }
}

- (void)getAddressSuccess:(NSDictionary *)dict {
    [self cancelRequest];
    
    //DebugLog(@"DICT: %@", dict);
    //DebugLog(@"keys = %@", [dict allKeys]);
    NSArray *tmpArray = [dict objectForKey:@"results"];
    NSMutableArray *results = [[NSMutableArray alloc] initWithCapacity:tmpArray.count];
    
    for (NSDictionary *tmpDict in tmpArray) {
        //DebugLog(@"keys = %@", [tmpDict allKeys]);
        
        /*
         "formatted_address",
         types,
         geometry,
         "address_components"
         */
        
        NSArray *addressComponentsArray = [tmpDict objectForKey:@"address_components"];
        //DebugLog(@"addressComponentsArray: %@", addressComponentsArray);
        NSString *name = nil;
        NSString *_country = nil;
        
        // GET COUNTRY
        for (NSDictionary *componentDict in addressComponentsArray) {
            NSArray *typesArray = [componentDict objectForKey:@"types"];

            for (NSString *type in typesArray) {
                if ([type isEqualToString:@"country"]) {
                    _country = [componentDict objectForKey:@"long_name"];
                    goto outer2; /* breaks outer loop */
                }
            }
        }
        outer2:;
        
        // GET NAME
        // Found nothing? try again with another field..
        if (name.length <= 0) {
            for (NSDictionary *componentDict in addressComponentsArray) {
                NSArray *typesArray = [componentDict objectForKey:@"types"];
                
                // Try and get location...
                for (NSString *type in typesArray) {
                    //DebugLog(@"type: %@", type);
                    if ([type isEqualToString:@"postal_town"]) {
                        name = [componentDict objectForKey:@"long_name"];
                        goto outer1; /* breaks outer loop */
                    }
                }
                
            }
            outer1:;
        }
        
        // GET NAME
        // Found nothing? try again with another field..
        if (name.length <= 0) {
            for (NSDictionary *componentDict in addressComponentsArray) {
                NSArray *typesArray = [componentDict objectForKey:@"types"];
                
                for (NSString *type in typesArray) {
                    if ([type isEqualToString:@"administrative_area_level_2"]) {
                        name = [componentDict objectForKey:@"long_name"];
                        goto outer; /* breaks outer loop */
                    }
                }
                
            }
        outer:;
        }
        
        // GET NAME
        // Found nothing? try again with another field..
        if (name.length <= 0) {
            for (NSDictionary *componentDict in addressComponentsArray) {
                NSArray *typesArray = [componentDict objectForKey:@"types"];
                
                for (NSString *type in typesArray) {
                    if ([type isEqualToString:@"administrative_area_level_1"]) {
                        name = [componentDict objectForKey:@"long_name"];
                        goto outer0; /* breaks outer loop */
                    }
                }                
            }
            outer0:;
        }
        
        //DebugLog(@"Name: %@", name);
        //DebugLog(@"Country: %@", _country);
        
        if ([name length] > 0) {
            name = [NSString stringWithFormat:@"%@%@", name, [_country length] > 0 ? [NSString stringWithFormat:@", %@", _country] : @""];
            
            DebugLog(@"Address: %@", name);
            [results addObject:name];
            break;
        }
        
        // Still nothing?
        if (results.count <= 0) {
            
            if ([_country length] > 0) {
                // Still nothing? try the country
                DebugLog(@"country = %@", _country);
                [results addObject:_country];
                break;
                
            } else {
                // Still nothing? try the full address
                NSString *formattedAddress = [tmpDict objectForKey:@"formatted_address"]; // Full formatted address
                DebugLog(@"formattedAddress = %@", formattedAddress);
                
                if (formattedAddress.length > 0) {
                    [results addObject:formattedAddress];
                    break;
                }
            }
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(addressDidComplete:results:)])
        [self.delegate addressDidComplete:self results:results];
}

- (double)getDistanceBetweenLocationsWithSrcLat:(double)srcLat
                                  withSrcLong:(double)srcLng
                                   withDstLat:(double)dstLat
                                   withDstLng:(double)dstLng {
    CLLocationDegrees userLat = srcLat;
    CLLocationDegrees userLong = srcLng;
    CLLocationDegrees destLat = dstLat;
    CLLocationDegrees destLong = dstLng;
    CLLocation *userLoc = [[CLLocation alloc] initWithLatitude:userLat longitude:userLong];
    CLLocation *destinationLoc = [[CLLocation alloc] initWithLatitude:destLat longitude:destLong];
    double dist = [userLoc distanceFromLocation:destinationLoc] / 1609.344;
    return dist;
}

@end
