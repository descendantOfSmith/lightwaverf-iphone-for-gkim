//
//  DayActionSheet.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 02/08/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "LightwaveActionSheet.h"

typedef enum
{
    kDayActionSheetMonday = 1,
    kDayActionSheetTuesday = 2,
    kDayActionSheetWednesday = 3,
    kDayActionSheetThursday = 4,
    kDayActionSheetFriday = 5,
    kDayActionSheetSaturday = 6,
    kDayActionSheetSunday = 7
} DayActionSheetTypes;

@interface DayActionSheet : LightwaveActionSheet {
    
}

- (id)initWithPoint:(CGPoint)point
       buttonTitles:(NSArray *)tmpButtonTitles
        cancelTitle:(NSString *)tmpCancelTitle
         dataSource:(id<LightwaveActionSheetDataSource>)tmpDataSource
           delegate:(id<LightwaveActionSheetDelegate>)tmpDelegate
       selectedDays:(NSString *)tmpSelectedDays;

@end
