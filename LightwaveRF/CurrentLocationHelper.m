//
//  CurrentLocationHelper.m
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 20/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "CurrentLocationHelper.h"

@interface CurrentLocationHelper ()
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) GetAddressLocator *getAddressLocator;
@end

static CurrentLocationHelper *locationHelper = nil;

@implementation CurrentLocationHelper

@synthesize locationManager;
@synthesize currentLocation;
@synthesize getAddressLocator;

+ (id)shared {
    @synchronized(self) {
        if(locationHelper == nil)
            locationHelper = [[super allocWithZone:NULL] init];
    }
    return locationHelper;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self shared];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)dealloc {
    //DebugLog(@"DEALLOC");
    
    [self stopSearchingLocation];
}

#pragma mark - Private

- (void)cancelLocationManager {
    if (self.locationManager) {
        [self.locationManager stopUpdatingLocation];
        self.locationManager.delegate = nil;
    }
    
    self.locationManager = nil;
}

- (void)cancelAddressLocator {
    [self.getAddressLocator cancelRequest];
    self.getAddressLocator = nil;
}

- (void)getAddress {
    if (self.getAddressLocator == nil)
        self.getAddressLocator = [[GetAddressLocator alloc] init];
    
    self.getAddressLocator.delegate = self;
    [self.getAddressLocator getAddressForLat:self.currentLocation.coordinate.latitude
                                   longitude:self.currentLocation.coordinate.longitude];
}

#pragma mark - Public

- (void)getLocation {
    if (self.locationManager == nil)
        self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
}

- (void)stopSearchingLocation {
    [self cancelLocationManager];
    
    [self cancelAddressLocator];
    
    self.currentLocation = nil;
}

#pragma mark -
#pragma mark CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    
    [self cancelLocationManager];
    
    self.currentLocation = newLocation;
    
    
#if TARGET_IPHONE_SIMULATOR
    
    // UK
    self.currentLocation = [[CLLocation alloc] initWithLatitude:57.686785 longitude:11.908170];
    
    // Holland
    //self.currentLocation = [[CLLocation alloc] initWithLatitude:051.71 longitude:005.36];
    
#endif
    
    
    [self getAddress];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    
    DebugLog(@"Error = %@", [error localizedDescription]);

    NSString *err1 = NSLocalizedString(@"location_error1", @"Unable to locate your current location.");
    NSString *err2 = NSLocalizedString(@"location_error2", @"Please ensure location services is enabled and try again.");
    
    [self failedWithError:[NSString stringWithFormat:@"%@ %@",err1,err2]];
    self.currentLocation = nil;
}

#pragma mark -
#pragma mark GetAddressLocator Delegate

- (void)addressRequestDidFail:(GetAddressLocator *)getAddressLocator {
    NSString *err1 = NSLocalizedString(@"location_error3", @"Unable to locate your city/region.");
    NSString *err2 = NSLocalizedString(@"check_connection", @"Please check your internet connection and try again.");
        
    [self failedWithError:[NSString stringWithFormat:@"%@ %@",err1,err2]];
    self.currentLocation = nil;
}

- (void)addressDidComplete:(GetAddressLocator *)getAddressLocator results:(NSMutableArray *)tmpResults {    
    if (tmpResults.count > 0) {
        NSString *_userLocationStr = [tmpResults objectAtIndex:0];
        [self sucessWithLocation:_userLocationStr];
        
    } else
        [self failedWithError:NSLocalizedString(@"location_error3", @"Unable to locate your city/region.")];

    [self cancelAddressLocator];
}

#pragma mark -
#pragma mark Handlers

- (void)sucessWithLocation:(NSString *)_locationStr {
    //DebugLog(@"Location = %@", _locationStr);
    
    // From LWRF Docs: Force to 3 digits and 2 decimals = %06.2f (length 6 chars, with 2 decimal places)
    
    NSString *location = [NSString stringWithFormat:@"%06.2f,%06.2f", self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude];
    //DebugLog(@"Location = %@", location);
    
    NSMutableDictionary *_dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                  _locationStr, @"region",
                                  location, @"latlong",
                                  nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kCurrentLocationSuccessNotificationKey
                                                        object:_dict];
    
    // Cleanup
    [self stopSearchingLocation];
}

- (void)failedWithError:(NSString *)_errorStr {
    //DebugLog(@"failedWithError = %@", _errorStr);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kCurrentLocationFailedNotificationKey
                                                        object:_errorStr];
    
    // Cleanup
    [self stopSearchingLocation];
}

@end
