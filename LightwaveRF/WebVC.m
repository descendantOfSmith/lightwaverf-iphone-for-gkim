//
//  WebVC.m
//  LightwaveRF
//
//  Created by Nik Lever on 11/07/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import "WebVC.h"

@interface WebVC ()

@end

@implementation WebVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithURL:(NSString *)_url{
    self = [super initWithNibName:nil bundle:nil];
    url = _url;
    return self;
}

-(void)setURL:(NSString *)_url{
    
    
    NSURL *_goToURL = [NSURL URLWithString:_url];
    
    // Cleanup cache
    
//    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    // Clear cookies
    
//    for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
//        
//        if([[cookie domain] isEqualToString:[_goToURL host]]) {
//            
//            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
//        }
//    }

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:_goToURL];
    
    // Double safe, tell it not to keep a cache
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    
    [_web_wvw loadRequest:request];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (self.navigationController != nil){
        UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(shutDown)];
        self.navigationItem.leftBarButtonItem = closeButton;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (url!=nil){
        [self setURL:url];
        url = nil;
    }
}

-(void)shutDown{
    if (self.navigationController != nil) [[self navigationController] dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWeb_wvw:nil];
    url = nil;
    [super viewDidUnload];
}

#pragma mark - Webview Delegate

- (void)webViewDidStartLoad:(UIWebView *)tmpWebview {
    //DebugLog(@"webViewDidStartLoad");
    
    [self showSpinnerWithText:@""];
}

- (void)webViewDidFinishLoad:(UIWebView *)tmpWebview {
    //DebugLog(@"webViewDidFinishLoad");
    
    [self removeSpinner];
}

- (void)webView:(UIWebView *)tmpWebview didFailLoadWithError:(NSError *)error {
    //DebugLog(@"webView didFailLoadWithError");
    
    [self removeSpinner];
}

#pragma mark - NSNotification General Delegate

- (void)receivedChangedLanguageNotification:(NSNotification *)_notification {
    
}

@end
