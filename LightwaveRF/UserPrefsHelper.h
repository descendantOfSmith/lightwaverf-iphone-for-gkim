//
//  UserPrefsHelper.h
//  LightwaveRF
//
//  Created by Dan H (Sync Interactive) on 20/09/2013.
//  Copyright (c) 2013 Nicholas Lever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserPrefsHelper : NSObject {
    
}

+ (id)shared;

- (void)registerDefaults:(NSDictionary *)_dict;
- (void)save;
- (NSString *)getCurrentWiFi;
- (BOOL)isAtCurrentLocation;
- (BOOL)isLockedToPrivateAccounts;

#pragma mark - Keys

//- (NSString *)keyForUserName;
//- (NSString *)keyForUserEmail;
//- (NSString *)keyForUserPassword;
//- (NSString *)keyForUserMac;
- (NSString *)keyForRegistered;
- (NSString *)keyForCode;
- (NSString *)keyForDbInitialised;
- (NSString *)keyForDemoAccount;
- (NSString *)keyForEnableWFLPopups;
- (NSString *)keyForVideoShown;
- (NSString *)keyForTestWiFi;
- (NSString *)keyForUseWiFiName;
- (NSString *)keyForLoginAuto;
//- (NSString *)keyForElecRate;
//- (NSString *)keyForImpKwh;
//- (NSString *)keyForLocationInfo;
//- (NSString *)keyForTimeZoneInfo;
//- (NSString *)keyForWiFiPin;
//- (NSString *)keyForEmailPwd;
//- (NSString *)keyForSSID;
- (NSString *)keyForShowHomeFooter;
- (NSString *)keyForShowHomeFooterHasRunBefore;
- (NSString *)keyForCurrentAccountId;

#pragma mark - Getters

- (NSString *)getUserName;
- (NSString *)getUserEmail;
- (NSString *)getUserPassword;
- (NSString *)getUserMac;
- (NSString *)getCode;
- (NSString *)getElecRate;
- (NSString *)getImpKwh;
- (NSString *)getLocationInfo;
- (NSString *)getTimeZoneInfo;
- (NSString *)getTimeZoneInfoWithoutHours;
- (NSString *)getWiFiPin;
- (NSString *)getEmailPwd;
- (NSString *)getSSID;
- (BOOL)getRegistered;
- (BOOL)getDemoAccount;
- (BOOL)getEnableWFLPopups;
- (BOOL)getVideoShown;
- (BOOL)getTestWiFi;
- (BOOL)getUseWiFiName;
- (BOOL)getLoginAuto;
- (BOOL)getShowHomeFooter;
- (BOOL)getShowHomeFooterHasRunBefore;
- (NSInteger)getDBInitialised;
- (NSInteger)getCurrentAccountId;
- (NSString *)getCurrentLanguageCode;
- (NSMutableArray *)getAvailableLanguages;

#pragma mark - Setters

- (void)setUserName:(NSString *)_value;
- (void)setUserEmail:(NSString *)_value;
- (void)setUserPassword:(NSString *)_value;
- (void)setUserMac:(NSString *)_value;
- (void)setCode:(NSString *)_value;
- (void)setElecRate:(NSString *)_value;
- (void)setImpKwh:(NSString *)_value;
- (void)setLocationInfo:(NSString *)_value;
- (void)setTimeZoneInfo:(NSString *)_value;
- (void)setWiFiPin:(NSString *)_value;
- (void)setEmailPwd:(NSString *)_value;
- (void)setSSID:(NSString *)_value;
- (void)setRegistered:(BOOL)_value;
- (void)setDemoAccount:(BOOL)_value;
- (void)setEnableWFLPopups:(BOOL)_value;
- (void)setVideoShown:(BOOL)_value;
- (void)setTestWifi:(BOOL)_value;
- (void)setUseWiFiName:(BOOL)_value;
- (void)setLoginAuto:(BOOL)_value;
- (void)setShowHomeFooter:(BOOL)_value;
- (void)setShowHomeFooterHasRunBefore:(BOOL)_value;
- (void)setDBInitialised:(NSInteger)_value;
- (void)setCurrentAccountId:(NSInteger)_value;

@end
