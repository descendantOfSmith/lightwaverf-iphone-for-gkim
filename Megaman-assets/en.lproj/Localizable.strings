/* 
  Localizable.strings
  LightwaveRF

  Created by Nik Lever on 06/12/2012.
  Copyright (c) 2012 Nicholas Lever. All rights reserved.
 
 LightwaveRF | Jim
 
 */

"end_date_alert" = "End year cannot be later than %d.";
"choose_language" = "Choose Language";
"wifi_name_info" = "This feature is intended for users with multiple WiFiLink locations. The app will check if the WiFi network you are connected to is what is saved on your Account%@. When they do not match, all commands will be remote.";
"delete_zone" = "Delete Zone";
"delete_zone_info" = "This room has devices named in it. Deleting this room without correctly deleting these devices first may cause incorrect pairing and timer problems.  Please delete all devices before deleting this room.";
"delete_device" = "Delete Device";
"delete_device_info" = "Deleting does not remove the pairing information stored in the product. To remove pairing information put your product in pairing mode and then press 'OK'.";
"connected" = "Connected";
"disconnected" = "Disconnected";
"error_timers_limit" = "Sorry, you have reached the limit of %d timers. You will need to delete an existing timer before you can add another.";
"error_events_limit" = "Sorry, you have reached the limit of %d scenes. You will need to delete an existing scene before you can add another.";
"once_only_on" = "Once only on";
"no_days" = "No days";
"no_months" = "No months";
"beta_version" = "%@ Build (Beta)";
"release_version" = "Version %@ (%@)";
"more" = "More";
"event" = "Scene";
"at" = "at";
"from" = "From";
"until" = "until";
"each" = "Each";
"to_run_forever" = "to run forever";
"from_now_until" = "From now until";
"from_now_to_run_forever" = "From now to run forever";
"until_cancelled" = "until cancelled";
"from_now_until_cancelled" = "from now until cancelled";
"save_elec_rate" = "Save Elec Rate";
"app_default_language" = "English";
"select_a_colour" = "Select a Colour (cycling will use column colours)";
"change_cycling" = "Change Cycling";
"forgotten_password" = "Forgotten Password?";
"sync_events" = "This will delete and re-create all scenes and timers stored on your WiFiLink. Any Scenes or Timed Events which have not completed their cycle will be stopped. Are you sure?";
"submit" = "Submit";
"previous" = "Previous";
"next_text" = "Next";
"edit_delays" = "Edit Order/Delays";
"cancel_sequence" = "Cancel Sequence";
"deleting_sequence" = "Deleting Sequence";
"add_timer" = "Add Timer";
"already_registered" = "Already registered.";
"all" = "all";
"about_you" = "About you";
"actions" = "Actions";
"account" = "Account(\"%@\")";
"account_location" = "Account Location:";
"account_timezone" = "Account Timezone:";
"account_wifi" = "Account WiFi:";
"current_location" = "Current Location:";
"current_timezone" = "Current Timezone:";
"current_wifi" = "Current WiFi:";
"action_error" = "There was an error last time you used this action. Here are the most recent errors.";
"activate" = "Activate";
"activate_routine" = "Activate Routine";
"add" = "Add";
"add_action" = "Add Action";
"add_another_event" = "Add another scene";
"add_another_zone" = "Add another zone";
"add_event" = "Add Scene";
"add_delay" = "Add Delay";
"add_delay2" = "Add Delay";
"add_heating_profile" = "Added Heating Profile";
"add_new_device" = "Add new device";
"add_new_mood" = "Add new mood &#8230;";
"add_product" = "Add a product &#8230;";
"add_random" = "Add Random Delay";
"add_zone" = "add a new zone";
"address" = "Address";
"all" = "All";
"all_off" = "All off";
"after_dawn" = "After dawn";
"after_dusk" = "After dusk";
"arrive_home" = "Arrive Home";
"at_home" = "At home?";
"auto_login" = "Login automatically";
"autumn" = "Autumn";
"away" = "Away";
"back" = "Back";
"bathroom" = "Bathroom";
"before_dawn" = "Before dawn";
"before_dusk" = "Before dusk";
"buffering_video" = "Buffering video ...";
"build" = "Build";
"cancel" = "Cancel";
"choose_products" = "Choose Products";
"close" = "CLOSE";
"code" = "Code";
"commands" = "commands";
"connect" = "Connect";
"continue" = "Continue";
"cost_per_hour" = "Cost Per Hour";
"cost_today" = "Cost So Far Today";
"cost_yesterday" = "Cost For Yesterday";
"current" = "Current";
"set_location_message" = "Your current location is %@ - %@. For dusk/dawn accuracy, you can set this as your WiFiLinks location. Click yes to set %@ as the location of WiFiLink <%@>";
"kwh_today" = "kWh So Far Today";
"kwh_yesterday" = "kWh For Yesterday";
"country" = "Country";
"confirm_delete" = "Delete?";
"create_account" = "Create Account";
"dash" = "-";
"date" = "Date";
"dates" = "Dates";
"day" = "Day";
"days" = "Days";
"day1" = "Mon";
"day2" = "Tue";
"day3" = "Wed";
"day4" = "Thu";
"day5" = "Fri";
"day6" = "Sat";
"day7" = "Sun";
"dawn" = "Dawn";
"dusk" = "Dusk";
"default_temperature" = "20";
"delete" = "Delete";
"delete_room" = "Are you sure you want to delete this room?";
"deleting_event" = "Deleting Scene";
"deleting_timer" = "Deleting Timer";
"delay" = "Delay";
"device" = "Device";
"devices" = "Devices";
"device_locked" = "Device locked";
"device_fully_locked" = "Fully locked";
"device_unlocked" = "Device unlocked";
"device_mood" = "Device/Mood";
"device_slider_show_icon" = "Device slider show icon";
"device_type_icon" = "Device Type Icon";
"device_pairing_complete" = "Device pairing complete";
"device_pairing_start" = "Put your product in pairing mode and press 'OK'";
"dimmer" = "Dimmer";
"done" = "Done";
"done2" = "Done";
"edit" = "Edit";
"edit_actions" = "Edit Actions";
"edit_delay" = "Edit Delay";
"edit_devices" = "Edit Devices";
"edit_event" = "Edit scene";
"elec_rate" = "Elec Rate";
"email" = "Email";
"email_address" = "Email address";
"energy" = "Energy";
"end_time" = "End time";
"enter_event_name" = "Enter scene name";
"energy_spinner_request" = "Requesting Energy Meter...";
"enter_code" = "Enter the code on the bottom of your Wifi-Link.";
"enter_link" = "Enter the code on the display of your Wifi-Link.";
"error" = "Error";
"example_company" = "example@company.com";
"event_name" = "Scene name";
"event_info" = "Scene Info";
"events_removed1" = "Removing this device has affected the following Scenes - ";
"events_removed2" = " - Please check these Scenes.";
"event_time_error" = "Please enter a future date/time.";
"events" = "Scenes";
"event_triggered" = "Scene triggered";
"every_day" = "Every Day";
"every_month" = "Every Month";
"favourites" = "Favourites";
"file_to_support" = "Send Settings to Support";
"find_installer" = "Find an approved installer";
"forgotten" = "Forgotten your password?";
"garage" = "Garage";
"geo_loc" = "Geo Location";
"geo_loc_explanation" = "This is used to work out when dawn and dusk are";
"get_settings_from_web_server" = "Get settings from web server";
"get_settings" = "Get Settings";
"save_settings" = "Save Settings";
"get_settings_msg" = "Are you sure you want to get your settings from the server?";
"save_settings_msg" = "Are you sure you want to save your settings to the server?";
"hallway" = "Hallway";
"heating" = "Heating";
"help" = "Help";
"holiday" = "Holiday";
"holidays" = "Holidays";
"home" = "Home";
"home_icon" = "Home";
"home_monitoring" = "Home Monitoring";
"home_away_delete_error" = "The Scenes Home and Away cannot be deleted";
"hours" = "Hours";
"hub" = "Hub";
"hub_beginning" = "74-0A-BC-";
"hub_setting_default" = "XX";
"hub_serial" = "Hub Serial Number";
"impkwh" = "imp/kWh";
"incorrect_password" = "Incorrect Password";
"info" = "Info";
"intro1" = "Using INGENIUM® RF is easy. Simply add zones which represent areas in your home, then connect your INGENIUM® RF products to those zones.";
"intro2" = "Adding zones. A zone can be an area that includes multiple INGENIUM® RF products, for example \'Lounge\' or \'Kitchen\'";
"intro2b" = "To ad a new zone tap the plus button at the top of the home page";
"intro3" = "Adding products to your zones. After creating your zone, simply add your products to that zone";
"intro3b" = "Put your product in pairing mode and press the connect button";
"intro4" = "That\'s it. You\'re good to go. You can now use all the other amazing features in the app to start using your smarter INGENIUM® RF home.";
"john_appleseed" = "John Appleseed";
"kitchen" = "Kitchen";
"kW" = "kW";
"last_updated" = "Last Updated";
"latitude" = "Latitude";
"leave_home" = "Leave Home";
"lighting" = "Lighting";
"line1" = "Line 1";
"line2" = "Line 2";
"location" = "Location:";
"location_set" = "Location Set";
"login_problem" = "Login Problem";
"login" = "Login";
"log_in" = "Log In";
"log_out" = "Log Out";
"log_out_message" = "You may have unsaved settings, would you like to save your settings to the server?";
"longitude" = "Longitude";
"lounge" = "Lounge";
"minutes" = "Mins";
"money_unit" = "p";
"money_unit2" = "£";
"mood" = "Mood";
"mood_saved" = "Mood Saved";
"mood_help" = "You can create up to 3 moods for each zone";
"mood_lighting" = "Mood Lighting";
"monitor" = "Monitor";
"month" = "Month";
"months" = "Months";
"month1" = "Jan";
"month2" = "Feb";
"month3" = "Mar";
"month4" = "Apr";
"month5" = "May";
"month6" = "Jun";
"month7" = "Jul";
"month8" = "Aug";
"month9" = "Sep";
"month10" = "Oct";
"month11" = "Nov";
"month12" = "Dec";
"name" = "Name";
"new_to_lightwave" = "New to INGENIUM® RF?";
"new_to_lightwave_watch" = "New to INGENIUM® RF? Watch this video";
"new_zone_name" = "New zone name";
"next" = "Next";
"no" = "No";
"nocode" = "I didn\'t receive a code";
"no_name" = "No name";
"off" = "Off";
"offline" = "Offline";
"offline_message" = "Some functions may not be available while you\'re offline.";
"ok" = "Ok";
"on" = "On";
"open" = "OPEN";
"pair" = "Connect";
"pause" = "Pause";
"pausing_timer" = "Pausing Timer";
"password" = "Password";
"password_placeholder" = "1234";
"pin" = "Pin";
"please_login" = "Please enter your details and press Login";
"plus" = "+";
"postcode" = "Postcode";
"power" = "Power";
"program" = "PROGRAM";
"problem_was" = "The problem was:";
"products" = "Products";
"random_delay" = "Random Delay";
"rename_event_error" = "Scenes Home and Away cannot be renamed";
"reorder" = "Change order";
"required" = "Required";
"return_code_was" = "Return code was ";
"save" = "Save";
"save_mood" = "Save Mood";
"save_settings" = "Save Settings";
"save_rate" = "Save Rate";
"save_impkwh" = "Save Imp/kWh";
"save_settings_to_web_server" = "Save settings to web server";
"settings_saved" = "Settings Saved";
"schedule_event" = "Schedule Scene";
"seconds" = "Secs";
"setup_products" = "Setup";
"setup" = "Setup";
"set" = "Set";
"set_date" = "Set Date";
"set_days" = "Set Days";
"set_months" = "Set Months";
"set_start_date" = "Set Start Date";
"set_end_date" = "Set End Date";
"set_delay" = "Set Delay";
"set_end_time" = "Set End Time";
"set_start_time" = "Set Start Time";
"set_ssid" = "Set WiFi Network as Home";
"set_temp" = "Set Temp";
"set_time" = "Set Time";
"set_timezone" = "Set Timezone on your WiFiLink";
"set_timezone_message" = "You are about to change the timezone of %@'s WiFi Link (MAC Address - %@) to %@. Are you sure?";
"set_ssid_message" = "You are about to change the Home WiFi Network of %@'s WiFiLink (%@) to %@.  When your device is connected to this WiFi network, the app will automatically send all commands via local WiFi by default. Are you sure?";
"change_wifi_location" = "Change WiFiLink Location";
"set_wifi_location" = "Set WiFiLink Location";
"ssid_set" = "Home Location Set";
"settings" = "Settings";
"settings_wifi_register" = "Check WiFiLink and press 'Yes' to register this device.";
"show_video" = "Show video button on Login screen";
"show_footer" = "Show 'Switch Location' Panel";
"skip" = "Skip";
"socket" = "Socket";
"spring" = "Spring";
"start_profile" = "Start Profile";
"summer" = "Summer";
"start_mood" = "Start Mood";
"start_now" = "Start Now";
"start_time" = "Start time";
"start_after_delay" = "Start after a delay";
"start_at_time" = "Start at time";
"start_pairing" = "Start pairing";
"stay_in_3G" = "Stay in 3G/Remote Mode";
"stop" = "STOP";
"stop_all_events" = "Stop All Scenes";
"stop_all_events_message" = "All scenes stopped";
"sync_device_events" = "Sync Scenes/Timers with WiFiLink";
"switch_location" = "Switch Location";
"switch_all_lights_off" = "Switch All Lights Off";
"tap_to_change" = "Tap to change between KW/H and Cost/Hour";
"target" = "target";
"temp" = "Temp";
"temp_for" = "Temp for";
"test_remote_link" = "Test Wifi remote link";
"test_account" = "You are using the Test account, remote calls are disabled. Try connecting to the WiFi Link.";
"time" = "Time";
"timers" = "Timers";
"timer_paused" = "Timer Paused";
"timer_activated" = "Timer Activated";
"timezone_set" = "Timezone Set";
"to" = "to";
"to_off" = "to off";
"to_on" = "to on";
"toggle_button" = "Toggle Button";
"tick" = "Tick";
"type" = "Type";
"up_to" = "Up to";
"until_forever" = "Until Forever";
"unpaired" = "unpaired";
"updating_wfl" = "Updating WFL scenes & timers";
"updating_wfl_complete" = "Updating WFL complete";
"Unlock" = "Unlock";
"Unknown" = "Unknown";
"use_wifi_name" = "Use WiFi name to check your location";
"version" = "Version";
"video_name" = "Day in the life";
"your_home" = "Your Home";
"warning" = "Warning";
"weekday" = "Weekday";
"weekend" = "Weekend";
"weekdays" = "Weekdays";
"weekends" = "Weekends";
"welcome" = "Welcome";
"we_need" = "We need";
"winter" = "Winter";
"wifi_link" = "WiFi Link";
"wifi_stub" = "740ABC-";
"wifi_stub_placeholder" = "030A8B";
"wifi_current" = "Wifi: Current(\"%@\")";
"wrong_email" = "Wrong Email";
"year" = "Year";
"yes" = "Yes";
"your_first_zone" = "Your first zone";
"your_first_event" = "Your first scene";
"zone" = "Zone";
"zone_name" = "Zone Name";
"zones" = "Zones";
"_200dp" = "200dp";
"intro_title1" = "Using INGENIUM® RF is easy";
"intro_message1" = "Simply add zones which represent areas in your home, then connect your INGENIUM® RF products to these zones.";
"intro_title2" = "Adding Zones";
"intro_message2" = "A zone can be an area that includes multiple INGENIUM® RF products, for example \'Lounge\' or \'Kitchen\'.";
"intro_message2b" = "To add a new zone tap the plus button at the top of the home page.";
"intro_title3" = "Adding products to your zones";
"intro_message3" = "After creating your zone, simply add your products to that zone. Put your product in paring mode and hit the plus button.";
"intro_title4" = "That\'s it. You\'re good to go.";
"intro_message4" = "You can now use all the other amazing features in the app to start using your smarter INGENIUM® RF home.";
"mood_already_set" = "This Mood is already set for this Zone, you can edit the existing one or delete it and add a new one.";
"zone_has_heating" = "This Zone already has a heating control. You can only add one heating control per Zone.";
"sorry_only" = "Sorry, only";
"slots_for_products" = " slots for products per zone";
"how_to_save_mood" = "Press the Edit button and then the Save Mood button to store the current state for a zone.";
"edit_event_called" = "edit scene called";
"init_message" = "Please click the video or press login to access an existing account.";
"loading" = "Loading...";
"email_error" = "There are no email clients installed.";
"impkwh_error" = "Please enter a value between 300 and 4000";
"login_error1" = "Please enter your email and password or create a new account by pressing \'New to INGENIUM® RF?\'";
"login_error2" = "We cannot log you in. Please check email address and PIN are correct or setup an account if you are a new customer.";
"login_error3" = "Please create a new account";
"login_error4" = "We can\'t find a user with this email address";
"login_error5" = "The password you entered is incorrect, please try again";
"login_error6" = "Please use the New to INGENIUM® RF? button";
"register_error1" = "Please enter your name";
"register_error2" = "Please enter your email address";
"register_error3" = "Please check your email address";
"register_error4" = "Please use 4 numbers in your password";
"register_error5" = "Please enter the code on the base of your Wifi Link. eg. 03-F2-E1. When entering do not type the hyphens they will be added automatically.";
"register_error6" = "The code number will have 4 digits";
"register_error7" = "There was a problem logging on to the server. Check the code you entered matches the one on the Wifi Link screen or press the button to resend the code";
"register_error8" = "An account with this email address already exists.";
"register_error9" = "The Wifi Link MAC field should only contain numbers and the following letters A, B, C, D, E, F.";
"remote_error1" = "Creating your house on the web server failed, please try later";
"remote_error2" = "Update failed, please check your credentials.";
"remote_error3" = "Problem connecting to the server.";
"remote_error4" = "WFL unavailable, switching to remote.";
"remote_error5" = "WFL located, switching from remote.";
"video_error1" = "There is a problem playing the video at this time, please try later.";
"warning1" = "This will replace the zones and settings stored remotely with those on this phone Are you sure?";
"warning2" = "";
"wifilink_register" = "WiFiLink Register";
"wifi_popups" = "WiFi/3G Popup Notifications";
"wifi_location_saved" = "WiFiLink saved";
"web_service_error" = "There was a problem connecting to the INGENIUM® RF server.";
"message1" = "Server settings have been replaced.";
"message2" = "This will replace the settings you have stored on your phone. Are you sure?";
"message3" = "Settings received";
"message4" = "You already have an scene with that name. Please use another name";
"message5" = "The configuration file could not be understood, sorry.";
"message6" = "Could not find the information about your house, please restart.";
"message7" = "Error loading the information about your house, please restart.";
"message8" = "Error saving the information about your house, please restart.";
"message9" = "Please make sure we have your email, pin and MAC address - go to settings";
"message10" = "Please make sure we have your email, pin and MAC address - press New to INGENIUM® RF on the Login screen.";
"message11" = "Check the Wifi Link and press Yes to register this phone";
"message12" = "Settings updated";
"message13" = "Resend 4 Digit Code";
"message14" = "Resend the 4 digit code to your Wifi Link";
"message15" = "Error loading the information about your house.";
"message16" = "Enter a valid scene name.";
"message17" = "Enter a valid device name.";
"message18" = "Device naming is restricted to alphanumeric characters, as well as a dash, underscore and space.";
"message19" = "Enter a valid zone name.";
"message20" = "Zone naming is restricted to alphanumeric characters, as well as a dash, underscore and space.";
"message21" = "Restricted to a maximum of 16 characters.";
"help1" = "To add a new Zone press the + button. To edit Zone names, move them around in the list or delete them press the Edit button";
"help2" = "To add a new Device or mood to your list press the + button. To edit Device names, move them around in the list or delete them press the Edit button";
"help3" = "To add a new Scene press the + button. To edit Scene names, move them around in the list or delete an Scene press the Edit button";
"help4" = "To edit the Actions in an scene press the Actions button";
"help5" = "To add an action to this Scene press the Add Action button. To add a Delay press the Add Delay button. To delete an action or move it in the list press the Edit button.";
"help6" = "Go to the Settings tab and press \'Save settings to web server\' button to upload your edits when you have finished editing.";
"help7" = "To add a new Heating time press the \'Add...\' button. To edit or delete a heating time press the \'Edit\' button.";
"helptitle1" = "Select the Home tab";
"helptitle2" = "Home tab: press Zone name";
"helptitle3" = "Select the Scene tab";
"helptitle4" = "Scene tab: press Actions button";
"helptitle5" = "Scene tab: actions screen";
"helptitle6" = "Select the Settings tab";
"udp_error1" = "Problem saving timer";
"udp_error2" = "Problem saving scene";
"udp_error3" = "Sequence not stored on Wifi";
"udp_error4" = "Time not set";
"udp_error5" = "All Off could not be set";
"udp_error6" = "Mood could not be set";
"udp_error7" = "Device could not be set";
"udp_error8" = "Problem creating the Scene";
"udp_error9" = "Problem running the Scene";
"udp_error10" = "Timer not created";
"udp_error11" = "UDP not detected, unable to poll WiFi Link.";
"location_error1" = "Unable to locate your current location.";
"location_error2" = "Please ensure location services is enabled and try again.";
"location_error3" = "Unable to locate your city/region.";
"check_connection" = "Please check your internet connection and try again.";
"error_devices_limit" = "Sorry, only %d slots for products per zone.";
"error_zones_limit" = "You can only add %d zones at present. We are working to increase this limit, watch out for an update.";
"error_actions_limit" = "Sorry, there is a maximum of %d actions in an scene";
"error_delay_minimum" = "Sorry, there is a minimum of 3 secs for a delay";
"error_action_delay" = "You must add an action before adding a delay.";
"error_heating_profile" = "Heating sections cannot overlap. Please adjust the times to avoid an overlap.";
"dayFull1" = "Monday";
"dayFull2"= "Tuesday";
"dayFull3"= "Wednesday";
"dayFull4"= "Thursday";
"dayFull5"= "Friday";
"dayFull6"= "Saturday";
"dayFull7"= "Sunday";
"dayFull8"= "Holiday";
"monthFull1" = "January";
"monthFull2" = "February";
"monthFull3" = "March";
"monthFull4" = "April";
"monthFull5" = "May";
"monthFull6" = "June";
"monthFull7" = "July";
"monthFull8" = "August";
"monthFull9" = "September";
"monthFull10" = "October";
"monthFull11" = "November";
"monthFull12" = "December";
"debug_account1"= "*@jsjsdebug.com";
"debug_account2"= "nik@catalystpics.co.uk";
"debug_account3"= "johnsinc@bigfoot.com";
"debug_account4"= "john@work.com";
"device_type1"= "light-dimmer";
"device_type2"= "socket";
"device_type3"= "open-close";
"device_type4"= "mood";
"device_type5"= "mood";
"device_type6"= "mood";
"device_type7"= "heating";
"device_type8"= "camera";
"device_type9"= "mag-switch";
"device_type10"= "pir-sensor";
"device_type11"= "colour-leds";
"device_type_friendly1"= "Dimming";
"device_type_friendly2"= "On/Off";
"device_type_friendly3"= "Open Close";
"device_type_friendly4"= "Mood 1";
"device_type_friendly5"= "Mood 2";
"device_type_friendly6"= "Mood 3";
"device_type_friendly7"= "Heating control";
"device_type_friendly8"= "Camera";
"device_type_friendly9"= "Magnetic door switch";
"device_type_friendly10"= "PIR sensor";
"device_type_friendly11"= "Colour LEDs";

